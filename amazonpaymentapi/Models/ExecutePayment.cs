﻿using System;

#region Namespace
namespace amazonpaymentapi
{
    #region Input
    public class ExecutePaymentInput
    {
        public decimal amount { get; set; }
        public string amazon_order_reference_id { get; set; }
        public string mobileno { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string account_type { get; set; }
        public string email { get; set; }
        public string ip_payment_agent { get; set; }
        public string ip_payment_client { get; set; }
        public string last6digitccno { get; set; }
        public string csreasoncode { get; set; }
        public string productcode { get; set; }
        public bool? retry { get; set; }
        public string devicetype { get; set; }
        public bool? autotopup { get; set; }
        public string referencecode { get; set; }
        //13-Feb-2019: Moorthy : Added for 'Some phone numbers are not captured in full' issue
        public string AliasName { get; set; }
    } 
    #endregion

    #region Output
    public class ExecutePaymentOutput : CommonOutput
    {
        public string transaction_id { get; set; }
        public string order_id { get; set; }
        public string amazon_order_reference_id { get; set; }
    } 
    #endregion
} 
#endregion 