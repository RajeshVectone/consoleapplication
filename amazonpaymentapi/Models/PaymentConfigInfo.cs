﻿using System;
namespace amazonpaymentapi
{
    public class PaymentConfigInfo : CommonOutput
    {
        public string sitecode { get; set; }
        public string payment_agent { get; set; }
        public string payment_mode { get; set; }
        public string iccid { get; set; }
        public string brand_type { get; set; }
        public int brand { get; set; }
        public string currency { get; set; }
        public string orderid { get; set; }
    }
} 