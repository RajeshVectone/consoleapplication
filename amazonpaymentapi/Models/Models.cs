﻿namespace amazonpaymentapi
{
    public class CommonOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class ClientTokenOutput : CommonOutput
    {
        public string country { get; set; }
        public string access_key { get; set; }
        public string secret_key { get; set; }
        public string merchant_id { get; set; }
        public string lwa_client_id { get; set; }
    }
}