﻿namespace amazonpaymentapi
{
    public class Credentials
    {
        public string username { get; set; }
        public string password { get; set; }
        public string url_key { get; set; }
    }

    //public class CTALoginkey
    //{
    //    public int errcode { get; set; }
    //    public string errmsg { get; set; }
    //}

    //public class UspapireturntokenstatusInput
    //{
    //    public string projectname { get; set; }
    //    public string tokenid { get; set; }
    //}

    //public class UspapireturntokenstatusOutput
    //{
    //    public string status { get; set; }
    //}
} 