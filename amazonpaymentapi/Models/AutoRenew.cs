﻿using System;

namespace amazonpaymentapi
{
    public class AutoRenewInput
    {
        public string subscription_id { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string country { get; set; }
        public string referencecode { get; set; }
        public string accountid { get; set; }
        public string sitecode { get; set; }
        public string product_code { get; set; }
        public string application_code { get; set; }
        public string payment_agent { get; set; }
        public string service_type { get; set; }
        public string payment_mode { get; set; }
        public string client_ip { get; set; }
        public string email { get; set; }
        public string account_type { get; set; }
        public string last6digitccno { get; set; }
    }

    public class AutoRenewOutput : CommonOutput
    {
        public string transaction_id { get; set; }
        public string order_id { get; set; }
    }
} 