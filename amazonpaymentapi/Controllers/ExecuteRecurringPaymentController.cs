﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;
using AmazonPay;
using AmazonPay.StandardPaymentRequests;
using AmazonPay.CommonRequests;
using AmazonPay.Responses;
using NLog;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AmazonPay.RecurringPaymentRequests;

namespace amazonpaymentapi
{
    public class ExecuteRecurringPaymentController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private static Client client = null;
        private static AmazonPay.CommonRequests.Configuration clientConfig = null;
        private static Dictionary<string, string> apiResponse = new Dictionary<string, string>();
        private static string AMAZONORDERREFERENCEID = "";
        private static decimal AMOUNT = 0.0M;
        private static string COUNTRY = "";
        private static string confirmJson = "";
        private static string authorizeJson = "";
        private static string captureJson = "";
        private static string amazonAuthorizationId;
        private static IList<string> amazonCaptureIdList = new List<string>();
        private static bool captureNow;
        private static string captureReferenceId = "";
        private static string amazonCaptureId = "";
        private static string SellerOrderId = "";
        #endregion

        #region Post
        [BasicAuthenticationFilter]
        public HttpResponseMessage Post(ExecutePaymentInput input)
        {
            Log.Info("Execute Recurring Payment Controller");
            ExecutePaymentOutput output = new ExecutePaymentOutput();
            try
            {
                if (input != null)
                {
                    Log.Info("Input : " + JsonConvert.SerializeObject(input, Formatting.Indented));

                    output.amazon_order_reference_id = input.amazon_order_reference_id;
                    input.country = input.country.ToLower();
                    PaymentConfigInfo paymentConfigInfo = GetPaymentConfigInfo(input.mobileno, input.account_type);
                    if (paymentConfigInfo != null && paymentConfigInfo.errcode == 0)
                    {
                        Log.Info("PaymentConfigInfo : " + JsonConvert.SerializeObject(paymentConfigInfo, Formatting.Indented));

                        string orderId = paymentConfigInfo.brand_type + "-" + (String.IsNullOrEmpty(input.devicetype) ? "WEB" : input.devicetype.ToString().ToUpper()) + "-" + Math.Abs(input.amount) + "-AMZ-" + paymentConfigInfo.orderid;
                        SellerOrderId = output.order_id = orderId;
                        Log.Info("orderId : " + orderId);

                        AMAZONORDERREFERENCEID = input.amazon_order_reference_id;
                        AMOUNT = input.amount;
                        COUNTRY = input.country;

                        clientConfig = new AmazonPay.CommonRequests.Configuration();

                        clientConfig.WithAccessKey(ConfigurationManager.AppSettings["access_key_" + input.country])
                            .WithSecretKey(ConfigurationManager.AppSettings["secret_key_" + input.country])
                            .WithMerchantId(ConfigurationManager.AppSettings["merchant_id_" + input.country])
                            .WithClientId(ConfigurationManager.AppSettings["lwa_client_id_" + input.country])
                            //.WithSandbox(true);
                            .WithSandbox(false);

                        if (input.country == "uk")
                            clientConfig.WithRegion(Regions.supportedRegions.uk);
                        else
                            clientConfig.WithRegion(Regions.supportedRegions.us);


                        client = new Client(clientConfig);

                        MakeApiCallConfirmAndAuthorize();

                        Log.Info("confirmJson : " + confirmJson);
                        Log.Info("authorizeJson : " + authorizeJson);
                        Log.Info("captureJson : " + captureJson);

                        output.errcode = 0;
                        output.errmsg = "Success";
                        output.transaction_id = captureReferenceId;
                        //output.order_id = amazonCaptureId;
                        //Log.Info("Input : " + JsonConvert.SerializeObject(input, Formatting.Indented));

                        InsertPayment(paymentConfigInfo, input, output.transaction_id, orderId, output.errcode == 0 ? 100 : -1, output.errmsg);

                        //TODO: Need to change
                        //if (input.autotopup != null && input.autotopup == true)
                            InsertAutoRenew(input.mobileno, input.amazon_order_reference_id, input.amount, input.currency, input.devicetype);
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = string.Format("Payment Config Info not found for mobileno:{0} and account_type:{1}", input.mobileno, input.account_type);
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "input details not found";
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("AmazonRejected") || ex.Message.Contains("TransactionTimedOut"))
                    output.errcode = -1;
                else if (ex.Message.Contains("PaymentMethodNotAllowed"))
                    output.errcode = -2;
                else if (ex.Message.Contains("InvalidPaymentMethod") || ex.Message.Contains("InvalidParameterValue"))
                    output.errcode = -3;
                else
                    output.errcode = -1;
                if (ex.Message.IndexOf("==>") > -1)
                    output.errmsg = ex.Message.Split(new[] { "==>" }, StringSplitOptions.RemoveEmptyEntries)[1];
                else
                    output.errmsg = ex.Message;
                Log.Error(ex.Message);
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(output, Formatting.Indented));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        #endregion

        #region MakeApiCallConfirmAndAuthorize
        public static void MakeApiCallConfirmAndAuthorize()
        {
            ConfirmBillingAgreementApiCall();
            AuthorizeApiCall();
            CaptureApiCall();
        }
        #endregion

        #region ConfirmBillingAgreementApiCall
        public static void ConfirmBillingAgreementApiCall()
        {
            try
            {
                ConfirmBillingAgreementRequest getRequestParameters = new ConfirmBillingAgreementRequest();
                getRequestParameters.WithAmazonBillingreementId(AMAZONORDERREFERENCEID);

                ConfirmBillingAgreementResponse confirmOrderReferenceResponse = client.ConfirmBillingAgreement(getRequestParameters);
                if (confirmOrderReferenceResponse.GetSuccess())
                {
                    confirmJson = confirmOrderReferenceResponse.GetJson();
                }
                else
                {
                    Log.Error(confirmOrderReferenceResponse.GetJson());
                    throw new Exception(confirmOrderReferenceResponse.GetErrorCode() + "==>" + confirmOrderReferenceResponse.GetErrorMessage());
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region GenerateRandomUniqueString
        public static string GenerateRandomUniqueString()
        {
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            GuidString = GuidString.Replace("/", "");
            return GuidString;
        }
        #endregion

        #region AuthorizeApiCall
        public static void AuthorizeApiCall()
        {
            try
            {
                string uniqueReferenceId = GenerateRandomUniqueString();

                AuthorizeOnBillingAgreementRequest request = new AuthorizeOnBillingAgreementRequest();
                request.WithAmazonBillingAgreementId(AMAZONORDERREFERENCEID)
                    .WithAmount(AMOUNT)
                    .WithAuthorizationReferenceId(uniqueReferenceId)
                    .WithTransactionTimeout(0)
                    .WithCaptureNow(true)
                    //.WithCurrencyCode(Regions.currencyCode.GBP)
                    .WithSellerAuthorizationNote("Vectone Mobile");

                if (COUNTRY == "uk")
                    request.WithCurrencyCode(Regions.currencyCode.GBP);
                else
                    request.WithCurrencyCode(Regions.currencyCode.EUR);

                AuthorizeResponse response = client.AuthorizeOnBillingAgreement(request);

                // Authorize was not a success Get the Error code and the Error message
                if (!response.GetSuccess())
                {
                    string errorCode = response.GetErrorCode();
                    string errorMessage = response.GetErrorMessage();
                    authorizeJson = response.GetJson();
                    Log.Error(authorizeJson);
                    throw new Exception(errorCode + "==>" + errorMessage);
                }
                else
                {
                    if (response.reasonCode == "AmazonRejected" || response.reasonCode == "TransactionTimedOut" || response.reasonCode == "InvalidPaymentMethod")
                        throw new Exception(response.reasonCode + "==>" + response.reasonCode);
                    amazonAuthorizationId = response.GetAuthorizationId();
                    captureNow = response.GetCaptureNow();

                    // If captureNow was true then the capture has already happened. save the capture id(s).
                    if (captureNow)
                    {
                        amazonCaptureIdList = response.GetCaptureIdList();
                    }
                    authorizeJson = response.GetJson();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region CaptureApiCall
        public static void CaptureApiCall()
        {
            try
            {
                string captureId = "";
                string uniqueReferenceId = GenerateRandomUniqueString();

                // If the capture has not happened on the previous Authorize API call then capture the amount.
                if (!captureNow)
                {
                    CaptureRequest captureRequestParameters = new CaptureRequest();
                    captureRequestParameters.WithAmazonAuthorizationId(amazonAuthorizationId)
                        .WithAmount(AMOUNT)
                        .WithCaptureReferenceId(uniqueReferenceId)
                        //.WithCurrencyCode(Regions.currencyCode.GBP)
                        .WithSellerCaptureNote("Vectone Mobile");

                    if (COUNTRY == "uk")
                        captureRequestParameters.WithCurrencyCode(Regions.currencyCode.GBP);
                    else
                        captureRequestParameters.WithCurrencyCode(Regions.currencyCode.EUR);

                    CaptureResponse captureResponse = client.Capture(captureRequestParameters);

                    // Capture was not a success Get the Error code and the Error message
                    if (!captureResponse.GetSuccess())
                    {
                        string errorCode = captureResponse.GetErrorCode();
                        string errorMessage = captureResponse.GetErrorMessage();
                        captureJson = "Capture API call Failed" + Environment.NewLine + captureResponse.GetJson();
                        Log.Error(captureJson);
                        throw new Exception(errorCode + "==>" + errorMessage);
                    }
                    else
                    {
                        // In this example the below is to simply display the output
                        captureJson = captureResponse.GetJson();
                    }
                }
                else
                {
                    // In this case the capture had already happened . running the GetCaptureDetails API call to get the output of the capture.
                    GetCaptureDetailsRequest getCaptureRequestParameters = new GetCaptureDetailsRequest();
                    foreach (string id in amazonCaptureIdList)
                    {
                        captureId = id;
                    }
                    getCaptureRequestParameters.WithAmazonCaptureId(captureId);

                    CaptureResponse getCaptureDetailsResponse = client.GetCaptureDetails(getCaptureRequestParameters);

                    if (getCaptureDetailsResponse.GetSuccess())
                    {
                        captureReferenceId = getCaptureDetailsResponse.captureReferenceId;
                        amazonCaptureId = getCaptureDetailsResponse.amazonCaptureId;
                        captureJson = getCaptureDetailsResponse.GetJson();
                    }
                    else
                    {
                        throw new Exception(getCaptureDetailsResponse.GetErrorCode() + "==>" + getCaptureDetailsResponse.GetErrorMessage());
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region InsertAutoRenew
        //Used to insert auto renew details
        void InsertAutoRenew(string account_id, string token, decimal topupamount, string currency, string device_type)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payment_server"].ConnectionString))
                {
                    conn.Open();
                    var sp = "amazon_insert_subscribtion_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @account_id = account_id,
                                @amz_subscribtionid = token,
                                @topupamount = topupamount,
                                @currency = currency,
                                @device_type = device_type
                            },
                            commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
        #endregion

        #region InsertPayment
        //Used to insert payment log
        void InsertPayment(PaymentConfigInfo paymentConfigInfo, ExecutePaymentInput executePayment, string transaction_Id, string orderId, int status, string message = "")
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payment_server"].ConnectionString))
                {
                    conn.Open();
                    var sp = "amazon_insert_payment_transaction_detail";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sitecode = paymentConfigInfo.sitecode,
                                @productcode = paymentConfigInfo.brand_type + "-" + Math.Truncate(executePayment.amount),
                                @paymentagent = paymentConfigInfo.payment_agent,
                                @ref_id = orderId,//paymentConfigInfo.brand_type + "-WEB-" + Math.Truncate(executePayment.amount) + "-PAYPAL-" + paymentConfigInfo.orderid,
                                @servicetype = executePayment.account_type == "1" ? "MVNO" : "CHILLITALK",
                                @paymentmode = paymentConfigInfo.payment_mode,
                                @paymentstep = 7,
                                @paymentstatus = 3,
                                @accountid = executePayment.mobileno,
                                @last6digitccno = executePayment.last6digitccno,
                                @totalamount = executePayment.amount,
                                @currency = executePayment.currency,
                                @merchantid = transaction_Id,
                                @providercode = "AP",
                                @csreasoncode = status,
                                @generalerrorcode = status,
                                @message = message,
                                @ip_payment_agent = executePayment.ip_payment_agent,
                                @ip_payment_client = executePayment.ip_payment_client,
                                @email = executePayment.email,
                                @paypal_transid = transaction_Id,
                                @paypal_email_id = Helper.AMAZONACCOUNT
                            },
                            commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
        #endregion

        #region GetPaymentConfigInfo
        //Used to get the Payment Config Info
        PaymentConfigInfo GetPaymentConfigInfo(string mobileno, string account_type)
        {
            PaymentConfigInfo output = new PaymentConfigInfo();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["crm_server"].ConnectionString))
                {
                    conn.Open();
                    var sp = "amazon_get_payment_config_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @mobileno = mobileno,
                                @account_type = account_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        output.errcode = 0;
                        output.errmsg = "Success";
                        output.sitecode = result.ElementAt(0).sitecode;
                        output.payment_agent = result.ElementAt(0).PAYMENT_AGENT;
                        output.payment_mode = result.ElementAt(0).PAYMENT_MODE;
                        output.brand_type = result.ElementAt(0).brand_type;
                        output.brand = result.ElementAt(0).brand;
                        output.currency = result.ElementAt(0).currency;
                        output.orderid = result.ElementAt(0).orderid;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = string.Format("Payment config info not found for {0}   {1}", mobileno, account_type);
                        Log.Error(string.Format("Payment config info not found for {0}   {1}", mobileno, account_type));
                    }
                }
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
                Log.Error(ex.Message);
            }
            return output;
        }
        #endregion
    }
}
