﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;
using NLog;
using Newtonsoft.Json;

namespace amazonpaymentapi
{
    public class ClientTokenController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        [BasicAuthenticationFilter]
        public HttpResponseMessage Get(string country = "uk")
        {
            Log.Info("Client Token Controller");
            ClientTokenOutput output = new ClientTokenOutput();
            try
            {
                output.country = country;
                output.access_key = Helper.GetAppSettings("access_key_" + country);
                output.secret_key = Helper.GetAppSettings("secret_key_" + country);
                output.merchant_id = Helper.GetAppSettings("merchant_id_" + country);
                output.lwa_client_id = Helper.GetAppSettings("lwa_client_id_" + country);

                Log.Debug(JsonConvert.SerializeObject(output));
                if (output.access_key != "" && output.secret_key != "" && output.merchant_id != "" && output.lwa_client_id != "")
                {
                    output.errcode = 0;
                    output.errmsg = "Success";
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "Some token missing";
                }
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
                Log.Error(ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
