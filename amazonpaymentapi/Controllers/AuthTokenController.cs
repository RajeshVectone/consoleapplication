﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;

namespace amazonpaymentapi
{
    public class AuthTokenController : ApiController
    {
        //[BasicAuthenticationFilter]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Authorization: Basic " + Helper.GetAuthToken());
        }
    }
}
