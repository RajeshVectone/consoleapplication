﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using System.Security.Principal;
using NLog;

namespace amazonpaymentapi
{
    public class DummyPrincipalProvider : IProvidePrincipal
    {
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public IPrincipal ReturnTokenStatus(string projectName, string tokenId)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["auth_server"].ConnectionString))
            {
                conn.Open();
                try
                {
                    var result = conn.Query<dynamic>(
                            "Usp_API_Return_Token_Status", new
                            {
                                @Proj_Name = projectName,
                                @Token_ID = tokenId
                            },
                            commandType: CommandType.StoredProcedure);

                    if (result != null && result.Count() > 0 && result.ElementAt(0).Status.ToLower() == "ok")
                    {
                        var identity = new GenericIdentity(tokenId);
                        IPrincipal principal = new GenericPrincipal(identity, new[] { "User" });
                        return principal;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }
            }
            return null;
        }

        public IPrincipal CreatePrincipal(string username, string password, string projectId)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["auth_server"].ConnectionString))
            {
                conn.Open();
                try
                {
                    var result = conn.Query<dynamic>(
                            "USP_API_Get_API_Details", new
                            {
                                @User_Id = username,
                                @Pwd = password,
                                @Proj_Id = projectId
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode == 1 && result.ElementAt(0).errmsg == "Success")
                    {
                        var identity = new GenericIdentity(username);
                        IPrincipal principal = new GenericPrincipal(identity, new[] { "User" });
                        return principal;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }
            }
            return null;
        }
    }
}