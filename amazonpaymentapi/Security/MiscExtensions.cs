﻿using System;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.ComponentModel;
using System.Web.Routing;
using System.Globalization;

namespace apnsproviderapi
{   
	public static class MiscExtensions
	{
		/// <summary>Converts bytes into a hex string.</summary>
		public static string ToHexString(this byte[] bytes, int length = 0)
		{
			if (bytes == null || bytes.Length <= 0)
				return "";

			var sb = new StringBuilder();

			foreach (byte b in bytes)
			{
				sb.Append(b.ToString("x2"));

				if (length > 0 && sb.Length >= length)
					break;
			}
			return sb.ToString();
		}
	}
}
