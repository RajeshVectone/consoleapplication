﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMSOTAEXE
{
    public class GetCustomerInfoOutput
    {
        public string mobileno { get; set; }
        public string sitecode { get; set; }
        public string iccid { get; set; }
        public string ota_command { get; set; }
        public string ota_length { get; set; }
        public int ID { get; set; }
    }
}
