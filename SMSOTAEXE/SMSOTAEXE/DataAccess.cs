﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace SMSOTAEXE
{
    #region DataAccess Class
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetCustomerInfo
        public static List<GetCustomerInfoOutput> GetCustomerInfo()
        {
            List<GetCustomerInfoOutput> outputList = new List<GetCustomerInfoOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "smsota_exe_ota_process2";
                    var result = conn.Query<GetCustomerInfoOutput>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetCustomerInfo() " +  ex.Message);
            }
            return outputList;
        }
        #endregion

        #region UpdateOtaStatus
        public static void UpdateOtaStatus(int id, string sitecode, string sms_id)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "smsota_exe_update_ota_porcess2";
                    var result = conn.Query<GetCustomerInfoOutput>(
                            sp, new
                            {
                                @id = id,
                                @sitecode = sitecode,
                                @sms_id = sms_id
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                log.Error("UpdateOtaStatus() "+ ex.Message);
            }
        } 
        #endregion
    }
    #endregion
}
