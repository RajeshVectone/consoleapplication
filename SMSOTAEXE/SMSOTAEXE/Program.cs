﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NLog;

namespace SMSOTAEXE
{
    class Program
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        static void Main(string[] args)
        {
            //Console.WriteLine(System.Web.HttpUtility.UrlEncode("Mod simplu de înregistrare acum. Vă rugăm să vă trimiteți selfie-ul, ID-ul foto și numărul mobil la support@vectonemobile.at și vă vom înregistra cartela SIM.",Encoding.Default).Replace("+", " "));
            //Console.WriteLine("");
            //Console.WriteLine(System.Web.HttpUtility.UrlEncode("Zarejestruj się teraz! Wyślij swoje selfie, dokument tożsamości i numer telefonu na adres support@vectonemobile.at, aby zarejestrować kartę SIM.").Replace("+", " "));
            //Console.WriteLine("");
            //Console.WriteLine(System.Web.HttpUtility.UrlEncode("Könnyű regisztráció most. Kérjük, küldje el fényképét, mobilszámát és fotóazonosítóját a support@vectonemobile.at címre, és regisztráljuk a SIM-kártyát.").Replace("+", " "));

            Console.WriteLine("Process Started");
            log.Info("Process Started");
            //while (true)
            //{
            DoProcess();
            //Thread.Sleep(60000);
            //}
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }

        private static void DoProcess()
        {
            int ukCount = 0;
            int atCount = 0;
            int frCount = 0;
            int beCount = 0;
            try
            {
                List<GetCustomerInfoOutput> outputList = DataAccess.GetCustomerInfo();
                if (outputList != null && outputList.Count > 0)
                {
                    Console.WriteLine("Total Records : " + outputList.Count);
                    log.Info("Total Records : " + outputList.Count);
                    Console.WriteLine("Start Time :" + DateTime.Now.ToString());
                    for (int index = 0; index < outputList.Count; index++)
                    {
                        string line = outputList[index].mobileno + "," + outputList[index].ota_command + "," + outputList[index].ota_length;
                        try
                        {
                            //Console.WriteLine("Sms starts");
                            //log.Info("Sms starts");
                            Console.WriteLine(outputList[index].mobileno);
                            log.Info(outputList[index].mobileno);

                            string url = "";
                            if (outputList[index].sitecode == "MCM")
                            {
                                ukCount++;
                                url = ConfigurationManager.AppSettings["mapurl" + ukCount + "_" + outputList[index].sitecode];
                                if (ukCount == 9)
                                    ukCount = 0;
                            }
                            else if (outputList[index].sitecode == "BAU")
                            {
                                atCount++;
                                url = ConfigurationManager.AppSettings["mapurl" + atCount + "_" + outputList[index].sitecode];
                                if (atCount == 9)
                                    atCount = 0;
                            }
                            else if (outputList[index].sitecode == "MFR")
                            {
                                frCount++;
                                url = ConfigurationManager.AppSettings["mapurl" + frCount + "_" + outputList[index].sitecode];
                                if (frCount == 3)
                                    frCount = 0;
                            }
                            else if (outputList[index].sitecode == "MBE")
                            {
                                beCount++;
                                url = ConfigurationManager.AppSettings["mapurl" + beCount + "_" + outputList[index].sitecode];
                                if (beCount == 4)
                                    beCount = 0;
                            }

                            Console.WriteLine(url);
                            log.Info(url);

                            //Used to split the line
                            string[] split = line.Split(',');
                            if (split.Length == 3)
                            {
                                string msg = split[1];

                                int lenStr = msg.Length;

                                int smsCount = 0;
                                if (lenStr % 280 != 0)
                                    smsCount = (lenStr / 280) + 1;
                                else
                                    smsCount = lenStr / 280;

                                for (int j = 0; j < smsCount; j++)
                                {
                                    string smsMsg = "";
                                    if (j == smsCount - 1)
                                        smsMsg = msg.Substring(j * 280, lenStr - (280 * j));
                                    else
                                        smsMsg = msg.Substring(j * 280, 280);

                                    //item = new object[] { "5555", "1", "1", split[0], "246", "1", split[1], "127", split[2] };
                                    //string postData = string.Format("orig-addr={0}&orig-noa={1}&dest-noa={2}&dest-addr={3}&tp-dcs={4}&tp-udhi={5}&tp-ud={6}&tp-pid={7}&tp-udl={8}", item);
                                    //UK - 8888
                                    //AT - 5555
                                    //BE - 5555
                                    //FR - 5555

                                    string postData = "orig-addr=5555" +
                                    "&orig-noa=1" +
                                    "&dest-addr=" + split[0] +
                                    "&dest-noa=1" +
                                    "&tp-dcs=246" +
                                    "&tp-pid=127" +
                                    "&tp-udhi=1" +
                                    "&tp-ud=" + smsMsg +
                                    "&tp-udl=" + split[2];
                                    //"&payload-type=text";

                                    log.Info("postData : " + postData);
                                    HttpWebRequest Webrequest = (HttpWebRequest)WebRequest.Create(url);
                                    Webrequest.Method = "POST";
                                    Webrequest.ContentType = "application/x-www-form-urlencoded";

                                    ASCIIEncoding enc = new ASCIIEncoding();
                                    byte[] data = enc.GetBytes(postData);
                                    Webrequest.ContentLength = data.Length;

                                    Stream newStream = Webrequest.GetRequestStream();
                                    newStream.Write(data, 0, data.Length);
                                    newStream.Close();

                                    HttpWebResponse wr = (HttpWebResponse)Webrequest.GetResponse();
                                    StreamReader srd = new StreamReader(wr.GetResponseStream());
                                    string sResponse = srd.ReadToEnd();
                                    log.Info("sResponse : " + sResponse);
                                    string sms_id = "0";
                                    if (sResponse.Contains(":"))
                                        sms_id = sResponse.Split(':')[1];

                                    DataAccess.UpdateOtaStatus(outputList[index].ID, outputList[index].sitecode, sms_id);
                                }
                                //                            ASCIIEncoding encoding = new ASCIIEncoding();

                                //                           string postData = string.Format("orig-addr={0}&orig-noa={1}&dest-noa={2}&dest-addr={3}&tp-dcs={4}&tp-udhi={5}&tp-ud={6}&tp-pid={7}&tp-udl={8}",
                                //"5555", "1", "1", split[0], "246", "1", split[1], "127", split[2]);

                                //                            //Console.WriteLine(postData);
                                //                            log.Info(postData);

                                //                            try
                                //                            {
                                //                                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
                                //                                smsReq.Method = "POST";
                                //                                smsReq.ContentType = "application/x-www-form-urlencoded";
                                //                                smsReq.ServicePoint.Expect100Continue = false;
                                //                                ASCIIEncoding enc = new ASCIIEncoding();
                                //                                byte[] data = enc.GetBytes(postData);
                                //                                smsReq.ContentLength = data.Length;

                                //                                Stream newStream = smsReq.GetRequestStream();
                                //                                newStream.Write(data, 0, data.Length);
                                //                                newStream.Close();

                                //                                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                                //                                Stream resStream = smsRes.GetResponseStream();

                                //                                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                                //                                StreamReader readStream = new StreamReader(resStream, encode);
                                //                                string sResponse = readStream.ReadToEnd().Trim();

                                //                                Console.WriteLine(sResponse);
                                //                                log.Info(sResponse);

                                //                                string sms_id = "0";
                                //                                if (sResponse.Contains(":"))
                                //                                    sms_id = sResponse.Split(':')[1];

                                //                                DataAccess.UpdateOtaStatus(outputList[index].ID, outputList[index].sitecode, sms_id);
                                //                            }
                                //                            catch (Exception ex)
                                //                            {
                                //                                Console.WriteLine(ex.Message);
                                //                                log.Error(ex.Message);
                                //                            }
                            }
                            else
                            {
                                Console.WriteLine("ERROR : Data does not contain expected records");
                                log.Info("ERROR : Data does not contain expected records");
                                log.Info(line);
                            }

                            //Console.WriteLine("Sms ends");
                            //log.Info("Sms ends");

                            Thread.Sleep(5000); //5 Secs 
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            log.Error(ex.Message);
                        }
                    }
                    Console.WriteLine("End Time :" + DateTime.Now.ToString());
                }
                else
                {
                    Console.WriteLine("No records found");
                    log.Info("No records found");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error("ERROR : " + ex.Message);
            }
            Console.WriteLine("Completed");
        }
    }
}
