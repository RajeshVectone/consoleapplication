﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace JWT
{
    class Program
    {
        private static readonly DateTime UnixEpoch =
    new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("");

                // Define const Key this should be private secret key  stored in some safe place
                string file = ConfigurationSettings.AppSettings["file"];
                //string key = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
                const string sec = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.AM4Hj4fl0wi3fBG0G5ZZlf4oo6R2LUMe-1SByGxFjjA";

                //// Create Security key  using private key above:
                //// not that latest version of JWT using Microsoft namespace instead of System
                //var securityKey = new Microsoft
                //    .IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(file));

                //// Also note that securityKey length should be >256b
                //// so you have to make sure that your private key has a proper length
                ////var credentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, SecurityAlgorithms.EcdsaSha256Signature);
                //var credentials = new Microsoft.IdentityModel.Tokens.SigningCredentials
                //                  (securityKey, SecurityAlgorithms.EcdsaSha256Signature);

                var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(file));
                var credentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(
                    securityKey,
                    SecurityAlgorithms.EcdsaSha256);

                //  Finally create a Token
                var header = new JwtHeader(credentials);

                //Some PayLoad that contain information about the  customer
                var payload = new JwtPayload

           {
               { "device_token ", "AgAAAPIx5ffUCgQCcCoh6yq7LXIEUNk0+me89vLfv5ZingpyOOkgXXXyjPzYTzWmWSu+BYqcD47byirLZ++3dJccpF99hWppT7G5xAuU+y56WpSYsAS+GTAvYgwY5azDyzZiAVns55yvr4Q1nM+p27GuBBYMTZcUyLoj2q8vx/gI2XZRKVz6SQoDQWiFYGjlrTlemGxM7wcAAKf6Ga1vIyb37USn+tscOBpcOrltfomaRbSjJclK+Dx5FdSO1j0nSK9rdA2Cx2CL9idnYW7Frq1YxAGtDM7a1OymQLwibv/zD7JEmEAm21J8g3C4lGCYi+s64"},
               { "transaction_id", "2T2KBA89HR"},
               { "timestamp", ""+ GetCurrentUnixTimestampMillis()+""},
           };

                //
                var secToken = new JwtSecurityToken(header, payload);
                var handler = new JwtSecurityTokenHandler();

                // Token to String so you can use it in your client
                var tokenString = handler.WriteToken(secToken);

                Console.WriteLine(tokenString);
                Console.WriteLine("Consume Token");


                // And finally when  you received token from client
                // you can  either validate it or try to  read
                var token = handler.ReadJwtToken(tokenString);

                Console.WriteLine(token.Payload.First().Value);

                Console.ReadLine();
            }
            catch (Exception ex)
            {

            }
        }


        public static long GetCurrentUnixTimestampMillis()
        {
            return (long)(DateTime.UtcNow - UnixEpoch).TotalMilliseconds;
        }
    }
}



//using Microsoft.IdentityModel.Tokens;   
//using System;   
//using System.IdentityModel.Tokens.Jwt;   
//using System.Security.Claims;   
//using System.Security.Cryptography;   
//using System.Security.Cryptography.X509Certificates;  
//using System.Text;
//using System.Configuration;


//namespace JWTes256
//{

//    class Program
//    {
//        public static bool verbose = false;
//        //public string GenerateJWT()
//        static void Main(string[] args)
//        {
//            DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

//            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
//            // Provide proper private key
//            //string privateSecretKey = "OfED+KgbZxtu4e4+JSQWdtSgTnuNixKy1nMVAEww8QL3IN33XusJhrz9HXmIrdyX2F41xJHG4uj5/2Dzv3xjYYvqxexm3X3X5TOf3WoM1VNloJ7UnbqUJOiEjgK8sRdJntgfomO4U8s67cpysk0h9rc0He4xRspEjOapFfDg+VG8igidcNgbNDSSaV4491Fo3sq2aGSCtYvekzs7JwXJnNAyvDSJjfK/7M8MpxSMnm1vMscBXyiYFXhGC4wqWlYBE828/5DNyw3QZW5EjD7hvDrY5OlYd4smCTa53helNnJz5NT9HQaDbE2sMwIDAQABAoIBAEs63TvT94njrPDP3A/sfCEXg1F2y0D/PjzUhM1aJGcRiOUXnGlYdViGhLnnJoNZTZm9qI1LT0NWcDA5NmBN6gcrk2EApyTt1D1i4AQ66rYoTF9iEC4Wye28v245BYESA6IIelgIxXGsVyllERsbTkaphzibbYfHmvwMxkn135Zfzd/NOXl/O32vYIomzrNEP+tN2WXhhG8c8+iZ8PErBV3CqrYogYy97d2CeQbXcpd5unPiU4TK0nnzeBAXdgeYuJHFC45YHl9UvShRoe6CHR47ceIGp6WMc5BTyyTkZpctuYJTwaChdj/QuRSkTYmn6jFL+MRfYQJ8VVwSVo5DbkECgYEA4/YIMKcwObYcSuHzgkMwH645CRDoy9M98eptAoNLdJBHYz23U5IbGL1+qHDDCPXxKs9ZG7EEqyWezq42eoFoebLA5O6/xrYXoaeIb094dbCF4D932hAkgAaAZkZVsSiWDCjYSV+JoWX4NVBcIL9yyHRhaaPVULTRbPsZQWq9+hMCgYEA48j4RGO7CaVpgUVobYasJnkGSdhkSCd1VwgvHH3vtuk7/JGUBRaZc0WZGcXkAJXnLh7QnDHOzWASdaxVgnuviaDi4CIkmTCfRqPesgDR2Iu35iQsH7P2/o1pzhpXQS/Ct6J7/GwJTqcXCvp4tfZDbFxS8oewzp4RstILj+pDyWECgYByQAbOy5xB8GGxrhjrOl1OI3V2c8EZFqA/NKy5y6/vlbgRpwbQnbNy7NYj+Y/mV80tFYqldEzQsiQrlei78Uu5YruGgZogL3ccj+izUPMgmP4f6+9XnSuN9rQ3jhy4k4zQP1BXRcim2YJSxhnGV+1hReLknTX2IwmrQxXfUW4xfQKBgAHZW8qSVK5bXWPjQFnDQhp92QM4cnfzegxe0KMWkp+VfRsrw1vXNx";

//            string privateSecretKey = ConfigurationSettings.AppSettings["file"];

//            //rsa = DecodeRSAPrivateKey(FromBase64Url(privateSecretKey));
            
//            rsa = DecodeRSAPrivateKey(FromBase64Url(privateSecretKey));
//            //convert to csp format
//            var bytes = rsa.ExportCspBlob(false);
//            var publicKey = Convert.ToBase64String(bytes);
//            //

//            RsaSecurityKey _signingKey = new RsaSecurityKey(rsa);
//            Microsoft.IdentityModel.Tokens.SigningCredentials signingCredentials =
//                   new Microsoft.IdentityModel.Tokens.SigningCredentials(_signingKey, SecurityAlgorithms.RsaSha256);

//            JwtHeader head = new JwtHeader(signingCredentials);
//            head.Add("kid", "lzo-firstpublickey");


//            string sNewGuid = Guid.NewGuid().ToString("n");

//            var claims = new[]
//              {
//                  new Claim(JwtRegisteredClaimNames.Iss, "s6BhdRkqt3"),
//                  new Claim(JwtRegisteredClaimNames.Sub, "s6BhdRkqt3"),
//                  new Claim(JwtRegisteredClaimNames.Aud, "https://cis.ncrs/connect/token"),
//                  new Claim(JwtRegisteredClaimNames.Jti,  sNewGuid),
//                  new Claim(JwtRegisteredClaimNames.Exp, ((Int64)DateTime.Now.AddMinutes(55).Subtract(UnixEpoch).TotalSeconds).ToString(System.Globalization.CultureInfo.InvariantCulture), ClaimValueTypes.Integer64),
//                  new Claim(JwtRegisteredClaimNames.Iat, ((Int64)DateTime.Now.Subtract(UnixEpoch).TotalSeconds).ToString(System.Globalization.CultureInfo.InvariantCulture), ClaimValueTypes.Integer64)
//              };


//            JwtPayload payload = new JwtPayload(claims);
//            JwtSecurityToken jwt = new JwtSecurityToken(head, payload);
//            jwt.SigningKey = _signingKey;
//            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
//           // return encodedJwt;
//        }

//        private static byte[] FromBase64Url(string base64Url)
//        {
//            string base64 = string.Empty;
//            if (!string.IsNullOrEmpty(base64Url))
//            {
//                string padded = base64Url.Length % 4 == 0
//                    ? base64Url : base64Url + "====".Substring(base64Url.Length % 4);
//                base64 = padded.Replace("_", "/")
//                                        .Replace("-", "+");
//            }
//            return Convert.FromBase64String(base64);
//        }


//        public static RSACryptoServiceProvider DecodeRSAPrivateKey(byte[] privkey)
//        {
//            byte[] MODULUS, E, D, P, Q, DP, DQ, IQ;

//            // ---------  Set up stream to decode the asn.1 encoded RSA private key  ------
//            System.IO.MemoryStream mem = new System.IO.MemoryStream(privkey);
//            System.IO.BinaryReader binr = new System.IO.BinaryReader(mem);    //wrap Memory Stream with BinaryReader for easy reading
//            byte bt = 0;
//            ushort twobytes = 0;
//            int elems = 0;
//            try
//            {
//                twobytes = binr.ReadUInt16();
//                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
//                    binr.ReadByte();        //advance 1 byte
//                else if (twobytes == 0x8230)
//                    binr.ReadInt16();       //advance 2 bytes
//                else
//                    return null;

//                twobytes = binr.ReadUInt16();
//                if (twobytes != 0x0102) //version number
//                    return null;
//                bt = binr.ReadByte();
//                if (bt != 0x00)
//                    return null;


//                //------  all private key components are Integer sequences ----
//                elems = GetIntegerSize(binr);
//                MODULUS = binr.ReadBytes(elems);

//                elems = GetIntegerSize(binr);
//                E = binr.ReadBytes(elems);

//                elems = GetIntegerSize(binr);
//                D = binr.ReadBytes(elems);

//                elems = GetIntegerSize(binr);
//                P = binr.ReadBytes(elems);

//                elems = GetIntegerSize(binr);
//                Q = binr.ReadBytes(elems);

//                elems = GetIntegerSize(binr);
//                DP = binr.ReadBytes(elems);

//                elems = GetIntegerSize(binr);
//                DQ = binr.ReadBytes(elems);

//                elems = GetIntegerSize(binr);
//                IQ = binr.ReadBytes(elems);

//                Console.WriteLine("showing components ..");
//                if (verbose)
//                {
//                    showBytes("\nModulus", MODULUS);
//                    showBytes("\nExponent", E);
//                    showBytes("\nD", D);
//                    showBytes("\nP", P);
//                    showBytes("\nQ", Q);
//                    showBytes("\nDP", DP);
//                    showBytes("\nDQ", DQ);
//                    showBytes("\nIQ", IQ);
//                }

//                // ------- create RSACryptoServiceProvider instance and initialize with public key -----
//                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
//                RSAParameters RSAparams = new RSAParameters();
//                RSAparams.Modulus = MODULUS;
//                RSAparams.Exponent = E;
//                RSAparams.D = D;
//                RSAparams.P = P;
//                RSAparams.Q = Q;
//                RSAparams.DP = DP;
//                RSAparams.DQ = DQ;
//                RSAparams.InverseQ = IQ;
//                RSA.ImportParameters(RSAparams);
//                return RSA;
//            }
//            catch (Exception)
//            {
//                return null;
//            }
//            finally
//            {
//                binr.Close();
//            }
//        }

//        public static void showBytes(String info, byte[] data)
//        {
//            Console.WriteLine("{0}  [{1} bytes]", info, data.Length);
//            for (int i = 1; i <= data.Length; i++)
//            {
//                Console.Write("{0:X2}  ", data[i - 1]);
//                if (i % 16 == 0)
//                    Console.WriteLine();
//            }
//            Console.WriteLine("\n\n");
//        }

//        public static int GetIntegerSize(System.IO.BinaryReader binr)
//        {
//            byte bt = 0;
//            byte lowbyte = 0x00;
//            byte highbyte = 0x00;
//            int count = 0;
//            bt = binr.ReadByte();
//            if (bt != 0x02)     //expect integer
//                return 0;
//            bt = binr.ReadByte();

//            if (bt == 0x81)
//                count = binr.ReadByte();    // data size in next byte
//            else
//                if (bt == 0x82)
//                {
//                    highbyte = binr.ReadByte(); // data size in next 2 bytes
//                    lowbyte = binr.ReadByte();
//                    byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };
//                    count = BitConverter.ToInt32(modint, 0);
//                }
//                else
//                {
//                    count = bt;     // we already have the data size
//                }



//            while (binr.ReadByte() == 0x00)
//            {   //remove high order zeros in data
//                count -= 1;
//            }
//            binr.BaseStream.Seek(-1, System.IO.SeekOrigin.Current);     //last     ReadByte wasn't a removed zero, so back up a byte
//            return count;
//        }
//    }
//}
