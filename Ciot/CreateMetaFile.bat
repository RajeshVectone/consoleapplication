@ECHO OFF
ECHO ^<?xml version="1.0" encoding="UTF-8"?^>    > %1.meta
ECHO ^<otp-metadata profile="otp-gb-1.6" xmlns="http://logius.nl/digipoort/gb/v16/metabestand.xsd"^>    >> %1.meta
ECHO 	^<data-reference id="%1"^>    >> %1.meta
ECHO 		^<organisation^>    >> %1.meta
ECHO 			^<sender^>mumo3124@ftp.otpnet.nl^</sender^>    >> %1.meta
ECHO 			^<receiver^>ciot2658@ftp.otpnet.nl^</receiver^>    >> %1.meta
ECHO 		^</organisation^>    >> %1.meta
ECHO 		^<content mimeType="application/gpg"^>    >> %1.meta
ECHO 			^<filename^>%1^</filename^>    >> %1.meta
ECHO 			^<size^>%2^</size^>    >> %1.meta
ECHO 		^</content^>    >> %1.meta
ECHO 	^</data-reference^>    >> %1.meta
ECHO ^</otp-metadata^>    >> %1.meta
