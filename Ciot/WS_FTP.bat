:@echo off
@echo Encrypting files
PUSHD "D:\ftproot\ciot"
REM ***
FOR %%i IN (*.xml) DO gpg --recipient CIOT --encrypt --always-trust --encrypt-to 0xDBFAEE85 %%i
rename barablu_TFN_*.gpg mumo3124_TFN_*.gpg
FOR %%i IN (*.xml.gpg) DO CALL CreateMetaFile %%i %%~zi
REM ***
FOR %%i IN (*.xml) DO gzip -9 %%i
FOR %%i IN (*.xml.gz) DO move %%i ARCHIVE
@echo Transfering File
POPD
ftpscrpt -f DIGIPORTftpScript.scp
@echo done
