﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace SMSByTxtFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "input.txt";
            DoProcess(fileName);
            //Console.WriteLine("Completed");
            //Console.ReadLine();
        }

        private static void DoProcess(string fileName)
        {
            try
            {
                string line;
                int iLine = 0;
                using (System.IO.StreamReader file = new System.IO.StreamReader(fileName))
                {
                    int iCount = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        iCount++;
                        Console.WriteLine("Line No : " + iLine++);
                        try
                        {
                            //if (Convert.ToString(ConfigurationManager.AppSettings["run"]) == "1")
                            //{
                            Thread.Sleep(100);
                            Console.WriteLine("Sms loop starts");
                            Console.WriteLine(line);

                            string url = "";
                            url = ConfigurationManager.AppSettings["mapurl" + iCount];
                            if (iCount == 9)
                                iCount = 0;

                            Console.WriteLine(url);

                            //string sText = "Bitte senden Sie ein Foto Ihres Gesichts, ein Foto Ihres Ausweises und Ihre Handynummer an support@vectonemobile.at und wir registrieren Ihre SIM-Karte für Sie.";
                            //string[] arrText = new string[6];
                            //arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003D40301004700650072006200690061006D006100730020006B006C00690065006E00740065002C0020006A006500690020006E006F007200690074006500200074006F006C0069006100750020006E006100750064006F0074006900730020006D016B007301730020007000610073006C006100750067006F006D00690073002C0020006E0065006D&tp-udhi=1&sequence-id=1";
                            //arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003D40302006F006B0061006D006100690020012F00720065006700690073007400720075006F006B0069007400650020007300610076006F002000530049004D0020006B006F007200740065006C0119002C00200061007400730069007300690173007300640061006D0069002000700072006F006700720061006D0105002000680074007400700073&tp-udhi=1&sequence-id=2";
                            //arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003D40303003A002F002F006200690074002E006C0079002F0032006D00420076004700520045&tp-udhi=1&sequence-id=3";

                            //arrText[3] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003B40301004100740073006901730073006B0069007400650020007300610076006F00200076006500690064006F002C002000610073006D0065006E007300200074006100700061007400790062011700730020006900720020006D006F00620069006C0069006F006A006F002000740065006C00650066006F006E006F0020006E0075006F00740072&tp-udhi=1&sequence-id=1";
                            //arrText[4] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003B4030200610075006B0105002000610064007200650073007500200073007500700070006F0072007400400076006500630074006F006E0065006D006F00620069006C0065002E006100740020006900720020006D0065007300200075017E00720065006700690073007400720075006F00730069006D00650020006A0075006D00730020006A016B&tp-udhi=1&sequence-id=2";
                            //arrText[5] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003B4030300730173002000530049004D0020006B006F007200740065006C0119002E&tp-udhi=1&sequence-id=3";

                            string[] arrText = new string[3];
                            arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500030D0301004700650072006200690061006D006100730020006B006C00690065006E00740065002C0020006A006500690020006E006F007200690074006500200074006F006C0069006100750020006E006100750064006F0074006900730020006D016B007301730020007000610073006C006100750067006F006D00690073002C0020006E0065006D&tp-udhi=1&sequence-id=1";
                            arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500030D0302006F006B0061006D006100690020012F00720065006700690073007400720075006F006B0069007400650020007300610076006F002000530049004D0020006B006F007200740065006C0119002C00200061007400730069007300690173007300640061006D0069002000700072006F006700720061006D0105002000680074007400700073&tp-udhi=1&sequence-id=2";
                            arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500030D0303003A002F002F006200690074002E006C0079002F0032006E00570062004E0038004E&tp-udhi=1&sequence-id=3";

                            for (int i = 0; i < arrText.Length; i++)
                            {
                                ASCIIEncoding encoding = new ASCIIEncoding();
                                string postData = string.Format(arrText[i], line.Trim());

                                Console.WriteLine(postData);

                                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
                                smsReq.Method = "POST";
                                smsReq.ContentType = "application/x-www-form-urlencoded";
                                smsReq.ServicePoint.Expect100Continue = false;
                                ASCIIEncoding enc = new ASCIIEncoding();
                                byte[] data = enc.GetBytes(postData);
                                smsReq.ContentLength = data.Length;

                                Stream newStream = smsReq.GetRequestStream();
                                newStream.Write(data, 0, data.Length);
                                newStream.Close();

                                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                                Stream resStream = smsRes.GetResponseStream();

                                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                                StreamReader readStream = new StreamReader(resStream, encode);
                                string sResponse = readStream.ReadToEnd().Trim();

                                Console.WriteLine("Sms ends");
                                Console.WriteLine(i);
                                Log(line + "--" + sResponse);
                            }
                            //}
                            //else
                            //    break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Log(ex.Message);
                        }
                    }
                    //file.Close();

                    //Used to move the file
                    //File.Move(fileName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Backups", DateTime.Now.ToString("MMddyyyyHHmmss")) + ".txt");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log("ERROR : " + ex.Message);
            }
        }

        public static void Log(string message)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + DateTime.Now.ToString("ddMMyyyy") + "_logs.txt");
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
