﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace SMSByTxtFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "input.txt";
            DoProcess(fileName);
            //Console.WriteLine("Completed");
            //Console.ReadLine();
        }

        private static void DoProcess(string fileName)
        {
            try
            {
                string line;
                int iLine = 0;
                using (System.IO.StreamReader file = new System.IO.StreamReader(fileName))
                {
                    int iCount = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        iCount++;
                        Console.WriteLine("Line No : " + iLine++);
                        try
                        {
                            //if (Convert.ToString(ConfigurationManager.AppSettings["run"]) == "1")
                            //{
                            Thread.Sleep(100);
                            Console.WriteLine("Sms loop starts");
                            Console.WriteLine(line);

                            string url = "";
                            url = ConfigurationManager.AppSettings["mapurl" + iCount];
                            if (iCount == 9)
                                iCount = 0;

                            Console.WriteLine(url);

                            //string sText = "Bitte senden Sie ein Foto Ihres Gesichts, ein Foto Ihres Ausweises und Ihre Handynummer an support@vectonemobile.at und wir registrieren Ihre SIM-Karte für Sie.";
                            //string[] arrText = new string[6];
                            //arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036A0301004B00650064007600650073002000DC0067007900660065006C00FC006E006B002C0020006100200073007A006F006C006700E1006C00740061007400E10073006F006B00200074006F007600E10062006200690020006800610073007A006E00E1006C0061007400E100E9007200740020006B00E90072006A00FC006B002C002000720065&tp-udhi=1&sequence-id=1";
                            //arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036A0302006700690073007A0074007200E1006C006A006100200069006E006700790065006E006500730065006E002000530049004D002D006B00E100720074007900E1006A00E1007400200061007A00200061006C006B0061006C006D0061007A00E100730075006E006B0020006C0065007400F6006C007400E9007300E900760065006C00200020&tp-udhi=1&sequence-id=2";
                            //arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036A030300680074007400700073003A002F002F006200690074002E006C0079002F0032006D00420076004700520045&tp-udhi=1&sequence-id=3";

                            //arrText[3] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003400301004B00E90072006A00FC006B002C0020006B00FC006C0064006A00F6006E002000650067007900200066006F007400F3007400200061007A002000610072006300E1007200F3006C002C00200061007A0020006900670061007A006F006C007600E1006E007900E1007200F3006C002000E9007300200061002000740065006C00650066006F&tp-udhi=1&sequence-id=1";
                            //arrText[4] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003400302006E0073007A00E1006D00E100740020006100200073007500700070006F0072007400400076006500630074006F006E0065006D006F00620069006C0065002E006100740020006300ED006D00720065002C00200068006F00670079002000720065006700690073007A0074007200E1006C00680061007300730075006B0020006100200053&tp-udhi=1&sequence-id=2";
                            //arrText[5] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500034003030049004D002D006B00E100720074007900E1006A00E10074002E&tp-udhi=1&sequence-id=3";


                            string[] arrText = new string[3];
                            arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036F0301004B00650064007600650073002000DC0067007900660065006C00FC006E006B002C0020006100200073007A006F006C006700E1006C00740061007400E10073006F006B00200074006F007600E10062006200690020006800610073007A006E00E1006C0061007400E100E9007200740020006B00E90072006A00FC006B002C002000720065&tp-udhi=1&sequence-id=1";
                            arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036F0302006700690073007A0074007200E1006C006A006100200069006E006700790065006E006500730065006E002000530049004D002D006B00E100720074007900E1006A00E1007400200061007A00200061006C006B0061006C006D0061007A00E100730075006E006B0020006C0065007400F6006C007400E9007300E900760065006C00200020&tp-udhi=1&sequence-id=2";
                            arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036F030300680074007400700073003A002F002F006200690074002E006C0079002F0032006E00570062004E0038004E&tp-udhi=1&sequence-id=3";

                            for (int i = 0; i < arrText.Length; i++)
                            {
                                ASCIIEncoding encoding = new ASCIIEncoding();
                                string postData = string.Format(arrText[i], line.Trim());

                                Console.WriteLine(postData);

                                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
                                smsReq.Method = "POST";
                                smsReq.ContentType = "application/x-www-form-urlencoded";
                                smsReq.ServicePoint.Expect100Continue = false;
                                ASCIIEncoding enc = new ASCIIEncoding();
                                byte[] data = enc.GetBytes(postData);
                                smsReq.ContentLength = data.Length;

                                Stream newStream = smsReq.GetRequestStream();
                                newStream.Write(data, 0, data.Length);
                                newStream.Close();

                                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                                Stream resStream = smsRes.GetResponseStream();

                                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                                StreamReader readStream = new StreamReader(resStream, encode);
                                string sResponse = readStream.ReadToEnd().Trim();

                                Console.WriteLine("Sms ends");
                                Console.WriteLine(i);
                                Log(line + "--" + sResponse);
                            }
                            //}
                            //else
                            //    break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Log(ex.Message);
                        }
                    }
                    //file.Close();

                    //Used to move the file
                    //File.Move(fileName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Backups", DateTime.Now.ToString("MMddyyyyHHmmss")) + ".txt");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log("ERROR : " + ex.Message);
            }
        }

        public static void Log(string message)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + DateTime.Now.ToString("ddMMyyyy") + "_logs.txt");
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
