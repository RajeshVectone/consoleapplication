﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace TariffAutomationUKMinutesCountrywiseConsole
{
    public class TariffAutomationUKMinutesCountrywise
    {
        public string Calldate { get; set; }
        public string Days { get; set; }
        public string Country { get; set; }
        public int? Cli { get; set; }
        public int? Mins { get; set; }
    }

    public class TariffAutomationUKMinutesSummary
    {
        public string CallType { get; set; }
        public string Calldate { get; set; }
        public double? TotalMins { get; set; }
        public double? TotalCli { get; set; }
        public double? Mins_Percentage { get; set; }
        public double? Total_No_Minutes { get; set; }
        public double? TotalUniqueCLI { get; set; }
    }

    public class TariffAutomationATMinutesCountrywise
    {
        public string Calldate { get; set; }
        public string Days { get; set; }
        public string Country { get; set; }
        public int? Cli { get; set; }
        public int? Mins { get; set; }
    }

    public class TariffAutomationATMinutesSummary
    {
        public string CallType { get; set; }
        public string Calldate { get; set; }
        public double? TotalMins { get; set; }
        public double? TotalCli { get; set; }
        public double? Mins_Percentage { get; set; }
        public double? Total_No_Minutes { get; set; }
        public double? TotalUniqueCLI { get; set; }
    }

    public class TariffAutomationBEMinutesCountrywise
    {
        public string Calldate { get; set; }
        public string Days { get; set; }
        public string Country { get; set; }
        public int? Cli { get; set; }
        public int? Mins { get; set; }
    }

    public class TariffAutomationBEMinutesSummary
    {
        public string CallType { get; set; }
        public string Calldate { get; set; }
        public double? TotalMins { get; set; }
        public double? TotalCli { get; set; }
        public double? Mins_Percentage { get; set; }
        public double? Total_No_Minutes { get; set; }
        public double? TotalUniqueCLI { get; set; }
    }

    public class TariffAutomationFRMinutesCountrywise
    {
        public string Calldate { get; set; }
        public string Days { get; set; }
        public string Country { get; set; }
        public int? Cli { get; set; }
        public int? Mins { get; set; }
    }

    public class TariffAutomationFRMinutesSummary
    {
        public string CallType { get; set; }
        public string Calldate { get; set; }
        public double? TotalMins { get; set; }
        public double? TotalCli { get; set; }
        public double? Mins_Percentage { get; set; }
        public double? Total_No_Minutes { get; set; }
        public double? TotalUniqueCLI { get; set; }
    }

    public class TariffAutomationDMATMinutesCountrywise
    {
        public string Calldate { get; set; }
        public string Days { get; set; }
        public string Country { get; set; }
        public int? Cli { get; set; }
        public int? Mins { get; set; }
    }

    public class TariffAutomationDMATMinutesSummary
    {
        public string CallType { get; set; }
        public string Calldate { get; set; }
        public double? TotalMins { get; set; }
        public double? TotalCli { get; set; }
        public double? Mins_Percentage { get; set; }
        public double? Total_No_Minutes { get; set; }
        public double? TotalUniqueCLI { get; set; }
    }

    public class TariffAutomationCallingCardMinutesCountrywise
    {
        public string calldate { get; set; }
        public string Days { get; set; }
        public string country { get; set; }
        public double? cli { get; set; }
        public double? talktime { get; set; }
    }

    public class TariffAutomationcallingcardMinutesSummary
    {
        public string CallType { get; set; }
        public string Calldate { get; set; }
        public double? TotalMins { get; set; }
        public double? TotalCli { get; set; }
        public double? Mins_Percentage { get; set; }
        public double? Total_No_Minutes { get; set; }
        public double? TotalUniqueCLI { get; set; }
    }


    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                DoProcess();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region DoProcess
        static void DoProcess()
        {
            try
            {
                string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile1"]);
                string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName1"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";

                Console.WriteLine("docName : " + docName);
                log.Info("docName : " + docName);

                File.Copy(templateName, docName);

                //UK
                TariffAutomationUKMinutesCountrywise(docName);
                TariffAutomationUKMinutesSummary(docName);

                //AT
                TariffAutomationATMinutesCountrywise(docName);
                TariffAutomationATMinutesSummary(docName);

                //BE
                TariffAutomationBEMinutesCountrywise(docName);
                TariffAutomationBEMinutesSummary(docName);

                //FR
                TariffAutomationFRMinutesCountrywise(docName);
                TariffAutomationFRMinutesSummary(docName);

                SendEmail(docName, string.Format(ConfigurationManager.AppSettings["MailSubject1"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss")));

                string templateName2 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile2"]);
                string docName2 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName2"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";

                File.Copy(templateName2, docName2);

                Console.WriteLine("docName2 : " + docName2);
                log.Info("docName2 : " + docName2);

                ////AT
                TariffAutomationDMATMinutesCountrywise(docName2);
                TariffAutomationDMATMinutesSummary(docName2);

                SendEmail(docName2, string.Format(ConfigurationManager.AppSettings["MailSubject2"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss")));


                string templateName3 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile3"]);
                string docName3 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName3"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";

                File.Copy(templateName3, docName3);

                Console.WriteLine("docName3 : " + docName3);
                log.Info("docName3 : " + docName3);

                ////AT
                TariffAutomationCallingCardMinutesCountrywise(docName3);
                TariffAutomationcallingcardMinutesSummary(docName3);

                SendEmail(docName3, string.Format(ConfigurationManager.AppSettings["MailSubject3"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region TariffAutomationUKMinutesCountrywise
        static void TariffAutomationUKMinutesCountrywise(string docName)
        {
            Console.WriteLine("TariffAutomationUKMinutesCountrywise()");
            log.Info("TariffAutomationUKMinutesCountrywise()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationUKMinutesCountrywise();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "UK").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct();
                        uint rowIndex = 3;
                        foreach (var item in distinctDates)
                        {
                            if (rowIndex == 3)
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 24, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                rowIndex++;
                            }
                            else
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 24, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                            }
                        }

                        var distinctDays = (from ld in records select new { id = ld.Days }).ToList().Distinct();
                        rowIndex = 3;
                        foreach (var item in distinctDays)
                        {
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 24, item.id, CellValues.String, wsPart);
                            rowIndex++;
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 24, item.id, CellValues.String, wsPart);
                        }

                        var distinctCountrys = (from ld in records select new { id = ld.Country }).ToList().Distinct();
                        rowIndex = 1;
                        int countryCount = 0;
                        string columnName1 = "";
                        string columnName2 = "";
                        foreach (var item in distinctCountrys)
                        {
                            switch (countryCount)
                            {
                                case 0:
                                    columnName1 = "C";
                                    columnName2 = "D";
                                    break;
                                case 1:
                                    columnName1 = "E";
                                    columnName2 = "F";
                                    break;
                                case 2:
                                    columnName1 = "G";
                                    columnName2 = "H";
                                    break;
                                case 3:
                                    columnName1 = "I";
                                    columnName2 = "J";
                                    break;
                                case 4:
                                    columnName1 = "K";
                                    columnName2 = "L";
                                    break;
                                case 5:
                                    columnName1 = "M";
                                    columnName2 = "N";
                                    break;
                                case 6:
                                    columnName1 = "O";
                                    columnName2 = "P";
                                    break;
                                case 7:
                                    columnName1 = "Q";
                                    columnName2 = "R";
                                    break;
                                case 8:
                                    columnName1 = "S";
                                    columnName2 = "T";
                                    countryCount = -1;
                                    break;
                                default:
                                    break;
                            }
                            InsertValue(columnName1, rowIndex, item.id, CellValues.String, wsPart);

                            for (int i = 0; i < distinctDates.Count(); i++)
                            {
                                var item2 = records.Where(r => r.Country == item.id && r.Calldate == distinctDates.ElementAt(i).id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    if (i == 0)
                                    {
                                        InsertValue(columnName1, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                    else
                                    {
                                        InsertValue(columnName1, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                }
                            }


                            if (countryCount == -1)
                                rowIndex += 8;
                            countryCount++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationUKMinutesCountrywise : " + ex.Message);
                log.Error("TariffAutomationUKMinutesCountrywise : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationUKMinutesSummary
        static void TariffAutomationUKMinutesSummary(string docName)
        {
            Console.WriteLine("TariffAutomationUKMinutesSummary()");
            log.Info("TariffAutomationUKMinutesSummary()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationUKMinutesSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "UK").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct(); //.OrderBy(x => x.id);

                        int index2 = 0;
                        foreach (var itemDates in distinctDates)
                        {
                            if (index2 == 0)
                            {
                                InsertValue("C", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("D", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 1)
                            {
                                InsertValue("G", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("H", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 2)
                            {
                                InsertValue("K", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("L", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 3)
                            {
                                InsertValue("O", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("P", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            index2++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationUKMinutesSummary : " + ex.Message);
                log.Error("TariffAutomationUKMinutesSummary : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationATMinutesCountrywise
        static void TariffAutomationATMinutesCountrywise(string docName)
        {
            Console.WriteLine("TariffAutomationATMinutesCountrywise()");
            log.Info("TariffAutomationATMinutesCountrywise()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationATMinutesCountrywise();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "AT").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct();
                        uint rowIndex = 3;
                        foreach (var item in distinctDates)
                        {
                            if (rowIndex == 3)
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 24, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                rowIndex++;
                            }
                            else
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 24, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                            }
                        }

                        var distinctDays = (from ld in records select new { id = ld.Days }).ToList().Distinct();
                        rowIndex = 3;
                        foreach (var item in distinctDays)
                        {
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 24, item.id, CellValues.String, wsPart);
                            rowIndex++;
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 24, item.id, CellValues.String, wsPart);
                        }

                        var distinctCountrys = (from ld in records select new { id = ld.Country }).ToList().Distinct();
                        rowIndex = 1;
                        int countryCount = 0;
                        string columnName1 = "";
                        string columnName2 = "";
                        foreach (var item in distinctCountrys)
                        {
                            switch (countryCount)
                            {
                                case 0:
                                    columnName1 = "C";
                                    columnName2 = "D";
                                    break;
                                case 1:
                                    columnName1 = "E";
                                    columnName2 = "F";
                                    break;
                                case 2:
                                    columnName1 = "G";
                                    columnName2 = "H";
                                    break;
                                case 3:
                                    columnName1 = "I";
                                    columnName2 = "J";
                                    break;
                                case 4:
                                    columnName1 = "K";
                                    columnName2 = "L";
                                    break;
                                case 5:
                                    columnName1 = "M";
                                    columnName2 = "N";
                                    break;
                                case 6:
                                    columnName1 = "O";
                                    columnName2 = "P";
                                    break;
                                case 7:
                                    columnName1 = "Q";
                                    columnName2 = "R";
                                    break;
                                case 8:
                                    columnName1 = "S";
                                    columnName2 = "T";
                                    countryCount = -1;
                                    break;
                                default:
                                    break;
                            }
                            InsertValue(columnName1, rowIndex, item.id, CellValues.String, wsPart);


                            for (int i = 0; i < distinctDates.Count(); i++)
                            {
                                var item2 = records.Where(r => r.Country == item.id && r.Calldate == distinctDates.ElementAt(i).id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    if (i == 0)
                                    {
                                        InsertValue(columnName1, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                    else
                                    {
                                        InsertValue(columnName1, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                }
                            }


                            if (countryCount == -1)
                                rowIndex += 8;
                            countryCount++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationATMinutesCountrywise : " + ex.Message);
                log.Error("TariffAutomationATMinutesCountrywise : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationATMinutesSummary
        static void TariffAutomationATMinutesSummary(string docName)
        {
            Console.WriteLine("TariffAutomationATMinutesSummary()");
            log.Info("TariffAutomationATMinutesSummary()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationATMinutesSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "AT").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct(); //.OrderBy(x => x.id);

                        int index2 = 0;
                        foreach (var itemDates in distinctDates)
                        {
                            if (index2 == 0)
                            {
                                InsertValue("C", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("D", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 1)
                            {
                                InsertValue("G", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("H", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 2)
                            {
                                InsertValue("K", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("L", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 3)
                            {
                                InsertValue("O", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("P", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            index2++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationATMinutesSummary : " + ex.Message);
                log.Error("TariffAutomationATMinutesSummary : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationBEMinutesCountrywise
        static void TariffAutomationBEMinutesCountrywise(string docName)
        {
            Console.WriteLine("TariffAutomationBEMinutesCountrywise()");
            log.Info("TariffAutomationBEMinutesCountrywise()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationBEMinutesCountrywise();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "BE").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct();
                        uint rowIndex = 3;
                        foreach (var item in distinctDates)
                        {
                            if (rowIndex == 3)
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 24, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                rowIndex++;
                            }
                            else
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 24, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                            }
                        }

                        var distinctDays = (from ld in records select new { id = ld.Days }).ToList().Distinct();
                        rowIndex = 3;
                        foreach (var item in distinctDays)
                        {
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 24, item.id, CellValues.String, wsPart);
                            rowIndex++;
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 24, item.id, CellValues.String, wsPart);
                        }

                        var distinctCountrys = (from ld in records select new { id = ld.Country }).ToList().Distinct();
                        rowIndex = 1;
                        int countryCount = 0;
                        string columnName1 = "";
                        string columnName2 = "";
                        foreach (var item in distinctCountrys)
                        {
                            switch (countryCount)
                            {
                                case 0:
                                    columnName1 = "C";
                                    columnName2 = "D";
                                    break;
                                case 1:
                                    columnName1 = "E";
                                    columnName2 = "F";
                                    break;
                                case 2:
                                    columnName1 = "G";
                                    columnName2 = "H";
                                    break;
                                case 3:
                                    columnName1 = "I";
                                    columnName2 = "J";
                                    break;
                                case 4:
                                    columnName1 = "K";
                                    columnName2 = "L";
                                    break;
                                case 5:
                                    columnName1 = "M";
                                    columnName2 = "N";
                                    break;
                                case 6:
                                    columnName1 = "O";
                                    columnName2 = "P";
                                    break;
                                case 7:
                                    columnName1 = "Q";
                                    columnName2 = "R";
                                    break;
                                case 8:
                                    columnName1 = "S";
                                    columnName2 = "T";
                                    countryCount = -1;
                                    break;
                                default:
                                    break;
                            }
                            InsertValue(columnName1, rowIndex, item.id, CellValues.String, wsPart);



                            for (int i = 0; i < distinctDates.Count(); i++)
                            {
                                var item2 = records.Where(r => r.Country == item.id && r.Calldate == distinctDates.ElementAt(i).id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    if (i == 0)
                                    {
                                        InsertValue(columnName1, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                    else
                                    {
                                        InsertValue(columnName1, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                }
                            }


                            if (countryCount == -1)
                                rowIndex += 8;
                            countryCount++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationBEMinutesCountrywise : " + ex.Message);
                log.Error("TariffAutomationBEMinutesCountrywise : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationBEMinutesSummary
        static void TariffAutomationBEMinutesSummary(string docName)
        {
            Console.WriteLine("TariffAutomationBEMinutesSummary()");
            log.Info("TariffAutomationBEMinutesSummary()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationBEMinutesSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "BE").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct(); //.OrderBy(x => x.id);

                        int index2 = 0;
                        foreach (var itemDates in distinctDates)
                        {
                            if (index2 == 0)
                            {
                                InsertValue("C", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("D", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 1)
                            {
                                InsertValue("G", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("H", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 2)
                            {
                                InsertValue("K", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("L", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 3)
                            {
                                InsertValue("O", 35, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 37, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 37, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 37, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 39, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 39, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 39, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 41, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 41, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 41, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 43, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 43, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 43, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 45, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("P", 45, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            index2++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationBEMinutesSummary : " + ex.Message);
                log.Error("TariffAutomationBEMinutesSummary : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationFRMinutesCountrywise
        static void TariffAutomationFRMinutesCountrywise(string docName)
        {
            Console.WriteLine("TariffAutomationFRMinutesCountrywise()");
            log.Info("TariffAutomationFRMinutesCountrywise()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationFRMinutesCountrywise();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "FR").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct();
                        uint rowIndex = 3;
                        foreach (var item in distinctDates)
                        {
                            if (rowIndex == 3)
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                rowIndex++;
                            }
                            else
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                            }
                        }

                        var distinctDays = (from ld in records select new { id = ld.Days }).ToList().Distinct();
                        rowIndex = 3;
                        foreach (var item in distinctDays)
                        {
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 1, item.id, CellValues.String, wsPart);
                            rowIndex++;
                            InsertValue("B", rowIndex + 7, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            rowIndex++;
                            InsertValue("B", rowIndex + 14, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 15, item.id, CellValues.String, wsPart);
                        }

                        var distinctCountrys = (from ld in records select new { id = ld.Country }).ToList().Distinct();
                        rowIndex = 1;
                        int countryCount = 0;
                        string columnName1 = "";
                        string columnName2 = "";
                        foreach (var item in distinctCountrys)
                        {
                            switch (countryCount)
                            {
                                case 0:
                                    columnName1 = "C";
                                    columnName2 = "D";
                                    break;
                                case 1:
                                    columnName1 = "E";
                                    columnName2 = "F";
                                    break;
                                case 2:
                                    columnName1 = "G";
                                    columnName2 = "H";
                                    break;
                                case 3:
                                    columnName1 = "I";
                                    columnName2 = "J";
                                    break;
                                case 4:
                                    columnName1 = "K";
                                    columnName2 = "L";
                                    break;
                                case 5:
                                    columnName1 = "M";
                                    columnName2 = "N";
                                    break;
                                case 6:
                                    columnName1 = "O";
                                    columnName2 = "P";
                                    break;
                                case 7:
                                    columnName1 = "Q";
                                    columnName2 = "R";
                                    break;
                                case 8:
                                    columnName1 = "S";
                                    columnName2 = "T";
                                    countryCount = -1;
                                    break;
                                default:
                                    break;
                            }
                            InsertValue(columnName1, rowIndex, item.id, CellValues.String, wsPart);



                            for (int i = 0; i < distinctDates.Count(); i++)
                            {
                                var item2 = records.Where(r => r.Country == item.id && r.Calldate == distinctDates.ElementAt(i).id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    if (i == 0)
                                    {
                                        InsertValue(columnName1, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                    else
                                    {
                                        InsertValue(columnName1, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                }
                            }


                            if (countryCount == -1)
                                rowIndex += 8;
                            countryCount++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationFRMinutesCountrywise : " + ex.Message);
                log.Error("TariffAutomationFRMinutesCountrywise : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationFRMinutesSummary
        static void TariffAutomationFRMinutesSummary(string docName)
        {
            Console.WriteLine("TariffAutomationFRMinutesSummary()");
            log.Info("TariffAutomationFRMinutesSummary()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationFRMinutesSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "FR").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct(); //.OrderBy(x => x.id);

                        int index2 = 0;
                        foreach (var itemDates in distinctDates)
                        {
                            if (index2 == 0)
                            {
                                InsertValue("C", 25, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 27, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 27, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 27, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 35, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("D", 35, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 1)
                            {
                                InsertValue("G", 25, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 27, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 27, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 27, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 35, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("H", 35, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 2)
                            {
                                InsertValue("K", 25, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 27, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 27, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 27, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 35, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("L", 35, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 3)
                            {
                                InsertValue("O", 25, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 27, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 27, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 27, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 35, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("P", 35, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            index2++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationFRMinutesSummary : " + ex.Message);
                log.Error("TariffAutomationFRMinutesSummary : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationDMATMinutesCountrywise
        static void TariffAutomationDMATMinutesCountrywise(string docName)
        {
            Console.WriteLine("TariffAutomationDMATMinutesCountrywise()");
            log.Info("TariffAutomationDMATMinutesCountrywise()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationDMATMinutesCountrywise();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "DMATMinuteSummary").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct();
                        uint rowIndex = 5;
                        foreach (var item in distinctDates)
                        {
                            if (rowIndex == 5)
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                rowIndex++;
                            }
                            else
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                            }
                        }

                        var distinctDays = (from ld in records select new { id = ld.Days }).ToList().Distinct();
                        rowIndex = 5;
                        foreach (var item in distinctDays)
                        {
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            rowIndex++;
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                        }

                        var distinctCountrys = (from ld in records select new { id = ld.Country }).ToList().Distinct();
                        rowIndex = 3;
                        int countryCount = 0;
                        string columnName1 = "";
                        string columnName2 = "";
                        foreach (var item in distinctCountrys)
                        {
                            switch (countryCount)
                            {
                                case 0:
                                    columnName1 = "C";
                                    columnName2 = "D";
                                    break;
                                case 1:
                                    columnName1 = "E";
                                    columnName2 = "F";
                                    break;
                                case 2:
                                    columnName1 = "G";
                                    columnName2 = "H";
                                    break;
                                case 3:
                                    columnName1 = "I";
                                    columnName2 = "J";
                                    break;
                                case 4:
                                    columnName1 = "K";
                                    columnName2 = "L";
                                    break;
                                case 5:
                                    columnName1 = "M";
                                    columnName2 = "N";
                                    break;
                                case 6:
                                    columnName1 = "O";
                                    columnName2 = "P";
                                    break;
                                case 7:
                                    columnName1 = "Q";
                                    columnName2 = "R";
                                    countryCount = -1;
                                    break;
                                default:
                                    break;
                            }
                            InsertValue(columnName1, rowIndex, item.id, CellValues.String, wsPart);

                            for (int i = 0; i < distinctDates.Count(); i++)
                            {
                                var item2 = records.Where(r => r.Country == item.id && r.Calldate == distinctDates.ElementAt(i).id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    if (i == 0)
                                    {
                                        InsertValue(columnName1, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                    else
                                    {
                                        InsertValue(columnName1, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Mins)), CellValues.Number, wsPart);
                                    }
                                }
                            }


                            if (countryCount == -1)
                                rowIndex += 8;
                            countryCount++;
                            ////TODO : Need to remove
                            //if (rowIndex >= 22)
                            //    break;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationDMATMinutesCountrywise : " + ex.Message);
                log.Error("TariffAutomationDMATMinutesCountrywise : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationDMATMinutesSummary
        static void TariffAutomationDMATMinutesSummary(string docName)
        {
            Console.WriteLine("TariffAutomationDMATMinutesSummary()");
            log.Info("TariffAutomationDMATMinutesSummary()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationDMATMinutesSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "DMATMinuteSummary").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct(); //.OrderBy(x => x.id);

                        int index2 = 0;
                        foreach (var itemDates in distinctDates)
                        {
                            if (index2 == 0)
                            {
                                InsertValue("C", 27, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 35, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 35, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 35, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 37, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("D", 37, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 1)
                            {
                                InsertValue("G", 27, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 35, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 35, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 35, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 37, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("H", 37, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 2)
                            {
                                InsertValue("K", 27, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 35, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 35, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 35, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 37, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("L", 37, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 3)
                            {
                                InsertValue("O", 27, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "INTERNATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "ONNET MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "TOTAL OFFNET MT").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL MO").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 35, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 35, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 35, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 37, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("P", 37, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            index2++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationDMATMinutesSummary : " + ex.Message);
                log.Error("TariffAutomationDMATMinutesSummary : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationCallingCardMinutesCountrywise
        static void TariffAutomationCallingCardMinutesCountrywise(string docName)
        {
            Console.WriteLine("TariffAutomationCallingCardMinutesCountrywise()");
            log.Info("TariffAutomationCallingCardMinutesCountrywise()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationCallingCardMinutesCountrywise();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Sheet1").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct();
                        uint rowIndex = 5;
                        foreach (var item in distinctDates)
                        {
                            if (rowIndex == 5)
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                rowIndex++;
                            }
                            else
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                            }
                        }

                        var distinctDays = (from ld in records select new { id = ld.Days }).ToList().Distinct();
                        rowIndex = 5;
                        foreach (var item in distinctDays)
                        {
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            rowIndex++;
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                        }

                        var distinctCountrys = (from ld in records select new { id = ld.country }).ToList().Distinct();
                        rowIndex = 3;
                        int countryCount = 0;
                        string columnName1 = "";
                        string columnName2 = "";
                        foreach (var item in distinctCountrys)
                        {
                            switch (countryCount)
                            {
                                case 0:
                                    columnName1 = "C";
                                    columnName2 = "D";
                                    break;
                                case 1:
                                    columnName1 = "E";
                                    columnName2 = "F";
                                    break;
                                case 2:
                                    columnName1 = "G";
                                    columnName2 = "H";
                                    break;
                                case 3:
                                    columnName1 = "I";
                                    columnName2 = "J";
                                    break;
                                case 4:
                                    columnName1 = "K";
                                    columnName2 = "L";
                                    break;
                                case 5:
                                    columnName1 = "M";
                                    columnName2 = "N";
                                    break;
                                case 6:
                                    columnName1 = "O";
                                    columnName2 = "P";
                                    break;
                                case 7:
                                    columnName1 = "Q";
                                    columnName2 = "R";
                                    countryCount = -1;
                                    break;
                                default:
                                    break;
                            }
                            InsertValue(columnName1, rowIndex, item.id, CellValues.String, wsPart);
                            for (int i = 0; i < distinctDates.Count(); i++)
                            {
                                var item2 = records.Where(r => r.country == item.id && r.calldate == distinctDates.ElementAt(i).id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    if (i == 0)
                                    {
                                        InsertValue(columnName1, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                    }
                                    else
                                    {
                                        InsertValue(columnName1, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue(columnName2, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                    }
                                }
                            }


                            if (countryCount == -1)
                                rowIndex += 8;
                            countryCount++;
                            ////TODO : Need to remove
                            //if (rowIndex >= 22)
                            //    break;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationCallingCardMinutesCountrywise : " + ex.Message);
                log.Error("TariffAutomationCallingCardMinutesCountrywise : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationcallingcardMinutesSummary
        static void TariffAutomationcallingcardMinutesSummary(string docName)
        {
            Console.WriteLine("TariffAutomationcallingcardMinutesSummary()");
            log.Info("TariffAutomationcallingcardMinutesSummary()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationcallingcardMinutesSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Sheet1").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct(); //.OrderBy(x => x.id);

                        int index2 = 0;
                        foreach (var itemDates in distinctDates)
                        {
                            if (index2 == 0)
                            {
                                InsertValue("C", 27, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "LOCAL ACCESS").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "FREE ACCESS").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("D", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("E", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("C", 35, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("D", 35, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 1)
                            {
                                InsertValue("G", 27, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "LOCAL ACCESS").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "FREE ACCESS").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("H", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("I", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("G", 35, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("H", 35, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 2)
                            {
                                InsertValue("K", 27, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "LOCAL ACCESS").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "FREE ACCESS").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("L", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("M", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("K", 35, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("L", 35, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            else if (index2 == 3)
                            {
                                InsertValue("O", 27, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                var item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "LOCAL ACCESS").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 29, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 29, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 29, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "FREE ACCESS").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 31, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 31, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 31, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }
                                item2 = records.Where(r => r.Calldate == itemDates.id && r.CallType.ToUpper().Trim() == "NATIONAL").Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 33, item2.ElementAt(0).TotalMins, CellValues.Number, wsPart);
                                    InsertValue("P", 33, item2.ElementAt(0).TotalCli, CellValues.Number, wsPart);
                                    InsertValue("Q", 33, item2.ElementAt(0).Mins_Percentage, CellValues.Number, wsPart);
                                }

                                item2 = records.Where(r => r.Calldate == itemDates.id).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    InsertValue("O", 35, item2.ElementAt(0).Total_No_Minutes, CellValues.Number, wsPart);
                                    InsertValue("P", 35, item2.ElementAt(0).TotalUniqueCLI, CellValues.Number, wsPart);
                                }
                            }
                            index2++;
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationcallingcardMinutesSummary : " + ex.Message);
                log.Error("TariffAutomationcallingcardMinutesSummary : " + ex.Message);
            }
        }
        #endregion

        #region InsertRow
        //InsertRow(iRowIndex, wsPart);
        static void InsertRow(uint rowIndex, WorksheetPart wrksheetPart)
        {
            Worksheet worksheet = wrksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row = null;
            if (sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).Count() != 0)
            {
                Row refRow = sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).First();
                //Copy row from refRow and insert it
                row = CopyToLine(refRow, rowIndex, sheetData);
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }
        }

        //Copy an existing row and insert it
        //We don't need to copy styles of a refRow because a CloneNode() or Clone() methods do it for us
        static Row CopyToLine(Row refRow, uint rowIndex, SheetData sheetData)
        {
            uint newRowIndex;
            var newRow = (Row)refRow.CloneNode(true);
            // Loop through all the rows in the worksheet with higher row 
            // index values than the one you just added. For each one,
            // increment the existing row index.
            IEnumerable<Row> rows = sheetData.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);
            foreach (Row row in rows)
            {
                newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value + 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }
                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }

            sheetData.InsertBefore(newRow, refRow);
            return newRow;
        }
        #endregion

        #region SendEmail
        static void SendEmail(string docName, string subject)
        {
            try
            {
                //Mail Sending 
                string mailContent = string.Empty;
                string mailSubject = subject;
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(';').ToList();
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item));
                }
                log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mail Sending Error : " + ex.Message);
                log.Debug("Mail Sending Error : " + ex.Message);
            }
        }
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, object value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(Convert.ToString(value));
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region GetCell
        private static Cell GetCell(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
            return null;
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
