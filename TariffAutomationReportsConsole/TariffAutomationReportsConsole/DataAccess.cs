﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace TariffAutomationUKMinutesCountrywiseConsole
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region TariffAutomationUKMinutesCountrywise
        public static List<TariffAutomationUKMinutesCountrywise> TariffAutomationUKMinutesCountrywise()
        {
            List<TariffAutomationUKMinutesCountrywise> OutputList = new List<TariffAutomationUKMinutesCountrywise>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_UK"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_UK_Minutes_Countrywise";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                               
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationUKMinutesCountrywise()
                        {
                            Calldate = r.Calldate,
                            Days = r.Days,
                            Country = r.Country,
                            Cli = r.Cli,
                            Mins = r.Mins
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationUKMinutesCountrywise() : " + ex.Message);
                log.Error("TariffAutomationUKMinutesCountrywise() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationUKMinutesSummary
        public static List<TariffAutomationUKMinutesSummary> TariffAutomationUKMinutesSummary()
        {
            List<TariffAutomationUKMinutesSummary> OutputList = new List<TariffAutomationUKMinutesSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_UK"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_UK_Minutes_Summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationUKMinutesSummary()
                        {
                            CallType = r.CallType,
                            Calldate = r.Calldate,
                            TotalMins = r.TotalMins,
                            TotalCli = r.TotalCli,
                            Mins_Percentage = r.Mins_Percentage,
                            Total_No_Minutes = r.Total_No_Minutes,
                            TotalUniqueCLI = r.TotalUniqueCLI
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationUKMinutesSummary() : " + ex.Message);
                log.Error("TariffAutomationUKMinutesSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationATMinutesCountrywise
        public static List<TariffAutomationATMinutesCountrywise> TariffAutomationATMinutesCountrywise()
        {
            List<TariffAutomationATMinutesCountrywise> OutputList = new List<TariffAutomationATMinutesCountrywise>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_AT"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_AT_Minutes_Countrywise";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationATMinutesCountrywise()
                        {
                            Calldate = r.Calldate,
                            Days = r.Days,
                            Country = r.Country,
                            Cli = r.Cli,
                            Mins = r.Mins
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationUKMinutesCountrywise() : " + ex.Message);
                log.Error("TariffAutomationUKMinutesCountrywise() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationATMinutesSummary
        public static List<TariffAutomationATMinutesSummary> TariffAutomationATMinutesSummary()
        {
            List<TariffAutomationATMinutesSummary> OutputList = new List<TariffAutomationATMinutesSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_AT"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_AT_Minutes_Summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationATMinutesSummary()
                        {
                            CallType = r.CallType,
                            Calldate = r.Calldate,
                            TotalMins = r.TotalMins,
                            TotalCli = r.TotalCli,
                            Mins_Percentage = r.Mins_Percentage,
                            Total_No_Minutes = r.Total_No_Minutes,
                            TotalUniqueCLI = r.TotalUniqueCLI
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationUKMinutesSummary() : " + ex.Message);
                log.Error("TariffAutomationUKMinutesSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationBEMinutesCountrywise
        public static List<TariffAutomationBEMinutesCountrywise> TariffAutomationBEMinutesCountrywise()
        {
            List<TariffAutomationBEMinutesCountrywise> OutputList = new List<TariffAutomationBEMinutesCountrywise>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_BE"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_BE_Minutes_Countrywise";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationBEMinutesCountrywise()
                        {
                            Calldate = r.Calldate,
                            Days = r.Days,
                            Country = r.Country,
                            Cli = r.Cli,
                            Mins = r.Mins
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationBEMinutesCountrywise() : " + ex.Message);
                log.Error("TariffAutomationBEMinutesCountrywise() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationBEMinutesSummary
        public static List<TariffAutomationBEMinutesSummary> TariffAutomationBEMinutesSummary()
        {
            List<TariffAutomationBEMinutesSummary> OutputList = new List<TariffAutomationBEMinutesSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_BE"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_BE_Minutes_Summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationBEMinutesSummary()
                        {
                            CallType = r.CallType,
                            Calldate = r.Calldate,
                            TotalMins = r.TotalMins,
                            TotalCli = r.TotalCli,
                            Mins_Percentage = r.Mins_Percentage,
                            Total_No_Minutes = r.Total_No_Minutes,
                            TotalUniqueCLI = r.TotalUniqueCLI
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationBEMinutesSummary() : " + ex.Message);
                log.Error("TariffAutomationBEMinutesSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationFRMinutesCountrywise
        public static List<TariffAutomationFRMinutesCountrywise> TariffAutomationFRMinutesCountrywise()
        {
            List<TariffAutomationFRMinutesCountrywise> OutputList = new List<TariffAutomationFRMinutesCountrywise>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_FR"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_FR_Minutes_Countrywise";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationFRMinutesCountrywise()
                        {
                            Calldate = r.Calldate,
                            Days = r.Days,
                            Country = r.Country,
                            Cli = r.Cli,
                            Mins = r.Mins
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationFRMinutesCountrywise() : " + ex.Message);
                log.Error("TariffAutomationFRMinutesCountrywise() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationFRMinutesSummary
        public static List<TariffAutomationFRMinutesSummary> TariffAutomationFRMinutesSummary()
        {
            List<TariffAutomationFRMinutesSummary> OutputList = new List<TariffAutomationFRMinutesSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_FR"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_FR_Minutes_Summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationFRMinutesSummary()
                        {
                            CallType = r.CallType,
                            Calldate = r.Calldate,
                            TotalMins = r.TotalMins,
                            TotalCli = r.TotalCli,
                            Mins_Percentage = r.Mins_Percentage,
                            Total_No_Minutes = r.Total_No_Minutes,
                            TotalUniqueCLI = r.TotalUniqueCLI
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationFRMinutesSummary() : " + ex.Message);
                log.Error("TariffAutomationFRMinutesSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationDMATMinutesCountrywise
        public static List<TariffAutomationDMATMinutesCountrywise> TariffAutomationDMATMinutesCountrywise()
        {
            List<TariffAutomationDMATMinutesCountrywise> OutputList = new List<TariffAutomationDMATMinutesCountrywise>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_AT"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_AT_delight_Minutes_Countrywise";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationDMATMinutesCountrywise()
                        {
                            Calldate = r.Calldate,
                            Days = r.Days,
                            Country = r.Country,
                            Cli = r.Cli,
                            Mins = r.Mins
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationDMATMinutesCountrywise() : " + ex.Message);
                log.Error("TariffAutomationDMATMinutesCountrywise() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationDMATMinutesSummary
        public static List<TariffAutomationDMATMinutesSummary> TariffAutomationDMATMinutesSummary()
        {
            List<TariffAutomationDMATMinutesSummary> OutputList = new List<TariffAutomationDMATMinutesSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_AT"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_AT_delight_Minutes_Summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationDMATMinutesSummary()
                        {
                            CallType = r.CallType,
                            Calldate = r.Calldate,
                            TotalMins = r.TotalMins,
                            TotalCli = r.TotalCli,
                            Mins_Percentage = r.Mins_Percentage,
                            Total_No_Minutes = r.Total_No_Minutes,
                            TotalUniqueCLI = r.TotalUniqueCLI
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationDMATMinutesSummary() : " + ex.Message);
                log.Error("TariffAutomationDMATMinutesSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationCallingCardMinutesCountrywise
        public static List<TariffAutomationCallingCardMinutesCountrywise> TariffAutomationCallingCardMinutesCountrywise()
        {
            List<TariffAutomationCallingCardMinutesCountrywise> OutputList = new List<TariffAutomationCallingCardMinutesCountrywise>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_DC"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_CallingCard_Minutes_Countrywise";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationCallingCardMinutesCountrywise()
                        {
                            calldate = r.calldate,
                            Days = r.Days,
                            country = r.country,
                            cli = r.cli,
                            talktime = r.talktime
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationCallingCardMinutesCountrywise() : " + ex.Message);
                log.Error("TariffAutomationCallingCardMinutesCountrywise() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationcallingcardMinutesSummary
        public static List<TariffAutomationcallingcardMinutesSummary> TariffAutomationcallingcardMinutesSummary()
        {
            List<TariffAutomationcallingcardMinutesSummary> OutputList = new List<TariffAutomationcallingcardMinutesSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_DC"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_callingcard_Minutes_Summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationcallingcardMinutesSummary()
                        {
                            CallType = r.CallType,
                            Calldate = r.Calldate,
                            TotalMins = r.TotalMins,
                            TotalCli = r.TotalCli,
                            Mins_Percentage = r.Mins_Percentage,
                            Total_No_Minutes = r.Total_No_Minutes,
                            TotalUniqueCLI = r.TotalUniqueCLI
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationcallingcardMinutesSummary() : " + ex.Message);
                log.Error("TariffAutomationcallingcardMinutesSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}
