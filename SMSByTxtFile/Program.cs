using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace SMSByTxtFile
{
	internal class Program
	{
		public Program()
		{
		}

		private static void DoProcess(string fileName)
		{
			int num = 0;
			int num1 = 0;
			int num2 = 0;
			try
			{
				StreamReader streamReader = new StreamReader(fileName);
				int num3 = 0;
				while (true)
				{
					string str = streamReader.ReadLine();
					string str1 = str;
					if (str == null)
					{
						break;
					}
					try
					{
						if (Convert.ToString(ConfigurationManager.AppSettings["run"]) != "1")
						{
							Environment.Exit(0);
						}
						else
						{
							Thread.Sleep(2000);
							num++;
							Console.WriteLine("Sms loop starts");
							Console.WriteLine(str1);
							num1++;
							num2 = num1;
							string item = "";
							if (num2 == 1)
							{
								item = ConfigurationManager.AppSettings["mapurl1"];
							}
							else if (num2 == 2)
							{
								item = ConfigurationManager.AppSettings["mapurl2"];
							}
							else if (num2 == 3)
							{
								item = ConfigurationManager.AppSettings["mapurl3"];
							}
							else if (num2 == 4)
							{
								item = ConfigurationManager.AppSettings["mapurl4"];
							}
							else if (num2 == 5)
							{
								num1 = 0;
								item = ConfigurationManager.AppSettings["mapurl5"];
							}
							Console.WriteLine(item);
							string[] strArrays = str1.Split(new char[] { ',' });
							if ((int)strArrays.Length != 3)
							{
								Console.WriteLine("ERROR : Data does not contain expected records");
								Program.Log("ERROR : Data does not contain expected records");
								Program.Log(str1);
							}
							else
							{
								ASCIIEncoding aSCIIEncoding = new ASCIIEncoding();
								string str2 = string.Format("orig-addr={0}&orig-noa={1}&dest-noa={2}&dest-addr={3}&tp-dcs={4}&tp-udhi={5}&tp-ud={6}&tp-pid={7}&tp-udl={8}", new object[] { "5555", "1", "1", strArrays[0], "246", "1", strArrays[1], "127", strArrays[2] });
								Console.WriteLine(str2);
								HttpWebRequest length = (HttpWebRequest)WebRequest.Create(item);
								length.Method = "POST";
								length.ContentType = "application/x-www-form-urlencoded";
								length.ServicePoint.Expect100Continue = false;
								byte[] bytes = (new ASCIIEncoding()).GetBytes(str2);
								length.ContentLength = (long)((int)bytes.Length);
								Stream requestStream = length.GetRequestStream();
								requestStream.Write(bytes, 0, (int)bytes.Length);
								requestStream.Close();
								Stream responseStream = ((HttpWebResponse)length.GetResponse()).GetResponseStream();
								Encoding encoding = Encoding.GetEncoding("utf-8");
								(new StreamReader(responseStream, encoding)).ReadToEnd().Trim();
							}
							Console.WriteLine("Sms ends");
							Console.WriteLine(num);
							string[] strArrays1 = Regex.Split(fileName, "\\\\");
							fileName = strArrays1[(int)strArrays1.Length - 1];
							Program.Logger(string.Concat("fileName : ", fileName, " : Completed Count : ", num.ToString()));
							if (num3 == 10)
							{
								num3 = 0;
							}
						}
					}
					catch (Exception exception1)
					{
						Exception exception = exception1;
						Console.WriteLine(exception.Message);
						Program.Log(exception.Message);
					}
				}
				streamReader.Close();
				string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
				DateTime now = DateTime.Now;
				File.Move(fileName, string.Concat(Path.Combine(baseDirectory, "Backups", now.ToString("MMddyyyyHHmmss")), ".txt"));
			}
			catch (Exception exception3)
			{
				Exception exception2 = exception3;
				Console.WriteLine(exception2.Message);
				Program.Log(string.Concat("ERROR : ", exception2.Message));
			}
			Console.WriteLine("Completed");
		}

		public static void Log(string message)
		{
			StreamWriter streamWriter = File.AppendText(string.Concat(AppDomain.CurrentDomain.BaseDirectory, "logs.txt"));
			try
			{
				string str = string.Format("{0:G}: {1}.", DateTime.Now, message);
				streamWriter.WriteLine(str);
			}
			finally
			{
				streamWriter.Close();
			}
		}

		public static void Logger(string message)
		{
			StreamWriter streamWriter = File.AppendText(string.Concat(AppDomain.CurrentDomain.BaseDirectory, "\\Logs\\Files.txt"));
			try
			{
				string str = string.Format("{0:G}: {1}.", DateTime.Now, message);
				streamWriter.WriteLine(str);
			}
			finally
			{
				streamWriter.Close();
			}
		}

		private static void Main(string[] args)
		{
			string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory);
			for (int i = 0; i < files.Count<string>(); i++)
			{
				if ((!Path.GetFileName(files[i]).StartsWith("input") ? false : Path.GetExtension(files[i]) == ".txt"))
				{
					Console.WriteLine(Path.GetFileName(files[i]));
					Program.DoProcess(files[i]);
					Console.WriteLine(Path.GetFileName(files[i]));
				}
			}
			Console.ReadLine();
		}
	}
}