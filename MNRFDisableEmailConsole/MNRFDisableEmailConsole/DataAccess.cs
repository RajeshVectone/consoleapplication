﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Newtonsoft.Json;
using NLog;
#endregion

namespace MNRFDisableEmailConsole
{
    public static class DataAccess
    {
        #region Declarations
        static Logger Log = LogManager.GetLogger("Utility");
        #endregion

        #region GetMNRFDisablingEmailExeProcess
        public static List<GetMNRFDisablingEmailExeProcessOutput> GetMNRFDisablingEmailExeProcess(string sitecode)
        {
            Log.Info("GetMNRFDisablingEmailExeProcess : Input : " + sitecode);
            List<GetMNRFDisablingEmailExeProcessOutput> outputList = new List<GetMNRFDisablingEmailExeProcessOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "MNRF_DISABLING_EMAIL_EXE_PROCESS";
                    var result = conn.Query<GetMNRFDisablingEmailExeProcessOutput>(
                            sp, new
                            {
                                @sitecode = sitecode
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                        Log.Info("GetMNRFDisablingEmailExeProcess : Output : " + JsonConvert.SerializeObject(outputList));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetMNRFDisablingEmailExeProcess() : " +  ex.Message);
                Console.WriteLine("GetMNRFDisablingEmailExeProcess() : "  +  ex.Message);
            }
            return outputList;
        }
        #endregion

        #region GetMNRFDisablingEmailInsertRecord
        public static void GetMNRFDisablingEmailInsertRecord(GetMNRFDisablingEmailInsertRecordInput input)
        {
            Log.Info("GetMNRFDisablingEmailInsertRecord : Input : " + JsonConvert.SerializeObject(input));
            List<GetMNRFDisablingEmailExeProcessOutput> outputList = new List<GetMNRFDisablingEmailExeProcessOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "MNRF_DISABLING_EMAIL_INSERT_RECORD";
                    var result = conn.Query<Output>(
                            sp, new
                            {
                                @SITECODE = input.SITECODE,
                                @HH = input.HH,
                                @DATE = input.DATE,
                                @TCOUNT = input.TCOUNT,
                                @SUCC_COUNT = input.SUCC_COUNT,
                                @FAILURE_COUNT = input.FAILURE_COUNT
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("GetMNRFDisablingEmailInsertRecord : Output : " + JsonConvert.SerializeObject(result));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetMNRFDisablingEmailInsertRecord() : " +  ex.Message);
                Console.WriteLine("GetMNRFDisablingEmailInsertRecord() : " +  ex.Message);
            }
        }
        #endregion

    }
}
