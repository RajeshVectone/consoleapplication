﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;
#endregion

namespace MNRFDisableEmailConsole
{
    #region Declarations
    public class GetMNRFDisablingEmailExeProcessOutput
    {
        public string DATE { get; set; }
        public string ICCID { get; set; }
        public string MOBILENO { get; set; }
        public string EMAIL { get; set; }
        public int HH { get; set; }
    }

    public class GetMNRFDisablingEmailInsertRecordInput
    {
        public string SITECODE { get; set; }
        public int HH { get; set; }
        public string DATE { get; set; }
        public int TCOUNT { get; set; }
        public int SUCC_COUNT { get; set; }
        public int FAILURE_COUNT { get; set; }
    }

    public class Output
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
    #endregion

    class Program
    {
        #region Declarations
        static Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("MNRFDisableEmailConsole : Process Started");
            Log.Info("Process Started");
            DoProcess();
            Console.WriteLine("MNRFDisableEmailConsole : Process Completed");
            Log.Info("Process Completed");
        }
        #endregion

        #region DoProcess
        private static void DoProcess()
        {
            Console.WriteLine("DoProcess");
            Log.Info("DoProcess");
            try
            {
                string[] arrSiteCode = Utility.GetSiteCodes();
                foreach (var siteCode in arrSiteCode)
                {
                    GetMNRFDisablingEmailInsertRecordInput input = new GetMNRFDisablingEmailInsertRecordInput();
                    input.SITECODE = siteCode;
                    input.HH = Convert.ToInt32(DateTime.Now.ToString("HH"));
                    input.DATE = DateTime.Now.ToString("dd/MM/yyyy");
                    input.TCOUNT = 0;
                    input.SUCC_COUNT = 0;
                    input.FAILURE_COUNT = 0;

                    List<GetMNRFDisablingEmailExeProcessOutput> outputList = DataAccess.GetMNRFDisablingEmailExeProcess(siteCode);

                    if (outputList != null && outputList.Count > 0)
                    {
                        Log.Info("DoProcess : TCOUNT : " + outputList.Count);
                        input.TCOUNT = outputList.Count;
                        input.HH = outputList[0].HH;
                        input.DATE = outputList[0].DATE;
                        foreach (var output in outputList)
                        {
                            Log.Info("DoProcess : output : " + JsonConvert.SerializeObject(output));
                            try
                            {
                                var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                                var message = new MandrillMessage();
                                message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM_" + siteCode];
                                message.AddTo(output.EMAIL, "Vectone Customer");
                                //TODO : For Testing
                                //message.AddTo("a.dhakshinamoorthy@vectone.com", "DM");
                                var result = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPTEMPLATENAME"]).Result;
                                if (result != null && result.Count > 0 && result.ElementAt(0).Status == 0)
                                    input.SUCC_COUNT++;
                                else
                                    input.FAILURE_COUNT++;
                                Log.Info("Response : " + JsonConvert.SerializeObject(result));
                            }
                            catch (Exception ex)
                            {
                                input.FAILURE_COUNT++;
                                Log.Error("DoProcess : MailChimp Exception :" + ex.Message);
                            }
                        }
                    }
                    DataAccess.GetMNRFDisablingEmailInsertRecord(input);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log.Error("DoProcess() : " + ex.Message);
                try
                {
                    MailAddressCollection mailTo = new MailAddressCollection();
                    mailTo.Add(new MailAddress(ConfigurationManager.AppSettings["MailTo"]));
                    MailAddressCollection mailCc = new MailAddressCollection();
                    mailCc.Add(new MailAddress(ConfigurationManager.AppSettings["MailCc"]));
                    Utility.SendMail(false, new MailAddress("noreply@vectonemobile.co.uk"), mailTo, mailCc, null, ConfigurationManager.AppSettings["MAILSUBJECT"], ex.Message);
                }
                catch { }
            }
        }
        #endregion
    }
}
