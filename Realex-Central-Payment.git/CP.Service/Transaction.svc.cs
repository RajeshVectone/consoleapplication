﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Xml;
using CP.BusinessLogic.CyberSource;
using CP.BusinessLogic.DB;
using CP.BusinessLogic.RealexPayments.RealAuth;
using CP.BusinessLogic.Transactionm;
using CP.DataType;
using IVRPaymentService.Interface;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "IVRPaymentService")]

namespace IVRPaymentService
{
    // TODO: Modify the service behavior settings (instancing, concurrency etc) based on the service's requirements. Use ConcurrencyMode.Multiple if your service implementation is thread-safe.
    // TODO: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Transaction : ITransaction
    {
        /// <summary>
        /// Subscription create.
        /// </summary>
        //public Stream Subscription_Create(string siteCode, string productCode, string referenceId, string accountId, string ccNo, string expDate)
        public Stream Subscription_Create(Stream parameters)
        {
            Log.Write("Create Subscription");
            try
            {
                NameValueCollection query = new NameValueCollection();
                //foreach (string key in HttpContext.Current.Request.Params.AllKeys) query.Add(key, HttpContext.Current.Request.Params[key].Trim());
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split("=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower().Trim(), param[1].Trim());
                }

                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                List<string> missingParam = new List<string>();
                if (string.IsNullOrEmpty(query["sitecode"])) missingParam.Add("sitecode");
                if (string.IsNullOrEmpty(query["productcode"])) missingParam.Add("productcode");
                if (string.IsNullOrEmpty(query["paymentagent"])) missingParam.Add("paymentagent");
                if (string.IsNullOrEmpty(query["servicetype"])) missingParam.Add("servicetype");
                if (string.IsNullOrEmpty(query["paymentmode"])) missingParam.Add("paymentmode");
                if (string.IsNullOrEmpty(query["accountid"])) missingParam.Add("accountid");
                if (string.IsNullOrEmpty(query["ccno"])) missingParam.Add("ccno");
                if (string.IsNullOrEmpty(query["cctype"])) missingParam.Add("cctype");
                else
                    if (query["cctype"].ToLower() != "maestro" && string.IsNullOrEmpty(query["cvv"])) missingParam.Add("cvv");
                if (string.IsNullOrEmpty(query["expdate"]) || query["expdate"].Length != 6) missingParam.Add("expdate");
                if (string.IsNullOrEmpty(query["firstname"])) missingParam.Add("firstname");
                if (string.IsNullOrEmpty(query["lastname"])) missingParam.Add("lastname");
                if (string.IsNullOrEmpty(query["country"])) missingParam.Add("country");
                if (string.IsNullOrEmpty(query["email"])) missingParam.Add("email");
                if (string.IsNullOrEmpty(query["amount"])) missingParam.Add("amount");
                if (string.IsNullOrEmpty(query["currency"])) missingParam.Add("currency");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion

                string IpPaymentAgent = HttpContext.Current.Request.UserHostAddress;

                //TransactionReply result = new TransactionReply();
                RealExPayment soap = new RealExPayment();
                TransactionReply resultSub = new TransactionReply();

                RoutingPayment routing = new RoutingPayment();
                if (!routing.LoadRoutingData(query["sitecode"], query["productcode"]))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", query["sitecode"], query["productcode"]));


                CyberSource.Clients.SoapWebReference.Card card = new CyberSource.Clients.SoapWebReference.Card();
                card.accountNumber = query["ccno"];
                card.cardType = query["cctype"];
                card.cvNumber = query["cvv"];
                card.cvIndicator = "1";
                card.expirationYear = query["expdate"].Substring(0, 4);
                card.expirationMonth = query["expdate"].Substring(4, 2);

                // Billing info

                CyberSource.Clients.SoapWebReference.BillTo billTo = new CyberSource.Clients.SoapWebReference.BillTo();
                billTo.firstName = query["firstname"];
                billTo.lastName = query["lastname"];
                billTo.street1 = query["streetname"];
                billTo.city = query["city"];
                billTo.postalCode = query["postcode"];
                billTo.state = string.IsNullOrEmpty(query["state"]) ? string.Empty : query["state"];
                billTo.country = query["country"];
                billTo.email = string.IsNullOrEmpty(query["email"]) ? "unknown@mundio.com" : query["email"];
                billTo.ipAddress = string.IsNullOrEmpty(query["ipaddr"]) ? HttpContext.Current.Request.UserHostAddress : query["ipaddr"];

                CyberSource.Clients.SoapWebReference.PurchaseTotals payment = new CyberSource.Clients.SoapWebReference.PurchaseTotals();
                payment.grandTotalAmount = Convert.ToDouble(query["amount"]).ToString("#.00");
                payment.currency = query["currency"];

                int retError = 0;
                csTransaction transaction = new csTransaction();
                string ReferenceId = transaction.InsertPaymentData(
                    query["sitecode"],
                    query["appcode"],
                    query["productcode"],
                    query["paymentagent"],
                    query["servicetype"],
                    query["paymentmode"],
                    1,
                    0,
                    query["accountid"],
                    query["ccno"].Substring(query["ccno"].Length - 6, 6),
                    Convert.ToDouble(query["amount"]),
                    query["currency"],
                    string.Empty,
                    routing.MerchantID,
                    routing.ProviderCode,
                    "100",
                    0,
                    ref retError,
                    query["email"],
                    IpPaymentAgent,
                    query["clientip"],
                    "Payment Reference Created",
                    "0",
                    query["expdate"],
                    query["cctype"],
                    "",
                    "",
                    "",
                    "",
                    0)
                .ToString();

                resultSub = soap.Subscription_Capture(
                    ReferenceId,
                    query["accountid"],
                    card,
                    billTo,
                    payment,
                    routing.MerchantID,
                    query["appcode"],
                    query["country"]);
                if (resultSub != null && resultSub.ReplyMessage != null && resultSub.ReplyMessage.paySubscriptionRetrieveReply != null && !String.IsNullOrEmpty(resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID))
                {
                    Log.Write("reasonCode : " + resultSub.ReplyMessage.reasonCode);
                    Log.Write("subscriptionID : " + resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID);
                    Subscriptions subscription = new Subscriptions(query["accountid"], query["ccno"].Substring(query["ccno"].Length - 6, 6), routing.ProviderCode);
                    subscription.CreateSubscription(
                        query["accountid"],
                        query["ccno"].Substring(query["ccno"].Length - 6, 6),
                        resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID,
                        query["expdate"],
                        DateTime.Now.ToString("yyyyMMdd"),
                        routing.ProviderCode);
                    //result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                }

                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (resultSub != null && resultSub.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        resultSub.ReplyMessage.merchantReferenceCode,
                        resultSub.ReplyMessage.reasonCode,
                        resultSub.ReplyMessage.decision);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", resultSub.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", resultSub.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", resultSub.ReplyMessage.reasonCode).AppendLine();

                    // Build subscription data
                    if (resultSub.ReplyMessage.paySubscriptionCreateReply != null)
                        sbData
                            .AppendFormat("SubscriptionID={0}", resultSub.ReplyMessage.paySubscriptionCreateReply.subscriptionID);
                }
                else if (resultSub != null)
                    result = string.Format("-1:-1:{0}", resultSub.Description);

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                Log.Write("CreateSubscription - Exception :" + ex.ToString());
                // Operation failed
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
        }

        /// <summary>
        /// Subscription authorization.
        /// </summary>
        public Stream Subscription_Authorize(
            string siteCode, string applicationCode, string productCode, string paymentAgent, string serviceType, string paymentMode,
            string accountId,
            string ccNo, string ccCvv, string amount, string currency,
            string capture, string email, string ipclient)
        //public Stream Subscription_Authorize(Stream parameters)
        {
            try
            {
                Log.Write("Subscription_Authorize()");

                Log.Write("UserHostAddress : " + HttpContext.Current.Request.UserHostAddress);

                #region Get parameters & values

                NameValueCollection query = new NameValueCollection();
                string[] allKeys = HttpContext.Current.Request.Params.AllKeys;
                for (int iCount = 0; iCount < (int)allKeys.Length; iCount++)
                {
                    string key = allKeys[iCount];
                    query.Add(key, HttpContext.Current.Request.Params[key].Trim());
                }
                #endregion

                try
                {
                    //Log.Write("Input : " + String.Join(",", query.Cast<string>().Select(e => query[e])));
                }
                catch { }

                if (query["service-type"] == "RES")
                {
                    applicationCode = "CTP" + query["currency"];
                    productCode = "IVR" + Convert.ToDecimal(query["amount"]).ToString("000.#");
                }
                else
                {
                    applicationCode = String.IsNullOrEmpty(query["application-code"]) ? query["site-code"] : query["application-code"];
                    productCode = "IVR" + Convert.ToDecimal(query["amount"]).ToString("000.#");// query["product-code"];
                }
                // Run transaction
                TransactionReply trxReply = CP.BusinessLogic.Transaction.Subscription.Authorize(
                    query["site-code"],
                    applicationCode,
                    productCode,
                    query["payment-agent"],
                    query["service-type"],
                    query["payment-mode"],
                    query["account-id"],
                    query["cc-no"] ?? string.Empty,
                    query["cc-cvv"] ?? string.Empty,
                    query["amount"],
                    query["currency"],
                    query["capture"],
                    query["email"],
                    query["ipclient"],
                    query["country"]
                    );

                #region Generate the operation report in plain text format
                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        trxReply.ReplyMessage.merchantReferenceCode,
                        trxReply.GeneralErrorCode,
                        trxReply.Description);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", trxReply.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", trxReply.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", trxReply.ReplyMessage.reasonCode).AppendLine()
                        .AppendFormat("RequestID={0}", trxReply.ReplyMessage.requestID).AppendLine()
                        .AppendFormat("RequestToken={0}", trxReply.ReplyMessage.requestToken).AppendLine();
                    Log.Write("result : " + result);
                    Log.Write("sbData : " + sbData.ToString());
                }
                else if (trxReply != null)
                {
                    result = string.Format("-1:{0}:{1}", trxReply.GeneralErrorCode == 203 ? -1 : trxReply.GeneralErrorCode, trxReply.Description);
                    Log.Write("result : " + result);
                }
                #endregion

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                if (HttpContext.Current.Request.RequestType.Equals("GET"))
                    return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
                else
                    return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                Log.Write("Error Message : " + ex.Message);
                Log.Write("Error StackTrace : " + ex.StackTrace);
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
        }

        /// <summary>
        /// Enrollment authorization.
        /// </summary>
        public Stream Enrollment_Authorize(Stream parameters)
        {
            #region DB insert
            //string UID = "";
            //string sitecode = "";
            //string accountid = "";
            //string referenceid = "";
            //string ccno = "";
            //string decision = "";
            #endregion
            try
            {
                Log.Write("Enrollment_Authorize()");

                #region Get parameters & values
                NameValueCollection query = new NameValueCollection();
                NameValueCollection logQuery = new NameValueCollection();
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split(":=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower().Trim(), param[1].Trim());
                    if (param.Length == 2 && param[0].ToLower().Trim() != "cc-no" && param[0].ToLower().Trim() != "cc-cvv") logQuery.Add(param[0].ToLower().Trim(), param[1].Trim());
                }
                #endregion

                try
                {
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    Log.Write("Input : " + String.Join(",", logQuery.Cast<string>().Select(e => logQuery[e])));
                }
                catch { }

                #region DB insert
                //try
                //{
                //    UID = query["UID"];
                //    sitecode = query["site-code"];
                //    accountid = query["account-id"];
                //    ccno = query["cc-no"].Substring(query["cc-no"].Length - 6);
                //    if (!String.IsNullOrEmpty(query["UID"]))
                //        csTransaction.INSERTPAYMENTLOGDETAILS(sitecode, accountid, UID, "", "4", "Enrollment Authorize", "P STEP 1 ENTRY", ccno, System.Web.HttpContext.Current.Request.UserHostAddress);
                //}
                //catch { }
                #endregion

                Log.Write("Enrollment_Authorize() : " + query["account-id"]);

                // Run transaction
                TransactionReply trxReply = Enrollment.Authorize(
                    query["site-code"],
                    query["application-code"],
                    query["product-code"],
                    query["payment-agent"],
                    query["service-type"],
                    query["payment-mode"],
                    query["account-id"],
                    query["cc-no"],
                    query["cc-type"],
                    query["cc-cvv"],
                    query["exp-date"],
                    query["first-name"],
                    query["last-name"],
                    query["street-name"],
                    query["city"],
                    query["post-code"],
                    query["state"],
                    query["country"],
                    query["email"],
                    query["amount"],
                    query["currency"],
                    query["check-enroll"] ?? "0",
                    query["capture"] ?? "0",
                    query["client-ip"],
                    query["device_type"],
                        query["browser"],
                        query["os_version"],
                        query["topup_url"],
                        Convert.ToInt32(query["bundle_id"]));

                #region DB insert
                //if (trxReply.ReplyMessage != null)
                //{
                //    referenceid = trxReply.ReplyMessage.merchantReferenceCode;
                //    decision = trxReply.ReplyMessage.decision;
                //}
                #endregion

                #region Generate the operation report in plain text format
                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        trxReply.ReplyMessage.merchantReferenceCode,
                        trxReply.GeneralErrorCode,
                        trxReply.Description);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", trxReply.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", trxReply.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", trxReply.ReplyMessage.reasonCode).AppendLine()
                        .AppendFormat("SubscriptionID={0}", (trxReply.ReplyMessage.paySubscriptionCreateReply != null && trxReply.ReplyMessage.paySubscriptionCreateReply.subscriptionID != null) ? trxReply.ReplyMessage.paySubscriptionCreateReply.subscriptionID : "").AppendLine();

                    // Build enrollment data
                    if (trxReply.ReplyMessage.payerAuthEnrollReply != null)
                        sbData
                            .AppendFormat("AcsURL={0}", trxReply.ReplyMessage.payerAuthEnrollReply.acsURL).AppendLine()
                            .AppendFormat("PaReq={0}", trxReply.ReplyMessage.payerAuthEnrollReply.paReq).AppendLine()
                            .AppendFormat("Xid={0}", trxReply.ReplyMessage.payerAuthEnrollReply.xid).AppendLine();

                    //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                    sbData.AppendFormat("ThreeDStatus={0}", trxReply.ThreeDStatus).AppendLine();

                }
                else if (trxReply != null)
                {
                    //result = string.Format("-1:-1:{0}", trxReply.Description);
                    result = string.Format("{0}:{1}", !String.IsNullOrEmpty(trxReply.Description) && trxReply.Description == "Chargeback failure - card not allowed for transaction" ? "-111" : "-1", trxReply.Description);
                }
                #endregion

                Log.Write("result : " + result.Trim() + Environment.NewLine + sbData.ToString().Trim());

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                Log.Write("Exception : " + (ex.InnerException ?? ex).Message);
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message == "Chargeback failure - card not allowed for transaction" ? "-111" : "-1", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
            finally
            {
                #region DB insert
                //try
                //{
                //    if (!String.IsNullOrEmpty(UID))
                //        csTransaction.INSERTPAYMENTLOGDETAILS(sitecode, accountid, UID, referenceid, "5", decision, "P STEP 1 EXIT", ccno, System.Web.HttpContext.Current.Request.UserHostAddress);
                //}
                //catch { }
                #endregion
            }
        }

        /// <summary>
        /// Enrollment Validate authentication.
        /// </summary>
        public Stream Enrollment_Validate(Stream parameters)
        {
            #region DB insert
            //string UID = "";
            //string sitecode = "";
            //string accountid = "";
            //string referenceid = "";
            //string ccno = "";
            //string decision = "";
            #endregion
            try
            {
                Log.Write("Enrollment_Validate()");
                #region Get parameters & values
                NameValueCollection query = new NameValueCollection();
                NameValueCollection logQuery = new NameValueCollection();
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split(":=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower().Trim(), param[1].Trim()); if (param.Length == 2 && param[0].ToLower().Trim() != "cc-no" && param[0].ToLower().Trim() != "cc-cvv") logQuery.Add(param[0].ToLower().Trim(), param[1].Trim());
                }
                #endregion

                try
                {
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    Log.Write("Input : " + String.Join(",", logQuery.Cast<string>().Select(e => logQuery[e])));
                    //Log.Write("CC_NO : " + Convert.ToString(query["cc-cvv"]).Substring(0, 4) + "-XXXX-" + Convert.ToString(query["cc-cvv"]).Substring(query["cc-cvv"].Length - 6));
                }
                catch { }

                #region DB insert
                //try
                //{
                //    UID = query["UID"];
                //    sitecode = query["site-code"];
                //    accountid = query["account-id"];
                //    referenceid = query["reference-id"];
                //    ccno = query["cc-no"].Substring(query["cc-no"].Length - 6);
                //    if (!String.IsNullOrEmpty(query["UID"]))
                //        csTransaction.INSERTPAYMENTLOGDETAILS(sitecode, accountid, UID, referenceid, "12", "Enrollment Validate", "P STEP 2 ENTRY", ccno, System.Web.HttpContext.Current.Request.UserHostAddress);
                //}
                //catch { }
                #endregion

                Log.Write("Enrollment_Validate() : " + query["account-id"] + " - " + query["reference-id"]);


                // Run transaction
                TransactionReply trxReply = Enrollment.Validate(
                    query["site-code"],
                    query["product-code"],
                    query["reference-id"],
                    query["account-id"],
                    query["cc-no"],
                    query["cc-cvv"],
                    query["cc-type"],
                    query["exp-date"],
                    query["first-name"],
                    query["last-name"],
                    query["street-name"],
                    query["city"],
                    query["post-code"],
                    query["state"],
                    query["country"],
                    query["email"],
                    query["pares"],
                    query["capture"] ?? "0",
                    query["client-ip"],
                    query["application-code"]);

                #region DB insert
                //if (trxReply.ReplyMessage != null)
                //{
                //    decision = trxReply.ReplyMessage.decision;
                //}
                #endregion

                #region Generate the operation report in plain text format
                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        trxReply.ReplyMessage.merchantReferenceCode,
                        trxReply.GeneralErrorCode,
                        trxReply.Description);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", trxReply.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", trxReply.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", trxReply.ReplyMessage.reasonCode).AppendLine()
                        .AppendFormat("RequestID={0}", trxReply.ResultPASRef).AppendLine()
                        .AppendFormat("RequestToken={0}", trxReply.ResultAuthCode).AppendLine();

                    //07-Jun-2017 : Moorthy : Modified for Refund
                    if (trxReply.ReplyMessage.reasonCode == "100" && trxReply.ReplyMessage.decision.ToUpper() == "ACCEPT" && Convert.ToString(query["amount"]) == "0.03")
                    {
                        Log.Write("amount " + Convert.ToString(query["amount"]));
                        Log.Write("currency " + Convert.ToString(query["currency"]));

                        RefundAmount("0.02", Convert.ToString(query["currency"]), Convert.ToString(query["cc-no"]), Convert.ToString(query["exp-date"]), Convert.ToString(query["first-name"]), Convert.ToString(query["cc-type"]));
                    }

                    // Build validation data
                    if (trxReply.ReplyMessage.payerAuthValidateReply != null)
                        sbData
                            .AppendFormat("AuthenticationResult={0}", trxReply.ReplyMessage.payerAuthValidateReply.authenticationResult).AppendLine()
                            .AppendFormat("AuthenticationStatusMessage={0}", trxReply.ReplyMessage.payerAuthValidateReply.authenticationStatusMessage).AppendLine()
                            .AppendFormat("Cavv={0}", trxReply.ReplyMessage.payerAuthValidateReply.cavv).AppendLine()
                            .AppendFormat("CommerceIndicator={0}", trxReply.ReplyMessage.payerAuthValidateReply.commerceIndicator).AppendLine()
                            .AppendFormat("Eci={0}", trxReply.ReplyMessage.payerAuthValidateReply.eci).AppendLine()
                            .AppendFormat("EciRaw={0}", trxReply.ReplyMessage.payerAuthValidateReply.eciRaw).AppendLine()
                            .AppendFormat("UcafAuthenticationData={0}", trxReply.ReplyMessage.payerAuthValidateReply.ucafAuthenticationData).AppendLine()
                            .AppendFormat("UcafCollectionIndicator={0}", trxReply.ReplyMessage.payerAuthValidateReply.ucafCollectionIndicator).AppendLine()
                            .AppendFormat("Xid={0}", trxReply.ReplyMessage.payerAuthValidateReply.xid).AppendLine();

                    // Build subscription data
                    if (trxReply.ReplyMessage.paySubscriptionCreateReply != null)
                        sbData
                            .AppendFormat("SubscriptionId={0}", trxReply.ReplyMessage.paySubscriptionCreateReply.subscriptionID).AppendLine();

                    //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                    sbData.AppendFormat("ThreeDStatus={0}", trxReply.ThreeDStatus).AppendLine();

                    //24-Aug-2019 : Moorthy added for storing SRD value
                    sbData.AppendFormat("SRD={0}", trxReply.SRD).AppendLine();
                }
                else if (trxReply != null)
                    result = string.Format("-1:-1:{0}", trxReply.Description);
                #endregion

                Log.Write("result : " + result.Trim() + Environment.NewLine + sbData.ToString().Trim());

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                Log.Write("Exception : " + (ex.InnerException ?? ex).Message);
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
            finally
            {
                #region DB insert
                //                try
                //                {
                //                    if (!String.IsNullOrEmpty(UID))
                //                        csTransaction.INSERTPAYMENTLOGDETAILS(sitecode, accountid, UID, referenceid, "13", decision, "P STEP 2 EXIT", ccno
                //, System.Web.HttpContext.Current.Request.UserHostAddress);
                //                }
                //                catch { }
                #endregion
            }
        }

        //Temp used for Refund
        private void RefundAmount(string refundAmount, string currency, string ccNo, string expDate, string chName, string ccType)
        {
            try
            {
                Log.Write("RefundAmount()");

                expDate = expDate.Substring(4, 2) + expDate.Substring(2, 2);

                string timeStamp = generateTimestamp();
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(refundAmount) * 100));
                string orderId = Guid.NewGuid().ToString("N").ToUpper();
                string refundhash = generateRefundHash();
                string sha1hash = generateSHA1Hash(timeStamp, orderId, amount, currency, ccNo);

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("type", "credit");
                    xml.WriteAttributeString("timestamp", timeStamp);
                    xml.WriteElementString("merchantid", ConfigurationManager.AppSettings["vectonerefundmerchantid"]);
                    xml.WriteElementString("account", ConfigurationManager.AppSettings["vectonerefundsubaccount"]);
                    xml.WriteElementString("orderid", orderId);
                    xml.WriteStartElement("amount");
                    xml.WriteAttributeString("currency", currency);
                    xml.WriteString(amount.ToString());
                    xml.WriteEndElement();
                    xml.WriteStartElement("card");
                    xml.WriteElementString("number", ccNo);
                    xml.WriteElementString("expdate", expDate);
                    xml.WriteElementString("chname", chName);
                    xml.WriteElementString("type", ccType);
                    xml.WriteEndElement();
                    xml.WriteStartElement("comment");
                    xml.WriteAttributeString("id", "1");
                    xml.WriteString("Refund Test By Vectone");
                    xml.WriteEndElement();
                    xml.WriteElementString("refundhash", refundhash);
                    xml.WriteElementString("sha1hash", sha1hash);
                }
                xml.WriteEndElement();
                xml.Flush();
                xml.Close();

                string requestXML = strBuilder.ToString();

                //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                //      Log.Write2("Refund Request XML : " + requestXML);

                HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["vectonerefundurl"]);
                wReq.ContentType = "text/xml";
                wReq.UserAgent = "Realex Refund";
                wReq.Timeout = 45 * 1000;	// milliseconds
                wReq.AllowAutoRedirect = false;
                wReq.ContentLength = requestXML.Length;
                wReq.Method = "POST";

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                sReq.Write(requestXML);
                sReq.Flush();
                sReq.Close();

                HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                String responseXML = sResp.ReadToEnd();
                sResp.Close();

                Log.Write("Refund Response XML : " + responseXML);
            }
            catch (Exception ex)
            {
                Log.Write("Refund : " + ex.Message);
            }
        }

        //Used to Refund the Amount
        public Stream Enrollment_Refund(Stream parameters)
        {
            try
            {
                Log.Write("Enrollment_Refund");
                NameValueCollection query = new NameValueCollection();
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split(":=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower().Trim(), param[1].Trim());
                }

                List<string> missingParam = new List<string>();

                if (string.IsNullOrEmpty(query["reference-id"])) missingParam.Add("reference-id");
                if (string.IsNullOrEmpty(query["cc-no"])) missingParam.Add("cc-no");
                if (string.IsNullOrEmpty(query["exp-date"]) || query["exp-date"].Length != 4) missingParam.Add("exp-date");
                if (string.IsNullOrEmpty(query["ch-name"])) missingParam.Add("ch-name");
                if (string.IsNullOrEmpty(query["cc-type"])) missingParam.Add("cc-type");
                if (string.IsNullOrEmpty(query["amount"])) missingParam.Add("amount");
                if (string.IsNullOrEmpty(query["currency"])) missingParam.Add("currency");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }

                string timeStamp = generateTimestamp();
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(query["amount"]) * 100));
                string orderId = Guid.NewGuid().ToString("N").ToUpper();
                string refundhash = generateRefundHash();
                string sha1hash = generateSHA1Hash(timeStamp, orderId, amount, Convert.ToString(query["currency"]), Convert.ToString(query["cc-no"]));

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("type", "credit");
                    xml.WriteAttributeString("timestamp", timeStamp);
                    xml.WriteElementString("merchantid", ConfigurationManager.AppSettings["vectonerefundmerchantid"]);
                    xml.WriteElementString("account", ConfigurationManager.AppSettings["vectonerefundsubaccount"]);
                    xml.WriteElementString("orderid", orderId);
                    xml.WriteStartElement("amount");
                    xml.WriteAttributeString("currency", Convert.ToString(query["currency"]));
                    xml.WriteString(amount.ToString());
                    xml.WriteEndElement();
                    xml.WriteStartElement("card");
                    xml.WriteElementString("number", query["cc-no"]);
                    xml.WriteElementString("expdate", query["exp-date"]);
                    xml.WriteElementString("chname", query["ch-name"]);
                    xml.WriteElementString("type", query["cc-type"]);
                    xml.WriteEndElement();
                    xml.WriteStartElement("comment");
                    xml.WriteAttributeString("id", "1");
                    xml.WriteString("Refund Test By Vectone");
                    xml.WriteEndElement();
                    xml.WriteElementString("refundhash", refundhash);
                    xml.WriteElementString("sha1hash", sha1hash);
                }
                xml.WriteEndElement();
                xml.Flush();
                xml.Close();

                string requestXML = strBuilder.ToString();

                //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                //      Log.Write("Request XML : " + requestXML);
                HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://remote.sandbox.elavonpaymentgateway.com/remote");
                wReq.ContentType = "text/xml";
                wReq.UserAgent = "Realex Refund";
                wReq.Timeout = 45 * 1000;	// milliseconds
                wReq.AllowAutoRedirect = false;
                wReq.ContentLength = requestXML.Length;
                wReq.Method = "POST";

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                sReq.Write(requestXML);
                sReq.Flush();
                sReq.Close();

                HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                String responseXML = sResp.ReadToEnd();
                sResp.Close();

                Log.Write("Response XML : " + responseXML);

                RefundResponse response = new RefundResponse(responseXML);

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write("Success"); }, Encoding.Default);

            }
            catch (Exception ex)
            {
                // Operation failed
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
        }

        private string generateSHA1Hash(string timeStamp, string orderId, Int64 amount, string currency, string cardNumber)
        {
            System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1Managed();

            String hashInput =
                timeStamp + "." +
                System.Configuration.ConfigurationManager.AppSettings["vectonerefundmerchantid"] + "." +
                orderId + "." +
                amount + "." +
                currency + "." +
               cardNumber;

            String hashStage1 =
                hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashInput))) + "." +
                System.Configuration.ConfigurationManager.AppSettings["vectonerefundsharedsecret"];

            String hashStage2 =
                hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashStage1)));

            return hashStage2;
        }

        private string generateRefundHash()
        {
            System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1Managed();

            String hashInput = System.Configuration.ConfigurationManager.AppSettings["vectonerefundpassword"];

            String hashStage1 =
                hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashInput)));

            Log.Write("generateRefundHash() : " + hashStage1);

            return hashStage1;
        }

        private String hexEncode(byte[] data)
        {

            String result = "";
            foreach (byte b in data)
            {
                result += b.ToString("X2");
            }
            result = result.ToLower();

            return (result);
        }

        private string generateTimestamp()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss");
        }

        #region Unused Methods
        /// <summary>
        /// Enrollment capture.
        /// </summary>
        public Stream Enrollment_Capture(Stream parameters)
        {
            try
            {
                #region Get parameters & values
                NameValueCollection query = new NameValueCollection();
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split(":=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower().Trim(), param[1].Trim());
                }
                #endregion

                // Run transaction
                TransactionReply trxReply = Enrollment.Capture(
                    query["site-code"],
                    query["product-code"],
                    query["reference-id"],
                    query["account-id"]);

                #region Generate the operation report in plain text format
                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        trxReply.ReplyMessage.merchantReferenceCode,
                        trxReply.GeneralErrorCode,
                        trxReply.Description);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", trxReply.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", trxReply.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", trxReply.ReplyMessage.reasonCode).AppendLine()
                        .AppendFormat("RequestID={0}", trxReply.ReplyMessage.requestID).AppendLine()
                        .AppendFormat("RequestToken={0}", trxReply.ReplyMessage.requestToken).AppendLine();
                }
                else if (trxReply != null)
                    result = string.Format("-1:-1:{0}", trxReply.Description);
                #endregion

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                // Operation failed
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
        }

        /// <summary>
        /// Enrollment Validate authentication.
        /// </summary>
        public Stream Enrollment_Authorize_Reverse(Stream parameters)
        {
            try
            {
                #region Get parameters & values
                NameValueCollection query = new NameValueCollection();
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split(":=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower(), param[1]);
                }
                #endregion

                // Run transaction
                TransactionReply trxReply = Enrollment.Authorize_Reverse(
                    query["site-code"],
                    query["product-code"],
                    query["reference-id"],
                    query["account-id"]);

                #region Generate the operation report in plain text format
                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        trxReply.ReplyMessage.merchantReferenceCode,
                        trxReply.GeneralErrorCode,
                        trxReply.Description);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", trxReply.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", trxReply.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", trxReply.ReplyMessage.reasonCode).AppendLine()
                        .AppendFormat("RequestID={0}", trxReply.ReplyMessage.requestID).AppendLine()
                        .AppendFormat("RequestToken={0}", trxReply.ReplyMessage.requestToken).AppendLine();

                    // Build validation data
                    if (trxReply.ReplyMessage.ccAuthReversalReply != null)
                        sbData
                            .AppendFormat("Amount={0}", trxReply.ReplyMessage.ccAuthReversalReply.amount).AppendLine()
                            .AppendFormat("AuthorizationCode={0}", trxReply.ReplyMessage.ccAuthReversalReply.authorizationCode).AppendLine()
                            .AppendFormat("ForwardCode={0}", trxReply.ReplyMessage.ccAuthReversalReply.forwardCode).AppendLine()
                            .AppendFormat("ProcessorResponse={0}", trxReply.ReplyMessage.ccAuthReversalReply.processorResponse).AppendLine()
                            .AppendFormat("RequestDateTime={0}", trxReply.ReplyMessage.ccAuthReversalReply.requestDateTime).AppendLine();
                }
                else if (trxReply != null)
                    result = string.Format("-1:-1:{0}", trxReply.Description);
                #endregion

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                // Operation failed
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
        }

        public Stream Enrollment_Review(Stream parameters)
        {
            try
            {
                #region Get parameters & values
                NameValueCollection query = new NameValueCollection();
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split(":=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower().Trim(), param[1].Trim());
                }
                #endregion

                // Run transaction
                TransactionReply trxReply = Enrollment.ReviewCapture(
                    query["site-code"],
                    query["product-code"],
                    query["reference-id"],
                    query["account-id"]);

                #region Generate the operation report in plain text format
                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        trxReply.ReplyMessage.merchantReferenceCode,
                        trxReply.GeneralErrorCode,
                        trxReply.Description);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", trxReply.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", trxReply.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", trxReply.ReplyMessage.reasonCode).AppendLine()
                        .AppendFormat("RequestID={0}", trxReply.ReplyMessage.requestID).AppendLine()
                        .AppendFormat("RequestToken={0}", trxReply.ReplyMessage.requestToken).AppendLine();
                }
                else if (trxReply != null)
                    result = string.Format("-1:-1:{0}", trxReply.Description);
                #endregion

                // Return the operation repor
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                // Operation failed
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
        }

        public Stream Enrollment_ReceiptIn(Stream parameters)
        {
            try
            {
                Log.Write("Enrollment_ReceiptIn()");
                #region Get parameters & values
                NameValueCollection query = new NameValueCollection();
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split(":=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower().Trim(), param[1].Trim());
                }
                #endregion

                //try
                //{
                //    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                //    //    Log.Write2("Input : " + String.Join(",", query.Cast<string>().Select(e => query[e])));
                //}
                //catch { }

                Log.Write("Enrollment_ReceiptIn() : " + query["account-Id"] + " - " + query["subscription_id"]);

                // Run transaction
                TransactionReply trxReply = Enrollment.ReceiptIn(
                    query["site-code"],
                    query["application-code"],
                    query["product-code"],
                    query["payment-agent"],
                    query["service-type"],
                    query["payment-mode"],
                    query["amount"],
                    query["currency"],
                    query["client-ip"],
                    query["country"],
                    query["account-Id"],
                    query["reference-code"],
                    query["subscription_id"],
                    // Newly added on 
                    query["email"],
                    query["actualmobileno"]
                    //13-Jul-2018 : Added for cvn
                    , query["cvn"]
                    );

                #region Generate the operation report in plain text format
                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        trxReply.ReplyMessage.merchantReferenceCode,
                        trxReply.GeneralErrorCode,
                        String.IsNullOrEmpty(trxReply.Description) ? trxReply.ReplyMessage.decision : trxReply.Description);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", trxReply.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", trxReply.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", trxReply.ReplyMessage.reasonCode).AppendLine()
                        .AppendFormat("ResultAuthCode={0}", trxReply.ResultAuthCode).AppendLine()
                        .AppendFormat("ResultPASRef={0}", trxReply.ResultPASRef).AppendLine()
                        //24-Aug-2019 : Moorthy added for storing SRD value
                        .AppendFormat("SRD={0}", trxReply.SRD).AppendLine();
                }
                else if (trxReply != null)
                    result = string.Format("-1:-1:{0}", trxReply.Description);
                #endregion

                Log.Write("result : " + result.Trim() + Environment.NewLine + sbData.ToString().Trim());

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                // Operation failed
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }


        }

        /// <summary>
        /// Enrollment Validate authentication.
        /// </summary>
        public Stream Enrollment_Void(Stream parameters)
        {
            try
            {
                #region Get parameters & values
                NameValueCollection query = new NameValueCollection();
                StreamReader paramReader = new StreamReader(parameters);
                while (!paramReader.EndOfStream)
                {
                    string[] param = paramReader.ReadLine().Split(":=".ToCharArray(), 2);
                    if (param.Length == 2) query.Add(param[0].ToLower(), param[1]);
                }
                #endregion

                // Run transaction
                TransactionReply trxReply = Enrollment.Void(
                    query["site-code"],
                    query["product-code"],
                    query["reference-id"],
                    query["account-id"]);

                #region Generate the operation report in plain text format
                string result = "-1:-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}",
                        trxReply.ReplyMessage.merchantReferenceCode,
                        trxReply.GeneralErrorCode,
                        trxReply.Description);

                    // Build transaction data
                    sbData
                        .AppendFormat("Decision={0}", trxReply.ReplyMessage.decision).AppendLine()
                        .AppendFormat("MerchantReferenceCode={0}", trxReply.ReplyMessage.merchantReferenceCode).AppendLine()
                        .AppendFormat("ReasonCode={0}", trxReply.ReplyMessage.reasonCode).AppendLine()
                        .AppendFormat("RequestID={0}", trxReply.ReplyMessage.requestID).AppendLine()
                        .AppendFormat("RequestToken={0}", trxReply.ReplyMessage.requestToken).AppendLine();

                    // Build validation data
                    if (trxReply.ReplyMessage.ccAuthReversalReply != null)
                        sbData
                            .AppendFormat("Amount={0}", trxReply.ReplyMessage.ccAuthReversalReply.amount).AppendLine()
                            .AppendFormat("AuthorizationCode={0}", trxReply.ReplyMessage.ccAuthReversalReply.authorizationCode).AppendLine()
                            .AppendFormat("ForwardCode={0}", trxReply.ReplyMessage.ccAuthReversalReply.forwardCode).AppendLine()
                            .AppendFormat("ProcessorResponse={0}", trxReply.ReplyMessage.ccAuthReversalReply.processorResponse).AppendLine()
                            .AppendFormat("RequestDateTime={0}", trxReply.ReplyMessage.ccAuthReversalReply.requestDateTime).AppendLine();
                }
                else if (trxReply != null)
                    result = string.Format("-1:-1:{0}", trxReply.Description);
                #endregion

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim() + Environment.NewLine + sbData.ToString().Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                // Operation failed
                string result = string.Format("-1:-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
        }

        /// <summary>
        /// Test API for API Metrics.
        /// </summary>
        public Stream Enrollment_Test()
        {
            string result = string.Format("0:0:{0}", DateTime.Now.ToString() + " : Success");
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
            return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
        }

        #endregion
    }
}
