03/07/2017 04:19:24 => Enrollment_Authorize()
03/07/2017 04:19:28 => Authorize()
03/07/2017 04:20:02 => ThreeDSubmitTransaction()
03/07/2017 04:20:08 => 3DS Request:<?xml version="1.0" encoding="utf-16"?>
<request timestamp="20170703162002" type="3ds-verifyenrolled">
  <merchantid>vectone</merchantid>
  <account>DK</account>
  <orderid>VMDK-WEB500-RX-141704</orderid>
  <amount currency="DKK">50000</amount>
  <card>
    <number>5566772788795135</number>
    <expdate>0318</expdate>
    <type>MC</type>
    <chname>Salam</chname>
    <cvn>
      <number>763</number>
      <presind>1</presind>
    </cvn>
  </card>
  <autosettle flag="0" />
  <sha1hash>c9b98ebaaeeda49c59a70ff6c383749000a2b81e</sha1hash>
  <comments />
  <tssinfo>
    <address type="billing">
      <code />
      <country />
    </address>
    <address type="shipping">
      <code />
      <country />
    </address>
    <custnum />
    <varref />
    <prodid />
  </tssinfo>
</request>
03/07/2017 04:29:49 => 3DS Response:<?xml version="1.0" encoding="UTF-8"?>

<response timestamp="20170703115015">
  <merchantid>vectone</merchantid>
  <account>dk</account>
  <orderid>VMDK-WEB500-RX-141704</orderid>
  <authcode></authcode>
  <result>00</result>
  <message>Enrolled</message>
  <pasref></pasref>
  <timetaken>1</timetaken>
  <authtimetaken>1</authtimetaken>
  <pareq>eJxVUdtOwkAQffcrSN/t7vYOGZagxIBGgwrG17IdaRPawna5+fXOQkF9mzOXMzPnwOBQrjo71E1RV31HuNzpYKXqrKiWfWc+e7hNnIG8gVmuEUfvqLYaJTxj06RL7BQZjSyibpZihBiGgYc8TWKOC9//ysJMBVHkSJgO33Ajod0iaYnrAbtAYtMqTysjIVWbu8mLDEUSeNTRQihRT0bSE5x3E+EFEbBzBqq0RLlDZeoKgZ0QqHpbGX2UiUd9FwBbvZK5MeseY/v93m1nXFWXwGwN2O8V062NGuI6FJl8NR/LfPU4Z09NmK/q73hq5rUa46eu+8BsB2SpQelxEfOY+x0heiHviRDYKQ9paY+QIecu5/TUGcLabhlea7b0NwWktCYnjkSc0CMXBHhY0+XUQQJdY2C/R9+PrZDKkEBRV5ADJJof8SiOAyvpqWBZCtJFJNw/0VgAzI6y1i3WmkzRP/N/AONKtvs=</pareq>
  <url>https://secure.barclaycard.co.uk/barclays/tdsecure/pa.jsp?partner=business.mc&amp;VAA=B</url>
  <enrolled>Y</enrolled>
  <xid>QtVghlJU/Ks5hloz7PtUocHeXro=</xid>
  <sha1hash>e59af87eda33e3c4c4861a3fcd717f35046d7986</sha1hash>
</response>

03/07/2017 04:30:22 => Enrollment_Authorize()
03/07/2017 04:30:23 => Authorize()
03/07/2017 04:31:18 => ThreeDSubmitTransaction()
03/07/2017 04:31:22 => 3DS Request:<?xml version="1.0" encoding="utf-16"?>
<request timestamp="20170703163118" type="3ds-verifyenrolled">
  <merchantid>vectone</merchantid>
  <account>DK</account>
  <orderid>VMDK-PP990-RX-141706</orderid>
  <amount currency="DKK">50000</amount>
  <card>
    <number>5566772788795135</number>
    <expdate>0318</expdate>
    <type>MC</type>
    <chname>Salam</chname>
    <cvn>
      <number>763</number>
      <presind>1</presind>
    </cvn>
  </card>
  <autosettle flag="0" />
  <sha1hash>30f5c4819baec4807a0fb62be2eb91f8353e1227</sha1hash>
  <comments />
  <tssinfo>
    <address type="billing">
      <code />
      <country />
    </address>
    <address type="shipping">
      <code />
      <country />
    </address>
    <custnum />
    <varref />
    <prodid />
  </tssinfo>
</request>
03/07/2017 04:31:25 => 3DS Response:<?xml version="1.0" encoding="UTF-8"?>

<response timestamp="20170703120125">
  <merchantid>vectone</merchantid>
  <account>dk</account>
  <orderid>VMDK-PP990-RX-141706</orderid>
  <authcode></authcode>
  <result>00</result>
  <message>Enrolled</message>
  <pasref></pasref>
  <timetaken>1</timetaken>
  <authtimetaken>1</authtimetaken>
  <pareq>eJxVUctSwkAQvPsVqdzNPpNNqGEpBCnQ0qIUD3oLywopSQIhvP7eWQigt+l59Mx0Q+eQL72drTZZWbR9FlDfs4UpZ1kxb/sfk8F97Hf0HUwWlbX9d2u2ldXwYjebdG69bNb2I6O4DKfTiEdTmdAoZlIkiglqvkWScuVrGHff7FpDs0XjkoADuUBkq8wiLWoNqVk/jF51yGLJsaOBkNtq1NecUZrEjMsIyDkDRZpbvbOmLgsL5ITAlNuiro465th3AbCtlnpR16sWIfv9PmhmAlPmQFwNyO2K8dZFG+Q6ZDMt+4zIZPjY+8q6z8kneerFk3mpJoMf2gbiOmCW1lZzyhRVVHiMtyhr8RDIKQ9p7o7QIaUBpfjUGcLKbelea670NwWodIVOHJE4xkcuCOxhhZdjBwp0jYHcju4NnZCmRoGiRCkl0RAluJBCOElPBceSoS4spuJE4wAQN0oat0hjMkb/zP8FAk+zRA==</pareq>
  <url>https://secure.barclaycard.co.uk/barclays/tdsecure/pa.jsp?partner=business.mc&amp;VAA=B</url>
  <enrolled>Y</enrolled>
  <xid>4D1/49HECZiAK9Y/JC8Tgo7TFk0=</xid>
  <sha1hash>dd649de2106f4b3f268fc9520211143b1e10fc49</sha1hash>
</response>

03/07/2017 04:31:25 => NewCardSubmit()
03/07/2017 04:31:26 => SubmitCardNewTransaction()
03/07/2017 04:31:26 => requestXML : <?xml version="1.0" encoding="utf-16"?>
<request timestamp="20170703163126" type="card-new">
  <merchantid>vectone</merchantid>
  <account>DK</account>
  <orderid>VMDK-PP990-RX-141706</orderid>
  <amount currency="DKK">50000</amount>
  <card>
    <number>5566772788795135</number>
    <expdate>0318</expdate>
    <type>MC</type>
    <payerref>4581460026</payerref>
    <chname>Salam</chname>
    <ref>Salam795135</ref>
    <cvn>
      <number>763</number>
      <presind>1</presind>
    </cvn>
  </card>
  <autosettle flag="0" />
  <sha1hash>4a29e1081aae2ef10ea1dc290f1a4d1f963e760b</sha1hash>
  <comments />
  <tssinfo>
    <address type="billing">
      <code />
      <country>DK</country>
    </address>
    <address type="shipping">
      <code />
      <country />
    </address>
    <custnum />
    <varref />
    <prodid />
  </tssinfo>
</request>
03/07/2017 04:31:26 => responseXML : <?xml version="1.0" encoding="UTF-8"?>

<response timestamp="20170703120126">
  <merchantid>vectone</merchantid>
  <account>DK</account>
  <orderid>VMDK-PP990-RX-141706</orderid>
  <result>520</result>
  <message>This Card Ref [Salam795135] has already been used [Perhaps you've already set up this card for this Payer?]</message>
</response>

