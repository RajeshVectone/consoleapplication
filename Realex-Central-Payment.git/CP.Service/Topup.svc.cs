﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Web;
using Microsoft.ServiceModel.Web;
using Microsoft.ServiceModel.Web.SpecializedServices;
using CyberSource.Clients.SoapWebReference;
using CP.BusinessLogic;
using CP.DataType;
using CP.BusinessLogic.CyberSource;
using CP.BusinessLogic.Transaction;
using System.Data;
using IVRPaymentService.Interface;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "IVRPaymentService")]

namespace IVRPaymentService
{
    // TODO: Modify the service behavior settings (instancing, concurrency etc) based on the service's requirements. Use ConcurrencyMode.Multiple if your service implementation is thread-safe.
    // TODO: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Topup : ITopup
    {
        /// <summary>
        /// Subscription payment.
        /// </summary>
        public Stream Topup_Validate(
            string mobileNo, string securityCode)
        {
            try
            {
                #region Get parameters & values
                NameValueCollection query = new NameValueCollection();
                foreach (string key in HttpContext.Current.Request.Params.AllKeys) query.Add(key, HttpContext.Current.Request.Params[key].Trim());
                #endregion

                // Execute process
                IVRPaymentService.ESP.Topup trxReply = IVRPaymentService.ESP.Topup.Validate(
                    query["account-id"] ?? mobileNo,
                    query["cc-secretcode"] ?? securityCode);

                #region Generate the operation report in plain text format
                string result = "-1:Unknown Error";
                StringBuilder sbData = new StringBuilder();
                if (trxReply != null)
                {
                    // Build summary
                    result = string.Format("{0}:{1}:{2}:{3}:{4}",
                        string.IsNullOrEmpty(trxReply.PaymentRef) ? "null" : trxReply.PaymentRef,
                        trxReply.ErrorCode,
                        string.IsNullOrEmpty(trxReply.ErrorMessage) ? "null" : trxReply.ErrorMessage,
                        trxReply.ProductID,
                        trxReply.Amount);
                }
                #endregion

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                // Operation failed
                string result = string.Format("-1:{0}", (ex.InnerException ?? ex).Message);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                return new AdapterStream((writer) => { writer.Write(result.Trim()); }, Encoding.Default);
            }
        }
    }
}
