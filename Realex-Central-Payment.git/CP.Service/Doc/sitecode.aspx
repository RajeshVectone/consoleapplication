﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sitecode.aspx.cs" Inherits="CP.Service.Doc.sitecode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Central Payment - SITE CODE</title>
</head>
<body>
    <form id="form1" runat="server">
    <h2>
        SITE CODE</h2>
    <asp:SqlDataSource runat="server" ID="dsData" ConnectionString="<%$ ConnectionStrings:CentralPayment %>"
        ProviderName="System.Data.SqlClient" SelectCommand="select * from ref_sitecode(nolock)">
    </asp:SqlDataSource>
    <asp:DataGrid runat="server" ID="dgData" DataSourceID="dsData">
        <HeaderStyle BackColor="Silver" />
    </asp:DataGrid>
    </form>
</body>
</html>
