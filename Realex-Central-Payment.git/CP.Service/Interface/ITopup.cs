﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using Microsoft.ServiceModel.Web;
using Microsoft.ServiceModel.Web.SpecializedServices;
using System.IO;
using System.Collections.Specialized;

namespace IVRPaymentService.Interface
{
    [ServiceContract]
    public interface ITopup
    {
        [WebHelp(Comment =
            "<b>Validate the customer's security code.</b>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>account-id    </td><td>: Customer's mobile number</td></tr>" +
            "<tr><td>cc-secretcode </td><td>: Security code</td></tr>" +
            "</table>" +
            "<b>Return: text-plain colon separated values</b><br/>" +
            "{error_code}:{error_message}:{mobile_no}:{product_id}:{amount}:{amount_bonus}")]
        [WebInvoke(
            Method = "*",
            UriTemplate = "topup/validate" +
                "?account-id={mobileNo}" +
                "&cc-secretcode={securityCode}")]
        [OperationContract]
        Stream Topup_Validate(string mobileNo, string securityCOde);
    }
}
