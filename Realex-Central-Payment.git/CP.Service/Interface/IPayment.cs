﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using Microsoft.ServiceModel.Web;
using Microsoft.ServiceModel.Web.SpecializedServices;
using System.IO;
using System.Collections.Specialized;

namespace CP.Service.Interface
{
    [ServiceContract]
    public interface IPayment
    {
        [WebHelp(Comment =
            "Request payment service (authorize and capture) from a subscribed credit card.<br/>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>site-code</td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site Code</a></td></tr>" +
            "<tr><td>application-code</td><td>: <a href='../doc/paymentagent.aspx' target='_blank'>Application Code</a></td></tr>" +
            "<tr><td>product-code</td><td>: Product Code</td></tr>" +
            "<tr><td>service-type</td><td>: <a href='../doc/servicetype.aspx' target='_blank'>Service Type</a></td></tr>" +
            "<tr><td>payment-mode</td><td>: <a href='../doc/paymentmode.aspx' target='_blank'>Payment Mode</a></td></tr>" +
            "<tr><td>account-id</td><td>: Customer's Mobile Number</td></tr>" +
            "<tr><td>cvv</td><td>: 3 Digit Credit Card Security Code</td></tr>" +
            "</table>")]
        [WebInvoke(
            Method = "*",
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "subscription/payment" +
                "?site-code={siteCode}" +
                "&application-code={applicationCode}" +
                "&product-code={productCode}" +
                "&service-type={serviceType}" +
                "&payment-mode={paymentMode}" +
                "&account-id={accountId}" +
                "&cvv={cvv}" +
                "&amount={amount}" +
                "&currency={currency}")]
        [OperationContract]
        Stream Subscription_Payment(
            string siteCode, string applicationCode, string productCode, string serviceType, string paymentMode,
            string accountId, string cvv,
            string amount, string currency);
    }
}
