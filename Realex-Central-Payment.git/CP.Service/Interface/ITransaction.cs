﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using Microsoft.ServiceModel.Web;
using Microsoft.ServiceModel.Web.SpecializedServices;
using System.IO;
using System.Collections.Specialized;

namespace IVRPaymentService.Interface
{
    [ServiceContract]
    public interface ITransaction
    {
        [WebHelp(Comment =
            "<b>Create subscription for a credit card.</b>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>site-code    </td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site code</a></td></tr>" +
            "<tr><td>product-code </td><td>: User defined</td></tr>" +
            "<tr><td>reference-id </td><td>: The Merchant Reference ID recorded earlier from the capture or authorize-capture process</td></tr>" +
            "<tr><td>account-id   </td><td>: User defined, usually the customer's mobile number or email</td></tr>" +
            "<tr><td>cc-no        </td><td>: Card number</td></tr>" +
            "<tr><td>exp-date     </td><td>: Card expiry date (format: yyyyMM)</td></tr>" +
            "</table>")]
        [WebInvoke(
            Method = "*",
            UriTemplate = "subscription/create"
                // + "?site-code={siteCode}" +
                //"&product-code={productCode}" +
                //"&reference-id={referenceId}" +
                //"&account-id={accountId}" +
                //"&cc-no={ccNo}" +
                //"&exp-date={expDate}"
                )]
        [OperationContract]
        //Stream Subscription_Create(string siteCode, string productCode, string referenceId, string accountId, string ccNo, string expDate);
        Stream Subscription_Create(Stream parameters);

        [WebHelp(Comment =
            "<b>Request authorization service for a payment from a subscribed credit card.</b>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>site-code        </td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site code</a></td></tr>" +
            "<tr><td>application-code </td><td>: User defined, leave this blank and the merchant reference ID will use the site code</td></tr>" +
            "<tr><td>product-code     </td><td>: User defined</td></tr>" +
            "<tr><td>payment-agent    </td><td>: <a href='../doc/paymentagent.aspx' target='_blank'>Payment Agent</a></td></tr>" +
            "<tr><td>service-type     </td><td>: <a href='../doc/servicetype.aspx' target='_blank'>Service Type</a></td></tr>" +
            "<tr><td>payment-mode     </td><td>: <a href='../doc/paymentmode.aspx' target='_blank'>Payment Mode</a></td></tr>" +
            "<tr><td>account-id       </td><td>: User defined, usually the customer's mobile number or email</td></tr>" +
            "<tr><td>cc-no            </td><td>: Card number</td></tr>" +
            "<tr><td>cc-cvv           </td><td>: Card verification value, known as Card security code</td></tr>" +
            "<tr><td>amount           </td><td>: Paid amount</td></tr>" +
            "<tr><td>currency         </td><td>: Currency</td></tr>" +
            "<tr><td>capture          </td><td>: Settle the transaction, default 0 (1 = TRUE; 0 = FALSE)</td></tr>" +
            "</table>")]
        [WebInvoke(
            Method = "*",
            UriTemplate = "subscription/authorize" +
                "?site-code={siteCode}" +
                "&application-code={applicationCode}" +
                "&product-code={productCode}" +
                "&payment-agent={paymentAgent}" +
                "&service-type={serviceType}" +
                "&payment-mode={paymentMode}" +
                "&account-id={accountId}" +
                "&cc-no={ccNo}" +
                "&cc-cvv={ccCvv}" +
                "&amount={amount}" +
                "&currency={currency}" +
                "&capture={capture}" +
                "&email={email}" +
                "&ipclient={ipclient}")]
        [OperationContract]
        Stream Subscription_Authorize(
            string siteCode, string applicationCode, string productCode, string paymentAgent, string serviceType, string paymentMode,
            string accountId,
            string ccNo, string ccCvv, string amount, string currency,
            string capture, string email, string ipclient);

        //[WebHelp(Comment =
        //    "<b>Request capture service for an authorized payment from a subscribed credit card.</b>" +
        //    "<table border='0' cellspacing='0' cellpadding='0'>" +
        //    "<tr><td>site-code    </td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site code</a></td></tr>" +
        //    "<tr><td>product-code </td><td>: User defined</td></tr>" +
        //    "<tr><td>service-type </td><td>: <a href='../doc/servicetype.aspx' target='_blank'>Service Type</a></td></tr>" +
        //    "<tr><td>account-id   </td><td>: User defined, usually the customer's mobile number or email</td></tr>" +
        //    "<tr><td>reference-id</td><td>: The Merchant Reference ID recorded earlier from the authorization process</td></tr>" +
        //    "</table>")]
        //[WebInvoke(
        //    Method = "*",
        //    UriTemplate = "subscription/capture" +
        //        "?site-code={siteCode}" +
        //        "&product-code={productCode}" +
        //        "&service-type={serviceType}" +
        //        "&account-id={accountId}" +
        //        "&reference-id={referenceId}")]
        //[OperationContract]
        //Stream Subscription_Capture(
        //    string siteCode, string productCode, string serviceType, string accountId, string referenceId);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// NON-SUBSCRIPTION PAYMENT
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        [WebHelp(Comment =
            "<b>Request authorization service for a credit card payment,<br/>can include enrollment checking and directly capture transaction.</b><br/>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>site-code        </td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site code</a></td></tr>" +
            "<tr><td>application-code </td><td>: User defined, leave this blank and the merchant reference ID will use the site code</td></tr>" +
            "<tr><td>product-code     </td><td>: User defined</td></tr>" +
            "<tr><td>payment-agent    </td><td>: <a href='../doc/paymentagent.aspx' target='_blank'>Payment Agent</a></td></tr>" +
            "<tr><td>service-type     </td><td>: <a href='../doc/servicetype.aspx' target='_blank'>Service Type</a></td></tr>" +
            "<tr><td>payment-mode     </td><td>: <a href='../doc/paymentmode.aspx' target='_blank'>Payment Mode</a></td></tr>" +
            "<tr><td>account-id       </td><td>: User defined, usually the customer's mobile number or email</td></tr>" +
            "<tr><td>cc-no            </td><td>: Card number</td></tr>" +
            "<tr><td>cc-cvv           </td><td>: Card verification value, known as Card security code</td></tr>" +
            "<tr><td>exp-date         </td><td>: Card expiry date (format: yyyyMM)</td></tr>" +
            "<tr><td>first-name       </td><td>: First name</td></tr>" +
            "<tr><td>last-name        </td><td>: Last name</td></tr>" +
            "<tr><td>street-name      </td><td>: Street address</td></tr>" +
            "<tr><td>city             </td><td>: Town</td></tr>" +
            "<tr><td>post-code        </td><td>: Postal Code</td></tr>" +
            "<tr><td>state            </td><td>: State</td></tr>" +
            "<tr><td>country          </td><td>: Country code</td></tr>" +
            "<tr><td>email            </td><td>: Email</td></tr>" +
            "<tr><td>amount           </td><td>: Paid amount</td></tr>" +
            "<tr><td>currency         </td><td>: Currency</td></tr>" +
            "<tr><td>check-enroll     </td><td>: Check for Card enrollment, default 0 (1 = TRUE; 0 = FALSE)</td></tr>" +
            "<tr><td>capture          </td><td>: Capture transaction, default 0 (1 = TRUE; 0 = FALSE)</td></tr>" +
            "<tr><td>client-ip        </td><td>: The requester IP address (optional)</td></tr>" +
            "</table>")]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "enrollment/authorize")]
        [OperationContract]
        Stream Enrollment_Authorize(Stream parameters);

        [WebHelp(Comment =
            "<b>Request validation service for an enrolled credit card payment.</b>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>site-code   </td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site code</a></td></tr>" +
            "<tr><td>product-code</td><td>: User defined</td></tr>" +
            "<tr><td>reference-id</td><td>: The Merchant Reference ID recorded earlier from the authorization process</td></tr>" +
            "<tr><td>account-id  </td><td>: User defined, usually the customer's mobile number or email</td></tr>" +
            "<tr><td>cc-no       </td><td>: Card number</td></tr>" +
            "<tr><td>cc-cvv      </td><td>: Card verification value, known as Card security code</td></tr>" +
            "<tr><td>exp-date    </td><td>: Card expiry date (format: yyyyMM)</td></tr>" +
            "<tr><td>first-name  </td><td>: First name</td></tr>" +
            "<tr><td>last-name   </td><td>: Last name</td></tr>" +
            "<tr><td>street-name </td><td>: Street address</td></tr>" +
            "<tr><td>city        </td><td>: Town</td></tr>" +
            "<tr><td>post-code   </td><td>: Postal Code</td></tr>" +
            "<tr><td>state       </td><td>: State</td></tr>" +
            "<tr><td>country     </td><td>: Country code</td></tr>" +
            "<tr><td>email       </td><td>: Email</td></tr>" +
            "<tr><td>pares       </td><td>: PaReq data from previous enrollment proces</td></tr>" +
            "<tr><td>capture     </td><td>: Capture transaction, default 0 (1 = TRUE; 0 = FALSE)</td></tr>" +
            "<tr><td>client-ip   </td><td>: The requester IP address (optional)</td></tr>" +
            "</table>")]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "enrollment/validate")]
        [OperationContract]
        Stream Enrollment_Validate(Stream parameters);

        [WebHelp(Comment =
            "<b>Request capture service for an authorized credit card payment.</b>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>site-code   </td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site code</a></td></tr>" +
            "<tr><td>product-code</td><td>: User defined</td></tr>" +
            "<tr><td>reference-id</td><td>: The Merchant Reference ID recorded earlier from the authorization process</td></tr>" +
            "<tr><td>account-id  </td><td>: User defined, usually the customer's mobile number or email</td></tr>" +
            "</table>")]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "enrollment/capture")]
        [OperationContract]
        Stream Enrollment_Capture(Stream parameters);

        [WebHelp(Comment =
            "<b>Request full authorization reversal service for an authorized credit card payment.</b>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>site-code   </td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site code</a></td></tr>" +
            "<tr><td>product-code</td><td>: User defined</td></tr>" +
            "<tr><td>reference-id</td><td>: The Merchant Reference ID recorded earlier from the authorization process</td></tr>" +
            "<tr><td>account-id  </td><td>: User defined, usually the customer's mobile number or email</td></tr>" +
            "</table>")]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "enrollment/authreverse")]
        [OperationContract]
        Stream Enrollment_Authorize_Reverse(Stream parameters);


        [WebHelp(Comment =
            "<b>Request capture service for review credit card payment.</b>" +
            "<table border='0' cellspacing='0' cellpadding='0'>" +
            "<tr><td>site-code   </td><td>: <a href='../doc/sitecode.aspx' target='_blank'>Site code</a></td></tr>" +
            "<tr><td>product-code</td><td>: User defined</td></tr>" +
            "<tr><td>reference-id</td><td>: The Merchant Reference ID recorded earlier from the authorization process</td></tr>" +
            "<tr><td>account-id  </td><td>: User defined, usually the customer's mobile number or email</td></tr>" +
            "</table>")]
        [WebInvoke(
            Method = "POST",
            UriTemplate = "enrollment/review")]
        [OperationContract]
        Stream Enrollment_Review(Stream parameters);


        [WebInvoke(
            Method = "POST",
            UriTemplate = "enrollment/receiptin")]
        [OperationContract]
        Stream Enrollment_ReceiptIn(Stream parameters);


        [WebInvoke(
           Method = "POST",
           UriTemplate = "enrollment/void")]
        [OperationContract]
        Stream Enrollment_Void(Stream parameters);

        //Used to Refund the Amount
        [WebInvoke(
            Method = "POST",
            UriTemplate = "enrollment/refund")]
        [OperationContract]
        Stream Enrollment_Refund(Stream parameters);

        [WebInvoke(
            Method = "GET",
            UriTemplate = "enrollment/test")]
        [OperationContract]
        Stream Enrollment_Test();
        
    }
}
