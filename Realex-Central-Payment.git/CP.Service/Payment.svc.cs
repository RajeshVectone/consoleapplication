﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Web;
using Microsoft.ServiceModel.Web;
using Microsoft.ServiceModel.Web.SpecializedServices;
using CyberSource.Clients.SoapWebReference;
using CP.BusinessLogic;
using CP.DataType;
using CP.BusinessLogic.CyberSource;
using CP.BusinessLogic.Transaction;
using System.Data;
using CP.Service.Interface;

// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "CP.Service")]

namespace CP.Service
{
    // TODO: Modify the service behavior settings (instancing, concurrency etc) based on the service's requirements. Use ConcurrencyMode.Multiple if your service implementation is thread-safe.
    // TODO: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Payment : IPayment
    {
        /// <summary>
        /// Subscription payment.
        /// </summary>
        public Stream Subscription_Payment(
            string siteCode, string applicationCode, string productCode, string serviceType, string paymentMode,
            string accountId, string cvv,
            string amount, string currency)
        {
            StringWriter result = new StringWriter();
            DataSet ds = new DataSet("CentralPayment");
            DataTable dt = ds.Tables.Add("TransactionReply");
            dt.Columns.Add("MerchantReferenceCode", typeof(string)).DefaultValue = "-1";
            dt.Columns.Add("GeneralErrorCode", typeof(string)).DefaultValue = "-1";
            dt.Columns.Add("Description", typeof(string)).DefaultValue = "Unknown Error";

            try
            {
                NameValueCollection query = new NameValueCollection();
                #region Get parameters & values
                foreach (string key in HttpContext.Current.Request.Params.AllKeys) query.Add(key, HttpContext.Current.Request.Params[key].Trim());
                #endregion

                // Run transaction
                TransactionReply trxReply = CP.BusinessLogic.Transaction.Subscription.Payment(
                    query["site-code"],
                    query["application-code"],
                    query["product-code"],
                    query["service-type"],
                    query["payment-mode"],
                    query["account-id"],
                    query["cvv"],
                    query["amount"],
                    query["currency"]);

                #region Generate the operation report in XML format
                if (trxReply != null && trxReply.ReplyMessage != null)
                {
                    dt.Columns["MerchantReferenceCode"].DefaultValue = trxReply.ReplyMessage.merchantReferenceCode;
                    dt.Columns["GeneralErrorCode"].DefaultValue = trxReply.GeneralErrorCode;
                    dt.Columns["Description"].DefaultValue = trxReply.Description;
                    dt.Columns.Add("Decision", typeof(string)).DefaultValue = trxReply.ReplyMessage.decision;
                    dt.Columns.Add("ReasonCode", typeof(string)).DefaultValue = trxReply.ReplyMessage.reasonCode;
                    dt.Columns.Add("RequestID", typeof(string)).DefaultValue = trxReply.ReplyMessage.requestID;
                    dt.Columns.Add("RequestToken", typeof(string)).DefaultValue = trxReply.ReplyMessage.requestToken;
                }
                else if (trxReply != null)
                {
                    dt.Columns["GeneralErrorCode"].DefaultValue = trxReply.GeneralErrorCode;
                    dt.Columns["Description"].DefaultValue = trxReply.Description;
                }

                // Build summary & generate XML
                dt.Rows.Add(dt.NewRow());
                dt.WriteXml(result, XmlWriteMode.IgnoreSchema, false);
                #endregion

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
                return new AdapterStream((writer) => { writer.Write(result.ToString()); }, Encoding.Default);
            }
            catch (Exception ex)
            {
                // Operation failed
                dt.Columns["GeneralErrorCode"].DefaultValue = "-2";
                dt.Columns["Description"].DefaultValue = (ex.InnerException ?? ex).Message;

                // Build summary & generate XML
                dt.Rows.Add(dt.NewRow());
                dt.WriteXml(result, XmlWriteMode.IgnoreSchema, false);

                // Return the operation report
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
                return new AdapterStream((writer) => { writer.Write(result.ToString()); }, Encoding.Default);
            }
        }
    }
}
