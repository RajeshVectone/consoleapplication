/*
 * Author       : Herdianto Suseno (2010-12-06)
 * Description  : Represents the TopUp operations for residential numbers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using CCPaymentDL;

namespace CCPaymentBL
{
    /// <summary>
    /// Represents the TopUp operations.
    /// </summary>
    public class TopupRes
    {
        private const string SP_GET_MOBILE = "residential_get_auto_topup_accountid_v2";
        private const string SP_CHECK_THRESHOLD = "esp_check_threshold";
        private const string SP_GET_SUBSCRIPTION = "get_residensubscription_info";
        //private const string SP_GET_SUBSCRIPTION = "select top 1 * from tAuxAutoTopup(nolock) where mobileno = '{0}' and status = 1";
        private const string SP_TOPUP = "vwPay_AutoTopUpBalance_v3";
        private const string SP_AUTOTOPUP_STATUS = "vmukCybS_UpdateAutoTopupStatus";
        private const string SP_TOPUPTRIGGER_STATUS = "update autotopup_transaction set status = 1 where accountid = {0} and status = 2";
        private const string SP_UNSUBSCRIBEAUTOTOPUP = "vmukAUT_CancelAutoTopup";
        private const string SP_ROUTING = "payment_srv.payment.dbo.paymentRoutingGetData";
        

        /// <summary>
        /// Initializes a new instance of CCPaymentBL.Topup.
        /// </summary>
        public TopupRes() { }


        /// <summary>
        /// Gets the routing payment that needs topup.
        /// </summary>
        /// <returns></returns>
        public static string GetRouting(string _sitecode, string _productcode)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_ROUTING, new SqlConnection(BusinessFacade.ResidentialConnection));
                sql.Parameters.Add("@sitecode", SqlDbType.VarChar, 5).Value = _sitecode;
                sql.Parameters.Add("@productcode", SqlDbType.VarChar, 20).Value = _productcode;               

                sql.CommandType = CommandType.StoredProcedure;
                return (DataAccess.ExecuteScalar(sql, "merchant_id") ?? "").ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetRouting exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetRouting exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }


        /// <summary>
        /// Gets the mobile number that needs topup.
        /// </summary>
        /// <returns></returns>
        public static string GetAutoTopupResidential()
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_GET_MOBILE, new SqlConnection(BusinessFacade.ResidentialConnection));
                sql.CommandType = CommandType.StoredProcedure;
                return (DataAccess.ExecuteScalar(sql, "accountid") ?? "").ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetAutoTopupResidential exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetAutoTopupResidential exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        /// <summary>
        /// Gets the mobile threshold information.
        /// </summary>
        public static MobileThreshold GetAccountThreshold(string accountId)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_CHECK_THRESHOLD, new SqlConnection(BusinessFacade.ESPResidentialConnection));
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add("@accountid", SqlDbType.VarChar, 16).Value = accountId;

                List<MobileThreshold> result = new List<MobileThreshold>();
                DataAccess.ExecuteQuery<MobileThreshold>(MobileThreshold.GenerateList<MobileThreshold>, sql, ref result);
                return result.Count > 0 ? result[0] : null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetAccountThreshold exception: {0}\n", (ex.InnerException ?? ex).Message));
                Console.WriteLine();
                HistoryLog.WriteLine(string.Format("GetAccountThreshold exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        /// <summary>
        /// Gets the auto topup information from a mobile number.
        /// </summary>
        public static AuxAutoTopup GetTopupInfo(string accountId)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_GET_SUBSCRIPTION, new SqlConnection(BusinessFacade.WebPaymentConnection));
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add("@mobileno", SqlDbType.VarChar, 30).Value = accountId;                

                List<AuxAutoTopup> result = new List<AuxAutoTopup>();
                DataAccess.ExecuteQuery(AuxAutoTopup.GenerateList<AuxAutoTopup>, sql, ref result);
                return result.Count > 0 ? result[0] : null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetTopupInfo exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetTopupInfo exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        /// <summary>
        /// Gets the auto topup information from a mobile number.
        /// </summary>
        public static AuxAutoTopup GetTopupInfoEx(string accountId)
        {
            try
            {
                SqlCommand sql = new SqlCommand(string.Format(SP_GET_SUBSCRIPTION, accountId), new SqlConnection(BusinessFacade.WebPaymentConnection));
                sql.CommandType = CommandType.Text;

                List<AuxAutoTopup> result = new List<AuxAutoTopup>();
                DataAccess.ExecuteQuery(AuxAutoTopup.GenerateList<AuxAutoTopup>, sql, ref result);
                return result.Count > 0 ? result[0] : null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetTopupInfo exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetTopupInfo exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        /// <summary>
        /// Do topup operation.
        /// </summary>
        public static StatusData TopupResidential(int orderId, string mobileNo, string paymentRef, int productId, double amount, string curr)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_TOPUP, new SqlConnection(BusinessFacade.ResidentialConnection));
                sql.CommandType = CommandType.StoredProcedure;

                DataAccess.AddSqlCommandParam(sql, "@orderID", SqlDbType.BigInt, 8, ParameterDirection.Input, orderId);
                DataAccess.AddSqlCommandParam(sql, "@cardno", SqlDbType.VarChar, 32, ParameterDirection.Input, "-");
                DataAccess.AddSqlCommandParam(sql, "@currency", SqlDbType.VarChar, 5, ParameterDirection.Input, curr);
                DataAccess.AddSqlCommandParam(sql, "@errMsg", SqlDbType.VarChar, 1000, ParameterDirection.Input, "-");
                DataAccess.AddSqlCommandParam(sql, "@note", SqlDbType.VarChar, 2000, ParameterDirection.Input, paymentRef);
                DataAccess.AddSqlCommandParam(sql, "@mobileno", SqlDbType.VarChar, 20, ParameterDirection.Input, mobileNo);
                DataAccess.AddSqlCommandParam(sql, "@amount", SqlDbType.Float, 8, ParameterDirection.Input, amount);
                DataAccess.AddSqlCommandParam(sql, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, curr);

                List<TopupResidentialStatus> result = new List<TopupResidentialStatus>();
                DataAccess.ExecuteQuery<TopupResidentialStatus>(TopupResidentialStatus.GenerateList<TopupResidentialStatus>, sql, ref result);
                if (result.Count > 0 && result[0].ErrorCode == 0)
                    return new StatusData(0, result[0].ErrorMessage ?? "Topup succeeded.");
                else
                    return new StatusData(
                        result.Count > 0 ? result[0].ErrorCode : -1,
                        string.Format("Topup failed, {0}.", result.Count > 0 ? result[0].ErrorMessage : "unknown error"));
            }
            catch (Exception ex)
            {
                Console.WriteLine("TopupResidential exception: {0}\n", (ex.InnerException ?? ex).Message);
                HistoryLog.WriteLine(string.Format("TopupResidential exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        /// <summary>
        /// Unsubscribe from auto-topup
        /// </summary>
        public static void UnsubscribeAutoTopup(string mobileno)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_UNSUBSCRIBEAUTOTOPUP, new SqlConnection(BusinessFacade.ESPConnection));
                sql.CommandType = CommandType.StoredProcedure;
                DataAccess.AddSqlCommandParam(sql, "@mobileno", SqlDbType.VarChar, 16, ParameterDirection.Input, mobileno);
                DataAccess.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine("UnsubscribeAutoTopup exception: {0}\n", (ex.InnerException ?? ex).Message);
                HistoryLog.WriteLine(string.Format("UnsubscribeAutoTopup exception: {0}", (ex.InnerException ?? ex).Message));
            }
        }

        /// <summary>
        /// Update the auto-topup status.
        /// </summary>
        public static void UpdateAutoTopupStatus(int referenceCode, string fullReferenceCode, PaymentStatus paymentStatus, StatusData topupStatus)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_AUTOTOPUP_STATUS, new SqlConnection(BusinessFacade.WebPaymentConnection));
                sql.CommandType = CommandType.StoredProcedure;
                DataAccess.AddSqlCommandParam(sql, "@ref_code", SqlDbType.Int, 4, ParameterDirection.Input, referenceCode);
                DataAccess.AddSqlCommandParam(sql, "@decision", SqlDbType.VarChar, 10, ParameterDirection.Input, paymentStatus == null ? "ERROR" : (paymentStatus.Decision ?? "ERROR"));
                DataAccess.AddSqlCommandParam(sql, "@request_id", SqlDbType.VarChar, 30, ParameterDirection.Input, paymentStatus == null ? "" : paymentStatus.RequestID ?? "");
                DataAccess.AddSqlCommandParam(sql, "@authorization_code", SqlDbType.VarChar, 10, ParameterDirection.Input, paymentStatus == null ? "" : paymentStatus.AuthorizationCode ?? "");
                DataAccess.AddSqlCommandParam(sql, "@request_time", SqlDbType.VarChar, 50, ParameterDirection.Input, paymentStatus == null ? "" : paymentStatus.RequestTime ?? "");
                DataAccess.AddSqlCommandParam(sql, "@payment_status", SqlDbType.Int, 4, ParameterDirection.Input, paymentStatus == null ? -1 : paymentStatus.ReasonCode);
                DataAccess.AddSqlCommandParam(sql, "@payment_description", SqlDbType.VarChar, 255, ParameterDirection.Input, paymentStatus == null ? "Canceled" : string.Format("{0}: {1}", fullReferenceCode, paymentStatus.ReasonDescription) ?? "Canceled");
                DataAccess.AddSqlCommandParam(sql, "@topup_status", SqlDbType.Int, 4, ParameterDirection.Input, topupStatus == null ? -1 : topupStatus.ErrorCode);
                DataAccess.AddSqlCommandParam(sql, "@topup_description", SqlDbType.VarChar, 255, ParameterDirection.Input, topupStatus == null ? "Canceled" : topupStatus.ErrorMessage ?? "Canceled");
                DataAccess.AddSqlCommandParam(sql, "@sorderid", SqlDbType.VarChar, 20, ParameterDirection.Input, fullReferenceCode);
                DataAccess.AddSqlCommandParam(sql, "@reconciliation_id", SqlDbType.VarChar, 100, ParameterDirection.Input, paymentStatus == null ? "" : paymentStatus.ReconciliationID ?? "");
                DataAccess.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("UpdateAutoTopupStatus exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("UpdateAutoTopupStatus exception: {0}", (ex.InnerException ?? ex).Message));
            }
        }

        /// <summary>
        /// Update the topup trigger status.
        /// </summary>
        public static void UpdateTopupTriggerStatus(string mobileNo)
        {
            try
            {
                SqlCommand sql = new SqlCommand(string.Format(SP_TOPUPTRIGGER_STATUS, mobileNo), new SqlConnection(BusinessFacade.ResidentialConnection));
                sql.CommandType = CommandType.Text;
                DataAccess.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("UpdateTopupTriggerStatus exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("UpdateTopupTriggerStatus exception: {0}", (ex.InnerException ?? ex).Message));
            }
        }
    }
}
