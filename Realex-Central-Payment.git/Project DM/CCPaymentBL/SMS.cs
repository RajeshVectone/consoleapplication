/*
 * Author       : Herdianto Suseno (2011-11-20)
 * Description  : Represents the send SMS operations.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Web;

namespace CCPaymentBL
{
    /// <summary>
    /// Represents the send SMS operations.
    /// </summary>
    public class SMS
    {
        private const string Uri_SMS = "/sms" +
            "?service-name=port" +
            "&destination-addr={0}" +
            "&originator-addr-type=5" +
            "&originator-addr={1}" +
            "&payload-type=text" +
            "&message={2}";

        public string[] SendSMS(string destination, string originator, string message)
        {
            string[] tpgResponseData = { "-1", "Failed connecting to the TPG Service!" };
            try
            {
                // Generate request
                HttpWebRequest tpg = (HttpWebRequest)WebRequest.Create(string.Format(BusinessFacade.TPGService + Uri_SMS,
                    destination,
                    originator,
                    HttpUtility.UrlPathEncode(message)));
                tpg.Method = "POST";
                tpg.UserAgent = "TPGWeb";
                tpg.ContentLength = 0;

                // Send request and get the response data
                HttpWebResponse tpgResponse = (HttpWebResponse)tpg.GetResponse();
                StreamReader tpgResponseReader = new StreamReader(tpgResponse.GetResponseStream());
                tpgResponseData = tpgResponseReader.ReadToEnd().Trim().Split(":".ToCharArray(), 2);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return tpgResponseData;
        }
    }
}
