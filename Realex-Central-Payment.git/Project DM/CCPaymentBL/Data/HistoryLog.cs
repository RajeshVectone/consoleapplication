using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Configuration;

namespace CCPaymentBL
{
    public static class HistoryLog
    {
        private static readonly object locking = new object();
        private static readonly object error = new object();
        private static readonly object success = new object();
        private static bool UsingLog = bool.Parse(BusinessFacade.UsingLog);

        public static void WriteLine(string message)
        {
            lock(locking)
            {
                if(UsingLog)
                {
                    string fileName = string.Format("{0}-{1}.txt", "Log", DateTime.Now.ToString("dd-MM-yyyy"));
                    string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    string dir = string.Format(@"{0}\Logs\{1}", path, DateTime.Now.ToString("MM-yyyy"));
                    if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                    string file = string.Format(@"{0}\{1}", dir, fileName);
                    StreamWriter sw = new StreamWriter(file, true);
                    string msgFormat = string.Format("{0} => {1}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"), message);
                    sw.WriteLine(msgFormat);
                    sw.Flush();
                    sw.Close();
                }
            }
        }
        public static void WriteError(string message)
        {
            lock (error)
            {
                if (UsingLog)
                {
                    string fileName = string.Format("{0}-{1}.txt", "Error", DateTime.Now.ToString("dd-MM-yyyy"));
                    string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    string dir = string.Format(@"{0}\Logs\{1}", path, DateTime.Now.ToString("MM-yyyy"));
                    if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                    string file = string.Format(@"{0}\{1}", dir, fileName);
                    StreamWriter sw = new StreamWriter(file, true);
                    string msgFormat = string.Format("{0} => {1}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"), message);
                    sw.WriteLine(msgFormat);
                    sw.Flush();
                    sw.Close();
                }
            }
        }
        public static void WriteSuccess(string message)
        {
            lock (success)
            {
                if (UsingLog)
                {
                    string fileName = string.Format("{0}-{1}.txt", "Success", DateTime.Now.ToString("dd-MM-yyyy"));
                    string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    string dir = string.Format(@"{0}\Logs\{1}", path, DateTime.Now.ToString("MM-yyyy"));
                    if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                    string file = string.Format(@"{0}\{1}", dir, fileName);
                    StreamWriter sw = new StreamWriter(file, true);
                    string msgFormat = string.Format("{0} => {1}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"), message);
                    sw.WriteLine(msgFormat);
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        public static void EmailError(string message)
        {
            EmailUs em = new EmailUs(
                        BusinessFacade.SmtpServer,
                        BusinessFacade.EmailTo, "", "",
                        message,
                        BusinessFacade.MailFrom,
                        BusinessFacade.MailUser,
                        BusinessFacade.MailPass,
                        "Auto Top up Failed");
            em.emailHTMLSend();
        }
    }
}
