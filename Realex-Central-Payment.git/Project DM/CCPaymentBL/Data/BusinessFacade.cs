/*
 * Author       : Herdianto Suseno (2010-05-19)
 * Description  : Represents the global application resources.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace CCPaymentBL
{
    public class BusinessFacade
    {
        /// <summary>
        /// Log folder location.
        /// </summary>
        public static string LogDirectory { get { return ConfigurationManager.AppSettings["cybs.logDirectory"]; } }

        /// <summary>
        /// Gets Merchant ID for topup payment.
        /// </summary>
        public static string TopupMerchant { get { return ConfigurationManager.AppSettings["TopupMechant"]; } }

        /// <summary>
        /// Gets Merchant ID for topup residential payment
        /// </summary>
        //public static string TopupResMerchantEx { get { return ConfigurationManager.AppSettings["TopupResMechant"]; } }
        public static string TopupResMerchant(string site) {
            string defaultMerchantId = ConfigurationManager.AppSettings["TopupResMechant.DEFAULT"];
            string merchantId = ConfigurationManager.AppSettings[string.Format("TopupResMechant.{0}", site)];           

            return string.IsNullOrEmpty(merchantId) ? defaultMerchantId : merchantId;
        }

        /// <summary>
        /// Payment reference code.
        /// </summary>
        public static string PaymentRefCode { get { return ConfigurationManager.AppSettings["PaymentRefCode"]; } }

        /// <summary>
        /// Payment residential reference code.
        /// </summary>
        public static string PaymentResidentialRefCode { get { return ConfigurationManager.AppSettings["PaymentResidentialRefCode"]; } }

        /// <summary>
        /// Gets the MVNO database connection string.
        /// </summary>
        public static string MVNOConnection { get { return ConfigurationManager.ConnectionStrings["MVNO"].ConnectionString; } }

        /// <summary>
        /// Gets the ESP database connection string.
        /// </summary>
        public static string ESPConnection { get { return ConfigurationManager.ConnectionStrings["ESP"].ConnectionString; } }

        /// <summary>
        /// Gets the ESP Residential database connection string.
        /// </summary>
        public static string ESPResidentialConnection { get { return ConfigurationManager.ConnectionStrings["ESPResidential"].ConnectionString; } }

        /// <summary>
        /// Gets the Residential database connection string.
        /// </summary>
        public static string ResidentialConnection { get { return ConfigurationManager.ConnectionStrings["Residential"].ConnectionString; } }


        public static string paymentconnection { get { return ConfigurationManager.ConnectionStrings["Payment"].ConnectionString; } }

        /// <summary>
        /// Gets the WebPayment database connection string.
        /// </summary>
        public static string WebPaymentConnection { get { return ConfigurationManager.ConnectionStrings["WebPayment"].ConnectionString; } }

        /// <summary>
        /// TPG Service URL
        /// </summary>
        public static string TPGService { get { return ConfigurationManager.AppSettings["TPGService"]; } }

        /// <summary>
        /// System name for SMS message originator
        /// </summary>
        public static string SMSOriginator { get { return ConfigurationManager.AppSettings["SMSOriginator"]; } }

        /// <summary>
        /// SMS text
        /// </summary>
        public static string SMSMessage { get { return ConfigurationManager.AppSettings["SMSMessage"]; } }

        /// <summary>
        /// Using Log History
        /// </summary>
        public static string UsingLog { get { return ConfigurationManager.AppSettings["UsingLog"]; } }

        /// <summary>
        /// Smtp Server
        /// </summary>
        public static string SmtpServer { get { return ConfigurationManager.AppSettings["SmtpServer"]; } }
        /// <summary>
        /// Mail From
        /// </summary>
        public static string MailFrom { get { return ConfigurationManager.AppSettings["MailFrom"]; } }
        /// <summary>
        /// Mail User
        /// </summary>
        public static string MailUser { get { return ConfigurationManager.AppSettings["MailUser"]; } }
        /// <summary>
        /// Mail Pass
        /// </summary>
        public static string MailPass { get { return ConfigurationManager.AppSettings["MailPass"]; } }
        /// <summary>
        /// Email To
        /// </summary>
        public static string EmailTo { get { return ConfigurationManager.AppSettings["EmailTo"]; } }

        /// <summary>
        /// Application ID
        /// </summary>
        public static string ApplicationID { get { return ConfigurationManager.AppSettings["ApplicationID"]; } }
    }
}
