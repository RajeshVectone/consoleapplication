﻿/*
 * Author       : Herdianto Suseno (2010-05-19)
 * Description  : Represents the data access layer.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using CCPaymentDL;

namespace CCPaymentBL
{
    /// <summary>
    /// Represents the data access layer.
    /// </summary>
    public class DataAccess
    {
        /// <summary>
        /// Adds parameter to SqlCommand.
        /// </summary>
        public static void AddSqlCommandParam(SqlCommand sql, string parameterName, SqlDbType sqlDbType, int size, ParameterDirection direction, object value)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = parameterName;
            param.SqlDbType = sqlDbType;
            param.Size = size;
            param.Direction = direction;
            if (value != null)
                param.Value = value;

            sql.Parameters.Add(param);
        }

        /// <summary>
        /// Executes the query, and returns the specified column of the first row in the result set returned by the query.
        /// </summary>
        public static object ExecuteScalar(SqlCommand command, string columnName)
        {
            object result = null;
            command.Connection.Open();
            using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.KeyInfo))
            {
                if (dataReader.Read() && columnName != null)
                    result = dataReader[columnName];
            }
            command.Connection.Close();
            return result;
        }


        public static DataSet Execute(string storedProcedure, string connectionString, params SqlParameter[] args)
        {
            SqlCommand sql = new SqlCommand(storedProcedure, new SqlConnection(connectionString));
            sql.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter arg in args)
                sql.Parameters.Add(arg);

            DataSet result = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(sql);
            try
            {
                sql.Connection.Open();
                adapter.Fill(result);
            }
            catch (Exception) { }
            sql.Connection.Close();

            return result;
        }


        

        public static SqlParameter BuildSqlParameter(string paramName, ParameterDirection paramDirection, SqlDbType paramType, object value)
        {
            return new SqlParameter(paramName, paramType) { Direction = paramDirection, Value = value };
        }


        public static SqlParameter BuildSqlParameter(string paramName, ParameterDirection paramDirection, SqlDbType paramType, int size, object value)
        {
            return new SqlParameter(paramName, paramType) { Direction = paramDirection, Size = size, Value = value };
        }

        /// <summary>
        /// Executes the query, and returns all rows in the result set.
        /// </summary>
        public static void ExecuteQuery<T>(ListGenerator<T> generator, SqlCommand command, ref List<T> result)
        {
            command.Connection.Open();
            using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.KeyInfo))
            {
                generator(dataReader, ref result);
            }
            command.Connection.Close();
        }

        /// <summary>
        /// Executes a Transact-SQL statement against the connection and returns the number of rows affected.
        /// </summary>
        public static int ExecuteNonQuery(SqlCommand command)
        {
            int result = 0;
            command.Connection.Open();
            result = command.ExecuteNonQuery();
            command.Connection.Close();
            return result;
        }
    }

    /// <summary>
    /// Defines a helper function to specify how the row in the result set mapped into a T data type.
    /// </summary>
    public delegate void DataMapper<T>(IDataReader dataReader, ref T result);

    /// <summary>
    /// Defines a helper function to generate a strongly typed list of T from a result set.
    /// </summary>
    public delegate void ListGenerator<T>(IDataReader dataReader, ref List<T> result);
}
