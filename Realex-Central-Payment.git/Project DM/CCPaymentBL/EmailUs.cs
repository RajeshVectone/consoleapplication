using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mail;

namespace CCPaymentBL
{
    public class EmailUs
    {
        private string _SmtpServer;
        private string _MailTo;
        private string _MailCC;
        private string _MailBCC;
        private string _Body;
        private string _EmailAddress;
        private string _MailFrom;
        private string _SmtpUser;
        private string _SmtpPassword;
        private string _Subject;

        public string SmtpServer { get { return _SmtpServer; } set { _SmtpServer = value; } }
        public string MailTo { get { return _MailTo; } set { _MailTo = value; } }
        public string MailCC { get { return _MailCC; } set { _MailCC = value; } }
        public string MailBCC { get { return _MailBCC; } set { _MailBCC = value; } }
        public string Body { get { return _Body; } set { _Body = value; } }
        public string MailFrom { get { return _MailFrom; } set { _MailFrom = value; } }
        public string SmtpUser { get { return _SmtpUser; } set { _SmtpUser = value; } }
        public string SmtpPassword { get { return _SmtpPassword; } set { _SmtpPassword = value; } }
        public string Subject { get { return _Subject; } set { _Subject = value; } }


        public EmailUs()
        {
        }

        public EmailUs(
            string smtpserver,
            string mailto,
            string mailcc,
            string mailbcc,
            string body,
            string mailfrom,
            string smtpuser,
            string smtppassword,
            string subject
            )
        {
            _SmtpServer = smtpserver;
            _MailTo = mailto;
            _MailCC = mailcc;
            _MailBCC = mailbcc;
            _Body = body;
            _MailFrom = mailfrom;
            _SmtpUser = smtpuser;
            _SmtpPassword = smtppassword;
            _Subject = subject;
        }

        public bool isEmail(string inputEmail)
        {
            if (inputEmail == null) inputEmail = "";
            char[] delim = new char[] { ',', ';' };
            string[] inputEmailArr = inputEmail.Split(delim);

            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);

            for (int i = 0; i < inputEmailArr.Length; i++)
            {
                if (re.IsMatch(inputEmailArr[i]))
                    return (true);
                else
                    return (false);
            }
            return (true);
        }

        public bool emailSend()
        {
            // Create Mail Object
            MailMessage mail = new MailMessage();
            mail.Subject = _Subject;
            mail.Body = _Body;
            mail.BodyFormat = 0;

            int _errorcode = 0;
            if (!isEmail(_MailFrom))
                _errorcode = 1;
            else
                mail.From = _MailFrom;

            if (!isEmail(_MailTo))
                _errorcode = 1;
            else
                mail.To = _MailTo;

            if ((_MailCC != string.Empty) || (_MailCC.Length > 0))
            {

                if (!isEmail(_MailCC))
                    _errorcode = 1;
                else
                    mail.Cc = _MailCC;
            }

            if ((_MailBCC != string.Empty) || (_MailBCC.Length > 0))
            {
                if (!isEmail(_MailBCC))
                    _errorcode = 1;
                else
                    mail.Bcc = _MailBCC;
            }

            if ((_SmtpServer == string.Empty) || (_SmtpServer.Length == 0))
                _errorcode = 1;
            else
            {
                if (_SmtpUser != string.Empty)
                {
                    mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = 1;
                    mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusername"] = _SmtpUser;
                    mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"] = _SmtpPassword;
                }
                SmtpMail.SmtpServer = _SmtpServer;
            }

            try
            {
                if (_errorcode == 0)
                {
                    SmtpMail.Send(mail);
                }
            }
            catch (Exception exc)
            {
                string errmessg = exc.Message;
                _errorcode = 1;
            }
            return (_errorcode == 0);
        }
        public bool emailHTMLSend()
        {
            // Create Mail Object
            MailMessage mail = new MailMessage();
            mail.Subject = _Subject;
            mail.Body = _Body;
            mail.BodyFormat = MailFormat.Html;

            int _errorcode = 0;
            if (!isEmail(_MailFrom))
                _errorcode = 1;
            else
                mail.From = _MailFrom;

            if (!isEmail(_MailTo))
                _errorcode = 1;
            else
                mail.To = _MailTo;

            if ((_MailCC != string.Empty) || (_MailCC.Length > 0))
            {

                if (!isEmail(_MailCC))
                    _errorcode = 1;
                else
                    mail.Cc = _MailCC;
            }

            if ((_MailBCC != string.Empty) || (_MailBCC.Length > 0))
            {
                if (!isEmail(_MailBCC))
                    _errorcode = 1;
                else
                    mail.Bcc = _MailBCC;
            }

            if ((_SmtpServer == string.Empty) || (_SmtpServer.Length == 0))
                _errorcode = 1;
            else
            {
                if (_SmtpUser != string.Empty)
                {
                    mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = 1;
                    mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusername"] = _SmtpUser;
                    mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"] = _SmtpPassword;
                }
                SmtpMail.SmtpServer = _SmtpServer;
            }

            try
            {
                if (_errorcode == 0)
                {
                    SmtpMail.Send(mail);
                }
            }
            catch (Exception exc)
            {
                string errmessg = exc.Message;
                _errorcode = 1;
            }
            return (_errorcode == 0);
        }
    }
}
