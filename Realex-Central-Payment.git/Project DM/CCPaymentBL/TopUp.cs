/*
 * Author       : Herdianto Suseno (2010-08-27)
 * Description  : Represents the TopUp operations.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using CCPaymentDL;
using System.Collections;

namespace CCPaymentBL
{
    /// <summary>
    /// Represents the TopUp operations.
    /// </summary>
    public class Topup
    {
        private const string SP_GET_MOBILE = "mvno_get_auto_topup_msisdn_v1";
        private const string SP_CHECK_THRESHOLD = "esp_check_threshold";
        private const string SP_GET_SUBSCRIPTION = "select top 1 * from tAuxAutoTopup(nolock) where mobileno = '{0}' and status = 1";
        private const string SP_TOPUP = "tp_do_topup_process_byccdc_v2";
        private const string SP_AUTOTOPUP_STATUS = "vmukCybS_UpdateAutoTopupStatus";
        private const string SP_TOPUPTRIGGER_STATUS = "update autotopup_transaction set status = 1 where msisdn = {0} and status = 2";
        private const string SP_UNSUBSCRIBEAUTOTOPUP = "vmukAUT_CancelAutoTopup";

        /// <summary>
        /// Initializes a new instance of CCPaymentBL.Topup.
        /// </summary>
        public Topup() { }

        /// <summary>
        /// Gets the mobile number that needs topup.
        /// </summary>
        /// <returns></returns>
        public static string GetAutoTopupMobile()
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_GET_MOBILE, new SqlConnection(BusinessFacade.MVNOConnection));
                sql.CommandType = CommandType.StoredProcedure;
                return (DataAccess.ExecuteScalar(sql, "mobileno") ?? "").ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetAutoTopupMobile exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetAutoTopupMobile exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        public static string getmail(string mobileno)
        {
            try
            {
                SqlCommand sql = new SqlCommand("getpaymentcustemailid", new SqlConnection(BusinessFacade.paymentconnection));
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add("@account_id", SqlDbType.VarChar, 16).Value = mobileno;
                return (DataAccess.ExecuteScalar(sql, "email") ?? "").ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetAutoTopupMobile exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetAutoTopupMobile exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }

        }



        public static string IsVectonePAYG(string mobileNo)
        {
            string result = null;
            string vrd = null;
            SqlParameter errCode = new SqlParameter();
            SqlParameter errMesg = new SqlParameter();
            int brand = -1;
            try
            {
                errCode = DataAccess.BuildSqlParameter("@errcode", ParameterDirection.Output, SqlDbType.Int, null);
                errMesg = DataAccess.BuildSqlParameter("@errmsg", ParameterDirection.Output, SqlDbType.VarChar, 200, null);
                DataSet data = DataAccess.Execute("WEB_postpaid_check_msisdn", BusinessFacade.ESPConnection,
                    DataAccess.BuildSqlParameter("@mobileno", ParameterDirection.Input, SqlDbType.VarChar, 20, mobileNo),
                    errCode, errMesg);

                data = DataAccess.Execute("wsvc_brand_differentiation", BusinessFacade.ESPConnection,
                    DataAccess.BuildSqlParameter("@msisdn", ParameterDirection.Input, SqlDbType.VarChar, 16, mobileNo));
                string getresult = data.Tables[0].Rows[0]["brand_type"].ToString();
                if (getresult == "1")
                {
                    vrd = "Vectone";
                }
                else
                {
                    vrd = "Delight";
                }

                result = vrd;
                //
            }
            catch (Exception) { result = null; }

            return result;
        }


      


        public static string getchillimail(string accesscode)
        {
            try
            {
                SqlCommand sql = new SqlCommand("getpaymentcustemailid", new SqlConnection(BusinessFacade.ResidentialConnection));
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add("@account_id", SqlDbType.VarChar, 16).Value = accesscode;
                return (DataAccess.ExecuteScalar(sql, "email") ?? "").ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetAutoTopupMobile exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetAutoTopupMobile exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }

        }


        public static string getchicustomername(string accesscode)
        {
            try
            {
                SqlCommand sql = new SqlCommand("getpaymentcustemailid", new SqlConnection(BusinessFacade.ResidentialConnection));
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add("@account_id", SqlDbType.VarChar, 16).Value = accesscode;
                return (DataAccess.ExecuteScalar(sql, "firstname") ?? "").ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetAutoTopupMobile exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetAutoTopupMobile exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }

        }

        /// <summary>
        /// Gets the mobile threshold information.
        /// </summary>
        public static MobileThreshold GetMobileThreshold(string mobileNo)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_CHECK_THRESHOLD, new SqlConnection(BusinessFacade.ESPConnection));
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add("@msisdn", SqlDbType.VarChar, 16).Value = mobileNo;

                List<MobileThreshold> result = new List<MobileThreshold>();
                DataAccess.ExecuteQuery<MobileThreshold>(MobileThreshold.GenerateList<MobileThreshold>, sql, ref result);
                return result.Count > 0 ? result[0] : null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetMobileThreshold exception: {0}\n", (ex.InnerException ?? ex).Message));
                Console.WriteLine();
                HistoryLog.WriteLine(string.Format("GetMobileThreshold exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        /// <summary>
        /// Gets the auto topup information from a mobile number.
        /// </summary>
        public static AuxAutoTopup GetTopupInfo(string mobileNo)
        {
            try
            {
                SqlCommand sql = new SqlCommand(string.Format(SP_GET_SUBSCRIPTION, mobileNo), new SqlConnection(BusinessFacade.WebPaymentConnection));
                sql.CommandType = CommandType.Text;

                List<AuxAutoTopup> result = new List<AuxAutoTopup>();
                DataAccess.ExecuteQuery(AuxAutoTopup.GenerateList<AuxAutoTopup>, sql, ref result);
                return result.Count > 0 ? result[0] : null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("GetTopupInfo exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("GetTopupInfo exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        /// <summary>
        /// Do topup operation.
        /// </summary>
        public static StatusData TopupMobile(string mobileNo, string paymentRef, int productId, double amount, string curr)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_TOPUP, new SqlConnection(BusinessFacade.ESPConnection));
                sql.CommandType = CommandType.StoredProcedure;
                DataAccess.AddSqlCommandParam(sql, "@mobileno", SqlDbType.VarChar, 16, ParameterDirection.Input, mobileNo);
                DataAccess.AddSqlCommandParam(sql, "@paymentref", SqlDbType.VarChar, 64, ParameterDirection.Input, paymentRef);
                DataAccess.AddSqlCommandParam(sql, "@productid", SqlDbType.Int, 4, ParameterDirection.Input, productId);
                DataAccess.AddSqlCommandParam(sql, "@amount", SqlDbType.Float, 8, ParameterDirection.Input, amount);
                DataAccess.AddSqlCommandParam(sql, "@ccdc_curr", SqlDbType.VarChar, 3, ParameterDirection.Input, curr);
                DataAccess.AddSqlCommandParam(sql, "@calledby", SqlDbType.VarChar, 4, ParameterDirection.Input, BusinessFacade.ApplicationID);
                DataAccess.AddSqlCommandParam(sql, "@errcode", SqlDbType.Int, 4, ParameterDirection.Output, null);
                DataAccess.AddSqlCommandParam(sql, "@errmsg", SqlDbType.VarChar, 64, ParameterDirection.Output, null);
                DataAccess.AddSqlCommandParam(sql, "@incentif_data", SqlDbType.VarChar, 10, ParameterDirection.Output, null);
                DataAccess.AddSqlCommandParam(sql, "@total_notif", SqlDbType.VarChar, 255, ParameterDirection.Output, null);

                DataAccess.ExecuteScalar(sql, null);
                if (sql.Parameters["@errcode"].Value.ToString() == "0")
                    return new StatusData(0, "Topup succeeded.");
                else
                    return new StatusData(-1, string.Format("Topup failed, {0}.", sql.Parameters["@errmsg"].Value));
            }
            catch (Exception ex)
            {
                Console.WriteLine("TopupMobile exception: {0}\n", (ex.InnerException ?? ex).Message);
                HistoryLog.WriteLine(string.Format("TopupMobile exception: {0}", (ex.InnerException ?? ex).Message));
                return null;
            }
        }

        /// <summary>
        /// Unsubscribe from auto-topup
        /// </summary>
        public static void UnsubscribeAutoTopup(string mobileno)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_UNSUBSCRIBEAUTOTOPUP, new SqlConnection(BusinessFacade.ESPConnection));
                sql.CommandType = CommandType.StoredProcedure;
                DataAccess.AddSqlCommandParam(sql, "@mobileno", SqlDbType.VarChar, 16, ParameterDirection.Input, mobileno);
                DataAccess.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine("UnsubscribeAutoTopup exception: {0}\n", (ex.InnerException ?? ex).Message);
                HistoryLog.WriteLine(string.Format("UnsubscribeAutoTopup exception: {0}", (ex.InnerException ?? ex).Message));
            }
        }

        /// <summary>
        /// Update the auto-topup status.
        /// </summary>
        public static void UpdateAutoTopupStatus(int referenceCode, string fullReferenceCode, PaymentStatus paymentStatus, StatusData topupStatus)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_AUTOTOPUP_STATUS, new SqlConnection(BusinessFacade.WebPaymentConnection));
                sql.CommandType = CommandType.StoredProcedure;
                DataAccess.AddSqlCommandParam(sql, "@ref_code", SqlDbType.Int, 4, ParameterDirection.Input, referenceCode);
                DataAccess.AddSqlCommandParam(sql, "@decision", SqlDbType.VarChar, 10, ParameterDirection.Input, paymentStatus == null ? "ERROR" : (paymentStatus.Decision ?? "ERROR"));
                DataAccess.AddSqlCommandParam(sql, "@request_id", SqlDbType.VarChar, 30, ParameterDirection.Input, paymentStatus == null ? "" : paymentStatus.RequestID ?? "");
                DataAccess.AddSqlCommandParam(sql, "@authorization_code", SqlDbType.VarChar, 10, ParameterDirection.Input, paymentStatus == null ? "" : paymentStatus.AuthorizationCode ?? "");
                DataAccess.AddSqlCommandParam(sql, "@request_time", SqlDbType.VarChar, 50, ParameterDirection.Input, paymentStatus == null ? "" : paymentStatus.RequestTime ?? "");
                DataAccess.AddSqlCommandParam(sql, "@payment_status", SqlDbType.Int, 4, ParameterDirection.Input, paymentStatus == null ? -1 : paymentStatus.ReasonCode);
                DataAccess.AddSqlCommandParam(sql, "@payment_description", SqlDbType.VarChar, 255, ParameterDirection.Input, paymentStatus == null ? "Canceled" : string.Format("{0}: {1}", fullReferenceCode, paymentStatus.ReasonDescription) ?? "Canceled");
                DataAccess.AddSqlCommandParam(sql, "@topup_status", SqlDbType.Int, 4, ParameterDirection.Input, topupStatus == null ? -1 : topupStatus.ErrorCode);
                DataAccess.AddSqlCommandParam(sql, "@topup_description", SqlDbType.VarChar, 255, ParameterDirection.Input, topupStatus == null ? "Canceled" : topupStatus.ErrorMessage ?? "Canceled");
                DataAccess.AddSqlCommandParam(sql, "@sorderid", SqlDbType.VarChar, 20, ParameterDirection.Input, fullReferenceCode);
                DataAccess.AddSqlCommandParam(sql, "@reconciliation_id", SqlDbType.VarChar, 100, ParameterDirection.Input, paymentStatus.ReconciliationID);
                DataAccess.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("UpdateAutoTopupStatus exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("UpdateAutoTopupStatus exception: {0}", (ex.InnerException ?? ex).Message));
            }
        }

        /// <summary>
        /// Update the topup trigger status.
        /// </summary>
        public static void UpdateTopupTriggerStatus(string mobileNo)
        {
            try
            {
                SqlCommand sql = new SqlCommand(string.Format(SP_TOPUPTRIGGER_STATUS, mobileNo), new SqlConnection(BusinessFacade.MVNOConnection));
                sql.CommandType = CommandType.Text;
                DataAccess.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("UpdateTopupTriggerStatus exception: {0}\n", (ex.InnerException ?? ex).Message));
                HistoryLog.WriteLine(string.Format("UpdateTopupTriggerStatus exception: {0}", (ex.InnerException ?? ex).Message));
            }
        }
    }
}
