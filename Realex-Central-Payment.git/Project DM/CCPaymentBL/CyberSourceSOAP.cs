/*
 * Author       : Herdianto Suseno (2010-08-27)
 * Description  : Provides access to CyberSource services.
 */

using CyberSource.Clients;
using CyberSource.Clients.SoapWebReference;
using System;
using System.Configuration;
using System.Net;
using System.Web.Services.Protocols;
using System.Data;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using CCPaymentDL;

namespace CCPaymentBL
{
    /// <summary>
    /// Provides access to CyberSource services.
    /// </summary>
    public class CyberSourceSOAP
    {
        private const string SP_REGISTER_PAYMENT = "vmukCybS_RegisterPayment";

        /// <summary>
        /// Initializes a new instance of CCPaymentBL.CyberSourceSOAP.
        /// </summary>
        public CyberSourceSOAP() { }

        /// <summary>
        /// Registers payment and get the reference code.
        /// </summary>
        public static int RegisterPayment(string mobileNo, string subscriptionID, string currency, double thresholdAmount, double amount)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_REGISTER_PAYMENT, new SqlConnection(BusinessFacade.WebPaymentConnection));
                sql.CommandType = CommandType.StoredProcedure;
                DataAccess.AddSqlCommandParam(sql, "@mobileno", SqlDbType.VarChar, 30, ParameterDirection.Input, mobileNo);
                DataAccess.AddSqlCommandParam(sql, "@subscription_id", SqlDbType.VarChar, 40, ParameterDirection.Input, subscriptionID);
                DataAccess.AddSqlCommandParam(sql, "@currency", SqlDbType.VarChar, 5, ParameterDirection.Input, currency);
                DataAccess.AddSqlCommandParam(sql, "@threshold_amount", SqlDbType.Float, 8, ParameterDirection.Input, thresholdAmount);
                DataAccess.AddSqlCommandParam(sql, "@amount", SqlDbType.Float, 8, ParameterDirection.Input, amount);

                // TODO: must add "VMAUT" before the reference code
                int result = (int)DataAccess.ExecuteScalar(sql, "ref_code");
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("RegisterPayment exception: {0}", (ex.InnerException ?? ex).Message);
                HistoryLog.WriteLine(string.Format("RegisterPayment exception: {0}", (ex.InnerException ?? ex).Message));
                return -1;
            }
        }

        /// <summary>
        /// Executes payment
        /// </summary>
        public static PaymentStatus Payment(string merchantID, string fullReferenceCode, string subscriptionID, string mobileNo, string currency, string grandTotalAmount)
        {
            try
            {
                RequestMessage request = new RequestMessage();

                // set the merchant ID
                request.merchantID = merchantID;

                // Credit Card Authorization
                request.ccAuthService = new CCAuthService();
                request.ccAuthService.run = "true";

                // Credit Card Capture
                request.ccCaptureService = new CCCaptureService();
                request.ccCaptureService.run = "true";

                // add merchant define data
                request.merchantDefinedData = new MerchantDefinedData();
                request.merchantDefinedData.field1 = mobileNo;

                // add required fields
                request.merchantReferenceCode = fullReferenceCode;

                request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
                request.recurringSubscriptionInfo.subscriptionID = subscriptionID;

                request.purchaseTotals = new PurchaseTotals();
                request.purchaseTotals.currency = currency;
                request.purchaseTotals.grandTotalAmount = grandTotalAmount;

                request.decisionManager = new DecisionManager();
                request.decisionManager.enabled = "true";

                ReplyMessage reply = SoapClient.RunTransaction(request);
                SaveOrderState();
                ProcessReply(reply);

                return new PaymentStatus(request, reply);
            }
            catch (SignException se)
            {
                SaveOrderState();
                HandleSignException(se);
            }
            catch (SoapHeaderException she)
            {
                SaveOrderState();
                HandleSoapHeaderException(she);
            }
            catch (SoapBodyException sbe)
            {
                SaveOrderState();
                HandleSoapBodyException(sbe);
            }
            catch (WebException we)
            {
                SaveOrderState();
                HandleWebException(we);
            }

            // return nothing if an exception occured
            return null;
        }

        private static void SaveOrderState()
        {
            /*
             * This is where you store the order state in your system for
             * post-transaction analysis.  Information to store include the
             * invoice, the values of the reply fields, or the details of the
             * exception that occurred, if any.
             */
        }

        private static void ProcessReply(ReplyMessage reply)
        {
            string template = GetTemplate(reply.decision.ToUpper());
            string content = GetContent(reply);

            /*
             * Display result of transaction.  Being a console application,
             * this sample simply prints out some text on the screen.  Use
             * what is appropriate for your system (e.g. ASP.NET pages).
             */
            Console.WriteLine(template, content);
            HistoryLog.WriteLine(string.Format(template, content));
        }

        private static string GetTemplate(string decision)
        {
            /*
             * This is where you retrieve the HTML template that corresponds
             * to the decision.  This template has 'boiler-plate' wording and
             * can be stored in files or a database.  This is just one way to
             * retrieve feedback pages.  Use what is appropriate for your
             * system (e.g. ASP.NET pages).
             */

            if ("ACCEPT".Equals(decision))
            {
                return ("The transaction succeeded.{0}");
            }

            if ("REJECT".Equals(decision))
            {
                return ("Your order was not approved.{0}");
            }

            // ERROR
            return (
                "Your order could not be completed at this time.{0}" +
                "\nPlease try again later.");
        }

        private static string GetContent(ReplyMessage reply)
        {
            /*
             * This is where you retrieve the content that will be plugged
             * into the template.
             * 
             * The strings returned in this sample are mostly to demonstrate
             * how to retrieve the reply fields.  Your application should
             * display user-friendly messages.
             */

            int reasonCode = int.Parse(reply.reasonCode);
            switch (reasonCode)
            {
                // Success
                case 100:
                    return (
                        "\nRequest ID: " + reply.requestID +
                        "\nAuthorization Code: " +
                            reply.ccAuthReply.authorizationCode +
                        "\nCapture Request Time: " +
                            reply.ccCaptureReply.requestDateTime +
                        "\nCaptured Amount: " +
                            reply.ccCaptureReply.amount);

                // Missing field(s)
                case 101:
                    return (
                        "\nThe following required field(s) are missing: " +
                        EnumerateValues(reply.missingField));

                // Invalid field(s)
                case 102:
                    return (
                        "\nThe following field(s) are invalid: " +
                        EnumerateValues(reply.invalidField));

                // Insufficient funds
                case 204:
                    return (
                        "\nInsufficient funds in the account.  Please use a " +
                        "different card or select another form of payment.");

                // add additional reason codes here that you need to handle
                // specifically.

                default:
                    // For all other reason codes, return an empty string,
                    // in which case, the template will be displayed with no
                    // specific content.
                    return (String.Empty);
            }
        }

        private static void HandleSignException(SignException se)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format(
                "\nFailed to sign the request with error code '{0}' and " +
                "message '{1}'.", se.ErrorCode, se.Message);

            Console.WriteLine(template, content);
            HistoryLog.WriteLine(string.Format(template, content));
        }

        private static void HandleSoapHeaderException(SoapHeaderException she)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format(
                "\nA SOAP header exception was returned with fault code " +
                "'{0}' and message '{1}'.", she.Code, she.Message);

            Console.WriteLine(template, content);
            HistoryLog.WriteLine(string.Format(template, content));
        }

        private static void HandleSoapBodyException(SoapBodyException sbe)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format(
                "\nA SOAP body exception was returned with fault code " +
                "'{0}' and message '{1}'.", sbe.Code, sbe.Message);

            Console.WriteLine(template, content);
            HistoryLog.WriteLine(string.Format(template, content));

            if (sbe.Code.Namespace.Equals(SoapClient.CYBS_NAMESPACE) &&
                sbe.Code.Name.Equals("CriticalServerError"))
            {
                /* The transaction may have been completed by CyberSource.
                 * If your request included a payment service, you should
                 * notify the appropriate department in your company (e.g. by
                 * sending an email) so that they can confirm if the request
                 * did in fact complete by searching the CyberSource Support
                 * Screens using the request id.
                 * 
                 * The line below demonstrates how to retrieve the request id.
                 */

                Console.WriteLine(sbe.RequestID);
                HistoryLog.WriteLine(sbe.RequestID);
            }
        }

        private static void HandleWebException(WebException we)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format(
                "\nFailed to get a response with status '{0}' and " +
                "message '{1}'", we.Status, we.Message);

            Console.WriteLine(template, content);
            HistoryLog.WriteLine(string.Format(template, content));

            if (IsCriticalError(we))
            {
                /*
                 * The transaction may have been completed by CyberSource.
                 * If your request included a payment service, you should
                 * notify the appropriate department in your company (e.g. by
                 * sending an email) so that they can confirm if the request
                 * did in fact complete by searching the CyberSource Support
                 * Screens using the value of the merchantReferenceCode in
                 * your request.
                 */
            }
        }

        private static string EnumerateValues(string[] array)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (array != null)
            {
                foreach (string val in array)
                    sb.Append(val + "\n");
            }

            return (sb.ToString());
        }

        private static bool IsCriticalError(WebException we)
        {
            switch (we.Status)
            {
                case WebExceptionStatus.ProtocolError:
                    if (we.Response != null)
                    {
                        HttpWebResponse response
                            = (HttpWebResponse)we.Response;

                        // GatewayTimeout may be returned if you are
                        // connecting through a proxy server.
                        return (response.StatusCode ==
                            HttpStatusCode.GatewayTimeout);

                    }

                    // In case of ProtocolError, the Response property
                    // should always be present.  In the unlikely case 
                    // that it is not, we assume something went wrong
                    // along the way and to be safe, treat it as a
                    // critical error.
                    return (true);

                case WebExceptionStatus.ConnectFailure:
                case WebExceptionStatus.NameResolutionFailure:
                case WebExceptionStatus.ProxyNameResolutionFailure:
                case WebExceptionStatus.SendFailure:
                    return (false);

                default:
                    return (true);
            }
        }

        #region Methods in previous version

        private const string SP_PAYMENT_STATUS = "vmukCybS_UpdatePaymentStatus";

        /// <summary>
        /// Updates payment status.
        /// </summary>
        public static void UpdatePaymentStatus(int referenceCode, PaymentStatus status)
        {
            try
            {
                SqlCommand sql = new SqlCommand(SP_PAYMENT_STATUS, new SqlConnection(BusinessFacade.WebPaymentConnection));
                sql.CommandType = CommandType.StoredProcedure;
                DataAccess.AddSqlCommandParam(sql, "@ref_code", SqlDbType.Int, 4, ParameterDirection.Input, referenceCode);
                DataAccess.AddSqlCommandParam(sql, "@decision", SqlDbType.VarChar, 10, ParameterDirection.Input, status == null ? "ERROR" : status.Decision ?? "");
                DataAccess.AddSqlCommandParam(sql, "@request_id", SqlDbType.VarChar, 30, ParameterDirection.Input, status == null ? "" : status.RequestID ?? "");
                DataAccess.AddSqlCommandParam(sql, "@authorization_code", SqlDbType.VarChar, 10, ParameterDirection.Input, status == null ? "" : status.AuthorizationCode ?? "");
                DataAccess.AddSqlCommandParam(sql, "@request_time", SqlDbType.VarChar, 50, ParameterDirection.Input, status == null ? "" : status.RequestTime ?? "");
                DataAccess.AddSqlCommandParam(sql, "@payment_status", SqlDbType.Int, 4, ParameterDirection.Input, status == null ? -1 : status.ReasonCode);
                DataAccess.AddSqlCommandParam(sql, "@payment_description", SqlDbType.VarChar, 255, ParameterDirection.Input, status == null ? "Canceled" : status.ReasonDescription ?? "");
                DataAccess.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Update payment status failed: {0}", (ex.InnerException ?? ex).Message);
                HistoryLog.WriteLine(string.Format("Update payment status failed: {0}", (ex.InnerException ?? ex).Message));
            }
        }

        #endregion
    }
}
