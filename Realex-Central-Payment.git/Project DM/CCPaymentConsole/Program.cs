using System;
using System.IO;
using System.Web;
using System.Net;
using System.Collections.Generic;
using System.Text;
using CCPaymentBL;
using CCPaymentDL;
using System.Threading;
using System.Reflection;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace CCPaymentConsole
{
    class Program
    {
        public static object lockResidential = new object();
        public static string mobileno;
        public static string beginref;

        static void Main(string[] args)
        {           

            Assembly assem = Assembly.GetExecutingAssembly();

            string header = string.Format("Auto Topup Payment v{0}", assem.FullName.Split(',')[1].Split('=')[1]);
            HistoryLog.WriteLine(string.Format(string.Format("Auto Topup Payment v{0} Starting", assem.FullName.Split(',')[1].Split('=')[1])));
            Console.Title = header;
            Console.WriteLine(header);
            Console.WriteLine(new string('=', header.Length));
            Console.WriteLine();


            


                while (true)
                {
                    // Check if auto-topup mobile number exists
                    string autoTopupMobile = Topup.GetAutoTopupMobile() ?? string.Empty;
                    if (autoTopupMobile != string.Empty)
                    {
                        Console.WriteLine(string.Format("Auto-topup mobile number found: {0}\n", autoTopupMobile));
                        HistoryLog.WriteLine(string.Format("Auto-topup mobile number found: {0}\n", autoTopupMobile));

                        // Start auto payment topup thread process
                        Thread autoPaymentTopupThread = new Thread(new ParameterizedThreadStart(DoPaymentTopupProcess));
                        autoPaymentTopupThread.Start(autoTopupMobile);
                    }
                    //Check if auto-topup residential number exists
                    //ini di matikan untuk kebutuhan mobile portugal, residensial portugal udh di handle sama demon UK

                    //////////string autoTopupResidential = TopupRes.GetAutoTopupResidential() ?? string.Empty;
                    //////////if (autoTopupResidential != string.Empty)
                    //////////{
                    //////////    Console.WriteLine(string.Format("Auto-topup residential account found: {0}\n", autoTopupResidential));
                    //////////    HistoryLog.WriteLine(string.Format("Auto-topup residential account found: {0}\n", autoTopupResidential));

                    //////////    // Start auto payment topup thread process                    
                    //////////    Thread autoPaymentTopupResidentialThread = new Thread(new ParameterizedThreadStart(DoPaymentTopupResidentialProcess));
                    //////////    autoPaymentTopupResidentialThread.Start(autoTopupResidential);
                    //////////}
                    // Sleep
                    System.Threading.Thread.Sleep(500);
                }
        }




        private static string RemoveStringAtEnd(string searchStr, string targetStr)
        {
            if (targetStr.ToLower().EndsWith(searchStr.ToLower()))
            {
                string resultStr = targetStr.Substring(0, targetStr.Length - searchStr.Length);
                return resultStr;
            }
            return targetStr;
        }



        

        static void DoPaymentTopupProcess(object mobileno)
        {
            string autoTopupMobile = ((string)mobileno).Trim();
            HistoryLog.WriteLine(string.Format("<{0}> - Auto Topup Mobile Process Begining.", autoTopupMobile));

            // Threshold the mobile's balance
            MobileThreshold threshold = Topup.GetMobileThreshold(autoTopupMobile);
            if (threshold == null)
            {
                Console.WriteLine("<{0}> - Threshold information not found.\n", autoTopupMobile);
                HistoryLog.WriteLine(string.Format("<{0}> - Threshold information not found.", autoTopupMobile));
            }
            else if (threshold.Balance > threshold.TopupThreshold)
            {
                Console.WriteLine("<{0}> - Account balance is enough.\n", autoTopupMobile);
                HistoryLog.WriteLine(string.Format("<{0}> - Account balance is enough.", autoTopupMobile));
            }
            else
            {
                // Get CyberSource subscription info for the mobile number
                AuxAutoTopup TopupInfo = Topup.GetTopupInfo(autoTopupMobile);
                if (TopupInfo == null)
                {
                    Console.WriteLine("<{0}> - Subscription information not found.\n", autoTopupMobile);
                    HistoryLog.WriteLine(string.Format("<{0}> - Subscription information not found.", autoTopupMobile));
                }
                else
                {
                    // Get the transaction reference code for payment
                    int referenceCode = CyberSourceSOAP.RegisterPayment(autoTopupMobile,
                                                                        TopupInfo.SubscriptionID,
                                                                        TopupInfo.TopupCurr, threshold.Balance,
                                                                        TopupInfo.TopupAmount);
                    if (referenceCode == -1)
                    {
                        Console.WriteLine("<{0}> - Cannot register payment.\n", autoTopupMobile);
                        HistoryLog.WriteLine(string.Format("<{0}> - Cannot register payment.", autoTopupMobile));
                    }
                    else
                    {
                        PaymentStatus paymentStatus = null;
                        StatusData topupStatus = null;

                        string result = Topup.IsVectonePAYG(autoTopupMobile);                        
                        if (result == "Delight")
                        {
                            beginref = "DMUK-AUT-";
                        }
                        if (result == "Vectone")
                        {
                            beginref = "VMUK-AUT-";
                        }

                        // Do payment
                        string fullReferenceCode = string.Format("{0}{1}", beginref, referenceCode);

                        HistoryLog.WriteLine(string.Format("<{0}> - Process Payment.", autoTopupMobile));
                        //this is payment process on cybersource
                        paymentStatus = CyberSourceSOAP.Payment(BusinessFacade.TopupMerchant, fullReferenceCode,
                                                                TopupInfo.SubscriptionID, autoTopupMobile,
                                                                TopupInfo.TopupCurr,
                                                                TopupInfo.TopupAmount.ToString());
                        if (paymentStatus == null)
                        {
                            Console.WriteLine("<{0}> - Payment can not be completed.\n", autoTopupMobile);
                            HistoryLog.WriteLine(string.Format("<{0}> - Payment can not be completed. payment status null.", autoTopupMobile));
                        }
                        else if ("REJECT".Equals(paymentStatus.Decision.ToUpper()))
                        {
                            Console.WriteLine("<{0}> - Payment rejected.\n", autoTopupMobile);
                            HistoryLog.WriteLine(string.Format("<{0}> - Payment rejected.", autoTopupMobile));
                        }
                        else if (!"ACCEPT".Equals(paymentStatus.Decision.ToUpper()))
                        {
                            //this is payment succeed, so you may process email and sms thing!
                            Console.WriteLine("<{0}> - Payment could not be completed at this time.\n", autoTopupMobile);
                            HistoryLog.WriteLine(string.Format("<{0}> - Payment could not be completed at this time. payment status not Accept", autoTopupMobile));
                             string smsresult = Topup.IsVectonePAYG(autoTopupMobile);
                             if (smsresult == "Delight")
                             {
                                 SMS smsEngine = new SMS();
                                 string smsmsg = "Hello! Delight Mobile topped you up automatically by � " + TopupInfo.TopupAmount + " because your balance was low. Thank you for using this automated service. Queries: call 202.";
                                 string[] tpgResponse = smsEngine.SendSMS(autoTopupMobile, BusinessFacade.SMSOriginator,
                                                                          smsmsg);
                                 HistoryLog.WriteLine(string.Format("<{0}> - Send SMS.", autoTopupMobile));
                                 string amt = Convert.ToString(TopupInfo.TopupAmount);
                                 string autotopupemail = Topup.getmail(autoTopupMobile);
                                 bool sendmail = Program.Proceed_Email(autoTopupMobile, amt, fullReferenceCode, autotopupemail);
                                 // All process has been running
                                 Console.WriteLine("<{0}> - Topup mobile succeeded.\n", autoTopupMobile);
                                 HistoryLog.WriteLine(string.Format("<{0}> - Topup mobile succeeded.", autoTopupMobile));
                                 string mailSubject = "Delight Mobile UK Pay As You Go Top Up Declined";
                                 MailAddress mailFrom = new MailAddress("noreply@delightmobile.co.uk", "Delightmobile");
                                 MailAddressCollection mailTo = new MailAddressCollection();
                             }
                             if (smsresult == "Vectone")
                             {
                                 SMS smsEngine = new SMS();
                                 string smsmsg = "Hello! Vectone Mobile topped you up automatically by � " + TopupInfo.TopupAmount + " because your balance was low. Thank you for using this automated service. Queries: call 322.";
                                 string[] tpgResponse = smsEngine.SendSMS(autoTopupMobile, BusinessFacade.SMSOriginator,
                                                                          smsmsg);
                                 HistoryLog.WriteLine(string.Format("<{0}> - Send SMS.", autoTopupMobile));
                                 string amt = Convert.ToString(TopupInfo.TopupAmount);
                                 string autotopupemail = Topup.getmail(autoTopupMobile);
                                 bool sendmail = Program.Proceed_Emailvect(autoTopupMobile, amt, fullReferenceCode, autotopupemail);
                                 // All process has been running
                                 Console.WriteLine("<{0}> - Topup mobile succeeded.\n", autoTopupMobile);
                                 HistoryLog.WriteLine(string.Format("<{0}> - Topup mobile succeeded.", autoTopupMobile));
                                 string mailSubject = "Vectone Mobile UK Pay As You Go Top Up Declined";
                                 MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectonemobile");
                                 MailAddressCollection mailTo = new MailAddressCollection();
                             }
                          
                        }
                        else
                        {
                            // The decision is ACCEPT
                            Console.WriteLine("<{0}> - Payment suceeded.\n", autoTopupMobile);

                            HistoryLog.WriteLine(string.Format("<{0}> - Payment suceeded.", autoTopupMobile));

                            HistoryLog.WriteLine(string.Format("<{0}> - Process Topup mobile.", autoTopupMobile));
                            // Last process, do topup                           


                            topupStatus = Topup.TopupMobile(autoTopupMobile, fullReferenceCode,
                                                            TopupInfo.ProductID, TopupInfo.TopupAmount,
                                                            TopupInfo.TopupCurr);

                            string smsresult = Topup.IsVectonePAYG(autoTopupMobile);
                            if (smsresult == "Delight")
                            {

                                SMS smsEngine = new SMS();
                                string smsmsg = "Hello! Delight Mobile topped you up automatically by � " + TopupInfo.TopupAmount + " because your balance was low. Thank you for using this automated service. Queries: call 202.";
                                string[] tpgResponse = smsEngine.SendSMS(autoTopupMobile, BusinessFacade.SMSOriginator,
                                                                         smsmsg);
                                HistoryLog.WriteLine(string.Format("<{0}> - Send SMS.", autoTopupMobile));
                                string amt = Convert.ToString(TopupInfo.TopupAmount);
                                string autoTopupemail = Topup.getmail(autoTopupMobile);
                                bool sendmail = Program.Proceed_Email(autoTopupMobile, amt, fullReferenceCode, autoTopupemail);
                                Console.WriteLine("<{0}> - Mail sent.\n", sendmail);
                                // All process has been running
                                Console.WriteLine("<{0}> - Topup mobile succeeded.\n", autoTopupMobile);
                                HistoryLog.WriteLine(string.Format("<{0}> - Topup mobile succeeded.", autoTopupMobile));
                                string mailSubject = "Delight Mobile UK Pay As You Go Top Up Declined";
                                MailAddress mailFrom = new MailAddress("noreply@delightmobile.co.uk", "Delightmobile");
                                MailAddressCollection mailTo = new MailAddressCollection();
                                Console.WriteLine("<{0}> - SMS (siva) Payment suceeded.\n", autoTopupMobile);
                            }

                            if (smsresult == "Vectone")
                            {
                                SMS smsEngine = new SMS();
                                string smsmsg = "Hello! Vectone Mobile topped you up automatically by � " + TopupInfo.TopupAmount + " because your balance was low. Thank you for using this automated service. Queries: call 322.";
                                string[] tpgResponse = smsEngine.SendSMS(autoTopupMobile, BusinessFacade.SMSOriginator,
                                                                         smsmsg);
                                HistoryLog.WriteLine(string.Format("<{0}> - Send SMS.", autoTopupMobile));
                                string amt = Convert.ToString(TopupInfo.TopupAmount);
                                string autoTopupemail = Topup.getmail(autoTopupMobile);
                                bool sendmail = Program.Proceed_Emailvect(autoTopupMobile, amt, fullReferenceCode, autoTopupemail);
                                Console.WriteLine("<{0}> - Mail sent.\n", sendmail);
                                // All process has been running
                                Console.WriteLine("<{0}> - Topup mobile succeeded.\n", autoTopupMobile);
                                HistoryLog.WriteLine(string.Format("<{0}> - Topup mobile succeeded.", autoTopupMobile));
                                string mailSubject = "Delight Mobile UK Pay As You Go Top Up Declined";
                                MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectonemobile");
                                MailAddressCollection mailTo = new MailAddressCollection();
                                Console.WriteLine("<{0}> - SMS (siva) Payment suceeded.\n", autoTopupMobile);
                            }
                            if (topupStatus == null)
                            {
                                Console.WriteLine("<{0}> - Topup failed.\n", autoTopupMobile);
                                HistoryLog.WriteLine(string.Format("<{0}> - Topup failed. Top Up Status null.", autoTopupMobile));
                            }
                            else if (topupStatus.ErrorCode != 0)
                            {
                                Console.WriteLine("<{0}> - Topup failed, {1}.\n", autoTopupMobile, topupStatus.ErrorMessage);
                                HistoryLog.WriteLine(string.Format("<{0}> - Topup failed, {1}.", autoTopupMobile, topupStatus.ErrorMessage));
                            }
                            else
                            {
                                //this one ryan
                                // this when topup, but maybe payment succeed but topup failed
                                // Send SMS to user when auto-topup success
                                //ok i ll add above this coding part..please check it..how to process the steps.

                            }
                        }

                        // Cancel the auto-topup subscription if the payment rejected
                        if (paymentStatus != null && "REJECT".Equals(paymentStatus.Decision.ToUpper()))
                        {
                            Topup.UnsubscribeAutoTopup(autoTopupMobile);
                            HistoryLog.WriteLine(string.Format("<{0}> - Unsubscribe Auto TopUp.", autoTopupMobile));
                        }

                        // Update status
                        Topup.UpdateAutoTopupStatus(referenceCode, fullReferenceCode, paymentStatus, topupStatus);
                        HistoryLog.WriteLine(string.Format("<{0}> - Update Auto TopUp Status.", autoTopupMobile));
                    }
                }
            }
            // Update status
            Topup.UpdateTopupTriggerStatus(autoTopupMobile);
            HistoryLog.WriteLine(string.Format("<{0}> - Update TopUp Trigger Status.", autoTopupMobile));
            HistoryLog.WriteLine(string.Format("<{0}> - Auto TopUp Mobile Process Ending.\n", autoTopupMobile));
        }

        /// <summary>
        /// Executes payment and topup process for residential numbers.
        /// </summary>
        static void DoPaymentTopupResidentialProcess(object residentialNo)
        {
            lock (lockResidential)
            {
                bool IsTransactionSuccess = false;
                bool IsTopUpSuccess = false;

                string autoTopupResidential = ((string)residentialNo).Trim();
                HistoryLog.WriteLine(string.Format("<{0}> - Auto Topup Residential Process Begining.", autoTopupResidential));

                // Threshold the residential's balance
                MobileThreshold threshold = TopupRes.GetAccountThreshold(autoTopupResidential);
                if (threshold == null)
                {
                    Console.WriteLine("<{0}> - Threshold information not found.\n", autoTopupResidential);
                    HistoryLog.WriteLine(string.Format("<{0}> - Threshold information not found.", autoTopupResidential));
                }
                else if (threshold.Balance > threshold.TopupThreshold)
                {
                    Console.WriteLine("<{0}> - Account balance is enough.\n", autoTopupResidential);
                    HistoryLog.WriteLine(string.Format("<{0}> - Account balance is enough.", autoTopupResidential));
                }
                else
                {
                    // Get CyberSource subscription info for the residential number
                    AuxAutoTopup TopupInfo = TopupRes.GetTopupInfo(autoTopupResidential);
                    if (TopupInfo == null)
                    {
                        Console.WriteLine("<{0}> - Subscription information not found.\n", autoTopupResidential);
                        HistoryLog.WriteLine(string.Format("<{0}> - Subscription information not found.",
                                                           autoTopupResidential));
                    }
                    else
                    {
                        // Get the transaction reference code for payment
                        int referenceCode = CyberSourceSOAP.RegisterPayment(autoTopupResidential,
                                                                            TopupInfo.SubscriptionID,
                                                                            TopupInfo.TopupCurr, threshold.Balance,
                                                                            TopupInfo.TopupAmount);
                        if (referenceCode == -1)
                        {
                            Console.WriteLine("<{0}> - Cannot register payment.\n", autoTopupResidential);
                            HistoryLog.WriteLine(string.Format("<{0}> - Cannot register payment.", autoTopupResidential));
                        }
                        else
                        {
                            PaymentStatus paymentStatus = null;
                            StatusData topupStatus = null;
                            string paymentMessage = string.Empty;
                            string topUpMessage = string.Empty;

                            // Do payment
                            string fullReferenceCode = string.Format("{0}{1}", BusinessFacade.PaymentResidentialRefCode, referenceCode);
                            HistoryLog.WriteLine(string.Format("<{0}> - Process Payment.", autoTopupResidential));
                            string merchant = TopupRes.GetRouting(TopupInfo.Site, "WEB123") ?? string.Empty;

                            if (merchant == "")
                            {
                                Console.WriteLine("<{0}> - Routing payment not found.\n", autoTopupResidential);
                                HistoryLog.WriteLine(string.Format("<{0}> - Routing use default.", autoTopupResidential));
                                merchant = BusinessFacade.TopupResMerchant(TopupInfo.Site);
                                HistoryLog.WriteLine(string.Format("<{0}> - Process Payment continue with default routing.", autoTopupResidential));
                            }


                            paymentStatus = CyberSourceSOAP.Payment(merchant,
                                                                    fullReferenceCode,
                                                                    TopupInfo.SubscriptionID, autoTopupResidential,
                                                                    TopupInfo.TopupCurr,
                                                                    TopupInfo.TopupAmount.ToString());

                            //versi didit
                            //paymentStatus = CyberSourceSOAP.Payment(BusinessFacade.TopupResMerchant(TopupInfo.Site),
                            //                                        fullReferenceCode,
                            //                                        TopupInfo.SubscriptionID, autoTopupResidential,
                            //                                        TopupInfo.TopupCurr,
                            //                                        TopupInfo.TopupAmount.ToString());
                            if (paymentStatus == null)
                            {
                                Console.WriteLine("<{0}> - Payment can not be completed.\n", autoTopupResidential);
                                paymentMessage =
                                    string.Format("<{0}><{1}> - Payment can not be completed. payment status null",
                                                  autoTopupResidential, fullReferenceCode);
                                HistoryLog.WriteLine(paymentMessage);
                                HistoryLog.WriteError(paymentMessage);
                            }
                            else if ("REJECT".Equals(paymentStatus.Decision.ToUpper()))
                            {
                                Console.WriteLine("<{0}> - Payment rejected.\n ", autoTopupResidential);
                                HistoryLog.WriteLine(string.Format("<{0}> - Payment rejected.\n ", autoTopupResidential));
                                paymentMessage =
                                    string.Format("<{0}><{1}> - Payment rejected.\n Payment Information \n {2} \n",
                                                  autoTopupResidential, fullReferenceCode, paymentStatus);
                                HistoryLog.WriteError(paymentMessage);
                            }
                            else if (!"ACCEPT".Equals(paymentStatus.Decision.ToUpper()))
                            {
                                Console.WriteLine("<{0}> - Payment could not be completed at this time.\n",
                                                  autoTopupResidential);
                                HistoryLog.WriteLine(
                                    string.Format("<{0}> - Payment could not be completed at this time.\n",
                                                  autoTopupResidential));
                                paymentMessage =
                                    string.Format(
                                        "<{0}><{1}> - Payment could not be completed at this time.\n Payment Information \n {2} \n",
                                        autoTopupResidential, fullReferenceCode, paymentStatus);
                                HistoryLog.WriteError(paymentMessage);
                            }
                            else
                            {
                                IsTransactionSuccess = true;
                                // The decision is ACCEPT
                                Console.WriteLine("<{0}> - Payment suceeded.\n", autoTopupResidential);
                                HistoryLog.WriteLine(string.Format("<{0}> - Payment suceeded.\n", autoTopupResidential));

                                //Last process, do topup
                                HistoryLog.WriteLine(string.Format("<{0}> - Process Topup Residential.",
                                                                   autoTopupResidential));
                                topupStatus = TopupRes.TopupResidential(referenceCode, autoTopupResidential,
                                                                        fullReferenceCode, TopupInfo.ProductID,
                                                                        TopupInfo.TopupAmount, TopupInfo.TopupCurr);

                                string autoTopupemail = Topup.getchillimail(autoTopupResidential);
                                string amt = Convert.ToString(TopupInfo.TopupAmount);
                                string autoTopupname = Topup.getchicustomername(autoTopupResidential);
                                bool sendmail = Program.Proceed_Email(autoTopupname, amt, fullReferenceCode, autoTopupemail);
                                Console.WriteLine("<{0}> - Mail sent.\n", sendmail);
                                // All process has been running
                                Console.WriteLine("<{0}> - Topup mobile succeeded.\n", autoTopupResidential);
                                HistoryLog.WriteLine(string.Format("<{0}> - Topup mobile succeeded.", autoTopupResidential));
                                Console.WriteLine("<{0}> - SMS (siva) Payment suceeded.\n", autoTopupResidential);

                                if (topupStatus == null)
                                {
                                    Console.WriteLine("<{0}> - Topup failed.\n", autoTopupResidential);
                                    HistoryLog.WriteLine(string.Format("<{0}> - Topup failed. Topup status null",
                                                                       autoTopupResidential));
                                    topUpMessage = string.Format("<{0}><{1}> - Topup failed. Topup status null \n Payment Information {2}\n",
                                                                    autoTopupResidential, fullReferenceCode, paymentStatus);
                                    HistoryLog.WriteError(topUpMessage);
                                    //HistoryLog.EmailError(topUpMessage);
                                }
                                else if (topupStatus.ErrorCode != 0)
                                {
                                    Console.WriteLine("<{0}> - Topup failed, {1}.\n", autoTopupResidential,
                                                      topupStatus.ErrorMessage);
                                    HistoryLog.WriteLine(string.Format("<{0}> - Topup failed, {1}.",
                                                                       autoTopupResidential, topupStatus.ErrorMessage));
                                    topUpMessage =
                                        string.Format(
                                            "<{0}><{1}> - Topup failed. \n Payment Information {2}\n TopUp Information {3}\n",
                                            autoTopupResidential, fullReferenceCode, paymentStatus, topupStatus);
                                    HistoryLog.WriteError(topUpMessage);
                                    //HistoryLog.EmailError(topUpMessage);
                                }
                                else
                                {
                                    // Send SMS to user when auto-topup success
                                    SMS smsEngine = new SMS();
                                    string[] tpgResponse = smsEngine.SendSMS(autoTopupResidential, BusinessFacade.SMSOriginator, BusinessFacade.SMSMessage);
                                    topUpMessage = string.Format("<{0}><{1}> - Topup residential succeeded.\n Payment Information {2}\n TopUp Information {3}\n",
                                            autoTopupResidential, fullReferenceCode, paymentStatus, topupStatus);

                                    // All process has been running
                                    Console.WriteLine("<{0}> - Topup residential succeeded.\n", autoTopupResidential);
                                    HistoryLog.WriteLine(string.Format("<{0}> - Topup residential succeeded.\n.", autoTopupResidential));
                                    HistoryLog.WriteSuccess(topUpMessage);
                                    IsTopUpSuccess = true;
                                    if (!topupStatus.ErrorMessage.ToUpper().Equals("SUCCESS"))
                                    {
                                        HistoryLog.WriteError(topUpMessage);
                                        //HistoryLog.EmailError(topUpMessage);
                                        IsTopUpSuccess = false;
                                    }
                                }
                                if (IsTransactionSuccess && !IsTopUpSuccess)
                                {
                                    object args = new object[]
                                                      {
                                                          autoTopupResidential, 
                                                          referenceCode, 
                                                          fullReferenceCode,
                                                          TopupInfo, 
                                                          paymentStatus, 
                                                          0
                                                      };
                                    Thread topUpProcessAgain = new Thread(new ParameterizedThreadStart(DoTopUpResidentialProcess));
                                    topUpProcessAgain.Start(args);
                                }
                            }

                            // Cancel the auto-topup subscription if the payment rejected
                            if (paymentStatus != null && "REJECT".Equals(paymentStatus.Decision.ToUpper()))
                            {
                                TopupRes.UnsubscribeAutoTopup(autoTopupResidential);
                                HistoryLog.WriteLine(string.Format("<{0}> - Unsubscribe Auto TopUp.",
                                                                   autoTopupResidential));
                            }

                            // Update status
                            TopupRes.UpdateAutoTopupStatus(referenceCode, fullReferenceCode, paymentStatus, topupStatus);
                            HistoryLog.WriteLine(string.Format("<{0}> - Update Auto TopUp Status.", autoTopupResidential));
                        }
                    }
                }
                if (IsTransactionSuccess && !IsTopUpSuccess)
                    return;
                // Update status
                TopupRes.UpdateTopupTriggerStatus(autoTopupResidential);
                HistoryLog.WriteLine(string.Format("<{0}> - Update TopUp Trigger Status.", autoTopupResidential));
                HistoryLog.WriteLine(string.Format("<{0}> - Auto TopUp Residential Process Ending.\n", autoTopupResidential));

            }
        }

        static void DoTopUpResidentialProcess(object args)
        {
            Array argArray = (Array)args;
            string autoTopupResidential = (string)argArray.GetValue(0);
            int referenceCode = (int)argArray.GetValue(1);
            string fullReferenceCode = (string)argArray.GetValue(2);
            AuxAutoTopup TopupInfo = (AuxAutoTopup)argArray.GetValue(3);
            PaymentStatus paymentStatus = (PaymentStatus)argArray.GetValue(4);
            int process = (int)argArray.GetValue(5);

            bool IsTopUpSuccess = false;
            StatusData topupStatus = null;
            string topUpMessage = string.Empty;
            process = process + 1;
            string updateStatus = string.Format("please execute this : update autotopup_transaction set status = 1 where accountid = {0} and status = 2", autoTopupResidential);

            //Last process, do topup
            HistoryLog.WriteLine(string.Format("<{0}> - Process Topup Residential Again<{1}>.", autoTopupResidential, process));
            topupStatus = TopupRes.TopupResidential(referenceCode, autoTopupResidential, fullReferenceCode, TopupInfo.ProductID,
                                                    TopupInfo.TopupAmount, TopupInfo.TopupCurr);

            if (topupStatus == null)
            {
                Console.WriteLine("<{0}> - Topup failed Again<{1}>.\n", autoTopupResidential, process);
                HistoryLog.WriteLine(string.Format("<{0}> - Topup failed. Topup status null", autoTopupResidential));
                topUpMessage = string.Format("<{0}><{1}> - Topup failed. Topup status null \n Payment Information {2}\n {3}\n",
                                                autoTopupResidential, fullReferenceCode, paymentStatus, updateStatus);
                HistoryLog.WriteError(topUpMessage);
                if (!IsTopUpSuccess && process >= 5)
                    HistoryLog.EmailError(topUpMessage);
            }
            else if (topupStatus.ErrorCode != 0)
            {
                Console.WriteLine("<{0}> - Topup failed, {1}.\n", autoTopupResidential, topupStatus.ErrorMessage);
                HistoryLog.WriteLine(string.Format("<{0}> - Topup failed, {1}.", autoTopupResidential, topupStatus.ErrorMessage));
                topUpMessage = string.Format("<{0}><{1}> - Topup failed. \n Payment Information {2}\n TopUp Information {3}\n {4}\n",
                                                autoTopupResidential, fullReferenceCode, paymentStatus, topupStatus, updateStatus);
                HistoryLog.WriteError(topUpMessage);
                if (!IsTopUpSuccess && process >= 5)
                    HistoryLog.EmailError(topUpMessage);
            }
            else
            {
                // Send SMS to user when auto-topup success
                SMS smsEngine = new SMS();
                string[] tpgResponse = smsEngine.SendSMS(autoTopupResidential, BusinessFacade.SMSOriginator, BusinessFacade.SMSMessage);
                topUpMessage = string.Format("<{0}><{1}> - Topup residential succeeded.\n Payment Information {2}\n TopUp Information {3}\n {4}\n",
                                                autoTopupResidential, fullReferenceCode, paymentStatus, topupStatus, updateStatus);

                // All process has been running
                Console.WriteLine("<{0}> - Topup residential succeeded<{1}>.\n", autoTopupResidential, process);
                HistoryLog.WriteLine(string.Format("<{0}> - Topup residential succeeded<{1}>.\n.", autoTopupResidential, process));
                HistoryLog.WriteSuccess(topUpMessage);
                IsTopUpSuccess = true;
                if (!topupStatus.ErrorMessage.ToUpper().Equals("SUCCESS"))
                {
                    HistoryLog.WriteError(topUpMessage);
                    if (!IsTopUpSuccess && process >= 5)
                        HistoryLog.EmailError(topUpMessage);
                }
            }
            if (!IsTopUpSuccess && process < 5)
            {
                object otherArgs = new object[]
                              {
                                  autoTopupResidential, 
                                  referenceCode, 
                                  fullReferenceCode,
                                  TopupInfo, 
                                  paymentStatus, 
                                  process
                              };
                DoTopUpResidentialProcess(otherArgs);
            }
            if (!IsTopUpSuccess)
                return;
            TopupRes.UpdateTopupTriggerStatus(autoTopupResidential);
            HistoryLog.WriteLine(string.Format("<{0}> - Update TopUp Trigger Status.", autoTopupResidential));
            HistoryLog.WriteLine(string.Format("<{0}> - Auto TopUp Residential Process Ending.\n", autoTopupResidential));
        }


        static bool Proceed_Email(string topupno, string amount, string merchantcode, string email)
        {
            try
            {
                string mailContent = string.Empty;
                string mailLocation = System.IO.Path.GetFullPath("~/mail/Auto-top-up-email.htm");
                string[] setstring = mailLocation.Split('~');
                Assembly assembly = Assembly.GetExecutingAssembly();
                string assemblyDir = assembly.CodeBase;
                assemblyDir = assemblyDir.Replace("file:///", "");
                assemblyDir = Path.GetDirectoryName(assemblyDir);
                string applicationDir = RemoveStringAtEnd(@"\bin\Debug\", setstring[0]);
                string crtpath = applicationDir + setstring[1];
                StreamReader mailContent_file = new StreamReader(crtpath);
                string mailContents = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContents.Replace("[First_Name]", topupno).Replace("<�xxx>", amount).Replace("XXXXXX", merchantcode);
                string mailSubject = "Delight Mobile UK Pay As You Go Autotopup Confirmation";
                MailAddress mailFrom = new MailAddress("noreply@delightmobile.co.uk", "Delightmobile");
                MailAddressCollection mailTo = new MailAddressCollection();
                MailAddressCollection mailbcc = new MailAddressCollection();
                mailTo.Add(new MailAddress(email, "customer"));
                string finance = "financereconcilation@vectone.com";
                string csteam = "CSNotifications@vectone.com";
                string autouser = "Autotopemails@vectone.com";
                mailbcc.Add(new MailAddress(finance, "financeteam"));
                mailbcc.Add(new MailAddress(csteam, "csteam"));
                mailbcc.Add(new MailAddress(autouser, "Auto"));
                bool emailSent = CCPaymentConsole.email.Send(true, mailFrom, mailTo, null, mailbcc,
                    mailSubject,
                    mailContent);
                return true;
            }

            catch (Exception ex)
            {

                return false;
            }
        }


        static bool Proceed_Emailvect(string topupno, string amount, string merchantcode, string email)
        {
            try
            {
                string mailContent = string.Empty;
                string mailLocation = System.IO.Path.GetFullPath("~/mail/Auto-top-up-vecremail.htm");
                string[] setstring = mailLocation.Split('~');
                Assembly assembly = Assembly.GetExecutingAssembly();
                string assemblyDir = assembly.CodeBase;
                assemblyDir = assemblyDir.Replace("file:///", "");
                assemblyDir = Path.GetDirectoryName(assemblyDir);
                string applicationDir = RemoveStringAtEnd(@"\bin\Debug\", setstring[0]);
                string crtpath = applicationDir + setstring[1];
                StreamReader mailContent_file = new StreamReader(crtpath);
                string mailContents = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContents.Replace("[First_Name]", topupno).Replace("<�xxx>", amount).Replace("XXXXXX", merchantcode);
                string mailSubject = "Vectone Mobile UK Pay As You Go Autotopup Confirmation";
                MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectonemobile");
                MailAddressCollection mailTo = new MailAddressCollection();
                MailAddressCollection mailbcc = new MailAddressCollection();
                mailTo.Add(new MailAddress(email, "customer"));
                string finance = "financereconcilation@vectone.com";
                string csteam = "CSNotifications@vectone.com";
                string autouser = "Autotopemails@vectone.com";
                mailbcc.Add(new MailAddress(finance, "financeteam"));
                mailbcc.Add(new MailAddress(csteam, "csteam"));
                mailbcc.Add(new MailAddress(autouser, "Auto"));
                bool emailSent = CCPaymentConsole.email.Send(true, mailFrom, mailTo, null, mailbcc,
                    mailSubject,
                    mailContent);
                return true;
            }

            catch (Exception ex)
            {

                return false;
            }
        }
    }
}