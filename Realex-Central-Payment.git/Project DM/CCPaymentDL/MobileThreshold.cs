﻿/*
 * Author       : Herdianto Suseno (2010-08-30)
 * Description  : Represents the mobile number threshold information.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace CCPaymentDL
{
    /// <summary>
    /// Represents the mobile number threshold information.
    /// </summary>
    public class MobileThreshold
    {
        #region properties

        private int _isAutoTopup;
        private string _message;
        private double _balance;
        private double _topupThreshold;

        public int IsAutoTopup
        {
            get { return _isAutoTopup; }
            set { _isAutoTopup = value; }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public double Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        public double TopupThreshold
        {
            get { return _topupThreshold; }
            set { _topupThreshold = value; }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of CCPaymentDL.MobileThreshold
        /// </summary>
        public MobileThreshold() { }

        /// <summary>
        /// Specifies how the row in the result set mapped into the typed data.
        /// </summary>
        public static void DataMapper<T>(IDataReader dataReader, ref T result)
        {
            DataTable dataSchema = dataReader.GetSchemaTable();
            MobileThreshold newData = (result as MobileThreshold);

            foreach (DataRow schemaInfo in dataSchema.Rows)
            {
                string colName = schemaInfo["columnname"].ToString();
                if (dataReader[colName] != null)
                {
                    switch (colName.ToLower())
                    {
                        case "is_autotopup": newData.IsAutoTopup = int.Parse(dataReader[colName].ToString()); break;
                        case "message": newData.Message = dataReader[colName].ToString(); break;
                        case "balance": newData.Balance = double.Parse(dataReader[colName].ToString()); break;
                        case "topup_threshold": newData.TopupThreshold = double.Parse(dataReader[colName].ToString()); break;
                        default: break;
                    }
                }
            }
        }

        /// <summary>
        /// Generates a strongly types list of typed data from a result set.
        /// </summary>
        public static void GenerateList<T>(IDataReader dataReader, ref List<T> result)
        {
            while (dataReader.Read())
            {
                MobileThreshold newData = new MobileThreshold();
                DataMapper<MobileThreshold>(dataReader, ref newData);
                (result as List<MobileThreshold>).Add(newData);
            }
        }
    }
}
