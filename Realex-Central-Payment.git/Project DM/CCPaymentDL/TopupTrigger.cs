﻿/*
 * Author       : Herdianto Suseno (2011-04-19)
 * Description  : Represents the topup trigger information.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace CCPaymentDL
{
    /// <summary>
    /// Represents the topup trigger information.
    /// </summary>
    public class TopupTrigger
    {
        #region properties

        private int _transid;
        private string _mobileno;

        public int TransId
        {
            get { return _transid; }
            set { _transid = value; }
        }

        public string MobileNo
        {
            get { return _mobileno; }
            set { _mobileno = value; }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of CCPaymentDL.TopupTrigger
        /// </summary>
        public TopupTrigger()
        {
            _transid = -1;
            _mobileno = string.Empty;
        }

        /// <summary>
        /// Specifies how the row in the result set mapped into the typed data.
        /// </summary>
        public static void DataMapper<T>(IDataReader dataReader, ref T result)
        {
            DataTable dataSchema = dataReader.GetSchemaTable();
            TopupTrigger newData = (result as TopupTrigger);

            foreach (DataRow schemaInfo in dataSchema.Rows)
            {
                string colName = schemaInfo["columnname"].ToString();
                if (dataReader[colName] != null)
                {
                    switch (colName.ToLower())
                    {
                        case "transid": newData.TransId = int.Parse(dataReader[colName].ToString()); break;
                        case "mobileno": newData.MobileNo = dataReader[colName].ToString(); break;
                        default: break;
                    }
                }
            }
        }

        /// <summary>
        /// Generates a strongly types list of typed data from a result set.
        /// </summary>
        public static void GenerateList<T>(IDataReader dataReader, ref List<T> result)
        {
            while (dataReader.Read())
            {
                TopupTrigger newData = new TopupTrigger();
                DataMapper<TopupTrigger>(dataReader, ref newData);
                (result as List<TopupTrigger>).Add(newData);
            }
        }
    }
}
