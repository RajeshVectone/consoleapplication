﻿/*
 * Author       : Herdianto Suseno (2010-08-30)
 * Description  : Represents the mobile number subscription information.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace CCPaymentDL
{
    /// <summary>
    /// Represents the mobile number subscription information.
    /// </summary>
    public class AuxAutoTopup
    {
        #region Properties

        private string _mobileno;
        private string _subscriptionID;
        private double _topupAmount;
        private string _topupCurr;
        private int _status;
        private DateTime _lastUpdate;
        private long _lastOrderID;
        private int _productID;
        private string _site;

        public string MobileNo
        {
            get { return _mobileno; }
            set { _mobileno = value; }
        }

        public string SubscriptionID
        {
            get { return _subscriptionID; }
            set { _subscriptionID = value; }
        }

        public double TopupAmount
        {
            get { return _topupAmount; }
            set { _topupAmount = value; }
        }

        public string TopupCurr
        {
            get { return _topupCurr; }
            set { _topupCurr = value; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public DateTime LastUpdate
        {
            get { return _lastUpdate; }
            set { _lastUpdate = value; }
        }

        public long LastOrderID
        {
            get { return _lastOrderID; }
            set { _lastOrderID = value; }
        }

        public int ProductID
        {
            get { return _productID; }
            set { _productID = value; }
        }

        public string Site
        {
            get { return _site; }
            set { _site = value; }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of CCPaymentDL.AuxAutoTopup
        /// </summary>
        public AuxAutoTopup() { }

        /// <summary>
        /// Specifies how the row in the result set mapped into the typed data.
        /// </summary>
        public static void DataMapper<T>(IDataReader dataReader, ref T result)
        {
            DataTable dataSchema = dataReader.GetSchemaTable();
            AuxAutoTopup newData = (result as AuxAutoTopup);

            foreach (DataRow schemaInfo in dataSchema.Rows)
            {
                string colName = schemaInfo["columnname"].ToString();
                if (dataReader[colName] != null && !dataReader[colName].Equals(DBNull.Value))
                {
                    switch (colName.ToLower())
                    {
                        case "mobileno": newData.MobileNo = dataReader[colName].ToString(); break;
                        case "subscriptionid": newData.SubscriptionID = dataReader[colName].ToString(); break;
                        case "topupamount": newData.TopupAmount = double.Parse(dataReader[colName].ToString()); break;
                        case "topupcurr": newData.TopupCurr = dataReader[colName].ToString(); break;
                        case "status": newData.Status = int.Parse(dataReader[colName].ToString()); break;
                        case "lastupdate": newData.LastUpdate = Convert.ToDateTime(dataReader[colName]); break;
                        case "lastorderid": newData.LastOrderID = long.Parse(dataReader[colName].ToString()); break;
                        case "productid": newData.ProductID = int.Parse(dataReader[colName].ToString()); break;
                        case "site": newData.Site = dataReader[colName].ToString(); break;
                        default: break;
                    }
                }
            }
        }

        /// <summary>
        /// Generates a strongly types list of typed data from a result set.
        /// </summary>
        public static void GenerateList<T>(IDataReader dataReader, ref List<T> result)
        {
            while (dataReader.Read())
            {
                AuxAutoTopup newData = new AuxAutoTopup();
                DataMapper<AuxAutoTopup>(dataReader, ref newData);
                (result as List<AuxAutoTopup>).Add(newData);
            }
        }
    }
}
