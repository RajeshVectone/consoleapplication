/*
 * Author       : Herdianto Suseno (2010-08-30)
 * Description  : Represents the payment status information.
 */

using System;
using System.Collections.Generic;
using System.Text;
using CyberSource.Clients.SoapWebReference;
using System.Data;
using System.IO;

namespace CCPaymentDL
{
    /// <summary>
    /// Represents the payment status information.
    /// </summary>
    public class PaymentStatus
    {
        private DataTable _tblReasonCodes;
        private void Generate_ReasonCodesSchema()
        {
            string appDataLocation = @"D:\Didit's Documents\Projects\CC Payment On-Demand\.Source Code 2.0\CCPaymentConsole\App_Data\";

            DataSet ds = new DataSet("Reasons");
            DataTable dt = new DataTable("ReasonData");
            ds.Tables.Add(dt);

            dt.Columns.Add("code", typeof(System.Int32));
            dt.Columns.Add("description", typeof(System.String));
            DataRow newRow = dt.NewRow();
            newRow["code"] = 100;
            newRow["description"] = "Successful transaction.";
            dt.Rows.Add(newRow);

            ds.WriteXmlSchema(string.Format("{0}ReasonCodes.xsd", appDataLocation));
            ds.WriteXml(string.Format("{0}ReasonCodes_Sample.xml", appDataLocation));
        }

        private string _refcode = string.Empty;
        private string _subscriptionID = string.Empty;
        private string _decision = string.Empty;
        private string _requestID = string.Empty;
        private string _currency = string.Empty;
        private double _amount = 0;
        private int _reasonCode = -1;
        private string _authorizationCode = string.Empty;
        private string _requestTime = string.Empty;
        private string _reconciliationID = string.Empty;
        private string _reasonDescription = string.Empty;

        private string GetReasonDescription(int code)
        {
            if (_tblReasonCodes != null)
            {
                DataRow rowFind = _tblReasonCodes.Rows.Find(code);
                if (rowFind != null)
                    return rowFind["description"].ToString();
            }
            return "";
        }

        /// <summary>
        /// Initializes a new instance of CCPaymentDL.PaymentStatus
        /// </summary>
        /// <param name="reply"></param>
        public PaymentStatus(RequestMessage request, ReplyMessage reply)
        {
            #region Generate CyberSource error description table
            try
            {
                if (_tblReasonCodes == null)
                {
                    string rcLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                    DataSet ds = new DataSet();
                    ds.ReadXmlSchema(string.Format(@"{0}\App_Data\ReasonCodes.xsd", rcLocation));
                    ds.ReadXml(string.Format(@"{0}\App_Data\ReasonCodes.xml", rcLocation));

                    _tblReasonCodes = ds.Tables["ReasonData"];
                }
            }
            catch (Exception) { }
            #endregion

            if (request != null)
            {
                _refcode = request.merchantReferenceCode ?? string.Empty;
                if (request.recurringSubscriptionInfo != null)
                {
                    _subscriptionID = request.recurringSubscriptionInfo.subscriptionID ?? string.Empty;
                    _decision = reply.decision ?? string.Empty;
                    _requestID = reply.requestID ?? string.Empty;
                }
                if (request.purchaseTotals != null)
                {
                    _currency = request.purchaseTotals.currency ?? string.Empty;
                    _amount = double.Parse(request.purchaseTotals.grandTotalAmount ?? "0");
                }
            }

            if (reply != null)
            {
                _reasonCode = int.Parse(reply.reasonCode ?? "-1");
                if (reply.ccAuthReply != null)
                {
                    _authorizationCode = reply.ccAuthReply.authorizationCode ?? string.Empty;
                }
                if (reply.ccCaptureReply != null)
                {
                    _requestTime = reply.ccCaptureReply.requestDateTime ?? string.Empty;
                    _reconciliationID = reply.ccCaptureReply.reconciliationID;
                }
            }

            switch (_reasonCode)
            {
                case 101:
                    StringBuilder missingFields = new StringBuilder();
                    foreach (string item in reply.missingField)
                        missingFields.AppendFormat(", {0}", item);
                    _reasonDescription = string.Format("The following required field(s) are missing: {0}", missingFields.ToString());
                    break;
                case 102:
                    StringBuilder invalidField = new StringBuilder();
                    if (reply.invalidField != null)
                    {
                        foreach (string item in reply.invalidField)
                            invalidField.AppendFormat(", {0}", item);
                    }
                    _reasonDescription = string.Format("The following field(s) are invalid: {0}", invalidField.ToString());
                    break;
                default:
                    _reasonDescription = GetReasonDescription(_reasonCode);
                    break;
            }
        }

        /// <summary>
        /// Gets merchant's reference code
        /// </summary>
        public string ReferenceCode { get { return _refcode; } }

        /// <summary>
        /// Gets the Subscription ID.
        /// </summary>
        public string SubscriptionID { get { return _subscriptionID; } }

        /// <summary>
        /// Gets the payment status decision.
        /// </summary>
        public string Decision { get { return _decision; } }

        /// <summary>
        /// Gets the request ID.
        /// </summary>
        public string RequestID { get { return _requestID; } }

        /// <summary>
        /// Gets the payment currency.
        /// </summary>
        public string Currency { get { return _currency; } }

        /// <summary>
        /// Gets the payment amount.
        /// </summary>
        public double Amount { get { return _amount; } }

        /// <summary>
        /// Gets the payment status decision code.
        /// </summary>
        public int ReasonCode { get { return _reasonCode; } }

        /// <summary>
        /// Gets the authorization code.
        /// </summary>
        public string AuthorizationCode { get { return _authorizationCode; } }

        /// <summary>
        /// Gets the payment requested time.
        /// </summary>
        public string RequestTime { get { return _requestTime; } }

        /// <summary>
        /// Gets the capture reply reconciliation ID.
        /// </summary>
        public string ReconciliationID { get { return _reconciliationID; } }

        /// <summary>
        /// Gets the payment status description.
        /// </summary>
        public string ReasonDescription { get { return _reasonDescription; } }

        public override string ToString()
        {
            return "\nRequestId : " + RequestID +
                   "\nRequestTime : " + RequestTime +
                   "\nReferenceCode : " + ReferenceCode +
                   "\nSubscriptionID : " + SubscriptionID +
                   "\nDecision : " + Decision +
                   "\nAmount : " + Amount +
                   "\nCurrency : " + Currency +
                   "\nAuthorizationCode : " + AuthorizationCode +
                   "\nReconciliationID : " + ReconciliationID +
                   "\nReasonCode : " + ReasonCode +
                   "\nReasonDescription : " + ReasonDescription;
        }
    }
}
