/*
 * Author       : Herdianto Suseno (2010-08-30)
 * Description  : Represents an operation status.
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace CCPaymentDL
{
    /// <summary>
    /// Represents an operation status.
    /// </summary>
    public class StatusData
    {
        private int _errCode;
        private string _errMessage;

        /// <summary>
        /// Initializes a new instance of CCPaymentDL.StatusData.
        /// </summary>
        public StatusData(int errCode, string errMessage)
        {
            _errCode = errCode;
            _errMessage = errMessage;
        }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        public int ErrorCode { get { return _errCode; } }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        public string ErrorMessage { get { return _errMessage; } }

        public override string ToString()
        {
            return "\nCode : " + ErrorCode + "\nMessage : " + ErrorMessage;
        }
    }
}
