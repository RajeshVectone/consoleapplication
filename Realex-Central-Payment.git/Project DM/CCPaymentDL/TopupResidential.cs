﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace CCPaymentDL
{
    /// <summary>
    /// Represents the topup residential status.
    /// </summary>
    public class TopupResidentialStatus
    {
        #region Properties

        private int _id;
        private string _mobileNo;
        private double _newBalance;
        private string _errorMessage;
        private int _errorCode;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string MobileNo
        {
            get { return _mobileNo; }
            set { _mobileNo = value; }
        }

        public double NewBalance
        {
            get { return _newBalance; }
            set { _newBalance = value; }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        public int ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of CCPaymentDL.TopupResidential
        /// </summary>
        public TopupResidentialStatus() { }

        /// <summary>
        /// Specifies how the row in the result set mapped into the typed data.
        /// </summary>
        public static void DataMapper<T>(IDataReader dataReader, ref T result)
        {
            DataTable dataSchema = dataReader.GetSchemaTable();
            TopupResidentialStatus newData = (result as TopupResidentialStatus);

            foreach (DataRow schemaInfo in dataSchema.Rows)
            {
                string colName = schemaInfo["columnname"].ToString();
                if (dataReader[colName] != null && !dataReader[colName].Equals(DBNull.Value))
                {
                    switch (colName.ToLower())
                    {
                        case "id": newData.ID = int.Parse(dataReader[colName].ToString()); break;
                        case "mobileno": newData.MobileNo = dataReader[colName].ToString(); break;
                        case "newbalance": newData.NewBalance = double.Parse(dataReader[colName].ToString()); break;
                        case "errormessage": newData.ErrorMessage = dataReader[colName].ToString(); break;
                        case "errorcode": newData.ErrorCode = int.Parse(dataReader[colName].ToString()); break;
                        default: break;
                    }
                }
            }
        }

        /// <summary>
        /// Generates a strongly types list of typed data from a result set.
        /// </summary>
        public static void GenerateList<T>(IDataReader dataReader, ref List<T> result)
        {
            while (dataReader.Read())
            {
                TopupResidentialStatus newData = new TopupResidentialStatus();
                DataMapper<TopupResidentialStatus>(dataReader, ref newData);
                (result as List<TopupResidentialStatus>).Add(newData);
            }
        }
    }
}
