using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using CCPaymentBL;
using CCPaymentDL;

namespace CCPaymentMonitor
{
    public partial class FrmMain : Form
    {
        private bool processRun;
        private Thread daemonThread;

        private delegate void SetStatus(string status);

        public FrmMain()
        {
            InitializeComponent();
        }

        #region Daemon process

        /// <summary>
        /// The daemon main process
        /// </summary>
        private void daemonProcess()
        {
            while (processRun)
            {
                try
                {
                    // Check if auto-topup mobile number exists
                    string autoTopupMobile = Topup.GetAutoTopupMobile() ?? string.Empty;
                    if (autoTopupMobile != string.Empty)
                    {
                        Console.WriteLine(string.Format("Auto-topup mobile number found: {0}", autoTopupMobile));

                        // Start auto payment topup thread process
                        Thread autoPaymentTopupThread = new Thread(new ParameterizedThreadStart(DoPaymentTopupProcess));
                        autoPaymentTopupThread.Start(autoTopupMobile);
                    }
                }
                catch (Exception ex) { safeSetStatus((ex.InnerException ?? ex).Message); }

                // Sleep, reduce system resource usage
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// The payment and topup process
        /// </summary>
        private void DoPaymentTopupProcess(object mobileNo)
        {
            string autoTopupMobile = (string)mobileNo;

            // Threshold the mobile's balance
            MobileThreshold threshold = Topup.GetMobileThreshold(autoTopupMobile);
            if (threshold == null) Console.WriteLine("Threshold information not found <{0}>", autoTopupMobile);
            else if (threshold.Balance > threshold.TopupThreshold) Console.WriteLine("Account balance is enough <{0}>", autoTopupMobile);
            else
            {
                // Get CyberSource subscription info for the mobile number
                AuxAutoTopup TopupInfo = Topup.GetTopupInfo(autoTopupMobile);
                if (TopupInfo == null) Console.WriteLine("Subscription information not found <{0}>", autoTopupMobile);
                else
                {
                    // Get the transaction reference code for payment
                    int referenceCode = CyberSourceSOAP.RegisterPayment(autoTopupMobile, TopupInfo.SubscriptionID, TopupInfo.TopupCurr, threshold.Balance, TopupInfo.TopupAmount);
                    if (referenceCode == null || referenceCode == -1) Console.WriteLine("Cannot register payment <{0}>", autoTopupMobile);
                    else
                    {
                        // Do payment
                        PaymentStatus paymentStatus = CyberSourceSOAP.Payment(referenceCode, TopupInfo.SubscriptionID, TopupInfo.TopupCurr, TopupInfo.TopupAmount.ToString());
                        if (paymentStatus == null) Console.WriteLine("Payment can not be completed <{0}>", autoTopupMobile);
                        else if ("ACCEPT".Equals(paymentStatus.Decision.ToUpper()))
                        {
                            Console.WriteLine("Payment suceeded <{0}>\n", autoTopupMobile);

                            // Do topup
                            StatusData topupStatus = Topup.TopupMobile(autoTopupMobile, referenceCode, TopupInfo.ProductID, TopupInfo.TopupAmount, TopupInfo.TopupCurr);
                            if (topupStatus.ErrorCode == 0) Console.WriteLine("Topup mobile succeeded <{0}>\n", autoTopupMobile);
                            else
                            {
                                Console.WriteLine("Topup failed <{0}>", autoTopupMobile);
                                Console.WriteLine("{0}\n", topupStatus.ErrorMessage);
                            }
                        }
                        else if (paymentStatus != null && "REJECT".Equals(paymentStatus.Decision.ToUpper()))
                        {
                            // unscubscribe mobile if payment rejected
                            CyberSourceSOAP.MobileUnSubscribe(autoTopupMobile);
                            Console.WriteLine("Payment rejected <{0}>\n", autoTopupMobile);
                        }
                    }
                }
            }
        }

        #endregion

        private void safeSetStatus(string status)
        {
            if (this.statusStrip1.InvokeRequired)
            {
                SetStatus setStatus = new SetStatus(safeSetStatus);
                this.Invoke(setStatus, new object[] { status });
            }
            else lblStatus.Text = string.Format("Status: {0}", status);
        }

        private void EnableDaemon()
        {
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            safeSetStatus("Status: Online");
            processRun = true;
        }

        private void DisableDaemon()
        {
            processRun = false;
            if (daemonThread != null) daemonThread.Join();

            btnStart.Enabled = true;
            btnStop.Enabled = false;
            safeSetStatus("Status: Offline");
            processRun = false;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            EnableDaemon();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            DisableDaemon();
        }
    }
}
