﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayerAuth.aspx.cs" Inherits="WebApplication1.PayerAuth" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function closeWin() {
            window.close();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField runat="server" ID="PaReq" />
        <asp:HiddenField runat="server" ID="TermUrl" />
        
        <asp:Label runat="server" ID="lblResult" Text="Please wait..." /><br />
        <asp:Button runat="server" ID="btnClose" Text="Close" Visible="false" OnClientClick="return closeWin()" />
    </div>
    </form>
</body>
</html>
