﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="pnlInput">
            <table cellpadding="0" cellspacing="0" border="0" style="text-align: right;">
                <tr><td style="padding-right: 4px;">Card Number:</td><td><asp:TextBox runat="server" ID="txtCCNo" Text="4000000000000002" /></td></tr>
                <tr><td style="padding-right: 4px;">Expired Date:</td><td><asp:TextBox runat="server" ID="txtExpDate" Text="201212" /></td></tr>
                <tr><td style="padding-right: 4px;">First Name:</td><td><asp:TextBox runat="server" ID="txtFirstName" Text="John" /></td></tr>
                <tr><td style="padding-right: 4px;">Last Name:</td><td><asp:TextBox runat="server" ID="txtLastName" Text="Smith" /></td></tr>
                <tr><td style="padding-right: 4px;">Street:</td><td><asp:TextBox runat="server" ID="txtStreetName" Text="Orchid Street" /></td></tr>
                <tr><td style="padding-right: 4px;">City:</td><td><asp:TextBox runat="server" ID="txtCity" Text="London" /></td></tr>
                <tr><td style="padding-right: 4px;">Country:</td><td><asp:TextBox runat="server" ID="txtCountry" Text="UK" /></td></tr>
                <tr><td style="padding-right: 4px;">Email:</td><td><asp:TextBox runat="server" ID="txtEmail" Text="tester@switchlab.com" /></td></tr>
                <tr><td style="padding-right: 4px;">Amount:</td><td><asp:TextBox runat="server" ID="txtAmount" Text="20.00" /></td></tr>
                <tr><td style="padding-right: 4px;">Currency:</td><td><asp:TextBox runat="server" ID="txtCurrency" Text="GBP" /></td></tr>
                <tr><td colspan="2" style="border-bottom: solid 1px black; padding-bottom: 8px;"><asp:Button runat="server" ID="btnSubmit" Text="Submit" /></td></tr>
            </table>
        </asp:Panel>
        <asp:HiddenField runat="server" ID="PaReq" />
        <asp:HiddenField runat="server" ID="TermUrl" />
        <table runat="server" id="pnlConfirm" visible="false" cellpadding="0" cellspacing="0" border="0" style="text-align: right; margin-top: 8px;">
            <tr><td nowrap valign="top" style="padding-right: 4px">Decision:</td><td style="text-align: left;"><asp:Label runat="server" ID="lbldecision" /></td></tr>
            <tr><td nowrap valign="top" style="padding-right: 4px">Reference ID:</td><td style="text-align: left;"><asp:Label runat="server" ID="lblmerchantReferenceCode" /></td></tr>
            <tr><td nowrap valign="top" style="padding-right: 4px">Reason Code:</td><td style="text-align: left;"><asp:Label runat="server" ID="lblreasonCode" /></td></tr>
            <tr><td nowrap valign="top" style="padding-right: 4px">Description:</td><td style="text-align: left;"><asp:Label runat="server" ID="lbldescription" /></td></tr>
            <tr><td nowrap valign="top" style="padding-right: 4px">Request ID:</td><td style="text-align: left;"><asp:Label runat="server" ID="lblrequestID" /></td></tr>
            <tr><td nowrap valign="top" style="padding-right: 4px">Request Token:</td><td style="text-align: left;"><asp:Label runat="server" ID="lblrequestToken" /></td></tr>
            <tr><td nowrap valign="top" style="padding-right: 4px">PaReq:</td><td style="text-align: left;"><asp:Label runat="server" ID="lblPaReq" /></td></tr>
        </table>
    </form>
</body>
</html>
