﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using CP.Service.Client;

namespace WebApplication1
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack && Request["step"] == null)
            {
                // run authorization service
                NameValueCollection cpParams = new NameValueCollection();
                cpParams.Add("site-code", "MCM");
                cpParams.Add("product-code", "TP20");
                cpParams.Add("payment-agent", "IVR");
                cpParams.Add("service-type", "RES");
                cpParams.Add("payment-mode", "SUB");
                cpParams.Add("account-id", "447514719202");
                cpParams.Add("cc-no", txtCCNo.Text);
                cpParams.Add("exp-date", txtExpDate.Text);
                cpParams.Add("first-name", txtFirstName.Text);
                cpParams.Add("last-name", txtLastName.Text);
                cpParams.Add("street-name", txtStreetName.Text);
                cpParams.Add("city", txtCity.Text);
                cpParams.Add("country", txtCountry.Text);
                cpParams.Add("email", txtEmail.Text);
                cpParams.Add("amount", txtAmount.Text);
                cpParams.Add("currency", txtCurrency.Text);
                cpParams.Add("check-enroll", "1");
                TransactionReply trxAuth = Enrollment.Authorization(cpParams, "http://localhost/cpweb/payerauth.aspx?step=2");
                Session["trxAuth"] = trxAuth;

                if (trxAuth != null && trxAuth["ReasonCode"] != null && trxAuth["ReasonCode"].Equals("475"))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "popupPayerAuth",
                        "popupPayerAuth = window.open('payerauth.aspx?step=1', 'popupPayerAuth', 'left=100, top=120, width=600,height=400');", true);
                }

                // display transaction result
                pnlConfirm.Visible = true;
                pnlInput.Enabled = false;
                btnSubmit.Enabled = false;
                lbldecision.Text = trxAuth["Decision"];
                lblmerchantReferenceCode.Text = trxAuth["MerchantReferenceCode"];
                lblreasonCode.Text = trxAuth["ReasonCode"];
                lbldescription.Text = trxAuth["Description"];
                lblrequestID.Text = trxAuth["RequestID"];
                lblrequestToken.Text = trxAuth["RequestToken"];
                lblPaReq.Text = trxAuth["PaReq"];
            }
        }
    }
}
