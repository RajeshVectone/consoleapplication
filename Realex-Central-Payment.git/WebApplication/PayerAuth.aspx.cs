﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CP.Service.Client;
using System.Collections.Specialized;

namespace WebApplication1
{
    public partial class PayerAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TransactionReply trxAuth = (TransactionReply)Session["trxAuth"];

            // payer authentication step
            if (trxAuth != null && Request["step"] == "1")
            {
                form1.Action = trxAuth["AcsURL"];
                PaReq.Value = trxAuth["PaReq"];
                TermUrl.Value = trxAuth["TermUrl"];
                ClientScript.RegisterStartupScript(this.GetType(), "submit", "form1.submit()", true);
            }

            // term page
            if (trxAuth != null && Request["step"] == "2")
            {
                NameValueCollection cpParams = new NameValueCollection();
                cpParams.Add("site-code", trxAuth.Request["site-code"]);
                cpParams.Add("product-code", trxAuth.Request["product-code"]);
                cpParams.Add("reference-id", trxAuth["MerchantReferenceCode"]);
                cpParams.Add("account-id", trxAuth.Request["account-id"]);
                cpParams.Add("cc-no", trxAuth.Request["cc-no"]);
                cpParams.Add("exp-date", trxAuth.Request["exp-date"]);
                cpParams.Add("first-name", trxAuth.Request["first-name"]);
                cpParams.Add("last-name", trxAuth.Request["last-name"]);
                cpParams.Add("street-name", trxAuth.Request["street-name"]);
                cpParams.Add("city", trxAuth.Request["city"]);
                cpParams.Add("country", trxAuth.Request["country"]);
                cpParams.Add("email", trxAuth.Request["email"]);
                cpParams.Add("pares", Request["paRes"]);
                cpParams.Add("capture", "0");
                TransactionReply trxValidate = Enrollment.Validate(cpParams);

                lblResult.Text = trxValidate.Response["Description"];
                btnClose.Visible = true;
            }
        }
    }
}
