﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CP.DataType;
using CyberSource.Clients.SoapWebReference;
using CP.BusinessLogic.CyberSource;
using CP.BusinessLogic.DB;
using System.Web;
using CP.BusinessLogic.DB.Reference;
using System.Xml;
using CP.BusinessLogic.RealexPayments.RealAuth;
using System.Data.SqlClient;


namespace CP.BusinessLogic.Transactionm
{
    public class Enrollment
    {
        /// <summary>
        /// Enrollment authorization.
        /// </summary>
        /// 

        /***
         * 
         * 
         * 
         *  query["site-code"],
         *  query["application-code"],
         *  query["product-code"],
         *  query["payment-agent"],
         *  query["service-type"],
         *  query["payment-mode"],
         *  query["payer-refer"],
         *  query["payment-method-refer"],
         *  query["amount"],
         *  query["currency"],
         *  query["client-ip"]
         * 
         * 
         * 
         * 
         */

        public static TransactionReply ReceiptIn(
            string siteCode,
            string applicationCode,
            string productCode,
            string paymentAgent,
            string serviceType,
            string paymentMode,
            string amount,
            string currency,
            string clientIP,
            string country,
            string accountId,
            string referenceCode,
            string subscriptionId,
            // newly addd on  03/02/2016.
            string email, string actualmobileno
            //13-Jul-2018 : Added for cvn
            , string cvn
            )
        {
            Log.Write("ReceiptIn()");
            TransactionReply result = new TransactionReply();

            string cannotContinue = "The authorization process completed but the operation cannot continue due to error: {0}";
            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            csTransaction transaction;
            string referenceId;
            string payerref;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */

                if (siteCode == "LO2")
                {
                    if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                    if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                    if (string.IsNullOrEmpty(referenceCode)) missingParam.Add("reference-code");
                    if (string.IsNullOrEmpty(paymentAgent)) missingParam.Add("payment-agent");
                    if (string.IsNullOrEmpty(serviceType)) missingParam.Add("service-type");
                    if (string.IsNullOrEmpty(paymentMode)) missingParam.Add("payment-mode");
                    if (string.IsNullOrEmpty(amount)) missingParam.Add("amount");
                    if (string.IsNullOrEmpty(currency)) missingParam.Add("currency");
                    if (string.IsNullOrEmpty(accountId)) missingParam.Add("accountId");
                    if (string.IsNullOrEmpty(subscriptionId)) missingParam.Add("subscriptionId");
                    // for Email not display in realex issue 03/02/2016
                    //if (string.IsNullOrEmpty(email)) missingParam.Add("email");
                }
                else
                {
                    if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                    if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                    if (string.IsNullOrEmpty(referenceCode)) missingParam.Add("reference-code");
                    if (string.IsNullOrEmpty(paymentAgent)) missingParam.Add("payment-agent");
                    if (string.IsNullOrEmpty(serviceType)) missingParam.Add("service-type");
                    if (string.IsNullOrEmpty(paymentMode)) missingParam.Add("payment-mode");
                    if (string.IsNullOrEmpty(amount)) missingParam.Add("amount");
                    if (string.IsNullOrEmpty(currency)) missingParam.Add("currency");
                    if (string.IsNullOrEmpty(country)) missingParam.Add("country");
                    if (string.IsNullOrEmpty(accountId)) missingParam.Add("accountId");
                    if (string.IsNullOrEmpty(subscriptionId)) missingParam.Add("subscriptionId");
                    // for Email not display in realex issue 03/02/2016
                    //if (string.IsNullOrEmpty(email)) missingParam.Add("email");
                }

                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion
                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                payerref = accountId;
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion

                #region Get IP Payment Agent

                string IpPaymentAgent = HttpContext.Current.Request.UserHostAddress;

                #endregion


                #region Validate transaction information
                /*
                 * REF_PAYMENT_STEP   = 1:Begin Process Payment
                 * REF_PAYMENT_STATUS = 0: New
                 * REF_REASON_CODE    = -10:Insert payment data for new payment failed
                 */
                int retError = 0;
                transaction = new csTransaction();
                try
                {
                    referenceId = transaction.InsertReceiptInData(
                        siteCode,
                        applicationCode,
                        productCode,
                        paymentAgent,
                        serviceType,
                        paymentMode,
                        16,
                        0,
                        payerref,
                        Convert.ToDouble(amount),
                        siteCode.Equals("FR1") ? "EUR" : currency,
                        //string.Empty,
                        subscriptionId,
                        routing.MerchantID,
                        routing.ProviderCode,
                        "100",
                        0,
                        ref retError,
                        IpPaymentAgent,
                        clientIP,
                        referenceCode)
                    .ToString();
                }
                catch (Exception ex)
                {
                    throw new PaymentException(-10,
                        string.Format("Insert payment data failed. {0}", (ex.InnerException ?? ex).Message));
                }
                #endregion


                /*
                 * Get Product Info
                 */
                string productSKU1 = string.Format("{0}-{1}", siteCode, productCode);
                string productSKU2 = string.Empty;
                try
                {
                    string[] part_referenceId = referenceId.Split("-".ToCharArray(), 3);
                    productSKU2 = string.Format("{0}-{1}", part_referenceId[0], part_referenceId[1]);
                }
                catch (Exception) { }
                ProductSKUInfo infoSKU = ProductSKU.GetInfo(productSKU1, productSKU2);

                try
                {
                    BillTo billTo = new BillTo();
                    billTo.email = email;
                    billTo.ipAddress = string.IsNullOrEmpty(clientIP) ? HttpContext.Current.Request.UserHostAddress : clientIP;

                    //05-Dec-2017 : Moorthy : Modified to display the mobile no in product id (Realex)
                    //billTo.customerID = accountId;
                    if (actualmobileno != null)
                        billTo.customerID = (accountId == "FREE SIM" ? actualmobileno : accountId);
                    else
                        billTo.customerID = accountId;

                    // Product Info
                    List<Item> items = new List<Item>();
                    items.Add(new Item()
                    {
                        productName = infoSKU.ItemDescription,
                        unitPrice = infoSKU.ItemPrice.ToString(),
                        taxAmount = infoSKU.ItemVAT.ToString(),
                        productSKU = infoSKU.ProductSKU,
                        quantity = "1"
                    });
                    // Payment info
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.grandTotalAmount = amount;
                    payment.currency = siteCode.Equals("FR1") ? "EUR" : currency; // TODO: <Developer> Hardcoded to EUR currency if the request from Chillitalk France (sitecode = FR1)

                    // Run transaction
                    RealExPayment soap = new RealExPayment();


                    #region Get provider error description

                    #endregion
                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status/475:The customer is enrolled in Payer Authentication
                     * REF_PAYMENT_STEP   = 2:Enrolled/5:Authorize/7:Authorize-Capture
                     * REF_PAYMENT_STATUS = 1:In Process/3:Accepted/-1:Rejected/-2:Error)
                     */
                    try
                    {
                        int paymentStep;
                        int paymentStatus;
                        // Run transaction
                        result = soap.Enrollment_ReceiptIn(
                                billTo,
                                referenceId,
                                accountId,
                                payment,
                                routing.MerchantID,
                                applicationCode,
                                country,
                                subscriptionId, email
                            //13-Jul-2018 : Added for cvn
                                , cvn);

                        //transaction.FroudScore(referenceId, result.ReplyMessage.afsReply.afsResult);
                        //transaction.FroudScore(referenceId, );


                        //result.Description = results.ToString();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed calling realex service, " + (ex.InnerException ?? ex).Message;
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling realex service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                return result;
            }
            // Return the operation result
            return result;
        }


        /// <summary>
        /// The greek social id, uses the Luhn formula.<br />
        /// The last digit is the validation digit using the Luhn check digit algorithm.<ul><li>
        ///  1 - Counting from the check digit, which is the rightmost, and moving left, double the value of every second digit.</li><li>
        ///  2 - Sum the digits of the products (e.g., 10: 1 + 0 = 1, 14: 1 + 4 = 5) together with the undoubled digits from the original number.</li><li>
        ///  3 - If the total modulo 10 is equal to 0 (if the total ends in zero) then the number is valid according to the Luhn formula; else it is not valid.</li></ul>
        /// </summary>
        /// <param name="sidNum">The social id number in string</param>
        /// <returns>True if pass the Luhn validation, else false</returns>
        public static bool isCheckLuhn(string id)
        {
            int idLength = id.Length;
            int currentDigit;
            int idSum = 0;
            int currentProcNum = 0; //the current process number (to calc odd/even proc)

            for (int i = idLength - 1; i >= 0; i--)
            {
                //get the current rightmost digit from the string
                string idCurrentRightmostDigit = id.Substring(i, 1);

                //parse to int the current rightmost digit, if fail return false (not-valid id)
                if (!int.TryParse(idCurrentRightmostDigit, out currentDigit))
                    return false;

                //bouble value of every 2nd rightmost digit (odd)
                //if value 2 digits (can be 18 at the current case),
                //then sumarize the digits (made it easy the by remove 9)
                if (currentProcNum % 2 != 0)
                {
                    if ((currentDigit *= 2) > 9)
                        currentDigit -= 9;
                }
                currentProcNum++; //increase the proc number

                //summarize the processed digits
                idSum += currentDigit;
            }

            //if digits sum is exactly divisible by 10, return true (valid), else false (not-valid)
            return (idSum % 10 == 0);
        }

        public static TransactionReply Authorize(
             string siteCode, string applicationCode, string productCode, string paymentAgent, string serviceType, string paymentMode, string accountId,
             string ccNo, string ccType, string cvv, string expDate, string firstName, string lastName, string streetName, string city, string postCode, string state, string country, string email, string amount, string currency,
             string checkEnroll, string capture, string clientIP, string device_type, string browser, string os_version, string topup_url, int bundle_id)
        {
            Log.Write("Authorize()");
            TransactionReply result = new TransactionReply();
            TransactionReply resultSub = new TransactionReply();

            string cannotContinue = "The authorization process completed but the operation cannot continue due to error: {0}";
            //ccType = "Visa";
            //country = "UK";
            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            csTransaction transaction;
            string referenceId;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(paymentAgent)) missingParam.Add("payment-agent");
                if (string.IsNullOrEmpty(serviceType)) missingParam.Add("service-type");
                if (string.IsNullOrEmpty(paymentMode)) missingParam.Add("payment-mode");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (string.IsNullOrEmpty(ccNo)) missingParam.Add("cc-no");
                if (string.IsNullOrEmpty(ccType)) missingParam.Add("cc-type");
                //04-Oct-2017 : Added for MAESTRO dont require cvn value 
                if (ccType.ToLower() != "maestro" && string.IsNullOrEmpty(cvv)) missingParam.Add("cvv");
                if (string.IsNullOrEmpty(expDate) || expDate.Length != 6) missingParam.Add("exp-date");
                if (string.IsNullOrEmpty(firstName)) missingParam.Add("first-name");
                if (string.IsNullOrEmpty(lastName)) missingParam.Add("last-name");
                //02-Jul-2018 : Commented 
                //if (string.IsNullOrEmpty(streetName)) missingParam.Add("street-name");
                //if (string.IsNullOrEmpty(city)) missingParam.Add("city");
                //if (string.IsNullOrEmpty(postCode)) missingParam.Add("post-code");
                if (string.IsNullOrEmpty(country)) missingParam.Add("country");
                if (string.IsNullOrEmpty(email)) missingParam.Add("email");
                if (string.IsNullOrEmpty(amount)) missingParam.Add("amount");
                if (string.IsNullOrEmpty(currency)) missingParam.Add("currency");
                if (Array.IndexOf(ParamValues.FlagData, checkEnroll ?? "") == -1) missingParam.Add("check-enroll");
                if (Array.IndexOf(ParamValues.FlagData, capture ?? "") == -1) missingParam.Add("capture");
                //Log.Write("Card Type : " + ccType);
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion

                //24-Aug-2018 : Moorthy Added for Quick Top email validation
                string orgEmail = email;
                if (email.ToLower() == "quicktopup@vectone.com")
                    email = "";

                //17-Jan-2019 : Moorthy : Added to avoid Fraud customer. -- REF : Mail (Raja : Subject : Book43.xlsx)
                //if (email.ToUpper().StartsWith("GUEST"))
                //    throw new PaymentException(10 - 7, "Fails Fraud Checks - (mail id starts with 'guest')");

                if (ccType.ToUpper().Equals("AMEX"))
                {
                    if (cvv.Length != 4)
                        throw new PaymentException(509, "Security Code/CVV2/CVC must be 4 digits");
                }
                else
                {
                    if (cvv.Length != 3)
                        throw new PaymentException(509, "Security Code/CVV2/CVC must be 3 digits");
                }

                if (expDate.Substring(0, 4) == DateTime.Now.ToString("yyyy") && Convert.ToInt32(expDate.Substring(4)) < Convert.ToInt32(DateTime.Now.ToString("MM")))
                {
                    throw new PaymentException(509, "Card expiry year is too far into the past.");
                }

                switch (ccType.ToUpper())
                {
                    case ("VISA"):
                    case ("MC"):
                    case ("MASTERCARD"):
                    case ("MASTER"):
                    case ("MAESTRO"):
                    case ("AMEX"):
                    case ("LASER"):
                    case ("DINERS"):
                    case ("SWITCH"):
                    case ("SOLO"):
                    case ("JCB"):
                        break;
                    default:
                        throw new PaymentException(509, "Invalid credit card type specified.");
                }

                //27-Aug-2018 : Moorthy Added for 
                if (!isCheckLuhn(ccNo))
                {
                    throw new PaymentException(509, "Card number fails Luhn Check");
                }

                //TODO : Need to remove
                //if (applicationCode != null && applicationCode.ToUpper().Contains("DCSEK"))
                //    throw new PaymentException(507, "currency/card combination not allowed");

                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion

                #region Get IP Payment Agent
                //System.Web.HttpContext context = System.Web.HttpContext.Current;
                //string IpPaymentAgent = (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? context.Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();

                //string IpPaymentAgent = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList.GetValue(0).ToString();
                string IpPaymentAgent = HttpContext.Current.Request.UserHostAddress;

                #endregion



                #region Validate transaction information
                /*
                 * REF_PAYMENT_STEP   = 1:Begin Process Payment
                 * REF_PAYMENT_STATUS = 0: New
                 * REF_REASON_CODE    = -10:Insert payment data for new payment failed
                 */
                int retError = 0;
                transaction = new csTransaction();

                //22-Feb-2019 : Moorthy : Added for Payment Gateway Check Rule
                string ruleMsg = "";
                if (transaction.PaymentGatewayCheckRule(ccNo.Substring(ccNo.Length - 6, 6), Convert.ToDouble(amount), accountId, clientIP, ccType, applicationCode, ref ruleMsg) == "1")
                {
                    Log.Write("Chargeback failure - " + ruleMsg);
                    throw new PaymentException(-111, "Chargeback failure - " + ruleMsg);
                }

                try
                {
                    //06-Aug-2018 : Moorthy Added to avoid 3DS Enrolled duplicate reference
                    //referenceId = transaction.Validate3dsEnrolled(accountId, ccNo.Substring(ccNo.Length - 6, 6), siteCode, paymentAgent, serviceType, amount);
                    //if (String.IsNullOrEmpty(referenceId))
                    referenceId = transaction.InsertPaymentData(
                        siteCode,
                        applicationCode,
                        productCode,
                        paymentAgent,
                        serviceType,
                        paymentMode,
                        1,
                        0,
                        accountId,
                        ccNo.Substring(ccNo.Length - 6, 6),
                        Convert.ToDouble(amount),
                        siteCode.Equals("FR1") ? "EUR" : currency,
                        string.Empty,
                        routing.MerchantID,
                        routing.ProviderCode,
                        "100",
                        0,
                        ref retError,
                        orgEmail,
                        IpPaymentAgent,
                        clientIP,
                        "Payment Reference Created",
                        checkEnroll.Equals("0") ? "0" : "1",
                        expDate,
                        ccType,
                        device_type,
                        browser,
                        os_version,
                        topup_url,
                        bundle_id)
                    .ToString();
                    //else
                    //{
                    //    Log.Write("Already 3DS Enrolled : referenceId - " + referenceId);
                    //    checkEnroll = "0";
                    //}
                }
                catch (Exception ex)
                {
                    throw new PaymentException(-10,
                        string.Format("Insert payment data failed. {0}", (ex.InnerException ?? ex).Message));
                }
                #endregion

                /*
                 * Get Product Info
                 */
                string productSKU1 = string.Format("{0}-{1}", siteCode, productCode);
                string productSKU2 = string.Empty;
                try
                {
                    string[] part_referenceId = referenceId.Split("-".ToCharArray(), 3);
                    productSKU2 = string.Format("{0}-{1}", part_referenceId[0], part_referenceId[1]);
                }
                catch (Exception) { }
                ProductSKUInfo infoSKU = ProductSKU.GetInfo(productSKU1, productSKU2);

                /*
                 * Call Cybersource service
                 * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
                 * REF_PAYMENT_STATUS = 2:On Manual Review
                 */
                try
                {
                    // Card info
                    Card card = new Card();
                    card.fullName = firstName + lastName;
                    card.accountNumber = ccNo;
                    card.cardType = ccType;
                    card.cvNumber = cvv;
                    card.cvIndicator = "1";
                    card.expirationYear = expDate.Substring(0, 4);
                    card.expirationMonth = expDate.Substring(4, 2);

                    // Billing info
                    BillTo billTo = new BillTo();
                    billTo.firstName = firstName;
                    billTo.lastName = lastName;
                    billTo.street1 = streetName;
                    billTo.city = city;
                    billTo.postalCode = postCode;
                    billTo.state = string.IsNullOrEmpty(state) ? string.Empty : state;
                    billTo.country = country;
                    billTo.email = email;
                    billTo.ipAddress = string.IsNullOrEmpty(clientIP) ? HttpContext.Current.Request.UserHostAddress : clientIP;
                    billTo.customerID = accountId;


                    // Product Info
                    List<Item> items = new List<Item>();
                    items.Add(new Item()
                    {
                        productName = infoSKU.ItemDescription,
                        unitPrice = infoSKU.ItemPrice.ToString(),
                        taxAmount = infoSKU.ItemVAT.ToString(),
                        productSKU = infoSKU.ProductSKU,
                        quantity = "1"
                    });

                    // Payment info
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.grandTotalAmount = amount;
                    payment.currency = siteCode.Equals("FR1") ? "EUR" : currency; // TODO: <Developer> Hardcoded to EUR currency if the request from Chillitalk France (sitecode = FR1)

                    // Run transaction
                    RealExPayment soap = new RealExPayment();

                    //This the implementation with out 3DS
                    if (checkEnroll.Equals("0"))
                    {
                        Log.Write("with out 3DS");

                        //08-Mar-2019 : Moorthy : Added to avoid without 3DS transaction
                        result = new TransactionReply();
                        result.GeneralErrorCode = -1;
                        result.ReplyMessage = new ReplyMessage();
                        result.ReplyMessage.reasonCode = "-1";
                        result.ReplyMessage.decision = "Without 3DS Disabled";
                        result.Description = "Without 3DS Disabled";
                        result.ProviderMessage = "Without 3DS Disabled";

                        transaction.UpdatePaymentData(
                          referenceId,
                          1,
                          1,
                          routing.ProviderCode,
                          result.ReplyMessage.reasonCode,
                          null,
                          null,
                          -1,
                          "Without 3DS Disabled");

                        return result;

                        result = soap.Payment_Authorize(
                        productCode,
                        referenceId,
                        accountId,
                        card,
                        billTo,
                        payment,
                        capture.Equals("1"),
                        BusinessFacade.DecisionManager,
                        BusinessFacade.AVSIgnore,
                        BusinessFacade.CVNIgnore,
                        routing.MerchantID,
                        "5",
                        applicationCode,
                        country,
                        items.ToArray());
                        if ((result.ReplyMessage.reasonCode == "0" || result.ReplyMessage.reasonCode == "00") && (result.ReplyMessage.decision.ToUpper() == "APPROVAL" || result.ReplyMessage.decision.ToUpper() == "SUCCESSFULLY AUTHORISED" || result.ReplyMessage.decision.ToUpper() == "AUTHENTICATION SUCCESSFUL"))
                        {

                            result.GeneralErrorCode = 100;
                            result.ReplyMessage.reasonCode = "100";//verify with Dhashina
                            result.ReplyMessage.decision = "ACCEPT";
                            // result.ReplyMessage.reasonCode = reasonCode;
                            result.Description = "Transaction Succeeded."; //Please ask dhahina about the description ?
                            result.ProviderMessage = ""; //
                            //result.Cardref = resultSub.ReplyMessage.paySubscriptionCreateReply != null ? resultSub.ReplyMessage.paySubscriptionCreateReply.subscriptionID : "";

                            transaction.UpdatePaymentData(
                              referenceId,
                              1,
                              1,
                              routing.ProviderCode,
                              result.ReplyMessage.reasonCode,
                              null,
                              null,
                             0,
                              result.Description);

                            try
                            {
                                resultSub = soap.Subscription_Capture(
                                    referenceId,
                                    accountId,
                                    card,
                                    billTo,
                                    payment,
                                    routing.MerchantID,
                                    applicationCode,
                                    country);

                                //if (resultSub.ReplyMessage.reasonCode == "0")
                                //{
                                Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                                subscription.CreateSubscription(
                                    accountId,
                                    ccNo.Substring(ccNo.Length - 6, 6),
                                    resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID,
                                    expDate,
                                    DateTime.Now.ToString("yyyyMMdd"),
                                    routing.ProviderCode);
                                result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                                #region  c1
                                //} else
                                //{
                                //Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                                //subscription.ValidateSubscription(
                                //    accountId,
                                //    ccNo.Substring(ccNo.Length - 6, 6),
                                //    resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID,
                                //    expDate,
                                //    DateTime.Now.ToString("yyyyMMdd"),
                                //    routing.ProviderCode,
                                //    resultSub.PayerRefError,
                                //    resultSub.CardrefError);

                                //result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;


                                //}
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Log.Write("CreateSubscription - Exception :" + ex.ToString());
                                //result.GeneralErrorCode = -20;
                                //result.Description = string.Format("Failed to insert subscription data into database for transaction \"{0}\"!", referenceId);
                            }
                        }
                        else
                        {
                            // result.ReplyMessage.reasonCode = reasonCode;
                            result.Description = "Transaction Failed."; //Please ask dhahina about the description ?
                            result.ProviderMessage = ""; //

                            transaction.UpdatePaymentData(
                              referenceId,
                              1,
                              1,
                              routing.ProviderCode,
                              result.ReplyMessage.reasonCode,
                              null,
                              null,
                              !String.IsNullOrEmpty(result.ReplyMessage.reasonCode) ? Convert.ToInt32(result.ReplyMessage.reasonCode) : -1,
                              !String.IsNullOrEmpty(result.ReplyMessage.decision) ? result.ReplyMessage.decision : "Transaction Failed.");


                        }

                        return result;
                    }

                    //Without 3DS ends here 
                    Log.Write("with 3DS");
                    //3DS starts from here 
                    result = soap.Enrollment_Authorize(
                       referenceId,
                       accountId,
                       card,
                       billTo,
                       payment,
                       checkEnroll.Equals("1"),    // 1 = true ; 0 = false
                       BusinessFacade.DecisionManager,
                       BusinessFacade.AVSIgnore,
                       BusinessFacade.CVNIgnore,
                       routing.MerchantID,
                       capture.Equals("1"), applicationCode, country,       // 1 = true ; 0 = false
                       items.ToArray());

                    #region Get provider error description
                    /*
                     * REF_REASON_CODE = -17:Failed loading provider message
                     */
                    //csReasonCode reason = new csReasonCode();
                    //try
                    //{
                    //    if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
                    //    {
                    //        result.GeneralErrorCode = reason.generalErrorCode;
                    //        result.ProviderMessage = reason.providerMessage;
                    //    }
                    //    else
                    //    {
                    //        result.GeneralErrorCode = -17;
                    //        result.Description = "Failed loading provider message";
                    //    }
                    //}
                    //catch (Exception) { }
                    #endregion

                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status/475:The customer is enrolled in Payer Authentication
                     * REF_PAYMENT_STEP   = 2:Enrolled/5:Authorize/7:Authorize-Capture
                     * REF_PAYMENT_STATUS = 1:In Process/3:Accepted/-1:Rejected/-2:Error)
                     */
                    try
                    {
                        int paymentStep;
                        int paymentStatus;
                        if (result.ReplyMessage.reasonCode == "0" && result.ReplyMessage.decision == "Y")
                        {
                            paymentStep = 2;
                            //if (capture != "1") { paymentStep = 5; } else { paymentStep = 7; }

                            paymentStatus = 3;

                            result.GeneralErrorCode = 100;
                            result.ReplyMessage.reasonCode = "475";
                            transaction.UpdatePaymentData(
                               referenceId,
                               paymentStep,
                               paymentStatus,
                               routing.ProviderCode,
                              result.ReplyMessage.reasonCode,
                               null,
                               null,
                              475,
                               "3ds Enrolled");
                            result.ReplyMessage.decision = "Enrolled";
                            // result.ReplyMessage.reasonCode = reasonCode;
                            result.Description = "3DS Enrolled";
                            result.ProviderMessage = "3DS Enrolled";

                            //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                            result.ThreeDStatus = "3DS Enrolled";

                        } //01-Feb-2019 : Enabled for NOT ENROLLED https://developer.realexpayments.com/#!/api/3d-secure
                        else if (result.ReplyMessage.reasonCode == "110" && (result.ReplyMessage.decision == "N" && (ccType.ToUpper() == "VISA" || ccType.ToUpper() == "MASTERCARD" || ccType.ToUpper() == "MC" || ccType.ToUpper() == "MASTER"))) // || result.ReplyMessage.decision == "U"))
                        {
                            //14-Sep-2018: Moorthy : Commented
                            //15-Jul-2019: Moorthy : Commented
                            //Log.Write("Proceed to Authorisation. Your application should set the <eci> in the request to the following values:If the card type is Visa: 6,If the card type is MasterCard: 1");
                            //result = WithOut3DS(
                            //productCode,
                            //referenceId,
                            //accountId,
                            //card,
                            //billTo,
                            //payment,
                            //"1",
                            //routing,
                            //applicationCode,
                            //country,
                            //items,
                            //ccType.ToUpper() == "VISA" ? "6" : "1");

                            //return result;

                            //14-Sep-2018: Moorthy : Uncommented
                            //15-Jul-2019: Moorthy : UnCommented
                            #region OldCode

                            paymentStep = 13;
                            if (capture != "1") { paymentStep = 5; }
                            else
                            {
                                paymentStep = 7;
                            }
                            paymentStatus = 3;
                            result.GeneralErrorCode = -1;
                            result.ReplyMessage.reasonCode = "476";


                            result.ReplyMessage.decision = "Not Enrolled";
                            // result.ReplyMessage.reasonCode = reasonCode;
                            result.Description = "3DS Not Enrolled";
                            result.ProviderMessage = "3DS Not Enrolled";

                            //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                            result.ThreeDStatus = "3DS Not Enrolled";

                            transaction.UpdatePaymentDataValidate(
                                referenceId,
                                paymentStep,
                                paymentStatus,
                                routing.ProviderCode,
                                result.ReplyMessage.reasonCode,
                                result.ReplyMessage.requestID,
                                result.ReplyMessage.requestToken,
                                result.GeneralErrorCode,
                                 result.ReplyMessage.decision);

                            #region  C2
                            // resultSub = soap.Payment_Authorize(
                            //referenceId,
                            //accountId,
                            //card,
                            //billTo,
                            //payment,
                            //"",
                            //capture.Equals("1"),
                            //BusinessFacade.DecisionManager,
                            //BusinessFacade.AVSIgnore,
                            //BusinessFacade.CVNIgnore,
                            //routing.MerchantID,
                            //"6",
                            //items.ToArray());

                            // paymentStep = 8;
                            // paymentStatus =
                            //    result.ReplyMessage.decision.ToUpper().Equals("Y") ? 5 :

                            //    result.ReplyMessage.decision.ToUpper().Equals("N") ? -1 : -2;

                            // transaction.UpdatePaymentDataValidate(
                            //     referenceId,
                            //     paymentStep,
                            //     paymentStatus,
                            //     routing.ProviderCode,
                            //     resultSub.ReplyMessage.reasonCode,
                            //     resultSub.ReplyMessage.requestID,
                            //     resultSub.ReplyMessage.requestToken,
                            //     resultSub.GeneralErrorCode,
                            //      "The merchant will not be liable for repudiation chargebacks");

                            //if (resultSub.ReplyMessage.paySubscriptionCreateReply != null)
                            //{
                            //    Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                            //    subscription.CreateSubscription(
                            //        accountId,
                            //        ccNo.Substring(ccNo.Length - 6, 6),
                            //        resultSub.ReplyMessage.paySubscriptionCreateReply.subscriptionID,
                            //        expDate,
                            //        DateTime.Now.ToString("yyyyMMdd"),
                            //        routing.ProviderCode);
                            //    result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                            //}
                            #endregion

                            #endregion

                        } //01-Feb-2019 : Enabled for NOT ENROLLED https://developer.realexpayments.com/#!/api/3d-secure
                        else if ((result.ReplyMessage.reasonCode == "110" && result.ReplyMessage.decision == "U") || (result.ReplyMessage.reasonCode == "110" && result.ReplyMessage.decision == "N" && ccType.ToUpper() == "AMEX"))
                        {
                            Log.Write("Cardholder not enrolled for AMEX OR Unable to verify enrollment");

                            paymentStep = 13;
                            if (capture != "1") { paymentStep = 5; }
                            else
                            {
                                paymentStep = 7;
                            }
                            paymentStatus = 3;
                            result.GeneralErrorCode = 110;
                            result.ReplyMessage.reasonCode = "110";


                            result.ReplyMessage.decision = "Not Enrolled";
                            // result.ReplyMessage.reasonCode = reasonCode;
                            result.Description = "3DS Not Enrolled";
                            result.ProviderMessage = "3DS Not Enrolled";

                            //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                            result.ThreeDStatus = "3DS Not Enrolled";

                            transaction.UpdatePaymentDataValidate(
                                referenceId,
                                paymentStep,
                                paymentStatus,
                                routing.ProviderCode,
                                result.ReplyMessage.reasonCode,
                                result.ReplyMessage.requestID,
                                result.ReplyMessage.requestToken,
                                result.GeneralErrorCode,
                                 result.ReplyMessage.decision);
                        }
                        else if (result.ReplyMessage.reasonCode == "220" || result.ReplyMessage.decision == "U")
                        {
                            //resultSub = soap.Payment_Authorize(
                            //   referenceId,
                            //   accountId,
                            //   card,
                            //   billTo,
                            //   payment,
                            //   "",
                            //   capture.Equals("1"),
                            //   BusinessFacade.DecisionManager,
                            //   BusinessFacade.AVSIgnore,
                            //   BusinessFacade.CVNIgnore,
                            //   routing.MerchantID,
                            //   "7",
                            //   items.ToArray());
                            //paymentStep = 8;
                            //paymentStatus =
                            //   result.ReplyMessage.decision.ToUpper().Equals("Y") ? 5 :
                            //   result.ReplyMessage.decision.ToUpper().Equals("N") ? -1 : -2;

                            paymentStep = 15;

                            paymentStatus = 3;
                            result.ReplyMessage.reasonCode = "220";
                            transaction.UpdatePaymentDataValidate(
                                referenceId,
                                paymentStep,
                                paymentStatus,
                                routing.ProviderCode,
                                resultSub.ReplyMessage.reasonCode,
                                resultSub.ReplyMessage.requestID,
                                resultSub.ReplyMessage.requestToken,
                                resultSub.GeneralErrorCode,
                                "Enrolment status could not be verified");
                            //"The merchant will be liable for repudiation chargebacks");
                            result.ReplyMessage.decision = "REJECT";
                            // result.ReplyMessage.reasonCode = reasonCode;
                            result.Description = "3DS Enrolled Reject";
                            result.ProviderMessage = "3DS Enrolled Reject";

                            //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                            result.ThreeDStatus = "3DS Enrolled Reject";

                        }
                        else if (result.ReplyMessage.reasonCode == "509")
                        {
                            paymentStep = 15;

                            paymentStatus = 3;
                            result.GeneralErrorCode = 509;

                            transaction.UpdatePaymentDataValidate(
                                referenceId,
                                paymentStep,
                                paymentStatus,
                                routing.ProviderCode,
                                result.ReplyMessage.reasonCode,
                                null,
                                null,
                                509,
                                 result.ReplyMessage.decision);
                            result.Description = result.ReplyMessage.decision;
                            result.ProviderMessage = result.ReplyMessage.decision;
                            result.ReplyMessage.decision = "REJECT";

                            //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                            result.ThreeDStatus = "REJECT";

                        }
                        //if (result.ReplyMessage != null && result.ReplyMessage.decision.ToUpper().Equals("ENROLLED") && capture == "1" && result.ReplyMessage.reasonCode == "475")
                        //{


                        //    #region Add subscription data
                        //    /*
                        // * REF_REASON_CODE = -20:Failed inserting subscription data into database
                        // */
                        //    //try
                        //    //{
                        //        //03Apr2019 : Moorthy : commented for Fraud issue
                        //        //// Run transaction
                        //        //resultSub = soap.Subscription_Capture(
                        //        //    referenceId,
                        //        //    accountId,
                        //        //    card,
                        //        //    billTo,
                        //        //    payment,
                        //        //    routing.MerchantID,
                        //        //    applicationCode,
                        //        //    country);

                        //        ////if (resultSub.ReplyMessage.reasonCode == "0")
                        //        ////{
                        //        //Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                        //        //subscription.CreateSubscription(
                        //        //    accountId,
                        //        //    ccNo.Substring(ccNo.Length - 6, 6),
                        //        //    resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID,
                        //        //    expDate,
                        //        //    DateTime.Now.ToString("yyyyMMdd"),
                        //        //    routing.ProviderCode);
                        //        //result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                        //        ////} else
                        //        ////{
                        //        ////Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                        //        ////subscription.ValidateSubscription(
                        //        ////    accountId,
                        //        ////    ccNo.Substring(ccNo.Length - 6, 6),
                        //        ////    resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID,
                        //        ////    expDate,
                        //        ////    DateTime.Now.ToString("yyyyMMdd"),
                        //        ////    routing.ProviderCode,
                        //        ////    resultSub.PayerRefError,
                        //        ////    resultSub.CardrefError);

                        //        ////result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;


                        //        ////}
                        //    //}
                        //    //catch (Exception ex)
                        //    //{
                        //    //    Log.Write("CreateSubscription : Exception :" + ex.ToString());
                        //    //    //resultSub.GeneralErrorCode = -20;
                        //    //    //resultSub.Description = string.Format("Failed to insert subscription data into database for transaction \"{0}\"!", referenceId);
                        //    //}
                        //    //#endregion

                        //}
                        //result.Description = results.ToString();
                        return result;
                    }

                    catch (Exception)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed updating payment status";
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    string errMsg = result.ReplyMessage == null ?
                        //string.Format("Failed calling realex service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format("{0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message);
                    try
                    {
                        transaction.UpdatePaymentDataValidate(
                                                referenceId,
                                                1,
                                                -1,
                                                routing.ProviderCode,
                                                "",
                                                "",
                                                "",
                                                -1,
                                                errMsg);
                        //"Failed calling realex service");
                    }
                    catch { }

                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, errMsg);
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = -1; // ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;

                //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                //if (ex.Message == "Chargeback failure - card allowed for transaction")
                result.ThreeDStatus = "Failures before hitting the 3DS page";

                Log.Write("Error Result :" + result.Description);
                return result;
            }
            //Log.Write("Result :" + result);
            // Return the operation result
            return result;
        }

        /// <summary>
        /// Enrollment validate authentication.
        /// </summary>
        /// 

        public static TransactionReply Validate(
            string siteCode, string productCode, string referenceId, string accountId,
            string ccNo, string cvv, string ccType, string expDate, string firstName, string lastName, string streetName, string city, string postCode, string state, string country, string email,
            string pares, string capture, string clientIP, string applicationcode)
        {
            Log.Write("Enrollment.Validate()");
            TransactionReply result = new TransactionReply();
            TransactionReply resultauth = new TransactionReply();
            TransactionReply resultSub = new TransactionReply();
            string cannotContinue = "The validation process completed but the operation cannot continue due to error: {0}";
            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            csTransaction transaction;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(referenceId)) missingParam.Add("reference-id");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (string.IsNullOrEmpty(ccType)) missingParam.Add("cc-type");
                //04-Oct-2017 : Added for MAESTRO dont require cvn value 
                if (ccType.ToLower() != "maestro" && string.IsNullOrEmpty(ccNo)) missingParam.Add("cc-no");
                if (string.IsNullOrEmpty(expDate) || expDate.Length != 6) missingParam.Add("exp-date");
                if (string.IsNullOrEmpty(firstName)) missingParam.Add("first-name");
                if (string.IsNullOrEmpty(lastName)) missingParam.Add("last-name");
                //02-Jul-2018 : Commented 
                //if (string.IsNullOrEmpty(streetName)) missingParam.Add("street-name");
                //if (string.IsNullOrEmpty(city)) missingParam.Add("city");
                //if (string.IsNullOrEmpty(postCode)) missingParam.Add("post-code");
                if (string.IsNullOrEmpty(country)) missingParam.Add("country");
                if (string.IsNullOrEmpty(email)) missingParam.Add("email");
                if (string.IsNullOrEmpty(pares)) missingParam.Add("pares");
                if (Array.IndexOf(ParamValues.FlagData, capture ?? "") == -1) missingParam.Add("capture");
                //Log.Write("Card Type : " + ccType);
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion

                //24-Aug-2018 : Moorthy Added for Quick Top email validation
                if (email.ToLower() == "quicktopup@vectone.com")
                    email = "";

                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion
                #region Load transaction data
                /*
                 * REF_PAYMENT_STEP = 2:Enrolled/5:Authorize
                 * REF_REASON_CODE  = -19:Transaction data not found
                 */
                transaction = new csTransaction();

                if (!transaction.loadCSTransactionDataByReferenceIDAndPaymentStep(referenceId, 2))
                    throw new PaymentException(-19,
                        string.Format("The transaction for \"{0}\" has not been enrolled or the transaction has been validated!", referenceId));
                #endregion

                #region Update payment process to prevent double capture
                /*
                     * REF_REASON_CODE    = -475: Still in enrollment process
                     * REF_PAYMENT_STEP   = 3   : Authentication requested
                     * REF_PAYMENT_STATUS = 1   : In Process
                //     */
                //try
                //{
                //    transaction.UpdatePaymentData(referenceId, 3, 1, routing.ProviderCode, result.ReplyMessage.reasonCode, result.ReplyMessage.requestID, result.ReplyMessage.requestToken, -475, "N/A");
                //}
                //catch (Exception)
                //{
                //    result.GeneralErrorCode = -18;
                //    result.Description = "Failed updating payment status";
                //}
                #endregion

                /*
                 * Get Product Info
                 */
                string productSKU1 = string.Format("{0}-{1}", siteCode, productCode);
                string productSKU2 = string.Empty;
                try
                {
                    string[] part_referenceId = referenceId.Split("-".ToCharArray(), 3);
                    productSKU2 = string.Format("{0}-{1}", part_referenceId[0], part_referenceId[1]);
                }
                catch (Exception) { }
                ProductSKUInfo infoSKU = ProductSKU.GetInfo(productSKU1, productSKU2);

                /*
                 * Call Cybersource service
                 * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
                 * REF_PAYMENT_STATUS = 2:On Manual Review
                 */
                try
                {
                    // Card info
                    Card card = new Card();
                    card.accountNumber = ccNo;
                    card.cardType = ccType;
                    card.cvNumber = cvv;
                    card.cvIndicator = "1";
                    card.expirationYear = expDate.Substring(0, 4);
                    card.expirationMonth = expDate.Substring(4, 2);

                    // Billing info

                    BillTo billTo = new BillTo();
                    billTo.firstName = firstName;
                    billTo.lastName = lastName;
                    billTo.street1 = streetName;
                    billTo.city = city;
                    billTo.postalCode = postCode;
                    billTo.state = string.IsNullOrEmpty(state) ? string.Empty : state;
                    billTo.country = country;
                    billTo.email = string.IsNullOrEmpty(email) ? "unknown@mundio.com" : email;
                    billTo.ipAddress = string.IsNullOrEmpty(clientIP) ? HttpContext.Current.Request.UserHostAddress : clientIP;

                    // Product Info
                    List<Item> items = new List<Item>();
                    items.Add(new Item()
                    {
                        productName = infoSKU.ItemDescription,
                        unitPrice = infoSKU.ItemPrice.ToString(),
                        taxAmount = infoSKU.ItemVAT.ToString(),
                        productSKU = infoSKU.ProductSKU,
                        quantity = "1"
                    });

                    // Payment info
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.grandTotalAmount = transaction.amount.ToString("#.00");
                    payment.currency = transaction.currency;

                    // Run transaction
                    RealExPayment soap = new RealExPayment();
                    result = soap.Enrollment_Validate(
                        referenceId,
                        accountId,
                        card,
                        billTo,
                        payment,
                        pares,
                        capture.Equals("1"),
                        BusinessFacade.DecisionManager,
                        BusinessFacade.AVSIgnore,
                        BusinessFacade.CVNIgnore,
                        routing.MerchantID,
                        applicationcode, country,
                        items.ToArray());
                    transaction.FroudScore(referenceId, result.ReplyMessage.afsReply.afsResult);
                    transaction.EciValue(referenceId, card.fullName, result.ReplyMessage.payerAuthValidateReply.eci);
                    #region Get provider error description
                    /*
                     * REF_REASON_CODE = -17:Failed loading provider message
                     */
                    //csReasonCode reason = new csReasonCode();
                    //try
                    //{
                    //    if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
                    //    {
                    //        result.GeneralErrorCode = reason.generalErrorCode;
                    //        result.ProviderMessage = reason.providerMessage;
                    //    }
                    //    else
                    //    {
                    //        result.GeneralErrorCode = -17;
                    //        result.Description = "Failed loading provider message";
                    //    }
                    //}
                    //catch (Exception) { }
                    #endregion

                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status
                     * REF_PAYMENT_STEP   = 4:Validate Authentication/6:Capture
                     * REF_PAYMENT_STATUS = 1:In Process/3:Accepted/-1:Rejected/-2:Error
                     */
                    //try
                    //{
                    //    int paymentStep = capture != "1" ? 4 : 6;
                    //    int paymentStatus =
                    //        result.ReplyMessage.decision.ToUpper().Equals("Y") ?
                    //        paymentStep.Equals(4) ? 1 : 3 :
                    //        result.ReplyMessage.decision.ToUpper().Equals("N") ? -1 : -2;

                    //    transaction.UpdatePaymentDataValidate(
                    //        referenceId,
                    //        paymentStep,
                    //        paymentStatus,
                    //        routing.ProviderCode,
                    //        result.ReplyMessage.reasonCode,
                    //        result.ReplyMessage.requestID,
                    //        result.ReplyMessage.requestToken,
                    //        result.GeneralErrorCode,
                    //        result.ProviderMessage);
                    //}

                    //catch (Exception)
                    //{
                    //    result.GeneralErrorCode = -18;
                    //    result.Description = "Failed updating payment status";
                    //}
                    #endregion

                    if (result.ReplyMessage.decision.ToUpper().Equals("Y"))
                    {

                        int paymentStep = 7;
                        int paymentStatus = 4;
                        //result.ReplyMessage.decision.ToUpper().Equals("Y") ?
                        //paymentStep.Equals(4) ? 1 : 3 :
                        //result.ReplyMessage.decision.ToUpper().Equals("N") ? -1 : -2;

                        transaction.UpdatePaymentDataValidate(
                        referenceId,
                        paymentStep,
                        paymentStatus,
                        routing.ProviderCode,
                        "100",
                        null,
                        null,
                        0,
                            "The cardholder successfully authenticated. You may proceed with the authorisation and avail of the liability shift.", "Y","");

                        result = soap.Payment_Authorize(
                       productCode,
                       referenceId,
                       accountId,
                       card,
                       billTo,
                       payment,
                       pares,
                       capture.Equals("1"),
                       BusinessFacade.DecisionManager,
                       BusinessFacade.AVSIgnore,
                       BusinessFacade.CVNIgnore,
                       routing.MerchantID,
                       "5",
                       applicationcode, country, result.ReplyMessage,
                       items.ToArray());
                        //if (resultauth.ReplyMessage.decision.ToUpper().Equals("APPROVAL") && resultauth.ReplyMessage)
                        //{
                        string message = "";
                        string reasonCode = "";
                        paymentStep = 7;
                        paymentStatus = 4; // result.ReplyMessage.decision.ToUpper().Equals("APPROVAL") ? 3 : -1;
                        //result.ReplyMessage.decision.ToUpper().Equals("Y") ?
                        //paymentStep.Equals(4) ? 1 : 3 :
                        //result.ReplyMessage.decision.ToUpper().Equals("N") ? -1 : -2;

                        reasonCode = result.ReplyMessage.reasonCode == "0" ? "100" : result.ReplyMessage.reasonCode;
                        message = result.ReplyMessage.reasonCode == "0" ? "Transaction Succeeded." : result.ReplyMessage.decision;
                        string decision = result.ReplyMessage.reasonCode == "0" ? "ACCEPT" : "REJECT";
                        result.GeneralErrorCode = result.ReplyMessage.reasonCode == "0" ? 0 : Convert.ToInt32(result.ReplyMessage.reasonCode);
                        //transaction.UpdatePaymentDataValidate(
                        //    referenceId,
                        //    paymentStep,
                        //    paymentStatus,
                        //    routing.ProviderCode,
                        //   reasonCode,
                        //    result.ReplyMessage.requestID,
                        //    result.ReplyMessage.requestToken,
                        //    errorcode,
                        //     message);

                        //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                        result.ThreeDStatus = "Y";

                        transaction.UpdateRlxPaymentDataValidate(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            reasonCode,
                            result.ResultPASRef,
                            result.ResultAuthCode,
                            result.GeneralErrorCode,
                            message,
                            result.ThreeDStatus,
                            result.SRD);


                        //}
                        result.ReplyMessage.reasonCode = reasonCode;
                        result.Description = message;
                        result.ReplyMessage.decision = decision;

                        //To Create Subscription
                        try
                        {
                            if (reasonCode == "100")
                            {
                                //if (!referenceId.Contains("-N"))
                                //{
                                Log.Write("Create Subscription : " + referenceId);
                                resultSub = soap.Subscription_Capture(
                                    referenceId,
                                    accountId,
                                    card,
                                    billTo,
                                    payment,
                                    routing.MerchantID,
                                    applicationcode,
                                    country);
                                if (resultSub != null && resultSub.ReplyMessage != null && resultSub.ReplyMessage.paySubscriptionRetrieveReply != null && !String.IsNullOrEmpty(resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID))
                                {
                                    Log.Write("reasonCode : " + resultSub.ReplyMessage.reasonCode);
                                    Log.Write("subscriptionID : " + resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID);
                                    Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                                    subscription.CreateSubscription(
                                        accountId,
                                        ccNo.Substring(ccNo.Length - 6, 6),
                                        resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID,
                                        expDate,
                                        DateTime.Now.ToString("yyyyMMdd"),
                                        routing.ProviderCode);
                                    result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                                }
                                //}
                                //else
                                //{
                                //    Log.Write("Subscription Not Created : " + referenceId);
                                //}
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Write("CreateSubscription - Exception :" + ex.ToString());
                        }
                    }

                    if (result.ReplyMessage.decision.ToUpper().Equals("N"))
                    {
                        Log.Write("The cardholder did not authenticate successfully - if you authorise this transaction you will be liable for any chargeback.");
                        int paymentStep = 7;
                        int paymentStatus = 4;

                        result.GeneralErrorCode = 201;
                        result.ReplyMessage.reasonCode = "201";

                        //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                        result.ThreeDStatus = "N";

                        transaction.UpdatePaymentDataValidate(
                           referenceId,
                           paymentStep,
                           paymentStatus,
                           routing.ProviderCode,
                           result.ReplyMessage.reasonCode,
                           result.ReplyMessage.requestID,
                           result.ReplyMessage.requestToken,
                           result.GeneralErrorCode,
                           "The cardholder did not authenticate successfully - if you authorise this transaction you will be liable for any chargeback.",
                           result.ThreeDStatus,
                           result.SRD);

                        result.Description = "The cardholder did not authenticate successfully - if you authorise this transaction you will be liable for any chargeback.";
                        result.ReplyMessage.decision = "The cardholder did not authenticate successfully - if you authorise this transaction you will be liable for any chargeback.";

                        string message = "The cardholder did not authenticate successfully - if you authorise this transaction you will be liable for any chargeback.";
                        string reasonCode = "201";
                        string decision = "REJECT";
                        //26-Feb-2019 : Rule Disabled
                        //if (csTransaction.DoTransaction(ccNo.Substring(ccNo.Length - 6), referenceId, accountId) == 1)//if (!productCode.StartsWith("N"))
                        //{
                        //    Log.Write("Existing Customer : " + productCode + " , " + referenceId + " , " + accountId);
                        //    result = soap.Payment_Authorize(
                        //   productCode,
                        //   referenceId,
                        //   accountId,
                        //   card,
                        //   billTo,
                        //   payment,
                        //   pares,
                        //   capture.Equals("1"),
                        //   BusinessFacade.DecisionManager,
                        //   BusinessFacade.AVSIgnore,
                        //   BusinessFacade.CVNIgnore,
                        //   routing.MerchantID,
                        //   card.cardType.ToUpper() == "VISA" || card.cardType.ToUpper() == "AMEX" ? "7" : "0", //"5"
                        //   applicationcode, country, result.ReplyMessage,
                        //   items.ToArray());
                        //    //if (resultauth.ReplyMessage.decision.ToUpper().Equals("APPROVAL") && resultauth.ReplyMessage)
                        //    //{
                        //    message = "";
                        //    reasonCode = "";
                        //    paymentStep = 7;
                        //    paymentStatus =
                        //       result.ReplyMessage.decision.ToUpper().Equals("APPROVAL") ?
                        //        3 : -1;
                        //    reasonCode = result.ReplyMessage.reasonCode == "0" ? "100" : result.ReplyMessage.reasonCode;
                        //    message = result.ReplyMessage.reasonCode == "0" ? "Transaction Succeeded." : result.ReplyMessage.decision;
                        //    decision = result.ReplyMessage.reasonCode == "0" ? "ACCEPT" : "REJECT";
                        //    result.GeneralErrorCode = result.ReplyMessage.reasonCode == "0" ? 0 : Convert.ToInt32(result.ReplyMessage.reasonCode);

                        //    transaction.UpdateRlxPaymentDataValidate(
                        //        referenceId,
                        //        paymentStep,
                        //        paymentStatus,
                        //        routing.ProviderCode,
                        //        reasonCode,
                        //        result.ResultPASRef,
                        //        result.ResultAuthCode,
                        //        result.GeneralErrorCode,
                        //        message,
                        //        "N");


                        //    //}
                        //}
                        //else
                        //{
                        //    Log.Write("New Customer : " + productCode + " , " + referenceId + " , " + accountId);
                        //}
                        result.ReplyMessage.reasonCode = reasonCode;
                        result.Description = message;
                        result.ReplyMessage.decision = decision;

                    }
                    if (result.ReplyMessage.decision.ToUpper().Equals("U")) // || result.ReplyMessage.decision.ToUpper().Equals("A"))
                    {

                        int paymentStep = 7;

                        int paymentStatus = 4;
                        result.GeneralErrorCode = -1;
                        result.ReplyMessage.reasonCode = "-1";

                        //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                        result.ThreeDStatus = "U";

                        transaction.UpdatePaymentDataValidate(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            result.ReplyMessage.reasonCode,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            result.GeneralErrorCode,
                            "Cardholder authentication temporarily unavailable - no liability shift available.",
                            result.ThreeDStatus,
                            result.SRD);
                        //"The merchant will be liable for repudiation chargebacks");

                        result.ReplyMessage.decision = "ERROR";
                        result.Description = "Cardholder authentication temporarily unavailable - no liability shift available."; //"The merchant will be liable for repudiation chargebacks";

                    }
                    //31-Jul-2018 : Moorthy Added for "A: The cardholder is enrolled and the bank acknowledges the attempted authentication. You may proceed with the authorisation and avail of the liability shift." in "3ds-verifysig" step https://developer.realexpayments.com/#!/api/3d-secure/3ds-verifysig
                    if (result.ReplyMessage.decision.ToUpper().Equals("A"))
                    {
                        //TODO : Need to remove
                        //Log.Write("The merchant will be liable for repudiation chargebacks");
                        Log.Write("The cardholder is enrolled and the bank acknowledges the attempted authentication. You may proceed with the authorisation and avail of the liability shift.");
                        int paymentStep = 7;
                        int paymentStatus = 4;

                        transaction.UpdatePaymentDataValidate(
                        referenceId,
                        paymentStep,
                        paymentStatus,
                        routing.ProviderCode,
                        "-1",
                        null,
                        null,
                        -1,
                            "The cardholder is enrolled and the bank acknowledges the attempted authentication. You may proceed with the authorisation and avail of the liability shift.",
                            "A",
                            "");

                        result = soap.Payment_Authorize(
                       productCode,
                       referenceId,
                       accountId,
                       card,
                       billTo,
                       payment,
                       pares,
                       capture.Equals("1"),
                       BusinessFacade.DecisionManager,
                       BusinessFacade.AVSIgnore,
                       BusinessFacade.CVNIgnore,
                       routing.MerchantID,
                       "5",
                       applicationcode, country, result.ReplyMessage,
                       items.ToArray());

                        Log.Write("Validate result.ReplyMessage.decision : " + result.ReplyMessage.decision);
                        string message = "";
                        string reasonCode = "";
                        paymentStep = 7;
                        paymentStatus = 4; // result.ReplyMessage.decision.ToUpper().Equals("APPROVAL") ? 3 : -1;

                        reasonCode = result.ReplyMessage.reasonCode == "0" ? "100" : result.ReplyMessage.reasonCode;
                        message = result.ReplyMessage.reasonCode == "0" ? "Transaction Succeeded." : result.ReplyMessage.decision;
                        string decision = result.ReplyMessage.reasonCode == "0" ? "ACCEPT" : "REJECT";
                        result.GeneralErrorCode = result.ReplyMessage.reasonCode == "0" ? 0 : Convert.ToInt32(result.ReplyMessage.reasonCode);

                        //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
                        result.ThreeDStatus = "A";

                        transaction.UpdateRlxPaymentDataValidate(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            reasonCode,
                            result.ResultPASRef,
                            result.ResultAuthCode,
                            result.GeneralErrorCode,
                            message,
                            result.ThreeDStatus,
                            result.SRD);

                        result.ReplyMessage.reasonCode = reasonCode;
                        result.Description = message;
                        result.ReplyMessage.decision = decision;

                    }
                    //if (result.ReplyMessage.decision.ToUpper().Equals("A"))
                    //{
                    //    resultauth = soap.Payment_Authorize(
                    //                          referenceId,
                    //                          accountId,
                    //                          card,
                    //                          billTo,
                    //                          payment,
                    //                          pares,
                    //                          capture.Equals("1"),
                    //                          BusinessFacade.DecisionManager,
                    //                          BusinessFacade.AVSIgnore,
                    //                          BusinessFacade.CVNIgnore,
                    //                          routing.MerchantID,
                    //                          "6",
                    //                          items.ToArray());

                    //    int paymentStep = capture != "1" ? 4 : 6;
                    //    int paymentStatus =
                    //        result.ReplyMessage.decision.ToUpper().Equals("Y") ?
                    //        paymentStep.Equals(4) ? 1 : 3 :
                    //        result.ReplyMessage.decision.ToUpper().Equals("N") ? -1 : -2;

                    //    transaction.UpdatePaymentDataValidate(
                    //        referenceId,
                    //        paymentStep,
                    //        paymentStatus,
                    //        routing.ProviderCode,
                    //        resultauth.ReplyMessage.reasonCode,
                    //        resultauth.ReplyMessage.requestID,
                    //        resultauth.ReplyMessage.requestToken,
                    //        resultauth.GeneralErrorCode,
                    //         "The merchant will not be liable for repudiation chargebacks");

                    //    #region Add subscription data
                    //    /*
                    //     * REF_REASON_CODE = -20:Failed inserting subscription data into database
                    //     */
                    //    try
                    //    {
                    //        if (resultSub.ReplyMessage.paySubscriptionCreateReply != null)
                    //        {
                    //            Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                    //            subscription.CreateSubscription(
                    //                accountId,
                    //                ccNo.Substring(ccNo.Length - 6, 6),
                    //                resultSub.ReplyMessage.paySubscriptionCreateReply.subscriptionID,
                    //                expDate,
                    //                DateTime.Now.ToString("yyyyMMdd"),
                    //                routing.ProviderCode);
                    //            result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                    //        }
                    //    }
                    //    catch (Exception)
                    //    {
                    //        resultSub.GeneralErrorCode = -20;
                    //        resultSub.Description = string.Format("Failed to insert subscription data into database for transaction \"{0}\"!", referenceId);
                    //    }
                    //    #endregion
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling Realex service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = -1; //ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                Log.Write("Error Result :" + result.Description);
                return result;
            }

            // Return the operation result
            //if (result.ReplyMessage.decision != "APPROVAL" || result.ReplyMessage.decision=="")
            //{
            //    result.ReplyMessage.decision = "N";
            //}
            //else if(result.ReplyMessage.decision == "APPROVAL")
            //{
            // result.ReplyMessage.decision = "Y";
            //}
            return result;
        }

        /// <summary>
        /// Enrollment capture.
        /// </summary>
        public static TransactionReply Capture(
            string siteCode, string productCode, string referenceId, string accountId)
        {
            TransactionReply result = new TransactionReply();
            TransactionReply resultSub = new TransactionReply();
            string cannotContinue = "The capture process completed but the operation cannot continue due to error: {0}";

            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            csTransaction transaction;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(referenceId)) missingParam.Add("reference-id");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion
                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion
                #region Load transaction data
                /*
                 * REF_PAYMENT_STEP = 5:Authorize
                 * REF_REASON_CODE  = -19:Transaction data not found
                 */
                transaction = new csTransaction();
                if (!transaction.loadCSTransactionDataByReferenceIDAndPaymentStep(referenceId, 5))
                    throw new PaymentException(-19,
                        string.Format("The transaction for \"{0}\" has not been enrolled or the transaction cannot be found!", referenceId));
                #endregion

                #region Update payment process to prevent double capture
                /*
                     * REF_REASON_CODE    = 0 : Successful (but still in process, see the payment_status = 1)
                     * REF_PAYMENT_STEP   = 6 : Capture
                     * REF_PAYMENT_STATUS = 1 : In Process
                     */
                try
                {
                    transaction.UpdatePaymentData(referenceId, 6, 1, routing.ProviderCode, result.ReplyMessage.reasonCode, result.ReplyMessage.requestID, result.ReplyMessage.requestToken, 0, "N/A");
                }
                catch (Exception)
                {
                    result.GeneralErrorCode = -18;
                    result.Description = "Failed updating payment status";
                }
                #endregion

                /*
                 * Get Product Info
                 */
                string productSKU1 = string.Format("{0}-{1}", siteCode, productCode);
                string productSKU2 = string.Empty;
                try
                {
                    string[] part_referenceId = referenceId.Split("-".ToCharArray(), 3);
                    productSKU2 = string.Format("{0}-{1}", part_referenceId[0], part_referenceId[1]);
                }
                catch (Exception) { }
                ProductSKUInfo infoSKU = ProductSKU.GetInfo(productSKU1, productSKU2);

                /*
                 * Call Cybersource service
                 * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
                 * REF_PAYMENT_STATUS = 2:On Manual Review
                 */
                try
                {
                    // Product Info
                    List<Item> items = new List<Item>();
                    items.Add(new Item()
                    {
                        productName = infoSKU.ItemDescription,
                        unitPrice = infoSKU.ItemPrice.ToString(),
                        taxAmount = infoSKU.ItemVAT.ToString(),
                        productSKU = infoSKU.ProductSKU,
                        quantity = "1"
                    });

                    // Payment information
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.grandTotalAmount = transaction.amount.ToString("#.00");
                    payment.currency = transaction.currency;

                    // Run transaction
                    RealExPayment soap = new RealExPayment();
                    result = soap.Enrollment_Capture(
                        referenceId,
                        accountId,
                        payment,
                        transaction.requestID,
                        transaction.requestToken,
                        BusinessFacade.DecisionManager,
                        routing.MerchantID,
                        items.ToArray());

                    #region Get provider error description
                    /*
                     * REF_REASON_CODE = -17:Failed loading provider message
                     */
                    csReasonCode reason = new csReasonCode();
                    try
                    {
                        if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
                        {
                            result.GeneralErrorCode = reason.generalErrorCode;
                            result.ProviderMessage = reason.providerMessage;
                        }
                        else
                        {
                            result.GeneralErrorCode = -17;
                            result.Description = "Failed loading provider message";
                        }
                    }
                    catch (Exception) { }
                    #endregion
                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status
                     * REF_PAYMENT_STEP   = 6:Capture
                     * REF_PAYMENT_STATUS = 3:Accepted/-1:Rejected/-2:Error
                     */
                    try
                    {
                        int paymentStep = 6;
                        int paymentStatus =
                            result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") ? 3 :
                            result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                        transaction.UpdatePaymentData(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            result.ReplyMessage.reasonCode,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            reason.generalErrorCode,
                            result.ProviderMessage);
                    }
                    catch (Exception)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed updating payment status";
                    }
                    #endregion

                    // If transaction captured and succeed, create subscription
                    if (result.ReplyMessage != null && result.ReplyMessage.decision.ToUpper().Equals("ACCEPT"))
                    {
                        // Run transaction
                        resultSub = soap.Subcription_Create(
                            referenceId,
                            accountId,
                            payment,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            BusinessFacade.DecisionManager,
                            routing.MerchantID);

                        #region Add subscription data
                        /*
                         * REF_REASON_CODE = -20:Failed inserting subscription data into database
                         */
                        try
                        {
                            if (resultSub.ReplyMessage.paySubscriptionCreateReply != null)
                            {
                                string ccNo = transaction.Last6DigitsCC;
                                Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                                subscription.CreateSubscription(
                                    accountId,
                                    ccNo.Substring(ccNo.Length - 6, 6),
                                    resultSub.ReplyMessage.paySubscriptionCreateReply.subscriptionID,
                                    "999912",
                                    DateTime.Now.ToString("yyyyMMdd"),
                                    routing.ProviderCode);
                                result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                            }
                        }
                        catch (Exception)
                        {
                            resultSub.GeneralErrorCode = -20;
                            resultSub.Description = string.Format("Failed to insert subscription data into database for transaction \"{0}\"!", referenceId);
                        }
                        #endregion
                        #region Get provider error description
                        /*
                         * REF_REASON_CODE = -17:Failed loading provider message
                         */
                        reason = new csReasonCode();
                        try
                        {
                            if (reason.LoadReasonCodeData(resultSub.ReplyMessage.reasonCode, routing.ProviderCode))
                            {
                                resultSub.GeneralErrorCode = reason.generalErrorCode;
                                resultSub.ProviderMessage = reason.providerMessage;
                            }
                            else
                            {
                                resultSub.GeneralErrorCode = -17;
                                resultSub.Description = "Failed loading provider message";
                            }
                        }
                        catch (Exception) { }
                        #endregion
                        #region Update payment process
                        /*
                         * REF_REASON_CODE    = -18:Failed updating payment status/475:The customer is enrolled in Payer Authentication
                         * REF_PAYMENT_STEP   = 8:Create Subscription
                         * REF_PAYMENT_STATUS = 5:Subscribed/-1:Rejected/-2:Error
                         */
                        try
                        {
                            int paymentStep = 8;
                            int paymentStatus =
                                resultSub.ReplyMessage.decision.ToUpper().Equals("ACCEPT") ? 5 :
                                resultSub.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                            transaction.UpdatePaymentData(
                                referenceId,
                                paymentStep,
                                paymentStatus,
                                routing.ProviderCode,
                                resultSub.ReplyMessage.reasonCode,
                                resultSub.ReplyMessage.requestID,
                                resultSub.ReplyMessage.requestToken,
                                reason.generalErrorCode,
                                resultSub.ProviderMessage);
                        }
                        catch (Exception)
                        {
                            resultSub.GeneralErrorCode = -18;
                            resultSub.Description = "Failed updating payment status";
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling Cybersource service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                return result;
            }

            // Return the operation result
            return result;
        }

        /// <summary>
        /// Enrollment full authorization reversal.
        /// </summary>
        public static TransactionReply Authorize_Reverse(
            string siteCode, string productCode, string referenceId, string accountId)
        {
            TransactionReply result = new TransactionReply();
            string cannotContinue = "The authorization reversal process completed but the operation cannot continue due to error: {0}";

            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            csTransaction transaction;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(referenceId)) missingParam.Add("reference-id");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion
                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion
                #region Load transaction data
                /*
                 * REF_PAYMENT_STEP = 5:Authorize
                 * REF_REASON_CODE  = -19:Transaction data not found
                 */
                transaction = new csTransaction();
                if (!transaction.loadCSTransactionDataByReferenceIDAndPaymentStep(referenceId, 5))
                    throw new PaymentException(-19,
                        string.Format("The transaction for \"{0}\" has not been enrolled or the transaction cannot be found!", referenceId));
                #endregion

                /*
                 * Call Cybersource service
                 * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
                 * REF_PAYMENT_STATUS = 2:On Manual Review
                 */
                try
                {
                    // Payment info
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.grandTotalAmount = transaction.amount.ToString("#.00");
                    payment.currency = transaction.currency;

                    // Run transaction
                    RealExPayment soap = new RealExPayment();
                    result = soap.Enrollment_AuthReverse(
                        referenceId,
                        accountId,
                        payment,
                        transaction.requestID,
                        transaction.requestToken,
                        BusinessFacade.DecisionManager,
                        routing.MerchantID);

                    #region Get provider error description
                    /*
                     * REF_REASON_CODE = -17:Failed loading provider message
                     */
                    csReasonCode reason = new csReasonCode();
                    try
                    {
                        if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
                        {
                            result.GeneralErrorCode = reason.generalErrorCode;
                            result.ProviderMessage = reason.providerMessage;
                        }
                        else
                        {
                            result.GeneralErrorCode = -17;
                            result.Description = "Failed loading provider message";
                        }
                    }
                    catch (Exception) { }
                    #endregion
                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status
                     * REF_PAYMENT_STEP   = 9:Authorization Reversal
                     * REF_PAYMENT_STATUS = 6:Reversed/-1:Rejected/-2:Error
                     */
                    try
                    {
                        int paymentStep = 9;
                        int paymentStatus =
                            result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") ? 6 :
                            result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                        transaction.UpdatePaymentData(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            result.ReplyMessage.reasonCode,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            reason.generalErrorCode,
                            result.ProviderMessage);
                    }
                    catch (Exception)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed updating payment status";
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling Cybersource service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                return result;
            }

            // Return the operation result
            return result;
        }


        /// <summary>
        /// Enrollment full authorization reversal.
        /// </summary>
        public static TransactionReply Void(string siteCode, string productCode, string referenceId, string accountId)
        {
            TransactionReply result = new TransactionReply();
            string cannotContinue = "The authorization reversal process completed but the operation cannot continue due to error: {0}";

            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            csTransaction transaction;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(referenceId)) missingParam.Add("reference-id");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion
                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion
                #region Load transaction data
                /*
                 * REF_PAYMENT_STEP = 5:Authorize
                 * REF_REASON_CODE  = -19:Transaction data not found
                 */
                transaction = new csTransaction();
                if (!transaction.loadCSTransactionDataByReferenceIDAndPaymentStep(referenceId, 5))
                    throw new PaymentException(-19,
                        string.Format("The transaction for \"{0}\" has not been enrolled or the transaction cannot be found!", referenceId));
                #endregion

                /*
                 * Call Cybersource service
                 * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
                 * REF_PAYMENT_STATUS = 2:On Manual Review
                 */
                try
                {
                    // Payment info
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.grandTotalAmount = transaction.amount.ToString("#.00");
                    payment.currency = transaction.currency;

                    // Run transaction
                    RealExPayment soap = new RealExPayment();
                    result = soap.Enrollment_Void(
                        referenceId,
                        accountId,
                        payment,
                        transaction.requestID,
                        transaction.requestToken,
                        BusinessFacade.DecisionManager,
                        routing.MerchantID);

                    #region Get provider error description
                    /*
                     * REF_REASON_CODE = -17:Failed loading provider message
                     */
                    csReasonCode reason = new csReasonCode();
                    try
                    {
                        if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
                        {
                            result.GeneralErrorCode = reason.generalErrorCode;
                            result.ProviderMessage = reason.providerMessage;
                        }
                        else
                        {
                            result.GeneralErrorCode = -17;
                            result.Description = "Failed loading provider message";
                        }
                    }
                    catch (Exception) { }
                    #endregion
                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status
                     * REF_PAYMENT_STEP   = 9:Authorization Reversal
                     * REF_PAYMENT_STATUS = 6:Reversed/-1:Rejected/-2:Error
                     */
                    try
                    {
                        int paymentStep = 9;
                        int paymentStatus =
                            result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") ? 6 :
                            result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                        transaction.UpdatePaymentData(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            result.ReplyMessage.reasonCode,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            reason.generalErrorCode,
                            result.ProviderMessage);
                    }
                    catch (Exception)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed updating payment status";
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling Cybersource service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                return result;
            }

            // Return the operation result
            return result;
        }



        public static TransactionReply ReviewCapture(
            string siteCode, string productCode, string referenceId, string accountId)
        {
            TransactionReply result = new TransactionReply();
            TransactionReply resultSub = new TransactionReply();
            string cannotContinue = "The capture process completed but the operation cannot continue due to error: {0}";

            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            csTransaction transaction;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(referenceId)) missingParam.Add("reference-id");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion
                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion
                #region Load transaction data
                /*
                 * REF_PAYMENT_STEP = 5:Authorize
                 * REF_REASON_CODE  = -19:Transaction data not found
                 */



                transaction = new csTransaction();
                if (!transaction.loadCSTransactionDataByReferenceIDAndPaymentStep(referenceId, 7))
                    throw new PaymentException(-19,
                        string.Format("The transaction for \"{0}\" cannot be found!", referenceId));
                #endregion

                #region Update payment process to prevent double capture
                /*
                     * REF_REASON_CODE    = 0 : Successful (but still in process, see the payment_status = 1)
                     * REF_PAYMENT_STEP   = 6 : Capture
                     * REF_PAYMENT_STATUS = 1 : In Process
                     */

                try
                {   //result.ReplyMessage.reasonCode, result.ReplyMessage.requestID, result.ReplyMessage.requestToken, 0);
                    transaction.UpdatePaymentData(referenceId, 6, 1, routing.ProviderCode, transaction.generalErrorCode.ToString(), transaction.requestID, transaction.requestToken, 0, "N/A");
                }
                catch (Exception)
                {
                    result.GeneralErrorCode = -18;
                    result.Description = "Failed updating payment status";
                }
                #endregion

                /*
                 * Get Product Info
                 */
                string productSKU1 = string.Format("{0}-{1}", siteCode, productCode);
                string productSKU2 = string.Empty;
                try
                {
                    string[] part_referenceId = referenceId.Split("-".ToCharArray(), 3);
                    productSKU2 = string.Format("{0}-{1}", part_referenceId[0], part_referenceId[1]);
                }
                catch (Exception) { }
                ProductSKUInfo infoSKU = ProductSKU.GetInfo(productSKU1, productSKU2);

                /*
                 * Call Cybersource service
                 * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
                 * REF_PAYMENT_STATUS = 2:On Manual Review
                 */
                try
                {
                    // Product Info
                    List<Item> items = new List<Item>();
                    items.Add(new Item()
                    {
                        productName = infoSKU.ItemDescription,
                        unitPrice = infoSKU.ItemPrice.ToString(),
                        taxAmount = infoSKU.ItemVAT.ToString(),
                        productSKU = infoSKU.ProductSKU,
                        quantity = "1"
                    });

                    // Payment information
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.grandTotalAmount = transaction.amount.ToString("#.00");
                    payment.currency = transaction.currency;

                    // Run transaction
                    RealExPayment soap = new RealExPayment();
                    result = soap.Enrollment_Capture(
                        referenceId,
                        accountId,
                        payment,
                        transaction.requestID,
                        transaction.requestToken,
                        false,//BusinessFacade.DecisionManager, klo true maka akan masuk review lg
                        routing.MerchantID,
                        items.ToArray());

                    #region Get provider error description
                    /*
                     * REF_REASON_CODE = -17:Failed loading provider message
                     */
                    csReasonCode reason = new csReasonCode();
                    try
                    {
                        if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
                        {
                            result.GeneralErrorCode = reason.generalErrorCode;
                            result.ProviderMessage = reason.providerMessage;
                        }
                        else
                        {
                            result.GeneralErrorCode = -17;
                            result.Description = "Failed loading provider message";
                        }
                    }
                    catch (Exception) { }
                    #endregion
                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status
                     * REF_PAYMENT_STEP   = 6:Capture
                     * REF_PAYMENT_STATUS = 3:Accepted/-1:Rejected/-2:Error
                     */
                    try
                    {
                        int paymentStep = 12; //6 , 12 : manual review
                        int paymentStatus =
                            result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") ? 4 :  //3
                            result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                        transaction.UpdatePaymentData(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            result.ReplyMessage.reasonCode,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            reason.generalErrorCode,
                            result.ProviderMessage);
                    }
                    catch (Exception)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed updating payment status";
                    }
                    #endregion

                    // If transaction captured and succeed, create subscription
                    if (result.ReplyMessage != null && result.ReplyMessage.decision.ToUpper().Equals("ACCEPT"))
                    {
                        // Run transaction
                        resultSub = soap.Subcription_Create(
                            referenceId,
                            accountId,
                            payment,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            BusinessFacade.DecisionManager,
                            routing.MerchantID);

                        #region Add subscription data
                        /*
                         * REF_REASON_CODE = -20:Failed inserting subscription data into database
                         */
                        try
                        {
                            if (resultSub.ReplyMessage.paySubscriptionCreateReply != null)
                            {
                                string ccNo = transaction.Last6DigitsCC;
                                Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                                subscription.CreateSubscription(
                                    accountId,
                                    ccNo.Substring(ccNo.Length - 6, 6),
                                    resultSub.ReplyMessage.paySubscriptionCreateReply.subscriptionID,
                                    "999912",
                                    DateTime.Now.ToString("yyyyMMdd"),
                                    routing.ProviderCode);
                                result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                            }
                        }
                        catch (Exception)
                        {
                            resultSub.GeneralErrorCode = -20;
                            resultSub.Description = string.Format("Failed to insert subscription data into database for transaction \"{0}\"!", referenceId);
                        }
                        #endregion
                        #region Get provider error description
                        /*
                         * REF_REASON_CODE = -17:Failed loading provider message
                         */
                        reason = new csReasonCode();
                        try
                        {
                            if (reason.LoadReasonCodeData(resultSub.ReplyMessage.reasonCode, routing.ProviderCode))
                            {
                                resultSub.GeneralErrorCode = reason.generalErrorCode;
                                resultSub.ProviderMessage = reason.providerMessage;
                            }
                            else
                            {
                                resultSub.GeneralErrorCode = -17;
                                resultSub.Description = "Failed loading provider message";
                            }
                        }
                        catch (Exception) { }
                        #endregion
                        #region Update payment process
                        /*
                         * REF_REASON_CODE    = -18:Failed updating payment status/475:The customer is enrolled in Payer Authentication
                         * REF_PAYMENT_STEP   = 8:Create Subscription
                         * REF_PAYMENT_STATUS = 5:Subscribed/-1:Rejected/-2:Error
                         */
                        try
                        {
                            int paymentStep = 8;
                            int paymentStatus =
                                resultSub.ReplyMessage.decision.ToUpper().Equals("ACCEPT") ? 5 :
                                resultSub.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                            transaction.UpdatePaymentData(
                                referenceId,
                                paymentStep,
                                paymentStatus,
                                routing.ProviderCode,
                                resultSub.ReplyMessage.reasonCode,
                                resultSub.ReplyMessage.requestID,
                                resultSub.ReplyMessage.requestToken,
                                reason.generalErrorCode,
                                resultSub.ProviderMessage);
                        }
                        catch (Exception)
                        {
                            resultSub.GeneralErrorCode = -18;
                            resultSub.Description = "Failed updating payment status";
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling Cybersource service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                return result;
            }

            // Return the operation result
            return result;
        }
        public static TransactionReply WithOut3DS(string productCode, string referenceId, string accountId, Card card, BillTo billTo, PurchaseTotals payment, string capture,
           RoutingPayment routing, string applicationCode, string country, List<Item> items, string eciValue)
        {
            TransactionReply result = new TransactionReply();
            TransactionReply resultSub = new TransactionReply();
            RealExPayment soap = new RealExPayment();
            csTransaction transaction;
            transaction = new csTransaction();


            result = soap.Payment_Authorize(
                        productCode,
                        referenceId,
                        accountId,
                        card,
                        billTo,
                        payment,
                        capture.Equals("1"),
                        BusinessFacade.DecisionManager,
                        BusinessFacade.AVSIgnore,
                        BusinessFacade.CVNIgnore,
                        routing.MerchantID,
                //"5",
                        eciValue,
                        applicationCode,
                        country,
                        items.ToArray());

            if (result.ReplyMessage.reasonCode == "0" && result.ReplyMessage.decision == "APPROVAL")
            {

                result.GeneralErrorCode = 100;
                result.ReplyMessage.reasonCode = "100";//verify with Dhashina

                result.ReplyMessage.decision = "ACCEPT";
                // result.ReplyMessage.reasonCode = reasonCode;
                result.Description = "Transaction Succeeded."; //Please ask dhahina about the description ?
                result.ProviderMessage = ""; //

                transaction.UpdatePaymentData(
                              referenceId,
                              1,
                              1,
                              routing.ProviderCode,
                              result.ReplyMessage.reasonCode,
                              null,
                              null,
                             100,
                              result.Description);

                try
                {

                    resultSub = soap.Subscription_Capture(
                        referenceId,
                        accountId,
                        card,
                        billTo,
                        payment,
                        routing.MerchantID,
                        applicationCode,
                        country);

                    //if (resultSub.ReplyMessage.reasonCode == "0")
                    //{
                    Subscriptions subscription = new Subscriptions(accountId, card.accountNumber.Substring(card.accountNumber.Length - 6, 6), routing.ProviderCode);
                    subscription.CreateSubscription(
                        accountId,
                        card.accountNumber.Substring(card.accountNumber.Length - 6, 6),
                        resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID,
                        card.expirationYear + card.expirationMonth,
                        DateTime.Now.ToString("yyyyMMdd"),
                        routing.ProviderCode);
                    result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;
                    #region c1
                    //} else
                    //{
                    //Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                    //subscription.ValidateSubscription(
                    //    accountId,
                    //    ccNo.Substring(ccNo.Length - 6, 6),
                    //    resultSub.ReplyMessage.paySubscriptionRetrieveReply.subscriptionID,
                    //    expDate,
                    //    DateTime.Now.ToString("yyyyMMdd"),
                    //    routing.ProviderCode,
                    //    resultSub.PayerRefError,
                    //    resultSub.CardrefError);

                    //result.ReplyMessage.paySubscriptionCreateReply = resultSub.ReplyMessage.paySubscriptionCreateReply;


                    //}
                    #endregion
                }
                catch (Exception ex)
                {
                    Log.Write("CreateSubscription - Exception :" + ex.ToString());
                    //result.GeneralErrorCode = -20;
                    //result.Description = string.Format("Failed to insert subscription data into database for transaction \"{0}\"!", referenceId);
                }
            }
            else
            {
                // result.ReplyMessage.reasonCode = reasonCode;
                result.Description = "Transaction Failed."; //Please ask dhahina about the description ?
                result.ProviderMessage = ""; //

                transaction.UpdatePaymentData(
                  referenceId,
                  1,
                  1,
                  routing.ProviderCode,
                  result.ReplyMessage.reasonCode,
                  null,
                  null,
                 Convert.ToInt32(result.ReplyMessage.reasonCode),
                 !String.IsNullOrEmpty(result.ReplyMessage.decision) ? result.ReplyMessage.decision : "Transaction Failed.");
            }

            return result;
        }
    }
}
