﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Web;
using CyberSource.Clients.SoapWebReference;
using CP.DataType;
using CP.BusinessLogic.DB;
using CP.BusinessLogic.CyberSource;
using CP.BusinessLogic.DB.Reference;

namespace CP.BusinessLogic.Transaction
{
    public class Subscription
    {
        /// <summary>
        /// Subscription create.
        /// </summary>
        public static TransactionReply Create(
            string siteCode, string productCode, string referenceId, string accountId,
            string ccNo, string expDate)
        {
            TransactionReply result = new TransactionReply();
            string cannotContinue = "The create subscription process completed but the operation cannot continue due to error: {0}";

            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            csTransaction transaction;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(referenceId)) missingParam.Add("reference-id");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (string.IsNullOrEmpty(ccNo)) missingParam.Add("cc-no");
                if (string.IsNullOrEmpty(expDate)) missingParam.Add("exp-date");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion
                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion
                #region Load transaction data
                /*
                 * REF_PAYMENT_STEP = 6:Capture/7:Authorize - Capture
                 * REF_REASON_CODE  = -19:Transaction data not found
                 */
                transaction = new csTransaction();
                if (!transaction.loadCSTransactionDataByReferenceIDAndPaymentStep(referenceId, 6))
                    if (!transaction.loadCSTransactionDataByReferenceIDAndPaymentStep(referenceId, 7))
                        throw new PaymentException(-19,
                            string.Format("The transaction for \"{0}\" cannot be found!", referenceId));
                #endregion

                /*
                 * Call Cybersource service
                 * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
                 * REF_PAYMENT_STATUS = 2:On Manual Review
                 */
                try
                {
                    // Payment info
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.currency = transaction.currency;
                    payment.grandTotalAmount = transaction.amount.ToString("#.00");

                    // Run transaction
                    RealExPayment soap = new RealExPayment();
                    result = soap.Subcription_Create(
                        referenceId,
                        accountId,
                        payment,
                        transaction.requestID,
                        transaction.requestToken,
                        BusinessFacade.DecisionManager,
                        routing.MerchantID);

                    #region Add subscription data
                    /*
                     * REF_REASON_CODE = -20:Failed inserting subscription data into database
                     */
                    try
                    {
                        if (result.ReplyMessage.paySubscriptionCreateReply != null)
                        {
                            Subscriptions subscription = new Subscriptions(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode);
                            subscription.CreateSubscription(
                                accountId,
                                ccNo.Substring(ccNo.Length - 6, 6),
                                result.ReplyMessage.paySubscriptionCreateReply.subscriptionID,
                                expDate,
                                DateTime.Now.ToString("yyyyMMdd"),
                                routing.ProviderCode);
                        }
                    }
                    catch (Exception)
                    {
                        result.GeneralErrorCode = -20;
                        result.Description = string.Format("Failed to insert subscription data into database for transaction \"{0}\"!", referenceId);
                    }
                    #endregion
                    #region Get provider error description
                    /*
                     * REF_REASON_CODE = -17:Failed loading provider message
                     */
                    csReasonCode reason = new csReasonCode();
                    try
                    {
                        if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
                        {
                            result.GeneralErrorCode = reason.generalErrorCode;
                            result.ProviderMessage = reason.providerMessage;
                        }
                        else
                        {
                            result.GeneralErrorCode = -17;
                            result.Description = "Failed loading provider message";
                        }
                    }
                    catch (Exception) { }
                    #endregion
                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status/475:The customer is enrolled in Payer Authentication
                     * REF_PAYMENT_STEP   = 8:Create Subscription
                     * REF_PAYMENT_STATUS = 5:Subscribed/-1:Rejected/-2:Error
                     */
                    try
                    {
                        int paymentStep = 8;
                        int paymentStatus =
                            result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") ? 5 :
                            result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                        transaction.UpdatePaymentData(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            result.ReplyMessage.reasonCode,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            reason.generalErrorCode,
                            result.ProviderMessage);
                    }
                    catch (Exception)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed updating payment status";
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling Cybersource service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                return result;
            }

            // Return the operation result
            return result;
        }

        //06-Jun-2017 : Modified : For Subscription
        /// <summary>
        /// Subscription authorization.
        /// </summary>
        public static TransactionReply Authorize(
            string siteCode, string applicationCode, string productCode, string paymentAgent, string serviceType, string paymentMode, string accountId,
            string ccNo, string ccCvv, string amount, string currency,
            string capture, string email, string ipclient, string country)
        {
            Log.Write("Subscription.Authorize()");
            TransactionReply result = new TransactionReply();
            string cannotContinue = "The authorization process completed but the operation cannot continue due to error: {0}";

            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            Subscriptions subscription;
            csTransaction transaction;
            string referenceId;
            try
            {
                //19-Mar-2019 : Moorthy : Added for Disable the IVR Topup
                Log.Write("IVR Topup Disabled");
                throw new PaymentException(-1, "IVR Topup Disabled");

                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */

                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(paymentAgent)) missingParam.Add("payment-agent");
                if (string.IsNullOrEmpty(serviceType)) missingParam.Add("service-type");
                if (string.IsNullOrEmpty(paymentMode)) missingParam.Add("payment-mode");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (string.IsNullOrEmpty(ccNo) && string.IsNullOrEmpty(ccCvv))
                {
                    missingParam.Add("cc-no");
                    missingParam.Add("cc-cvv");
                }
                if (string.IsNullOrEmpty(amount)) missingParam.Add("amount");
                if (string.IsNullOrEmpty(currency)) missingParam.Add("currency");
                if (Array.IndexOf(ParamValues.FlagData, capture ?? "") == -1) missingParam.Add("capture");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion

                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion

                #region Get IP Payment Agent

                string IpPaymentAgent = HttpContext.Current.Request.UserHostAddress;

                #endregion

                #region Load subscription data
                /*
                 * REF_REASON_CODE = -13:Subscription data not found
                 */
                subscription = new Subscriptions();
                if (!string.IsNullOrEmpty(ccNo))
                {
                    if (ccNo.Length == 4)
                    {
                        if (!subscription.LoadSubscriptionsData(accountId, ccNo.Substring(ccNo.Length - 4, 4), routing.ProviderCode))
                            throw new PaymentException(-13,
                                string.Format("Cannot find subscription data for account-id \"{0}\" and cc-no \"{1}\"", accountId, ccNo));
                    }
                    else if (ccNo.Length >= 6)
                    {
                        if (!subscription.LoadSubscriptionsData(accountId, ccNo.Substring(ccNo.Length - 6, 6), routing.ProviderCode))
                        {
                            throw new PaymentException(-13,
                                string.Format("Cannot find subscription data for account-id \"{0}\" and cc-no \"{1}\"", accountId, ccNo));
                        }
                    }
                    else
                    {
                        throw new PaymentException(-13,
                                string.Format("Invalid cc-no \"{0}\"", ccNo));
                    }
                }
                else
                {
                    if (!subscription.LoadSubscriptionsData_byCVV(accountId, ccCvv, routing.ProviderCode))
                        throw new PaymentException(-13,
                            string.Format("Cannot find subscription data for account-id \"{0}\" and cc-cvv \"{1}\"", accountId, ccCvv));
                }
                #endregion

                #region Insert transaction information
                /*
                 * REF_PAYMENT_STEP   = 1:Begin Process Payment
                 * REF_PAYMENT_STATUS = 0: New
                 * REF_REASON_CODE    = -10:Insert payment data for new payment failed
                 */
                int retError = 0;
                transaction = new csTransaction();
                try
                {
                    referenceId = transaction.InsertPaymentData(
                        siteCode,
                        applicationCode,
                        productCode,
                        paymentAgent,
                        serviceType,
                        paymentMode,
                        1,
                        0,
                        accountId,
                        subscription.last6DigitsCC,
                        Convert.ToDouble(amount),
                        currency,
                        subscription.subscriptionID,
                        routing.MerchantID,
                        routing.ProviderCode,
                        "100",
                        0,
                        ref retError,
                        string.IsNullOrEmpty(email) ? "N/A" : email,
                        IpPaymentAgent,
                        string.IsNullOrEmpty(ipclient) ? "N/A" : ipclient,
                        "Payment Reference Created",
                        "1",
                        "",
                        "")
                    .ToString();
                }
                catch (Exception ex)
                {
                    throw new PaymentException(-10,
                        string.Format("Insert payment data failed. {0}", (ex.InnerException ?? ex).Message));
                }
                #endregion

                /*
                 * Get Product Info
                 */
                string productSKU1 = string.Format("{0}-{1}", siteCode, productCode);
                string productSKU2 = string.Empty;
                try
                {
                    string[] part_referenceId = referenceId.Split("-".ToCharArray(), 3);
                    productSKU2 = string.Format("{0}-{1}", part_referenceId[0], part_referenceId[1]);
                }
                catch (Exception) { }
                ProductSKUInfo infoSKU = ProductSKU.GetInfo(productSKU1, productSKU2);

                try
                {
                    BillTo billTo = new BillTo();
                    billTo.email = email;
                    billTo.ipAddress = string.IsNullOrEmpty(ipclient) ? HttpContext.Current.Request.UserHostAddress : ipclient;
                    billTo.customerID = accountId;

                    // Product Info
                    List<Item> items = new List<Item>();
                    items.Add(new Item()
                    {
                        productName = infoSKU.ItemDescription,
                        unitPrice = infoSKU.ItemPrice.ToString(),
                        taxAmount = infoSKU.ItemVAT.ToString(),
                        productSKU = infoSKU.ProductSKU,
                        quantity = "1"
                    });
                    // Payment info
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.grandTotalAmount = amount;
                    payment.currency = siteCode.Equals("FR1") ? "EUR" : currency; // TODO: <Developer> Hardcoded to EUR currency if the request from Chillitalk France (sitecode = FR1)

                    // Run transaction
                    RealExPayment soap = new RealExPayment();
                    try
                    {
                        result = soap.Subscription_AuthorizeNew(
                                billTo,
                                referenceId,
                                accountId,
                                payment,
                                routing.MerchantID,
                                applicationCode,
                                country,
                                subscription.subscriptionID, email);

                        #region Update payment process
                        /*
                     * REF_REASON_CODE    = -18:Failed updating payment status/475:The customer is enrolled in Payer Authentication
                     * REF_PAYMENT_STEP   = 5:Authorize/7:Authorize - Capture
                     * REF_PAYMENT_STATUS = 1:In Process/3:Accepted/-1:Rejected/-2:Error)
                     */
                        try
                        {
                            string message = "";
                            string reasonCode = "";
                            reasonCode = result.ReplyMessage.reasonCode == "0" ? "100" : "203";
                            message = result.ReplyMessage.reasonCode == "0" ? "Transaction Succeeded." : !String.IsNullOrEmpty(result.ReplyMessage.decision) ? result.ReplyMessage.decision : "Transaction Failed.";
                            string decision = result.ReplyMessage.reasonCode == "0" ? "ACCEPT" : "REJECT";
                            int errorcode = result.ReplyMessage.reasonCode == "0" ? 0 : -1;

                            result.ReplyMessage.reasonCode = reasonCode;
                            result.Description = message;
                            result.ReplyMessage.decision = decision;
                            result.GeneralErrorCode = errorcode;

                            int paymentStep = capture != "1" ? 5 : 7;
                            int paymentStatus =
                                result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") && paymentStep.Equals(5) ? 1 :
                                            result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") && paymentStep.Equals(7) ? 3 :
                                            result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;


                            transaction.UpdatePaymentData(
                                referenceId,
                                paymentStep,
                                paymentStatus,
                                routing.ProviderCode,
                                result.ReplyMessage.reasonCode,
                                result.ReplyMessage.requestID,
                                result.ReplyMessage.requestToken,
                                errorcode,
                                message);

                            //transaction.UpdatePaymentDataValidate(referenceId, paymentStep, paymentStatus, routing.ProviderCode, "100", null, null, 0, "Payment Process Start , Authenticate Success");

                            //transaction.UpdateRlxPaymentDataValidate(referenceId, paymentStep, paymentStatus, routing.ProviderCode, reasonCode, result.ResultPASRef, result.ResultAuthCode, errorcode, message);


                            //int paymentStep = capture != "1" ? 5 : 7;
                            //int paymentStatus =
                            //    result.ReplyMessage.decision.ToUpper().Equals("APPROVAL") && paymentStep.Equals(5) ? 1 :
                            //    result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") && paymentStep.Equals(7) ? 3 :
                            //    result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                            //transaction.UpdatePaymentData(
                            //    referenceId,
                            //    paymentStep,
                            //    paymentStatus,
                            //    routing.ProviderCode,
                            //    result.ReplyMessage.reasonCode,
                            //    result.ReplyMessage.requestID,
                            //    result.ReplyMessage.requestToken,
                            //    result.ReplyMessage.reasonCode,
                            //    result.ProviderMessage);
                        }
                        catch (Exception)
                        {
                            result.GeneralErrorCode = -18;
                            result.Description = "Failed updating payment status";
                        }
                        #endregion

                        //TODO : Commented for Testing
                        #region Delete subscription data when status reject
                        ///*
                        // * REF_REASON_CODE = -20:Failed inserting subscription data into database
                        // */
                        //try
                        //{
                        //    if (result.ReplyMessage.decision.ToUpper().Equals("REJECT"))
                        //    {
                        //        subscription.RemoveSubscription(subscription.subscriptionID, routing.ProviderCode);
                        //    }
                        //}
                        //catch (Exception)
                        //{
                        //    result.GeneralErrorCode = -28;
                        //    result.Description = string.Format("Failed to delete subscription data into database for transaction \"{0}\"!", referenceId);
                        //}
                        #endregion

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed updating payment status " + ex.Message;
                    }
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling realex service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                return result;
            }
            // Return the operation result
            return result;

            #region Commented CyberSource Code
            //    /*
            //     * Call Cybersource service
            //     * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
            //     * REF_PAYMENT_STATUS = 2:On Manual Review
            //     */
            //    try
            //    {
            //        // Card info
            //        Card card = new Card();
            //        //card.accountNumber = ccNo;
            //        card.cvNumber = ccCvv;
            //        card.cvIndicator = "1";
            //        //card.expirationYear = expDate.Substring(0, 4);
            //        //card.expirationMonth = expDate.Substring(4, 2);
            //        // Payment info
            //        PurchaseTotals payment = new PurchaseTotals();
            //        payment.currency = siteCode.Equals("FR1") ? "EUR" : currency; // TODO: <Developer> Hardcoded to EUR currency if the request from Chillitalk France (sitecode = FR1)
            //        payment.grandTotalAmount = amount;

            //        // Run transaction
            //        RealExPayment soap = new RealExPayment();
            //        result = soap.Subscription_Authorize(
            //            referenceId,
            //            accountId,
            //            subscription.subscriptionID,
            //            payment,
            //            capture == "1",
            //            BusinessFacade.DecisionManager,
            //            routing.MerchantID,
            //            card, country, applicationCode);

            //        #region Get provider error description
            //        /*
            //         * REF_REASON_CODE = -17:Failed loading provider message
            //         */
            //        csReasonCode reason = new csReasonCode();
            //        try
            //        {
            //            if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
            //            {
            //                result.GeneralErrorCode = reason.generalErrorCode;
            //                result.ProviderMessage = reason.providerMessage;
            //            }
            //            else
            //            {
            //                result.GeneralErrorCode = -17;
            //                result.Description = "Failed loading provider message";
            //            }
            //        }
            //        catch (Exception) { }
            //        #endregion
            //        #region Update payment process
            //        /*
            //         * REF_REASON_CODE    = -18:Failed updating payment status/475:The customer is enrolled in Payer Authentication
            //         * REF_PAYMENT_STEP   = 5:Authorize/7:Authorize - Capture
            //         * REF_PAYMENT_STATUS = 1:In Process/3:Accepted/-1:Rejected/-2:Error)
            //         */
            //        try
            //        {
            //            int paymentStep = capture != "1" ? 5 : 7;
            //            int paymentStatus =
            //                result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") && paymentStep.Equals(5) ? 1 :
            //                result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") && paymentStep.Equals(7) ? 3 :
            //                result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
            //            transaction.UpdatePaymentData(
            //                referenceId,
            //                paymentStep,
            //                paymentStatus,
            //                routing.ProviderCode,
            //                result.ReplyMessage.reasonCode,
            //                result.ReplyMessage.requestID,
            //                result.ReplyMessage.requestToken,
            //                reason.generalErrorCode,
            //                result.ProviderMessage);
            //        }
            //        catch (Exception)
            //        {
            //            result.GeneralErrorCode = -18;
            //            result.Description = "Failed updating payment status";
            //        }
            //        #endregion

            //        #region Delete subscription data when status reject
            //        /*
            //             * REF_REASON_CODE = -20:Failed inserting subscription data into database
            //             */
            //        try
            //        {
            //            if (result.ReplyMessage.decision.ToUpper().Equals("REJECT"))
            //            {                            
            //                subscription.RemoveSubscription(
            //                    subscription.subscriptionID,
            //                    routing.ProviderCode);                           
            //            }
            //        }
            //        catch (Exception)
            //        {
            //            result.GeneralErrorCode = -28;
            //            result.Description = string.Format("Failed to delete subscription data into database for transaction \"{0}\"!", referenceId);
            //        }
            //        #endregion
            //    }
            //    catch (Exception ex)
            //    {
            //        if (result == null) result = new TransactionReply();
            //        throw new PaymentException(-16, result.ReplyMessage == null ?
            //            string.Format("Failed calling Cybersource service, {0}", (ex.InnerException ?? ex).Message) :
            //            string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
            //    result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
            //    result.Description = (ex.InnerException ?? ex).Message;
            //    return result;
            //}

            //// Return the operation result
            //return result; 
            #endregion
        }

        /// <summary>
        /// Subscription capture.
        /// </summary>

        //06-Jun-2017 : Modified : For Subscription
        /// <summary>
        /// Subscription payment.
        /// </summary>
        public static TransactionReply Payment(
            string siteCode, string applicationCode, string productCode, string serviceType, string paymentMode,
            string accountId, string cvv,
            string amount, string currency)
        {
            TransactionReply result = new TransactionReply();
            string cannotContinue = "The authorization process completed but the operation cannot continue due to error: {0}";

            List<string> missingParam = new List<string>();
            RoutingPayment routing;
            Subscriptions subscription;
            csTransaction transaction;
            string referenceId;
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(siteCode)) missingParam.Add("site-code");
                if (string.IsNullOrEmpty(applicationCode)) missingParam.Add("application-code");
                if (string.IsNullOrEmpty(productCode)) missingParam.Add("product-code");
                if (string.IsNullOrEmpty(serviceType)) missingParam.Add("service-type");
                if (string.IsNullOrEmpty(paymentMode)) missingParam.Add("payment-mode");
                if (string.IsNullOrEmpty(accountId)) missingParam.Add("account-id");
                if (string.IsNullOrEmpty(cvv)) missingParam.Add("cvv");
                if (string.IsNullOrEmpty(amount)) missingParam.Add("amount");
                if (string.IsNullOrEmpty(currency)) missingParam.Add("currency");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value: ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion
                #region Load routing data
                /*
                 * REF_REASON_CODE = -14:Routing data not found
                 */
                routing = new RoutingPayment();
                if (!routing.LoadRoutingData(siteCode, productCode))
                    throw new PaymentException(-14,
                        string.Format("Cannot find routing data for site-code \"{0}\" and product-code \"{1}\"", siteCode, productCode));
                #endregion
                #region Load subscription data
                /*
                 * REF_REASON_CODE = -13:Subscription data not found
                 */
                subscription = new Subscriptions();
                if (!subscription.LoadSubscriptionsData_Account(accountId, routing.ProviderCode))
                    throw new PaymentException(-13,
                        string.Format("Cannot find subscription data for account-id \"{0}\"", accountId));

                //Versi 13 jan 2014
                //if (!subscription.LoadSubscriptionsData_byCVV(accountId, cvv, routing.ProviderCode))
                //    throw new PaymentException(-13,
                //        string.Format("Cannot find subscription data for account-id \"{0}\" and cvv \"{1}\"", accountId, cvv));
                #endregion

                #region Get IP Payment Agent
                //System.Web.HttpContext context = System.Web.HttpContext.Current;
                //string IpPaymentAgent = (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? context.Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();

                //string ips = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList.GetValue(0).ToString();
                string IpPaymentAgent = HttpContext.Current.Request.UserHostAddress;

                #endregion

                #region Validate transaction information
                /*
                 * REF_PAYMENT_STEP   = 1:Begin Process Payment
                 * REF_PAYMENT_STATUS = 0: New
                 * REF_REASON_CODE    = -10:Insert payment data for new payment failed
                 */
                int retError = 0;
                transaction = new csTransaction();
                try
                {
                    referenceId = transaction.InsertPaymentData(
                        siteCode,
                        applicationCode,
                        productCode,
                        applicationCode,
                        serviceType,
                        paymentMode,
                        1,
                        0,
                        accountId,
                        subscription.last6DigitsCC,
                        Convert.ToDouble(amount),
                        currency,
                        subscription.subscriptionID,
                        routing.MerchantID,
                        routing.ProviderCode,
                        "100",
                        0,
                        ref retError,
                        "N/A",
                        IpPaymentAgent,
                        "SMS Topup",
                        "Payment Reference Created",
                        "1",
                        "",
                        "")
                    .ToString();
                }
                catch (Exception ex)
                {
                    throw new PaymentException(-10,
                        string.Format("Insert payment data failed. {0}", (ex.InnerException ?? ex).Message));
                }
                #endregion

                /*
                 * Call Cybersource service
                 * REF_REASON_CODE    = -16:Failed calling Cybersource service/-1:The process may have completed its cycle but an internal error occurred
                 * REF_PAYMENT_STATUS = 2:On Manual Review
                 */
                try
                {

                    // Card info
                    Card card = new Card();
                    //card.accountNumber = ccNo;
                    card.cvNumber = cvv;
                    card.cvIndicator = "1";
                    //card.expirationYear = expDate.Substring(0, 4);
                    //card.expirationMonth = expDate.Substring(4, 2);


                    // Payment info
                    PurchaseTotals payment = new PurchaseTotals();
                    payment.currency = siteCode.Equals("FR1") ? "EUR" : currency; // TODO: <Developer> Hardcoded to EUR currency if the request from Chillitalk France (sitecode = FR1)
                    payment.grandTotalAmount = amount;

                    // Run transaction
                    RealExPayment soap = new RealExPayment();
                    result = soap.Subscription_Authorize(
                        referenceId,
                        accountId,
                        subscription.subscriptionID,
                        payment,
                        true,
                        BusinessFacade.DecisionManager,
                        routing.MerchantID,
                        card, "", applicationCode);

                    #region Get provider error description
                    /*
                     * REF_REASON_CODE = -17:Failed loading provider message
                     */
                    csReasonCode reason = new csReasonCode();
                    try
                    {
                        if (reason.LoadReasonCodeData(result.ReplyMessage.reasonCode, routing.ProviderCode))
                        {
                            result.GeneralErrorCode = reason.generalErrorCode;
                            result.ProviderMessage = reason.providerMessage;
                        }
                        else
                        {
                            result.GeneralErrorCode = -17;
                            result.Description = "Failed loading provider message";
                        }
                    }
                    catch (Exception) { }
                    #endregion
                    #region Update payment process
                    /*
                     * REF_REASON_CODE    = -18:Failed updating payment status/475:The customer is enrolled in Payer Authentication
                     * REF_PAYMENT_STEP   = 7:Authorize-Capture
                     * REF_PAYMENT_STATUS = 3:Accepted/-1:Rejected/-2:Error
                     */
                    try
                    {
                        int paymentStep = 7;
                        int paymentStatus =
                            result.ReplyMessage.decision.ToUpper().Equals("ACCEPT") ? 3 :
                            result.ReplyMessage.decision.ToUpper().Equals("REJECT") ? -1 : -2;
                        transaction.UpdatePaymentData(
                            referenceId,
                            paymentStep,
                            paymentStatus,
                            routing.ProviderCode,
                            result.ReplyMessage.reasonCode,
                            result.ReplyMessage.requestID,
                            result.ReplyMessage.requestToken,
                            reason.generalErrorCode,
                            result.ProviderMessage);
                    }
                    catch (Exception)
                    {
                        result.GeneralErrorCode = -18;
                        result.Description = "Failed updating payment status";
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    if (result == null) result = new TransactionReply();
                    throw new PaymentException(-16, result.ReplyMessage == null ?
                        string.Format("Failed calling Cybersource service, {0}", (ex.InnerException ?? ex).Message) :
                        string.Format(cannotContinue, (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                // REF_REASON_CODE = -1:The process may have completed its cycle but an internal error occurred
                result.GeneralErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.Description = (ex.InnerException ?? ex).Message;
                return result;
            }

            // Return the operation result
            return result;
        }
    }
}
