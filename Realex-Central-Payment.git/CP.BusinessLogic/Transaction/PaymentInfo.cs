﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CyberSource.Clients.SoapWebReference;

namespace CP.BusinessLogic.Transaction
{
    public class PaymentInfo
    {
        /// <summary>
        /// Gets billing information.
        /// </summary>
        public static BillTo GetBillInfo(string accountId)
        {
            // TODO: Get bill information data by account ID from the database. Currently hardcoded.
            BillTo result = new BillTo();
            result.firstName = "John";
            result.lastName = "Doe";
            result.street1 = "1295 Charleston Road";
            result.city = "Mountain View";
            result.state = "CA";
            result.postalCode = "94043";
            result.country = "US";
            result.email = "null@cybersource.com";
            result.ipAddress = "10.7.111.111";
            return result;
        }

        /// <summary>
        /// Gets credit card information.
        /// </summary>
        public static Card GetCardInfo(string accountId, string ccNo)
        {
            // TODO: Get card information data from the database. Currently hardcoded.
            Card result = new Card();
            result.accountNumber = ccNo;
            result.expirationMonth = "12";
            result.expirationYear = "2020";
            return result;
        }

        /// <summary>
        /// Gets the merchant reference code.
        /// </summary>
        /// <returns></returns>
        public static string GetReferenceId(string sSiteCode, string sProductCode, string sPaymentAgent, string sServiceType, string sPaymentMode, int iPaymentStep, int iPaymentStatus, string sAccountID, string sLast6DigitCCNo, double dTotalAmount, string sCurrency, string sSubscriptionID, string sMerchantID, string sProviderCode, string sCSReasonCode, int iGeneralErrorCode, ref csTransaction transaction)
        {
            try
            {
                return transaction.InsertPaymentData(sSiteCode, sProductCode, sPaymentAgent, sServiceType, sPaymentMode, iPaymentStep, iPaymentStatus, sAccountID, sLast6DigitCCNo, dTotalAmount, sCurrency, sSubscriptionID, sMerchantID, sProviderCode, sCSReasonCode, iGeneralErrorCode)
                    .ToString();
            }
            catch (Exception) { throw new Exception("Failed generating reference ID."); }
        }

        /// <summary>
        /// Gets subscription ID.
        /// </summary>
        public static string GetSubscriptionId(ref RoutingPayment routing, ref Subscriptions subscription)
        {
            try
            {
                if (routing.LoadRoutingData())
                {
                    if (subscription.LoadSubscriptionsData(subscription.accountID, subscription.last6DigitsCC, routing.ProviderCode))
                        return subscription.subscriptionID;
                    else
                        throw new Exception("Cannot find subscription information.");
                }
                else throw new Exception("Cannot load routing payment information.");
            }
            catch (Exception) { throw new Exception("Failed getting subscription information."); }
        }

        /// <summary>
        /// Gets purchase total information.
        /// </summary>
        public static PurchaseTotals GetPurchaseTotalInfo(string currency, string amount)
        {
            PurchaseTotals result = new PurchaseTotals();
            result.currency = currency;
            result.grandTotalAmount = amount;
            return result;
        }

        /// <summary>
        /// Gets payment information (token) from the specified reference ID.
        /// </summary>
        public static CP.DataType.PaymentData GetPaymentInfo(string referenceId, ref csTransaction transaction)
        {
            CP.DataType.PaymentData result = new CP.DataType.PaymentData();
            try
            {
                if (transaction.loadCSTransactionDataByReferenceIDAndPaymentStep(Convert.ToInt32(referenceId), 5)) // See table REF_PAYMENT_STEP
                {
                    result.Currency = transaction.currency;
                    result.Amount = transaction.amount.ToString();
                    result.AuthRequestId = transaction.requestID;
                    result.AuthRequestToken = transaction.requestToken;
                    return result;
                }
                else throw new Exception("Cannot find transaction data.");
            }
            catch (Exception) { throw new Exception("Failed getting transaction data."); }
        }
    }
}
