﻿using System;
using System.Collections.Generic;
using System.Text;
using Switchlab.DataAccessLayer;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;

namespace CP.BusinessLogic.DB
{
    public class Subscriptions
    {

        private const string SP_getSubscriptionDataAccount = "subscriptionGetData_by_account";
        private const string SP_getSubscriptionData = "subscriptionGetData";
        private const string SP_getSubscriptionData_fourdigit = "subscriptionGetDatalast4digit";
        private const string SP_getSubscriptionData_byCVV = "subscriptionGetData_by_cvv";
        private const string SP_createNewSubscription = "SubscriptionNew";
        private const string SP_validateSubscription = "SubscriptionValidate";
        private const string SP_createNewSubscription_withCVV = "SubscriptionNew_v2";
        private const string SP_RemoveSubscription = "SubscriptionRemove";
        

        #region privateField
        private string _accountID = string.Empty;
        private string _last6DigitsCC = string.Empty;
        private string _subscriptionID = string.Empty;
        private string _expiredDate = string.Empty;
        private string _purchaseDate = string.Empty;
        private string _providerCode = string.Empty;
        private string _cvv = string.Empty;
        #endregion

        #region publicField
        public string accountID { get { return _accountID; } set { _accountID = value; } }
        public string last6DigitsCC { get { return _last6DigitsCC; } set { _last6DigitsCC = value; } }
        public string subscriptionID { get { return _subscriptionID; } set { _subscriptionID = value; } }
        public string expiredDate { get { return _expiredDate; } set { _expiredDate = value; } }
        public string purchaseDate { get { return _purchaseDate; } set { _purchaseDate = value; } }
        public string providerCode { get { return _providerCode; } set { _providerCode = value; } }
        public string cvv { get { return _cvv; } set { _cvv = value; } }
        #endregion

        public Subscriptions() { }

        public Subscriptions(string sAccountID, string sLast6DigitsCC, string sProviderCode)
        {
            _accountID = sAccountID;
            _last6DigitsCC = sLast6DigitsCC;
            _providerCode = sProviderCode;
        }

        public bool RemoveSubscription(           
            string sSubscriptionID,
            string providerCode
            )
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(providerCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@SubscriptionID", SqlDbType.VarChar, 50, ParameterDirection.Input, sSubscriptionID);
                
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_RemoveSubscription);

                string sError = "";
                int result = 0;
                result = DAL.ExecuteNonQuery(cmd, out sError);

                if (result > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool CreateSubscription(
            string sAccountID
            , string sLast6DigitCC
            , string sSubscriptionID
            , string sExpiryDate
            , string sPurchaseDate
            , string sProviderCode
            )
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(_providerCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@account_id", SqlDbType.VarChar, 30, ParameterDirection.Input, sAccountID);
                DAL.AddParamToSQLCmd(cmd, "@last6digitscc", SqlDbType.VarChar, 6, ParameterDirection.Input, sLast6DigitCC);
                DAL.AddParamToSQLCmd(cmd, "@SubscriptionID", SqlDbType.VarChar, 50, ParameterDirection.Input, sSubscriptionID);
                DAL.AddParamToSQLCmd(cmd, "@expirydate", SqlDbType.VarChar, 6, ParameterDirection.Input, sExpiryDate);
                DAL.AddParamToSQLCmd(cmd, "@purchasedate", SqlDbType.VarChar, 50, ParameterDirection.Input, sPurchaseDate);
                DAL.AddParamToSQLCmd(cmd, "@provider_code", SqlDbType.VarChar, 6, ParameterDirection.Input, sProviderCode);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_createNewSubscription);

                string sError = "";
                int result = 0;
                result = DAL.ExecuteNonQuery(cmd, out sError);

                if (result > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool ValidateSubscription(
             string sAccountID
            , string sLast6DigitCC
            , string sSubscriptionID
            , string sExpiryDate
            , string sPurchaseDate
            , string sProviderCode
            , int payerrefError
            , int cardrefError
            )
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(_providerCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@account_id", SqlDbType.VarChar, 30, ParameterDirection.Input, sAccountID);
                DAL.AddParamToSQLCmd(cmd, "@last6digitscc", SqlDbType.VarChar, 6, ParameterDirection.Input, sLast6DigitCC);
                DAL.AddParamToSQLCmd(cmd, "@SubscriptionID", SqlDbType.VarChar, 50, ParameterDirection.Input, sSubscriptionID);
                DAL.AddParamToSQLCmd(cmd, "@expirydate", SqlDbType.VarChar, 6, ParameterDirection.Input, sExpiryDate);
                DAL.AddParamToSQLCmd(cmd, "@purchasedate", SqlDbType.VarChar, 50, ParameterDirection.Input, sPurchaseDate);
                DAL.AddParamToSQLCmd(cmd, "@provider_code", SqlDbType.VarChar, 6, ParameterDirection.Input, sProviderCode);
                DAL.AddParamToSQLCmd(cmd, "@payerefError", SqlDbType.Int, 20, ParameterDirection.Input, payerrefError);
                DAL.AddParamToSQLCmd(cmd, "@cardrefError", SqlDbType.Int, 20, ParameterDirection.Input, cardrefError);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_validateSubscription);

                string sError = "";
                int result = 0;
                result = DAL.ExecuteNonQuery(cmd, out sError);

                if (result > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool CreateSubscription(
            string sAccountID
            , string sLast6DigitCC
            , string sSubscriptionID
            , string sExpiryDate
            , string sPurchaseDate
            , string sProviderCode
            , string sCvv
            )
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(_providerCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@account_id", SqlDbType.VarChar, 50, ParameterDirection.Input, sAccountID);
                DAL.AddParamToSQLCmd(cmd, "@last6digitscc", SqlDbType.VarChar, 6, ParameterDirection.Input, sLast6DigitCC);
                DAL.AddParamToSQLCmd(cmd, "@SubscriptionID", SqlDbType.VarChar, 50, ParameterDirection.Input, sSubscriptionID);
                DAL.AddParamToSQLCmd(cmd, "@expirydate", SqlDbType.VarChar, 6, ParameterDirection.Input, sExpiryDate);
                DAL.AddParamToSQLCmd(cmd, "@purchasedate", SqlDbType.VarChar, 8, ParameterDirection.Input, sPurchaseDate);
                DAL.AddParamToSQLCmd(cmd, "@provider_code", SqlDbType.VarChar, 5, ParameterDirection.Input, sProviderCode);
                DAL.AddParamToSQLCmd(cmd, "@cvv_number", SqlDbType.VarChar, 5, ParameterDirection.Input, sCvv);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_createNewSubscription_withCVV);

                string sError = "";
                int result = 0;
                result = DAL.ExecuteNonQuery(cmd, out sError);

                if (result > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool LoadSubscriptionsData()
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(_providerCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@account_id", SqlDbType.VarChar, 30, ParameterDirection.Input, _accountID);
                DAL.AddParamToSQLCmd(cmd, "@provider_code", SqlDbType.VarChar, 6, ParameterDirection.Input, _providerCode);
                if (!_cvv.Equals(string.Empty))
                {
                    DAL.AddParamToSQLCmd(cmd, "@cvv_number", SqlDbType.VarChar, 5, ParameterDirection.Input, _cvv);
                    DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_getSubscriptionData_byCVV);
                }
                else if (_last6DigitsCC.Length == 4)
                {
                    DAL.AddParamToSQLCmd(cmd, "@last6digitscc", SqlDbType.VarChar, 6, ParameterDirection.Input, _last6DigitsCC);
                    DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_getSubscriptionData_fourdigit);
                }
                else
                {
                    DAL.AddParamToSQLCmd(cmd, "@last6digitscc", SqlDbType.VarChar, 6, ParameterDirection.Input, _last6DigitsCC);
                    DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_getSubscriptionData);
                }

                List<Subscriptions> data = new List<Subscriptions>();
                DAL.ExecuteReaderCmd<Subscriptions>(cmd, GenerateSubscriptionData<Subscriptions>, ref data);
                if (data != null)
                {
                    if (data.Capacity > 0)
                    {
                        copyToObject(data[0]);
                        //if (data != null && data.Capacity > 0)
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool LoadSubscriptionsDataAccount()
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(_providerCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@account_id", SqlDbType.VarChar, 30, ParameterDirection.Input, _accountID);
                DAL.AddParamToSQLCmd(cmd, "@provider_code", SqlDbType.VarChar, 6, ParameterDirection.Input, _providerCode);                              
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_getSubscriptionDataAccount);
               
                List<Subscriptions> data = new List<Subscriptions>();
                DAL.ExecuteReaderCmd<Subscriptions>(cmd, GenerateSubscriptionData<Subscriptions>, ref data);
                if (data != null)
                {
                    if (data.Capacity > 0)
                    {
                        copyToObject(data[0]);
                        //if (data != null && data.Capacity > 0)
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool LoadSubscriptionsData(string sAccountID, string sLast6DigitCC, string sProviderCode)
        {
            _accountID = sAccountID;
            _last6DigitsCC = sLast6DigitCC;
            _providerCode = sProviderCode;

            return LoadSubscriptionsData();
        }
        public bool LoadSubscriptionsData_Account(string sAccountID, string sProviderCode)
        {
            _accountID = sAccountID;
           // _last6DigitsCC = sLast6DigitCC;
            _providerCode = sProviderCode;

            return LoadSubscriptionsDataAccount();
        }
        public bool LoadSubscriptionsData_byCVV(string sAccountID, string sCvv, string sProviderCode)
        {
            _accountID = sAccountID;
            _cvv = sCvv;
            _providerCode = sProviderCode;

            return LoadSubscriptionsData();
        }

        public void copyToObject(Subscriptions Source)
        {
            if (Source != null)
            {
                _accountID = Source.accountID;
                _last6DigitsCC = Source.last6DigitsCC;
                _subscriptionID = Source.subscriptionID;
                _expiredDate = Source.expiredDate;
                _purchaseDate = Source.purchaseDate;
                _providerCode = Source.providerCode;
            }
        }

        private static void GenerateSubscriptionData<T>(IDataReader returnData, ref List<Subscriptions> listNew)
        {

            try
            {
                while (returnData.Read())
                {
                    Subscriptions newItem = new Subscriptions();
                    newItem.accountID = GeneralConverter.ToString(returnData["account_id"]);
                    newItem.last6DigitsCC = GeneralConverter.ToString(returnData["last6digitscc"]);
                    newItem.subscriptionID = GeneralConverter.ToString(returnData["subscriptionid"]);
                    newItem.expiredDate = GeneralConverter.ToString(returnData["expirydate"]);
                    newItem.purchaseDate = GeneralConverter.ToString(returnData["purchasedate"]);
                    newItem.providerCode = GeneralConverter.ToString(returnData["provider_code"]);
                    newItem.cvv = GeneralConverter.ToString(returnData["cvv_number"]);

                    listNew.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}
