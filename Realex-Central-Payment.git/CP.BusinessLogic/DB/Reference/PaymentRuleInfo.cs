﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CP.BusinessLogic.DB.Reference
{
    public class PaymentRuleInfo
    {        
        public string Site_code { get; set; }
        public int HS { get; set; }
        public int AFS { get; set; }
        public int ENR { get; set; }        
    }
}
