﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CP.BusinessLogic.DB.Reference
{
    public class ProductSKUInfo
    {
        public int ItemID { get; set; }
        public string ItemDescription { get; set; }
        public double ItemPrice { get; set; }
        public int ItemType { get; set; }
        public string ItemPriceTag { get; set; }
        public string ItemCurrency { get; set; }
        public double ItemVAT { get; set; }
        public string SiteID { get; set; }
        public string Note { get; set; }
        public int groupID { get; set; }
        public int groupSort { get; set; }
        public int intValue1 { get; set; }
        public string strValue1 { get; set; }
        public int svc_id { get; set; }
        public string ProductSKU { get; set; }
        public int bundleid { get; set; }
    }
}
