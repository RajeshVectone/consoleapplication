﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Switchlab.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CP.BusinessLogic.DB.Reference;

namespace CP.BusinessLogic.DB
{
    public class PaymentRule
    {        
        private const string SP_EnROLL = "payment_getEnroll";
        public static PaymentRuleInfo GetEnroll(string sitecode)
        {
            
            try
            {
                
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentCentralConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@sitecode", SqlDbType.VarChar, 32, ParameterDirection.Input, sitecode);
               

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_EnROLL);

                foreach (DataRow item in DAL.ExecuteQuery(cmd).Tables[0].Rows)
                {
                    return new PaymentRuleInfo()
                    {
                        Site_code = GeneralConverter.ToString(item["Site_code"]),
                        AFS = GeneralConverter.ToInt32(item["AFS"]),
                        HS = GeneralConverter.ToInt32(item["HS"]),
                        ENR = GeneralConverter.ToInt32(item["ENR"])
                    };
                }
            }
            catch (Exception) { }
            return new PaymentRuleInfo();
        }
    }
}
