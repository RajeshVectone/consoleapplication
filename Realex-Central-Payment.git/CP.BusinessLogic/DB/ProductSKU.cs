﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Switchlab.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CP.BusinessLogic.DB.Reference;

namespace CP.BusinessLogic.DB
{
    public class ProductSKU
    {
        private const string SP_GETSKU = "payment_getInfoSKU";

        public static ProductSKUInfo GetInfo(string productSKU, params string[] alternateSKUs)
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetWebPaymentConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@productSKU", SqlDbType.VarChar, 32, ParameterDirection.Input, productSKU);

                int i = 2;
                foreach (string item in alternateSKUs)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string paramName = string.Format("@productSKU{0}", i);
                        DAL.AddParamToSQLCmd(cmd, paramName, SqlDbType.VarChar, 32, ParameterDirection.Input, item);
                    }
                }

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_GETSKU);
                foreach (DataRow item in DAL.ExecuteQuery(cmd).Tables[0].Rows)
                {
                    return new ProductSKUInfo()
                    {
                        ItemID = GeneralConverter.ToInt32(item["ItemID"]),
                        ItemDescription = GeneralConverter.ToString(item["ItemDescription"]),
                        ItemPrice = GeneralConverter.ToDouble(item["ItemPrice"]),
                        ItemType = GeneralConverter.ToInt32(item["ItemType"]),
                        ItemPriceTag = GeneralConverter.ToString(item["ItemPriceTag"]),
                        ItemCurrency = GeneralConverter.ToString(item["ItemCurrency"]),
                        ItemVAT = GeneralConverter.ToDouble(item["ItemVAT"]),
                        SiteID = GeneralConverter.ToString(item["SiteID"]),
                        Note = GeneralConverter.ToString(item["Note"]),
                        groupID = GeneralConverter.ToInt32(item["groupID"]),
                        groupSort = GeneralConverter.ToInt32(item["groupSort"]),
                        intValue1 = GeneralConverter.ToInt32(item["intValue1"]),
                        strValue1 = GeneralConverter.ToString(item["strValue1"]),
                        svc_id = GeneralConverter.ToInt32(item["svc_id"]),
                        ProductSKU = GeneralConverter.ToString(item["ProductSKU"]),
                        bundleid = GeneralConverter.ToInt32(item["bundleid"])
                    };
                }
            }
            catch (Exception) { }
            return new ProductSKUInfo() { ItemDescription = productSKU, ItemPrice = 0, ItemVAT = 0 };
        }
    }
}
