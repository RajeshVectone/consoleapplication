﻿using System;
using System.Collections.Generic;
using System.Text;
using Switchlab.DataAccessLayer;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;
using System.Xml;

namespace CP.BusinessLogic.DB
{
    public class csTransaction
    {
        private const string SP_paymentInsert = "csPaymentInsert_v3_Rx";
        private const string SP_ReceiptInInsert = "RXReceipitInInsert";
        //private const string SP_paymentInsert = "csPaymentInsert_v3";
        private const string SP_PaymentAddDescription = "csPaymentAddDescription";
        private const string SP_paymentUpdate = "csPaymentUpdate";
        private const string SP_paymentUpdateValidate = "csPaymentUpdateValidate";
        private const string SP_paymentfraudScore = "farud_score_insert";
        private const string SP_paymentEci = "tcs_eci_insert";
        private const string SP_csTransactionGetByRefIDStep = "csTransactionGetByRefIDStep";
        private const string Default_ProviderCode = "CS";
        private const string SP_PaymentGetPayerPayMethod = "PaymentGetPayerPayMethod";

        #region privateField
        private string _referenceID;
        private int _paymentStep;
        private int _paymentStatus;
        private string _CSReasonCode = string.Empty;
        private DateTime _executionTime = DateTime.MinValue;
        private string _requestID = string.Empty;
        private string _requestToken = string.Empty;
        private string _providerCode = string.Empty;
        private int _generalErrorCode;

        private string _currency = string.Empty;
        private double _amount;
        #endregion

        #region publicField
        public string referenceID { get { return _referenceID; } set { _referenceID = value; } }
        public string Last6DigitsCC { get; set; }
        public int paymentStep { get { return _paymentStep; } set { _paymentStep = value; } }
        public int paymentStatus { get { return _paymentStatus; } set { _paymentStatus = value; } }
        public string CSReasonCode { get { return _CSReasonCode; } set { _CSReasonCode = value; } }
        public DateTime executionTime { get { return _executionTime; } set { _executionTime = value; } }
        public string requestID { get { return _requestID; } set { _requestID = value; } }
        public string requestToken { get { return _requestToken; } set { _requestToken = value; } }
        public string providerCode { get { return _providerCode; } set { _providerCode = value; } }
        public int generalErrorCode { get { return _generalErrorCode; } set { _generalErrorCode = value; } }

        public string currency { get { return _currency; } set { _currency = value; } }
        public double amount { get { return _amount; } set { _amount = value; } }
        #endregion

        public csTransaction() { }




        public string InsertReceiptInData(
            string sSiteCode
            , string sApplicationCode
            , string sProductCode
            , string sPaymentAgent
            , string sServiceType
            , string sPaymentMode
            , int iPaymentStep
            , int iPaymentStatus
            , string sAccountID
            , double dTotalAmount
            , string sCurrency
            , string sSubscriptionID
            , string sMerchantID
            , string sProviderCode
            , string sCSReasonCode
            , int iGeneralErrorCode
            , ref int iReturnError
            , string sIPPaymentAgent
            , string sIPUser
            , string srefid
            )
        {

            try
            {
                string refid = string.Empty;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(sProviderCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string sendToProduction = ConfigurationManager.AppSettings["cybs.sendToProduction"] ?? "true";

                DAL.AddParamToSQLCmd(cmd, "@sitecode", SqlDbType.VarChar, 5, ParameterDirection.Input, sSiteCode);
                DAL.AddParamToSQLCmd(cmd, "@productcode", SqlDbType.VarChar, 20, ParameterDirection.Input, sProductCode);
                if (!string.IsNullOrEmpty(sApplicationCode))
                    DAL.AddParamToSQLCmd(cmd, "@applicationcode", SqlDbType.VarChar, 15, ParameterDirection.Input, sApplicationCode);
                DAL.AddParamToSQLCmd(cmd, "@paymentagent", SqlDbType.VarChar, 12, ParameterDirection.Input, sPaymentAgent);
                DAL.AddParamToSQLCmd(cmd, "@servicetype", SqlDbType.VarChar, 4, ParameterDirection.Input, sServiceType);
                DAL.AddParamToSQLCmd(cmd, "@paymentmode", SqlDbType.VarChar, 3, ParameterDirection.Input, sPaymentMode);
                DAL.AddParamToSQLCmd(cmd, "@paymentstep", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStep);
                DAL.AddParamToSQLCmd(cmd, "@paymentstatus", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStatus);
                DAL.AddParamToSQLCmd(cmd, "@accountid", SqlDbType.VarChar, 32, ParameterDirection.Input, sAccountID);
                DAL.AddParamToSQLCmd(cmd, "@totalamount", SqlDbType.Float, 0, ParameterDirection.Input, dTotalAmount);
                DAL.AddParamToSQLCmd(cmd, "@currency", SqlDbType.VarChar, 3, ParameterDirection.Input, sCurrency);
                DAL.AddParamToSQLCmd(cmd, "@subscriptionid", SqlDbType.VarChar, 40, ParameterDirection.Input, sSubscriptionID);
                DAL.AddParamToSQLCmd(cmd, "@merchantid", SqlDbType.VarChar, 10, ParameterDirection.Input, sMerchantID);
                DAL.AddParamToSQLCmd(cmd, "@providercode", SqlDbType.VarChar, 5, ParameterDirection.Input, sProviderCode);
                DAL.AddParamToSQLCmd(cmd, "@csreasoncode", SqlDbType.VarChar, 30, ParameterDirection.Input, sCSReasonCode);
                DAL.AddParamToSQLCmd(cmd, "@generalerrorcode", SqlDbType.Int, 0, ParameterDirection.Input, iGeneralErrorCode);
                DAL.AddParamToSQLCmd(cmd, "@returnerror", SqlDbType.Int, 0, ParameterDirection.Output, null);
                DAL.AddParamToSQLCmd(cmd, "@sendToProduction", SqlDbType.VarChar, 5, ParameterDirection.Input, sendToProduction);
                DAL.AddParamToSQLCmd(cmd, "@IPpaymentagent", SqlDbType.VarChar, 20, ParameterDirection.Input, sIPPaymentAgent);
                DAL.AddParamToSQLCmd(cmd, "@IPClient", SqlDbType.VarChar, 20, ParameterDirection.Input, sIPUser);
                DAL.AddParamToSQLCmd(cmd, "@ref_id", SqlDbType.VarChar, 30, ParameterDirection.Input, srefid);



                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_ReceiptInInsert);
                DataSet dsPayment = DAL.ExecuteQuery(cmd);

                iReturnError = GeneralConverter.ToInt32(cmd.Parameters["@returnerror"].Value);
                refid = dsPayment.Tables[dsPayment.Tables.Count - 1].Rows[0][0].ToString();
                return refid;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }












        /// <summary>
        /// 
        /// </summary>
        /// <param name="sSiteCode"></param>
        /// <param name="sProductCode"></param>
        /// <param name="sPaymentAgent"></param>
        /// <param name="sServiceType"></param>
        /// <param name="sPaymentMode"></param>
        /// <param name="iPaymentStep"></param>
        /// <param name="iPaymentStatus"></param>
        /// <param name="sAccountID"></param>
        /// <param name="sLast6DigitCCNo"></param>
        /// <param name="dTotalAmount"></param>
        /// <param name="sCurrency"></param>
        /// <param name="sSubscriptionID"></param>
        /// <param name="sMerchantID"></param>
        /// <param name="sProviderCode"></param>
        /// <param name="sCSReasonCode">Reason code given by CS</param>
        /// <param name="iGeneralErrorCode">error code that's derived from CS Reasoncode</param>
        /// <param name="iReturnError">If there's any error during payment insertion</param>        
        /// <returns>Reference ID</returns>
        public string InsertPaymentData(
            string sSiteCode
            , string sApplicationCode
            , string sProductCode
            , string sPaymentAgent
            , string sServiceType
            , string sPaymentMode
            , int iPaymentStep
            , int iPaymentStatus
            , string sAccountID
            , string sLast6DigitCCNo
            , double dTotalAmount
            , string sCurrency
            , string sSubscriptionID
            , string sMerchantID
            , string sProviderCode
            , string sCSReasonCode
            , int iGeneralErrorCode
            , ref int iReturnError
            , string sEmail
            , string sIPPaymentAgent
            , string sIPUser
            , string sReplyMessage
            , string sThreeDFlag
            , string expirydate
            , string cardtype
            , string device_type = ""
            , string browser = ""
            , string os_version = ""
            , string topup_url = ""
            , int bundle_id = 0
            )
        {
            try
            {
                string refid = string.Empty;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(sProviderCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string sendToProduction = ConfigurationManager.AppSettings["cybs.sendToProduction"] ?? "true";

                DAL.AddParamToSQLCmd(cmd, "@sitecode", SqlDbType.VarChar, 5, ParameterDirection.Input, sSiteCode);
                DAL.AddParamToSQLCmd(cmd, "@productcode", SqlDbType.VarChar, 20, ParameterDirection.Input, sProductCode);
                if (!string.IsNullOrEmpty(sApplicationCode))
                    DAL.AddParamToSQLCmd(cmd, "@applicationcode", SqlDbType.VarChar, 15, ParameterDirection.Input, sApplicationCode);
                DAL.AddParamToSQLCmd(cmd, "@paymentagent", SqlDbType.VarChar, 12, ParameterDirection.Input, sPaymentAgent);
                DAL.AddParamToSQLCmd(cmd, "@servicetype", SqlDbType.VarChar, 4, ParameterDirection.Input, sServiceType);
                DAL.AddParamToSQLCmd(cmd, "@paymentmode", SqlDbType.VarChar, 3, ParameterDirection.Input, sPaymentMode);
                DAL.AddParamToSQLCmd(cmd, "@paymentstep", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStep);
                DAL.AddParamToSQLCmd(cmd, "@paymentstatus", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStatus);
                DAL.AddParamToSQLCmd(cmd, "@accountid", SqlDbType.VarChar, 32, ParameterDirection.Input, sAccountID);
                DAL.AddParamToSQLCmd(cmd, "@last6digitccno", SqlDbType.VarChar, 6, ParameterDirection.Input, sLast6DigitCCNo);
                DAL.AddParamToSQLCmd(cmd, "@totalamount", SqlDbType.Float, 0, ParameterDirection.Input, dTotalAmount);
                DAL.AddParamToSQLCmd(cmd, "@currency", SqlDbType.VarChar, 3, ParameterDirection.Input, sCurrency);
                DAL.AddParamToSQLCmd(cmd, "@subscriptionid", SqlDbType.VarChar, 40, ParameterDirection.Input, sSubscriptionID);
                DAL.AddParamToSQLCmd(cmd, "@merchantid", SqlDbType.VarChar, 10, ParameterDirection.Input, sMerchantID);
                DAL.AddParamToSQLCmd(cmd, "@providercode", SqlDbType.VarChar, 5, ParameterDirection.Input, sProviderCode);
                DAL.AddParamToSQLCmd(cmd, "@csreasoncode", SqlDbType.VarChar, 30, ParameterDirection.Input, sCSReasonCode);
                DAL.AddParamToSQLCmd(cmd, "@generalerrorcode", SqlDbType.Int, 0, ParameterDirection.Input, iGeneralErrorCode);
                DAL.AddParamToSQLCmd(cmd, "@returnerror", SqlDbType.Int, 0, ParameterDirection.Output, null);
                DAL.AddParamToSQLCmd(cmd, "@sendToProduction", SqlDbType.VarChar, 5, ParameterDirection.Input, sendToProduction);
                DAL.AddParamToSQLCmd(cmd, "@email", SqlDbType.VarChar, 100, ParameterDirection.Input, sEmail);
                DAL.AddParamToSQLCmd(cmd, "@IPpaymentagent", SqlDbType.VarChar, 20, ParameterDirection.Input, sIPPaymentAgent);
                DAL.AddParamToSQLCmd(cmd, "@IPClient", SqlDbType.VarChar, 20, ParameterDirection.Input, sIPUser);
                DAL.AddParamToSQLCmd(cmd, "@ReplyMessage", SqlDbType.VarChar, 512, ParameterDirection.Input, sReplyMessage);
                DAL.AddParamToSQLCmd(cmd, "@ThreeDFlag", SqlDbType.Char, 1, ParameterDirection.Input, sThreeDFlag);
                DAL.AddParamToSQLCmd(cmd, "@device_type", SqlDbType.VarChar, 50, ParameterDirection.Input, device_type);
                DAL.AddParamToSQLCmd(cmd, "@browser", SqlDbType.VarChar, 100, ParameterDirection.Input, browser);
                DAL.AddParamToSQLCmd(cmd, "@os_version", SqlDbType.VarChar, 100, ParameterDirection.Input, os_version);
                DAL.AddParamToSQLCmd(cmd, "@topup_url", SqlDbType.VarChar, 500, ParameterDirection.Input, topup_url);
                DAL.AddParamToSQLCmd(cmd, "@bundle_id", SqlDbType.Int, 0, ParameterDirection.Input, bundle_id);
                DAL.AddParamToSQLCmd(cmd, "@expirydate", SqlDbType.VarChar, 10, ParameterDirection.Input, expirydate);
                DAL.AddParamToSQLCmd(cmd, "@cardtype", SqlDbType.VarChar, 20, ParameterDirection.Input, cardtype);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_paymentInsert);
                DataSet dsPayment = DAL.ExecuteQuery(cmd);

                iReturnError = GeneralConverter.ToInt32(cmd.Parameters["@returnerror"].Value);
                refid = dsPayment.Tables[dsPayment.Tables.Count - 1].Rows[0][0].ToString();
                return refid;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="referenceId"></param>
        /// <param name="description">All description in XML format</param>
        public bool AddPaymentDescription(string referenceId, string sProviderCode, XmlDocument description)
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(sProviderCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@reference_id", SqlDbType.VarChar, 32, ParameterDirection.Input, referenceId);
                DAL.AddParamToSQLCmd(cmd, "@description", SqlDbType.VarChar, 2048, ParameterDirection.Input, description.InnerXml);
                DAL.AddParamToSQLCmd(cmd, "@errcode", SqlDbType.Int, 4, ParameterDirection.Output, -1);
                DAL.AddParamToSQLCmd(cmd, "@errmessage", SqlDbType.VarChar, 256, ParameterDirection.Output, string.Empty);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_PaymentAddDescription);
                DAL.ExecuteNonQuery(cmd);

                return cmd.Parameters["@errcode"].Value.Equals(0);
            }
            catch (Exception) { }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iReferenceID"></param>
        /// <param name="iPaymentStep"></param>
        /// <param name="iPaymentStatus"></param>
        /// <param name="sProviderCode"></param>
        /// <param name="sCSReasonCode">Reason code given by CS</param>
        /// <param name="sRequestID"></param>
        /// <param name="sRequestToken"></param>        
        /// <param name="iGeneralErrorCode">error code that's derived from CS Reasoncode</param>
        /// <returns></returns>

        public bool UpdatePaymentDataValidate(
            string iReferenceID
            , int iPaymentStep
            , int iPaymentStatus
            , string sProviderCode
            , string sCSReasonCode
            , string sRequestID
            , string sRequestToken
            , int iGeneralErrorCode
            , string ReplyMessage
            , string ThreeDStatus = ""
            , string SRD = "")
        {
            try
            {
                int result = 0;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(sProviderCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@referenceid", SqlDbType.VarChar, 32, ParameterDirection.Input, iReferenceID);
                DAL.AddParamToSQLCmd(cmd, "@paymentstep", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStep);
                DAL.AddParamToSQLCmd(cmd, "@paymentstatus", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStatus);
                DAL.AddParamToSQLCmd(cmd, "@providercode", SqlDbType.VarChar, 5, ParameterDirection.Input, sProviderCode);
                DAL.AddParamToSQLCmd(cmd, "@csreasoncode", SqlDbType.VarChar, 30, ParameterDirection.Input, sCSReasonCode);
                DAL.AddParamToSQLCmd(cmd, "@requestid", SqlDbType.VarChar, 64, ParameterDirection.Input, DBNull.Value);
                DAL.AddParamToSQLCmd(cmd, "@requesttoken", SqlDbType.VarChar, 512, ParameterDirection.Input, DBNull.Value);
                DAL.AddParamToSQLCmd(cmd, "@generalerrorcode", SqlDbType.Int, 0, ParameterDirection.Input, iGeneralErrorCode);
                DAL.AddParamToSQLCmd(cmd, "@ReplyMessage", SqlDbType.VarChar, 512, ParameterDirection.Input, ReplyMessage);
                DAL.AddParamToSQLCmd(cmd, "@3d_status ", SqlDbType.Char, 1, ParameterDirection.Input, ThreeDStatus);
                //24-Aug-2019 : Moorthy added for storing SRD value
                DAL.AddParamToSQLCmd(cmd, "@srd ", SqlDbType.VarChar, 100, ParameterDirection.Input, SRD);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_paymentUpdateValidate);
                result = GeneralConverter.ToInt32(DAL.ExecuteScalarCmd(cmd));
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iReferenceID"></param>
        /// <param name="iPaymentStep"></param>
        /// <param name="iPaymentStatus"></param>
        /// <param name="sProviderCode"></param>
        /// <param name="sCSReasonCode">Reason code given by CS</param>
        /// <param name="sRequestID"></param>
        /// <param name="sRequestToken"></param>        
        /// <param name="iGeneralErrorCode">error code that's derived from CS Reasoncode</param>
        /// <returns></returns>

        public bool UpdateRlxPaymentDataValidate(
            string iReferenceID
            , int iPaymentStep
            , int iPaymentStatus
            , string sProviderCode
            , string sCSReasonCode
            , string sRequestID
            , string sRequestToken
            , int iGeneralErrorCode
            , string ReplyMessage
            , string ThreeDStatus
            , string SRD
            )
        {
            try
            {
                int result = 0;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(sProviderCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@referenceid", SqlDbType.VarChar, 32, ParameterDirection.Input, iReferenceID);
                DAL.AddParamToSQLCmd(cmd, "@paymentstep", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStep);
                DAL.AddParamToSQLCmd(cmd, "@paymentstatus", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStatus);
                DAL.AddParamToSQLCmd(cmd, "@providercode", SqlDbType.VarChar, 5, ParameterDirection.Input, sProviderCode);
                DAL.AddParamToSQLCmd(cmd, "@csreasoncode", SqlDbType.VarChar, 30, ParameterDirection.Input, sCSReasonCode);
                DAL.AddParamToSQLCmd(cmd, "@requestid", SqlDbType.VarChar, 64, ParameterDirection.Input, sRequestID);
                DAL.AddParamToSQLCmd(cmd, "@requesttoken", SqlDbType.VarChar, 512, ParameterDirection.Input, sRequestToken);
                DAL.AddParamToSQLCmd(cmd, "@generalerrorcode", SqlDbType.Int, 0, ParameterDirection.Input, iGeneralErrorCode);
                DAL.AddParamToSQLCmd(cmd, "@ReplyMessage", SqlDbType.VarChar, 512, ParameterDirection.Input, ReplyMessage);
                DAL.AddParamToSQLCmd(cmd, "@3d_status ", SqlDbType.Char, 1, ParameterDirection.Input, ThreeDStatus);
                //24-Aug-2019 : Moorthy added for storing SRD value
                DAL.AddParamToSQLCmd(cmd, "@srd ", SqlDbType.VarChar, 100, ParameterDirection.Input, SRD);
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_paymentUpdateValidate);
                result = GeneralConverter.ToInt32(DAL.ExecuteScalarCmd(cmd));
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool FroudScore(string referenceId, string score)
        {
            try
            {
                int result = 0;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentFroudConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@requestid", SqlDbType.VarChar, 32, ParameterDirection.Input, DBNull.Value);
                DAL.AddParamToSQLCmd(cmd, "@referenceid", SqlDbType.VarChar, 32, ParameterDirection.Input, referenceId);
                DAL.AddParamToSQLCmd(cmd, "@score", SqlDbType.VarChar, 0, ParameterDirection.Input, score);
                //DAL.AddParamToSQLCmd(cmd, "@eci_value", SqlDbType.VarChar, 0, ParameterDirection.Input, ecivalue);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_paymentfraudScore);
                result = GeneralConverter.ToInt32(DAL.ExecuteScalarCmd(cmd));
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool FroudScore_Eci(string referenceId, string score, string eci)
        {
            try
            {
                int result = 0;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentFroudConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@requestid", SqlDbType.VarChar, 32, ParameterDirection.Input, DBNull.Value);
                DAL.AddParamToSQLCmd(cmd, "@referenceid", SqlDbType.VarChar, 32, ParameterDirection.Input, referenceId);
                DAL.AddParamToSQLCmd(cmd, "@score", SqlDbType.VarChar, 0, ParameterDirection.Input, score);
                DAL.AddParamToSQLCmd(cmd, "@tcs_eci_val", SqlDbType.VarChar, 0, ParameterDirection.Input, eci);
                //DAL.AddParamToSQLCmd(cmd, "@eci_value", SqlDbType.VarChar, 0, ParameterDirection.Input, ecivalue);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_paymentfraudScore);
                result = GeneralConverter.ToInt32(DAL.ExecuteScalarCmd(cmd));
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool EciValue(string referenceId, string cardname, string ecivalue)
        {
            try
            {
                int result = 0;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentFroudConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();

                int eci = default(int);
                bool results = Int32.TryParse(ecivalue, out eci);
                if (ecivalue == null)
                {
                    ecivalue = "0";
                }

                DAL.AddParamToSQLCmd(cmd, "@reference_id", SqlDbType.VarChar, 32, ParameterDirection.Input, referenceId);
                DAL.AddParamToSQLCmd(cmd, "@eci", SqlDbType.Int, 0, ParameterDirection.Input, eci);
                DAL.AddParamToSQLCmd(cmd, "@card_name", SqlDbType.VarChar, 32, ParameterDirection.Input, DBNull.Value);
                //DAL.AddParamToSQLCmd(cmd, "@eci_value", SqlDbType.VarChar, 0, ParameterDirection.Input, ecivalue);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_paymentEci);
                result = GeneralConverter.ToInt32(DAL.ExecuteScalarCmd(cmd));
                if (result == 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatePaymentData(
            string iReferenceID
            , int iPaymentStep
            , int iPaymentStatus
            , string sProviderCode
            , string sCSReasonCode
            , string sRequestID
            , string sRequestToken
            , int iGeneralErrorCode
            , string ReplyMessage
            )
        {
            try
            {
                int result = 0;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(sProviderCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@referenceid", SqlDbType.VarChar, 32, ParameterDirection.Input, iReferenceID);
                DAL.AddParamToSQLCmd(cmd, "@paymentstep", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStep);
                DAL.AddParamToSQLCmd(cmd, "@paymentstatus", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStatus);
                DAL.AddParamToSQLCmd(cmd, "@providercode", SqlDbType.VarChar, 5, ParameterDirection.Input, sProviderCode);
                DAL.AddParamToSQLCmd(cmd, "@csreasoncode", SqlDbType.VarChar, 30, ParameterDirection.Input, sCSReasonCode);
                DAL.AddParamToSQLCmd(cmd, "@requestid", SqlDbType.VarChar, 64, ParameterDirection.Input, DBNull.Value);
                DAL.AddParamToSQLCmd(cmd, "@requesttoken", SqlDbType.VarChar, 512, ParameterDirection.Input, DBNull.Value);
                DAL.AddParamToSQLCmd(cmd, "@generalerrorcode", SqlDbType.Int, 0, ParameterDirection.Input, iGeneralErrorCode);
                DAL.AddParamToSQLCmd(cmd, "@ReplyMessage", SqlDbType.VarChar, 512, ParameterDirection.Input, ReplyMessage);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_paymentUpdate);
                result = GeneralConverter.ToInt32(DAL.ExecuteScalarCmd(cmd));
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool loadCSTransactionDataByReferenceIDAndPaymentStep(
            string iReferenceID
            , int iPaymentStep)
        {
            return loadCSTransactionDataByReferenceIDAndPaymentStep(iReferenceID, iPaymentStep, null);
        }

        public bool loadCSTransactionDataByReferenceIDAndPaymentStep(
            string iReferenceID
            , int iPaymentStep
            , string sProviderCode
            )
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(sProviderCode ?? Default_ProviderCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@referenceid", SqlDbType.VarChar, 32, ParameterDirection.Input, iReferenceID);
                DAL.AddParamToSQLCmd(cmd, "@paymentstep", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStep);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_csTransactionGetByRefIDStep);
                List<csTransaction> data = new List<csTransaction>();
                DAL.ExecuteReaderCmd<csTransaction>(cmd, GenerateCSTransactionData<csTransaction>, ref data);
                if (data != null)
                {
                    if (data.Capacity > 0)
                    {
                        copyToObject(data[0]);
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void copyToObject(csTransaction Source)
        {
            if (Source != null)
            {
                _referenceID = Source.referenceID;
                _CSReasonCode = Source.CSReasonCode;
                _executionTime = Source.executionTime;
                _paymentStep = Source.paymentStep;
                _paymentStatus = Source.paymentStatus;
                _requestID = Source.requestID;
                _requestToken = Source.requestToken;
                _generalErrorCode = Source.generalErrorCode;
                _currency = Source.currency;
                _amount = Source.amount;
            }
        }

        private static void GenerateCSTransactionData<T>(IDataReader returnData, ref List<csTransaction> listNew)
        {
            try
            {
                while (returnData.Read())
                {
                    csTransaction newItem = new csTransaction();
                    newItem.referenceID = GeneralConverter.ToString(returnData["REFERENCE_ID"]);
                    newItem.Last6DigitsCC = GeneralConverter.ToString(returnData["LAST6DIGITSCC"]);
                    newItem.CSReasonCode = GeneralConverter.ToString(returnData["CS_ERROR_CODE"]);
                    newItem.executionTime = GeneralConverter.ToDateTime(returnData["EXECUTION_DATE"]);
                    newItem.paymentStep = GeneralConverter.ToInt32(returnData["PAYMENT_STEP"]);
                    newItem.paymentStatus = GeneralConverter.ToInt32(returnData["PAYMENT_STATUS"]);
                    newItem.requestID = GeneralConverter.ToString(returnData["REQUEST_ID"]);
                    newItem.requestToken = GeneralConverter.ToString(returnData["REQUEST_TOKEN"]);
                    newItem.generalErrorCode = GeneralConverter.ToInt32(returnData["GENERAL_ERROR_CODE"]);
                    newItem.currency = GeneralConverter.ToString(returnData["CURRENCY"]);
                    newItem.amount = GeneralConverter.ToDouble(returnData["TOTALAMOUNT"]);
                    listNew.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //DB Insert
        public static void INSERTPAYMENTLOGDETAILS(string sitecode, string mobileno, string unique_code, string payref, string step, string reason_code, string comments, string cc_no, string ipclient)
        {
            try
            {
                int result = 0;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString("Mobilecmsapi").ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@sitecode", SqlDbType.VarChar, 5, ParameterDirection.Input, sitecode);
                DAL.AddParamToSQLCmd(cmd, "@mobileno", SqlDbType.VarChar, 20, ParameterDirection.Input, mobileno);
                DAL.AddParamToSQLCmd(cmd, "@unique_code", SqlDbType.VarChar, 50, ParameterDirection.Input, unique_code);
                DAL.AddParamToSQLCmd(cmd, "@pay_ref", SqlDbType.VarChar, 50, ParameterDirection.Input, payref);
                DAL.AddParamToSQLCmd(cmd, "@step", SqlDbType.VarChar, 5, ParameterDirection.Input, step);
                DAL.AddParamToSQLCmd(cmd, "@reason_code", SqlDbType.VarChar, 150, ParameterDirection.Input, reason_code);
                DAL.AddParamToSQLCmd(cmd, "@comments", SqlDbType.VarChar, 150, ParameterDirection.Input, comments);
                DAL.AddParamToSQLCmd(cmd, "@cc_no", SqlDbType.VarChar, 6, ParameterDirection.Input, cc_no);
                DAL.AddParamToSQLCmd(cmd, "@ip_client", SqlDbType.VarChar, 50, ParameterDirection.Input, ipclient);
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, "website_central_insert_payment_info");


                result = GeneralConverter.ToInt32(DAL.ExecuteScalarCmd(cmd));

            }
            catch (Exception ex)
            {

            }
        }

        //06-Aug-2018 : Moorthy Added to avoid 3DS Enrolled duplicate reference
        public string Validate3dsEnrolled(string account_id, string cc_no, string sitecode, string payment_agent, string service_type, string amount)
        {
            try
            {
                string refid = string.Empty;
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString("CS").ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@account_id", SqlDbType.VarChar, 32, ParameterDirection.Input, account_id);
                DAL.AddParamToSQLCmd(cmd, "@cc_no", SqlDbType.VarChar, 6, ParameterDirection.Input, cc_no);
                DAL.AddParamToSQLCmd(cmd, "@sitecode", SqlDbType.VarChar, 5, ParameterDirection.Input, sitecode);
                DAL.AddParamToSQLCmd(cmd, "@payment_agent", SqlDbType.VarChar, 12, ParameterDirection.Input, payment_agent);
                DAL.AddParamToSQLCmd(cmd, "@service_type", SqlDbType.VarChar, 4, ParameterDirection.Input, service_type);
                DAL.AddParamToSQLCmd(cmd, "@amount", SqlDbType.Float, 8, ParameterDirection.Input, amount);
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, "sp_validate_3ds_enrolled");
                DataSet dsPayment = DAL.ExecuteQuery(cmd);
                if (dsPayment.Tables.Count > 0 && dsPayment.Tables[0].Rows.Count > 0)
                    refid = dsPayment.Tables[0].Rows[0][0].ToString();
                return refid;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        //21-Feb-2019 : Moorthy : Added for Restriction based on 3DSecure results 
        public static int DoTransaction(string cc_no, string referenceId, string mobileno)
        {
            int result = -1;
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString("CS").ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@last6digit", SqlDbType.VarChar, 6, ParameterDirection.Input, cc_no);
                DAL.AddParamToSQLCmd(cmd, "@reference_id", SqlDbType.VarChar, 50, ParameterDirection.Input, referenceId);
                DAL.AddParamToSQLCmd(cmd, "@mobileno ", SqlDbType.VarChar, 50, ParameterDirection.Input, mobileno);
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, "paymentgateway_check_cardcount");
                result = GeneralConverter.ToInt32(DAL.ExecuteScalarCmd(cmd));
            }
            catch
            {
                result = -1;
            }
            return result;
        }

        //22-Feb-2019 : Moorthy : Added for Payment Gateway Check Rule
        public string PaymentGatewayCheckRule(string cc_no, double amount, string mobileno, string ipaddress, string card_type, string called_by, ref string errmsg)
        {
            CP.BusinessLogic.CyberSource.Log.Write("PaymentGatewayCheckRule : Input : " + cc_no + " , " + amount + " , " + mobileno + " , " + ipaddress + " , " + card_type + " , " + called_by);
            string refid = "-1";
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString("CS").ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@cc_no", SqlDbType.VarChar, 6, ParameterDirection.Input, cc_no);
                DAL.AddParamToSQLCmd(cmd, "@amount", SqlDbType.Float, 8, ParameterDirection.Input, amount);
                DAL.AddParamToSQLCmd(cmd, "@mobileno ", SqlDbType.VarChar, 20, ParameterDirection.Input, mobileno);
                DAL.AddParamToSQLCmd(cmd, "@ipaddress", SqlDbType.VarChar, 50, ParameterDirection.Input, ipaddress);
                DAL.AddParamToSQLCmd(cmd, "@card_type", SqlDbType.VarChar, 50, ParameterDirection.Input, card_type);
                DAL.AddParamToSQLCmd(cmd, "@called_by", SqlDbType.VarChar, 25, ParameterDirection.Input, called_by);
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, "payment_gateway_check_rule_v2");
                DataSet dsPayment = DAL.ExecuteQuery(cmd);
                if (dsPayment.Tables != null && dsPayment.Tables.Count > 0 && dsPayment.Tables[0].Rows.Count > 0)
                {
                    refid = dsPayment.Tables[0].Rows[0]["rule_status"].ToString();
                    errmsg = dsPayment.Tables[0].Rows[0]["payment_message"].ToString();
                }
                //CP.BusinessLogic.CyberSource.Log.Write2("PaymentGatewayCheckRule : Output : " + refid + " ," + errmsg);
            }
            catch (Exception ex)
            {
                CP.BusinessLogic.CyberSource.Log.Write("PaymentGatewayCheckRule : Error : " + ex.Message);
                refid = "-1";
            }
            return refid;
        }
    }
}
