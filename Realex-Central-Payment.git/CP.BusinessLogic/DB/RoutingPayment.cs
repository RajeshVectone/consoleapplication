﻿using System;
using System.Collections.Generic;
using System.Text;
using Switchlab.DataAccessLayer;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;

namespace CP.BusinessLogic.DB
{
    public class RoutingPayment
    {
        private const string SP_getPaymentRoutingData = "paymentRoutingGetData";

        #region privateField
        private int _paymentroutingid;
        private string _providercode = string.Empty;
        private string _sitecode = string.Empty;
        private string _productcodeprefix = string.Empty;
        private string _merchantid = string.Empty;
        private string _productcode = string.Empty;
        #endregion

        #region publicField
        public int PaymentRoutingID { get { return _paymentroutingid; } set { _paymentroutingid = value; } }
        public string ProviderCode { get { return _providercode; } set { _providercode = value; } }
        public string SiteCode { get { return _sitecode; } set { _sitecode = value; } }
        public string ProductCodePrefix { get { return _productcodeprefix; } set { _productcodeprefix = value; } }
        public string MerchantID { get { return _merchantid; } set { _merchantid = value; } }
        public string ProductCode { get { return _productcode; } set { _productcode = value; } }
        #endregion

        public RoutingPayment() { }

        public RoutingPayment(string sSiteCode, string sProductCode)
        {
            _sitecode = sSiteCode;
            _productcode = sProductCode;
        }

        public bool LoadRoutingData()
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentCentralConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@sitecode", SqlDbType.VarChar, 5, ParameterDirection.Input, _sitecode);
                DAL.AddParamToSQLCmd(cmd, "@productcode", SqlDbType.VarChar, 20, ParameterDirection.Input, _productcode);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_getPaymentRoutingData);
                List<RoutingPayment> data = new List<RoutingPayment>();
                DAL.ExecuteReaderCmd<RoutingPayment>(cmd, GeneratePaymentRoutingData<RoutingPayment>, ref data);
                if (data != null)
                {
                    if (data.Capacity > 0)
                    {
                        copyToObject(data[0]);
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool LoadRoutingData(string sSiteCode, string sProductCode)
        {
            _sitecode = sSiteCode;
            _productcode = sProductCode;

            return LoadRoutingData();
        }

        public void copyToObject(RoutingPayment Source)
        {
            if (Source != null)
            {
                _paymentroutingid = Source.PaymentRoutingID;
                _providercode = Source.ProviderCode;
                _sitecode = Source.SiteCode;
                _productcodeprefix = Source.ProductCodePrefix;
                _merchantid = Source.MerchantID;
            }
        }

        private static void GeneratePaymentRoutingData<T>(IDataReader returnData, ref List<RoutingPayment> listNew)
        {
            try
            {
                while (returnData.Read())
                {
                    RoutingPayment newItem = new RoutingPayment();
                    newItem.PaymentRoutingID = GeneralConverter.ToInt32(returnData["ID"]);
                    newItem.ProviderCode = GeneralConverter.ToString(returnData["provider_code"]);
                    newItem.SiteCode = GeneralConverter.ToString(returnData["sitecode"]);
                    newItem.ProductCodePrefix = GeneralConverter.ToString(returnData["product_code_prefix"]);
                    newItem.MerchantID = GeneralConverter.ToString(returnData["merchant_id"]);

                    listNew.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
