﻿using System;
using System.Collections.Generic;
using System.Text;
using Switchlab.DataAccessLayer;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;

namespace CP.BusinessLogic.DB
{
    public class csReasonCode
    {
        private const string SP_reasoncode_get_by_provider_reason_code = "reasoncode_get_by_provider_reason_code";

        #region privateField
        private int _generalErrorCode;
        private string _providerReasonCode = string.Empty;
        private string _providerMessage = string.Empty;
        private string _possibleAction = string.Empty;
        private int _paymentStatus;
        private string _providerCode = string.Empty;
        #endregion

        #region publicField
        public int generalErrorCode { get { return _generalErrorCode; } set { _generalErrorCode = value; } }
        public string providerReasonCode { get { return _providerReasonCode; } set { _providerReasonCode = value; } }
        public string providerMessage { get { return _providerMessage; } set { _providerMessage = value; } }
        public string possibleAction { get { return _possibleAction; } set { _possibleAction = value; } }
        public int paymentStatus { get { return _paymentStatus; } set { _paymentStatus = value; } }
        public string providerCode { get { return _providerCode; } set { _providerCode = value; } }
        #endregion

        public csReasonCode() { }

        public csReasonCode(string sProviderReasonCode, string sProviderCode)
        {
            _providerReasonCode = sProviderReasonCode;
            _providerCode = sProviderCode;
        }

        public bool LoadReasonCodeData()
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentConnectionString(_providerCode).ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@providercode", SqlDbType.VarChar, 6, ParameterDirection.Input, _providerCode);
                DAL.AddParamToSQLCmd(cmd, "@providerreasoncode", SqlDbType.VarChar, 30, ParameterDirection.Input, _providerReasonCode);

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_reasoncode_get_by_provider_reason_code);
                List<csReasonCode> data = new List<csReasonCode>();
                DAL.ExecuteReaderCmd<csReasonCode>(cmd, GenerateReasonCodeData<csReasonCode>, ref data);
                if (data != null)
                {
                    if (data.Capacity > 0)
                    {
                        copyToObject(data[0]);
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool LoadReasonCodeData(string sProviderReasonCode, string sProviderCode)
        {
            _providerReasonCode = sProviderReasonCode;
            _providerCode = sProviderCode;

            return LoadReasonCodeData();
        }

        public void copyToObject(csReasonCode Source)
        {
            if (Source != null)
            {
                _generalErrorCode = Source.generalErrorCode;
                _providerReasonCode = Source.providerReasonCode;
                _providerMessage = Source.providerMessage;
                _possibleAction = Source.possibleAction;
                _paymentStatus = Source.paymentStatus;
                _providerCode = Source.providerCode;
            }
        }

        private static void GenerateReasonCodeData<T>(IDataReader returnData, ref List<csReasonCode> listNew)
        {

            try
            {
                while (returnData.Read())
                {
                    csReasonCode newItem = new csReasonCode();
                    newItem.generalErrorCode = GeneralConverter.ToInt32(returnData["General_Error_Code"]);
                    newItem.providerReasonCode = GeneralConverter.ToString(returnData["Provider_Reason_Code"]);
                    newItem.providerMessage = GeneralConverter.ToString(returnData["Provider_Message"]);
                    newItem.possibleAction = GeneralConverter.ToString(returnData["Possible_Action"]);
                    newItem.paymentStatus = GeneralConverter.ToInt32(returnData["Payment_Status"]);
                    newItem.providerCode = GeneralConverter.ToString(returnData["Provider_Code"]);

                    listNew.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
