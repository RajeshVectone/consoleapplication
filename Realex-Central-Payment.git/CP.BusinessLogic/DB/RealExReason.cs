﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Switchlab.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CP.BusinessLogic.DB.Reference;
using CP.BusinessLogic.RealexPayments.RealAuth;
using CP.BusinessLogic.RealEx;

namespace CP.BusinessLogic.DB
{
    public class RealExReason
    {
        private const string SP_TransactionLog = "CTP_Payment_Transaction_ErrorLog";

        public  void InsertLog(TransactionResponse _response,RealExErrorLog errorlog)
        {

            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentCentralConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@REFERENCE_ID", SqlDbType.VarChar, 200, ParameterDirection.Input, _response.ResultOrderID);
                DAL.AddParamToSQLCmd(cmd, "@ERRORID", SqlDbType.Int, 20, ParameterDirection.Input, 0);
                DAL.AddParamToSQLCmd(cmd, "@RESULTID", SqlDbType.Int, 20, ParameterDirection.Input, _response.ResultCode);
                DAL.AddParamToSQLCmd(cmd, "@MESSAGE", SqlDbType.VarChar, 200, ParameterDirection.Input, _response.Message);
                DAL.AddParamToSQLCmd(cmd, "@STAGE", SqlDbType.VarChar, 200, ParameterDirection.Input, errorlog.STAGE);
                DAL.AddParamToSQLCmd(cmd, "@STEP", SqlDbType.Int, 20, ParameterDirection.Input, errorlog.STEP);
                DAL.AddParamToSQLCmd(cmd, "@EXTRAMESSAGE", SqlDbType.VarChar, 200, ParameterDirection.Input, errorlog.EXTRAMESSAGE);
                DAL.AddParamToSQLCmd(cmd, "@ECI", SqlDbType.VarChar, 20, ParameterDirection.Input, _response.Eci);
                DAL.AddParamToSQLCmd(cmd, "@STATUS", SqlDbType.VarChar, 20, ParameterDirection.Input,errorlog.STATUS);


                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_TransactionLog);
                DAL.ExecuteScalarCmd(cmd);

            }


            catch (Exception) { }

        }


        public void InsertReceiptInLog(TransactionResponse _response, RealExErrorLog errorlog)
        {

            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentCentralConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();

                DAL.AddParamToSQLCmd(cmd, "@REFERENCE_ID", SqlDbType.VarChar, 200, ParameterDirection.Input, _response.ResultOrderID);
                DAL.AddParamToSQLCmd(cmd, "@ERRORID", SqlDbType.Int, 20, ParameterDirection.Input, 0);
                DAL.AddParamToSQLCmd(cmd, "@RESULTID", SqlDbType.Int, 20, ParameterDirection.Input, _response.ResultCode);
                DAL.AddParamToSQLCmd(cmd, "@MESSAGE", SqlDbType.VarChar, 200, ParameterDirection.Input, _response.Message);
                DAL.AddParamToSQLCmd(cmd, "@STAGE", SqlDbType.VarChar, 200, ParameterDirection.Input, errorlog.STAGE);
                DAL.AddParamToSQLCmd(cmd, "@STEP", SqlDbType.Int, 20, ParameterDirection.Input, errorlog.STEP);
                DAL.AddParamToSQLCmd(cmd, "@EXTRAMESSAGE", SqlDbType.VarChar, 200, ParameterDirection.Input, errorlog.EXTRAMESSAGE);
                DAL.AddParamToSQLCmd(cmd, "@ECI", SqlDbType.VarChar, 20, ParameterDirection.Input, _response.Eci);
                DAL.AddParamToSQLCmd(cmd, "@STATUS", SqlDbType.VarChar, 20, ParameterDirection.Input, errorlog.STATUS);


                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_TransactionLog);
                DAL.ExecuteScalarCmd(cmd);

            }


            catch (Exception) { }

        }


    }
}
