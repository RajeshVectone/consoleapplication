﻿using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CP.BusinessLogic.DB
{
    public class ReceiptInOutput
    {
        public string PayerMethodref { get; set; }
        public string AccountId { get; set; }
    }

    public class ReceiptInInput
    {

    }
    
    public class ReceiptIn
    {
        private const string SP_GetPayerMethod = "PaymentGetPayerPayMethod";

        public static ReceiptInOutput GetPaymentmethod(string AccountId)
        {
            try
            {
                SQLDataAccessLayer DAL = new SQLDataAccessLayer(BusinessFacade.GetDBPaymentCentralConnectionString().ConnectionString);
                SqlCommand cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@accountid", SqlDbType.VarChar, 30, ParameterDirection.Input, AccountId);

               

                DAL.SetCommandType(cmd, CommandType.StoredProcedure, SP_GetPayerMethod);
                foreach (DataRow item in DAL.ExecuteQuery(cmd).Tables[0].Rows)
                {
                    return new ReceiptInOutput()
                    {
                        PayerMethodref = GeneralConverter.ToString(item["SubscriptionID"]),
                        AccountId = GeneralConverter.ToString(item["AccountID"])                        
                    };
                }
            }
            catch (Exception ex) { }
            return new ReceiptInOutput() { PayerMethodref = "", AccountId = AccountId};
        }


    }
}
