﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Switchlab.DataAccessLayer;
using System.Configuration;

namespace CP.BusinessLogic
{
    /// <summary>
    /// Class that contain all the Connection List
    /// </summary>
    /// Initial: Danur, 2010-10-26 14:00
    [Serializable()]
    public class BusinessFacade
    {
        /// <summary>
        /// Return a ConnectionStringSettings type of Payment DB connection
        /// </summary>
        public static ConnectionStringSettings GetDBPaymentConnectionString(string ProviderCode)
        {
            ConnectionStringsSection sec = ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection;
            return sec.ConnectionStrings[ProviderCode];
        }
          public static ConnectionStringSettings GetDBPaymentFroudConnectionString()
        {
            ConnectionStringsSection sec = ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection;
            return sec.ConnectionStrings["CS"];
        }
      
        /// Return a ConnectionStringSettings type of central Payment DB connection
        /// </summary>
        public static ConnectionStringSettings GetDBPaymentCentralConnectionString()
        {
            ConnectionStringsSection sec = ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection;
            return sec.ConnectionStrings["CentralPayment"];
        }

        /// <summary>
        /// Return a ConnectionStringSettings type of MVNO DB connection
        /// </summary>
        public static ConnectionStringSettings GetCRMConnectionString()
        {
            ConnectionStringsSection sec = ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection;
            return sec.ConnectionStrings["CRM"];
        }

        /// <summary>
        /// Return a ConnectionStringSettings type of WebPayment DB connection
        /// </summary>
        public static ConnectionStringSettings GetWebPaymentConnectionString()
        {
            ConnectionStringsSection sec = ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection;
            return sec.ConnectionStrings["WebPayment"];
        }

        /// <summary>
        /// Gets the decision manager status.
        /// </summary>
        public static bool DecisionManager { get { return (ConfigurationManager.AppSettings["DecisionManager"] ?? "").ToString().ToLower().Equals("true") ? true : false; } }

        /// <summary>
        /// Gets the AVS ignore status.
        /// </summary>
        public static bool AVSIgnore { get { return (ConfigurationManager.AppSettings["AVSIgnore"] ?? "").ToString().ToLower().Equals("true") ? true : false; } }

        /// <summary>
        /// Gets the CVN ignore status.
        /// </summary>
        public static bool CVNIgnore { get { return (ConfigurationManager.AppSettings["CVNIgnore"] ?? "").ToString().ToLower().Equals("true") ? true : false; } }

        /// <summary>
        /// Gets the log directory.
        /// </summary>
        public static string LogDirectory { get { return (ConfigurationManager.AppSettings["cybs.logDirectory"] ?? "").ToString(); } }
    }
}
