﻿using System;
using System.Collections.Generic;
using CP.DataType;
using Switchlab.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;
using CP.BusinessLogic;

namespace IVRPaymentService.ESP
{
    public class Topup
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string MobileNo { get; set; }
        public string SecurityCode { get; set; }
        public int ProductID { get; set; }
        public double Amount { get; set; }
        public double AmountBonus { get; set; }
        public string PaymentRef { get; set; }
        public int Status { get; set; }

        private const string SP_VALIDATE_CODE = "vmuk_validatesecuritycode";

        public Topup(string defaultErrorMessage)
        {
            ErrorCode = -1;
            ErrorMessage = defaultErrorMessage;
        }

        /// <summary>
        /// Validate security code and retrieve previous 10% bonus.
        /// </summary>
        public static Topup Validate(string mobileNo, string securityCode)
        {
            Topup result = new Topup("The validation process executed but the operation cannot continue due to invalid data returned by the validation process");
            result.MobileNo = mobileNo;
            result.SecurityCode = securityCode;
            List<string> missingParam = new List<string>();
            try
            {
                #region Validate parameters
                /*
                 * REF_REASON_CODE = -15:Invalid or missing parameter value
                 */
                if (string.IsNullOrEmpty(mobileNo)) missingParam.Add("account-id");
                if (string.IsNullOrEmpty(securityCode)) missingParam.Add("cc-secretcode");
                if (missingParam.Count > 0)
                {
                    string errDesc = "Invalid or missing parameter value, ";
                    foreach (string param in missingParam) errDesc += param + "|";
                    throw new PaymentException(-15, errDesc.TrimEnd("|".ToCharArray()));
                }
                #endregion

                /*
                 * Validate security code and retrieve previous 10% bonus.
                 */
                try
                {
                    SQLDataAccessLayer dal = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
                    SqlCommand sql = new SqlCommand();
                    dal.SetCommandType(sql, CommandType.StoredProcedure, SP_VALIDATE_CODE);
                    dal.AddParamToSQLCmd(sql, "@mobileno", SqlDbType.VarChar, 12, ParameterDirection.Input, mobileNo);
                    dal.AddParamToSQLCmd(sql, "@securitycode", SqlDbType.VarChar, 10, ParameterDirection.Input, securityCode);

                    DataSet dsResult = dal.ExecuteQuery(sql);
                    foreach (DataRow row in dsResult.Tables[0].Rows)
                    {
                        double valDouble;
                        result.ErrorCode = GeneralConverter.ToInt32(row["errcode"]);
                        result.ErrorMessage = GeneralConverter.ToString(row["errmsg"]);
                        result.ProductID = GeneralConverter.ToInt32(row["productid"]);

                        valDouble = GeneralConverter.ToDouble(row["amount"]);
                        result.Amount = valDouble > 0 ? valDouble / 100 : valDouble;

                        valDouble = GeneralConverter.ToDouble(row["amountbonus"]);
                        result.AmountBonus = valDouble > 0 ? valDouble / 100 : valDouble;

                        result.PaymentRef = GeneralConverter.ToString(row["paymentref"]);
                        result.Status = GeneralConverter.ToInt32(row["status"]);
                    }
                }
                catch (Exception ex)
                {
                    throw new PaymentException(-1,
                        string.Format("Failed validating security code, {0}", (ex.InnerException ?? ex).Message));
                }
            }
            catch (Exception ex)
            {
                result.ErrorCode = ex is PaymentException ? ((PaymentException)ex).ErrorCode : -1;
                result.ErrorMessage = ex is PaymentException ? ((PaymentException)ex).Message : (ex.InnerException ?? ex).Message;
                return result;
            }

            // Return the operation result
            return result;
        }
    }
}
