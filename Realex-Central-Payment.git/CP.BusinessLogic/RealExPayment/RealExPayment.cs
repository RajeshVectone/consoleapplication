﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using System.Net;
using CyberSource.Clients;
using CyberSource.Clients.SoapWebReference;
using CP.DataType;
using CP.BusinessLogic.RealexPayments.RealAuth;
using CP.BusinessLogic.DB;
using CP.BusinessLogic.RealEx;
using System.Configuration;

namespace CP.BusinessLogic.CyberSource
{
    public class RealExPayment
    {
        /// <summary>
        /// Cybersource subscription create.
        /// </summary>
        /// 
        TransactionResponse tRespresult;
        RealExReason realexReson;
        public TransactionReply Subcription_Create(string refId, string defineData, PurchaseTotals purchaseTotals, string authRequestId, string authRequestToken, bool decisionManager, string merchantId)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Payment information
            request.purchaseTotals = purchaseTotals;

            // Recurring information
            request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            request.recurringSubscriptionInfo.frequency = "on-demand";

            // Create subscription service
            request.paySubscriptionCreateService = new PaySubscriptionCreateService();
            request.paySubscriptionCreateService.paymentRequestID = authRequestId;
            request.paySubscriptionCreateService.paymentRequestToken = authRequestToken;
            request.paySubscriptionCreateService.run = "true";

            // Decision manager
            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = decisionManager ? "true" : "false";

            // Transaction result
            ReplyMessage reply = CallCyberSource(request, merchantId);
            reply.merchantReferenceCode = string.IsNullOrEmpty(reply.merchantReferenceCode) ? refId : reply.merchantReferenceCode;
            return new TransactionReply(reply, ProcessReply(reply));
        }


        //06-Jun-2017 : Added : For IVR Topup
        /// <summary>
        /// Cybersource subscription authorization.
        /// </summary>
        public TransactionReply Subscription_AuthorizeNew(BillTo billto, string refId, string defineData, PurchaseTotals purchaseTotals, string merchantId, string applicationcode, string country, string subscriptionId, string email, string cvn = "")
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            request.billTo = billto;
            request.purchaseTotals = purchaseTotals;



            TransactionResponse replyReceiptIn = CallReceiptIn(request, applicationcode, country, defineData, subscriptionId, billto, cvn);

            var replymessage = new ReplyMessage
            {
                merchantReferenceCode = replyReceiptIn.ResultOrderID,
                reasonCode = replyReceiptIn.ResultCode.ToString(),
                decision = replyReceiptIn.Message,
                requestID = replyReceiptIn.ResultPASRef,
                paySubscriptionRetrieveReply = new PaySubscriptionRetrieveReply { subscriptionID = replyReceiptIn.Cardref }
            };

            csTransaction transaction = new csTransaction();
            transaction.FroudScore_Eci(replyReceiptIn.ResultOrderID.ToString(), replyReceiptIn.ResultSuitabilityScore.ToString(), replyReceiptIn.Eci.ToString());
            transaction.EciValue(replyReceiptIn.ResultOrderID.ToString(), "", replyReceiptIn.Eci);

            //
            replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;

            //24-Aug-2019 : Moorthy added for storing SRD value
            return new TransactionReply(replymessage, ProcessReply(replymessage), replyReceiptIn.ResultPASRef, replyReceiptIn.ResultAuthCode, replyReceiptIn.SRD);


            #region Old Code
            //TransactionResponse reply = CallRealEx(request, applicationcode, country);
            //var replymessage = new ReplyMessage
            //{
            //    merchantReferenceCode = reply.ResultOrderID,
            //    reasonCode = reply.ResultCode.ToString(),
            //    payerAuthEnrollReply = new PayerAuthEnrollReply { acsURL = reply.Url, paReq = reply.Pareq, xid = reply.XID },
            //    decision = reply.Enrolled == "" ? reply.ResultMessage : reply.Enrolled, //03-Feb-2017 : Moorthy : Modified to return the result message if the Enrolled is empty 
            //    requestID = reply.ResultPASRef,
            //    requestToken = reply.ResultPASRef

            //};
            //replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;
            ////return reply;
            //return new TransactionReply(replymessage, ProcessReply(replymessage)); 
            #endregion
        }

        /// <summary>
        /// Cybersource subscription authorization.
        /// </summary>
        public TransactionReply Subscription_Authorize(string refId, string defineData, string subscriptionId, PurchaseTotals purchaseTotals, bool capture, bool decisionManager, string merchantId, Card card, string country, string applicationcode)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            //improvement 14 jan 2014 add cvv on request
            request.card = card;

            // Subscription data
            request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            request.recurringSubscriptionInfo.subscriptionID = subscriptionId;

            // Payment information
            request.purchaseTotals = purchaseTotals;

            // Authorize transaction
            request.ccAuthService = new CCAuthService();
            request.ccAuthService.run = "true";

            // Decision manager
            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = decisionManager ? "true" : "false";




            // Capture service
            if (capture)
            {
                request.ccCaptureService = new CCCaptureService();
                request.ccCaptureService.run = "true";
            }

            // Return transaction result
            ReplyMessage reply = CallCyberSource(request, merchantId);
            reply.merchantReferenceCode = string.IsNullOrEmpty(reply.merchantReferenceCode) ? refId : reply.merchantReferenceCode;
            return new TransactionReply(reply, ProcessReply(reply));
        }

        public TransactionReply Payment_Authorize(string productCode, string refId, string defineData, Card card, BillTo billTo, PurchaseTotals purchaseTotals, bool capture,
          bool decisionManager, bool avsIgnore, bool cvnIgnore, string merchantId, string eci, string applicationcode, string country, params Item[] items)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Product Information
            if (items != null)
                request.item = items;

            // Payment information
            request.card = card;
            request.billTo = billTo;
            request.purchaseTotals = purchaseTotals;


            TransactionResponse reply = CallRealExAuth_Direct(request, applicationcode, country, productCode, eci);

            var replymessage = new ReplyMessage
            {
                merchantReferenceCode = request.merchantReferenceCode,
                reasonCode = reply.ResultCode.ToString(),
                payerAuthEnrollReply = new PayerAuthEnrollReply { xid = reply.XID },
                // ccAuthReply=new CCAuthReply{cavvResponseCode=reply.Cavv},
                decision = reply.Message,
                requestID = reply.ResultPASRef,
                //afsReply = new AFSReply { afsResult = reply.ResultSuitabilityScore.ToString() },
                //payerAuthValidateReply = new PayerAuthValidateReply { eci = reply.Eci }

            };
            replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;
            //return reply;

            //24-Aug-2019 : Moorthy added for storing SRD value
            return new TransactionReply(replymessage, ProcessReply(replymessage), reply.ResultPASRef, reply.ResultAuthCode, reply.SRD);
        }



        private TransactionResponse CallRealExAuth_Direct(RequestMessage request, string applicationcode, string country, string productcode, string eciValue)
        {
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            // request.merchantID = merchantId.Trim();
            TransactionResponse tResp;
            // run transaction
            string errDesc = string.Empty;
            try
            {
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(request.purchaseTotals.grandTotalAmount) * 100));
                //CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(2, 2), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);  // Closed For Exp Date Issue
                CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(0, 4), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);
                //TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                //String orderID = DateTime.Now.ToString("yyyyMMddHHmmss");
                //tReq.Eci = eci;
                //if (request.purchaseTotals.currency == "USD")
                //{
                // tResp = tReq.ThreeDVerifySign("usd", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);

                //05-Apr-2017 : Moorthy : Modified to avoid error when cardtype is empty
                string cardType = "";
                if (request != null && request.card != null && request.card.cardType != null)
                    cardType = request.card.cardType;
                var creden = GetCredential(applicationcode, country, request.purchaseTotals.currency, cardType);

                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                TransactionRequest tReq = new TransactionRequest(creden.Account, creden.Password, creden.Refund1, creden.Refund2);
                tReq.Cavv = "";
                tReq.Xid = "";
                tReq.Eci = "";
                tReq.Pares = "";
                tReq.CustIPAddress = request.billTo.ipAddress;
                //tReq.CustIPAddress = "192.168.2.45";
                tReq.CustNum = string.IsNullOrEmpty(request.merchantDefinedData.field1) ? "" : request.merchantDefinedData.field1;
                tReq.Email = request.billTo.email;
                tReq.ProductId = productcode;
                //18-Feb-2019: Moorthy : Bookmark for ECI value
                //tReq.Eci = eciValue; 
                tResp = tReq.Authorize_direct(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}
                //else
                //{
                //    tResp = tReq.Authorize("uk", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}

                realexReson = new RealExReason();
                //Payment Logs to be updated
                var realexError = new RealExErrorLog
                {
                    STEP = 11,
                    STAGE = "Payment Process ",
                    EXTRAMESSAGE = "",
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status

                };
                realexReson.InsertLog(tResp, realexError);


                return tResp;
            }
            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");
        }










        /// <summary>
        /// Cybersource enrollment authorization.
        /// </summary>
        public TransactionReply Enrollment_Authorize(string refId, string defineData, Card card, BillTo billTo, PurchaseTotals purchaseTotals, bool checkEnroll,
            bool decisionManager, bool avsIgnore, bool cvnIgnore, string merchantId, bool capture, string applicationcode, string country, params Item[] items)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Product Information
            if (items != null)
                request.item = items;

            // Payment information
            request.card = card;
            request.billTo = billTo;
            request.purchaseTotals = purchaseTotals;

            //// Authorization service
            //request.ccAuthService = new CCAuthService();
            //request.ccAuthService.run = "true";

            //// Business rules
            //request.businessRules = new BusinessRules();
            //request.businessRules.ignoreAVSResult = avsIgnore ? "true" : "false";
            //request.businessRules.ignoreCVResult = cvnIgnore ? "true" : "false";

            //// Decision manager
            //request.decisionManager = new DecisionManager();
            //request.decisionManager.enabled = decisionManager ? "true" : "false";

            //// Enrollment service
            //if (checkEnroll)
            //{
            //    request.payerAuthEnrollService = new PayerAuthEnrollService();
            //    request.payerAuthEnrollService.run = "true";
            //}

            //// Capture service
            //if (capture)
            //{
            //    request.ccCaptureService = new CCCaptureService();
            //    request.ccCaptureService.run = "true";
            //}

            // Return transaction result
            // ReplyMessage reply = CallCyberSource(request, merchantId);
            TransactionResponse reply = CallRealEx(request, applicationcode, country);
            var replymessage = new ReplyMessage
            {
                merchantReferenceCode = reply.ResultOrderID,
                reasonCode = reply.ResultCode.ToString(),
                payerAuthEnrollReply = new PayerAuthEnrollReply { acsURL = reply.Url, paReq = reply.Pareq, xid = reply.XID },
                decision = reply.Enrolled == "" ? reply.ResultMessage : reply.Enrolled, //03-Feb-2017 : Moorthy : Modified to return the result message if the Enrolled is empty 
                requestID = reply.ResultPASRef,
                requestToken = reply.ResultPASRef

            };
            replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;
            //return reply;
            return new TransactionReply(replymessage, ProcessReply(replymessage));
        }




        public TransactionReply Payment_Authorize(string productCode, string refId, string defineData, Card card, BillTo billTo, PurchaseTotals purchaseTotals, string pares, bool capture,
            bool decisionManager, bool avsIgnore, bool cvnIgnore, string merchantId, string eci, string applicationcode, string country, ReplyMessage replymessagei, params Item[] items)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Product Information
            if (items != null)
                request.item = items;

            // Payment information
            request.card = card;
            request.billTo = billTo;
            request.purchaseTotals = purchaseTotals;

            TransactionResponse reply = CallRealExAuth(request, applicationcode, country, replymessagei, productCode);

            var replymessage = new ReplyMessage
            {
                merchantReferenceCode = request.merchantReferenceCode,
                reasonCode = reply.ResultCode.ToString(),
                payerAuthEnrollReply = new PayerAuthEnrollReply { xid = reply.XID },
                // ccAuthReply=new CCAuthReply{cavvResponseCode=reply.Cavv},
                decision = reply.Message,
                requestID = reply.ResultPASRef,
                //afsReply = new AFSReply { afsResult = reply.ResultSuitabilityScore.ToString() },
                //payerAuthValidateReply = new PayerAuthValidateReply { eci = reply.Eci }

            };
            replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;
            //return reply;

            //24-Aug-2019 : Moorthy added for storing SRD value
            return new TransactionReply(replymessage, ProcessReply(replymessage), reply.ResultPASRef, reply.ResultAuthCode, reply.SRD);
        }



        public TransactionReply Payment_Authorize(string refId, string defineData, Card card, BillTo billTo, PurchaseTotals purchaseTotals, string pares, bool capture,
            bool decisionManager, bool avsIgnore, bool cvnIgnore, string merchantId, string eci, string applicationcode, string country, ReplyMessage replymessagei, params Item[] items)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Product Information
            if (items != null)
                request.item = items;

            // Payment information
            request.card = card;
            request.billTo = billTo;
            request.purchaseTotals = purchaseTotals;


            TransactionResponse reply = CallRealExAuth(request, applicationcode, country, replymessagei);

            var replymessage = new ReplyMessage
            {
                merchantReferenceCode = request.merchantReferenceCode,
                reasonCode = reply.ResultCode.ToString(),
                payerAuthEnrollReply = new PayerAuthEnrollReply { xid = reply.XID },
                // ccAuthReply=new CCAuthReply{cavvResponseCode=reply.Cavv},
                decision = reply.Status,
                requestID = reply.ResultPASRef,
                //afsReply = new AFSReply { afsResult = reply.ResultSuitabilityScore.ToString() },
                //payerAuthValidateReply = new PayerAuthValidateReply { eci = reply.Eci }

            };
            replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;
            //return reply;

            //24-Aug-2019 : Moorthy added for storing SRD value
            return new TransactionReply(replymessage, ProcessReply(replymessage), reply.ResultPASRef, reply.ResultAuthCode, reply.SRD);
        }

        /// <summary>
        /// Cybersource enrollment validate authentication.
        /// </summary>
        public TransactionReply Enrollment_Validate(string refId, string defineData, Card card, BillTo billTo, PurchaseTotals purchaseTotals, string pares, bool capture,
        bool decisionManager, bool avsIgnore, bool cvnIgnore, string merchantId, string applicationcode, string country, params Item[] items)
        {
            RequestMessage request = new RequestMessage();
            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Product Information
            if (items != null)
                request.item = items;

            // Payment information
            request.card = card;
            request.billTo = billTo;
            request.purchaseTotals = purchaseTotals;

            //// Authorization service
            //request.ccAuthService = new CCAuthService();
            //request.ccAuthService.run = "true";

            // Validate authentication service
            request.payerAuthValidateService = new PayerAuthValidateService();
            request.payerAuthValidateService.signedPARes = pares;
            // request.payerAuthValidateService.run = "true";

            // Business rules
            request.businessRules = new BusinessRules();
            request.businessRules.ignoreAVSResult = avsIgnore ? "true" : "false";
            request.businessRules.ignoreCVResult = cvnIgnore ? "true" : "false";

            //// Decision manager
            //request.decisionManager = new DecisionManager();
            //request.decisionManager.enabled = decisionManager ? "true" : "false";

            //// Capture service
            //if (capture)
            //{
            //    request.ccCaptureService = new CCCaptureService();
            //    request.ccCaptureService.run = "true";
            //}
            // Return transaction result

            TransactionResponse reply = CallRealExVerifySign(request, applicationcode, country);

            var replymessage = new ReplyMessage
            {
                merchantReferenceCode = request.merchantReferenceCode,
                reasonCode = reply.ResultCode.ToString(),
                payerAuthEnrollReply = new PayerAuthEnrollReply { xid = reply.XID },

                ccAuthReply = new CCAuthReply { cavvResponseCode = reply.Cavv, },
                decision = reply.Status,
                requestID = reply.ResultPASRef,
                afsReply = new AFSReply { afsResult = reply.ResultSuitabilityScore.ToString() },
                payerAuthValidateReply = new PayerAuthValidateReply { eci = reply.Eci },

                requestToken = reply.ResultPASRef,
            };

            csTransaction transaction = new csTransaction();
            transaction.FroudScore_Eci(reply.ResultOrderID.ToString(), reply.ResultSuitabilityScore.ToString(), reply.Eci.ToString());
            replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;
            //return reply;
            return new TransactionReply(replymessage, ProcessReply(replymessage));
        }

        /// <summary>
        /// Cybersource enrollment capture.
        /// </summary>
        public TransactionReply Enrollment_Capture(string refId, string defineData, PurchaseTotals purchaseTotals, string requestId, string requestToken, bool decisionManager, string merchantId, params Item[] items)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Product information
            if (items != null)
                request.item = items;

            // Payment information
            request.purchaseTotals = purchaseTotals;

            // Capture service
            request.ccCaptureService = new CCCaptureService();
            request.ccCaptureService.authRequestID = requestId;
            request.ccCaptureService.authRequestToken = requestToken;
            request.ccCaptureService.run = "true";

            // Decision manager
            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = decisionManager ? "true" : "false";

            // Return transaction result
            ReplyMessage reply = CallCyberSource(request, merchantId);
            reply.merchantReferenceCode = string.IsNullOrEmpty(reply.merchantReferenceCode) ? refId : reply.merchantReferenceCode;
            return new TransactionReply(reply, ProcessReply(reply));
        }

        /// <summary>
        /// Cybersource enrollment capture.
        /// </summary>
        public TransactionReply Enrollment_Void(string refId,
            string defineData,
            PurchaseTotals purchaseTotals,
            string requestId,
            string requestToken,
            bool decisionManager,
            string merchantId)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Payment info
            request.purchaseTotals = purchaseTotals;

            // Authorization reversal service
            request.ccAuthReversalService = new CCAuthReversalService();
            request.ccAuthReversalService.authRequestID = requestId;
            request.ccAuthReversalService.authRequestToken = requestToken;
            request.ccAuthReversalService.run = "true";

            // Decision manager
            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = decisionManager ? "true" : "false";

            // Return transaction result
            TransactionResponse reply = CallRealExVoid(request, defineData, defineData);

            var replymessage = new ReplyMessage
          {
              merchantReferenceCode = request.merchantReferenceCode,
              reasonCode = reply.ResultCode.ToString(),
              payerAuthEnrollReply = new PayerAuthEnrollReply { xid = reply.XID },
              // ccAuthReply=new CCAuthReply{cavvResponseCode=reply.Cavv},
              decision = reply.Message,
              requestID = reply.ResultPASRef,
              //afsReply = new AFSReply { afsResult = reply.ResultSuitabilityScore.ToString() },
              //payerAuthValidateReply = new PayerAuthValidateReply { eci = reply.Eci }

          };


            //reply.merchantReferenceCode = string.IsNullOrEmpty(reply.merchantReferenceCode) ? refId : reply.merchantReferenceCode;
            return new TransactionReply(replymessage, ProcessReply(replymessage));
        }


        private TransactionResponse CallRealExVoid(RequestMessage request, string applicationcode, string country)
        {
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            // request.merchantID = merchantId.Trim();
            TransactionResponse tResp = null; ;
            // run transaction
            string errDesc = string.Empty;
            try
            {
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(request.purchaseTotals.grandTotalAmount) * 100));
                //CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(2, 2), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);
                CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(0, 4), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);
                //TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                //String orderID = DateTime.Now.ToString("yyyyMMddHHmmss");
                //tReq.Eci = eci;
                //if (request.purchaseTotals.currency == "USD")
                //{
                // tResp = tReq.ThreeDVerifySign("usd", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);

                //05-Apr-2017 : Moorthy : Modified to avoid error when cardtype is empty
                string cardType = "";
                if (request != null && request.card != null && request.card.cardType != null)
                    cardType = request.card.cardType;
                var creden = GetCredential(applicationcode, country, request.purchaseTotals.currency, cardType);

                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                TransactionRequest tReq = new TransactionRequest(creden.Account, creden.Password, creden.Refund1, creden.Refund2);

                tReq.CustIPAddress = request.billTo.ipAddress;
                tReq.CustNum = string.IsNullOrEmpty(request.merchantDefinedData.field1) ? "" : request.merchantDefinedData.field1;
                tReq.Email = request.billTo.email;

                tResp = tReq.Void(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}
                //else
                //{
                //    tResp = tReq.Authorize("uk", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}

                var realexError = new RealExErrorLog
                {
                    STEP = 5,
                    STAGE = "Payment Process ",
                    EXTRAMESSAGE = "",
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status

                };
                realexReson.InsertLog(tResp, realexError);



            }
            catch (WebException we) { errDesc = HandleWebException(we); }
            return tResp;
        }











        /// <summary>
        /// Cybersource enrollment capture.
        /// </summary>
        public TransactionReply Enrollment_AuthReverse(string refId, string defineData, PurchaseTotals purchaseTotals, string requestId, string requestToken, bool decisionManager, string merchantId)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Payment info
            request.purchaseTotals = purchaseTotals;

            // Authorization reversal service
            request.ccAuthReversalService = new CCAuthReversalService();
            request.ccAuthReversalService.authRequestID = requestId;
            request.ccAuthReversalService.authRequestToken = requestToken;
            request.ccAuthReversalService.run = "true";

            // Decision manager
            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = decisionManager ? "true" : "false";

            // Return transaction result
            ReplyMessage reply = CallCyberSource(request, merchantId);
            reply.merchantReferenceCode = string.IsNullOrEmpty(reply.merchantReferenceCode) ? refId : reply.merchantReferenceCode;
            return new TransactionReply(reply, ProcessReply(reply));
        }

        #region Cyber Source transaction methods
        private ReplyMessage CallCyberSource(RequestMessage request, string merchantId)
        {
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            request.merchantID = merchantId.Trim();

            // run transaction
            string errDesc = string.Empty;
            try
            {
                ReplyMessage reply = SoapClient.RunTransaction(request);

                reply.requestID = reply.requestID ?? "";
                reply.requestToken = reply.requestToken ?? "";

                return reply;
            }

            // transaction failed
            catch (SignException se) { errDesc = HandleSignException(se); }
            catch (SoapHeaderException she) { errDesc = HandleSoapHeaderException(she); }
            catch (SoapBodyException sbe) { errDesc = HandleSoapBodyException(sbe); }
            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");
        }
        private TransactionResponse CallRealEx(RequestMessage request, string applicationcode, string country)
        {
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            //  request.merchantID = merchantId.Trim();
            TransactionResponse tResp;
            // run transaction
            string errDesc = string.Empty;
            try
            {
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(request.purchaseTotals.grandTotalAmount) * 100));
                //ReplyMessage reply = SoapClient.RunTransaction(request);

                //reply.requestID = reply.requestID ?? "";
                //reply.requestToken = reply.requestToken ?? "";
                //CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(2, 2), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);
                CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(0, 4), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);
                //TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                //String orderID = DateTime.Now.ToString("yyyyMMddHHmmss");

                //05-Apr-2017 : Moorthy : Modified to avoid error when cardtype is empty
                string cardType = "";
                if (request != null && request.card != null && request.card.cardType != null)
                    cardType = request.card.cardType;
                var creden = GetCredential(applicationcode, country, request.purchaseTotals.currency, cardType);

                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                TransactionRequest tReq = new TransactionRequest(creden.Account, creden.Password, creden.Refund1, creden.Refund2);
                //  tResp = tReq.ThreeDSecurity(request.purchaseTotals.currency, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                tResp = tReq.ThreeDSecurity(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);


                realexReson = new RealExReason();
                var realexError = new RealExErrorLog
                {
                    STEP = 1,
                    STAGE = "3DS Enrolled Checking",
                    EXTRAMESSAGE = "",
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status


                };
                realexReson.InsertLog(tResp, realexError);
                return tResp;
            }

            // transaction failed
            //catch (SignException se) { errDesc = HandleSignException(se); }
            //catch (SoapHeaderException she) { errDesc = HandleSoapHeaderException(she); }
            //catch (SoapBodyException sbe) { errDesc = HandleSoapBodyException(sbe); }
            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");
        }

        private TransactionResponse CallRealExAuth(RequestMessage request, string applicationcode, string country, ReplyMessage reply, string productcode)
        {
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            // request.merchantID = merchantId.Trim();
            TransactionResponse tResp;
            // run transaction
            string errDesc = string.Empty;
            try
            {
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(request.purchaseTotals.grandTotalAmount) * 100));
                //CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(2, 2), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);  //  Locked For EXp Date Issue 
                CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(0, 4), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);
                //TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                //String orderID = DateTime.Now.ToString("yyyyMMddHHmmss");
                //tReq.Eci = eci;
                //if (request.purchaseTotals.currency == "USD")
                //{
                // tResp = tReq.ThreeDVerifySign("usd", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);

                //05-Apr-2017 : Moorthy : Modified to avoid error when cardtype is empty
                string cardType = "";
                if (request != null && request.card != null && request.card.cardType != null)
                    cardType = request.card.cardType;
                var creden = GetCredential(applicationcode, country, request.purchaseTotals.currency, cardType);

                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                TransactionRequest tReq = new TransactionRequest(creden.Account, creden.Password, creden.Refund1, creden.Refund2);
                tReq.Cavv = reply.ccAuthReply.cavvResponseCode;
                tReq.Xid = reply.payerAuthEnrollReply.xid;
                tReq.Eci = reply.payerAuthValidateReply.eci;
                tReq.Pares = reply.requestID;
                tReq.CustIPAddress = request.billTo.ipAddress;
                tReq.CustNum = string.IsNullOrEmpty(request.merchantDefinedData.field1) ? "" : request.merchantDefinedData.field1;
                tReq.Email = request.billTo.email;
                tReq.ProductId = productcode;
                tResp = tReq.Authorize_new(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}
                //else
                //{
                //    tResp = tReq.Authorize("uk", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}

                var realexError = new RealExErrorLog
                {
                    STEP = 5,
                    STAGE = "Payment Process ",
                    EXTRAMESSAGE = "",
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status

                };
                realexReson.InsertLog(tResp, realexError);


                return tResp;
            }


            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");
        }



        private TransactionResponse CallRealExAuth(RequestMessage request, string applicationcode, string country, ReplyMessage reply)
        {
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            // request.merchantID = merchantId.Trim();
            TransactionResponse tResp;
            // run transaction
            string errDesc = string.Empty;
            try
            {
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(request.purchaseTotals.grandTotalAmount) * 100));
                //CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(2, 2), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);  // Locked for Exp Date Issue
                CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(0, 4), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);
                //TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                //String orderID = DateTime.Now.ToString("yyyyMMddHHmmss");
                //tReq.Eci = eci;
                //if (request.purchaseTotals.currency == "USD")
                //{
                // tResp = tReq.ThreeDVerifySign("usd", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);

                //05-Apr-2017 : Moorthy : Modified to avoid error when cardtype is empty
                string cardType = "";
                if (request != null && request.card != null && request.card.cardType != null)
                    cardType = request.card.cardType;
                var creden = GetCredential(applicationcode, country, request.purchaseTotals.currency, cardType);

                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                TransactionRequest tReq = new TransactionRequest(creden.Account, creden.Password, creden.Refund1, creden.Refund2);
                tReq.Cavv = reply.ccAuthReply.cavvResponseCode;
                tReq.Xid = reply.payerAuthEnrollReply.xid;
                tReq.Eci = reply.payerAuthValidateReply.eci;
                tReq.Pares = reply.requestID;
                tReq.CustIPAddress = request.billTo.ipAddress;
                tReq.CustNum = string.IsNullOrEmpty(request.merchantDefinedData.field1) ? "" : request.merchantDefinedData.field1;
                tReq.Email = request.billTo.email;
                tResp = tReq.Authorize_new(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}
                //else
                //{
                //    tResp = tReq.Authorize("uk", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}

                var realexError = new RealExErrorLog
                {
                    STEP = 5,
                    STAGE = "Payment Process ",
                    EXTRAMESSAGE = "",
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status

                };
                realexReson.InsertLog(tResp, realexError);


                return tResp;
            }


            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");
        }

        public TransactionReply Enrollment_ReceiptIn(BillTo billto, string refId, string defineData, PurchaseTotals purchaseTotals, string merchantId, string applicationcode, string country, string subscriptionId, string email, string cvn)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            request.billTo = billto;
            request.purchaseTotals = purchaseTotals;



            TransactionResponse replyReceiptIn = CallReceiptIn(request, applicationcode, country, defineData, subscriptionId, billto, cvn);

            var replymessage = new ReplyMessage
            {
                merchantReferenceCode = replyReceiptIn.ResultOrderID,
                reasonCode = replyReceiptIn.ResultCode.ToString(),
                decision = replyReceiptIn.Message,
                requestID = replyReceiptIn.ResultPASRef,
                paySubscriptionRetrieveReply = new PaySubscriptionRetrieveReply { subscriptionID = replyReceiptIn.Cardref }
            };

            csTransaction transaction = new csTransaction();
            transaction.FroudScore_Eci(replyReceiptIn.ResultOrderID.ToString(), replyReceiptIn.ResultSuitabilityScore.ToString(), replyReceiptIn.Eci.ToString());
            transaction.EciValue(replyReceiptIn.ResultOrderID.ToString(), "", replyReceiptIn.Eci);

            //
            replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;

            //24-Aug-2019 : Moorthy added for storing SRD value
            return new TransactionReply(replymessage, ProcessReply(replymessage), replyReceiptIn.ResultPASRef, replyReceiptIn.ResultAuthCode, replyReceiptIn.SRD);
        }

        /// <summary>
        /// cybersource subscription capture.
        /// </summary>
        public TransactionReply Subscription_Capture(string refId, string defineData, Card card, BillTo billTo, PurchaseTotals purchaseTotals, string merchantId, string applicationcode, string country)
        {
            Log.Write("Subscription_Capture");
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;


            // Payment information
            request.card = card;
            request.billTo = billTo;
            request.purchaseTotals = purchaseTotals;


            TransactionResponse replyCardStorage = CallRealCardStorage(request, applicationcode, country, defineData);

            var replymessage = new ReplyMessage
            {
                merchantReferenceCode = replyCardStorage.ResultOrderID,
                reasonCode = replyCardStorage.ResultCode.ToString(),
                // payerAuthEnrollReply = new PayerAuthEnrollReply { acsURL = reply.Url, paReq = reply.Pareq, xid = reply.XID },
                decision = replyCardStorage.Message,
                requestID = replyCardStorage.ResultPASRef,
                paySubscriptionRetrieveReply = new PaySubscriptionRetrieveReply { subscriptionID = replyCardStorage.Cardref },
                paySubscriptionCreateReply = new PaySubscriptionCreateReply { subscriptionID = replyCardStorage.Cardref }
            };
            replymessage.merchantReferenceCode = string.IsNullOrEmpty(replymessage.merchantReferenceCode) ? refId : replymessage.merchantReferenceCode;
            //return reply;
            return new TransactionReply(replymessage, ProcessReply(replymessage), replyCardStorage.CardRefErrorCode, replyCardStorage.PayrefErrorCode);
        }

        private TransactionResponse CallReceiptIn(RequestMessage request, string applicationcode, string country, string accountid, string Paymentmethod, BillTo billto, string cvn)
        {
            TransactionResponse tResp;
            // run transaction
            string errDesc = string.Empty;

            try
            {
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(request.purchaseTotals.grandTotalAmount) * 100));
                //CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(2, 2), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT, payerref: accountid);

                //05-Apr-2017 : Moorthy : Modified to avoid error when cardtype is empty
                string cardType = "";
                if (request != null && request.card != null && request.card.cardType != null)
                    cardType = request.card.cardType;
                var creden = GetCredential(applicationcode, country, request.purchaseTotals.currency, cardType);

                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                TransactionRequest tReq = new TransactionRequest(creden.Account, creden.Password, creden.Refund1, creden.Refund2);
                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                tReq.Payertype = "Business";
                tReq.Payerref = accountid;
                //tReq.Paymentmethod = ReceiptIn.GetPaymentmethod(accountid).PayerMethodref;
                tReq.Email = billto.email;
                tReq.CustNum = billto.customerID;
                tReq.CustIPAddress = billto.ipAddress;

                tReq.Paymentmethod = Paymentmethod;
                //13-Jul-2018 : Added for cvn
                tReq.Cavv = cvn;

                tResp = tReq.ReceiptIn(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount);
                //if (tResp.ResultCode == 0 || tResp.ResultCode==501)
                //{

                realexReson = new RealExReason();
                var realexError = new RealExErrorLog
                {
                    STEP = 16,
                    STAGE = "Receipt-IN",
                    EXTRAMESSAGE = "pasref:" + (string.IsNullOrEmpty(tResp.ResultPASRef) ? "" : tResp.ResultPASRef) + "ResultAuthCode:"
                    + (string.IsNullOrEmpty(tResp.ResultAuthCode) ? "" : tResp.ResultAuthCode),
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status

                };

                realexReson.InsertLog(tResp, realexError);

                return tResp;
            }
            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");

        }




        private TransactionResponse CallRealCardStorage(RequestMessage request, string applicationcode, string country, string accountid)
        {
            Log.Write("CallRealCardStorage()");
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            // request.merchantID = merchantId.Trim();
            TransactionResponse tResp;
            // run transaction
            string errDesc = string.Empty;
            try
            {
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(request.purchaseTotals.grandTotalAmount) * 100));
                //CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(2, 2), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT, payerref: accountid);  //  Locked for Exp Date Issue 
                CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(0, 4), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT, payerref: accountid);

                //05-Apr-2017 : Moorthy : Modified to avoid error when cardtype is empty
                string cardType = "";
                if (request != null && request.card != null && request.card.cardType != null)
                    cardType = request.card.cardType;
                var creden = GetCredential(applicationcode, country, request.purchaseTotals.currency, cardType);

                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                TransactionRequest tReq = new TransactionRequest(creden.Account, creden.Password, creden.Refund1, creden.Refund2);
                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                tReq.Payertype = "Business";
                //
                tReq.Payerref = accountid;

                cReq.Cardref = request.billTo.firstName.Replace(" ", "") + request.card.accountNumber.Substring(request.card.accountNumber.Length - 6, 6);

                tReq.Postcode = request.billTo.postalCode;
                tReq.City = request.billTo.city;
                tReq.TransBillingAddressCountry = request.billTo.country;
                tReq.Email = request.billTo.email;
                tResp = tReq.PayerNew(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //if (tResp.ResultCode == 0 || tResp.ResultCode==501)
                //{



                realexReson = new RealExReason();
                var realexError = new RealExErrorLog
                {
                    STEP = 3,
                    STAGE = "Payer registration",
                    EXTRAMESSAGE = tResp.Payerref,
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status



                };
                int PayrefErrorCode = tResp.ResultCode;

                //30-Apr-2018 : Mootrthy : Commented to avoid displaying wrong Error Msg
                //realexReson.InsertLog(tResp, realexError);

                tResp = tReq.CardNew(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                tResp.Cardref = cReq.Cardref;
                tResp.Payerref = accountid;
                //}
                var realexError2 = new RealExErrorLog
                {
                    STEP = 4,
                    STAGE = "Card Ref registration",
                    EXTRAMESSAGE = tResp.Cardref,
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status

                };
                tResp.PayrefErrorCode = PayrefErrorCode;
                tResp.CardRefErrorCode = tResp.ResultCode;
                //30-Apr-2018 : Mootrthy : Commented to avoid displaying wrong Error Msg
                //realexReson.InsertLog(tResp, realexError2);
                return tResp;
            }


            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");
        }
        private TransactionResponse CallRealExVerifySign(RequestMessage request, string applicationcode, string country)
        {
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            // request.merchantID = merchantId.Trim();
            TransactionResponse tResp;
            // run transaction
            string errDesc = string.Empty;
            try
            {
                Int64 amount = decimal.ToInt64((Convert.ToDecimal(request.purchaseTotals.grandTotalAmount) * 100));
                //ReplyMessage reply = SoapClient.RunTransaction(request);

                //reply.requestID = reply.requestID ?? "";
                //reply.requestToken = reply.requestToken ?? "";
                //CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(2, 2), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);  //  Exp Date Issue changed 4 digit of year 
                CreditCard cReq = new CreditCard(request.card.cardType, request.card.accountNumber, request.card.expirationMonth + request.card.expirationYear.Substring(0, 4), request.billTo.firstName, request.card.cvNumber, CreditCard.CVN_PRESENT);

                //05-Apr-2017 : Moorthy : Modified to avoid error when cardtype is empty
                string cardType = "";
                if (request != null && request.card != null && request.card.cardType != null)
                    cardType = request.card.cardType;
                var creden = GetCredential(applicationcode, country, request.purchaseTotals.currency, cardType);

                // TransactionRequest tReq = new TransactionRequest("chillitalk", "3Of2rd6etL", "refund", "refund");
                TransactionRequest tReq = new TransactionRequest(creden.Account, creden.Password, creden.Refund1, creden.Refund2);
                String orderID = DateTime.Now.ToString("yyyyMMddHHmmss");


                tReq.TransPASRef = request.payerAuthValidateService.signedPARes;
                tReq.TransOrderID = request.merchantReferenceCode;
                //if (request.purchaseTotals.currency == "USD")
                //{

                //tResp = tReq.ThreeDVerifySign(request.purchaseTotals.currency, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                tResp = tReq.ThreeDVerifySign(creden.SubAccount, request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}
                //else
                //{
                //    tResp = tReq.ThreeDVerifySign("uk", request.merchantReferenceCode, request.purchaseTotals.currency, amount, cReq);
                //}



                //tRespresult = tResp;
                //var reply = new ReplyMessage 
                //{
                //    reasonCode=tResp.ResultCode.ToString(),
                //    merchantReferenceCode=tResp.ResultOrderID

                //};
                realexReson = new RealExReason();
                var realexError = new RealExErrorLog
                {
                    STEP = 2,
                    STAGE = "3DS Verification",
                    EXTRAMESSAGE = "",
                    MESSAGE = tResp.Message,
                    STATUS = tResp.Status

                };
                realexReson.InsertLog(tResp, realexError);

                return tResp;
            }

            // transaction failed
            //catch (SignException se) { errDesc = HandleSignException(se); }
            //catch (SoapHeaderException she) { errDesc = HandleSoapHeaderException(she); }
            //catch (SoapBodyException sbe) { errDesc = HandleSoapBodyException(sbe); }
            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");
        }
        private void SaveOrderState()
        {
            /*
             * This is where you store the order state in your system for
             * post-transaction analysis.  Information to store include the
             * invoice, the values of the reply fields, or the details of the
             * exception that occurred, if any.
             */
        }

        private string ProcessReply(ReplyMessage reply)
        {
            //string template = GetTemplate(reply.decision.ToUpper());
            string content = GetContent(reply);

            // Special condition
            //if (reply.reasonCode.Equals("475"))
            //    template = template.Replace("Your order was not approved.", "").Trim();

            /*
             * Display result of transaction.  Being a console application,
             * this sample simply prints out some text on the screen.  Use
             * what is appropriate for your system (e.g. ASP.NET pages).
             */
            //return string.Format(template, content);
            return string.Format(content);
        }

        private string GetTemplate(string decision)
        {
            /*
             * This is where you retrieve the HTML template that corresponds
             * to the decision.  This template has 'boiler-plate' wording and
             * can be stored in files or a database.  This is just one way to
             * retrieve feedback pages.  Use what is appropriate for your
             * system (e.g. ASP.NET pages).
             */

            if ("ACCEPT".Equals(decision))
            {
                return ("The transaction succeeded. {0}");
            }

            if ("REJECT".Equals(decision))
            {
                return ("Your order was not approved. {0}");
            }

            // ERROR
            return "Your order could not be completed at this time. {0}";
        }
        private string ProcessReply1(TransactionResponse reply)
        {
            //string template = GetTemplate(reply.decision.ToUpper());
            string content = GetContent1(reply);

            //// Special condition
            //if (reply.reasonCode.Equals("475"))
            //    template = template.Replace("Your order was not approved.", "").Trim();



            /*
             * Display result of transaction.  Being a console application,
             * this sample simply prints out some text on the screen.  Use
             * what is appropriate for your system (e.g. ASP.NET pages).
             */
            //  return string.Format(template, content);
            return string.Format(content);
        }
        private string GetContent1(TransactionResponse reply)
        {
            /*
             * This is where you retrieve the content that will be plugged
             * into the template.
             * 
             * The strings returned in this sample are mostly to demonstrate
             * how to retrieve the reply fields.  Your application should
             * display user-friendly messages.
             */


            //switch (reply.ResultCode)
            //{
            //    // Success
            //    case 100:
            //        return ("");
            //    //"\nRequest ID: " + reply.requestID +
            //    //"\nAuthorization Code: " +
            //    //    (reply.ccAuthReply != null ? reply.ccAuthReply.authorizationCode : "-") +
            //    //"\nCapture Request Time: " +
            //    //    (reply.ccCaptureReply != null ? reply.ccCaptureReply.requestDateTime : "-") +
            //    //"\nCaptured Amount: " +
            //    //    (reply.ccCaptureReply != null ? reply.ccCaptureReply.amount : "-"));

            //    // Missing field(s)
            //    case 101:
            //        //  return string.Format("One or more fields in the request are missing{0}", EnumerateValues(reply.missingField));
            //        return string.Format("Declined by Bank");

            //    // Invalid field(s)
            //    case 102:
            //        return string.Format("One or more fields in the request are invalid");

            //    // Insufficient funds
            //    //case 204:
            //    //    return "Insufficient funds in the account. Please use a different card or select another form of payment.";

            //    //// The customer is enrolled in Payer Authentication
            //    //case 475:
            //    //    return "The cardholder is enrolled in Payer Authentication. Please authenticate before proceeding with authorization.";

            //    // add additional reason codes here that you need to handle
            //    // specifically.

            //    default:
            // For all other reason codes, return an empty string,
            // in which case, the template will be displayed with no
            // specific content.
            return (reply.ToString());
            //}
        }
        private string GetContent(ReplyMessage reply)
        {
            /*
             * This is where you retrieve the content that will be plugged
             * into the template.
             * 
             * The strings returned in this sample are mostly to demonstrate
             * how to retrieve the reply fields.  Your application should
             * display user-friendly messages.
             */

            int reasonCode = int.Parse(reply.reasonCode);
            switch (reasonCode)
            {
                // Success
                case 100:
                    return ("");
                //"\nRequest ID: " + reply.requestID +
                //"\nAuthorization Code: " +
                //    (reply.ccAuthReply != null ? reply.ccAuthReply.authorizationCode : "-") +
                //"\nCapture Request Time: " +
                //    (reply.ccCaptureReply != null ? reply.ccCaptureReply.requestDateTime : "-") +
                //"\nCaptured Amount: " +
                //    (reply.ccCaptureReply != null ? reply.ccCaptureReply.amount : "-"));

                // Missing field(s)
                case 101:
                    //  return string.Format("One or more fields in the request are missing{0}", EnumerateValues(reply.missingField));
                    return string.Format("Declined by Bank{0}", EnumerateValues(reply.missingField));

                // Invalid field(s)
                case 102:
                    return string.Format("One or more fields in the request are invalid{0}", EnumerateValues(reply.invalidField));

                // Insufficient funds
                case 204:
                    return "Insufficient funds in the account. Please use a different card or select another form of payment.";

                // The customer is enrolled in Payer Authentication
                case 475:
                    return "The cardholder is enrolled in Payer Authentication. Please authenticate before proceeding with authorization.";

                // add additional reason codes here that you need to handle
                // specifically.

                default:
                    // For all other reason codes, return an empty string,
                    // in which case, the template will be displayed with no
                    // specific content.
                    return (String.Empty);
            }
        }

        private string HandleSignException(SignException se)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format("Failed to sign the request with error code '{0}' and " + "message '{1}'.", se.ErrorCode, se.Message);

            return string.Format(template, content);
        }

        private string HandleSoapHeaderException(SoapHeaderException she)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format("A SOAP header exception was returned with fault code '{0}' and message '{1}'.", she.Code, she.Message);

            return string.Format(template, content);
        }

        private string HandleSoapBodyException(SoapBodyException sbe)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format("A SOAP body exception was returned with fault code '{0}' and message '{1}'.", sbe.Code, sbe.Message);

            string result = string.Format(template, content);

            if (sbe.Code.Namespace.Equals(SoapClient.CYBS_NAMESPACE) &&
                sbe.Code.Name.Equals("CriticalServerError"))
            {
                /* The transaction may have been completed by CyberSource.
                 * If your request included a payment service, you should
                 * notify the appropriate department in your company (e.g. by
                 * sending an email) so that they can confirm if the request
                 * did in fact complete by searching the CyberSource Support
                 * Screens using the request id.
                 * 
                 * The line below demonstrates how to retrieve the request id.
                 */

                result += string.Format("Critical server error for Request ID: {0}.", sbe.RequestID);
            }

            return result;
        }

        private string HandleWebException(WebException we)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format("Failed to get a response with status '{0}' and message '{1}'.", we.Status, we.Message);

            string result = string.Format(template, content);

            if (IsCriticalError(we))
            {
                /*
                 * The transaction may have been completed by CyberSource.
                 * If your request included a payment service, you should
                 * notify the appropriate department in your company (e.g. by
                 * sending an email) so that they can confirm if the request
                 * did in fact complete by searching the CyberSource Support
                 * Screens using the value of the merchantReferenceCode in
                 * your request.
                 */
                result += "Critical server error.";
            }

            return result;
        }

        private string EnumerateValues(string[] array)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (array != null)
            {
                sb.Append(" (");
                foreach (string val in array)
                    sb.Append(val + ", ");
                sb.Remove(sb.Length - 2, 2).Append(")");
            }
            sb.Append(".");

            return (sb.ToString());
        }

        private bool IsCriticalError(WebException we)
        {
            switch (we.Status)
            {
                case WebExceptionStatus.ProtocolError:
                    if (we.Response != null)
                    {
                        HttpWebResponse response
                            = (HttpWebResponse)we.Response;

                        // GatewayTimeout may be returned if you are
                        // connecting through a proxy server.
                        return (response.StatusCode ==
                            HttpStatusCode.GatewayTimeout);

                    }

                    // In case of ProtocolError, the Response property
                    // should always be present.  In the unlikely case 
                    // that it is not, we assume something went wrong
                    // along the way and to be safe, treat it as a
                    // critical error.
                    return (true);

                case WebExceptionStatus.ConnectFailure:
                case WebExceptionStatus.NameResolutionFailure:
                case WebExceptionStatus.ProxyNameResolutionFailure:
                case WebExceptionStatus.SendFailure:
                    return (false);

                default:
                    return (true);
            }
        }
        public Credential GetCredential(string applicationcode, string country, string currency, string cardType)
        {
            string[] chillitalk = ConfigurationManager.AppSettings["chillitalk"].Split(',');
            string[] vectone = ConfigurationManager.AppSettings["vectone"].Split(','); ;
            string[] delightaccount = ConfigurationManager.AppSettings["delightaccount"].Split(',');
            string[] unitedphone = ConfigurationManager.AppSettings["unitedphone"].Split(',');
            Credential credent = new Credential();
            if (chillitalk.Contains(applicationcode))
            {
                credent.Account = "chillitalk";
                credent.Password = ConfigurationManager.AppSettings["chillitalk" + "Pas"];
                if (cardType.ToUpper() == "AMEX")
                    credent.SubAccount = ConfigurationManager.AppSettings["amexchillitalksubaccount" + currency.ToLower()];
                else
                {
                    //if (currency.ToUpper().Trim() == "SGD")
                    //    credent.SubAccount = "Singapore";
                    //else if (currency.ToUpper().Trim() == "ZAR")
                    //    credent.SubAccount = "South Africa";
                    //else if (currency.ToUpper().Trim() == "NOK")
                    //    credent.SubAccount = "Norway";
                    //else if (currency.ToUpper().Trim() == "HKD")
                    //    credent.SubAccount = "Hong Kong";
                    //else if (currency.ToUpper().Trim() == "CAD")
                    //    credent.SubAccount = "Canada";
                    //else if (currency.ToUpper().Trim() == "AUD")
                    //    credent.SubAccount = "Austria";
                    //else if (currency.ToUpper().Trim() == "JPY")
                    //    credent.SubAccount = "Japan";
                    //else if (currency.ToUpper().Trim() == "NZD")
                    //    credent.SubAccount = "Newzealand";
                    //if (currency.ToUpper().Trim() == "CZK")
                    //    credent.SubAccount = "Czech Republic";
                    //else
                    credent.SubAccount = currency;
                }
                credent.Refund1 = ConfigurationManager.AppSettings["chillitalk" + "Ref"];
                credent.Refund2 = ConfigurationManager.AppSettings["chillitalk" + "Ref"];
            }
            if (vectone.Contains(applicationcode))
            {
                credent.Account = "vectone";
                credent.Password = ConfigurationManager.AppSettings["vectone" + "Pas"];
                if (cardType.ToUpper() == "AMEX")
                    credent.SubAccount = ConfigurationManager.AppSettings["amexvectonesubaccount" + (country.ToUpper().Trim() == "US" ? "uk" : country.ToLower())];
                else
                    credent.SubAccount = (country.ToUpper().Trim() == "US" && currency.ToUpper().Trim() == "GBP") ? "UK" : country;
                    //credent.SubAccount = country.ToLower().Trim() == "fr" ? "at" : country;
                    
                credent.Refund1 = ConfigurationManager.AppSettings["vectone" + "Ref"];
                credent.Refund2 = ConfigurationManager.AppSettings["vectone" + "Ref"];
            }
            if (delightaccount.Contains(applicationcode))
            {
                credent.Account = "delight";
                credent.Password = ConfigurationManager.AppSettings["delightaccount" + "Pas"];
                if (cardType.ToUpper() == "AMEX")
                    credent.SubAccount = ConfigurationManager.AppSettings["amexdelightsubaccount" + country.ToLower()];
                else
                    credent.SubAccount = country;
                credent.Refund1 = ConfigurationManager.AppSettings["delightaccount" + "Ref"];
                credent.Refund2 = ConfigurationManager.AppSettings["delightaccount" + "Ref"];
            }
            if (unitedphone.Contains(applicationcode))
            {
                /*credent.Account = "unitedphone";
                credent.Password = ConfigurationManager.AppSettings["unitedphone" + "Pas"];
                credent.SubAccount = country;*/
                /* added on  20160323*/
                credent.Account = "unitedfone";
                credent.Password = ConfigurationManager.AppSettings["unitedphone" + "Pas"];
                if (cardType.ToUpper() == "AMEX")
                    credent.SubAccount = ConfigurationManager.AppSettings["amexunitedfonesubaccount"];
                else
                    credent.SubAccount = "unitedfone"; //applicationcode.ToString();

                credent.Refund1 = ConfigurationManager.AppSettings["unitedphone" + "Ref"];
                credent.Refund2 = ConfigurationManager.AppSettings["unitedphone" + "Ref"];
            }
            return credent;
        }
        #endregion
    }

    public class Credential
    {
        public string Account { get; set; }
        public string SubAccount { get; set; }
        public string Refund1 { get; set; }
        public string Refund2 { get; set; }
        public string Password { get; set; }

    }
}
