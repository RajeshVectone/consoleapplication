﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CP.BusinessLogic.CyberSource
{
    public class Log
    {



        public static void Write(string message, string in_out)
        {

            DateTime now = DateTime.Now;
            string str = string.Format("{0}-{1}.txt", in_out, now.ToString("dd-MM-yyyy"));
            string directoryName = "C:";
            now = DateTime.Now;
            string str1 = string.Format("{0}\\Logs\\9876\\{1}", directoryName, now.ToString("MM-yyyy"));
            if (!Directory.Exists(str1))
            {
                Directory.CreateDirectory(str1);
            }
            string str2 = string.Format("{0}\\{1}", str1, str);
            StreamWriter streamWriter = new StreamWriter(str2, true);
            now = DateTime.Now;
            string str3 = string.Format("{0} => {1}", now.ToString("dd/MM/yyyy hh:mm:ss"), message);
            streamWriter.WriteLine(str3);
            streamWriter.Flush();
            streamWriter.Close();

        }

        public static void Write(string message)
        {
            DateTime now = DateTime.Now;
            string fileName = string.Format("{0}-{1}.txt", "Payment", now.ToString("dd-MM-yyyy"));
            string directoryName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");
            string folderPath = string.Format("{0}\\{1}", directoryName, now.ToString("MM-yyyy"));
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fullFilePath = string.Format("{0}\\{1}", folderPath, fileName);
            StreamWriter streamWriter = new StreamWriter(fullFilePath, true);
            now = DateTime.Now;
            string str3 = string.Format("{0} => {1}", now.ToString("dd/MM/yyyy hh:mm:ss"), message);
            streamWriter.WriteLine(str3);
            streamWriter.Flush();
            streamWriter.Close();
        }

        public static void Write2(string message)
        {
            DateTime now = DateTime.Now;
            string fileName = string.Format("{0}-{1}_secret.txt", "Payment", now.ToString("dd-MM-yyyy"));
            string directoryName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");
            string folderPath = string.Format("{0}\\{1}", directoryName, now.ToString("MM-yyyy"));
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string fullFilePath = string.Format("{0}\\{1}", folderPath, fileName);
            StreamWriter streamWriter = new StreamWriter(fullFilePath, true);
            now = DateTime.Now;
            string str3 = string.Format("{0} => {1}", now.ToString("dd/MM/yyyy hh:mm:ss"), message);
            streamWriter.WriteLine(str3);
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}
