/*
Pay and Shop Limited (payandshop.com) - Licence Agreement.
� Copyright and zero Warranty Notice.


Merchants and their internet, call centre, and wireless application
developers (either in-house or externally appointed partners and
commercial organisations) may access payandshop.com technical
references, application programming interfaces (APIs) and other sample
code and software ("Programs") either free of charge from
www.payandshop.com or by emailing info@payandshop.com. 

payandshop.com provides the programs "as is" without any warranty of
any kind, either expressed or implied, including, but not limited to,
the implied warranties of merchantability and fitness for a particular
purpose. The entire risk as to the quality and performance of the
programs is with the merchant and/or the application development
company involved. Should the programs prove defective, the merchant
and/or the application development company assumes the cost of all
necessary servicing, repair or correction.

Copyright remains with payandshop.com, and as such any copyright
notices in the code are not to be removed. The software is provided as
sample code to assist internet, wireless and call center application
development companies integrate with the payandshop.com service.

Any Programs licensed by Pay and Shop to merchants or developers are
licensed on a non-exclusive basis solely for the purpose of availing
of the Pay and Shop payment solution service in accordance with the
written instructions of an authorised representative of Pay and Shop
Limited. Any other use is strictly prohibited.
*/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using System.Security.Cryptography;
using System.Xml;
using System.Net;
using System.IO;
using CP.BusinessLogic.CyberSource;
using System.Configuration;

namespace CP.BusinessLogic.RealexPayments
{

    namespace RealAuth
    {

        public class TransactionRequest
        {

            /*
             * <request timestamp="yyyymmddhhmmss" type="auth">
             *		<merchantid>realexsample</merchantid>
             *		<account>internet</account>
             *		<orderid>12345</orderid>
             * 
             *		<amount currency="EUR">4133</amount>
             *		<card>
             *			<number>4242424242424242</number>
             *			<expdate>0610</expdate>
             *			<type>VISA</type>
             *			<chname>Andrew Harcourt</chname>
             *			<issueno>19</issueno>
             *			<cvn>
             *				<number>123</number>
             *				<presind>1</presind>
             *			</cvn>
             *		</card>
             *		<autosettle flag="1"/>
             * 
             *		<sha1hash>blahblahblah</sha1hash>
             *		<comments>
             *			<comment id="1"></comment>
             *			<comment id="2"></comment>
             *		</comments>
             *		<tssinfo>
             *			<address type="billing">
             *				<code></code>
             *				<country></country>
             *			</address>
             *			<address type="shipping">
             *				<code></code>
             *				<country></country>
             *			</address>
             *			<custnum></custnum>
             *			<varref></varref>
             *			<prodid></prodid>
             *		</tssinfo>
             * </request>
             * 
             */


            // used in every transaction
            private String m_normalPassword;
            private String m_eci;

            public String Eci
            {
                get { return m_eci; }
                set { m_eci = value; }
            }
            private String m_rebatePassword;
            private String m_refundPassword;

            // used by all transaction types
            private String m_transType;
            private String m_transTimestamp;
            private String m_transMerchantName;
            private String m_transAccountName;
            private String m_transOrderID;

            private String m_transSHA1Hash;
            private ArrayList m_transComments;
            private String m_transBillingAddressCode;
            private String m_transBillingAddressCountry;
            private String m_transShippingAddressCode;
            private String m_transShippingAddressCountry;
            private String m_transCustomerNumber;
            private String m_transVariableReference;
            private String m_transProductID;
            private String m_payerref;
            private String m_paymentmethod;
            private String m_postcode;
            private String m_cavv;
            private String m_pares;
            private String m_custipaddress;
            private string m_custnum;
            private string m_productid;

            public string ProductId
            {
                get { return m_productid; }
                set { m_productid = value; }

            }

            public string CustNum
            {
                get { return m_custnum; }
                set { m_custnum = value; }

            }

            public string CustIPAddress
            {
                get { return m_custipaddress; }
                set { m_custipaddress = value; }

            }


            public String Pares
            {
                get { return m_pares; }
                set { m_pares = value; }
            }

            public String Cavv
            {
                get { return m_cavv; }
                set { m_cavv = value; }
            }
            private String m_xid;

            public String Xid
            {
                get { return m_xid; }
                set { m_xid = value; }
            }


            public String Postcode
            {
                get { return m_postcode; }
                set { m_postcode = value; }
            }
            private String m_email;

            public String Email
            {
                get { return m_email; }
                set { m_email = value; }
            }
            private String m_city;

            public String City
            {
                get { return m_city; }
                set { m_city = value; }
            }
            // used by *some* transaction types
            private Int64 m_transAmount;
            private String m_transCurrency;
            private CreditCard m_transCard;
            private int m_transAutoSettle;
            private String m_transPASRef;
            private String m_transAuthCode;

            //TODO: add any additional instance variables you would like to send to Realex here
            //private String m_transMyInterestingVariableName;

            // public properties reprenting the protected vars above
            public String TransType
            {
                get
                {
                    return (m_transType);
                }

                set
                {
                    value = value.ToLower();
                    switch (value)
                    {

                        case ("auth"):
                        case ("void"):
                        case ("settle"):
                        case ("credit"):
                        case ("rebate"):
                        case ("tss"):
                        case ("3ds-verifyenrolled"):
                        case ("3ds-verifysig"):
                        case ("offline"):
                        case ("card-new"):
                        case ("payer-new"):
                        case ("receipt-in"):
                            m_transType = value;
                            break;

                        case ("magicnewtransactiontype"):
                            throw new DataValidationException("Transaction type " + value + "not yet implemented.");

                        default:
                            throw new DataValidationException("Unknown transaction type requested.");
                    }
                }
            }

            public String TransTimestamp
            {
                get
                {
                    return (m_transTimestamp);
                }

                set
                {
                    throw new ReadOnlyException("This property is read-only.");
                }
            }

            public String TransMerchantName
            {
                get
                {
                    return (m_transMerchantName);
                }

                set
                {
                    m_transMerchantName = value;
                }
            }

            public String TransAccountName
            {
                get
                {
                    return (m_transAccountName);
                }

                set
                {
                    m_transAccountName = value;
                }
            }

            public String TransOrderID
            {
                get
                {
                    return (m_transOrderID);
                }

                set
                {
                    m_transOrderID = value;
                }
            }

            public ArrayList TransComments
            {
                get
                {
                    return (m_transComments);
                }

                set
                {
                    //FIXME: how should this be done?
                }
            }

            public String TransBillingAddressCode
            {
                get
                {
                    return (m_transBillingAddressCode);
                }

                set
                {
                    m_transBillingAddressCode = value;
                }
            }

            public String TransBillingAddressCountry
            {
                get
                {
                    return (m_transBillingAddressCountry);
                }

                set
                {
                    value = value.ToUpper();
                    Validator.assertAlphaStrict("Billing Country", value);
                    Validator.assertLength("Billing Country", value, 2);
                    m_transBillingAddressCountry = value;
                }
            }

            public String TransShippingAddressCode
            {
                get
                {
                    return (m_transShippingAddressCode);
                }

                set
                {
                    m_transShippingAddressCode = value;
                }
            }

            public String TransShippingAddressCountry
            {
                get
                {
                    return (m_transShippingAddressCountry);
                }

                set
                {
                    value = value.ToUpper();
                    Validator.assertAlphaStrict("Shipping Country", value);
                    Validator.assertLength("Shipping Country", value, 2);
                    m_transShippingAddressCountry = value;
                }
            }

            public String TransCustomerNumber
            {
                get
                {
                    return (m_transCustomerNumber);
                }

                set
                {
                    Validator.assertAlphaNumericLoose("Customer Number", value);
                    m_transCustomerNumber = value;
                }
            }

            public String TransVariableReference
            {
                get
                {
                    return (m_transVariableReference);
                }

                set
                {
                    Validator.assertAlphaNumericLoose("Variable Reference", value);
                    m_transVariableReference = value;
                }
            }

            public String TransProductID
            {
                get
                {
                    return (m_transProductID);
                }

                set
                {
                    Validator.assertAlphaNumericLoose("Product ID", value);
                    m_transProductID = value;
                }
            }


            // used by *some* transaction types
            public Int64 TransAmount
            {
                get
                {
                    return (m_transAmount);
                }

                set
                {
                    m_transAmount = value;
                }
            }

            public String TransCurrency
            {
                get
                {
                    return (m_transCurrency);
                }

                set
                {
                    m_transCurrency = value;
                }
            }

            public CreditCard TransCard
            {
                get
                {
                    return (m_transCard);
                }

                set
                {
                    m_transCard = value;
                }
            }

            public int TransAutoSettle
            {
                get
                {
                    return (m_transAutoSettle);
                }

                set
                {
                    m_transAutoSettle = value;
                }
            }

            public String TransPASRef
            {
                get
                {
                    return (m_transPASRef);
                }

                set
                {
                    m_transPASRef = value;
                }
            }

            public String TransAuthCode
            {
                get
                {
                    return (m_transAuthCode);
                }

                set
                {
                    m_transAuthCode = value;
                }
            }

            private String m_payertype;

            public String Payertype
            {
                get { return m_payertype; }
                set { m_payertype = value; }
            }
            public String Payerref
            {
                get { return m_payerref; }
                set { m_payerref = value; }
            }

            public String Paymentmethod
            {
                get { return m_paymentmethod; }
                set { m_paymentmethod = value; }
            }




            //TODO: Add your property handler(s) for your own variables here
            /*
            public String TransMyInterestingVariableName {
                get {
                    return (m_transMyInterestingVariableName);
                }
                set {
                    m_transMyInterestingVariableName = value;
                }
            }
            */


            // Constructor(s)
            public TransactionRequest(string merchantName, String normalPassword, String rebatePassword, String refundPassword)
            {

                m_transComments = new ArrayList();

                m_transMerchantName = merchantName;
                m_normalPassword = normalPassword;
                m_rebatePassword = rebatePassword;
                m_refundPassword = refundPassword;
                m_transAutoSettle = 1;
            }


            // Methods and other gack
            public void ContinueTransaction(TransactionResponse transactionResponse)
            {

                // nuke values that definitely won't be in the next transaction
                m_transTimestamp = null;
                m_transType = null;
                m_transSHA1Hash = null;

                TransPASRef = transactionResponse.ResultPASRef;
                TransAuthCode = transactionResponse.ResultAuthCode;
                TransOrderID = transactionResponse.ResultOrderID;
            }

            private void generateTimestamp()
            {
                m_transTimestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            }

            private String hexEncode(byte[] data)
            {

                String result = "";
                foreach (byte b in data)
                {
                    result += b.ToString("X2");
                }
                result = result.ToLower();

                return (result);
            }

            private void generateSHA1HashReceiptIn()
            {

                SHA1 sha = new SHA1Managed();

                String hashInput =
                    m_transTimestamp + "." +
                    m_transMerchantName + "." +
                    m_transOrderID + "." +
                    m_transAmount + "." +
                    m_transCurrency + "." +
                   m_payerref;

                String hashStage1 =
                    hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashInput))) + "." +
                    m_normalPassword;

                String hashStage2 =
                    hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashStage1)));

                m_transSHA1Hash = hashStage2;
            }

            private void generateSHA1Hash()
            {

                SHA1 sha = new SHA1Managed();

                String hashInput =
                    m_transTimestamp + "." +
                    m_transMerchantName + "." +
                    m_transOrderID + "." +
                    m_transAmount + "." +
                    m_transCurrency + "." +
                    m_transCard.CardNumber;

                String hashStage1 =
                    hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashInput))) + "." +
                    m_normalPassword;

                String hashStage2 =
                    hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashStage1)));

                m_transSHA1Hash = hashStage2;
            }
            private void generateSHA1HashPayerNew()
            {
                //timestamp.merchantid.orderid.amount.currency.payerref.chname.(card)number
                //imestamp.merchantid.orderid.amount.currency.payerref
                SHA1 sha = new SHA1Managed();

                String hashInput =
                   m_transTimestamp + "." +
                   m_transMerchantName + "." +
                   m_transOrderID + "." +
                    "." +
                    "." +
                    m_transCard.Payerref;// +
                //  m_transCard.CardholderName + "." +
                //m_transCard.CardNumber;

                String hashStage1 =
                    hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashInput))) + "." +
                    m_normalPassword;

                String hashStage2 =
                    hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashStage1)));

                m_transSHA1Hash = hashStage2;
            }
            private void generateSHA1HashCardNew()
            {

                SHA1 sha = new SHA1Managed();
                //String hashInput =
                //    m_transTimestamp + "." +
                //    m_transMerchantName + "." +
                //    m_transOrderID + "." +
                //    m_transAmount + "." +
                //    m_transCurrency + "." +
                //    m_payerref + "." +
                //    m_transCard.CardholderName + "." +
                //    m_transCard.CardNumber;

                String hashInput =
                    m_transTimestamp + "." +
                    m_transMerchantName + "." +
                    m_transOrderID + "." +
                    "." +
                    "." +
                    m_payerref + "." +
                    m_transCard.CardholderName + "." +
                    m_transCard.CardNumber;

                //String hashInput =
                //    m_transTimestamp + "." +
                //    m_transMerchantName + "." +
                //    m_transOrderID + "." +
                //    m_transAmount + "." +
                //    m_transCurrency + "." +
                //    m_transCard.CardNumber;

                String hashStage1 =
                    hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashInput))) + "." +
                    m_normalPassword;

                String hashStage2 =
                    hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashStage1)));

                m_transSHA1Hash = hashStage2;
            }

            public TransactionResponse Authorize_direct(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {

                TransType = "auth";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;
                TransAutoSettle = 1;

                return (SubmitTransactionAuthorise());
            }



            public TransactionResponse Authorize_new(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {

                TransType = "auth";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;
                TransAutoSettle = 1;

                return (SubmitTransactionNew());
            }

            public TransactionResponse Authorize(String transAccount, String transOrderID, String transCurrency, Int16 transAmount, CreditCard transCard)
            {

                TransType = "auth";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;
                TransAutoSettle = 1;

                return (SubmitTransaction());
            }

            public TransactionResponse ThreeDSecurity(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {
                this.TransType = "3ds-verifyenrolled";
                this.TransAccountName = transAccount;
                this.TransOrderID = transOrderID;
                this.TransCurrency = transCurrency;
                this.TransAmount = transAmount;
                this.TransCard = transCard;
                this.TransAutoSettle = 0;
                //return this.SubmitTransaction();

                return (ThreeDSubmitTransaction());
            }
            public TransactionResponse ThreeDVerifySign(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {
                this.TransType = "3ds-verifysig";
                this.TransAccountName = transAccount;
                this.TransOrderID = transOrderID;
                this.TransCurrency = transCurrency;
                this.TransAmount = transAmount;
                this.TransCard = transCard;
                this.TransAutoSettle = 0;
                //return this.SubmitTransaction();

                return (ThreeDSVerifyTransaction());
            }
            public TransactionResponse Rebate(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {

                TransType = "rebate";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;
                TransAutoSettle = 1;

                return (SubmitTransaction());
            }
            public TransactionResponse PayerNew(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {
                //Log.Write("PayerNew()");
                TransType = "payer-new";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;
                TransAutoSettle = 0;

                return (NewCardSubmit());
            }
            public TransactionResponse CardNew(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {

                TransType = "card-new";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;
                TransAutoSettle = 0;

                return (SubmitCardNewTransaction());
            }

            public TransactionResponse ReceiptIn(String transAccount, String transOrderID, String transCurrency, Int64 transAmount)
            {

                TransType = "receipt-in";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                //TransCard = transCard;
                TransAutoSettle = 1;

                return (SubmitReceiptIn());
            }

            public TransactionResponse Credit(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {

                TransType = "credit";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;
                TransAutoSettle = 1;

                return (SubmitTransaction());
            }

            public TransactionResponse Void(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {

                TransType = "void";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;

                return (SubmitVoidTransaction());
            }

            public TransactionResponse TSS(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {

                TransType = "tss";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;

                return (SubmitTransaction());
            }

            public TransactionResponse Offline(String transAccount, String transOrderID, String transCurrency, Int64 transAmount, CreditCard transCard)
            {

                TransType = "offline";
                TransAccountName = transAccount;
                TransOrderID = transOrderID;
                TransCurrency = transCurrency;
                TransAmount = transAmount;
                TransCard = transCard;
                TransAutoSettle = 0;

                return (SubmitTransaction());
            }
            public TransactionResponse SubmitCardNewTransaction()
            {
                //03-Oct-2018 : Moorthy : Added for Card Storage
                //Log.Write("SubmitCardNewTransaction()");
                try
                {
                    String requestXML = this.ToCardNewXML();

                    //03-Oct-2018 : Moorthy : Added for Card Storage
                    //Log.Write("requestXML : " + requestXML);

                    //11-Oct-2017 : Moorthy : Added for the Realex TLS 1.2 Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-remote.cgi");
                    //  HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://3DSecure.elavonpaymentgateway.com/3dsecure");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = true;
                    wReq.ContentLength = requestXML.Length;
                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);
                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                    String responseXML = sResp.ReadToEnd();

                    //03-Oct-2018 : Moorthy : Added for Card Storage
                    //Log.Write("responseXML : " + responseXML);

                    sResp.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }

            public TransactionResponse SubmitReceiptIn()
            {
                Log.Write("SubmitReceiptIn()");

                try
                {
                    String requestXML = this.ToXMLReceiptIn();
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    //    Log.Write2("requestXML : " + requestXML);

                    //11-Oct-2017 : Moorthy : Added for the Realex Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-remote.cgi");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = false;
                    wReq.ContentLength = requestXML.Length;
                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);

                    //Log.Write(requestXML);

                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();

                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                    String responseXML = sResp.ReadToEnd();

                    Log.Write("responseXML : " + responseXML);

                    sResp.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }

            public TransactionResponse SubmitTransactionAuthorise()
            {
                Log.Write("SubmitTransactionAuthorise() ");

                try
                {
                    String requestXML = this.ToXMLSecureAuthorise();
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    //Log.Write2("requestXML : " + requestXML);

                    //11-Oct-2017 : Moorthy : Added for the Realex Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-remote.cgi");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = false;
                    wReq.ContentLength = requestXML.Length;
                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);
                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                    String responseXML = sResp.ReadToEnd();
                    sResp.Close();

                    //Log.Write("Auth-Request:" + requestXML);
                    //Log.Write2("responseXML : " + responseXML);
                    Log.Write("responseXML : " + responseXML);

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }



            public TransactionResponse SubmitTransactionNew()
            {
                Log.Write("SubmitTransactionNew()");

                try
                {
                    String requestXML = this.ToXMLSecureAuthNew();
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    //    Log.Write2("requestXML : " + requestXML);

                    //11-Oct-2017 : Moorthy : Added for the Realex Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-remote.cgi");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = false;
                    wReq.ContentLength = requestXML.Length;
                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);
                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                    String responseXML = sResp.ReadToEnd();
                    sResp.Close();

                    //Log.Write("Auth-Request:" + requestXML);
                    //Log.Write("Auth-Response:" + responseXML);
                    Log.Write("responseXML : " + responseXML);
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    //    Log.Write2("requestXML : " + requestXML);
                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }










            public TransactionResponse SubmitVoidTransaction()
            {
                Log.Write("SubmitVoidTransaction()");
                try
                {

                    String requestXML = this.ToXMLSecureVoid();
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    //    Log.Write2("requestXML : " + requestXML);

                    //11-Oct-2017 : Moorthy : Added for the Realex Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-remote.cgi");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = false;
                    wReq.ContentLength = requestXML.Length;
                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);
                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                    String responseXML = sResp.ReadToEnd();
                    Log.Write("responseXML : " + responseXML);
                    sResp.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }

            public TransactionResponse SubmitTransaction()
            {
                Log.Write("SubmitTransaction()");
                try
                {

                    String requestXML = this.ToXMLSecureAuth();
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    //    Log.Write2("requestXML : " + requestXML);

                    //11-Oct-2017 : Moorthy : Added for the Realex Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-remote.cgi");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = false;
                    wReq.ContentLength = requestXML.Length;
                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);
                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                    String responseXML = sResp.ReadToEnd();
                    Log.Write("responseXML : " + responseXML);
                    sResp.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }
            public TransactionResponse ThreeDSubmitTransaction()
            {
                Log.Write("ThreeDSubmitTransaction()");
                try
                {
                    String requestXML = this.ToXML();
                    //Log.Write("3DS Request:" + requestXML);

                    //21-Sep-2017 : Moorthy : Added for the Realex Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-3dsecure.cgi");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = false;
                    wReq.ContentLength = requestXML.Length;
                    //03-Jul-2019 : Moorthy : Added & Commented for 'Bytes to be written to the stream exceed the Content-Length bytes size specified.'
                    //byte[] bytes = Encoding.ASCII.GetBytes(requestXML);
                    //wReq.ContentLength = bytes.Length;

                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);
                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());
                    String responseXML = sResp.ReadToEnd();
                    Log.Write("3DS Response:" + responseXML);
                    sResp.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }
            public TransactionResponse ThreeDSVerifyTransaction()
            {
                Log.Write("ThreeDSVerifyTransaction()");
                try
                {
                    String requestXML = this.ToXML();
                    //if (ConfigurationManager.AppSettings["DoLog"] != null && ConfigurationManager.AppSettings["DoLog"] == "1")
                    //    Log.Write2("Request XML : " + requestXML);

                    //21-Sep-2017 : Moorthy : Added for the Realex Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-3dsecure.cgi");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = false;
                    wReq.ContentLength = requestXML.Length;
                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);
                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                    String responseXML = sResp.ReadToEnd();
                    sResp.Close();

                    //03-Feb-2017 : Moorthy : Added Log
                    Log.Write("Response XML : " + responseXML);

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }
            public TransactionResponse NewCardSubmit()
            {
                //03-Oct-2018 : Moorthy : Added for Card Storage
                Log.Write("NewCardSubmit()");
                try
                {
                    String requestXML = this.ToXMLNewPayer();

                    //03-Oct-2018 : Moorthy : Added for Card Storage
                    //Log.Write("requestXML : " + requestXML);

                    //11-Oct-2017 : Moorthy : Added for the Realex Update
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://epage.payandshop.com/epage-remote.cgi");
                    //  HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create("https://3DSecure.elavonpaymentgateway.com/3dsecure");
                    wReq.ContentType = "text/xml";
                    wReq.UserAgent = "Realex Payments";
                    wReq.Timeout = 145 * 1000;	// milliseconds
                    wReq.AllowAutoRedirect = true;
                    wReq.ContentLength = requestXML.Length;
                    wReq.Method = "POST";

                    StreamWriter sReq = new StreamWriter(wReq.GetRequestStream());
                    sReq.Write(requestXML);
                    sReq.Flush();
                    sReq.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("request.xml", requestXML);

                    HttpWebResponse wResp = (HttpWebResponse)wReq.GetResponse();
                    StreamReader sResp = new StreamReader(wResp.GetResponseStream());

                    String responseXML = sResp.ReadToEnd();

                    //03-Oct-2018 : Moorthy : Added for Card Storage
                    //Log.Write("responseXML : " + responseXML);


                    sResp.Close();

                    // dump i/o to files for debugging purposes
                    //TODO: if you have trouble with your requests, uncomment the line below to save a copy.
                    // PLEASE remember to remove the line again before you go live; otherwise you will be keeping
                    // your customers' credit card data in cleartext on your server.
                    //File.WriteAllText("response.xml", responseXML);

                    return (new TransactionResponse(responseXML));
                }
                catch (WebException e)
                {
                    Log.Write("Error : " + e.StackTrace);
                    Log.Write("Error : " + e.Message);
                    throw new TransactionFailedException("Web request failed or timed out: " + e.Message);
                }
            }
            protected String ToXML()
            {

                generateTimestamp();	// timestamp the request as it's generated
                generateSHA1Hash();	// ... and ensure that we have a correct hash

                // NOTE: element variable names are named in the XML case, not in camel case, so as to
                // avoid confusion in mapping between the two.

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("timestamp", m_transTimestamp);
                    xml.WriteAttributeString("type", m_transType);

                    xml.WriteElementString("merchantid", m_transMerchantName);
                    xml.WriteElementString("account", m_transAccountName);
                    xml.WriteElementString("orderid", m_transOrderID);

                    switch (m_transType)
                    {
                        case ("auth"):
                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("3ds-verifyenrolled"):
                        case ("3ds-verifysig"):
                        case ("tss"):

                            xml.WriteStartElement("amount");
                            xml.WriteAttributeString("currency", m_transCurrency);
                            xml.WriteString(m_transAmount.ToString());
                            xml.WriteEndElement();

                            // m_transCard.WriteXMLAuthSecure(xml);
                            m_transCard.WriteXML(xml);

                            xml.WriteStartElement("autosettle");
                            xml.WriteAttributeString("flag", m_transAutoSettle.ToString());
                            // xml.WriteAttributeString("eci", m_eci.ToString());
                            xml.WriteEndElement();
                            break;
                    }

                    switch (m_transType)
                    {
                        case ("3ds-verifysig"):
                        case ("credit"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("pasref", m_transPASRef);
                            xml.WriteElementString("pares", m_transPASRef);
                            break;
                    }

                    switch (m_transType)
                    {

                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("authcode", m_transAuthCode);
                            break;
                    }

                    xml.WriteElementString("sha1hash", m_transSHA1Hash);

                    // if this is a transaction requiring an additional hash, include it here
                    SHA1 sha = new SHA1Managed();
                    switch (m_transType)
                    {
                        case ("credit"):
                            String refundHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_refundPassword)));
                            xml.WriteElementString("refundhash", refundHash);
                            break;
                        case ("rebate"):
                            String rebateHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_rebatePassword)));
                            xml.WriteElementString("refundhash", rebateHash);   // this is still sent as "refundhash", not "rebatehash"
                            break;
                    }

                    xml.WriteStartElement("comments");
                    {
                        int iComment = 1;	// this must start from 1, not 0.
                        foreach (String comment in m_transComments)
                        {
                            xml.WriteStartElement("comment");
                            xml.WriteAttributeString("id", iComment.ToString());
                            xml.WriteString(comment);
                            xml.WriteEndElement();

                            iComment++;
                        }
                    }
                    xml.WriteEndElement();

                    xml.WriteStartElement("tssinfo");
                    {
                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "billing");
                            xml.WriteElementString("code", m_transBillingAddressCode);
                            xml.WriteElementString("country", m_transBillingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "shipping");
                            xml.WriteElementString("code", m_transShippingAddressCode);
                            xml.WriteElementString("country", m_transShippingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteElementString("custnum", m_transCustomerNumber);
                            xml.WriteElementString("varref", m_transVariableReference);
                            xml.WriteElementString("prodid", m_transProductID);
                        }
                    }
                    xml.WriteEndElement();
                }

                //TODO: if you wish to send Realex any additional variables, include them here
                //xml.WriteElementString("MyInterestingVariable", m_myInterestingVariableName);

                xml.WriteEndElement();

                xml.Flush();
                xml.Close();

                return (strBuilder.ToString());
            }

            protected String ToXMLReceiptIn()
            {

                generateTimestamp();	// timestamp the request as it's generated
                generateSHA1HashReceiptIn();	// ... and ensure that we have a correct hash

                // NOTE: element variable names are named in the XML case, not in camel case, so as to
                // avoid confusion in mapping between the two.

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("timestamp", m_transTimestamp);
                    xml.WriteAttributeString("type", m_transType);
                    xml.WriteElementString("merchantid", m_transMerchantName);
                    xml.WriteElementString("account", m_transAccountName);
                    xml.WriteElementString("orderid", m_transOrderID); // It is optional for Receipt-in
                    xml.WriteStartElement("autosettle");
                    xml.WriteAttributeString("flag", m_transAutoSettle.ToString());
                    xml.WriteEndElement();
                    xml.WriteStartElement("amount");
                    xml.WriteAttributeString("currency", m_transCurrency);
                    xml.WriteString(m_transAmount.ToString());
                    xml.WriteEndElement();
                    xml.WriteElementString("payerref", m_payerref.ToString());
                    xml.WriteElementString("paymentmethod", m_paymentmethod.ToString());

                    //13-Jul-2018 : Added for cvn
                    if (!String.IsNullOrEmpty(m_cavv))
                    {
                        xml.WriteStartElement("paymentdata");
                        {
                            xml.WriteStartElement("cvn");
                            {
                                xml.WriteElementString("number", m_cavv);
                            }
                            xml.WriteEndElement();
                        }
                        xml.WriteEndElement();
                    }

                    xml.WriteElementString("sha1hash", m_transSHA1Hash);
                }

                //TODO: if you wish to send Realex any additional variables, include them here
                //xml.WriteElementString("MyInterestingVariable", m_myInterestingVariableName);
                xml.WriteStartElement("tssinfo");
                {
                    //Commented to avoid fraud checks
                    //xml.WriteElementString("custipaddress", m_custipaddress);
                    xml.WriteElementString("custnum", m_email == "N/A" ? "" : m_email);
                    //Commented to avoid fraud checks
                    //xml.WriteElementString("varref", m_custipaddress);
                    xml.WriteElementString("prodid", m_custnum);
                }

                xml.WriteEndElement();

                xml.Flush();
                xml.Close();

                return (strBuilder.ToString());
            }

            protected String ToXMLSecureAuthorise()
            {

                generateTimestamp();	// timestamp the request as it's generated
                generateSHA1Hash();	// ... and ensure that we have a correct hash

                // NOTE: element variable names are named in the XML case, not in camel case, so as to
                // avoid confusion in mapping between the two.

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("timestamp", m_transTimestamp);
                    xml.WriteAttributeString("type", m_transType);

                    xml.WriteElementString("merchantid", m_transMerchantName);
                    xml.WriteElementString("account", m_transAccountName);
                    xml.WriteElementString("orderid", m_transOrderID);

                    switch (m_transType)
                    {
                        case ("auth"):
                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("3ds-verifyenrolled"):
                        case ("3ds-verifysig"):
                        case ("tss"):

                            xml.WriteStartElement("amount");
                            xml.WriteAttributeString("currency", m_transCurrency);
                            xml.WriteString(m_transAmount.ToString());
                            xml.WriteEndElement();

                            // m_transCard.WriteXMLAuthSecure(xml);
                            m_transCard.WriteXMLAuthSecure(xml);
                            //xml.WriteElementString("pares", m_pares);
                            xml.WriteStartElement("autosettle");
                            xml.WriteAttributeString("flag", "1");
                            //18-Feb-2019: Moorthy : Bookmark for ECI value
                            //xml.WriteAttributeString("eci", m_eci.ToString());
                            xml.WriteEndElement();
                            break;
                    }

                    xml.WriteElementString("sha1hash", m_transSHA1Hash);

                    // if this is a transaction requiring an additional hash, include it here
                    SHA1 sha = new SHA1Managed();


                    xml.WriteStartElement("comments");
                    {
                        int iComment = 1;	// this must start from 1, not 0.
                        foreach (String comment in m_transComments)
                        {
                            xml.WriteStartElement("comment");
                            xml.WriteAttributeString("id", iComment.ToString());
                            xml.WriteString(comment);
                            xml.WriteEndElement();

                            iComment++;
                        }
                    }
                    xml.WriteEndElement();

                    xml.WriteStartElement("tssinfo");
                    {
                        {
                            /* Old code as per Dharmendra instruntion
                             * 
                             * xml.WriteElementString("custipaddress", m_custipaddress);
                            xml.WriteElementString("custnum", m_custnum);
                            xml.WriteElementString("varref", m_email);
                            xml.WriteElementString("prodid", m_productid);
                            */
                            /*New Setup will be darmendra ref mail 20160127 */
                            if (m_custipaddress.Trim() != "80.74.227.136" && m_custipaddress.Trim() != "80.74.227.135" && m_custipaddress.Trim().Length <= 15) // Added to avoid fraud checks for 3G customer (80.74.227.136/135 is our proxy ip for 3G customers) && m_custipaddress.Trim().Length <= 15 To store IPv4 address we require 15 characters.
                                xml.WriteElementString("custipaddress", m_custipaddress);

                            if (string.IsNullOrEmpty(m_email))
                                xml.WriteElementString("custnum", m_email);

                            if (m_custipaddress.Trim() != "80.74.227.136" && m_custipaddress.Trim() != "80.74.227.135" && m_custipaddress.Trim().Length <= 15) // Added to avoid fraud checks for 3G customer (80.74.227.136/135 is our proxy ip for 3G customers) && m_custipaddress.Trim().Length <= 15 To store IPv4 address we require 15 characters.
                                xml.WriteElementString("varref", m_custipaddress);
                            xml.WriteElementString("prodid", m_custnum);


                        }




                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "billing");
                            xml.WriteElementString("code", m_transBillingAddressCode);
                            xml.WriteElementString("country", m_transBillingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "shipping");
                            xml.WriteElementString("code", m_transShippingAddressCode);
                            xml.WriteElementString("country", m_transShippingAddressCountry);
                            xml.WriteEndElement();
                        }


                    }
                    xml.WriteEndElement();
                }

                //TODO: if you wish to send Realex any additional variables, include them here
                //xml.WriteElementString("MyInterestingVariable", m_myInterestingVariableName);

                xml.WriteEndElement();

                xml.Flush();
                xml.Close();

                return (strBuilder.ToString());
            }



            protected String ToXMLSecureAuthNew()
            {

                generateTimestamp();	// timestamp the request as it's generated
                generateSHA1Hash();	// ... and ensure that we have a correct hash

                // NOTE: element variable names are named in the XML case, not in camel case, so as to
                // avoid confusion in mapping between the two.

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("timestamp", m_transTimestamp);
                    xml.WriteAttributeString("type", m_transType);

                    xml.WriteElementString("merchantid", m_transMerchantName);
                    xml.WriteElementString("account", m_transAccountName);
                    xml.WriteElementString("orderid", m_transOrderID);

                    switch (m_transType)
                    {
                        case ("auth"):
                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("3ds-verifyenrolled"):
                        case ("3ds-verifysig"):
                        case ("tss"):

                            xml.WriteStartElement("amount");
                            xml.WriteAttributeString("currency", m_transCurrency);
                            xml.WriteString(m_transAmount.ToString());
                            xml.WriteEndElement();

                            // m_transCard.WriteXMLAuthSecure(xml);
                            m_transCard.WriteXMLAuthSecure(xml);
                            xml.WriteStartElement("mpi");
                            {
                                xml.WriteElementString("cavv", m_cavv);
                                xml.WriteElementString("xid", m_xid);
                                xml.WriteElementString("eci", m_eci);
                            }
                            xml.WriteEndElement();
                            xml.WriteElementString("pares", m_pares);
                            xml.WriteStartElement("autosettle");
                            xml.WriteAttributeString("flag", m_transAutoSettle.ToString());
                            //xml.WriteAttributeString("eci", m_eci.ToString());
                            xml.WriteEndElement();
                            break;
                    }

                    switch (m_transType)
                    {
                        case ("3ds-verifysig"):
                        case ("credit"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("pasref", m_transPASRef);
                            xml.WriteElementString("pares", m_transPASRef);
                            break;
                    }

                    switch (m_transType)
                    {

                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("authcode", m_transAuthCode);
                            break;
                    }

                    xml.WriteElementString("sha1hash", m_transSHA1Hash);

                    // if this is a transaction requiring an additional hash, include it here
                    SHA1 sha = new SHA1Managed();
                    switch (m_transType)
                    {
                        case ("credit"):
                            String refundHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_refundPassword)));
                            xml.WriteElementString("refundhash", refundHash);
                            break;
                        case ("rebate"):
                            String rebateHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_rebatePassword)));
                            xml.WriteElementString("refundhash", rebateHash);   // this is still sent as "refundhash", not "rebatehash"
                            break;
                    }

                    xml.WriteStartElement("comments");
                    {
                        int iComment = 1;	// this must start from 1, not 0.
                        foreach (String comment in m_transComments)
                        {
                            xml.WriteStartElement("comment");
                            xml.WriteAttributeString("id", iComment.ToString());
                            xml.WriteString(comment);
                            xml.WriteEndElement();

                            iComment++;
                        }
                    }
                    xml.WriteEndElement();

                    xml.WriteStartElement("tssinfo");
                    {



                        {
                            /* Old code Dharmendra instruntion
                             * 
                             * xml.WriteElementString("custipaddress", m_custipaddress);
                             xml.WriteElementString("custnum", m_custnum);
                             xml.WriteElementString("varref", m_email);
                             xml.WriteElementString("prodid", m_productid);
                             */
                            /*New Setup  darmendra ref mail 20160127 */
                            if (m_custipaddress.Trim() != "80.74.227.136" && m_custipaddress.Trim() != "80.74.227.135" && m_custipaddress.Trim().Length <= 15) // Added to avoid fraud checks for 3G customer (80.74.227.136/135 is our proxy ip for 3G customers) && m_custipaddress.Trim().Length <= 15 To store IPv4 address we require 15 characters.
                                xml.WriteElementString("custipaddress", m_custipaddress);
                            if (string.IsNullOrEmpty(m_email))
                                xml.WriteElementString("custnum", m_email);
                            if (m_custipaddress.Trim() != "80.74.227.136" && m_custipaddress.Trim() != "80.74.227.135" && m_custipaddress.Trim().Length <= 15) // Added to avoid fraud checks for 3G customer (80.74.227.136/135 is our proxy ip for 3G customers) && m_custipaddress.Trim().Length <= 15 To store IPv4 address we require 15 characters.
                                xml.WriteElementString("varref", m_custipaddress);
                            xml.WriteElementString("prodid", m_custnum);

                        }




                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "billing");
                            xml.WriteElementString("code", m_transBillingAddressCode);
                            xml.WriteElementString("country", m_transBillingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "shipping");
                            xml.WriteElementString("code", m_transShippingAddressCode);
                            xml.WriteElementString("country", m_transShippingAddressCountry);
                            xml.WriteEndElement();
                        }


                    }
                    xml.WriteEndElement();
                }

                //TODO: if you wish to send Realex any additional variables, include them here
                //xml.WriteElementString("MyInterestingVariable", m_myInterestingVariableName);

                xml.WriteEndElement();

                xml.Flush();
                xml.Close();

                return (strBuilder.ToString());
            }

            protected String ToXMLSecureVoid()
            {

                generateTimestamp();	// timestamp the request as it's generated
                generateSHA1Hash();	// ... and ensure that we have a correct hash

                // NOTE: element variable names are named in the XML case, not in camel case, so as to
                // avoid confusion in mapping between the two.

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("timestamp", m_transTimestamp);
                    xml.WriteAttributeString("type", m_transType);

                    xml.WriteElementString("merchantid", m_transMerchantName);
                    xml.WriteElementString("account", m_transAccountName);
                    xml.WriteElementString("orderid", m_transOrderID);

                    switch (m_transType)
                    {
                        case ("auth"):
                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("3ds-verifyenrolled"):
                        case ("3ds-verifysig"):
                        case ("tss"):

                            xml.WriteStartElement("amount");
                            xml.WriteAttributeString("currency", m_transCurrency);
                            xml.WriteString(m_transAmount.ToString());
                            xml.WriteEndElement();

                            // m_transCard.WriteXMLAuthSecure(xml);
                            m_transCard.WriteXMLAuthSecure(xml);
                            xml.WriteStartElement("mpi");
                            {
                                xml.WriteElementString("cavv", m_cavv);
                                xml.WriteElementString("xid", m_xid);
                                xml.WriteElementString("eci", m_eci);
                            }
                            xml.WriteEndElement();
                            xml.WriteElementString("pares", m_pares);
                            xml.WriteStartElement("autosettle");
                            xml.WriteAttributeString("flag", m_transAutoSettle.ToString());
                            // xml.WriteAttributeString("eci", m_eci.ToString());
                            xml.WriteEndElement();
                            break;
                    }

                    switch (m_transType)
                    {
                        case ("3ds-verifysig"):
                        case ("credit"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("pasref", m_transPASRef);
                            xml.WriteElementString("pares", m_transPASRef);
                            break;
                    }

                    switch (m_transType)
                    {

                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("authcode", m_transAuthCode);
                            break;
                    }

                    xml.WriteElementString("sha1hash", m_transSHA1Hash);

                    // if this is a transaction requiring an additional hash, include it here
                    SHA1 sha = new SHA1Managed();
                    switch (m_transType)
                    {
                        case ("credit"):
                            String refundHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_refundPassword)));
                            xml.WriteElementString("refundhash", refundHash);
                            break;
                        case ("rebate"):
                            String rebateHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_rebatePassword)));
                            xml.WriteElementString("refundhash", rebateHash);   // this is still sent as "refundhash", not "rebatehash"
                            break;
                    }

                    xml.WriteStartElement("comments");
                    {
                        int iComment = 1;	// this must start from 1, not 0.
                        foreach (String comment in m_transComments)
                        {
                            xml.WriteStartElement("comment");
                            xml.WriteAttributeString("id", iComment.ToString());
                            xml.WriteString(comment);
                            xml.WriteEndElement();

                            iComment++;
                        }
                    }
                    xml.WriteEndElement();

                    xml.WriteStartElement("tssinfo");
                    {
                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "billing");
                            xml.WriteElementString("code", m_transBillingAddressCode);
                            xml.WriteElementString("country", m_transBillingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "shipping");
                            xml.WriteElementString("code", m_transShippingAddressCode);
                            xml.WriteElementString("country", m_transShippingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteElementString("custnum", m_transCustomerNumber);
                            xml.WriteElementString("varref", m_transVariableReference);
                            xml.WriteElementString("prodid", m_transProductID);
                        }
                    }
                    xml.WriteEndElement();
                }

                //TODO: if you wish to send Realex any additional variables, include them here
                //xml.WriteElementString("MyInterestingVariable", m_myInterestingVariableName);

                xml.WriteEndElement();

                xml.Flush();
                xml.Close();

                return (strBuilder.ToString());
            }






            protected String ToXMLSecureAuth()
            {

                generateTimestamp();	// timestamp the request as it's generated
                generateSHA1Hash();	// ... and ensure that we have a correct hash

                // NOTE: element variable names are named in the XML case, not in camel case, so as to
                // avoid confusion in mapping between the two.

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("timestamp", m_transTimestamp);
                    xml.WriteAttributeString("type", m_transType);

                    xml.WriteElementString("merchantid", m_transMerchantName);
                    xml.WriteElementString("account", m_transAccountName);
                    xml.WriteElementString("orderid", m_transOrderID);

                    switch (m_transType)
                    {
                        case ("auth"):
                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("3ds-verifyenrolled"):
                        case ("3ds-verifysig"):
                        case ("tss"):

                            xml.WriteStartElement("amount");
                            xml.WriteAttributeString("currency", m_transCurrency);
                            xml.WriteString(m_transAmount.ToString());
                            xml.WriteEndElement();

                            // m_transCard.WriteXMLAuthSecure(xml);
                            m_transCard.WriteXMLAuthSecure(xml);
                            xml.WriteStartElement("mpi");
                            {
                                xml.WriteElementString("cavv", m_cavv);
                                xml.WriteElementString("xid", m_xid);
                                xml.WriteElementString("eci", m_eci);
                            }
                            xml.WriteEndElement();
                            xml.WriteElementString("pares", m_pares);
                            xml.WriteStartElement("autosettle");
                            xml.WriteAttributeString("flag", m_transAutoSettle.ToString());
                            // xml.WriteAttributeString("eci", m_eci.ToString());
                            xml.WriteEndElement();
                            break;
                    }

                    switch (m_transType)
                    {
                        case ("3ds-verifysig"):
                        case ("credit"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("pasref", m_transPASRef);
                            xml.WriteElementString("pares", m_transPASRef);
                            break;
                    }

                    switch (m_transType)
                    {

                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("authcode", m_transAuthCode);
                            break;
                    }

                    xml.WriteElementString("sha1hash", m_transSHA1Hash);

                    // if this is a transaction requiring an additional hash, include it here
                    SHA1 sha = new SHA1Managed();
                    switch (m_transType)
                    {
                        case ("credit"):
                            String refundHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_refundPassword)));
                            xml.WriteElementString("refundhash", refundHash);
                            break;
                        case ("rebate"):
                            String rebateHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_rebatePassword)));
                            xml.WriteElementString("refundhash", rebateHash);   // this is still sent as "refundhash", not "rebatehash"
                            break;
                    }

                    xml.WriteStartElement("comments");
                    {
                        int iComment = 1;	// this must start from 1, not 0.
                        foreach (String comment in m_transComments)
                        {
                            xml.WriteStartElement("comment");
                            xml.WriteAttributeString("id", iComment.ToString());
                            xml.WriteString(comment);
                            xml.WriteEndElement();

                            iComment++;
                        }
                    }
                    xml.WriteEndElement();

                    xml.WriteStartElement("tssinfo");
                    {
                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "billing");
                            xml.WriteElementString("code", m_transBillingAddressCode);
                            xml.WriteElementString("country", m_transBillingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "shipping");
                            xml.WriteElementString("code", m_transShippingAddressCode);
                            xml.WriteElementString("country", m_transShippingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteElementString("custnum", m_transCustomerNumber);
                            xml.WriteElementString("varref", m_transVariableReference);
                            xml.WriteElementString("prodid", m_transProductID);
                        }
                    }
                    xml.WriteEndElement();
                }

                //TODO: if you wish to send Realex any additional variables, include them here
                //xml.WriteElementString("MyInterestingVariable", m_myInterestingVariableName);

                xml.WriteEndElement();

                xml.Flush();
                xml.Close();

                return (strBuilder.ToString());
            }
            protected String ToXMLNewPayer()
            {

                generateTimestamp();	// timestamp the request as it's generated
                generateSHA1HashPayerNew();	// ... and ensure that we have a correct hash

                // NOTE: element variable names are named in the XML case, not in camel case, so as to
                // avoid confusion in mapping between the two.

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("timestamp", m_transTimestamp);
                    xml.WriteAttributeString("type", m_transType);

                    xml.WriteElementString("merchantid", m_transMerchantName);

                    xml.WriteElementString("orderid", m_transOrderID);

                    switch (m_transType)
                    {


                        case ("payer-new"):

                            xml.WriteStartElement("payer");
                            xml.WriteAttributeString("type", m_payertype);
                            xml.WriteAttributeString("ref", m_payerref);
                            xml.WriteEndElement();

                            m_transCard.WriteXMLPayerNew(xml);



                            // xml.WriteEndElement();
                            break;
                    }

                    switch (m_transType)
                    {
                        case ("credit"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("pasref", m_transPASRef);
                            break;
                    }

                    switch (m_transType)
                    {
                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("authcode", m_transAuthCode);
                            break;
                    }

                    xml.WriteElementString("sha1hash", m_transSHA1Hash);

                    // if this is a transaction requiring an additional hash, include it here
                    SHA1 sha = new SHA1Managed();
                    switch (m_transType)
                    {
                        case ("credit"):
                            String refundHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_refundPassword)));
                            xml.WriteElementString("refundhash", refundHash);
                            break;
                        case ("rebate"):
                            String rebateHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_rebatePassword)));
                            xml.WriteElementString("refundhash", rebateHash);   // this is still sent as "refundhash", not "rebatehash"
                            break;
                    }

                    xml.WriteStartElement("comments");
                    {
                        int iComment = 1;	// this must start from 1, not 0.
                        foreach (String comment in m_transComments)
                        {
                            xml.WriteStartElement("comment");
                            xml.WriteAttributeString("id", iComment.ToString());
                            xml.WriteString(comment);
                            xml.WriteEndElement();

                            iComment++;
                        }
                    }
                    //   xml.WriteEndElement();


                    {
                        xml.WriteStartElement("address");

                        xml.WriteElementString("city", m_city);
                        xml.WriteElementString("county", m_transBillingAddressCountry);
                        xml.WriteElementString("postcode", m_postcode);

                        xml.WriteEndElement();
                        xml.WriteElementString("email", m_email);
                    }



                    xml.WriteEndElement();
                }

                //TODO: if you wish to send Realex any additional variables, include them here
                //xml.WriteElementString("MyInterestingVariable", m_myInterestingVariableName);

                xml.WriteEndElement();

                xml.Flush();
                xml.Close();

                return (strBuilder.ToString());
            }
            protected String ToCardNewXML()
            {

                generateTimestamp();	// timestamp the request as it's generated
                generateSHA1HashCardNew();	// ... and ensure that we have a correct hash

                // NOTE: element variable names are named in the XML case, not in camel case, so as to
                // avoid confusion in mapping between the two.

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();

                XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                xml.WriteStartDocument();

                xml.WriteStartElement("request");
                {
                    xml.WriteAttributeString("timestamp", m_transTimestamp);
                    xml.WriteAttributeString("type", m_transType);

                    xml.WriteElementString("merchantid", m_transMerchantName);


                    xml.WriteElementString("account", m_transAccountName);
                    xml.WriteElementString("orderid", m_transOrderID);

                    switch (m_transType)
                    {
                        case ("auth"):
                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("3ds-verifyenrolled"):
                        case ("tss"):
                        case ("card-new"):
                        case ("payer-new"):
                            xml.WriteStartElement("amount");
                            xml.WriteAttributeString("currency", m_transCurrency);
                            xml.WriteString(m_transAmount.ToString());
                            //  xml.WriteAttributeString("payerref", m_payerref.ToString());
                            xml.WriteEndElement();

                            m_transCard.WriteCardNewXML(xml);

                            xml.WriteStartElement("autosettle");
                            xml.WriteAttributeString("flag", m_transAutoSettle.ToString());

                            xml.WriteEndElement();
                            break;
                    }

                    switch (m_transType)
                    {
                        case ("credit"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("pasref", m_transPASRef);
                            break;
                    }

                    switch (m_transType)
                    {
                        case ("credit"):
                        case ("offline"):
                        case ("rebate"):
                        case ("settle"):
                        case ("void"):
                            xml.WriteElementString("authcode", m_transAuthCode);
                            break;
                    }

                    xml.WriteElementString("sha1hash", m_transSHA1Hash);

                    // if this is a transaction requiring an additional hash, include it here
                    SHA1 sha = new SHA1Managed();
                    switch (m_transType)
                    {
                        case ("credit"):
                            String refundHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_refundPassword)));
                            xml.WriteElementString("refundhash", refundHash);
                            break;
                        case ("rebate"):
                            String rebateHash = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(m_rebatePassword)));
                            xml.WriteElementString("refundhash", rebateHash);   // this is still sent as "refundhash", not "rebatehash"
                            break;
                    }

                    xml.WriteStartElement("comments");
                    {
                        int iComment = 1;	// this must start from 1, not 0.
                        foreach (String comment in m_transComments)
                        {
                            xml.WriteStartElement("comment");
                            xml.WriteAttributeString("id", iComment.ToString());
                            xml.WriteString(comment);
                            xml.WriteEndElement();

                            iComment++;
                        }
                    }
                    xml.WriteEndElement();

                    xml.WriteStartElement("tssinfo");
                    {
                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "billing");
                            xml.WriteElementString("code", m_transBillingAddressCode);
                            xml.WriteElementString("country", m_transBillingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteStartElement("address");
                            xml.WriteAttributeString("type", "shipping");
                            xml.WriteElementString("code", m_transShippingAddressCode);
                            xml.WriteElementString("country", m_transShippingAddressCountry);
                            xml.WriteEndElement();
                        }

                        {
                            xml.WriteElementString("custnum", m_transCustomerNumber);
                            xml.WriteElementString("varref", m_transVariableReference);
                            xml.WriteElementString("prodid", m_transProductID);
                        }
                    }
                    xml.WriteEndElement();
                }

                //TODO: if you wish to send Realex any additional variables, include them here
                //xml.WriteElementString("MyInterestingVariable", m_myInterestingVariableName);

                xml.WriteEndElement();

                xml.Flush();
                xml.Close();

                return (strBuilder.ToString());
            }

        }

    }

}

