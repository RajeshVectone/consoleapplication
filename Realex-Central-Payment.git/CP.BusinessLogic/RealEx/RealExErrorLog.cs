﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CP.BusinessLogic.RealEx
{
    public class RealExErrorLog
    {
        public string REFERENCE_ID { get; set; }
        public int ERRORID { get; set; }
        public int RESULTID { get; set; }
        public string MESSAGE { get; set; }
        public string STAGE { get; set; }
        public int STEP { get; set; }
        public string EXTRAMESSAGE { get; set; }
        public string ECI { get; set; }
        public string ERRORCODE { get; set; }
        public string ERRORMSG { get; set; }
        public string STATUS { get; set; }

    }
}
