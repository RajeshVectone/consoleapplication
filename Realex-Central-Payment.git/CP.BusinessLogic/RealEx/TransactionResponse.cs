/*
Pay and Shop Limited (payandshop.com) - Licence Agreement.
� Copyright and zero Warranty Notice.


Merchants and their internet, call centre, and wireless application
developers (either in-house or externally appointed partners and
commercial organisations) may access payandshop.com technical
references, application programming interfaces (APIs) and other sample
code and software ("Programs") either free of charge from
www.payandshop.com or by emailing info@payandshop.com. 

payandshop.com provides the programs "as is" without any warranty of
any kind, either expressed or implied, including, but not limited to,
the implied warranties of merchantability and fitness for a particular
purpose. The entire risk as to the quality and performance of the
programs is with the merchant and/or the application development
company involved. Should the programs prove defective, the merchant
and/or the application development company assumes the cost of all
necessary servicing, repair or correction.

Copyright remains with payandshop.com, and as such any copyright
notices in the code are not to be removed. The software is provided as
sample code to assist internet, wireless and call center application
development companies integrate with the payandshop.com service.

Any Programs licensed by Pay and Shop to merchants or developers are
licensed on a non-exclusive basis solely for the purpose of availing
of the Pay and Shop payment solution service in accordance with the
written instructions of an authorised representative of Pay and Shop
Limited. Any other use is strictly prohibited.
*/

using System;
using System.Collections.Generic;
using System.Text;

using System.Xml;


namespace CP.BusinessLogic.RealexPayments
{

    namespace RealAuth
    {

        public class TransactionResponse
        {

            private int m_resultCode;
            private String m_resultMessage;
            private String m_resultAuthCode;
            private String m_resultPASRef;
            private String m_resultOrderID;
            private String m_xID;
            private String m_eci;
            private String m_PaRes;
            private String m_payerref;
            private String m_cardref;

            public int PayrefErrorCode { get; set; }
            public int CardRefErrorCode { get; set; }
           

            public String Cardref
            {
                get { return m_cardref; }
                set { m_cardref = value; }
            }

            public String Payerref
            {
                get { return m_payerref; }
                set { m_payerref = value; }
            }
            public String PaRes
            {
                get { return m_PaRes; }
                set { m_PaRes = value; }
            }

            public String Eci
            {
                get { return m_eci; }
                set { m_eci = value; }
            }
            private String m_message;

            public String Message
            {
                get { return m_message; }
                set { m_message = value; }
            }
            private String m_status;

            public String Status
            {
                get { return m_status; }
                set { m_status = value; }
            }
            private String m_result;

            public String Result
            {
                get { return m_result; }
                set { m_result = value; }
            }
            private String m_cavv;

            public String Cavv
            {
                get { return m_cavv; }
                set { m_cavv = value; }
            }


            //24-Aug-2019: Moorthy added for storing SRD value
            private String m_srd;

            public String SRD
            {
                get { return m_srd; }
                set { m_srd = value; }
            }

            public String XID
            {
                get { return m_xID; }
                set { m_xID = value; }
            }
            private int m_resultSuitabilityScore;
            private Dictionary<int, int> m_resultSuitabilityScoreCheck;
            private String m_pareq;

            public String Pareq
            {
                get { return m_pareq; }
                set { m_pareq = value; }
            }
            private String m_enrolled;

            public String Enrolled
            {
                get { return m_enrolled; }
                set { m_enrolled = value; }
            }
            private String m_url;

            public String Url
            {
                get { return m_url; }
                set { m_url = value; }
            }

            //TODO: if you have sent Realex additional variables and would like to retrieve them:
            //private String m_resultMyInterestingVariableName;

            public int ResultCode
            {
                get
                {
                    return (m_resultCode);
                }
            }

            public String ResultMessage
            {
                get
                {
                    return (m_resultMessage);
                }
            }

            public String ResultAuthCode
            {
                get
                {
                    return (m_resultAuthCode);
                }
            }

            public String ResultPASRef
            {
                get
                {
                    return (m_resultPASRef);
                }
            }

            public String ResultOrderID
            {
                get
                {
                    return (m_resultOrderID);
                }
            }

            public int ResultSuitabilityScore
            {
                get
                {
                    return (m_resultSuitabilityScore);
                }
            }

            public int ResultSuitabilityScoreCheck(int checkID)
            {
                return (m_resultSuitabilityScoreCheck[checkID]);
            }

            //TODO: if you have sent Realex additional variables and would like to retrieve them:
            /*
            public String ResultMyInterestingVariableName {
                get {
                    return (m_resultMyInterestingVariableName);
                }
                set {
                    m_resultMyInterestingVariableName = value;
                }
            }
            */

            public TransactionResponse(String responseXML)
            {

                m_resultSuitabilityScoreCheck = new Dictionary<int, int>();

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(responseXML);

                try
                {

                    // these *must* exist
                    m_resultCode = Convert.ToInt32(xml.GetElementsByTagName("result")[0].InnerText);
                    m_resultMessage = xml.GetElementsByTagName("message")[0].InnerText;

                    // these should exist, but don't throw exceptions if they don't.
                    XmlNode el;
                    el = xml.GetElementsByTagName("pasref")[0];
                    m_resultPASRef = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("authcode")[0];
                    m_resultAuthCode = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("orderid")[0];
                    m_resultOrderID = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("pareq")[0];
                    m_pareq = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("enrolled")[0];
                    m_enrolled = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("url")[0];
                    m_url = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("xid")[0];
                    m_xID = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("result")[0];
                    m_result = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("message")[0];
                    m_message = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("eci")[0];
                    m_eci = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("status")[0];
                    m_status = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("pares")[0];
                    m_PaRes = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("payerref")[0];
                    m_payerref = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("ref")[0];
                    m_cardref = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("cavv")[0];
                    m_cavv = (el != null) ? el.InnerText : "";

                    //24-Aug-2019 : Moorthy added for storing SRD value
                    try
                    {
                        el = xml.GetElementsByTagName("srd")[0];
                        m_srd = (el != null) ? el.InnerText : "";
                    }
                    catch { }

                    el = xml.GetElementsByTagName("tss")[0];
                    if (el != null)
                    {
                        foreach (XmlNode node in el.ChildNodes)
                        {
                            switch (node.Name)
                            {
                                case ("result"):
                                    m_resultSuitabilityScore = Convert.ToInt32(node.InnerText);
                                    break;
                                case ("check"):
                                    foreach (XmlAttribute attr in node.Attributes)
                                    {
                                        if (attr.Name == "id")
                                        {
                                            int inn = node.InnerText == "" ? 0 : Convert.ToInt32(node.InnerText);
                                            m_resultSuitabilityScoreCheck.Add(Convert.ToInt32(attr.InnerText), inn);
                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    //TODO: if you have sent Realex additional variables and would like to retrieve them:
                    /*
                    el = xml.GetElementsByTagName("MyInterestingVariable")[0];
                    if (el != null) {
                        m_resultMyInterestingVariableName = el.InnerText;
                    }
                    */
                }
                catch (NullReferenceException e)
                {
                    throw new TransactionFailedException("Error parsing XML response: mandatory fields not present. " + e.Message);
                }
            }

        }
    }
}
