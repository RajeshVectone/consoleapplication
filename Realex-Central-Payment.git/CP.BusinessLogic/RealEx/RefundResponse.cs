using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace CP.BusinessLogic.RealexPayments
{
    //Used to Refund the Amount
    namespace RealAuth
    {
        public class RefundResponse
        {
            private int m_resultCode;
            private String m_resultMessage;
            private String m_resultAuthCode;
            private String m_resultPASRef;
            private String m_resultOrderID;
            private String m_bank;
            private String m_country;
            private String m_countrycode;
            private String m_region;
            private String m_merchantId;
            private String m_account;
            private String m_cvnResult;
            private String m_avsPostCodeResponse;
            private String m_avsAddressResponse;
            private String m_batchId;
            private String m_timeTaken;
            private String m_authTimeTaken;
            private String m_sha1Hash;

            public String Sha1Hash
            {
                get { return m_sha1Hash; }
                set { m_sha1Hash = value; }
            }

            public String AuthTimeTaken
            {
                get { return m_authTimeTaken; }
                set { m_authTimeTaken = value; }
            }

            public String TimeTaken
            {
                get { return m_timeTaken; }
                set { m_timeTaken = value; }
            }

            public String BatchId
            {
                get { return m_batchId; }
                set { m_batchId = value; }
            }

            public String AVSAddressResponse
            {
                get { return m_avsAddressResponse; }
                set { m_avsAddressResponse = value; }
            }

            public String AVSPostCodeResponse
            {
                get { return m_avsPostCodeResponse; }
                set { m_avsPostCodeResponse = value; }
            }

            public String CVNResult
            {
                get { return m_cvnResult; }
                set { m_cvnResult = value; }
            }

            public String MerchantId
            {
                get { return m_merchantId; }
                set { m_merchantId = value; }
            }

            public String Account
            {
                get { return m_account; }
                set { m_account = value; }
            }

            public String Bank
            {
                get { return m_bank; }
                set { m_bank = value; }
            }

            public String Country
            {
                get { return m_country; }
                set { m_country = value; }
            }

            public String CountryCode
            {
                get { return m_countrycode; }
                set { m_countrycode = value; }
            }

            public String Region
            {
                get { return m_region; }
                set { m_region = value; }
            }
            
            private String m_message;

            public String Message
            {
                get { return m_message; }
                set { m_message = value; }
            }

            private String m_result;

            public String Result
            {
                get { return m_result; }
                set { m_result = value; }
            }

            public int ResultCode
            {
                get
                {
                    return (m_resultCode);
                }
            }

            public String ResultMessage
            {
                get
                {
                    return (m_resultMessage);
                }
            }

            public String ResultAuthCode
            {
                get
                {
                    return (m_resultAuthCode);
                }
            }

            public String ResultPASRef
            {
                get
                {
                    return (m_resultPASRef);
                }
            }

            public String ResultOrderID
            {
                get
                {
                    return (m_resultOrderID);
                }
            }

            public RefundResponse(String responseXML)
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(responseXML);

                try
                {
                    // these *must* exist
                    m_resultCode = Convert.ToInt32(xml.GetElementsByTagName("result")[0].InnerText);
                    m_resultMessage = xml.GetElementsByTagName("message")[0].InnerText;

                    // these should exist, but don't throw exceptions if they don't.
                    XmlNode el;

                    el = xml.GetElementsByTagName("merchantid")[0];
                    m_merchantId = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("account")[0];
                    m_account = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("orderid")[0];
                    m_resultOrderID = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("authcode")[0];
                    m_resultAuthCode = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("result")[0];
                    m_result = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("cvnresult")[0];
                    m_cvnResult = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("avspostcoderesponse")[0];
                    m_avsPostCodeResponse = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("avsaddressresponse")[0];
                    m_avsAddressResponse = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("batchid")[0];
                    m_batchId = (el != null) ? el.InnerText : ""; 

                    el = xml.GetElementsByTagName("message")[0];
                    m_message = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("pasref")[0];
                    m_resultPASRef = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("timetaken")[0];
                    m_timeTaken = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("authtimetaken")[0];
                    m_authTimeTaken = (el != null) ? el.InnerText : "";

                    el = xml.GetElementsByTagName("sha1hash")[0];
                    m_sha1Hash = (el != null) ? el.InnerText : "";
                    
                    el = xml.GetElementsByTagName("cardissuer")[0];
                    if (el != null)
                    {
                        foreach (XmlNode node in el.ChildNodes)
                        {
                            switch (node.Name)
                            {
                                case ("bank"):
                                    m_bank = node.InnerText;
                                    break;
                                case ("country"):
                                    m_country = node.InnerText;
                                    break;
                                case ("countrycode"):
                                    m_countrycode = node.InnerText;
                                    break;
                                case ("region"):
                                    m_region = node.InnerText;
                                    break;
                            }
                        }
                    }
                }
                catch (NullReferenceException e)
                {
                    throw new TransactionFailedException("Error parsing XML response: mandatory fields not present. " + e.Message);
                }
            }


        }
    }
}
