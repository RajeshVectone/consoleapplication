﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CP.Service.Client.DataType;
using System.Net;
using System.Reflection;

namespace CP.Service.Client
{
    public class Operation
    {
        /// <summary>
        /// Staging : http://192.168.1.28:8022/doc/paymentagent.aspx
        /// LIVE    : http://192.168.1.28:8020/doc/paymentagent.aspx
        /// </summary>
        private const string CPService_Enrollment = "http://localhost:4412/transaction.svc";

        /// <summary>
        /// See http://192.168.1.28:8020/doc/paymentagent.aspx
        /// </summary>
        private const string ClientAgent = "VMUK";

        /// <summary>
        /// Create subscription for a credit card.
        /// </summary>
        /// <param name="siteCode">Application site-code - http://192.168.1.28:8020/doc/sitecode.aspx</param>
        /// <param name="productCode">Product code</param>
        /// <param name="authorizedMerchantRef">Merchant reference number from previous successful capture</param>
        /// <param name="accountId">Customer ID</param>
        /// <param name="ccNo">Credit card number</param>
        /// <param name="expDate">Credit card expiry date (yyyyMM)</param>
        public static CybersourceTransaction Subscription_Create(string siteCode, string productCode, string authorizedMerchantRef, string accountId, string ccNo, string expDate)
        {
            CybersourceTransaction result = new CybersourceTransaction() { Decision = CybersourceDecision.ERROR, ErrCode = "-1", ErrMessage = "General error" };
            try
            {
                string urlTemplate = string.Format("/subscription/create?site-code={0}&product-code={1}&reference-id={2}&account-id={3}&cc-no={4}&exp-date={5}",
                    siteCode, productCode, authorizedMerchantRef, accountId, ccNo, expDate);
                HttpWebResponse res = DataAccess.CallService(CPService_Enrollment, urlTemplate, ClientAgent);
                return res.GetResponseStream().ReadAll().AssignTo<CybersourceTransaction>();
            }
            catch (Exception ex) { result.ErrMessage = (ex.InnerException ?? ex).Message; }
            return result;
        }

        /// <summary>
        /// Request authorization service for a payment from a subscribed credit card.
        /// </summary>
        /// <param name="siteCode">Application site-code - http://192.168.1.28:8020/doc/sitecode.aspx</param>
        /// <param name="productCode">Product code</param>
        /// <param name="serviceType">Service type - http://192.168.1.28:8020/doc/servicetype.aspx</param>
        /// <param name="paymentMode">Payment mode - http://192.168.1.28:8020/doc/paymentmode.aspx</param>
        /// <param name="accountId">Account/customer ID</param>
        /// <param name="ccNo">Credit card number</param>
        /// <param name="ccCvv">Credit card security code</param>
        /// <param name="amount">Payment amount</param>
        /// <param name="currency">Payment currency</param>
        /// <param name="capture">
        /// Try to settle the transaction
        /// 0 : FALSE, no payment settlement
        /// 1 : TRUE, try to have settlement on the payment
        /// </param>
        public static CybersourceTransaction Subscription_Authorize(string siteCode, string productCode, string serviceType, string paymentMode, string accountId, string ccNo, string ccCvv, string amount, string currency, string capture)
        {
            CybersourceTransaction result = new CybersourceTransaction() { Decision = CybersourceDecision.ERROR, ErrCode = "-1", ErrMessage = "General error" };
            try
            {
                string urlTemplate = string.Format("/subscription/authorize?site-code={0}&product-code={1}&payment-agent={2}&service-type={3}&payment-mode={4}&account-id={5}&cc-no={6}&cc-cvv={7}&amount={8}&currency={9}&capture={10}",
                    siteCode, productCode, ClientAgent, serviceType, paymentMode, accountId, ccNo, ccCvv, amount, currency, capture);
                HttpWebResponse res = DataAccess.CallService(CPService_Enrollment, urlTemplate, ClientAgent);
                return res.GetResponseStream().ReadAll().AssignTo<CybersourceTransaction>();
            }
            catch (Exception ex) { result.ErrMessage = (ex.InnerException ?? ex).Message; }
            return result;
        }

        /// <summary>
        /// Request capture service for an authorized payment from a subscribed credit card.
        /// </summary>
        /// <param name="siteCode">Application site-code - http://192.168.1.28:8020/doc/sitecode.aspx</param>
        /// <param name="productCode">Product code</param>
        /// <param name="serviceType">Service type - http://192.168.1.28:8020/doc/servicetype.aspx</param>
        /// <param name="accountId">Account/customer ID</param>
        /// <param name="authorizedMerchantRef">Merchant reference number from previous successful capture</param>
        public static CybersourceTransaction Subscription_Capture(string siteCode, string productCode, string serviceType, string accountId, string authorizeMerchantRef)
        {
            CybersourceTransaction result = new CybersourceTransaction() { Decision = CybersourceDecision.ERROR, ErrCode = "-1", ErrMessage = "General error" };
            try
            {
                string urlTemplate = string.Format("/subscription/capture?site-code={0}&product-code={1}&service-type={2}&account-id={3}&reference-id={4}",
                    siteCode, productCode, serviceType, accountId, authorizeMerchantRef);
                HttpWebResponse res = DataAccess.CallService(CPService_Enrollment, urlTemplate, ClientAgent);
                return res.GetResponseStream().ReadAll().AssignTo<CybersourceTransaction>();
            }
            catch (Exception ex) { result.ErrMessage = (ex.InnerException ?? ex).Message; }
            return result;
        }
    }
}
