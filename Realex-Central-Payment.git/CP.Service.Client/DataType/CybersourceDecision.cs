﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CP.Service.Client.DataType
{
    public class CybersourceDecision
    {
        public static string ERROR { get { return "ERROR"; } }
        public static string REJECT { get { return "REJECT"; } }
        public static string ACCEPT { get { return "ACCEPT"; } }
    }
}
