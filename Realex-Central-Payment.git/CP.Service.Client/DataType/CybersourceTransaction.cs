﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CP.Service.Client.DataType
{
    public class CybersourceTransaction
    {
        public string ErrCode { get; set; }
        public string ErrMessage { get; set; }
        public string Decision { get; set; }
        public string MerchantReferenceCode { get; set; }
        public string ReasonCode { get; set; }
        public string ProviderMessage { get; set; }
        public string RequestID { get; set; }
        public string RequestToken { get; set; }
        public string AcsURL { get; set; }
        public string PaReq { get; set; }
        public string ProofXML { get; set; }
        public string ProxyPAN { get; set; }
        public string UcafCollectionIndicator { get; set; }
        public string Xid { get; set; }
    }
}
