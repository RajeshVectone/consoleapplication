﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using CP.Service.Client.Attributes;

namespace CP.Service.Client
{
    public static class Helper
    {
        /// <summary>
        /// Convert an object into a specific data type
        /// </summary>
        public static T ToType<T>(this object value, T defaultValue) where T : struct
        {
            try
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch (Exception) { }
            return defaultValue;
        }

        /// <summary>
        /// Convert an object into a specific data type, exception thrown if the conversion failed
        /// </summary>
        public static T ToType<T>(this object value) where T : struct
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }

        /// <summary>
        /// Reads all data in the current stream
        /// </summary>
        public static Dictionary<string, string> ReadAll(this Stream data)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            try
            {
                StreamReader reader = new StreamReader(data);
                string line = string.Empty;
                while (!string.IsNullOrEmpty(line = reader.ReadLine()))
                {
                    string[] items = line.Split(":=".ToCharArray());
                    if (items.Length == 3)
                    {
                        result.Add("MerchantReferenceCode", items[0]);
                        result.Add("ErrCode", items[1]);
                        result.Add("ErrMessage", items[2]);
                    }
                    else if (items.Length == 2)
                        result.Add(items[0], items[1]);
                }
                reader.Close();
            }
            catch (Exception) { }
            return result;
        }

        /// <summary>
        /// Assigns all data into a specified class type properties
        /// </summary>
        public static T AssignTo<T>(this Dictionary<string, string> data) where T : new()
        {
            T toObject = new T();
            PropertyInfo[] toInfos = toObject.GetType().GetProperties();

            try
            {
                foreach (PropertyInfo item in toInfos)
                    if (data.ContainsKey(item.Name))
                        item.SetValue(toObject, data[item.Name], null);
            }
            catch (Exception) { }
            return toObject;
        }

        /// <summary>
        /// Gets the string value from current enum
        /// </summary>
        public static string GetStringValue(this Enum value)
        {
            Type type = value.GetType();
            FieldInfo fieldInfo = type.GetField(value.ToString());

            StringValueAttribute[] attribs = fieldInfo.GetCustomAttributes(
                typeof(StringValueAttribute), false) as StringValueAttribute[];

            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }
    }
}
