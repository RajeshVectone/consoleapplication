﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace CP.Service.Client
{
    public class DataAccess
    {
        /// <summary>
        /// Calls a web service with the specified parameters
        /// </summary>
        public static HttpWebResponse CallService(string serviceUrl, string uriTemplate, string userAgent, string method, string contentType, string contentBody)
        {
            HttpWebResponse result = null;
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] contentByte = encoding.GetBytes(contentBody.ToString());

                HttpWebRequest svc = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}",
                    serviceUrl,
                    uriTemplate));
                svc.Method = method;
                svc.UserAgent = userAgent;

                // Write content body
                if (!method.Equals(WebRequestMethods.Http.Get))
                {
                    svc.ContentType = contentType;
                    svc.ContentLength = contentByte.Length;
                    Stream svcContent = svc.GetRequestStream();
                    svcContent.Write(contentByte, 0, contentByte.Length);
                    svcContent.Close();
                }

                result = (HttpWebResponse)svc.GetResponse();
            }
            catch (Exception ex) { string s = ex.Message; }
            return result;
        }

        /// <summary>
        /// Calls a web service with the specified parameters in GET method
        /// </summary>
        public static HttpWebResponse CallService(string serviceUrl, string uriTemplate, string userAgent)
        {
            return CallService(serviceUrl, uriTemplate, userAgent, WebRequestMethods.Http.Get, string.Empty, string.Empty);
        }
    }
}
