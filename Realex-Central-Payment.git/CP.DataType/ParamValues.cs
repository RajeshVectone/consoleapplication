﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CP.DataType
{
    public class ParamValues
    {
        public static string[] PaymentAgentData = { "IVR", "WEB" };
        public static string[] ServiceTypeData = { "RES", "MVNO" };
        public static string[] PaymentModeData = { "DEF", "CARD", "SUB" };
        public static string[] FlagData = { "0", "1" }; // 0 = disable | 1 = enable
    }
}
