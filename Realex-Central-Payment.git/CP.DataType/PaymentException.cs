﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CP.DataType
{
    public class PaymentException : Exception
    {
        public int ErrorCode { get; private set; }

        public PaymentException(int errorCode, string message)
            : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
