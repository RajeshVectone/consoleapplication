﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CP.DataType
{
    public class PaymentData
    {
        /// <summary>
        /// Gets or sets the amount payment.
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// Gets or sets the payment currency.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets the authorization request ID.
        /// </summary>
        public string AuthRequestId { get; set; }

        /// <summary>
        /// Gets or sets the authorization request token.
        /// </summary>
        public string AuthRequestToken { get; set; }

        /// <summary>
        /// Initializes a new instance of IVRPaymentType.PaymentInfo class.
        /// </summary>
        public PaymentData() { }
    }
}
