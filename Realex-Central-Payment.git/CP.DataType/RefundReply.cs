﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CyberSource.Clients.SoapWebReference;
using System.Collections.Specialized;

namespace CP.DataType
{
    public class RefundReply
    {
        //public ReplyMessage ReplyMessage { get; set; }
        //public int GeneralErrorCode { get; set; }
        
        public string merchantid { get; set; }
        
        public string account { get; set; }

        public string orderid { get; set; }

        public string authcode { get; set; }

        public string Cardref { get; set; }

        public string PayerRef { get; set; }

        public int CardrefError { get; set; }

        public int PayerRefError { get; set; }

        /// <summary>
        /// Initializes a new instance of TransactionReply class.
        /// </summary>
        public RefundReply() { }

        /// <summary>
        /// Initializes a new instance of TransactionReply class from the specified information.
        /// </summary>
        //public RefundReply(ReplyMessage replyMessage, string description)
        //{
        //    ReplyMessage = replyMessage;
        //    Description = description;
        //}

        //public RefundReply(ReplyMessage replyMessage, string description, string _ResultPASRef, string _ResultAuthCode)
        //{
        //    ReplyMessage = replyMessage;
        //    Description = description;
        //    ResultPASRef = _ResultPASRef;
        //    ResultAuthCode = _ResultAuthCode;
        //}

        //public RefundReply(ReplyMessage replyMessage, string description, int _cardrefError, int _payerefError)
        //{
        //    ReplyMessage = replyMessage;
        //    Description = description;
        //    CardrefError = _cardrefError;
        //    PayerRefError = _payerefError;
        //}
    }
}
