﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CyberSource.Clients.SoapWebReference;
using System.Collections.Specialized;

namespace CP.DataType
{
    public class TransactionReply
    {
        public ReplyMessage ReplyMessage { get; set; }
        public string Description { get; set; }

        public int GeneralErrorCode { get; set; }
        public string ProviderMessage { get; set; }

        public string ResultPASRef { get; set; }

        public string ResultAuthCode { get; set; }

        public string Cardref { get; set; }

        public string PayerRef { get; set; }

        public int CardrefError { get; set; }

        public int PayerRefError { get; set; }

        //15-Jul-2019: Moorthy : Added for sending Three D Status in the response
        public string ThreeDStatus { get; set; }

        //24-Aug-2019 : Moorthy added for storing SRD value
        public string SRD { get; set; }

        /// <summary>
        /// Initializes a new instance of TransactionReply class.
        /// </summary>
        public TransactionReply() { }

        /// <summary>
        /// Initializes a new instance of TransactionReply class from the specified information.
        /// </summary>
        public TransactionReply(ReplyMessage replyMessage, string description)
        {
            ReplyMessage = replyMessage;
            Description = description;
        }

        //24-Aug-2019 : Moorthy added for storing SRD value
        public TransactionReply(ReplyMessage replyMessage, string description, string _ResultPASRef, string _ResultAuthCode, string _SRD)
        {
            ReplyMessage = replyMessage;
            Description = description;
            ResultPASRef = _ResultPASRef;
            ResultAuthCode = _ResultAuthCode;
            SRD = _SRD;
        }

        public TransactionReply(ReplyMessage replyMessage, string description,int _cardrefError,int _payerefError)
        {
            ReplyMessage = replyMessage;
            Description = description;
            CardrefError = _cardrefError;
            PayerRefError = _payerefError;

        }
    }
}
