﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace SMSByTxtFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "input.txt";
            DoProcess(fileName);
            //Console.WriteLine("Completed");
            //Console.ReadLine();
        }

        private static void DoProcess(string fileName)
        {
            try
            {
                string line;
                int iLine = 0;
                using (System.IO.StreamReader file = new System.IO.StreamReader(fileName))
                {
                    int iCount = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        iCount++;
                        Console.WriteLine("Line No : " + iLine++);
                        try
                        {
                            //if (Convert.ToString(ConfigurationManager.AppSettings["run"]) == "1")
                            //{
                            Thread.Sleep(100);
                            Console.WriteLine("Sms loop starts");
                            Console.WriteLine(line);

                            string url = "";
                            url = ConfigurationManager.AppSettings["mapurl" + iCount];
                            if (iCount == 9)
                                iCount = 0;

                            Console.WriteLine(url);

                            
                            string[] arrText = new string[3];
                            arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003A7030100530065006800720020006700650065006800720074006500720020004B0075006E00640065002C0020005700690072002000730065006E00640065006E002000490068006E0065006E002000650069006E00650020006E006500750065002000530049004D002D004B006100720074006500200075006D0020004900680072006500200061&tp-udhi=1&sequence-id=1";
                            arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003A70302006B007400750065006C006C00650020007A007500200061006B007400750061006C006900730069006500720065006E002E00200055006D00200049006800720065002000530049004D0020007A007500200061006B007400750061006C006900730069006500720065006E002C00200062006500730075006300680065006E002000620069&tp-udhi=1&sequence-id=2";
                            arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003A70303007400740065002000680074007400700073003A002F002F006200690074002E006C0079002F0032004D0072003900710036004F&tp-udhi=1&sequence-id=3";

                            for (int i = 0; i < arrText.Length; i++)
                            {
                                ASCIIEncoding encoding = new ASCIIEncoding();
                                string postData = string.Format(arrText[i], line.Trim());

                                Console.WriteLine(postData);

                                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
                                smsReq.Method = "POST";
                                smsReq.ContentType = "application/x-www-form-urlencoded";
                                smsReq.ServicePoint.Expect100Continue = false;
                                ASCIIEncoding enc = new ASCIIEncoding();
                                byte[] data = enc.GetBytes(postData);
                                smsReq.ContentLength = data.Length;

                                Stream newStream = smsReq.GetRequestStream();
                                newStream.Write(data, 0, data.Length);
                                newStream.Close();

                                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                                Stream resStream = smsRes.GetResponseStream();

                                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                                StreamReader readStream = new StreamReader(resStream, encode);
                                string sResponse = readStream.ReadToEnd().Trim();

                                Console.WriteLine("Sms ends");
                                Console.WriteLine(iCount);
                                Log(line + "--" + sResponse);
                            }
                            //}
                            //else
                            //    break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Log(ex.Message);
                        }
                    }
                    //file.Close();

                    //Used to move the file
                    //File.Move(fileName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Backups", DateTime.Now.ToString("MMddyyyyHHmmss")) + ".txt");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log("ERROR : " + ex.Message);
            }
        }

        public static void Log(string message)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + DateTime.Now.ToString("ddMMyyyy") + "_logs.txt");
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
