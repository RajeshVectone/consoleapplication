﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolTraningeApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mutable String Builder");
            Console.WriteLine("....................................");
            Console.WriteLine();
            StringBuilder sb = new StringBuilder("Very Good Morning");
            Console.WriteLine(sb.ToString());
            sb.Remove(0, 5);
            Console.WriteLine(sb.ToString());

            Console.WriteLine();

            Console.WriteLine("Immutable String");
            Console.WriteLine("....................................");
            Console.WriteLine();
            string s = "Very Good Morning";
            Console.WriteLine(s);
            //s.Substring(0, 5);
            s.Remove(0, 5);
            Console.WriteLine(s);


            Console.WriteLine("....................................");
            Console.WriteLine();
            //string news = s.Substring(0, 5);
            string news = s.Remove(0, 5);
            Console.WriteLine(news);
            Console.ReadLine();
        }
    }
}
