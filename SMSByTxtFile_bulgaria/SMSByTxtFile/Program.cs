﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace SMSByTxtFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "input.txt";
            DoProcess(fileName);
            //Console.WriteLine("Completed");
            //Console.ReadLine();
        }

        private static void DoProcess(string fileName)
        {
            try
            {
                string line;
                int iLine = 0;
                using (System.IO.StreamReader file = new System.IO.StreamReader(fileName))
                {
                    int iCount = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        iCount++;
                        Console.WriteLine("Line No : " + iLine++);
                        try
                        {
                            //if (Convert.ToString(ConfigurationManager.AppSettings["run"]) == "1")
                            //{
                            Thread.Sleep(100);
                            Console.WriteLine("Sms loop starts");
                            Console.WriteLine(line);

                            string url = "";
                            url = ConfigurationManager.AppSettings["mapurl" + iCount];
                            if (iCount == 9)
                                iCount = 0;

                            Console.WriteLine(url);

                            //string sText = "Bitte senden Sie ein Foto Ihres Gesichts, ein Foto Ihres Ausweises und Ihre Handynummer an support@vectonemobile.at und wir registrieren Ihre SIM-Karte für Sie.";
                            //string[] arrText = new string[6];
                            //arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500039C0301042304320430043604300435043C04380020041A043B04380435043D04420438002C0020043704300020043404300020043F0440043E0434044A043B0436043804420435002004340430002004380437043F043E043B043704320430044204350020043D04300448043804420435002004430441043B044304330438002C0020043C043E043B&tp-udhi=1&sequence-id=1";
                            //arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500039C0302044F002C002004400435043304380441044204400438044004300439044204350020041204300448043004420430002004210418041C0020043104350437043F043B04300442043D043E002C002004380437044204350433043B044F0439043A04380020043D0430044804350442043E0020043F04400438043B043E04360435043D04380435&tp-udhi=1&sequence-id=2";
                            //arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500039C0303002000680074007400700073003A002F002F006200690074002E006C0079002F0032006D00420076004700520045&tp-udhi=1&sequence-id=3";

                            //arrText[3] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003F20301041B043504410435043D0020043D043004470438043D002004340430002004410435002004400435043304380441044204400438044004300442043500200441043504330430002E0020041C043E043B044F002C002004380437043F044004300442043504420435002004120430044804350442043E002004410435043B04440438002C0020&tp-udhi=1&sequence-id=1";
                            //arrText[4] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003F20302041B041A00200441044A044100200441043D0438043C043A0430002C002004380020043C043E04310438043B0435043D0020043D043E043C043504400020043D043000200073007500700070006F0072007400400076006500630074006F006E0065006D006F00620069006C0065002E00610074002004380020043D04380435002004490435&tp-udhi=1&sequence-id=2";
                            //arrText[5] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003F2030300200440043504330438044104420440043804400430043C04350020041204300448043004420430002004210418041C002E&tp-udhi=1&sequence-id=3";

                            string[] arrText = new string[3];
                            arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003A30301042304320430043604300435043C04380020041A043B04380435043D04420438002C0020043704300020043404300020043F0440043E0434044A043B0436043804420435002004340430002004380437043F043E043B043704320430044204350020043D04300448043804420435002004430441043B044304330438002C0020043C043E043B&tp-udhi=1&sequence-id=1";
                            arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003A30302044F002C002004400435043304380441044204400438044004300439044204350020041204300448043004420430002004210418041C0020043104350437043F043B04300442043D043E002C002004380437044204350433043B044F0439043A04380020043D0430044804350442043E0020043F04400438043B043E04360435043D04380435&tp-udhi=1&sequence-id=2";
                            arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003A30303002000680074007400700073003A002F002F006200690074002E006C0079002F0032006E00570062004E0038004E&tp-udhi=1&sequence-id=3";


                            for (int i = 0; i < arrText.Length; i++)
                            {
                                ASCIIEncoding encoding = new ASCIIEncoding();
                                string postData = string.Format(arrText[i], line.Trim());

                                Console.WriteLine(postData);

                                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
                                smsReq.Method = "POST";
                                smsReq.ContentType = "application/x-www-form-urlencoded";
                                smsReq.ServicePoint.Expect100Continue = false;
                                ASCIIEncoding enc = new ASCIIEncoding();
                                byte[] data = enc.GetBytes(postData);
                                smsReq.ContentLength = data.Length;

                                Stream newStream = smsReq.GetRequestStream();
                                newStream.Write(data, 0, data.Length);
                                newStream.Close();

                                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                                Stream resStream = smsRes.GetResponseStream();

                                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                                StreamReader readStream = new StreamReader(resStream, encode);
                                string sResponse = readStream.ReadToEnd().Trim();

                                Console.WriteLine("Sms ends");
                                Console.WriteLine(i);
                                Log(line + "--" + sResponse);
                            }
                            //}
                            //else
                            //    break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Log(ex.Message);
                        }
                    }
                    //file.Close();

                    //Used to move the file
                    //File.Move(fileName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Backups", DateTime.Now.ToString("MMddyyyyHHmmss")) + ".txt");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log("ERROR : " + ex.Message);
            }
        }

        public static void Log(string message)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + DateTime.Now.ToString("ddMMyyyy") + "_logs.txt");
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
