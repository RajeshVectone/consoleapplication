﻿using System;
using System.Configuration;

namespace MNPUKSMSPortout
{
    public class ConfigAccessor
    {
        #region Xml related config

        public static string RequestHeaderXmlFile
        {
            get
            {
                return ConfigurationManager.AppSettings["RequestHeader"].ToString();
            }
        }

        public static string RequestContentXmlFile
        {
            get
            {
                return ConfigurationManager.AppSettings["RequestContent"].ToString();
            }
        }

        public static string ReportRequestHeaderXmlFile
        {
            get
            {
                return ConfigurationManager.AppSettings["ReportRequestHeader"].ToString();
            }
        }

        public static string NetworkOperatorCode
        {
            get
            {
                return ConfigurationManager.AppSettings["NetworkOperatorCode"].ToString();
            }
        }

        #endregion Xml related config

        #region Http related config, public

        public static string SyniverseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseUrl"].ToString();
            }
        }

        public static string SyniverseUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseUsername"].ToString();
            }
        }

        public static string SyniversePassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniversePassword"].ToString();
            }
        }

        public static string SyniverseCertificateFile
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseCertificateFile"].ToString();
            }
        }

        public static string SyniverseCertificatePassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseCertificatePassword"].ToString();
            }
        }

        public static string SyniverseXmlProblematicString
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseXmlProblematicString"].ToString().Replace("^", "\"");
            }
        }

        #endregion Http related config, public

        #region Database related config, public

        public static string NpUkConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["NP_UK"].ToString();
            }
        }

        public static string CrmConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["CRM"].ToString();
            }
        }

        public static string HlrConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["HLR"].ToString();
            }
        }

        public static string EspConnectionString
        {
            get
            {
                try
                {
                    return ConfigurationManager.ConnectionStrings["ESP"].ToString();
                }
                catch (Exception e)
                {
                    throw new Exception("Error getting connection string for ESP");
                }
            }
        }

        #endregion Database related config, public

        #region SMS/USSD related config

        public static string SmsServerUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsServerUrl"].ToString();
            }
        }

        public static string SmsOriginatorVectone
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["SmsOriginatorVectone"].ToString();
                }
                catch (Exception e)
                {
                    throw new Exception("Error retrieving config SmsOriginatorVectone");
                }
            }
        }

        public static string SmsOriginatorDelight
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["SmsOriginatorDelight"].ToString();
                }
                catch (Exception e)
                {
                    throw new Exception("Error retrieving config SmsOriginatorDelight");
                }
            }
        }

        public static string SmsOriginatorUk1
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["SmsOriginatorUk1"].ToString();
                }
                catch (Exception e)
                {
                    throw new Exception("Error retrieving config SmsOriginatorUk1");
                }
            }
        }

        public static string UssdUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["UssdUrl"].ToString();
            }
        }

        //public static string PortInSmsContent
        //{
        //    get
        //    {
        //        try
        //        {
        //            return ConfigurationManager.AppSettings["PortInSmsContent"].ToString();
        //        }
        //        catch
        //        {
        //            throw new Exception("PortInSmsContent config not found!");
        //        }
        //    }
        //}

        #endregion SMS/USSD related config

        #region Other config

        public static string HLRCheck
        {
            get
            {
                return ConfigurationManager.AppSettings["HLRCheck"].ToString();
            }
        }

        public static string MscVlrUrl
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["MscVlrUrl"].ToString();
                }
                catch
                {
                    throw new Exception("MscVlrUrl config not found!");
                }
            }
        }

        #endregion Other config

        #region Port Out/In Report related config

        //public static string PortOutReportDetailByPacMsisdn
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["PortOutReportDetailByPacMsisdn"].ToString();
        //    }
        //}

        //public static string PortOutReportDetailByPortDate
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["PortOutReportDetailByPortDate"].ToString();
        //    }
        //}

        public static string PortInReportDetailByPacMsisdn
        {
            get
            {
                return ConfigurationManager.AppSettings["PortInReportDetailByPacMsisdn"].ToString();
            }
        }

        public static string PortInReportDetailByPortDate
        {
            get
            {
                return ConfigurationManager.AppSettings["PortInReportDetailByPortDate"].ToString();
            }
        }

        #endregion Port Out/In Report related config
    }
}