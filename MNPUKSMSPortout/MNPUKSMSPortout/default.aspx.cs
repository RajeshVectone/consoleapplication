﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NLog;

namespace MNPUKSMSPortout
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string imsi, msisdn= string.Empty;
                if (Request.Form != null && Request.Form.Count > 0)
                {
                    imsi = Request.Form["imsi"];
                    msisdn = Request.Form["msisdn"];
                }
                else
                {
                    imsi = Request.QueryString["imsi"];
                    msisdn = Request.QueryString["msisdn"];
                }
                log.Info("MSISDN : {0} , ICCID : {1} ", msisdn, imsi);

                if (!String.IsNullOrEmpty(msisdn) && !String.IsNullOrEmpty(imsi))
                    DoCreatePac(msisdn, imsi);
                else
                {
                    try
                    {
                        Response.Clear();
                        Response.Write("Invalid MSISDN & ICCID!");
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                log.Error("Page_Load : " + ex.Message);
            }
        }

        private void DoCreatePac(string MSISDN, string ICCID)
        {
            try
            {
                string sessionId = PortingLogic.GetSessionId();
                CreateEntries reqObj = PortingLogic.CreateSingleCreateEntries(MSISDN, "", "ContactName", "ContactDetails", "Admin", "Submitted via Call Centre at " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "Vectone MSISDN: " + MSISDN);

                int counter = 0; // for retry
            Retry:
                string reqXml = XmlAdapter.CreateCookieXml(sessionId) + XmlAdapter.CreateCreateEntriesXml(reqObj);
                string resXml = XmlAdapter.send_xml(reqXml);

                #region If error
                if (resXml.Contains("error"))
                {
                    Error errObj = XmlAdapter.ReceiveErrorXml(resXml);

                    // if not login yet, login and retry, else display the error
                    if ((errObj.ErrorCode == 1601 || errObj.ErrorCode == 999901) && counter < 3)
                    {
                        counter++;

                        goto Retry;
                    }
                    else
                    {
                        string errMessage = errObj.Detail[1];
                        log.Error(String.Format("{0} (Syniverse error code {1})", errMessage, errObj.ErrorCode.ToString()));
                    }

                    PortingLogic.SaveTransactionLog("", reqObj.EntryCreationDetail[0].MsisdnClass.PrimaryMsisdn.Msisdn,
                        "(DNO)CreateEntry", errObj.ErrorCode, PortingLogic.NetworkOperatorCode,
                        null, null);
                }
                #endregion
                #region If success
                else
                {
                    CreateEntriesResp resObj = XmlAdapter.ReceiveCreateEntriesRespXml(resXml);

                    PortingLogic.SaveTransactionLog(resObj.PacDetail.Pac, reqObj.EntryCreationDetail[0].MsisdnClass.PrimaryMsisdn.Msisdn,
                        "(DNO)CreateEntry", 0, PortingLogic.NetworkOperatorCode,
                        null, null);


                    //PortingLogic.SaveSingleCreateEntriesResp(reqObj, resObj, lblValidatedData.Text); // save to database
                    PortingLogic.SaveSingleCreateEntriesResp(reqObj, resObj, ""); // save to database

                    try
                    {
                        string smsMessage = PortingLogic.GetSmsContent("CREATE_PAC");
                        int brand = PortingLogic.GetBrand(resObj.EntryResultList.EntryResult[0].Msisdn);
                        if (string.IsNullOrEmpty(smsMessage) || brand == PortingLogic.UnknownBrand)
                            throw new Exception("Sms content not found or brand is unknown");

                        smsMessage = smsMessage.Replace("{MSISDN}", resObj.EntryResultList.EntryResult[0].Msisdn);
                        smsMessage = smsMessage.Replace("{PAC}", resObj.PacDetail.Pac);
                        smsMessage = smsMessage.Replace("{EXP_DATE}", resObj.PacDetail.ExpiryDate.ToString("dd/MM/yy"));

                        PortingLogic.SendSms(brand, resObj.EntryResultList.EntryResult[0].Msisdn, smsMessage);
                    }
                    catch (Exception e)
                    {
                        log.Error("An error was encountered when sending SMS to the customer: " + e.Message);
                    }


                    //((CRM)Master).ShowInfo("Success: " + resXml); // change later
                    string successMsg =
                        "PAC created for MSISDN " + resObj.EntryResultList.EntryResult[0].Msisdn +
                        " : " + resObj.PacDetail.Pac +
                        ". This PAC will be valid until " + resObj.PacDetail.ExpiryDate.ToString("MMMM dd, yyyy") +
                        ". PAC info will also sent via SMS to customer's mobile phone.";

                    log.Info(successMsg);
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("DoCreatePac : " + ex.Message);
            }
        }
    }
}