﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Xml;
namespace MNPUKSMSPortout
{
    public class XmlAdapter
    {
        static Logger log = LogManager.GetLogger("Utility");

        private static string RequestHeaderXml, RequestContentXml, ReportRequestHeaderXml, CertificateFile, CertificatePassword;

        static XmlAdapter()
        {
            RequestHeaderXml = readFile(ConfigAccessor.RequestHeaderXmlFile);
            RequestContentXml = readFile(ConfigAccessor.RequestContentXmlFile);
            ReportRequestHeaderXml = readFile(ConfigAccessor.ReportRequestHeaderXmlFile);
            CertificateFile = ConfigAccessor.SyniverseCertificateFile;
            CertificatePassword = ConfigAccessor.SyniverseCertificatePassword;
            // use relative server path for web application, and use local path for non web application
        }

        #region Public & private method for creating Xml request and filling data

        /// <summary>
        /// Add request header and footer for input xml
        /// </summary>
        public static string AddRequestHeader(string xmlContent)
        {
            string RequestHeader = RequestHeaderXml;
            RequestHeader = RequestHeader.Replace("##Content##", xmlContent);
            return IndentXMLString(RequestHeader);
        }

        /// <summary>
        /// Add request header and footer for input xml
        /// </summary>
        public static string AddReportRequestHeader(string xmlContent)
        {
            string ReportRequestHeader = ReportRequestHeaderXml;
            ReportRequestHeader = ReportRequestHeader.Replace("##Content##", xmlContent);
            return IndentXMLString(ReportRequestHeader);
        }

        /// <summary>
        /// Get xml request format from file
        /// </summary>
        private static string GetXmlRequest(string requestName)
        {
            string fullXmlRequest = RequestContentXml;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(fullXmlRequest);

            string XmlRequestFormat = doc.GetElementsByTagName(requestName).Item(0).InnerXml;
            return XmlRequestFormat;
        }

        // helper method for indenting xml string for easier reading
        private static string IndentXMLString(string xml)
        {
            string outXml = string.Empty;
            MemoryStream ms = new MemoryStream();
            // Create a XMLTextWriter that will send its output to a memory stream (file)
            XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.Unicode);
            XmlDocument doc = new XmlDocument();

            try
            {
                // Load the unformatted XML text string into an instance
                // of the XML Document Object Model (DOM)
                doc.LoadXml(xml);

                // Set the formatting property of the XML Text Writer to indented
                // the text writer is where the indenting will be performed
                xtw.Formatting = Formatting.Indented;

                // write dom xml to the xmltextwriter
                doc.WriteContentTo(xtw);
                // Flush the contents of the text writer
                // to the memory stream, which is simply a memory file
                xtw.Flush();

                // set to start of the memory stream (file)
                ms.Seek(0, SeekOrigin.Begin);
                // create a reader to read the contents of
                // the memory stream (file)
                StreamReader sr = new StreamReader(ms);
                // return the formatted string to caller
                string returnStr = sr.ReadToEnd();
                log.Info("returnStr : " + returnStr);
                return returnStr;
            }
            catch (Exception ex)
            {
                log.Error("IndentXMLString : " + ex.Message);
                //MessageBox.Show(ex.ToString());
                return ex.ToString(); // return original input xml in case it can't be indented
            }
        }

        #endregion Public & private method for creating Xml request and filling data

        #region Private helper method for creating request and response

        private static string CreateSpOptionalDataXml(SpOptionalData optionalData)
        {
            string sodXml = "<sp_optional_data>";

            #region PrivateData, optional

            if (optionalData.PrivateData != null)
            {
                sodXml += "<private_data>";

                if (optionalData.PrivateData.AccNo != null)
                    sodXml += "<acc_no>" + optionalData.PrivateData.AccNo + "</acc_no>";

                if (optionalData.PrivateData.Ref != null)
                    sodXml += "<ref>" + optionalData.PrivateData.Ref + "</ref>";

                if (optionalData.PrivateData.AccNo != null)
                    sodXml += "<spid>" + optionalData.PrivateData.SpId + "</spid>";

                sodXml += "</private_data>";
            }

            #endregion PrivateData, optional

            #region ContactInfo, optional

            if (optionalData.ContactInfo != null)
            {
                sodXml += "<contact_info>";

                if (optionalData.ContactInfo.ContactName != null)
                    sodXml += "<contact_name>" + optionalData.ContactInfo.ContactName + "</contact_name>";

                if (optionalData.ContactInfo.ContactDetails != null)
                    sodXml += "<contact_details>" + optionalData.ContactInfo.ContactDetails + "</contact_details>";

                sodXml += "</contact_info>";
            }

            #endregion ContactInfo, optional

            sodXml += "</sp_optional_data>";
            return sodXml;
        }

        private static string CreateEntryCreationDetailXml(EntryCreationDetail entry)
        {
            string entryXml = string.Empty;

            entryXml += "<entry_creation_detail>";
            entryXml += "<msisdn_class>";

            #region PrimaryMsisdn, choose

            if (entry.MsisdnClass.PrimaryMsisdn != null)
            {
                entryXml += "<primary_msisdn><msisdn>";
                entryXml += entry.MsisdnClass.PrimaryMsisdn.Msisdn;
                entryXml += "</msisdn></primary_msisdn>";
            }

            #endregion PrimaryMsisdn, choose

            #region SecondaryMsisdn, choose

            if (entry.MsisdnClass.SecondaryMsisdn != null)
            {
                entryXml += "<secondary_msisdn><msisdn>";
                entryXml += entry.MsisdnClass.SecondaryMsisdn.Msisdn;
                entryXml += "</msisdn>";

                #region PrimaryMsisdn, optional

                if (entry.MsisdnClass.SecondaryMsisdn.PrimaryMsisdn != null)
                {
                    entryXml += "<primary_msisdn><msisdn>";
                    entryXml += entry.MsisdnClass.SecondaryMsisdn.PrimaryMsisdn.Msisdn;
                    entryXml += "</msisdn></primary_msisdn>";
                }

                #endregion PrimaryMsisdn, optional

                entryXml += "</secondary_msisdn>";
            }

            #endregion SecondaryMsisdn, choose

            entryXml += "</msisdn_class>";
            entryXml += "<no_code>" + entry.NoCode + "</no_code>";
            entryXml += "</entry_creation_detail>";

            return entryXml;
        }

        private static PacDetail ReceivePacDetailXml(string xmlString)
        {
            PacDetail obj = new PacDetail();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlString);

            obj.Pac = xml.GetElementsByTagName("pac")[0].InnerText;
            obj.EntryCount = Convert.ToInt32(xml.GetElementsByTagName("entry_count")[0].InnerText);
            obj.CreationDate = DateTime.ParseExact(xml.GetElementsByTagName("creation_date")[0].InnerText, "yyyyMMdd", null);
            obj.ExpiryDate = DateTime.ParseExact(xml.GetElementsByTagName("expiry_date")[0].InnerText, "yyyyMMdd", null);

            return obj;
        }

        private static SpData ReceiveSpDataXml(string xmlString)
        {
            SpData obj = new SpData();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlString);

            #region SpDetails

            obj.SpDetails = new SpDetails();
            obj.SpDetails.SpCode = xml.GetElementsByTagName("sp_code")[0].ToString();
            obj.SpDetails.SpName = xml.GetElementsByTagName("sp_name")[0].ToString();

            #endregion SpDetails

            #region SpOptionalData

            if (xml.GetElementsByTagName("sp_optional_data").Count > 0)
            {
                obj.SpOptionalData = new SpOptionalData();
                if (xml.GetElementsByTagName("private_data").Count > 0)
                {
                    obj.SpOptionalData.PrivateData = new PrivateData();
                    obj.SpOptionalData.PrivateData.AccNo = xml.GetElementsByTagName("acc_no")[0].InnerText;
                    obj.SpOptionalData.PrivateData.Ref = xml.GetElementsByTagName("ref")[0].InnerText;
                    obj.SpOptionalData.PrivateData.SpId = xml.GetElementsByTagName("spid")[0].InnerText;
                }

                if (xml.GetElementsByTagName("contact_info").Count > 0)
                {
                    obj.SpOptionalData.ContactInfo = new ContactInfo();

                    if (xml.GetElementsByTagName("contact_name").Count > 0)
                        obj.SpOptionalData.ContactInfo.ContactName = xml.GetElementsByTagName("contact_name")[0].ToString();

                    if (xml.GetElementsByTagName("contact_details").Count > 0)
                        obj.SpOptionalData.ContactInfo.ContactDetails = xml.GetElementsByTagName("contact_details")[0].ToString();
                }
            }

            #endregion SpOptionalData

            #region NoDetails

            obj.NoDetails = new NoDetails();
            obj.NoDetails.NoCode = xml.GetElementsByTagName("no_code")[0].InnerText;
            obj.NoDetails.NoName = xml.GetElementsByTagName("no_name")[0].InnerText;

            #endregion NoDetails

            return obj;
        }

        private static EntryResultList ReceiveEntryResultListXml(string xmlString)
        {
            EntryResultList obj = new EntryResultList();
            obj.EntryResult = new List<EntryResult>();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlString);

            XmlNodeList entryResultList = xml.GetElementsByTagName("entry_result");
            foreach (XmlNode entryResultXml in entryResultList)
            {
                XmlDocument xmlTemp = new XmlDocument();
                xmlTemp.LoadXml(entryResultXml.OuterXml);
                EntryResult entryObj = new EntryResult();

                entryObj.Msisdn = xmlTemp.GetElementsByTagName("msisdn")[0].InnerText;

                if (xmlTemp.GetElementsByTagName("error").Count > 0)
                    entryObj.Error = ReceiveErrorXml(xmlTemp.GetElementsByTagName("error")[0].InnerXml);

                obj.EntryResult.Add(entryObj);
            }

            return obj;
        }

        #endregion Private helper method for creating request and response

        #region Public create Request methods, input: object, output: xml string

        public static string CreateLogonXml() // assuming the username and password is in the config
        {
            try
            {
                string xml = GetXmlRequest("Logon");
                xml = xml.Replace("##Username##", ConfigAccessor.SyniverseUsername);
                xml = xml.Replace("##Password##", ConfigAccessor.SyniversePassword);
                return IndentXMLString(xml);
            }
            catch (Exception ex)
            {
                log.Error("CreateLogonXml : " + ex.Message);
                throw;
            }
        }

        public static string CreateLogonXml(Logon logonObj)
        {
            string xml = GetXmlRequest("Logon");
            xml = xml.Replace("##Username##", logonObj.Authenticator.Username);
            xml = xml.Replace("##Password##", logonObj.Authenticator.Password);
            return IndentXMLString(xml);
        }

        public static string CreateCookieXml(string sessionIdString)
        {
            string xml = GetXmlRequest("Cookie");
            xml = xml.Replace("##SessionId##", sessionIdString);
            return IndentXMLString(xml);
        }

        public static string CreateCreateEntriesXml(CreateEntries entriesObj)
        {
            string xml = GetXmlRequest("CreateEntries");
            xml = xml.Replace("##ReqCode##", entriesObj.ReqCode);

            #region SpOptionalData, optional

            string sodXml = string.Empty;

            if (entriesObj.SpOptionalData != null)
                sodXml = CreateSpOptionalDataXml(entriesObj.SpOptionalData);

            xml = xml.Replace("##SpOptionalData##", sodXml);

            #endregion SpOptionalData, optional

            #region EntryCreationDetail, list

            string entryXml = string.Empty;

            if (entriesObj.EntryCreationDetail != null)
                foreach (EntryCreationDetail entry in entriesObj.EntryCreationDetail)
                    entryXml += CreateEntryCreationDetailXml(entry);

            xml = xml.Replace("##EntryCreationDetail##", entryXml);

            #endregion EntryCreationDetail, list

            return IndentXMLString(xml);
        }

        public static string CreateRetrieveEntryXml(RetrieveEntry retrieveObj)
        {
            string xml = GetXmlRequest("RetrieveEntry");
            xml = xml.Replace("##ReqCode##", retrieveObj.ReqCode);

            #region EntryValidator

            string evXml = string.Empty;

            evXml += "<entry_validator>";

            if (retrieveObj.LooseEntryKey.EntryValidator.Pac != null)
                evXml += "<pac>" + retrieveObj.LooseEntryKey.EntryValidator.Pac + "</pac>";

            if (retrieveObj.LooseEntryKey.EntryValidator.AccNo != null)
                evXml += "<acc_no>" + retrieveObj.LooseEntryKey.EntryValidator.AccNo + "</acc_no>";

            if (retrieveObj.LooseEntryKey.EntryValidator.Ref != null)
                evXml += "<ref>" + retrieveObj.LooseEntryKey.EntryValidator.Ref + "</ref>";

            evXml += "</entry_validator>";

            xml = xml.Replace("##EntryValidator##", evXml);

            #endregion EntryValidator

            xml = xml.Replace("##Msisdn##", retrieveObj.LooseEntryKey.Msisdn);

            return IndentXMLString(xml);
        }

        public static string CreateCancelEntryXml(CancelEntry cancelObj)
        {
            string xml = GetXmlRequest("CancelEntry");
            xml = xml.Replace("##ReqCode##", cancelObj.ReqCode);
            xml = xml.Replace("##CancelAllInPac##", cancelObj.CancelAllInPac ? "True" : "False");
            xml = xml.Replace("##Pac##", cancelObj.EntryKey.Pac);
            xml = xml.Replace("##Msisdn##", cancelObj.EntryKey.Msisdn);
            return IndentXMLString(xml);
        }

        public static string CreateSubmitRequestXml(SubmitRequest submitObj)
        {
            string xml = GetXmlRequest("SubmitRequest");

            xml = xml.Replace("##ReqCode##", submitObj.ReqCode);
            xml = xml.Replace("##UseSuggestedDateOnConflict##", submitObj.UseSuggestedDateOnConflict ? "True" : "False");
            xml = xml.Replace("##Pac##", submitObj.EntryKey.Pac);
            xml = xml.Replace("##Msisdn##", submitObj.EntryKey.Msisdn);
            xml = xml.Replace("##NoCode##", submitObj.NoCode);
            xml = xml.Replace("##PortDate##", submitObj.PortDate.ToString("yyyyMMdd"));

            #region SpOptionalData, optional

            string sodXml = string.Empty;

            if (submitObj.SpOptionalData != null)
                sodXml = CreateSpOptionalDataXml(submitObj.SpOptionalData);

            xml = xml.Replace("##SpOptionalData##", sodXml);

            #endregion SpOptionalData, optional

            return IndentXMLString(xml);
        }

        public static string CreateReviseRequestXml(ReviseRequest reviseObj)
        {
            string xml = GetXmlRequest("ReviseRequest");

            xml = xml.Replace("##ReqCode##", reviseObj.ReqCode);
            xml = xml.Replace("##UseSuggestedDateOnConflict##", reviseObj.UseSuggestedDateOnConflict ? "True" : "False");
            xml = xml.Replace("##Pac##", reviseObj.EntryKey.Pac);
            xml = xml.Replace("##Msisdn##", reviseObj.EntryKey.Msisdn);

            #region NoCode and PortDate, optional

            string codeDateXml = string.Empty;

            if (reviseObj.NoCode != null)
                codeDateXml += "<no_code>" + reviseObj.NoCode + "</no_code>";

            if (reviseObj.PortDate != DateTime.MinValue)
                codeDateXml += "<port_date>" + reviseObj.PortDate.ToString("yyyyMMdd") + "</port_date>";

            xml = xml.Replace("##NoCodeAndPortDate##", codeDateXml);

            #endregion NoCode and PortDate, optional

            #region SpOptionalData, optional

            string sodXml = string.Empty;

            if (reviseObj.SpOptionalData != null)
                sodXml = CreateSpOptionalDataXml(reviseObj.SpOptionalData);

            xml = xml.Replace("##SpOptionalData##", sodXml);

            #endregion SpOptionalData, optional

            return IndentXMLString(xml);
        }

        public static string CreateReviseEntryXml(ReviseEntry reviseObj)
        {
            string xml = GetXmlRequest("ReviseEntry");

            xml = xml.Replace("##ReqCode##", reviseObj.ReqCode);
            xml = xml.Replace("##Pac##", reviseObj.EntryKey.Pac);
            xml = xml.Replace("##Msisdn##", reviseObj.EntryKey.Msisdn);

            #region NoCode, optional

            string codeXml = string.Empty;

            if (reviseObj.NoCode != null)
                codeXml += "<no_code>" + reviseObj.NoCode + "</no_code>";

            xml = xml.Replace("##NoCode##", codeXml);

            #endregion NoCode, optional

            #region ReviseMsisdnClass, optional

            string reviseMsisdnXml = string.Empty;

            if (reviseObj.ReviseMsisdnClass != null)
            {
                reviseMsisdnXml += "<revise_msisdn_class>";

                if (reviseObj.ReviseMsisdnClass.ReviseSecondaryMsisdn != null)
                {
                    if (reviseObj.ReviseMsisdnClass.ReviseSecondaryMsisdn.PrimaryMsisdn != null)
                    {
                        reviseMsisdnXml += "<revise_secondary_msisdn><primary_msisdn><msisdn>";
                        reviseMsisdnXml += reviseObj.ReviseMsisdnClass.ReviseSecondaryMsisdn.PrimaryMsisdn.Msisdn;
                        reviseMsisdnXml += "</msisdn></primary_msisdn></revise_secondary_msisdn>";
                    }
                    else // empty ReviseSecondaryMsisdn element
                        reviseMsisdnXml += "<revise_secondary_msisdn/>";
                }
                else // empty RevisePrimaryMsisdn element
                    reviseMsisdnXml += "<revise_primary_msisdn/>";

                reviseMsisdnXml += "</revise_msisdn_class>";
            }

            xml = xml.Replace("##ReviseMsisdnClass##", reviseMsisdnXml);

            #endregion ReviseMsisdnClass, optional

            return IndentXMLString(xml);
        }

        public static string CreateCancelRequestXml(CancelRequest cancelObj)
        {
            string xml = GetXmlRequest("CancelRequest");

            xml = xml.Replace("##ReqCode##", cancelObj.ReqCode);
            xml = xml.Replace("##CancelAllInPac##", cancelObj.CancelAllInPac ? "True" : "False");
            xml = xml.Replace("##Pac##", cancelObj.EntryKey.Pac);
            xml = xml.Replace("##Msisdn##", cancelObj.EntryKey.Msisdn);

            return IndentXMLString(xml);
        }

        public static string CreateAllSpDetailsXml(AllSpDetails detailObj)
        {
            string xml = GetXmlRequest("AllSpDetails");
            xml = xml.Replace("##ReqCode##", detailObj.ReqCode);
            return IndentXMLString(xml);
        }

        public static string CreatePasswordChangeXml(PasswordChange passwordObj)
        {
            string xml = GetXmlRequest("PasswordChange");
            xml = xml.Replace("##ReqCode##", passwordObj.ReqCode);
            xml = xml.Replace("##OldPassword##", passwordObj.OldPassword);
            xml = xml.Replace("##NewPassword##", passwordObj.NewPassword);
            return IndentXMLString(xml);
        }

        public static string CreateProgressSummaryXml(ProgressSummary progressObj)
        {
            string xml = GetXmlRequest("ProgressSummary");
            xml = xml.Replace("##ReqCode##", progressObj.ReqCode);
            return IndentXMLString(xml);
        }

        public static string CreateProgressReadRangeXml(ProgressReadRange progressObj)
        {
            string xml = GetXmlRequest("ProgressReadRange");
            xml = xml.Replace("##ReqCode##", progressObj.ReqCode);
            xml = xml.Replace("##LowId##", progressObj.LowId.ToString());
            xml = xml.Replace("##HighId##", progressObj.HighId.ToString());
            return IndentXMLString(xml);
        }

        public static string CreatePingXml(Ping pingObj)
        {
            string xml = GetXmlRequest("Ping");
            xml = xml.Replace("##ReqCode##", pingObj.ReqCode);
            return IndentXMLString(xml);
        }

        public static string CreateLogoffXml(Logoff logoffObj)
        {
            string xml = GetXmlRequest("Logoff");
            xml = xml.Replace("##ReqCode##", logoffObj.ReqCode);
            return IndentXMLString(xml);
        }

        #endregion Public create Request methods, input: object, output: xml string

        #region Public receive Response methods, input: xml string, output: object

        // problematic string, temp solution is replacing with empty string
        private static string problematicString = ConfigAccessor.SyniverseXmlProblematicString;

        public static Error ReceiveErrorXml(string errorXml)
        {
            errorXml = errorXml.Replace(problematicString, "");

            Error errorObj = new Error();
            errorObj.Detail = new List<string>();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(errorXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("error")[0];
            //errorObj.RespCode = mainNode.Attributes["resp_code"].InnerText;
            errorObj.ErrorCode = Convert.ToInt32(mainNode.Attributes["error_code"].InnerText);

            #endregion Attributes

            #region Elements

            errorObj.Text = xml.GetElementsByTagName("text")[0].InnerText;

            XmlNodeList detailNodes = xml.GetElementsByTagName("detail");
            Console.WriteLine(detailNodes.Count);
            foreach (XmlNode detailNode in detailNodes)
            {
                errorObj.Detail.Add(detailNode.InnerText);
            }

            #endregion Elements

            return errorObj;
        }

        public static LogonResp ReceiveLogonRespXml(string respXml)
        {
            try
            {
                LogonResp respObj = new LogonResp();

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(respXml);

                #region Elements

                respObj.Cookie = xml.GetElementsByTagName("cookie")[0].InnerText;

                if (xml.GetElementsByTagName("password_expiry_warning").Count > 0)
                {
                    PasswordExpiryWarning warning = new PasswordExpiryWarning();
                    warning.ExpiryDate = "The expiry date";
                    respObj.PasswordExpiryWarning = warning;
                }

                #endregion Elements

                return respObj;
            }
            catch (Exception ex)
            {
                log.Error("ReceiveLogonRespXml : " + ex.Message);
                throw;
            }
        }

        public static LogoffResp ReceiveLogoffRespXml(string respXml)
        {
            LogoffResp respObj = new LogoffResp();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("logoff_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            return respObj;
        }

        public static CreateEntriesResp ReceiveCreateEntriesRespXml(string respXml)
        {
            CreateEntriesResp respObj = new CreateEntriesResp();
            respObj.EntryResultList = new EntryResultList();
            respObj.EntryResultList.EntryResult = new List<EntryResult>();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("create_entries_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            #region Elements

            if (xml.GetElementsByTagName("error").Count > 0)
            {
                string errorXml = xml.GetElementsByTagName("error")[0].InnerXml;
                respObj.Error = ReceiveErrorXml(errorXml);
            }
            else
            {
                respObj.PacDetail = ReceivePacDetailXml(xml.GetElementsByTagName("pac_detail")[0].OuterXml);
            }

            respObj.EntryResultList =
                ReceiveEntryResultListXml(xml.GetElementsByTagName("entry_result_list")[0].OuterXml);

            #endregion Elements

            return respObj;
        }

        public static RetrieveEntryResp ReceiveRetrieveEntryRespXml(string respXml)
        {
            RetrieveEntryResp respObj = new RetrieveEntryResp();
            respObj.RetrieveEntryDetail = new RetrieveEntryDetail();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("retrieve_entry_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            #region Elements

            #region PacDetail

            respObj.RetrieveEntryDetail.PacDetail =
                ReceivePacDetailXml(xml.GetElementsByTagName("pac_detail")[0].OuterXml);

            #endregion PacDetail

            #region MsisdnClass

            respObj.RetrieveEntryDetail.MsisdnClass = new MsisdnClass();

            if (xml.GetElementsByTagName("secondary_msisdn").Count > 0)
            {
                SecondaryMsisdn sm = new SecondaryMsisdn();
                sm.Msisdn = xml.GetElementsByTagName("msisdn")[0].InnerText;

                if (xml.GetElementsByTagName("primary_msisdn").Count > 0)
                {
                    sm.PrimaryMsisdn = new PrimaryMsisdn();
                    sm.PrimaryMsisdn.Msisdn = xml.GetElementsByTagName("msisdn")[1].InnerText;
                }

                respObj.RetrieveEntryDetail.MsisdnClass.SecondaryMsisdn = sm;
            }
            else
            {
                PrimaryMsisdn pm = new PrimaryMsisdn();
                pm.Msisdn = xml.GetElementsByTagName("msisdn")[0].InnerText;
                respObj.RetrieveEntryDetail.MsisdnClass.PrimaryMsisdn = pm;
            }

            #endregion MsisdnClass

            #region DspData & RspData

            SpData temp1 = ReceiveSpDataXml(xml.GetElementsByTagName("dsp_data")[0].OuterXml);
            respObj.RetrieveEntryDetail.DspData = new DspData();
            respObj.RetrieveEntryDetail.DspData.NoDetails = temp1.NoDetails;
            respObj.RetrieveEntryDetail.DspData.SpDetails = temp1.SpDetails;
            respObj.RetrieveEntryDetail.DspData.SpOptionalData = temp1.SpOptionalData;

            SpData temp2 = ReceiveSpDataXml(xml.GetElementsByTagName("rsp_data")[0].OuterXml);
            respObj.RetrieveEntryDetail.RspData = new RspData();
            respObj.RetrieveEntryDetail.RspData.NoDetails = temp2.NoDetails;
            respObj.RetrieveEntryDetail.RspData.SpDetails = temp2.SpDetails;
            respObj.RetrieveEntryDetail.RspData.SpOptionalData = temp2.SpOptionalData;

            #endregion DspData & RspData

            #region PortDate

            if (xml.GetElementsByTagName("port_date").Count > 0)
                respObj.RetrieveEntryDetail.PortDate =
                    DateTime.ParseExact(xml.GetElementsByTagName("port_date")[0].InnerText, "yyyyMMdd", null);

            #endregion PortDate

            #region EntryStatus

            respObj.RetrieveEntryDetail.EntryStatus = new EntryStatus();
            string entryStatusString = xml.GetElementsByTagName("status")[0].InnerXml.Replace("<", "").Replace("/>", "");
            respObj.RetrieveEntryDetail.EntryStatus.Status =
                (Status)Enum.Parse(typeof(Status), entryStatusString, true);
            respObj.RetrieveEntryDetail.EntryStatus.IsTagged = xml.GetElementsByTagName("status")[0].InnerText == "True" ? true : false;

            #endregion EntryStatus

            #endregion Elements

            return respObj;
        }

        public static CancelEntryResp ReceiveCancelEntryRespXml(string respXml)
        {
            CancelEntryResp respObj = new CancelEntryResp();
            respObj.MultipleEntryResult = new MultipleEntryResult();
            respObj.MultipleEntryResult.ResultCount = new ResultCount();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("cancel_entry_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            if (mainNode.Attributes.Count > 1)
                respObj.CancelAllInPacRequested = mainNode.Attributes["cancel_all_in_pac_requested"].InnerText == "True" ? true : false;
            else
                respObj.CancelAllInPacRequested = false;

            #endregion Attributes

            #region Element

            respObj.Pac = xml.GetElementsByTagName("pac")[0].InnerText;

            respObj.MultipleEntryResult.EntryResultList =
                ReceiveEntryResultListXml(xml.GetElementsByTagName("entry_result_list")[0].OuterXml);

            respObj.MultipleEntryResult.ResultCount.GoodCount = Convert.ToInt32(xml.GetElementsByTagName("good_count")[0].InnerText);
            respObj.MultipleEntryResult.ResultCount.BadCount = Convert.ToInt32(xml.GetElementsByTagName("bad_count")[0].InnerText);

            #endregion Element

            return respObj;
        }

        public static SubmitRequestResp ReceiveSubmitRequestRespXml(string respXml)
        {
            SubmitRequestResp respObj = new SubmitRequestResp();
            respObj.EntryResult = new EntryResult();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("submit_request_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            if (mainNode.Attributes["port_date_warning"] != null)
                respObj.PortDateWarning = mainNode.Attributes["port_date_warning"].InnerText == "True" ? true : false;
            else
                respObj.PortDateWarning = false;

            #endregion Attributes

            #region Element

            respObj.EntryResult.Msisdn = xml.GetElementsByTagName("msisdn")[0].InnerText;

            if (xml.GetElementsByTagName("error").Count > 0)
                respObj.EntryResult.Error = ReceiveErrorXml(xml.GetElementsByTagName("error")[0].InnerText);

            respObj.PortDate =
                DateTime.ParseExact(xml.GetElementsByTagName("port_date")[0].InnerText, "yyyyMMdd", null);

            #endregion Element

            return respObj;
        }

        public static ReviseRequestResp ReceiveReviseRequestRespXml(string respXml)
        {
            ReviseRequestResp respObj = new ReviseRequestResp();
            SubmitRequestResp tempObj = ReceiveSubmitRequestRespXml(respXml.Replace("revise_request_resp", "submit_request_resp"));

            respObj.RespCode = tempObj.RespCode;
            respObj.PortDateWarning = tempObj.PortDateWarning;
            respObj.EntryResult = tempObj.EntryResult;
            respObj.PortDate = tempObj.PortDate;

            return respObj;
        }

        public static CancelRequestResp ReceiveCancelRequestRespXml(string respXml)
        {
            CancelRequestResp respObj = new CancelRequestResp();
            // call ReceiveCancelEntryRespXml as the function is very similar, just replace the element name
            CancelEntryResp tempObj = ReceiveCancelEntryRespXml(respXml.Replace("cancel_request_resp", "cancel_entry_resp"));

            respObj.RespCode = tempObj.RespCode;
            respObj.CancelAllInPacRequested = tempObj.CancelAllInPacRequested != null ? tempObj.CancelAllInPacRequested : false;
            respObj.Pac = tempObj.Pac;
            respObj.MultipleEntryResult = tempObj.MultipleEntryResult;

            return respObj;
        }

        public static ReviseEntryResp ReceiveReviseEntryRespXml(string respXml)
        {
            ReviseEntryResp respObj = new ReviseEntryResp();
            respObj.EntryResult = new EntryResult();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("revise_entry_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            #region Element

            respObj.EntryResult.Msisdn = xml.GetElementsByTagName("msisdn")[0].InnerText;

            if (xml.GetElementsByTagName("error").Count > 0)
                respObj.EntryResult.Error = ReceiveErrorXml(xml.GetElementsByTagName("error")[0].InnerText);

            #endregion Element

            return respObj;
        }

        public static PasswordChangeResp ReceivePasswordChangeRespXml(string respXml)
        {
            PasswordChangeResp respObj = new PasswordChangeResp();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("password_change_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            #region Elements

            if (xml.GetElementsByTagName("error").Count > 0)
            {
                string errorXml = xml.GetElementsByTagName("error")[0].InnerXml;
                respObj.Error = ReceiveErrorXml(errorXml);
            }

            #endregion Elements

            return respObj;
        }

        public static AllSpDetailsResp ReceiveAllSpDetailsRespXml(string respXml)
        {
            AllSpDetailsResp respObj = new AllSpDetailsResp();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("all_sp_details_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            #region Elements

            for (int count = 0; count < xml.GetElementsByTagName("sp_details").Count; count++)
            {
                if (count == 0)
                    respObj.SpDetails = new List<SpDetails>();

                SpDetails spObj = new SpDetails();
                spObj.SpCode = xml.GetElementsByTagName("sp_code")[count].InnerText;
                spObj.SpName = xml.GetElementsByTagName("sp_name")[count].InnerText;

                respObj.SpDetails.Add(spObj);
            }

            #endregion Elements

            return respObj;
        }

        public static PingResp ReceivePingRespXml(string respXml)
        {
            PingResp respObj = new PingResp();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("ping_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            return respObj;
        }

        public static ProgressSummaryResp ReceiveProgressSummaryRespXml(string respXml)
        {
            ProgressSummaryResp respObj = new ProgressSummaryResp();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("progress_summary_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            #region Element

            respObj.LowId = Convert.ToInt32(xml.GetElementsByTagName("low_id")[0].InnerText);
            respObj.HighId = Convert.ToInt32(xml.GetElementsByTagName("high_id")[0].InnerText);

            #endregion Element

            return respObj;
        }

        public static ProgressReadRangeResp ReceiveProgressReadRangeRespXml(string respXml)
        {
            ProgressReadRangeResp respObj = new ProgressReadRangeResp();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("progress_read_range_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            #region Element

            for (int count = 0; count < xml.GetElementsByTagName("progress_read_detail").Count; count++)
            {
                if (count == 0)
                    respObj.ProgressReadDetail = new List<ProgressReadDetail>();

                ProgressReadDetail prdObj = new ProgressReadDetail();

                prdObj.ProgressId = Convert.ToInt32(xml.GetElementsByTagName("progress_id")[count].InnerText);
                prdObj.Pac = xml.GetElementsByTagName("pac")[count].InnerText;
                prdObj.Msisdn = xml.GetElementsByTagName("msisdn")[count].InnerText;
                prdObj.UpdateDate =
                    DateTime.ParseExact(xml.GetElementsByTagName("update_date")[count].InnerText, "yyyyMMdd", null);
                prdObj.UpdateTime =
                    DateTime.ParseExact(xml.GetElementsByTagName("update_time")[count].InnerText, "HH:mm:ss", null);
                prdObj.OldStatus =
                    (Status)Enum.Parse(typeof(Status), xml.GetElementsByTagName("old_status")[count].InnerText, true);
                prdObj.NewStatus =
                    (Status)Enum.Parse(typeof(Status), xml.GetElementsByTagName("new_status")[count].InnerText, true);
                prdObj.DspCode = xml.GetElementsByTagName("dsp_code")[count].InnerText;
                prdObj.RspCode = xml.GetElementsByTagName("rsp_code")[count].InnerText;

                respObj.ProgressReadDetail.Add(prdObj);
            }

            #endregion Element

            return respObj;
        }

        #endregion Public receive Response methods, input: xml string, output: object

        #region Public Reporting methods

        public static string CreateReportXml(Report reportObj)
        {
            string xml = GetXmlRequest("Report");
            xml = xml.Replace("##ReqCode##", reportObj.ReqCode);
            xml = xml.Replace("##Name##", reportObj.Name);

            #region OptionalXml, optional

            string optionalXml = string.Empty;

            if (reportObj.Pac != null)
                optionalXml += "<pac>" + reportObj.Pac + "</pac>";

            if (reportObj.Msisdn != null)
                optionalXml += "<msisdn>" + reportObj.Msisdn + "</msisdn>";

            if (reportObj.PacExpirySelector != null)
            {
                optionalXml += "<pac_expiry_selector><equality_test>";
                optionalXml += reportObj.PacExpirySelector.EqualityTest.ToString();
                optionalXml += "</equality_test><days>";
                optionalXml += reportObj.PacExpirySelector.Days.ToString();
                optionalXml += "</days></pac_expiry_selector>";
            }

            if (reportObj.StatusSelector != StatusSelector.Null)
                optionalXml += "<status_selector><" + reportObj.StatusSelector.ToString() + " /></status_selector>";

            if (reportObj.BulkTransferSelector != BulkTransferSelector.Null)
                optionalXml += "<bulk_transfer_selector><" + reportObj.BulkTransferSelector.ToString() + " /></bulk_transfer_selector>";

            if (reportObj.MsisdnQuantitySelector != null)
            {
                optionalXml += "<msisdn_quantity_selector><equality_test><";
                optionalXml += reportObj.MsisdnQuantitySelector.EqualityTest.ToString();
                optionalXml += " /></equality_test><entry_count>";
                optionalXml += reportObj.MsisdnQuantitySelector.EntryCount.ToString();
                optionalXml += "</entry_count></msisdn_quantity_selector>";
            }

            if (reportObj.TransferTypeSelector != TransferTypeSelector.Null)
                optionalXml += "<transfer_type_selector><" + reportObj.TransferTypeSelector + " /></transfer_type_selector>";

            if (reportObj.PortDueSelector != null)
            {
                optionalXml += "<port_due_selector><equality_test><";
                optionalXml += reportObj.PortDueSelector.EqualityTest.ToString();
                optionalXml += " /></equality_test><days>";
                optionalXml += reportObj.PortDueSelector.Days.ToString();
                optionalXml += "</days></port_due_selector>";
            }

            if (reportObj.PacAgeSelector != null)
            {
                optionalXml += "<pac_age_selector><equality_test><";
                optionalXml += reportObj.PacAgeSelector.EqualityTest.ToString();
                optionalXml += " /></equality_test><days>";
                optionalXml += reportObj.PacAgeSelector.Days.ToString();
                optionalXml += "</days></pac_age_selector>";
            }

            if (reportObj.TaggedSelector != TaggedSelector.Null)
                optionalXml += "<tagged_selector><" + reportObj.TaggedSelector.ToString() + " /></tagged_selector>";

            xml = xml.Replace("##OptionalParameter##", optionalXml);

            #endregion OptionalXml, optional

            #region OptionalDsp, optional

            string dspXml = string.Empty;

            if (reportObj.DspAccNo != null)
                dspXml += "<dsp_acc_no>" + reportObj.DspAccNo + "</dsp_acc_no>";

            if (reportObj.DspRef != null)
                dspXml += "<dsp_ref>" + reportObj.DspRef + "</dsp_ref>";

            if (reportObj.DspContactName != null)
                dspXml += "<dsp_contact_name>" + reportObj.DspContactName + "</dsp_contact_name>";

            if (reportObj.DspContactDetails != null)
                dspXml += "<dsp_contact_details>" + reportObj.DspContactDetails + "</dsp_contact_details>";

            if (reportObj.DspSelector != null)
            {
                dspXml += "<dsp_selector>";

                if (reportObj.DspSelector.SpCode != null)
                    dspXml += "<sp_code>" + reportObj.DspSelector.SpCode + "</sp_code>";
                else
                    dspXml += "<All/ >";
                dspXml += "</dsp_selector>";
            }

            if (reportObj.DnoSelector != null)
            {
                dspXml += "<dno_selector>";

                if (reportObj.DnoSelector.NoCode != null)
                    dspXml += "<no_code>" + reportObj.DnoSelector.NoCode + "</no_code>";
                else
                    dspXml += "<All/ >";
                dspXml += "</dno_selector>";
            }

            xml = xml.Replace("##OptionalDspData##", dspXml);

            #endregion OptionalDsp, optional

            #region OptionalRsp, optional

            string rspXml = string.Empty;

            if (reportObj.RspAccNo != null)
                rspXml += "<rsp_acc_no>" + reportObj.RspAccNo + "</rsp_acc_no>";

            if (reportObj.RspRef != null)
                rspXml += "<rsp_ref>" + reportObj.RspRef + "</rsp_ref>";

            if (reportObj.RspContactName != null)
                rspXml += "<rsp_contact_name>" + reportObj.RspContactName + "</rsp_contact_name>";

            if (reportObj.RspContactDetails != null)
                rspXml += "<rsp_contact_details>" + reportObj.RspContactDetails + "</rsp_contact_details>";

            if (reportObj.RspSelector != null)
            {
                rspXml += "<rsp_selector>";

                if (reportObj.RspSelector.SpCode != null)
                    rspXml += "<sp_code>" + reportObj.RspSelector.SpCode + "</sp_code>";
                else
                    rspXml += "<All/ >";
                rspXml += "</rsp_selector>";
            }

            if (reportObj.RnoSelector != null)
            {
                rspXml += "<rno_selector>";

                if (reportObj.RnoSelector.NoCode != null)
                    rspXml += "<no_code>" + reportObj.RnoSelector.NoCode + "</no_code>";
                else
                    rspXml += "<All/ >";
                rspXml += "</rno_selector>";
            }

            xml = xml.Replace("##OptionalRspData##", rspXml);

            #endregion OptionalRsp, optional

            return IndentXMLString(xml);
        }

        public static ReportResp ReceiveReportRespXml(string respXml)
        {
            ReportResp respObj = new ReportResp();
            respObj.ReportRecord = new List<ReportRecord>();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes

            XmlNode mainNode = xml.GetElementsByTagName("report_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            #endregion Attributes

            #region Element

            for (int count = 0; count < xml.GetElementsByTagName("report_record").Count; count++)
            {
                string reportRecordXml = xml.GetElementsByTagName("report_record")[count].OuterXml;

                ReportRecord rcObj = ReceiveReportRecordXml(reportRecordXml);
                respObj.ReportRecord.Add(rcObj);
            }

            #endregion Element

            return respObj;
        }

        private static ReportRecord ReceiveReportRecordXml(string respXml)
        {
            ReportRecord respObj = new ReportRecord();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Optional Data

            if (xml.GetElementsByTagName("pac").Count > 0)
                respObj.Pac = xml.GetElementsByTagName("pac")[0].InnerText;

            if (xml.GetElementsByTagName("msisdn").Count > 0)
                respObj.Msisdn = xml.GetElementsByTagName("msisdn")[0].InnerText;

            if (xml.GetElementsByTagName("pac_expiry_days").Count > 0)
                respObj.PacExpiryDays = Convert.ToInt32(xml.GetElementsByTagName("pac_expiry_days")[0].InnerText);

            if (xml.GetElementsByTagName("status").Count > 0)
                respObj.Status = (Status)Enum.Parse(typeof(Status), xml.GetElementsByTagName("status")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            if (xml.GetElementsByTagName("is_bulk").Count > 0)
                respObj.IsBulk = (IsBulk)Enum.Parse(typeof(IsBulk), xml.GetElementsByTagName("is_bulk")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            if (xml.GetElementsByTagName("entry_count").Count > 0)
                respObj.EntryCount = Convert.ToInt32(xml.GetElementsByTagName("entry_count")[0].InnerText);

            if (xml.GetElementsByTagName("primary_msisdn").Count > 0)
            {
                respObj.PrimaryMsisdn = new PrimaryMsisdn();
                respObj.PrimaryMsisdn.Msisdn = xml.GetElementsByTagName("primary_msisdn")[0].InnerText;
            }

            if (xml.GetElementsByTagName("msisdn_type").Count > 0)
                respObj.MsisdnType = (MsisdnType)Enum.Parse(typeof(MsisdnType), xml.GetElementsByTagName("msisdn_type")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            if (xml.GetElementsByTagName("transfer_type").Count > 0)
                respObj.TransferType = (TransferType)Enum.Parse(typeof(TransferType), xml.GetElementsByTagName("transfer_type")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            if (xml.GetElementsByTagName("port_days").Count > 0)
                respObj.PortDays = xml.GetElementsByTagName("port_days")[0].InnerText;

            if (xml.GetElementsByTagName("port_date").Count > 0 && xml.GetElementsByTagName("port_date")[0].InnerText.Length > 0)
                respObj.PortDate = DateTime.ParseExact(xml.GetElementsByTagName("port_date")[0].InnerText, "yyyyMMdd", null);
            // note: might be error and need to use DateTime.ParseExact

            if (xml.GetElementsByTagName("pac_age").Count > 0)
                respObj.PacAge = Convert.ToInt32(xml.GetElementsByTagName("pac_age")[0].InnerText);

            if (xml.GetElementsByTagName("is_tagged").Count > 0)
                respObj.IsTagged = (TaggedSelector)Enum.Parse(typeof(TaggedSelector), xml.GetElementsByTagName("is_tagged")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            #endregion Optional Data

            #region Optional Dsp Data

            if (xml.GetElementsByTagName("dsp_contact_name").Count > 0)
                respObj.DspContactName = xml.GetElementsByTagName("dsp_contact_name")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_contact_details").Count > 0)
                respObj.DspContactDetails = xml.GetElementsByTagName("dsp_contact_details")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_code").Count > 0)
                respObj.DspCode = xml.GetElementsByTagName("dsp_code")[0].InnerText;

            if (xml.GetElementsByTagName("dno_code").Count > 0)
                respObj.DnoCode = xml.GetElementsByTagName("dno_code")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_acc_no").Count > 0)
                respObj.DspAccNo = xml.GetElementsByTagName("dsp_acc_no")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_ref").Count > 0)
                respObj.DspRef = xml.GetElementsByTagName("dsp_ref")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_name").Count > 0)
                respObj.DspName = xml.GetElementsByTagName("dsp_name")[0].InnerText;

            if (xml.GetElementsByTagName("dno_name").Count > 0)
                respObj.DnoName = xml.GetElementsByTagName("dno_name")[0].InnerText;

            #endregion Optional Dsp Data

            #region Optional Rsp Data

            if (xml.GetElementsByTagName("rsp_contact_name").Count > 0)
                respObj.RspContactName = xml.GetElementsByTagName("rsp_contact_name")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_contact_details").Count > 0)
                respObj.RspContactDetails = xml.GetElementsByTagName("rsp_contact_details")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_code").Count > 0)
                respObj.RspCode = xml.GetElementsByTagName("rsp_code")[0].InnerText;

            if (xml.GetElementsByTagName("rno_code").Count > 0)
                respObj.RnoCode = xml.GetElementsByTagName("rno_code")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_acc_no").Count > 0)
                respObj.RspAccNo = xml.GetElementsByTagName("rsp_acc_no")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_ref").Count > 0)
                respObj.RspRef = xml.GetElementsByTagName("rsp_ref")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_name").Count > 0)
                respObj.RspName = xml.GetElementsByTagName("rsp_name")[0].InnerText;

            if (xml.GetElementsByTagName("rno_name").Count > 0)
                respObj.RnoName = xml.GetElementsByTagName("rno_name")[0].InnerText;

            #endregion Optional Rsp Data

            return respObj;
        }

        #endregion Public Reporting methods

        #region Methods for sending Http post and read response

        /*****************************************************
         *   send_request is a method that writes the contents of
         *   a specified URL to the web
         *****************************************************/

        private static void send_request(HttpWebRequest request, string data)
        {
            byte[] bytes = null;

            request.CookieContainer = new CookieContainer();

            if (HttpContext.Current != null)
            {
                CookieCollection Cookies = (CookieCollection)HttpContext.Current.Session.Contents["cookies"];

                if (Cookies != null && Cookies.Count > 0)
                    request.CookieContainer.Add(Cookies);
            }

            // Get the data that is being posted(or sent) to the server
            bytes = System.Text.Encoding.ASCII.GetBytes(data);
            request.ContentLength = bytes.Length;

            // 1. Get an output stream from the request object
            Stream outputStream = request.GetRequestStream();

            // 2. Post the data out to the stream
            outputStream.Write(bytes, 0, bytes.Length);

            // 3. Close the output stream and send the data out to the web server
            outputStream.Close();
        }

        /*****************************************************
         *   read_response is a method that retrieves the contents of
         *   a specified URL in response to a request
         *****************************************************/

        private static string read_response(HttpWebRequest request)
        {
            // 1. Get the Web Response Object from the request
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // *** Save the cookies on the session
            if (HttpContext.Current != null && response.Cookies.Count > 0)
            {
                HttpContext.Current.Session.Contents["cookies"] = response.Cookies;
            }

            // 2. Get the Stream Object from the response
            Stream responseStream = response.GetResponseStream();

            // 3. Create a stream reader and associate it with the stream object
            StreamReader reader = new StreamReader(responseStream);

            // 4. read the entire stream
            return reader.ReadToEnd();
        }

        /*****************************************************
         *   send_xml is a method that forces a POST
         *   of data to a specified URL
         *
         *   A HTTP POST is a combination of a write to the Web Server
         *   and an immediate read from the Web server
         *****************************************************/

        public static string send_xml(string xml)
        {
            try
            {
                // Add request header for all outgoing message
                xml = XmlAdapter.AddRequestHeader(xml);
                log.Info("-------------------------------------send_receive_xml------------------------------------------");
                log.Info("xml : " + xml);
                return send_receive_xml(xml);
            }
            catch (Exception ex)
            {
                log.Error("send_xml : " + ex.Message);
                throw;
            }
        }

        public static string send_report_xml(string xml)
        {
            // Add request header for all outgoing message
            xml = XmlAdapter.AddReportRequestHeader(xml);
            return send_receive_xml(xml);
        }

        private static string send_receive_xml(string xml)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += delegate (
                       Object sender1,
                       X509Certificate certificate,
                       X509Chain chain,
                       SslPolicyErrors sslPolicyErrors
                     )
                    {
                        return true;
                    };

                //02-Jul-2019 : Moorthy : Added for the Realex TLS 1.2 Update
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // Create the Web Request Object
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigAccessor.SyniverseUrl);
                request.ProtocolVersion = HttpVersion.Version10;

                // Specify that you want to POST data
                request.Method = "POST";
                request.ContentType = "text/xml";
                
                request.ClientCertificates.Add(new X509Certificate2(CertificateFile, CertificatePassword));

                try
                {
                    send_request(request, xml);
                }
                catch (System.Net.WebException exc)
                {
                    log.Error("send_request : " + exc.Message);
                    return "*** Error while connecting: " + exc.Message;
                }

                string resp;

                try
                {
                    resp = read_response(request);
                    resp = resp.Replace(problematicString, "");  // temp solution
                }
                catch (System.Net.WebException exc)
                {
                    WebResponse response = exc.Response;
                    if (response != null)
                    {
                        Stream responseStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(responseStream);

                        return "*** Error while reading response: " + exc.Message + "\r\n\r\n" + reader.ReadToEnd();
                    }
                    else
                    {
                        return "*** Error while reading response: " + exc.Message + "\r\n\r\n" + "Got null Reponse";
                    }
                }

                return resp;
            }
            catch (Exception ex)
            {
                log.Error("send_receive_xml : " + ex.Message);
                log.Error("send_receive_xml : " + ex.StackTrace);
                throw;
            }
        }

        private class CertPolicy : ICertificatePolicy
        {
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

        #endregion Methods for sending Http post and read response

        #region Other helper methods, private

        private static string readFile(string filePath)
        {
            // use relative server path for web application, and use local path for non web application
            //if (HttpContext.Current != null)
            //    filePath = HttpContext.Current.Server.MapPath(filePath);

            if (!File.Exists(filePath))
                return string.Empty;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);
                string text = streamReader.ReadToEnd();
                streamReader.Close();

                return text;
            }
            catch
            {
                return string.Empty;
            }
        }

        #endregion Other helper methods, private
    }
}