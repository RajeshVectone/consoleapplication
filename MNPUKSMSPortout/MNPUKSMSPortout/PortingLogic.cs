﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using NLog;

namespace MNPUKSMSPortout
{
    public class PortingLogic
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        public static string NetworkOperatorCode;

        private static string NpUkConnectionString, CrmConnectionString, HlrdbConnectionString,// connection strings
            Sp_Get_Req_Code, // used in each request
            Sp_Create_Entry, Sp_Create_Entry_2, Sp_Insert_Entry_Result, // for create entries
            Sp_Insert_Transaction_Log, // for recording transaction log
            SmsServerUrl, 
            //Sp_Get_Sms_Content, // variables for sending SMS
            //Sp_Brand_Differentiation, //distinguishing brands
            HlrCheck; // will define if MSISDN will be checked to HLR or not
        #endregion

        #region PortingLogic
        static PortingLogic()
        {
            NetworkOperatorCode = ConfigAccessor.NetworkOperatorCode;
            NpUkConnectionString = ConfigAccessor.NpUkConnectionString;
            CrmConnectionString = ConfigAccessor.CrmConnectionString;
            HlrdbConnectionString = ConfigAccessor.HlrConnectionString;

            Sp_Get_Req_Code = "SP_GET_REF_CODE";
            Sp_Create_Entry = "SP_CREATE_ENTRY";
            Sp_Create_Entry_2 = "SP_CREATE_ENTRY_2"; // With validation details
            Sp_Insert_Entry_Result = "SP_INSERT_ENTRY_RESULT";
            Sp_Insert_Transaction_Log = "SP_INSERT_TRANSACTION_LOG";

            SmsServerUrl = ConfigAccessor.SmsServerUrl;
            HlrCheck = ConfigAccessor.HLRCheck;

            //TODO : Need to Remove
            //Sp_Get_Sms_Content = "sp_get_sms_content";
            //Sp_Brand_Differentiation = "brand_differentiation";
        } 
        #endregion

        #region CreateSingleCreateEntries
        public static CreateEntries CreateSingleCreateEntries(string primaryMsisdn, string secondaryMsisdn, string contactName, string contactDetails, string accNo, string additionalRef, string spId)
        {
            CreateEntries reqObj = new CreateEntries();
            reqObj.ReqCode = NetworkOperatorCode + GetReqCode();

            reqObj.SpOptionalData = new SpOptionalData();
            reqObj.SpOptionalData.ContactInfo = new ContactInfo();
            reqObj.SpOptionalData.ContactInfo.ContactName = contactName;
            reqObj.SpOptionalData.ContactInfo.ContactDetails = contactDetails;

            reqObj.SpOptionalData.PrivateData = new PrivateData();
            reqObj.SpOptionalData.PrivateData.AccNo = accNo;
            reqObj.SpOptionalData.PrivateData.Ref = additionalRef;
            reqObj.SpOptionalData.PrivateData.SpId = spId;

            reqObj.EntryCreationDetail = new List<EntryCreationDetail>();

            EntryCreationDetail ecd1 = new EntryCreationDetail();
            ecd1.NoCode = NetworkOperatorCode;
            ecd1.MsisdnClass = new MsisdnClass();
            ecd1.MsisdnClass.PrimaryMsisdn = new PrimaryMsisdn();
            ecd1.MsisdnClass.PrimaryMsisdn.Msisdn = primaryMsisdn;
            reqObj.EntryCreationDetail.Add(ecd1);

            if (secondaryMsisdn != null && secondaryMsisdn.Trim().Length > 0)
            {
                EntryCreationDetail ecd2 = new EntryCreationDetail();
                ecd2.NoCode = NetworkOperatorCode;
                ecd2.MsisdnClass = new MsisdnClass();
                ecd2.MsisdnClass.SecondaryMsisdn = new SecondaryMsisdn();
                ecd2.MsisdnClass.SecondaryMsisdn.Msisdn = secondaryMsisdn;
                ecd2.MsisdnClass.PrimaryMsisdn = new PrimaryMsisdn();
                ecd2.MsisdnClass.PrimaryMsisdn.Msisdn = primaryMsisdn;
                reqObj.EntryCreationDetail.Add(ecd2);
            }

            return reqObj;
        }
        #endregion

        #region GetSessionId
        public static string GetSessionId()
        {
            try
            {
                log.Info("----------Start GetSessionId-----------");
                string reqXml = XmlAdapter.CreateLogonXml();
                log.Info(reqXml);
                string resXml = XmlAdapter.send_xml(reqXml);
                log.Info(resXml);
                string sessionId = XmlAdapter.ReceiveLogonRespXml(resXml).Cookie;
                log.Info(sessionId);
                log.Info("----------End GetSessionId-----------");
                return sessionId;
            }
            catch (Exception ex)
            {
                log.Error("GetSessionId : " + ex.Message);
                throw;
            }
        }
        #endregion

        #region GetReqCode
        private static string GetReqCode()
        {
            SQLDataAccessLayer DAL = new SQLDataAccessLayer(NpUkConnectionString);
            SqlCommand cmd = new SqlCommand();

            DAL.AddParamToSQLCmd(cmd, "@ref_code", SqlDbType.VarChar, 10, ParameterDirection.Output, "");
            DAL.SetCommandType(cmd, CommandType.StoredProcedure, Sp_Get_Req_Code);

            DAL.ExecuteNonQuery(cmd);

            string reqCode = cmd.Parameters["@ref_code"].Value.ToString();
            return reqCode;
        }
        #endregion

        #region SaveTransactionLog
        public static void SaveTransactionLog(string Pac, string Msisdn, string TransactionName, int ErrorCode, string Dno, string Rno, string Ono)
        {
            SQLDataAccessLayer DAL = new SQLDataAccessLayer(NpUkConnectionString);
            SqlCommand cmd = new SqlCommand();

            DAL.AddParamToSQLCmd(cmd, "@Pac", SqlDbType.VarChar, 32, ParameterDirection.Input, Pac);
            DAL.AddParamToSQLCmd(cmd, "@Msisdn", SqlDbType.VarChar, 24, ParameterDirection.Input, Msisdn);
            DAL.AddParamToSQLCmd(cmd, "@TransactionName", SqlDbType.VarChar, 20, ParameterDirection.Input, TransactionName);
            DAL.AddParamToSQLCmd(cmd, "@ErrorCode", SqlDbType.Int, 0, ParameterDirection.Input, ErrorCode);
            DAL.AddParamToSQLCmd(cmd, "@Dno", SqlDbType.Char, 2, ParameterDirection.Input, Dno);
            DAL.AddParamToSQLCmd(cmd, "@Rno", SqlDbType.Char, 2, ParameterDirection.Input, Rno);
            DAL.AddParamToSQLCmd(cmd, "@Ono", SqlDbType.Char, 2, ParameterDirection.Input, Ono);

            DAL.SetCommandType(cmd, CommandType.StoredProcedure, Sp_Insert_Transaction_Log);
            DAL.ExecuteNonQuery(cmd);
        }
        #endregion

        #region SaveSingleCreateEntriesResp
        public static void SaveSingleCreateEntriesResp(CreateEntries reqObj, CreateEntriesResp respObj)
        {
            SQLDataAccessLayer DAL = new SQLDataAccessLayer(NpUkConnectionString);
            SqlCommand cmd = new SqlCommand();

            DAL.AddParamToSQLCmd(cmd, "@pac", SqlDbType.VarChar, 32, ParameterDirection.Input, respObj.PacDetail.Pac);
            DAL.AddParamToSQLCmd(cmd, "@entry_count", SqlDbType.Int, 0, ParameterDirection.Input, respObj.PacDetail.EntryCount);
            DAL.AddParamToSQLCmd(cmd, "@creation_date", SqlDbType.DateTime, 0, ParameterDirection.Input, respObj.PacDetail.CreationDate);
            DAL.AddParamToSQLCmd(cmd, "@expiry_date", SqlDbType.DateTime, 0, ParameterDirection.Input, respObj.PacDetail.ExpiryDate);
            DAL.AddParamToSQLCmd(cmd, "@msisdn", SqlDbType.VarChar, 24, ParameterDirection.Input, respObj.EntryResultList.EntryResult[0].Msisdn);
            DAL.AddParamToSQLCmd(cmd, "@error_code", SqlDbType.Int, 0, ParameterDirection.Input, 0);

            // optional parameter
            string accNo, refString, spId, contactName, contactDetails;
            accNo = refString = spId = contactName = contactDetails = string.Empty;

            #region Conditional checking

            if (reqObj.SpOptionalData != null)
            {
                if (reqObj.SpOptionalData.PrivateData != null)
                {
                    accNo = reqObj.SpOptionalData.PrivateData.AccNo;
                    refString = reqObj.SpOptionalData.PrivateData.Ref;
                    spId = reqObj.SpOptionalData.PrivateData.SpId;
                }

                if (reqObj.SpOptionalData.ContactInfo != null)
                {
                    contactName = reqObj.SpOptionalData.ContactInfo.ContactName;
                    contactDetails = reqObj.SpOptionalData.ContactInfo.ContactDetails;
                }
            }

            #endregion Conditional checking

            DAL.AddParamToSQLCmd(cmd, "@acc_no", SqlDbType.VarChar, 50, ParameterDirection.Input, accNo == string.Empty ? null : accNo);
            DAL.AddParamToSQLCmd(cmd, "@ref", SqlDbType.VarChar, 120, ParameterDirection.Input, refString == string.Empty ? null : refString);
            DAL.AddParamToSQLCmd(cmd, "@sp_id", SqlDbType.VarChar, 120, ParameterDirection.Input, spId == string.Empty ? null : spId);
            DAL.AddParamToSQLCmd(cmd, "@contact_name", SqlDbType.VarChar, 64, ParameterDirection.Input, contactName == string.Empty ? null : contactName);
            DAL.AddParamToSQLCmd(cmd, "@contact_details", SqlDbType.VarChar, 256, ParameterDirection.Input, contactDetails == string.Empty ? null : contactDetails);
            DAL.AddParamToSQLCmd(cmd, "@entry_id", SqlDbType.Int, 0, ParameterDirection.Output, 0);
            DAL.SetCommandType(cmd, CommandType.StoredProcedure, Sp_Create_Entry);
            DAL.ExecuteNonQuery(cmd);

            int entryId = Convert.ToInt32(cmd.Parameters["@entry_id"].Value);

            foreach (EntryCreationDetail ecdObj in reqObj.EntryCreationDetail)
            {
                DAL = new SQLDataAccessLayer(NpUkConnectionString);
                cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@entry_id", SqlDbType.Int, 0, ParameterDirection.Input, entryId);
                DAL.AddParamToSQLCmd(cmd, "@primary_msisdn", SqlDbType.VarChar, 24, ParameterDirection.Input, ecdObj.MsisdnClass.PrimaryMsisdn.Msisdn);
                DAL.AddParamToSQLCmd(cmd, "@secondary_msisdn", SqlDbType.VarChar, 24, ParameterDirection.Input,
                    ecdObj.MsisdnClass.SecondaryMsisdn != null ? ecdObj.MsisdnClass.SecondaryMsisdn.Msisdn : ecdObj.MsisdnClass.PrimaryMsisdn.Msisdn);
                DAL.AddParamToSQLCmd(cmd, "@error_code", SqlDbType.Int, 0, ParameterDirection.Input, 0);
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, Sp_Insert_Entry_Result);
                DAL.ExecuteNonQuery(cmd);
            }
        }
        #endregion

        #region SaveSingleCreateEntriesResp
        public static void SaveSingleCreateEntriesResp(CreateEntries reqObj, CreateEntriesResp respObj, string validationData)
        {
            SQLDataAccessLayer DAL = new SQLDataAccessLayer(NpUkConnectionString);
            SqlCommand cmd = new SqlCommand();

            DAL.AddParamToSQLCmd(cmd, "@pac", SqlDbType.VarChar, 32, ParameterDirection.Input, respObj.PacDetail.Pac);
            DAL.AddParamToSQLCmd(cmd, "@entry_count", SqlDbType.Int, 0, ParameterDirection.Input, respObj.PacDetail.EntryCount);
            DAL.AddParamToSQLCmd(cmd, "@creation_date", SqlDbType.DateTime, 0, ParameterDirection.Input, respObj.PacDetail.CreationDate);
            DAL.AddParamToSQLCmd(cmd, "@expiry_date", SqlDbType.DateTime, 0, ParameterDirection.Input, respObj.PacDetail.ExpiryDate);
            DAL.AddParamToSQLCmd(cmd, "@msisdn", SqlDbType.VarChar, 24, ParameterDirection.Input, respObj.EntryResultList.EntryResult[0].Msisdn);
            DAL.AddParamToSQLCmd(cmd, "@error_code", SqlDbType.Int, 0, ParameterDirection.Input, 0);

            // optional parameter
            string accNo, refString, spId, contactName, contactDetails;
            accNo = refString = spId = contactName = contactDetails = string.Empty;

            #region Conditional checking

            if (reqObj.SpOptionalData != null)
            {
                if (reqObj.SpOptionalData.PrivateData != null)
                {
                    accNo = reqObj.SpOptionalData.PrivateData.AccNo;
                    refString = reqObj.SpOptionalData.PrivateData.Ref;
                    spId = reqObj.SpOptionalData.PrivateData.SpId;
                }

                if (reqObj.SpOptionalData.ContactInfo != null)
                {
                    contactName = reqObj.SpOptionalData.ContactInfo.ContactName;
                    contactDetails = reqObj.SpOptionalData.ContactInfo.ContactDetails;
                }
            }

            #endregion Conditional checking

            DAL.AddParamToSQLCmd(cmd, "@acc_no", SqlDbType.VarChar, 50, ParameterDirection.Input, accNo == string.Empty ? null : accNo);
            DAL.AddParamToSQLCmd(cmd, "@ref", SqlDbType.VarChar, 120, ParameterDirection.Input, refString == string.Empty ? null : refString);
            DAL.AddParamToSQLCmd(cmd, "@sp_id", SqlDbType.VarChar, 120, ParameterDirection.Input, spId == string.Empty ? null : spId);
            DAL.AddParamToSQLCmd(cmd, "@contact_name", SqlDbType.VarChar, 64, ParameterDirection.Input, contactName == string.Empty ? null : contactName);
            DAL.AddParamToSQLCmd(cmd, "@contact_details", SqlDbType.VarChar, 256, ParameterDirection.Input, contactDetails == string.Empty ? null : contactDetails);
            DAL.AddParamToSQLCmd(cmd, "@validation_details", SqlDbType.VarChar, 250, ParameterDirection.Input, validationData == string.Empty ? null : validationData);
            DAL.AddParamToSQLCmd(cmd, "@entry_id", SqlDbType.Int, 0, ParameterDirection.Output, 0);
            DAL.SetCommandType(cmd, CommandType.StoredProcedure, Sp_Create_Entry_2); // with validation details
            DAL.ExecuteNonQuery(cmd);

            int entryId = Convert.ToInt32(cmd.Parameters["@entry_id"].Value);

            foreach (EntryCreationDetail ecdObj in reqObj.EntryCreationDetail)
            {
                DAL = new SQLDataAccessLayer(NpUkConnectionString);
                cmd = new SqlCommand();
                DAL.AddParamToSQLCmd(cmd, "@entry_id", SqlDbType.Int, 0, ParameterDirection.Input, entryId);
                DAL.AddParamToSQLCmd(cmd, "@primary_msisdn", SqlDbType.VarChar, 24, ParameterDirection.Input, ecdObj.MsisdnClass.PrimaryMsisdn.Msisdn);
                DAL.AddParamToSQLCmd(cmd, "@secondary_msisdn", SqlDbType.VarChar, 24, ParameterDirection.Input,
                    ecdObj.MsisdnClass.SecondaryMsisdn != null ? ecdObj.MsisdnClass.SecondaryMsisdn.Msisdn : ecdObj.MsisdnClass.PrimaryMsisdn.Msisdn);
                DAL.AddParamToSQLCmd(cmd, "@error_code", SqlDbType.Int, 0, ParameterDirection.Input, 0);
                DAL.SetCommandType(cmd, CommandType.StoredProcedure, Sp_Insert_Entry_Result);
                DAL.ExecuteNonQuery(cmd);
            }
        }
        #endregion

        #region Sending SMS operations
        //TODO : Need to Remove
        //public static string GetSmsContent(string contentKey)
        //{
        //    try
        //    {
        //        SQLDataAccessLayer DAL = new SQLDataAccessLayer(NpUkConnectionString);
        //        SqlCommand cmd = new SqlCommand();

        //        DAL.AddParamToSQLCmd(cmd, "@content_key", SqlDbType.VarChar, 25, ParameterDirection.Input, contentKey);
        //        DAL.AddParamToSQLCmd(cmd, "@content", SqlDbType.VarChar, 500, ParameterDirection.Output, "");
        //        DAL.SetCommandType(cmd, CommandType.StoredProcedure, Sp_Get_Sms_Content);

        //        DAL.ExecuteNonQuery(cmd);
        //        return cmd.Parameters["@content"].Value.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("Error getting SMS content: " + e.Message);
        //    }
        //}

        //public static readonly int VectoneBrand = 1;
        //public static readonly int DelightBrand = 2;
        //public static readonly int Uk1Brand = 100;
        //public static readonly int UnknownBrand = -1;

        //public static string SendSms(int brand, string destinationNumber, string message)
        //{
        //    try
        //    {
        //        // add country code
        //        destinationNumber = AddCountryCode(destinationNumber);

        //        string smsOriginator;
        //        if (brand == VectoneBrand)
        //            smsOriginator = ConfigAccessor.SmsOriginatorVectone;
        //        else if (brand == DelightBrand)
        //            smsOriginator = ConfigAccessor.SmsOriginatorDelight;
        //        else if (brand == Uk1Brand)
        //            smsOriginator = ConfigAccessor.SmsOriginatorUk1;
        //        else
        //            throw new Exception("Unknown mobile brand");

        //        string param =
        //            "?service-name=port" +
        //            "&destination-addr={0}" +
        //            "&originator-addr-type=5" +
        //            "&originator-addr={1}" +
        //            "&payload-type=text" +
        //            "&message={2}";

        //        string tpgResponseData = "-1: Failed connecting to the TPG Service!";

        //        // Generate request
        //        HttpWebRequest tpg = (HttpWebRequest)WebRequest.Create(
        //            string.Format(
        //                SmsServerUrl + param,
        //                destinationNumber,
        //                smsOriginator,
        //                HttpUtility.UrlPathEncode(message)));

        //        tpg.Method = "POST";
        //        tpg.UserAgent = "TPGWeb";
        //        tpg.ContentLength = 0;

        //        // Send request and get the response data
        //        HttpWebResponse tpgResponse = (HttpWebResponse)tpg.GetResponse();
        //        StreamReader tpgResponseReader = new StreamReader(tpgResponse.GetResponseStream());
        //        tpgResponseData = tpgResponseReader.ReadToEnd();

        //        log.Info("SMS Response :" + tpgResponseData);
        //        return tpgResponseData;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Exception when sending SMS: " + ex.Message);
        //        throw new Exception("Exception when sending SMS: " + ex.Message);
        //    }
        //}

        //public static string AddCountryCode(string Msisdn)
        //{
        //    // example input from crm : 07892170022
        //    // example input for hlrdb: 447892170022

        //    string prefix = "44"; // country code for UK

        //    if (Msisdn.StartsWith("0"))
        //        Msisdn = prefix + Msisdn.Substring(1, Msisdn.Length - 1);
        //    else if (!Msisdn.StartsWith(prefix))
        //        Msisdn = prefix + Msisdn;

        //    return Msisdn;
        //}

        #region RemoveCountryCode
        public static string RemoveCountryCode(string Msisdn)
        {
            // example input from crm : 447892170022
            // example input for hlrdb: 07892170022

            string prefix = "0";

            if (Msisdn.StartsWith("44"))
                Msisdn = prefix + Msisdn.Substring(2, Msisdn.Length - 2);
            else if (!Msisdn.StartsWith(prefix))
                Msisdn = prefix + Msisdn;

            return Msisdn;
        } 
        #endregion

        #endregion Sending SMS operations
        //TODO : Need to Remove
        //public static int GetBrand(string msisdn)
        //{
        //    msisdn = AddCountryCode(msisdn);

        //    try
        //    {
        //        SQLDataAccessLayer DAL = new SQLDataAccessLayer(ConfigAccessor.EspConnectionString);
        //        SqlCommand cmd = new SqlCommand();

        //        DAL.AddParamToSQLCmd(cmd, "@msisdn", SqlDbType.VarChar, 16, ParameterDirection.Input, msisdn);
        //        DAL.AddParamToSQLCmd(cmd, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Output, "");
        //        DAL.AddParamToSQLCmd(cmd, "@brand_type", SqlDbType.Int, int.MaxValue, ParameterDirection.Output, 0);
        //        DAL.AddParamToSQLCmd(cmd, "@errcode", SqlDbType.Int, int.MaxValue, ParameterDirection.Output, 0);
        //        DAL.AddParamToSQLCmd(cmd, "@errmsg", SqlDbType.VarChar, 100, ParameterDirection.Output, "");
        //        DAL.SetCommandType(cmd, CommandType.StoredProcedure, Sp_Brand_Differentiation);

        //        DAL.ExecuteNonQuery(cmd);

        //        if (string.IsNullOrEmpty(cmd.Parameters["@brand_type"].Value.ToString()))
        //            return UnknownBrand;

        //        return Int32.Parse(cmd.Parameters["@brand_type"].Value.ToString());
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error during BrandDifferentiation: " + ex.Message);
        //        throw new Exception("Error during BrandDifferentiation: " + ex.Message);
        //    }
        //}

    }
}