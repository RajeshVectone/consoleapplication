﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NLog;
using System.Configuration;

namespace MNPUKSMSPortout
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string msisdn = string.Empty;
                if (Request.Form != null && Request.Form.Count > 0)
                {
                    msisdn = Request.Form["msisdn"];
                }
                else
                {
                    msisdn = Request.QueryString["msisdn"];
                }
                log.Info("Request : MSISDN : {0}", msisdn);

                if (!String.IsNullOrEmpty(msisdn))
                {
                    msisdn = PortingLogic.RemoveCountryCode(msisdn);
                    Output output = DataAccess.GetMobilenoInfo(msisdn);
                    if (output != null && output.errcode == 0)
                    {
                        if (output.need_paccode == 1 && String.IsNullOrEmpty(output.paccode))
                        {
                            string response = DoCreatePac(msisdn, output.iccid, output.firstname, output.reqid);
                            try
                            {
                                Response.Clear();
                                Response.ContentType = "text/plain";
                                Response.Write(response);
                            }
                            catch { }
                        }
                        else
                        {
                            string smsMessage = ConfigurationManager.AppSettings["SMSCONTENTSUCCESS"];
                            smsMessage = string.Format(smsMessage, output.paccode, output.exp_date);
                            try
                            {
                                Response.Clear();
                                Response.ContentType = "text/plain";
                                Response.Write(smsMessage);
                            }
                            catch { }
                        }
                    }
                }
                else
                {
                    try
                    {
                        Response.Clear();
                        Response.ContentType = "text/plain";
                        Response.Write(string.Format("Invalid MSISDN : {0}!", msisdn));
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                log.Error("Page_Load : " + ex.Message);
                try
                {
                    Response.Clear();
                    Response.ContentType = "text/plain";
                    Response.Write(ConfigurationManager.AppSettings["SMSCONTENTFAILURE"]);
                }
                catch { }

            }
        } 
        #endregion

        #region DoCreatePac
        private string DoCreatePac(string mobileno, string iccid, string name, int reqId)
        {
            try
            {
                string sessionId = PortingLogic.GetSessionId();
                log.Info("sessionId : " + sessionId);

                CreateEntries reqObj = PortingLogic.CreateSingleCreateEntries(mobileno, "", name, mobileno, "Admin", "Submitted via Call Centre at " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "Vectone MSISDN: " + mobileno);

                int counter = 0; // for retry
                Retry:
                string reqXml = XmlAdapter.CreateCookieXml(sessionId) + XmlAdapter.CreateCreateEntriesXml(reqObj);
                log.Info("reqXml : " + reqXml);
                string resXml = XmlAdapter.send_xml(reqXml);
                log.Info("resXml : " + resXml);

                #region If error
                if (resXml.Contains("error"))
                {
                    Error errObj = XmlAdapter.ReceiveErrorXml(resXml);

                    string errMessage = string.Empty;

                    // if not login yet, login and retry, else display the error
                    if ((errObj.ErrorCode == 1601 || errObj.ErrorCode == 999901) && counter < 3)
                    {
                        counter++;

                        goto Retry;
                    }
                    else
                    {
                        errMessage = errObj.Detail[1];
                        log.Error(String.Format("{0} (Syniverse error code {1})", errMessage, errObj.ErrorCode.ToString()));
                    }

                    PortingLogic.SaveTransactionLog("", reqObj.EntryCreationDetail[0].MsisdnClass.PrimaryMsisdn.Msisdn,
                        "(DNO)CreateEntry", errObj.ErrorCode, PortingLogic.NetworkOperatorCode,
                        null, null);

                    DataAccess.UpdateRequest(mobileno, reqId, "", errMessage);

                    return ConfigurationManager.AppSettings["SMSCONTENTFAILURE"];
                }
                #endregion
                #region If success
                else
                {
                    CreateEntriesResp resObj = XmlAdapter.ReceiveCreateEntriesRespXml(resXml);

                    PortingLogic.SaveTransactionLog(resObj.PacDetail.Pac, reqObj.EntryCreationDetail[0].MsisdnClass.PrimaryMsisdn.Msisdn,
                        "(DNO)CreateEntry", 0, PortingLogic.NetworkOperatorCode,
                        null, null);


                    PortingLogic.SaveSingleCreateEntriesResp(reqObj, resObj, ""); // save to database

                    //TODO : Need to Remove
                    //try
                    //{
                    //string smsMessage = PortingLogic.GetSmsContent("CREATE_PAC");
                    //int brand = PortingLogic.GetBrand(resObj.EntryResultList.EntryResult[0].Msisdn);
                    //if (string.IsNullOrEmpty(smsMessage) || brand == PortingLogic.UnknownBrand)
                    //    throw new Exception("Sms content not found or brand is unknown");

                    //smsMessage = smsMessage.Replace("{MSISDN}", resObj.EntryResultList.EntryResult[0].Msisdn);
                    //smsMessage = smsMessage.Replace("{PAC}", resObj.PacDetail.Pac);
                    //smsMessage = smsMessage.Replace("{EXP_DATE}", resObj.PacDetail.ExpiryDate.ToString("dd/MM/yy"));

                    //PortingLogic.SendSms(brand, resObj.EntryResultList.EntryResult[0].Msisdn, smsMessage);
                    //}
                    //catch (Exception e)
                    //{
                    //    log.Error("An error was encountered when sending SMS to the customer: " + e.Message);
                    //}


                    //((CRM)Master).ShowInfo("Success: " + resXml); // change later
                    //string successMsg =
                    //    "PAC created for MSISDN " + resObj.EntryResultList.EntryResult[0].Msisdn +
                    //    " : " + resObj.PacDetail.Pac +
                    //    ". This PAC will be valid until " + resObj.PacDetail.ExpiryDate.ToString("MMMM dd, yyyy") +
                    //    ". PAC info will also sent via SMS to customer's mobile phone.";

                    string smsMessage = ConfigurationManager.AppSettings["SMSCONTENTSUCCESS"];
                    smsMessage = string.Format(smsMessage, resObj.PacDetail.Pac, resObj.PacDetail.ExpiryDate.ToString("dd/MM/yy"));
                    DataAccess.UpdateRequest(mobileno, reqId, resObj.PacDetail.Pac, smsMessage);
                    log.Info(smsMessage);
                    return smsMessage;
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("DoCreatePac : " + ex.Message);
                throw;
            }
        } 
        #endregion
    }
}