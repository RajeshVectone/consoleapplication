﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MNPUKSMSPortout
{
    #region Request DTD classes

    public class MnpRequestContainer
    {
        #region Logon

        private Logon _logon;

        public Logon Logon
        {
            get { return _logon; }
            set { _logon = value; }
        }

        #endregion Logon

        #region Cookie

        private string _cookie;

        public string Cookie
        {
            get { return _cookie; }
            set { _cookie = value; }
        }

        #endregion Cookie

        #region Logoff

        private Logoff _logoff;

        public Logoff LogOff
        {
            get { return _logoff; }
            set { _logoff = value; }
        }

        #endregion Logoff

        #region MnpRequestList

        private List<MnpRequest> _mnpRequestList;

        public List<MnpRequest> MnpRequestList
        {
            get { return _mnpRequestList; }
            set { _mnpRequestList = value; }
        }

        #endregion MnpRequestList
    }

    public abstract class MnpRequest
    {
        #region ReqCode

        private string _reqCode;

        public string ReqCode
        {
            get { return _reqCode; }
            set { _reqCode = value; }
        }

        #endregion ReqCode
    }

    public class Logon
    {
        #region Authenticator

        private Authenticator _authenticator;

        public Authenticator Authenticator
        {
            get { return _authenticator; }
            set { _authenticator = value; }
        }

        #endregion Authenticator
    }

    public class Logoff : MnpRequest
    {
    }

    public class CreateEntries : MnpRequest
    {
        #region SpOptionalData

        private SpOptionalData _spOptionalData;

        public SpOptionalData SpOptionalData
        {
            get { return _spOptionalData; }
            set { _spOptionalData = value; }
        }

        #endregion SpOptionalData

        #region EntryCreationDetail

        private List<EntryCreationDetail> _entryCreationDetail;

        public List<EntryCreationDetail> EntryCreationDetail
        {
            get { return _entryCreationDetail; }
            set { _entryCreationDetail = value; }
        }

        #endregion EntryCreationDetail
    }

    public class RetrieveEntry : MnpRequest
    {
        #region LooseEntryKey

        private LooseEntryKey _looseEntryKey;

        public LooseEntryKey LooseEntryKey
        {
            get { return _looseEntryKey; }
            set { _looseEntryKey = value; }
        }

        #endregion LooseEntryKey
    }

    public class CancelEntry : MnpRequest
    {
        #region Constructor to set default value

        public CancelEntry()
        {
            _cancelAllInPac = false;
        }

        #endregion Constructor to set default value

        #region EntryKey

        private EntryKey _entryKey;

        public EntryKey EntryKey
        {
            get { return _entryKey; }
            set { _entryKey = value; }
        }

        #endregion EntryKey

        #region CancelAllInPac

        private bool _cancelAllInPac;

        public bool CancelAllInPac
        {
            get { return _cancelAllInPac; }
            set { _cancelAllInPac = value; }
        }

        #endregion CancelAllInPac
    }

    public class SubmitRequest : MnpRequest
    {
        #region Constructor to set default value

        public SubmitRequest()
        {
            _useSuggestedDateOnConflict = false;
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region EntryKey

        private EntryKey _entryKey;

        public EntryKey EntryKey
        {
            get { return _entryKey; }
            set { _entryKey = value; }
        }

        #endregion EntryKey

        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate

        #region SpOptionalData

        private SpOptionalData _spOptionalData;

        public SpOptionalData SpOptionalData
        {
            get { return _spOptionalData; }
            set { _spOptionalData = value; }
        }

        #endregion SpOptionalData

        #region UseSuggestedDateOnConflict

        private bool _useSuggestedDateOnConflict;

        public bool UseSuggestedDateOnConflict
        {
            get { return _useSuggestedDateOnConflict; }
            set { _useSuggestedDateOnConflict = value; }
        }

        #endregion UseSuggestedDateOnConflict
    }

    public class ReviseRequest : MnpRequest
    {
        #region Constructor to set default value

        public ReviseRequest()
        {
            _useSuggestedDateOnConflict = true;
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region EntryKey

        private EntryKey _entryKey;

        public EntryKey EntryKey
        {
            get { return _entryKey; }
            set { _entryKey = value; }
        }

        #endregion EntryKey

        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate

        #region SpOptionalData

        private SpOptionalData _spOptionalData;

        public SpOptionalData SpOptionalData
        {
            get { return _spOptionalData; }
            set { _spOptionalData = value; }
        }

        #endregion SpOptionalData

        #region UseSuggestedDateOnConflict

        private bool _useSuggestedDateOnConflict;

        public bool UseSuggestedDateOnConflict
        {
            get { return _useSuggestedDateOnConflict; }
            set { _useSuggestedDateOnConflict = value; }
        }

        #endregion UseSuggestedDateOnConflict
    }

    public class CancelRequest : MnpRequest
    {
        #region Constructor to set default value

        public CancelRequest()
        {
            _cancelAllInPac = false;
        }

        #endregion Constructor to set default value

        #region EntryKey

        private EntryKey _entryKey;

        public EntryKey EntryKey
        {
            get { return _entryKey; }
            set { _entryKey = value; }
        }

        #endregion EntryKey

        #region CancelAllInPac

        private bool _cancelAllInPac;

        public bool CancelAllInPac
        {
            get { return _cancelAllInPac; }
            set { _cancelAllInPac = value; }
        }

        #endregion CancelAllInPac
    }

    public class ReviseEntry : MnpRequest
    {
        #region EntryKey

        private EntryKey _entryKey;

        public EntryKey EntryKey
        {
            get { return _entryKey; }
            set { _entryKey = value; }
        }

        #endregion EntryKey

        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode

        #region ReviseMsisdnClass

        private ReviseMsisdnClass _reviseMsisdnClass;

        public ReviseMsisdnClass ReviseMsisdnClass
        {
            get { return _reviseMsisdnClass; }
            set { _reviseMsisdnClass = value; }
        }

        #endregion ReviseMsisdnClass
    }

    public class ProgressSummary : MnpRequest
    {
    }

    public class ProgressReadRange : MnpRequest
    {
        #region LowId

        private int _lowId;

        public int LowId
        {
            get { return _lowId; }
            set { _lowId = value; }
        }

        #endregion LowId

        #region HighId

        private int _highId;

        public int HighId
        {
            get { return _highId; }
            set { _highId = value; }
        }

        #endregion HighId
    }

    public class PasswordChange : MnpRequest
    {
        #region OldPassword

        private string _oldPassword;

        public string OldPassword
        {
            get { return _oldPassword; }
            set { _oldPassword = value; }
        }

        #endregion OldPassword

        #region NewPassword

        private string _newPassword;

        public string NewPassword
        {
            get { return _newPassword; }
            set { _newPassword = value; }
        }

        #endregion NewPassword
    }

    public class AllSpDetails : MnpRequest
    {
    }

    public class Ping : MnpRequest
    {
    }

    #endregion Request DTD classes

    #region Reporting Request DTD classes

    public abstract class MnpReportRequest
    {
        #region ReqCode

        private string _reqCode;

        public string ReqCode
        {
            get { return _reqCode; }
            set { _reqCode = value; }
        }

        #endregion ReqCode

        #region Name

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion Name

        #region Logon

        private Logon _logon;

        public Logon Logon
        {
            get { return _logon; }
            set { _logon = value; }
        }

        #endregion Logon

        #region Cookie

        private string _cookie;

        public string Cookie
        {
            get { return _cookie; }
            set { _cookie = value; }
        }

        #endregion Cookie

        #region Logoff

        private Logoff _logoff;

        public Logoff LogOff
        {
            get { return _logoff; }
            set { _logoff = value; }
        }

        #endregion Logoff
    }

    public class Report : MnpReportRequest
    {
        #region Constructor to set default value

        public Report()
        {
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn

        #region PacExpirySelector

        private PacExpirySelector _pacExpirySelector;

        public PacExpirySelector PacExpirySelector
        {
            get { return _pacExpirySelector; }
            set { _pacExpirySelector = value; }
        }

        #endregion PacExpirySelector

        #region StatusSelector

        private StatusSelector _statusSelector;

        public StatusSelector StatusSelector
        {
            get { return _statusSelector; }
            set { _statusSelector = value; }
        }

        #endregion StatusSelector

        #region BulkTransferSelector

        private BulkTransferSelector _bulkTransferSelector;

        public BulkTransferSelector BulkTransferSelector
        {
            get { return _bulkTransferSelector; }
            set { _bulkTransferSelector = value; }
        }

        #endregion BulkTransferSelector

        #region MsisdnQuantitySelector

        private MsisdnQuantitySelector _msisdnQuantitySelector;

        public MsisdnQuantitySelector MsisdnQuantitySelector
        {
            get { return _msisdnQuantitySelector; }
            set { _msisdnQuantitySelector = value; }
        }

        #endregion MsisdnQuantitySelector

        #region PrimaryMsisdn

        private PrimaryMsisdn _primaryMsisdn;

        public PrimaryMsisdn PrimaryMsisdn
        {
            get { return _primaryMsisdn; }
            set { _primaryMsisdn = value; }
        }

        #endregion PrimaryMsisdn

        #region MsisdnTypeSelector

        private MsisdnTypeSelector _msisdnTypeSelector;

        public MsisdnTypeSelector MsisdnTypeSelector
        {
            get { return _msisdnTypeSelector; }
            set { _msisdnTypeSelector = value; }
        }

        #endregion MsisdnTypeSelector

        #region TransferTypeSelector

        private TransferTypeSelector _transferTypeSelector;

        public TransferTypeSelector TransferTypeSelector
        {
            get { return _transferTypeSelector; }
            set { _transferTypeSelector = value; }
        }

        #endregion TransferTypeSelector

        #region PortDueSelector

        private PortDueSelector _portDueSelector;

        public PortDueSelector PortDueSelector
        {
            get { return _portDueSelector; }
            set { _portDueSelector = value; }
        }

        #endregion PortDueSelector

        #region PacAgeSelector

        private PacAgeSelector _pacAgeSelector;

        public PacAgeSelector PacAgeSelector
        {
            get { return _pacAgeSelector; }
            set { _pacAgeSelector = value; }
        }

        #endregion PacAgeSelector

        #region TaggedSelector

        private TaggedSelector _taggedSelector;

        public TaggedSelector TaggedSelector
        {
            get { return _taggedSelector; }
            set { _taggedSelector = value; }
        }

        #endregion TaggedSelector

        #region DspAccNo

        private string _dspAccNo;

        public string DspAccNo
        {
            get { return _dspAccNo; }
            set { _dspAccNo = value; }
        }

        #endregion DspAccNo

        #region DspRef

        private string _dspRef;

        public string DspRef
        {
            get { return _dspRef; }
            set { _dspRef = value; }
        }

        #endregion DspRef

        #region DspContactName

        private string _dspContactName;

        public string DspContactName
        {
            get { return _dspContactName; }
            set { _dspContactName = value; }
        }

        #endregion DspContactName

        #region DspContactDetails

        private string _dspContactDetails;

        public string DspContactDetails
        {
            get { return _dspContactDetails; }
            set { _dspContactDetails = value; }
        }

        #endregion DspContactDetails

        #region DspSelector

        private DspSelector _dspSelector;

        public DspSelector DspSelector
        {
            get { return _dspSelector; }
            set { _dspSelector = value; }
        }

        #endregion DspSelector

        #region DnoSelector

        private DnoSelector _dnoSelector;

        public DnoSelector DnoSelector
        {
            get { return _dnoSelector; }
            set { _dnoSelector = value; }
        }

        #endregion DnoSelector

        #region RspAccNo

        private string _rspAccNo;

        public string RspAccNo
        {
            get { return _rspAccNo; }
            set { _rspAccNo = value; }
        }

        #endregion RspAccNo

        #region RspRef

        private string _rspRef;

        public string RspRef
        {
            get { return _rspRef; }
            set { _rspRef = value; }
        }

        #endregion RspRef

        #region RspContactName

        private string _rspContactName;

        public string RspContactName
        {
            get { return _rspContactName; }
            set { _rspContactName = value; }
        }

        #endregion RspContactName

        #region RspContactDetails

        private string _rspContactDetails;

        public string RspContactDetails
        {
            get { return _rspContactDetails; }
            set { _rspContactDetails = value; }
        }

        #endregion RspContactDetails

        #region RspSelector

        private RspSelector _rspSelector;

        public RspSelector RspSelector
        {
            get { return _rspSelector; }
            set { _rspSelector = value; }
        }

        #endregion RspSelector

        #region RnoSelector

        private RnoSelector _rnoSelector;

        public RnoSelector RnoSelector
        {
            get { return _rnoSelector; }
            set { _rnoSelector = value; }
        }

        #endregion RnoSelector

        #region SpId

        private string _spId;

        public string SpId
        {
            get { return _spId; }
            set { _spId = value; }
        }

        #endregion SpId

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate
    }

    #endregion Reporting Request DTD classes

    // Reference: Interface Spec v18 document chapter 8

    #region Response DTD classes

    public class MnpResponseContainer
    {
        #region Error

        private Error _error;

        public Error Error
        {
            get { return _error; }
            set { _error = value; }
        }

        #endregion Error

        #region LogonResp

        private LogonResp _logonResp;

        public LogonResp LogonResp
        {
            get { return _logonResp; }
            set { _logonResp = value; }
        }

        #endregion LogonResp

        #region LogoffResp

        private LogoffResp _logoffResp;

        public LogoffResp LogoffResp
        {
            get { return _logoffResp; }
            set { _logoffResp = value; }
        }

        #endregion LogoffResp

        #region MnpResponseList

        private List<MnpResponse> _mnpResponseList;

        public List<MnpResponse> MnpResponseList
        {
            get { return _mnpResponseList; }
            set { _mnpResponseList = value; }
        }

        #endregion MnpResponseList
    }

    public abstract class MnpResponse
    {
        #region RespCode

        private string _respCode;

        public string RespCode
        {
            get { return _respCode; }
            set { _respCode = value; }
        }

        #endregion RespCode
    }

    public class LogonResp
    {
        #region Cookie

        private string _cookie;

        public string Cookie
        {
            get { return _cookie; }
            set { _cookie = value; }
        }

        #endregion Cookie

        #region PasswordExpiryWarning

        private PasswordExpiryWarning _passwordExpiryWarning;

        public PasswordExpiryWarning PasswordExpiryWarning
        {
            get { return _passwordExpiryWarning; }
            set { _passwordExpiryWarning = value; }
        }

        #endregion PasswordExpiryWarning
    }

    public class LogoffResp : MnpResponse
    {
    }

    public class CreateEntriesResp : MnpResponse
    {
        #region PacDetail

        private PacDetail _pacDetail;

        public PacDetail PacDetail
        {
            get { return _pacDetail; }
            set { _pacDetail = value; }
        }

        #endregion PacDetail

        #region Error

        private Error _error;

        public Error Error
        {
            get { return _error; }
            set { _error = value; }
        }

        #endregion Error

        #region EntryResultList

        private EntryResultList _entryResultList;

        public EntryResultList EntryResultList
        {
            get { return _entryResultList; }
            set { _entryResultList = value; }
        }

        #endregion EntryResultList
    }

    public class RetrieveEntryResp : MnpResponse
    {
        #region RetrieveEntryDetail

        private RetrieveEntryDetail _retrieveEntryDetail;

        public RetrieveEntryDetail RetrieveEntryDetail
        {
            get { return _retrieveEntryDetail; }
            set { _retrieveEntryDetail = value; }
        }

        #endregion RetrieveEntryDetail
    }

    public class CancelEntryResp : MnpResponse
    {
        #region Constructor to set default values

        public CancelEntryResp()
        {
            _cancelAllInPacRequested = false;
        }

        #endregion Constructor to set default values

        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region MultipleEntryResult

        private MultipleEntryResult _multipleEntryResult;

        public MultipleEntryResult MultipleEntryResult
        {
            get { return _multipleEntryResult; }
            set { _multipleEntryResult = value; }
        }

        #endregion MultipleEntryResult

        #region CancelAllInPacRequested

        private bool _cancelAllInPacRequested;

        public bool CancelAllInPacRequested
        {
            get { return _cancelAllInPacRequested; }
            set { _cancelAllInPacRequested = value; }
        }

        #endregion CancelAllInPacRequested
    }

    public class SubmitRequestResp : MnpResponse
    {
        #region Constructor to set default values

        public SubmitRequestResp()
        {
            _portDateWarning = false;
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default values

        #region EntryResult

        private EntryResult _entryResult;

        public EntryResult EntryResult
        {
            get { return _entryResult; }
            set { _entryResult = value; }
        }

        #endregion EntryResult

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate

        #region PortDateWarning

        private bool _portDateWarning;

        public bool PortDateWarning
        {
            get { return _portDateWarning; }
            set { _portDateWarning = value; }
        }

        #endregion PortDateWarning

        #region DnoCode

        private string _dnoCode;

        public string DnoCode
        {
            get { return _dnoCode; }
            set { _dnoCode = value; }
        }

        #endregion DnoCode
    }

    public class ReviseRequestResp : MnpResponse
    {
        #region Constructor to set default values

        public ReviseRequestResp()
        {
            _portDateWarning = false;
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default values

        #region EntryResult

        private EntryResult _entryResult;

        public EntryResult EntryResult
        {
            get { return _entryResult; }
            set { _entryResult = value; }
        }

        #endregion EntryResult

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate

        #region PortDateWarning

        private bool _portDateWarning;

        public bool PortDateWarning
        {
            get { return _portDateWarning; }
            set { _portDateWarning = value; }
        }

        #endregion PortDateWarning
    }

    public class CancelRequestResp : MnpResponse
    {
        #region Constructor to set default values

        public CancelRequestResp()
        {
            _cancelAllInPacRequested = false;
        }

        #endregion Constructor to set default values

        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region MultipleEntryResult

        private MultipleEntryResult _multipleEntryResult;

        public MultipleEntryResult MultipleEntryResult
        {
            get { return _multipleEntryResult; }
            set { _multipleEntryResult = value; }
        }

        #endregion MultipleEntryResult

        #region CancelAllInPacRequested

        private bool _cancelAllInPacRequested;

        public bool CancelAllInPacRequested
        {
            get { return _cancelAllInPacRequested; }
            set { _cancelAllInPacRequested = value; }
        }

        #endregion CancelAllInPacRequested
    }

    public class ReviseEntryResp : MnpResponse
    {
        #region EntryResult

        private EntryResult _entryResult;

        public EntryResult EntryResult
        {
            get { return _entryResult; }
            set { _entryResult = value; }
        }

        #endregion EntryResult
    }

    public class PasswordChangeResp : MnpResponse
    {
        #region Confirmed

        // Empty element Confirmed

        #endregion Confirmed

        #region Error

        private Error _error;

        public Error Error
        {
            get { return _error; }
            set { _error = value; }
        }

        #endregion Error
    }

    public class AllSpDetailsResp : MnpResponse
    {
        #region SpDetails

        private List<SpDetails> _spDetails;

        public List<SpDetails> SpDetails
        {
            get { return _spDetails; }
            set { _spDetails = value; }
        }

        #endregion SpDetails
    }

    public class PingResp : MnpResponse
    {
    }

    public class ProgressSummaryResp : MnpResponse
    {
        #region LowId

        private int _lowId;

        public int LowId
        {
            get { return _lowId; }
            set { _lowId = value; }
        }

        #endregion LowId

        #region HighId

        private int _highId;

        public int HighId
        {
            get { return _highId; }
            set { _highId = value; }
        }

        #endregion HighId
    }

    public class ProgressReadRangeResp : MnpResponse
    {
        #region ProgressReadDetail

        private List<ProgressReadDetail> _progressReadDetail;

        public List<ProgressReadDetail> ProgressReadDetail
        {
            get { return _progressReadDetail; }
            set { _progressReadDetail = value; }
        }

        #endregion ProgressReadDetail
    }

    #endregion Response DTD classes

    #region Reporting Response DTD classes

    public class MnpReportResponseContainer
    {
        #region Error

        private Error _error;

        public Error Error
        {
            get { return _error; }
            set { _error = value; }
        }

        #endregion Error

        #region LogonResp

        private LogonResp _logonResp;

        public LogonResp LogonResp
        {
            get { return _logonResp; }
            set { _logonResp = value; }
        }

        #endregion LogonResp

        #region ReportResp

        private ReportResp _reportResp;

        public ReportResp ReportResp
        {
            get { return _reportResp; }
            set { _reportResp = value; }
        }

        #endregion ReportResp

        #region ErrorReportResp

        private Error _errorReportResp;

        public Error ErrorReportResp
        {
            get { return _errorReportResp; }
            set { _errorReportResp = value; }
        }

        #endregion ErrorReportResp

        #region LogoffResp

        private LogoffResp _logoffResp;

        public LogoffResp LogoffResp
        {
            get { return _logoffResp; }
            set { _logoffResp = value; }
        }

        #endregion LogoffResp

        #region ErrorLogoffResp

        private Error _errorLogoffResp;

        public Error ErrorLogoffResp
        {
            get { return _errorLogoffResp; }
            set { _errorLogoffResp = value; }
        }

        #endregion ErrorLogoffResp
    }

    public abstract class MnpReportResponse
    {
        #region RespCode

        private string _respCode;

        public string RespCode
        {
            get { return _respCode; }
            set { _respCode = value; }
        }

        #endregion RespCode
    }

    public class ReportResp : MnpReportResponse
    {
        private List<ReportRecord> _reportRecord;

        public List<ReportRecord> ReportRecord
        {
            get { return _reportRecord; }
            set { _reportRecord = value; }
        }
    }

    public class ReportRecord : MnpReportResponse
    {
        #region Constructor to set default value

        public ReportRecord()
        {
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn

        #region PacExpiryDays

        private int _pacExpiryDays;

        public int PacExpiryDays
        {
            get { return _pacExpiryDays; }
            set { _pacExpiryDays = value; }
        }

        #endregion PacExpiryDays

        #region Status

        private Status _status;

        public Status Status
        {
            get { return _status; }
            set { _status = value; }
        }

        #endregion Status

        #region IsBulk

        private IsBulk _isBulk;

        public IsBulk IsBulk
        {
            get { return _isBulk; }
            set { _isBulk = value; }
        }

        #endregion IsBulk

        #region EntryCount

        private int _entryCount;

        public int EntryCount
        {
            get { return _entryCount; }
            set { _entryCount = value; }
        }

        #endregion EntryCount

        #region PrimaryMsisdn

        private PrimaryMsisdn _primaryMsisdn;

        public PrimaryMsisdn PrimaryMsisdn
        {
            get { return _primaryMsisdn; }
            set { _primaryMsisdn = value; }
        }

        #endregion PrimaryMsisdn

        #region MsisdnType

        private MsisdnType _msisdnType;

        public MsisdnType MsisdnType
        {
            get { return _msisdnType; }
            set { _msisdnType = value; }
        }

        #endregion MsisdnType

        #region TransferType

        private TransferType _transferType;

        public TransferType TransferType
        {
            get { return _transferType; }
            set { _transferType = value; }
        }

        #endregion TransferType

        #region PortDays

        private string _portDays;

        public string PortDays
        {
            get { return _portDays; }
            set { _portDays = value; }
        }

        #endregion PortDays

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate

        #region PacAge

        private int _pacAge;

        public int PacAge
        {
            get { return _pacAge; }
            set { _pacAge = value; }
        }

        #endregion PacAge

        #region IsTagged

        private TaggedSelector _isTagged;

        public TaggedSelector IsTagged
        {
            get { return _isTagged; }
            set { _isTagged = value; }
        }

        #endregion IsTagged

        #region DspContactName

        private string _dspContactName;

        public string DspContactName
        {
            get { return _dspContactName; }
            set { _dspContactName = value; }
        }

        #endregion DspContactName

        #region DspContactDetails

        private string _dspContactDetails;

        public string DspContactDetails
        {
            get { return _dspContactDetails; }
            set { _dspContactDetails = value; }
        }

        #endregion DspContactDetails

        #region DspCode

        private string _dspCode;

        public string DspCode
        {
            get { return _dspCode; }
            set { _dspCode = value; }
        }

        #endregion DspCode

        #region DnoCode

        private string _dnoCode;

        public string DnoCode
        {
            get { return _dnoCode; }
            set { _dnoCode = value; }
        }

        #endregion DnoCode

        #region DspAccNo

        private string _dspAccNo;

        public string DspAccNo
        {
            get { return _dspAccNo; }
            set { _dspAccNo = value; }
        }

        #endregion DspAccNo

        #region DspRef

        private string _dspRef;

        public string DspRef
        {
            get { return _dspRef; }
            set { _dspRef = value; }
        }

        #endregion DspRef

        #region DspName

        private string _dspName;

        public string DspName
        {
            get { return _dspName; }
            set { _dspName = value; }
        }

        #endregion DspName

        #region DnoName

        private string _dnoName;

        public string DnoName
        {
            get { return _dnoName; }
            set { _dnoName = value; }
        }

        #endregion DnoName

        #region RspContactName

        private string _rspContactName;

        public string RspContactName
        {
            get { return _rspContactName; }
            set { _rspContactName = value; }
        }

        #endregion RspContactName

        #region RspContactDetails

        private string _rspContactDetails;

        public string RspContactDetails
        {
            get { return _rspContactDetails; }
            set { _rspContactDetails = value; }
        }

        #endregion RspContactDetails

        #region RspCode

        private string _rspCode;

        public string RspCode
        {
            get { return _rspCode; }
            set { _rspCode = value; }
        }

        #endregion RspCode

        #region RnoCode

        private string _rnoCode;

        public string RnoCode
        {
            get { return _rnoCode; }
            set { _rnoCode = value; }
        }

        #endregion RnoCode

        #region SpId

        private string _spId;

        public string SpId
        {
            get { return _spId; }
            set { _spId = value; }
        }

        #endregion SpId

        #region RspAccNo

        private string _rspAccNo;

        public string RspAccNo
        {
            get { return _rspAccNo; }
            set { _rspAccNo = value; }
        }

        #endregion RspAccNo

        #region RspRef

        private string _rspRef;

        public string RspRef
        {
            get { return _rspRef; }
            set { _rspRef = value; }
        }

        #endregion RspRef

        #region RspName

        private string _rspName;

        public string RspName
        {
            get { return _rspName; }
            set { _rspName = value; }
        }

        #endregion RspName

        #region RnoName

        private string _rnoName;

        public string RnoName
        {
            get { return _rnoName; }
            set { _rnoName = value; }
        }

        #endregion RnoName
    }

    #endregion Reporting Response DTD classes

    // Reference: Interface Spec v18 document chapter 9

    #region Common DTD

    public class PasswordExpiryWarning
    {
        #region ExpiryDate

        private string _expiryDate;

        public string ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }

        #endregion ExpiryDate
    }

    public class PrivateData
    {
        #region AccNo

        private string _accNo;

        public string AccNo
        {
            get { return _accNo; }
            set { _accNo = value; }
        }

        #endregion AccNo

        #region Ref

        private string _ref;

        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        #endregion Ref

        #region SpId

        private string _spId;

        public string SpId
        {
            get { return _spId; }
            set { _spId = value; }
        }

        #endregion SpId
    }

    public class SpDetails
    {
        #region SpCode

        private string _spCode;

        public string SpCode
        {
            get { return _spCode; }
            set { _spCode = value; }
        }

        #endregion SpCode

        #region SpName

        private string SpNameField;

        public string SpName
        {
            get { return SpNameField; }
            set { SpNameField = value; }
        }

        #endregion SpName
    }

    public class NoDetails
    {
        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode

        #region NoName

        private string _noName;

        public string NoName
        {
            get { return _noName; }
            set { _noName = value; }
        }

        #endregion NoName
    }

    public class ContactInfo
    {
        #region ContactName

        private string _contactName;

        public string ContactName
        {
            get { return _contactName; }
            set { _contactName = value; }
        }

        #endregion ContactName

        #region ContactDetails

        private string _contactDetails;

        public string ContactDetails
        {
            get { return _contactDetails; }
            set { _contactDetails = value; }
        }

        #endregion ContactDetails
    }

    public class Authenticator
    {
        #region Username

        private string _username;

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        #endregion Username

        #region Password

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        #endregion Password
    }

    public class SpOptionalData
    {
        #region PrivateData

        private PrivateData _privateData;

        public PrivateData PrivateData
        {
            get { return _privateData; }
            set { _privateData = value; }
        }

        #endregion PrivateData

        #region ContactInfo

        private ContactInfo _contactInfo;

        public ContactInfo ContactInfo
        {
            get { return _contactInfo; }
            set { _contactInfo = value; }
        }

        #endregion ContactInfo
    }

    public class RetrieveEntryDetail
    {
        #region Constructor to set default value

        public RetrieveEntryDetail()
        {
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region PacDetail

        private PacDetail _pacDetail;

        public PacDetail PacDetail
        {
            get { return _pacDetail; }
            set { _pacDetail = value; }
        }

        #endregion PacDetail

        #region MsisdnClass

        private MsisdnClass _msisdnClass;

        public MsisdnClass MsisdnClass
        {
            get { return _msisdnClass; }
            set { _msisdnClass = value; }
        }

        #endregion MsisdnClass

        #region DspData

        private DspData _dspData;

        public DspData DspData
        {
            get { return _dspData; }
            set { _dspData = value; }
        }

        #endregion DspData

        #region RspData

        private RspData _rspData;

        public RspData RspData
        {
            get { return _rspData; }
            set { _rspData = value; }
        }

        #endregion RspData

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate

        #region EntryStatus

        private EntryStatus _entryStatus;

        public EntryStatus EntryStatus
        {
            get { return _entryStatus; }
            set { _entryStatus = value; }
        }

        #endregion EntryStatus
    }

    public class SpData
    {
        #region SpDetails

        private SpDetails _spDetails;

        public SpDetails SpDetails
        {
            get { return _spDetails; }
            set { _spDetails = value; }
        }

        #endregion SpDetails

        #region SpOptionalData

        private SpOptionalData _spOptionalData;

        public SpOptionalData SpOptionalData
        {
            get { return _spOptionalData; }
            set { _spOptionalData = value; }
        }

        #endregion SpOptionalData

        #region NoDetails

        private NoDetails _noDetails;

        public NoDetails NoDetails
        {
            get { return _noDetails; }
            set { _noDetails = value; }
        }

        #endregion NoDetails
    }

    public class DspData : SpData { }

    public class RspData : SpData { }

    public class EntryStatus
    {
        #region Status

        private Status _status;

        public Status Status
        {
            get { return _status; }
            set { _status = value; }
        }

        #endregion Status

        #region IsTagged

        private bool _isTagged;

        public bool IsTagged
        {
            get { return _isTagged; }
            set { _isTagged = value; }
        }

        #endregion IsTagged
    }

    public enum Status
    {
        Null, Open, Closed, Locked, Cancelled, Expired, Archived
    }

    public class ProgressReadDetail
    {
        #region Constructor to set default value

        public ProgressReadDetail()
        {
            _updateDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region ProgressId

        private int _progressId;

        public int ProgressId
        {
            get { return _progressId; }
            set { _progressId = value; }
        }

        #endregion ProgressId

        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn

        #region UpdateDate & UpdateTime

        private DateTime _updateDate;

        public DateTime UpdateDate
        {
            get { return _updateDate; }
            set
            {   // change only year, month, and day
                _updateDate = new DateTime
                    (value.Year, value.Month, value.Day,
                    _updateDate.Hour, _updateDate.Minute, _updateDate.Second);
            }
        }

        public DateTime UpdateTime
        {
            get { return _updateDate; }
            set
            {   // change only hour, minute, and second
                _updateDate = new DateTime
                    (_updateDate.Year, _updateDate.Month, _updateDate.Day,
                    value.Hour, value.Minute, value.Second);
            }
        }

        #endregion UpdateDate & UpdateTime

        #region OldStatus

        private Status _oldStatus;

        public Status OldStatus
        {
            get { return _oldStatus; }
            set { _oldStatus = value; }
        }

        #endregion OldStatus

        #region NewStatus

        private Status _newStatus;

        public Status NewStatus
        {
            get { return _newStatus; }
            set { _newStatus = value; }
        }

        #endregion NewStatus

        #region DspCode

        private string _dspCode;

        public string DspCode
        {
            get { return _dspCode; }
            set { _dspCode = value; }
        }

        #endregion DspCode

        #region RspCode

        private string _rspCode;

        public string RspCode
        {
            get { return _rspCode; }
            set { _rspCode = value; }
        }

        #endregion RspCode
    }

    public class EntryCreationDetail
    {
        #region MsisdnClass

        private MsisdnClass _msisdnClass;

        public MsisdnClass MsisdnClass
        {
            get { return _msisdnClass; }
            set { _msisdnClass = value; }
        }

        #endregion MsisdnClass

        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode
    }

    public class MsisdnClass
    {
        #region PrimaryMsisdn

        private PrimaryMsisdn _primaryMsisdn;

        public PrimaryMsisdn PrimaryMsisdn
        {
            get { return _primaryMsisdn; }
            set { _primaryMsisdn = value; }
        }

        #endregion PrimaryMsisdn

        #region SecondaryMsisdn

        private SecondaryMsisdn _secondaryMsisdn;

        public SecondaryMsisdn SecondaryMsisdn
        {
            get { return _secondaryMsisdn; }
            set { _secondaryMsisdn = value; }
        }

        #endregion SecondaryMsisdn
    }

    public class PrimaryMsisdn
    {
        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn
    }

    public class SecondaryMsisdn
    {
        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn

        #region PrimaryMsisdn

        private PrimaryMsisdn _primaryMsisdn;

        public PrimaryMsisdn PrimaryMsisdn
        {
            get { return _primaryMsisdn; }
            set { _primaryMsisdn = value; }
        }

        #endregion PrimaryMsisdn
    }

    public class ReviseMsisdnClass
    {
        #region RevisePrimaryMsisdn

        // Empty element Revise_Primary_Msisdn

        #endregion RevisePrimaryMsisdn

        #region ReviseSecondaryMsisdn

        private ReviseSecondaryMsisdn _reviseSecondaryMsisdn;

        public ReviseSecondaryMsisdn ReviseSecondaryMsisdn
        {
            get { return _reviseSecondaryMsisdn; }
            set { _reviseSecondaryMsisdn = value; }
        }

        #endregion ReviseSecondaryMsisdn
    }

    public class ReviseSecondaryMsisdn
    {
        #region PrimaryMsisdn

        private PrimaryMsisdn _primaryMsisdn;

        public PrimaryMsisdn PrimaryMsisdn
        {
            get { return _primaryMsisdn; }
            set { _primaryMsisdn = value; }
        }

        #endregion PrimaryMsisdn
    }

    public class EntryKey
    {
        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn

        // only needed in our side for Submit Request specifically

        #region BbMsisdn

        private string _bbMsisdn;

        public string BbMsisdn
        {
            get { return _bbMsisdn; }
            set { _bbMsisdn = value; }
        }

        #endregion BbMsisdn
    }

    public class LooseEntryKey
    {
        #region EntryValidator

        private EntryValidator _entryValidator;

        public EntryValidator EntryValidator
        {
            get { return _entryValidator; }
            set { _entryValidator = value; }
        }

        #endregion EntryValidator

        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn
    }

    public class EntryValidator
    {
        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region AccNo

        private string _accNo;

        public string AccNo
        {
            get { return _accNo; }
            set { _accNo = value; }
        }

        #endregion AccNo

        #region Ref

        private string _ref;

        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        #endregion Ref
    }

    public class Error : MnpResponse
    {
        public Error()
        {
            _errorCode = 0;
        }

        #region ErrorCode

        private int _errorCode;

        public int ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }

        #endregion ErrorCode

        #region Text

        private string _text;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        #endregion Text

        #region Detail

        private List<string> _detail;

        public List<string> Detail
        {
            get { return _detail; }
            set { _detail = value; }
        }

        #endregion Detail
    }

    public class PacDetail
    {
        #region Constructor to set default value

        public PacDetail()
        {
            _creationDate = DateTime.MinValue;
            _expiryDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region EntryCount

        private int _entryCount;

        public int EntryCount
        {
            get { return _entryCount; }
            set { _entryCount = value; }
        }

        #endregion EntryCount

        #region CreationDate

        private DateTime _creationDate;

        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        #endregion CreationDate

        #region ExpiryDate

        private DateTime _expiryDate;

        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }

        #endregion ExpiryDate
    }

    public class MultipleEntryResult
    {
        #region ResultCount

        private ResultCount _resultCount;

        public ResultCount ResultCount
        {
            get { return _resultCount; }
            set { _resultCount = value; }
        }

        #endregion ResultCount

        #region EntryResultList

        private EntryResultList _entryResultList;

        public EntryResultList EntryResultList
        {
            get { return _entryResultList; }
            set { _entryResultList = value; }
        }

        #endregion EntryResultList
    }

    public class EntryResultList
    {
        #region EntryResult

        private List<EntryResult> _entryResult;

        public List<EntryResult> EntryResult
        {
            get { return _entryResult; }
            set { _entryResult = value; }
        }

        #endregion EntryResult
    }

    public class EntryResult
    {
        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn

        #region Confirmed

        // Empty element Confirmed

        #endregion Confirmed

        #region Error

        private Error _error;

        public Error Error
        {
            get { return _error; }
            set { _error = value; }
        }

        #endregion Error
    }

    public class ResultCount
    {
        #region GoodCount

        private int _goodCount;

        public int GoodCount
        {
            get { return _goodCount; }
            set { _goodCount = value; }
        }

        #endregion GoodCount

        #region BadCount

        private int _badCount;

        public int BadCount
        {
            get { return _badCount; }
            set { _badCount = value; }
        }

        #endregion BadCount
    }

    #endregion Common DTD

    #region Common DTD - For Reporting

    public class PacExpirySelector
    {
        #region EqualityTest

        private EqualityTest _equalityTest;

        public EqualityTest EqualityTest
        {
            get { return _equalityTest; }
            set { _equalityTest = value; }
        }

        #endregion EqualityTest

        #region Days

        private int _days;

        public int Days
        {
            get { return _days; }
            set { _days = value; }
        }

        #endregion Days
    }

    public enum StatusSelector
    {
        Null, Open, Closed, Locked, Cancelled, Expired, Archived, Active, Dormant, All
    }

    public enum BulkTransferSelector
    {
        Null, Consumer, Bulk, Both
    }

    public class MsisdnQuantitySelector
    {
        #region EqualityTest

        private EqualityTest _equalityTest;

        public EqualityTest EqualityTest
        {
            get { return _equalityTest; }
            set { _equalityTest = value; }
        }

        #endregion EqualityTest

        #region EntryCount

        private int _entryCount;

        public int EntryCount
        {
            get { return _entryCount; }
            set { _entryCount = value; }
        }

        #endregion EntryCount
    }

    public enum MsisdnTypeSelector
    {
        Null, Primary, Secondary, Both
    }

    public enum TransferTypeSelector
    {
        Null, Port, Migration, Both
    }

    public class PortDueSelector
    {
        #region EqualityTest

        private EqualityTest _equalityTest;

        public EqualityTest EqualityTest
        {
            get { return _equalityTest; }
            set { _equalityTest = value; }
        }

        #endregion EqualityTest

        #region Days

        private int _days;

        public int Days
        {
            get { return _days; }
            set { _days = value; }
        }

        #endregion Days
    }

    public class PacAgeSelector
    {
        #region EqualityTest

        private EqualityTest _equalityTest;

        public EqualityTest EqualityTest
        {
            get { return _equalityTest; }
            set { _equalityTest = value; }
        }

        #endregion EqualityTest

        #region Days

        private int _days;

        public int Days
        {
            get { return _days; }
            set { _days = value; }
        }

        #endregion Days
    }

    public enum TaggedSelector
    {
        Null, True, False, Both
    }

    public enum EqualityTest
    {
        Null, LessThan, LessThanOrEqualTo, Equals, GreaterThan, GreaterThanOrEqualTo
    }

    public enum IsBulk
    {
        Null, Bulk, Consumer
    }

    public enum MsisdnType
    {
        Null, Primary, Secondary
    }

    public enum TransferType
    {
        Null, Port, Migration, Undefined
    }

    public class DspSelector
    {
        #region SpCode

        private string _spCode;

        public string SpCode
        {
            get { return _spCode; }
            set { _spCode = value; }
        }

        #endregion SpCode

        #region All

        // Empty element All

        #endregion All
    }

    public class DnoSelector
    {
        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode

        #region All

        // Empty element All

        #endregion All
    }

    public class RspSelector
    {
        #region SpCode

        private string _spCode;

        public string SpCode
        {
            get { return _spCode; }
            set { _spCode = value; }
        }

        #endregion SpCode

        #region All

        // Empty element All

        #endregion All
    }

    public class RnoSelector
    {
        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode

        #region All

        // Empty element All

        #endregion All
    }

    #endregion Common DTD - For Reporting

    public class Result
    {
        public Result()
        {
            _errorCode = 0;
        }

        #region ErrorCode

        private int _errorCode;

        public int ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }

        #endregion ErrorCode

        #region ErrorMsg

        private string _errormsg;

        public string ErrorMsg
        {
            get { return _errormsg; }
            set { _errormsg = value; }
        }

        #endregion ErrorMsg
    }
}