﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using NLog;
using Newtonsoft.Json;

namespace MNPUKSMSPortout
{
    public class Output : CommonOutput
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string iccid { get; set; }
        public string email { get; set; }
        public int reqid { get; set; }
        public int need_paccode { get; set; }
        public string paccode { get; set; }
        public string exp_date { get; set; }
    }

    public class CommonOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetMobilenoInfo
        public static Output GetMobilenoInfo(string MSISDN)
        {
            Output output = new Output();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NP_UK"].ConnectionString))
                {
                    conn.Open();
                    var sp = "sms_portout_get_mobileno_info";
                    var result = conn.Query<Output>(
                            sp, new
                            {
                                @mobileno = MSISDN
                            },
                            commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (result != null)
                    {
                        output.firstname = result.firstname;
                        output.lastname = result.lastname;
                        output.email = result.email;
                        output.iccid = result.iccid;
                        output.errcode = result.errcode;
                        output.errmsg = result.errmsg;
                        output.reqid = result.reqid;
                        output.need_paccode = result.need_paccode;
                        output.paccode = result.paccode; 
                        output.exp_date = result.exp_date;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = string.Format("Customer Details not found for {0}", MSISDN);
                        log.Error(string.Format("Customer Details not found for {0}", MSISDN));
                    }
                }
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
                log.Error("GetMobilenoInfo : " + ex.Message);
            }
            log.Info("Output : " + JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region UpdateRequest
        public static CommonOutput UpdateRequest(string mobileno, int reqid,string paccode,string errmsg)
        {
            CommonOutput output = new CommonOutput();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NP_UK"].ConnectionString))
                {
                    conn.Open();
                    var sp = "sms_portout_update_request";
                    var result = conn.Query<CommonOutput>(
                            sp, new
                            {
                                @mobileno = mobileno,
                                @reqid = reqid,
                                @paccode = paccode,
                                @errmsg = errmsg,
                            },
                            commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (result != null)
                    {
                        output.errcode = result.errcode;
                        output.errmsg = result.errmsg;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = string.Format("Result not found for {0}", mobileno);
                        log.Error(string.Format("Result not found for {0}", mobileno));
                    }
                }
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
                log.Error("UpdateRequest : " + ex.Message);
            }
            log.Info("Output : " + JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion
    }
}