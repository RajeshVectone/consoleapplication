﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MNPUKSMSPortout
{
    public delegate void GenerateListFromReader<T>(IDataReader returnData, ref List<T> tempList);

    internal class SQLDataAccessLayer
    {
        // PRIVATE FIELD
        private string _connectionstring;

        // PUBLIC FIELD
        public string ConnectionString
        {
            get { return _connectionstring; }
            set { _connectionstring = value; }
        }

        // CONSTRUCTORS
        public SQLDataAccessLayer()
        {
            _connectionstring = String.Empty;
        }

        public SQLDataAccessLayer(string strConnection)
        {
            if (String.IsNullOrEmpty("strConnection"))
                throw new ArgumentNullException("strConnection");

            _connectionstring = strConnection;
        }

        //*********************************************************************
        //
        // SQL Helper Methods
        //
        // The following utility methods are used to interact with SQL Server.
        //
        //*********************************************************************

        /// <summary>
        /// Adding Paramater into SQL Command
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <param name="paramId"></param>
        /// <param name="sqlType"></param>
        /// <param name="paramSize"></param>
        /// <param name="paramDirection"></param>
        /// <param name="paramvalue"></param>
        public void AddParamToSQLCmd(SqlCommand sqlCmd, string paramId, SqlDbType sqlType, int paramSize, ParameterDirection paramDirection, object paramvalue)
        {
            // Validate Parameter Properties
            if (sqlCmd == null)
                throw (new ArgumentNullException("sqlCmd"));
            if (paramId == string.Empty)
                throw (new ArgumentOutOfRangeException("paramId"));

            // Add Parameter
            SqlParameter newSqlParam = new SqlParameter();
            newSqlParam.ParameterName = paramId;
            newSqlParam.SqlDbType = sqlType;
            newSqlParam.Direction = paramDirection;

            if (paramSize > 0)
                newSqlParam.Size = paramSize;

            if (paramvalue != null)
                newSqlParam.Value = paramvalue;

            sqlCmd.Parameters.Add(newSqlParam);
        }

        /// <summary>
        /// Execute SQL Command and return the first column of the first row
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <returns></returns>
        public Object ExecuteScalarCmd(SqlCommand sqlCmd)
        {
            // Validate Command Properties
            if (_connectionstring == string.Empty)
                throw (new ArgumentOutOfRangeException("ConnectionString"));

            if (sqlCmd == null)
                throw (new ArgumentNullException("sqlCmd"));

            Object result = null;

            using (SqlConnection cn = new SqlConnection(this._connectionstring))
            {
                sqlCmd.Connection = cn;
                cn.Open();
                result = sqlCmd.ExecuteScalar();
                cn.Close();
            }

            return result;
        }

        /// <summary>
        /// Execute a Transact-SQL and return the numbers of row affected
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(SqlCommand sqlCmd)
        {
            // Validate Command Properties
            if (_connectionstring == string.Empty)
                throw (new ArgumentOutOfRangeException("ConnectionString"));

            if (sqlCmd == null)
                throw (new ArgumentNullException("sqlCmd"));

            int result;

            using (SqlConnection cn = new SqlConnection(this._connectionstring))
            {
                sqlCmd.Connection = cn;
                cn.Open();
                result = sqlCmd.ExecuteNonQuery();
                cn.Close();
            }

            return result;
        }

        //<SW0001>
        /// <summary>
        /// Execute a Transact-SQL and return the numbers of row affected and error string catched
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(SqlCommand sqlCmd, out string sError)
        {
            // Validate Command Properties
            if (_connectionstring == string.Empty)
                throw (new ArgumentOutOfRangeException("ConnectionString"));

            if (sqlCmd == null)
                throw (new ArgumentNullException("sqlCmd"));

            int result;
            string str = string.Empty;

            using (SqlConnection cn = new SqlConnection(this._connectionstring))
            {
                try
                {
                    sqlCmd.Connection = cn;
                    cn.Open();
                    result = sqlCmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    if (cn.State == ConnectionState.Open)
                        cn.Close();
                    str = e.Message;
                    result = -1;
                }
            }

            sError = str;
            return result;
        }

        //<SW0001>

        /// <summary>
        /// Execute a query and return a dataset of the result
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <returns></returns>
        /// Initial: dony@20060919T1100
        public DataSet ExecuteQuery(SqlCommand sqlCmd)
        {
            // Validate Command Properties
            if (_connectionstring == string.Empty)
                throw (new ArgumentOutOfRangeException("ConnectionString"));

            if (sqlCmd == null)
                throw (new ArgumentNullException("sqlCmd"));

            DataSet result = new DataSet();

            using (SqlConnection cn = new SqlConnection(this._connectionstring))
            {
                sqlCmd.Connection = cn;
                sqlCmd.CommandTimeout = 300;
                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                da.Fill(result);
            }

            return result;
        }

        //<SW0002>
        /// <summary>
        /// Execute a query with error message out and return a dataset of the result
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <returns></returns>
        /// Initial: nail@20090216
        public DataSet ExecuteQuery(SqlCommand sqlCmd, out string ErrorMessage)
        {
            // Validate Command Properties
            if (_connectionstring == string.Empty)
                throw (new ArgumentOutOfRangeException("ConnectionString"));

            if (sqlCmd == null)
                throw (new ArgumentNullException("sqlCmd"));

            DataSet result = new DataSet();

            using (SqlConnection cn = new SqlConnection(this._connectionstring))
            {
                try
                {
                    sqlCmd.Connection = cn;
                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    da.Fill(result);
                    ErrorMessage = string.Empty;
                }
                catch (Exception ex)
                {
                    ErrorMessage = ex.Message;
                }
            }

            return result;
        }

        //<SW0002>

        /// <summary>
        /// Execute a query and return a Generic List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlCmd"></param>
        /// <param name="glfr"></param>
        /// <param name="List"></param>
        public void ExecuteReaderCmd<T>(SqlCommand sqlCmd, GenerateListFromReader<T> glfr, ref List<T> List)
        {
            if (_connectionstring == string.Empty)
                throw (new ArgumentOutOfRangeException("ConnectionString"));

            if (sqlCmd == null)
                throw (new ArgumentNullException("sqlCmd"));
            try
            {
                using (SqlConnection cn = new SqlConnection(this._connectionstring))
                {
                    sqlCmd.Connection = cn;

                    cn.Open();

                    glfr(sqlCmd.ExecuteReader(), ref List);

                    cn.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Set SQL Command Type
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <param name="cmdType"></param>
        /// <param name="cmdText"></param>
        public void SetCommandType(SqlCommand sqlCmd, CommandType cmdType, string cmdText)
        {
            sqlCmd.CommandType = cmdType;
            sqlCmd.CommandText = cmdText;
        }
    }
}