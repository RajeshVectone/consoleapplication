﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace DelightCallingTopUp
{

    #region TariffAutomationChillitalkGBPCurrencyMinutesCountrywise
    public class TariffAutomationChillitalkGBPCurrencyMinutesCountrywise
    {
        public string calldate { get; set; }
        public string Days { get; set; }
        public string currcode { get; set; }
        public string Country { get; set; }
        public double? cli { get; set; }
        public double? talktime { get; set; }
    }
    #endregion

    #region TariffAutomationChillitalkGBPCurrencyMinutesSummary
    public class TariffAutomationChillitalkGBPCurrencyMinutesSummary
    {
        public string CallType { get; set; }
        public string Calldate { get; set; }
        public string currcode { get; set; }
        public double? Talktime { get; set; }
        public double? cli { get; set; }
        public double? Mins_Percentage { get; set; }
        public double? Total_mins { get; set; }
        public double? TotalCLI { get; set; }
    }
    #endregion

    public class TariffAutomationCDRCDSMinutesDifference
    {
        public string Calldate { get; set; }
        public int? Cdr { get; set; }
        public int? Cds { get; set; }
        public int? Difference { get; set; }
    }

    public class TariffAutomationMRECMinutesDifference
    {
        public string Calldate { get; set; }
        public string Sitecode { get; set; }
        public int? Mrec { get; set; }
        public string Country { get; set; }
        public int? Cdr { get; set; }
        public int? Difference { get; set; }
    }

    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                //Template 1
                string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile1"]);
                string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName1"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                File.Copy(templateName, docName);

                Console.WriteLine("docName1 : " + docName);
                log.Info("docName1 : " + docName);

                DoTariffAutomationChillitalkGBPCurrencyMinutesCountrywise(docName);

                DoTariffAutomationChillitalkGBPCurrencyMinutesSummary(docName);

                string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject1"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                SendMail(docName, mailSubject);

                //Template 2
                templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile2"]);
                docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName2"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                File.Copy(templateName, docName);

                Console.WriteLine("docName2 : " + docName);
                log.Info("docName2 : " + docName);

                DoTariffAutomationCDRCDSMRECMinutesDifference(docName);

                mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject2"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                SendMail(docName, mailSubject);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region DoTariffAutomationCDRCDSMRECMinutesDifference
        public static void DoTariffAutomationCDRCDSMRECMinutesDifference(string docName)
        {
            Console.WriteLine("DoTariffAutomationCDRCDSMRECMinutesDifference()");
            log.Info("DoTariffAutomationCDRCDSMRECMinutesDifference()");
            try
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                {
                    WorkbookPart bkPart = spreadSheet.WorkbookPart;
                    Workbook workbook = bkPart.Workbook;
                    Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Sheet1").FirstOrDefault();
                    WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                    //For CDR & CDS
                    uint iRowIndex = 2;
                    var records = DataAccess.GetTariffAutomationCDRCDSMinutesDifference();
                    if (records != null)
                    {
                        Console.WriteLine("CDR & CDS - Records found : " + records.Count);
                        log.Info("CDR & CDS - Records found : " + records.Count);

                        foreach (var item in records)
                        {
                            InsertRow(iRowIndex, wsPart);
                            InsertValue("A", iRowIndex, Convert.ToString(item.Calldate), CellValues.String, wsPart);
                            InsertValue("B", iRowIndex, Convert.ToString(item.Cdr), CellValues.Number, wsPart);
                            InsertValue("C", iRowIndex, Convert.ToString(item.Cds), CellValues.Number, wsPart);
                            InsertValue("D", iRowIndex, Convert.ToString(item.Difference), CellValues.Number, wsPart);
                            iRowIndex++;
                        }
                    }
                    else
                    {
                        Console.WriteLine("CDR & CDS - No records found!");
                        log.Debug("CDR & CDS - No records found!");
                    }

                    //For Mrec & CDR
                    iRowIndex += 3;
                    var records2 = DataAccess.GetTariffAutomationMRECMinutesDifference();
                    if (records2 != null)
                    {
                        Console.WriteLine("Mrec & CDR - Records found : " + records2.Count);
                        log.Info("Mrec & CDR - Records found : " + records2.Count);

                        foreach (var item in records2)
                        {
                            InsertRow(iRowIndex, wsPart);
                            InsertValue("A", iRowIndex, Convert.ToString(item.Calldate), CellValues.String, wsPart);
                            InsertValue("B", iRowIndex, Convert.ToString(item.Sitecode), CellValues.String, wsPart);
                            InsertValue("C", iRowIndex, Convert.ToString(item.Mrec), CellValues.Number, wsPart);
                            InsertValue("D", iRowIndex, Convert.ToString(item.Calldate), CellValues.String, wsPart);
                            InsertValue("E", iRowIndex, Convert.ToString(item.Sitecode), CellValues.String, wsPart);
                            InsertValue("F", iRowIndex, Convert.ToString(item.Cdr), CellValues.Number, wsPart);
                            InsertValue("G", iRowIndex, Convert.ToString(item.Difference), CellValues.Number, wsPart);
                            iRowIndex++;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Mrec & CDR - No records found!");
                        log.Debug("Mrec & CDR - No records found!");
                    }
                    wsPart.Worksheet.Save();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoTariffAutomationCDRCDSMRECMinutesDifference : " + ex.Message);
                log.Error("DoTariffAutomationCDRCDSMRECMinutesDifference : " + ex.Message);
            }
        }
        #endregion

        #region DoTariffAutomationChillitalkGBPCurrencyMinutesCountrywise
        static void DoTariffAutomationChillitalkGBPCurrencyMinutesCountrywise(string docName)
        {
            Console.WriteLine("DoTariffAutomationChillitalkGBPCurrencyMinutesCountrywise()");
            log.Info("DoTariffAutomationChillitalkGBPCurrencyMinutesCountrywise()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.GetTariffAutomationChillitalkGBPCurrencyMinutesCountrywise();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Sheet1").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct();
                        uint rowIndex = 6;
                        foreach (var item in distinctDates)
                        {
                            if (rowIndex == 6)
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 26, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 36, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 46, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 56, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 66, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                rowIndex++;
                            }
                            else
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 26, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 36, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 46, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 56, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 66, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                            }
                        }

                        var distinctDays = (from ld in records select new { id = ld.Days }).ToList().Distinct();
                        rowIndex = 6;
                        foreach (var item in distinctDays)
                        {
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 26, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 36, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 46, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 56, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 66, item.id, CellValues.String, wsPart);

                            rowIndex++;

                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 26, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 36, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 46, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 56, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 66, item.id, CellValues.String, wsPart);
                        }

                        var distinctCurrCode = (from ld in records select new { id = ld.currcode }).ToList().Distinct();
                        foreach (var currCode in distinctCurrCode)
                        {
                            if (currCode.id.Trim().ToUpper() == "GBP")
                                rowIndex = 4;
                            else if (currCode.id.Trim().ToUpper() == "USD")
                                rowIndex = 30;
                            else if (currCode.id.Trim().ToUpper() == "SEK")
                                rowIndex = 40;
                            else if (currCode.id.Trim().ToUpper() == "DKK")
                                rowIndex = 50;
                            else if (currCode.id.Trim().ToUpper() == "CHF")
                                rowIndex = 60;
                            else if (currCode.id.Trim().ToUpper() == "EUR")
                                rowIndex = 70;
                            else
                                continue;

                            var itemCurrCode = records.Where(r => r.currcode == currCode.id).Select(r => r).ToList();

                            var distinctCountrys = (from ld in itemCurrCode select new { id = ld.Country }).ToList().Distinct();
                            int countryCount = 0;
                            string columnName1 = "";
                            string columnName2 = "";
                            foreach (var item in distinctCountrys)
                            {
                                switch (countryCount)
                                {
                                    case 0:
                                        columnName1 = "C";
                                        columnName2 = "D";
                                        break;
                                    case 1:
                                        columnName1 = "E";
                                        columnName2 = "F";
                                        break;
                                    case 2:
                                        columnName1 = "G";
                                        columnName2 = "H";
                                        break;
                                    case 3:
                                        columnName1 = "I";
                                        columnName2 = "J";
                                        break;
                                    case 4:
                                        columnName1 = "K";
                                        columnName2 = "L";
                                        break;
                                    case 5:
                                        columnName1 = "M";
                                        columnName2 = "N";
                                        break;
                                    case 6:
                                        columnName1 = "O";
                                        columnName2 = "P";
                                        break;
                                    case 7:
                                        columnName1 = "Q";
                                        columnName2 = "R";
                                        countryCount = -1;
                                        break;
                                    default:
                                        break;
                                }
                                InsertValue(columnName1, rowIndex, item.id, CellValues.String, wsPart);

                                for (int i = 0; i < distinctDates.Count(); i++)
                                {
                                    var item2 = records.Where(r => r.Country == item.id && r.currcode == currCode.id && r.calldate == distinctDates.ElementAt(i).id).Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        if (i == 0)
                                        {
                                            InsertValue(columnName1, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                            InsertValue(columnName2, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        }
                                        else
                                        {
                                            InsertValue(columnName1, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                            InsertValue(columnName2, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        }
                                    }
                                }

                                if (countryCount == -1)
                                    rowIndex += 8;
                                countryCount++;
                            }
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoTariffAutomationChillitalkGBPCurrencyMinutesCountrywise : " + ex.Message);
                log.Error("DoTariffAutomationChillitalkGBPCurrencyMinutesCountrywise : " + ex.Message);
            }
        }
        #endregion

        #region DoTariffAutomationChillitalkGBPCurrencyMinutesSummary
        static void DoTariffAutomationChillitalkGBPCurrencyMinutesSummary(string docName)
        {
            Console.WriteLine("DoTariffAutomationChillitalkGBPCurrencyMinutesSummary()");
            log.Info("DoTariffAutomationChillitalkGBPCurrencyMinutesSummary()");

            try
            {
                int iRowCount = 0;

                //while (iRowCount == 0)
                //{
                var records = DataAccess.GetTariffAutomationChillitalkGBPCurrencyMinutesSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;

                    Console.Write("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Sheet1").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.Calldate }).ToList().Distinct().OrderBy(x => x.id);

                        var distinctCurr = (from ld in records select new { id = ld.currcode }).ToList().Distinct();

                        int index2 = 0;
                        foreach (var itemDates in distinctDates)
                        {
                            if (index2 == 0)
                            {
                                for (uint i = 78; i <= 128;)
                                {
                                    InsertValue("C", i, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                    i += 10;
                                }

                                foreach (var itemCurr in distinctCurr)
                                {
                                    uint iRowIndex = 0;
                                    string currency = itemCurr.id.ToUpper();
                                    if (currency == "GBP")
                                        iRowIndex = 80;
                                    else if (currency == "USD")
                                        iRowIndex = 90;
                                    else if (currency == "SEK")
                                        iRowIndex = 100;
                                    else if (currency == "DKK")
                                        iRowIndex = 110;
                                    else if (currency == "CHF")
                                        iRowIndex = 120;
                                    else if (currency == "EUR")
                                        iRowIndex = 130;
                                    else
                                        continue;

                                    //Standard_Minutes
                                    var item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "LOCAL ACCESS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("E", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Free_Credit 
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "FREE ACCESS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("E", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Bundle Minutes 
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "CHILLITALK_APP").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("E", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Total Minutes & Unique CLIs 
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency).Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Total_mins)), CellValues.Number, wsPart);
                                        InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).TotalCLI)), CellValues.Number, wsPart);
                                        //InsertValue("E", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }
                                }
                            }
                            else if (index2 == 1)
                            {
                                for (uint i = 78; i <= 128;)
                                {
                                    InsertValue("G", i, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                    i += 10;
                                }

                                foreach (var itemCurr in distinctCurr)
                                {
                                    uint iRowIndex = 0;
                                    string currency = itemCurr.id.ToUpper();
                                    if (currency == "GBP")
                                        iRowIndex = 80;
                                    else if (currency == "USD")
                                        iRowIndex = 90;
                                    else if (currency == "SEK")
                                        iRowIndex = 100;
                                    else if (currency == "DKK")
                                        iRowIndex = 110;
                                    else if (currency == "CHF")
                                        iRowIndex = 120;
                                    else if (currency == "EUR")
                                        iRowIndex = 130;
                                    else
                                        continue;

                                    //Standard_Minutes
                                    var item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "LOCAL ACCESS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("I", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Free_Credit
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "FREE ACCESS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("I", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Bundle Minutes
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "CHILLITALK_APP").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("I", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Total Minutes & Unique CLIs
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency).Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Total_mins)), CellValues.Number, wsPart);
                                        InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).TotalCLI)), CellValues.Number, wsPart);
                                        //InsertValue("I", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }
                                }
                            }
                            else if (index2 == 2)
                            {
                                for (uint i = 78; i <= 128;)
                                {
                                    InsertValue("K", i, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                    i += 10;
                                }

                                foreach (var itemCurr in distinctCurr)
                                {
                                    uint iRowIndex = 0;
                                    string currency = itemCurr.id.ToUpper();
                                    if (currency == "GBP")
                                        iRowIndex = 80;
                                    else if (currency == "USD")
                                        iRowIndex = 90;
                                    else if (currency == "SEK")
                                        iRowIndex = 100;
                                    else if (currency == "DKK")
                                        iRowIndex = 110;
                                    else if (currency == "CHF")
                                        iRowIndex = 120;
                                    else if (currency == "EUR")
                                        iRowIndex = 130;
                                    else
                                        continue;

                                    //Standard_Minutes
                                    var item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "LOCAL ACCESS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("M", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Free_Credit
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "FREE ACCESS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("M", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Bundle Minutes
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "CHILLITALK_APP").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("M", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Total Minutes & Unique CLIs
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency).Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Total_mins)), CellValues.Number, wsPart);
                                        InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).TotalCLI)), CellValues.Number, wsPart);
                                        //InsertValue("M", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }
                                }

                            }
                            else if (index2 == 3)
                            {
                                for (uint i = 78; i <= 128;)
                                {
                                    InsertValue("O", i, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                    i += 10;
                                }

                                foreach (var itemCurr in distinctCurr)
                                {
                                    uint iRowIndex = 0;
                                    string currency = itemCurr.id.ToUpper();
                                    if (currency == "GBP")
                                        iRowIndex = 80;
                                    else if (currency == "USD")
                                        iRowIndex = 90;
                                    else if (currency == "SEK")
                                        iRowIndex = 100;
                                    else if (currency == "DKK")
                                        iRowIndex = 110;
                                    else if (currency == "CHF")
                                        iRowIndex = 120;
                                    else if (currency == "EUR")
                                        iRowIndex = 130;
                                    else
                                        continue;

                                    //Standard_Minutes
                                    var item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "LOCAL ACCESS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("Q", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Free_Credit
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "FREE ACCESS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("Q", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Bundle Minutes
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.CallType.ToUpper().Trim() == "CHILLITALK_APP").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        //InsertValue("Q", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Total Minutes & Unique CLIs
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.Calldate == itemDates.id && r.currcode.ToUpper().Trim() == currency).Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Total_mins)), CellValues.Number, wsPart);
                                        InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).TotalCLI)), CellValues.Number, wsPart);
                                        //InsertValue("Q", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_Percentage) / 100), CellValues.Number, wsPart);
                                    }
                                }
                            }
                            index2++;
                        }
                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoTariffAutomationChillitalkGBPCurrencyMinutesSummary() : " + ex.Message);
                log.Error("DoTariffAutomationChillitalkGBPCurrencyMinutesSummary() : " + ex.Message);
            }
        }
        #endregion

        #region SendMail
        private static void SendMail(string docName, string mailSubject)
        {
            try
            {
                //Mail Sending 
                string mailContent = string.Empty;
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item));
                }
                log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mail Sending Error : " + ex.Message);
                log.Debug("Mail Sending Error : " + ex.Message);
            }
        }
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, object value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(Convert.ToString(value));
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region InsertRow
        //InsertRow(iRowIndex, wsPart);
        static void InsertRow(uint rowIndex, WorksheetPart wrksheetPart)
        {
            Worksheet worksheet = wrksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row = null;
            if (sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).Count() != 0)
            {
                Row refRow = sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).First();
                //Copy row from refRow and insert it
                row = CopyToLine(refRow, rowIndex, sheetData);
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }
        }

        //Copy an existing row and insert it
        //We don't need to copy styles of a refRow because a CloneNode() or Clone() methods do it for us
        static Row CopyToLine(Row refRow, uint rowIndex, SheetData sheetData)
        {
            uint newRowIndex;
            var newRow = (Row)refRow.CloneNode(true);
            // Loop through all the rows in the worksheet with higher row 
            // index values than the one you just added. For each one,
            // increment the existing row index.
            IEnumerable<Row> rows = sheetData.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);
            foreach (Row row in rows)
            {
                newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value + 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }
                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }

            sheetData.InsertBefore(newRow, refRow);
            return newRow;
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
