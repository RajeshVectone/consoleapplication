﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace DelightCallingTopUp
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetTariffAutomationChillitalkGBPCurrencyMinutesCountrywise
        public static List<TariffAutomationChillitalkGBPCurrencyMinutesCountrywise> GetTariffAutomationChillitalkGBPCurrencyMinutesCountrywise()
        {
            List<TariffAutomationChillitalkGBPCurrencyMinutesCountrywise> OutputList = new List<TariffAutomationChillitalkGBPCurrencyMinutesCountrywise>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection1"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_Chillitalk_GBP_Currency_Minutes_Countrywise ";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                               
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationChillitalkGBPCurrencyMinutesCountrywise()
                        {
                            calldate = r.calldate,
                            Days = r.Days,
                            currcode = r.currcode,
                            Country = r.Country,
                            cli = r.cli,
                            talktime = r.talktime
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTariffAutomationChillitalkGBPCurrencyMinutesCountrywise() : " + ex.Message);
                log.Error("GetTariffAutomationChillitalkGBPCurrencyMinutesCountrywise() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetTariffAutomationChillitalkGBPCurrencyMinutesSummary
        public static List<TariffAutomationChillitalkGBPCurrencyMinutesSummary> GetTariffAutomationChillitalkGBPCurrencyMinutesSummary()
        {
            List<TariffAutomationChillitalkGBPCurrencyMinutesSummary> OutputList = new List<TariffAutomationChillitalkGBPCurrencyMinutesSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection1"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_Chillitalk_GBP_Currency_Minutes_Summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationChillitalkGBPCurrencyMinutesSummary()
                        {
                            CallType = r.CallType,
                            Calldate = r.Calldate,
                            currcode = r.currcode,
                            Talktime = r.Talktime,
                            cli = r.cli,
                            Mins_Percentage = r.Mins_Percentage,
                            Total_mins = r.Total_mins,
                            TotalCLI = r.TotalCLI
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTariffAutomationChillitalkGBPCurrencyMinutesSummary() : " + ex.Message);
                log.Error("GetTariffAutomationChillitalkGBPCurrencyMinutesSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetTariffAutomationCDRCDSMinutesDifference
        public static List<TariffAutomationCDRCDSMinutesDifference> GetTariffAutomationCDRCDSMinutesDifference()
        {
            List<TariffAutomationCDRCDSMinutesDifference> OutputList = new List<TariffAutomationCDRCDSMinutesDifference>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection2"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_CDR_CDS_MREC_Minutes_Difference";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @type = 1
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationCDRCDSMinutesDifference()
                        {
                            Calldate = r.Calldate,
                            Cdr = r.Cdr,
                            Cds = r.Cds,
                            Difference = r.Difference
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTariffAutomationCDRCDSMinutesDifference() : " + ex.Message);
                log.Error("GetTariffAutomationCDRCDSMinutesDifference() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetTariffAutomationMRECMinutesDifference
        public static List<TariffAutomationMRECMinutesDifference> GetTariffAutomationMRECMinutesDifference()
        {
            List<TariffAutomationMRECMinutesDifference> OutputList = new List<TariffAutomationMRECMinutesDifference>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection2"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_CDR_CDS_MREC_Minutes_Difference";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @type = 2
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationMRECMinutesDifference()
                        {
                            Calldate = r.Calldate,
                            Sitecode = r.Sitecode,
                            Mrec = r.Mrec,
                            Country = r.Country,
                            Cdr = r.Cdr,
                            Difference = r.Difference
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTariffAutomationMRECMinutesDifference() : " + ex.Message);
                log.Error("GetTariffAutomationMRECMinutesDifference() : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}
