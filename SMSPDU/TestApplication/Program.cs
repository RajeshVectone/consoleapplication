﻿using SMSPDULib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            SMS sms = new SMS();
            sms.Direction = SMSDirection.Submited;
            sms.Flash = false;
            sms.PhoneNumber = "";
            sms.UserDataStartsWithHeader = true;
            //sms.MessageEncoding = SMS.SMSEncoding._7bit;
            //sms.Message = "Hello World!";
            sms.MessageEncoding = SMS.SMSEncoding.UCS2;
            //sms.Message = "Hi 👋 how are you?? 5"; //"hellohello"; //
            //string messagesParts;
            //Compose single sms
            //messagesParts = sms.Compose();

            sms.MessageEncoding = SMS.SMSEncoding.UCS2;
            sms.Message = "Daten-Angebot! Holen Sie sich 30GB Daten, 500 Österreich-Minuten und 500 Österreich-SMS für 9,90 €. Aufladen und *3162# wählen, um dieses Angebot zu erhalten.";

            //string message1 = "Wielki Oferta DANYCH! Korzystaj ze 100GB Danych, Nielimitowanych połączeń i smsów już za €19,90. Aby kupić pakiet doładuj konto i wybierz *3001#.";
            string[] messagesParts2 = sms.ComposeLongSMS();
            Console.ReadLine();
        }
    }
}
