﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using NLog;
using SMSPDULib;
using System.Linq;

namespace Sample1
{
    class Program
    {
        static Logger log = LogManager.GetLogger("Utility");

        //Used to check if the  
        private static bool ContainsUnicodeCharacter(string input)
        {
            const int MaxAnsiCode = 255;

            return input.Any(c => c > MaxAnsiCode);
        }

        private static Regex _regex = new Regex(@"\\u(?<Value>[a-zA-Z0-9]{4})", RegexOptions.Compiled);
        private static string DecodeEncodedNonAsciiCharacters(string value)
        {
            return _regex.Replace(value, m => ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString()
            );
        }

        static void Main(string[] args)
        {

            SMS sms2 = new SMS();
            sms2.Direction = SMSDirection.Submited;
            sms2.Flash = false;
            sms2.PhoneNumber = "";
            sms2.MessageEncoding = SMS.SMSEncoding.UCS2;
            sms2.Message = "Sehr geehrter Kunde, Wir senden Ihnen eine neue SIM-Karte um Ihre aktuelle zu aktualisieren. Um Ihre SIM zu aktualisieren, besuchen bitte https://bit.ly/2Mr9q6O";
            string[] messagesParts2 = sms2.ComposeLongSMS();
            Console.ReadLine();
        }

        private static String hexEncode(byte[] data)
        {
            String result = "";
            foreach (byte b in data)
            {
                result += b.ToString("X2");
            }
            result = result.ToLower();
            return (result);
        }
    }
}
