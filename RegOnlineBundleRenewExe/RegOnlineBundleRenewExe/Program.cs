﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;
#endregion

#region NameSpace
namespace RegOnlineBundleRenewExe
{
    public class BundleRenewExeGetMobilenoOutput
    {
        public int idx { get; set; }
        public string mobileno { get; set; }
        public string iccid { get; set; }
        public int bundleid { get; set; }
        public int? paymode { get; set; }
        public int? autostop { get; set; }
        public int? sendmsg_mode { get; set; }
        public string sms_msg { get; set; }
        public string sms_sender { get; set; }
        public int sms_sender_type { get; set; }
        public string sms_url { get; set; }
        public double? price { get; set; }
        public string payment_ref { get; set; }
        //For Airtel Process
        public int? bundle_type { get; set; }
        public string dest_prefix { get; set; }
    }

    public class RlxPaymentGetTopupInfoOutput
    {
        public string MobileNo { get; set; }
        public string Site { get; set; }
        public int? ProductID { get; set; }
        public int? Status { get; set; }
        public DateTime? LastUpdate { get; set; }
        public long? LastOrderID { get; set; }
        public string SubscriptionID { get; set; }
        public string TopupCurr { get; set; }
        public int? sms_sender_type { get; set; }
        public string sms_url { get; set; }
        public double? TopupAmount { get; set; }
        public string last6digitscc { get; set; }
        public string non_vectone_no { get; set; }
    }

    public class BundleRenewExeSuccessInput
    {
        public string sitecode { get; set; }
        public string mobileno { get; set; }
        public int bundleid { get; set; }
        public string processby { get; set; }
        public DateTime? datenow { get; set; }
        public int paymode { get; set; }
        public string payment_ref { get; set; }
        public int payment_status { get; set; }
        public string payment_description { get; set; }
        public string purchased_ccno { get; set; }
        public double? price { get; set; }
    }

    public class RlxAutoRenewSubscribtionIdOutput
    {
        public string Account_Id { get; set; }
        public string SubscriptionID { get; set; }
        public string purchasedate { get; set; }
        public string expirydate { get; set; }
        public string TopupCurr { get; set; }
        public string last6digitscc { get; set; }
        public int payment_type { get; set; }
        public string email { get; set; }
    }

    public class Output
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string bundle_call { get; set; }
        public string bundle_sms { get; set; }
        public string bundle_data { get; set; }
        public string bundle_name { get; set; }
        public string renewaldate { get; set; }
        public string bundle_nat_call { get; set; }
        public string bundle_inter_call { get; set; }
        public string bundle_nat_inter_call { get; set; }
        public string bundle_v2v_min { get; set; }
    }

    //public class PaymentReply
    //{
    //    public double Amount
    //    {
    //        get;
    //        set;
    //    }

    //    public string AuthorizationCode
    //    {
    //        get;
    //        set;
    //    }

    //    public string Currency
    //    {
    //        get;
    //        set;
    //    }

    //    public string Decision
    //    {
    //        get;
    //        set;
    //    }

    //    public int ReasonCode
    //    {
    //        get;
    //        set;
    //    }

    //    public string ReasonDescription
    //    {
    //        get;
    //        set;
    //    }

    //    public string ReconciliationID
    //    {
    //        get;
    //        set;
    //    }

    //    public string ReferenceCode
    //    {
    //        get;
    //        set;
    //    }

    //    public string RequestID
    //    {
    //        get;
    //        set;
    //    }

    //    public string RequestTime
    //    {
    //        get;
    //        set;
    //    }

    //    public string SubscriptionID
    //    {
    //        get;
    //        set;
    //    }
    //}

    public class PaymentReply
    {
        public string MerchantReferenceCode { get; set; }
        public string ReasonCode { get; set; }
        public string Description { get; set; }
        public string Decision { get; set; }
        public string AcsURL { get; set; }
        public string PaReq { get; set; }
        public string SubscriptionId { get; set; }

        /// <summary>
        /// Initializes an instance of Payment class
        /// </summary>
        public PaymentReply() { }

        /// <summary>
        /// Initialized an instance of Payment class using the specified paramaters.
        /// </summary>
        public PaymentReply(string reasonCode, string decision, string description)
        {
            ReasonCode = reasonCode;
            Decision = decision.ToUpper();
            Description = description;
        }
    }

    #region Class
    class Program
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region Main
        static void Main(string[] args)
        {
            try
            {
                //if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FailureIP"]))
                //{
                //    Ping pingSender = new Ping();
                //    int iCount = 0;
                //    while (iCount < 4)
                //    {
                //        try
                //        {
                //            PingReply reply = pingSender.Send(ConfigurationManager.AppSettings["FailureIP"]);
                //            if (reply.Status == IPStatus.Success)
                //            {
                //                if (iCount == 3)
                //                {
                //                    Console.WriteLine("Alternate Server {0} is working fine.", ConfigurationManager.AppSettings["FailureIP"]);
                //                    log.Info("Alternate Server {0} is working fine.", ConfigurationManager.AppSettings["FailureIP"]);
                //                    Environment.Exit(1000);
                //                }
                //            }
                //        }
                //        catch { }
                //        iCount++;
                //    }
                //}
                var SiteCode = ConfigurationManager.AppSettings["SITECODE"].Split(';').ToList();
                foreach (var item in SiteCode)
                {
                    DoProcess(item);
                }
            }
            catch (Exception ex)
            {
                log.Error("Main : " + ex.Message);
            }
        }
        #endregion

        #region DoProcess
        private static void DoProcess(string siteCode)
        {
            Console.WriteLine("Process Started : " + siteCode);
            log.Info("Process Started : " + siteCode);
            try
            {
                List<BundleRenewExeGetMobilenoOutput> outputList = DataAccess.GetBundleRenewExeGetMobileno(siteCode);

                //For Testing
                //List<BundleRenewExeGetMobilenoOutput> outputList = new List<BundleRenewExeGetMobilenoOutput>();
                //BundleRenewExeGetMobilenoOutput test = new BundleRenewExeGetMobilenoOutput();
                //test.mobileno = "447448840812";
                //test.payment_ref = "VMUK-EBUN010-RX-2059745";
                //test.bundleid = 1795;
                //test.price = 10.0;
                //test.bundle_type = 1;
                //test.sms_sender = "1244";
                //test.sms_url = "http://80.74.227.106/tpgpost.svc/sms";
                //test.dest_prefix = "*1795#";
                //outputList.Add(test);

                if (outputList != null && outputList.Count > 0)
                {
                    Console.WriteLine(Convert.ToString(outputList.Count) + " records found.");
                    log.Info(Convert.ToString(outputList.Count) + " records found.");
                    for (int iCount = 0; iCount < outputList.Count; iCount++)
                    {
                        log.Info("outputList : " + JsonConvert.SerializeObject(outputList[iCount]));
                        List<RlxAutoRenewSubscribtionIdOutput> outputTopup = DataAccess.GetRlxAutoRenewSubscribtionId(outputList[iCount].mobileno, outputList[iCount].payment_ref);
                        if (outputTopup != null && outputTopup.Count > 0)
                        {
                            string referenceId = DataAccess.getPaymentRef(siteCode, ConfigurationManager.AppSettings["application_code_" + siteCode].ToString(), ConfigurationManager.AppSettings["product_code_" + siteCode] + "-" + outputList[iCount].bundleid, outputTopup.ElementAt(0).payment_type);
                            if (!String.IsNullOrEmpty(referenceId))
                            {
                                if (outputTopup.ElementAt(0).payment_type == 1) //Realex
                                {
                                    string tempSiteCode = siteCode;
                                    if(siteCode == "BAU")
                                        tempSiteCode = "AT1";
                                    if(siteCode == "MBE")
                                        tempSiteCode = "BE1";
                                    if(siteCode == "MFR")
                                        tempSiteCode = "FR1";
                                    PaymentReply reply = RealexPayment(tempSiteCode, ConfigurationManager.AppSettings["application_code_" + siteCode], referenceId, ConfigurationManager.AppSettings["payment_agent_" + siteCode], outputTopup.ElementAt(0).SubscriptionID, Convert.ToString(outputList[iCount].price), outputTopup.ElementAt(0).Account_Id, ConfigurationManager.AppSettings["payment_mode_" + siteCode], ConfigurationManager.AppSettings["service_type_" + siteCode], outputTopup.ElementAt(0).TopupCurr, "", ConfigurationManager.AppSettings["country_" + siteCode], outputList[iCount].mobileno.Trim());
                                    log.Info("Realex PaymentReply : " + JsonConvert.SerializeObject(reply));
                                    if (reply != null)
                                    {
                                        BundleRenewExeSuccessInput input = new BundleRenewExeSuccessInput();
                                        input.mobileno = outputList[iCount].mobileno.Trim();
                                        input.sitecode = siteCode;
                                        input.bundleid = outputList[iCount].bundleid;
                                        input.processby = ConfigurationManager.AppSettings["PROCESSBY"];
                                        input.datenow = DateTime.Now;
                                        input.paymode = 2;
                                        input.payment_ref = reply.MerchantReferenceCode;
                                        input.payment_description = reply.Decision;
                                        input.purchased_ccno = outputTopup.ElementAt(0).last6digitscc;
                                        input.price = outputList[iCount].price;

                                        if (reply.ReasonCode == "100" || reply.ReasonCode == "0")
                                        {
                                            input.payment_status = 1;

                                            log.Info("Payment Success");
                                            DataAccess.InsertPaymentData(reply.MerchantReferenceCode, siteCode, ConfigurationManager.AppSettings["application_code_" + siteCode], ConfigurationManager.AppSettings["payment_agent_" + siteCode], ConfigurationManager.AppSettings["service_type_" + siteCode], ConfigurationManager.AppSettings["payment_mode_" + siteCode], 3, 3, outputList[iCount].mobileno.Trim(), outputTopup.ElementAt(0).last6digitscc, Convert.ToString(outputList[iCount].price), outputTopup.ElementAt(0).TopupCurr, outputTopup.ElementAt(0).SubscriptionID, ConfigurationManager.AppSettings["application_code_" + siteCode], "CS", "", "0", "", "", "Transaction Succeeded.");
                                            Output output = null;
                                            if (outputList[iCount].bundle_type != null && outputList[iCount].bundle_type == 1) // Call Airtel Service
                                            {
                                                string airtelResponse = CallAirtelService(outputList[iCount].mobileno, outputList[iCount].dest_prefix);
                                                if (airtelResponse.StartsWith("0|"))
                                                {
                                                    output = DataAccess.GetRenewalMessage(input);
                                                }
                                            }
                                            else
                                                output = DataAccess.UpdateBundleRenewExeSuccess(input);

                                            log.Info("output : " + JsonConvert.SerializeObject(output));

                                            //Send SMS
                                            if (output != null && !String.IsNullOrEmpty(outputList[iCount].sms_sender) && !String.IsNullOrEmpty(output.errmsg))
                                            {
                                                log.Info("Sending SMS");
                                                string smsMessage = output.errmsg.Replace("%price", Convert.ToString(outputList[iCount].price));
                                                Utility.SendSMS(outputList[iCount].sms_url, outputList[iCount].mobileno.Trim(), outputList[iCount].sms_sender, smsMessage);
                                                DataAccess.SMSInsertRequestLog(outputList[iCount].mobileno.Trim(), outputList[iCount].sms_url, "0", outputList[iCount].sms_sender, "1", smsMessage, siteCode, "Bundle Renewal - Online Success");
                                            }
                                            else
                                            {
                                                log.Info("Not Sending SMS");
                                            }

                                            if (!String.IsNullOrEmpty(outputTopup.ElementAt(0).email))
                                            {
                                                log.Info("Sending Email");
                                                //Send Email
                                                try
                                                {
                                                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                                                    var message = new MandrillMessage();
                                                    message.FromEmail = ConfigurationManager.AppSettings["EmailFrom"];
                                                    message.FromName = ConfigurationManager.AppSettings["EmailDisplayName"];
                                                    message.AddTo(outputTopup.ElementAt(0).email, "Vectone Customer");
                                                    message.AddGlobalMergeVars("FNAME", "Customer");
                                                    message.AddGlobalMergeVars("MOBILENUMBER", input.mobileno);
                                                    message.AddGlobalMergeVars("RENEWALDATE", output.renewaldate);
                                                    message.AddGlobalMergeVars("bundlename", output.bundle_name);
                                                    message.AddGlobalMergeVars("amount", Convert.ToString(outputList[iCount].price));
                                                    message.AddGlobalMergeVars("GB", output.bundle_data);
                                                    message.AddGlobalMergeVars("ukminutes", output.bundle_nat_call);
                                                    message.AddGlobalMergeVars("iminutes", output.bundle_inter_call);
                                                    message.AddGlobalMergeVars("ukiminutes", output.bundle_nat_inter_call);
                                                    message.AddGlobalMergeVars("SMS", output.bundle_sms);
                                                    message.AddGlobalMergeVars("v2vminutes", output.bundle_v2v_min);
                                                    //message.AddGlobalMergeVars("paymentref", referenceId);
                                                    

                                                    var result = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPTEMPLATENAMESUCCESS"]).Result;
                                                    log.Info("Response : " + JsonConvert.SerializeObject(result));
                                                }
                                                catch (Exception ex)
                                                {
                                                    log.Error("MailChimp : " + ex.Message);
                                                }
                                            }
                                            else
                                            {
                                                log.Info("Email is empty!");
                                            }
                                            #region Old Code
                                            //string mailSubject = ConfigurationManager.AppSettings["EmailSubject"];
                                            //string mailLocation;
                                            //mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/" + ConfigurationManager.AppSettings["EmailSuccess"]);
                                            //string mailContent = "";
                                            //using (StreamReader mailContent_file = new StreamReader(mailLocation))
                                            //{
                                            //    mailContent = mailContent_file.ReadToEnd();
                                            //}
                                            ////mailContent = mailContent
                                            ////.Replace("[FullName]", "")
                                            ////.Replace("[CustomerNumber]", "");
                                            //MailAddress mailFrom = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"], ConfigurationManager.AppSettings["EmailDisplayName"]);
                                            //MailAddressCollection mailTo = new MailAddressCollection();
                                            //mailTo.Add(new MailAddress("", string.Format("{0}", "")));
                                            //Utility.SendMail(true, mailFrom, mailTo, null, null, mailSubject, mailContent); 
                                            #endregion

                                        }
                                        else
                                        {
                                            input.payment_status = 2;

                                            log.Info("Payment Failed");
                                            DataAccess.InsertPaymentData(reply.MerchantReferenceCode, siteCode, ConfigurationManager.AppSettings["application_code_" + siteCode], ConfigurationManager.AppSettings["payment_agent_" + siteCode], ConfigurationManager.AppSettings["service_type_" + siteCode], ConfigurationManager.AppSettings["payment_mode_" + siteCode], 3, 3, outputList[iCount].mobileno.Trim(), outputTopup.ElementAt(0).last6digitscc, Convert.ToString(outputList[iCount].price), outputTopup.ElementAt(0).TopupCurr, outputTopup.ElementAt(0).SubscriptionID, ConfigurationManager.AppSettings["application_code_" + siteCode], "CS", "", String.IsNullOrEmpty(reply.ReasonCode) ? "-1" : reply.ReasonCode, "", "", String.IsNullOrEmpty(reply.Description) ? "Transaction Failed.," : reply.Description);

                                            //05-Mar-2019: Moorthy : Added for Unsubscribe id the reason is 'CARD REPORTED LOST/STOLEN' or 'Expiry date invalid'
                                            if (reply != null && !String.IsNullOrEmpty(reply.ReasonCode) && (reply.ReasonCode == "103" || reply.ReasonCode == "509"))
                                                DataAccess.UnsubscribeAutoTopup(outputTopup.ElementAt(0).SubscriptionID, outputTopup.ElementAt(0).last6digitscc);

                                            Output output = DataAccess.UpdateBundleRenewExeSuccess(input);

                                            if (output != null && !String.IsNullOrEmpty(outputList[iCount].sms_sender) && !String.IsNullOrEmpty(output.errmsg))
                                            {
                                                log.Info("Sending SMS");
                                                Utility.SendSMS(outputList[iCount].sms_url, outputList[iCount].mobileno.Trim(), outputList[iCount].sms_sender, output.errmsg);
                                                DataAccess.SMSInsertRequestLog(outputList[iCount].mobileno.Trim(), outputList[iCount].sms_url, "0", outputList[iCount].sms_sender, "1", output.errmsg, siteCode, "Bundle Renewal - Online Failure");
                                            }
                                            else
                                            {
                                                log.Info("Not Sending SMS");
                                            }

                                            if (!String.IsNullOrEmpty(outputTopup.ElementAt(0).email))
                                            {
                                                log.Info("Sending Email");
                                                //Send Email
                                                try
                                                {
                                                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                                                    var message = new MandrillMessage();
                                                    message.FromEmail = ConfigurationManager.AppSettings["EmailFrom_" + siteCode];
                                                    message.FromName = ConfigurationManager.AppSettings["EmailDisplayName"];
                                                    message.AddTo(outputTopup.ElementAt(0).email, "Vectone Customer");
                                                    var result = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPTEMPLATENAMEFAILURE"]).Result;
                                                    log.Info("Response : " + JsonConvert.SerializeObject(result));
                                                }
                                                catch (Exception ex)
                                                {
                                                    log.Error("MailChimp : " + ex.Message);
                                                }
                                            }
                                            else
                                            {
                                                log.Info("Email is empty!");
                                            }
                                            #region Old Code
                                            //string mailSubject = ConfigurationManager.AppSettings["EmailSubject"];
                                            //string mailLocation;
                                            //mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/" + ConfigurationManager.AppSettings["EmailFailure"]);
                                            //string mailContent = "";
                                            //using (StreamReader mailContent_file = new StreamReader(mailLocation))
                                            //{
                                            //    mailContent = mailContent_file.ReadToEnd();
                                            //}
                                            ////mailContent = mailContent
                                            ////.Replace("[FullName]", "")
                                            ////.Replace("[CustomerNumber]", "");
                                            //MailAddress mailFrom = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"], ConfigurationManager.AppSettings["EmailDisplayName"]);
                                            //MailAddressCollection mailTo = new MailAddressCollection();
                                            //mailTo.Add(new MailAddress("", string.Format("{0}", "")));
                                            //Utility.SendMail(true, mailFrom, mailTo, null, null, mailSubject, mailContent); 
                                            #endregion

                                            //TODO : Need to uncomment
                                            //if (reply != null && !String.IsNullOrEmpty(reply.ReasonCode) && (reply.ReasonCode == "509" || reply.ReasonCode == "103" || reply.ReasonCode == "520"))
                                            //    DataAccess.UnsubscribeAutoTopup(outputTopup.ElementAt(0).SubscriptionID);
                                        }
                                    }
                                }
                                else if (outputTopup.ElementAt(0).payment_type == 2) //Paypal
                                {
                                    PayPalPaymentStatus reply = PayPalPayment(referenceId, outputTopup.ElementAt(0).SubscriptionID, outputList[iCount].mobileno.Trim(), outputTopup.ElementAt(0).TopupCurr, Convert.ToString(outputList[iCount].price), "Vectone", "", siteCode);

                                    log.Info("PayPal PaymentStatus : " + JsonConvert.SerializeObject(reply));

                                    if (reply != null)
                                    {
                                        BundleRenewExeSuccessInput input = new BundleRenewExeSuccessInput();
                                        input.mobileno = outputList[iCount].mobileno.Trim();
                                        input.sitecode = siteCode;
                                        input.bundleid = outputList[iCount].bundleid;
                                        input.processby = ConfigurationManager.AppSettings["PROCESSBY"];
                                        input.datenow = DateTime.Now;
                                        input.paymode = 2;
                                        input.payment_ref = referenceId;
                                        input.payment_description = reply.Decision;
                                        input.purchased_ccno = outputTopup.ElementAt(0).last6digitscc;
                                        input.price = outputList[iCount].price;

                                        if (reply.Decision.Equals("ACCEPT"))
                                        {
                                            input.payment_status = 1;

                                            log.Info("Payment Success");
                                            //DataAccess.InsertPaymentData(referenceId, siteCode, ConfigurationManager.AppSettings["application_code_" + siteCode], ConfigurationManager.AppSettings["payment_agent_" + siteCode], ConfigurationManager.AppSettings["service_type_" + siteCode], ConfigurationManager.AppSettings["payment_mode_" + siteCode], 3, 3, outputList[iCount].mobileno, outputTopup.ElementAt(0).last6digitscc, Convert.ToString(outputList[iCount].price), outputTopup.ElementAt(0).TopupCurr, outputTopup.ElementAt(0).SubscriptionID, ConfigurationManager.AppSettings["application_code_" + siteCode], "CS", "", "0", "", "", "Transaction Succeeded.");

                                            Output output = null;
                                            if (outputList[iCount].bundle_type != null && outputList[iCount].bundle_type == 1) // Call Airtel Service
                                            {
                                                string airtelResponse = CallAirtelService(outputList[iCount].mobileno, outputList[iCount].dest_prefix);
                                                if (airtelResponse.StartsWith("0|"))
                                                {
                                                    output = DataAccess.GetRenewalMessage(input);
                                                }
                                            }
                                            else
                                                output = DataAccess.UpdateBundleRenewExeSuccess(input);

                                            //Send SMS
                                            if (output != null && !String.IsNullOrEmpty(outputList[iCount].sms_sender) && !String.IsNullOrEmpty(output.errmsg))
                                            {
                                                log.Info("Sending SMS");
                                                string smsMessage = output.errmsg.Replace("%price", Convert.ToString(outputList[iCount].price));
                                                Utility.SendSMS(outputList[iCount].sms_url, outputList[iCount].mobileno.Trim(), outputList[iCount].sms_sender, smsMessage);
                                                DataAccess.SMSInsertRequestLog(outputList[iCount].mobileno.Trim(), outputList[iCount].sms_url, "0", outputList[iCount].sms_sender, "1", smsMessage, siteCode, "Bundle Renewal - Online Success");
                                            }
                                            else
                                            {
                                                log.Info("Not Sending SMS");
                                            }

                                            if (!String.IsNullOrEmpty(outputTopup.ElementAt(0).email))
                                            {
                                                log.Info("Sending Email");
                                                //Send Email
                                                try
                                                {
                                                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                                                    var message = new MandrillMessage();
                                                    message.FromEmail = ConfigurationManager.AppSettings["EmailFrom"];
                                                    message.FromName = ConfigurationManager.AppSettings["EmailDisplayName"];
                                                    message.AddTo(outputTopup.ElementAt(0).email, "Vectone Customer");
                                                    message.AddGlobalMergeVars("MOBILENUMBER", input.mobileno);
                                                    message.AddGlobalMergeVars("firtstname", "Customer");
                                                    message.AddGlobalMergeVars("bundlename", output.bundle_name);
                                                    message.AddGlobalMergeVars("amount", Convert.ToString(outputList[iCount].price));
                                                    message.AddGlobalMergeVars("minutes", output.bundle_call);
                                                    message.AddGlobalMergeVars("SMS", output.bundle_sms);
                                                    message.AddGlobalMergeVars("data", output.bundle_data);
                                                    message.AddGlobalMergeVars("paymentref", referenceId);

                                                    var result = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPTEMPLATENAMESUCCESS"]).Result;
                                                    log.Info("Response : " + JsonConvert.SerializeObject(result));
                                                }
                                                catch (Exception ex)
                                                {
                                                    log.Error("MailChimp : " + ex.Message);
                                                }
                                            }
                                            else
                                            {
                                                log.Info("Email is empty!");
                                            }
                                            #region Old Code
                                            //string mailSubject = ConfigurationManager.AppSettings["EmailSubject"];
                                            //string mailLocation;
                                            //mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/" + ConfigurationManager.AppSettings["EmailSuccess"]);
                                            //string mailContent = "";
                                            //using (StreamReader mailContent_file = new StreamReader(mailLocation))
                                            //{
                                            //    mailContent = mailContent_file.ReadToEnd();
                                            //}
                                            ////mailContent = mailContent
                                            ////.Replace("[FullName]", "")
                                            ////.Replace("[CustomerNumber]", "");
                                            //MailAddress mailFrom = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"], ConfigurationManager.AppSettings["EmailDisplayName"]);
                                            //MailAddressCollection mailTo = new MailAddressCollection();
                                            //mailTo.Add(new MailAddress("", string.Format("{0}", "")));
                                            //Utility.SendMail(true, mailFrom, mailTo, null, null, mailSubject, mailContent); 
                                            #endregion

                                        }
                                        else
                                        {
                                            input.payment_status = 2;

                                            log.Info("Payment Failed");
                                            //DataAccess.InsertPaymentData(referenceId, siteCode, ConfigurationManager.AppSettings["application_code_" + siteCode], ConfigurationManager.AppSettings["payment_agent_" + siteCode], ConfigurationManager.AppSettings["service_type_" + siteCode], ConfigurationManager.AppSettings["payment_mode_" + siteCode], 3, 3, outputList[iCount].mobileno, outputTopup.ElementAt(0).last6digitscc, Convert.ToString(outputList[iCount].price), outputTopup.ElementAt(0).TopupCurr, outputTopup.ElementAt(0).SubscriptionID, ConfigurationManager.AppSettings["application_code_" + siteCode], "CS", "", "-1", "", "", String.IsNullOrEmpty(reply.Decision) ? "Transaction Failed.," : reply.Decision);

                                            Output output = DataAccess.UpdateBundleRenewExeSuccess(input);

                                            //Send SMS
                                            if (output != null && !String.IsNullOrEmpty(outputList[iCount].sms_sender) && !String.IsNullOrEmpty(output.errmsg))
                                            {
                                                log.Info("Sending SMS");
                                                Utility.SendSMS(outputList[iCount].sms_url, outputList[iCount].mobileno.Trim(), outputList[iCount].sms_sender, output.errmsg);
                                                DataAccess.SMSInsertRequestLog(outputList[iCount].mobileno.Trim(), outputList[iCount].sms_url, "0", outputList[iCount].sms_sender, "1", output.errmsg, siteCode, "Bundle Renewal - Online Failure");
                                            }
                                            else
                                            {
                                                log.Info("Not Sending SMS");
                                            }

                                            if (!String.IsNullOrEmpty(outputTopup.ElementAt(0).email))
                                            {
                                                log.Info("Sending Email");
                                                //Send Email
                                                try
                                                {
                                                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                                                    var message = new MandrillMessage();
                                                    message.FromEmail = ConfigurationManager.AppSettings["EmailFrom"];
                                                    message.FromName = ConfigurationManager.AppSettings["EmailDisplayName"];
                                                    message.AddTo("", "Vectone Customer");
                                                    var result = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPTEMPLATENAMEFAILURE"]).Result;
                                                    log.Info("Response : " + JsonConvert.SerializeObject(result));
                                                }
                                                catch (Exception ex)
                                                {
                                                    log.Error("MailChimp : " + ex.Message);
                                                }
                                            }
                                            else
                                            {
                                                log.Info("Email is empty!");
                                            }
                                            #region Old Code
                                            //string mailSubject = ConfigurationManager.AppSettings["EmailSubject"];
                                            //string mailLocation;
                                            //mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/" + ConfigurationManager.AppSettings["EmailFailure"]);
                                            //string mailContent = "";
                                            //using (StreamReader mailContent_file = new StreamReader(mailLocation))
                                            //{
                                            //    mailContent = mailContent_file.ReadToEnd();
                                            //}
                                            ////mailContent = mailContent
                                            ////.Replace("[FullName]", "")
                                            ////.Replace("[CustomerNumber]", "");
                                            //MailAddress mailFrom = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"], ConfigurationManager.AppSettings["EmailDisplayName"]);
                                            //MailAddressCollection mailTo = new MailAddressCollection();
                                            //mailTo.Add(new MailAddress("", string.Format("{0}", "")));
                                            //Utility.SendMail(true, mailFrom, mailTo, null, null, mailSubject, mailContent); 
                                            #endregion

                                            //TODO : Need to uncomment
                                            //DataAccess.UnsubscribeAutoTopup(outputTopup.ElementAt(0).SubscriptionID);
                                        }
                                    }
                                }
                            }
                            //TODO : Need to check with Logu
                            //else
                            //{
                            //    DataAccess.UnsubscribeAutoTopup(outputTopup.ElementAt(0).SubscriptionID);
                            //    log.Info("Unable to generate reference id");
                            //}
                        }
                        else
                        {
                            log.Info("Subscription information not found!");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Info("No records found!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error("DoProcess() : " + ex.Message);
                try
                {
                    MailAddressCollection mailTo = new MailAddressCollection();
                    mailTo.Add(new MailAddress("a.dhakshinamoorthy@vectone.com"));
                    Utility.SendMail(false, new MailAddress("noreply@vectonemobile.co.uk"), mailTo, null, null, ConfigurationManager.AppSettings["MAILSUBJECT"], ex.Message);
                }
                catch { }
            }
            finally
            {
                Console.WriteLine("Process Completed");
                log.Info("Process Completed");
            }
        }

        private static string CallAirtelService(string mobileno, string dest_number)
        {
            log.Info("CallAirtelService : {0} - {1} ", mobileno, dest_number);
            try
            {
                string processby = ConfigurationManager.AppSettings["PROCESSBY"];
                string url = ConfigurationManager.AppSettings["AIRTELSERVICE"];

                string postData = "imsi=&ussd_string=" + dest_number +
                                  "&msisdn=" + mobileno +
                                  "&fullstring=" + dest_number +
                                  "&product_type=1" +
                                  "&paymode=2" +
                                  "&processby=" + processby;

                log.Info("CallAirtelService : Request : " + postData);

                HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create(url);
                wReq.Method = "POST";
                wReq.ContentType = "application/x-www-form-urlencoded";
                wReq.ServicePoint.Expect100Continue = false;

                ASCIIEncoding enc = new ASCIIEncoding();
                byte[] data = enc.GetBytes(postData);
                wReq.ContentLength = data.Length;

                Stream newStream = wReq.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();

                HttpWebResponse smsRes = (HttpWebResponse)wReq.GetResponse();
                Stream resStream = smsRes.GetResponseStream();

                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(resStream, encode);
                string sResponse = readStream.ReadToEnd().Trim();

                log.Info("CallAirtelService : Response : " + sResponse);
                return sResponse;
            }
            catch (Exception ex)
            {
                log.Error("CallAirtelService : " + ex.Message);
            }
            return "-1";
        }
        #endregion

        #region PayPal
        public static PayPalPaymentStatus PayPalPayment(string fullReferenceCode, string subscriptionID, string mobileNo, string currency, string grandTotalAmount, string brand, string email, string sitecode)
        {
            PayPalPaymentStatus paymentStatus = new PayPalPaymentStatus()
            {
                ReferenceCode = fullReferenceCode,
                SubscriptionID = subscriptionID,
                Currency = currency,
                Amount = double.Parse(grandTotalAmount),
                Decision = "REJECT",
                ReasonCode = -1,
                RequestTime = DateTime.Now.ToString()
            };
            try
            {
                string service_type = ConfigurationManager.AppSettings["service_type_" + sitecode];
                string payment_mode = ConfigurationManager.AppSettings["payment_mode_" + sitecode];
                string country = ConfigurationManager.AppSettings["country_" + sitecode];
                string client_ip = ConfigurationManager.AppSettings["client_ip"];
                string product_code = ConfigurationManager.AppSettings["product_code_" + sitecode];
                string application_code = ConfigurationManager.AppSettings["application_code_" + sitecode];
                string payment_agent = ConfigurationManager.AppSettings["payment_agent_" + sitecode];

                string svcLocation = "/v1/autorenew";
                NameValueCollection infoCollection = new NameValueCollection();

                PPLPaymentInput input = new PPLPaymentInput();
                input.subscription_id = subscriptionID;
                input.amount = grandTotalAmount;
                input.currency = currency;
                input.country = country;
                input.referencecode = fullReferenceCode;
                input.accountid = mobileNo;
                input.sitecode = sitecode;
                input.product_code = product_code;
                input.application_code = application_code;
                input.payment_agent = payment_agent;
                input.service_type = service_type;
                input.payment_mode = payment_mode;
                input.client_ip = client_ip;
                input.email = email;
                input.account_type = "1";

                log.Info("PayPalPayment : input : " + JsonConvert.SerializeObject(input));

                PPLPaymentOutput output = CallService2(ConfigurationManager.AppSettings["PplService"], svcLocation, "Mozilla/5.0", "POST", "application/json", input);

                log.Info("PayPalPayment : output : " + JsonConvert.SerializeObject(output));

                if (output.errcode == 0)
                {
                    paymentStatus.Decision = "ACCEPT";
                    paymentStatus.ReasonCode = 100;
                    paymentStatus.ReasonDescription = "Successful transaction";
                    paymentStatus.ReconciliationID = "";
                }
            }
            catch (Exception ex)
            {
                log.Error("PayPalPayment : " + ex.Message);
            }
            return paymentStatus;
        }

        public static PPLPaymentOutput CallService2(string serviceUrl, string uriTemplate, string userAgent, string method, string contentType, PPLPaymentInput input)
        {
            PPLPaymentOutput output = new PPLPaymentOutput();
            try
            {
                string result_key = "";
                string URL = serviceUrl + uriTemplate;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.UserAgent = "vectone-mobile";
                httpWebRequest.Headers.Add("Authorization", "Basic YWRtaW5pc3RyYXRvcjpWaWNhcmFnZV8yMDE2");
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "POST";

                //21-Mar-2019 : Moorthy : Added for the Realex TLS 1.2 Update
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                using (var streamWriters = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(input, Formatting.Indented);
                    streamWriters.Write(json);
                    streamWriters.Flush();
                    streamWriters.Dispose();
                }
                HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse();
                Stream resStreams = httpResponce.GetResponseStream();
                StreamReader readers = new StreamReader(resStreams);
                result_key = readers.ReadToEnd();
                httpResponce.Close();
                readers.Close();
                readers.Dispose();
                output = JsonConvert.DeserializeObject<PPLPaymentOutput>(result_key);
                if (output == null || output.transaction_id == null || output.order_id == null)
                {
                    output.errcode = -1;
                    output.errmsg = "Failure";
                }
                else
                {
                    output.errcode = 0;
                    output.errmsg = "Success";
                }
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }
        #endregion

        #region Realex
        #region Payment
        public static PaymentReply RealexPayment(string siteCode, string applicationCode, string fullReferenceCode, string payment_agent, string subscriptionID, string grandTotalAmount, string mobileNo, string paymentMode, string serviceType, string currency, string email, string Country, string actualMobileNo)
        {
            PaymentReply reply = new PaymentReply();
            reply.MerchantReferenceCode = fullReferenceCode;
            reply.ReasonCode = "-1";
            reply.Description = "REJECT";
            reply.Decision = "REJECT";
            reply.SubscriptionId = subscriptionID;

            try
            {
                StringBuilder content = new StringBuilder()
                    .AppendFormat("site-code={0}", siteCode).AppendLine()
                    .AppendFormat("application-code={0}", applicationCode).AppendLine()
                    .AppendFormat("reference-code={0}", fullReferenceCode).AppendLine()
                    .AppendFormat("payment-agent={0}", payment_agent).AppendLine()
                    .AppendFormat("product-code={0}", mobileNo).AppendLine()
                    .AppendFormat("service-type={0}", serviceType).AppendLine()
                    .AppendFormat("payment-mode={0}", paymentMode).AppendLine()
                    .AppendFormat("account-id={0}", mobileNo).AppendLine()
                    .AppendFormat("amount={0:0.00}", grandTotalAmount).AppendLine()
                    .AppendFormat("currency={0}", currency).AppendLine()
                    .AppendFormat("subscription_id={0}", subscriptionID).AppendLine()
                    //.AppendFormat("client-ip={0}", "192.168.12.16").AppendLine() //No need to send IP - To Avoid Fraud Failure
                    .AppendFormat("email={0}", email).AppendLine()
                    .AppendFormat("actualmobileno={0}", actualMobileNo).AppendLine()
                    .AppendFormat("country={0}", Country).AppendLine();

                string svcLocation = "enrollment/receiptin";
                NameValueCollection infoCollection = new NameValueCollection();

                //21-Mar-2019 : Moorthy : Added for the Realex TLS 1.2 Update
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                HttpWebResponse res = CallService(ConfigurationManager.AppSettings["RlxService"], svcLocation, "Mozilla/5.0", "POST", "text/plain", content.ToString());
                if (res != null)
                {
                    StreamReader reader = new StreamReader(res.GetResponseStream());
                    List<string> data = new List<string>();
                    while (reader.Peek() > 0)
                        data.Add(reader.ReadLine());

                    foreach (string info in data)
                    {
                        string[] chunk;
                        if (data.IndexOf(info) == 0)
                        {
                            chunk = info.Split(":".ToCharArray(), 3);
                            infoCollection.Add("Description", chunk.Length == 3 ? chunk[2] : "Invalid data");
                        }
                        else
                        {
                            chunk = info.Split("=:".ToCharArray(), 2);
                            if (chunk.Length == 2)
                                infoCollection.Add(chunk[0], chunk[1]);
                        }
                    }
                }

                reply.MerchantReferenceCode = infoCollection["MerchantReferenceCode"] ?? string.Empty;
                reply.ReasonCode = infoCollection["ReasonCode"] ?? "-1";
                reply.Description = infoCollection["Description"] ?? "Internal Error";
                reply.Decision = (infoCollection["Decision"] ?? "ERROR").ToUpper();
                reply.AcsURL = infoCollection["AcsURL"] ?? string.Empty;
                reply.PaReq = infoCollection["PaReq"] ?? string.Empty;
                reply.SubscriptionId = infoCollection["SubscriptionId"] ?? string.Empty;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
            log.Info("Payment Reply : " + JsonConvert.SerializeObject(reply));
            return reply;
        }
        #endregion

        #region CallService
        public static HttpWebResponse CallService(string serviceUrl, string uriTemplate, string userAgent, string method, string contentType, string contentBody)
        {
            HttpWebResponse result = null;
            try
            {
                log.Info("Request : " + contentBody);

                byte[] contentByte = (new ASCIIEncoding()).GetBytes(contentBody.ToString());
                HttpWebRequest svc = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", serviceUrl, uriTemplate));
                svc.Method = method;
                svc.UserAgent = userAgent;
                svc.ContentType = contentType;
                svc.ContentLength = (long)((int)contentByte.Length);

                //21-Mar-2019 : Moorthy : Added for the Realex TLS 1.2 Update
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                if (!method.Equals("GET"))
                {
                    Stream svcContent = svc.GetRequestStream();
                    svcContent.Write(contentByte, 0, (int)contentByte.Length);
                    svcContent.Close();
                }
                result = (HttpWebResponse)svc.GetResponse();
            }
            catch (Exception ex)
            {
                log.Error("CallService : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region CallWebService
        //private static void CallWebService(string url, string postData, DispatchgetspecialbundleinfoOutput output)
        //{
        //    try
        //    {
        //        log.Info("Request : " + postData);

        //        HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create(url);
        //        wReq.Method = "POST";
        //        wReq.ContentType = "application/x-www-form-urlencoded";
        //        wReq.ServicePoint.Expect100Continue = false;

        //        ASCIIEncoding enc = new ASCIIEncoding();
        //        byte[] data = enc.GetBytes(postData);
        //        wReq.ContentLength = data.Length;

        //        Stream newStream = wReq.GetRequestStream();
        //        newStream.Write(data, 0, data.Length);
        //        newStream.Close();

        //        HttpWebResponse smsRes = (HttpWebResponse)wReq.GetResponse();
        //        Stream resStream = smsRes.GetResponseStream();

        //        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
        //        StreamReader readStream = new StreamReader(resStream, encode);
        //        string sResponse = readStream.ReadToEnd().Trim();

        //        log.Info("Response : " + sResponse);
        //        DispatchupdatespecialbundleinfoInput input = new DispatchupdatespecialbundleinfoInput();
        //        input.errcode = 0;
        //        input.errmsg = sResponse;
        //        input.sitecode = output.sitecode;
        //        input.mobileno = output.mobileno;
        //        input.iccid = output.iccid;
        //        input.freesimid = output.freesimid;
        //        DataAccess.UpdateStatus(input);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("CallWebService() : " + ex.Message);
        //    }
        //}
        #endregion
        #endregion
    }
    #endregion
}
#endregion
