﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Newtonsoft.Json;
using NLog;
#endregion

#region NameSpace
namespace RegOnlineBundleRenewExe
{
    #region DataAccess Class
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetOnlineCustomer
        public static List<BundleRenewExeGetMobilenoOutput> GetBundleRenewExeGetMobileno(string sitecode)
        {
            List<BundleRenewExeGetMobilenoOutput> outputList = new List<BundleRenewExeGetMobilenoOutput>();
            try
            {
                log.Info("GetBundleRenewExeGetMobileno : Input : " + sitecode);
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "bundle_renew_exe_get_mobileno ";
                    var result = conn.Query<BundleRenewExeGetMobilenoOutput>(
                            sp, new
                            {
                                @sitecode = sitecode
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("GetBundleRenewExeGetMobileno : result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetBundleRenewExeGetMobileno() ", ex.Message);
                Console.WriteLine("GetBundleRenewExeGetMobileno() ", ex.Message);
            }
            log.Info("GetBundleRenewExeGetMobileno : Output : " + JsonConvert.SerializeObject(outputList));
            return outputList;
        }
        #endregion

        #region GetRlxPaymentGetTopupInfo
        public static List<RlxPaymentGetTopupInfoOutput> GetRlxPaymentGetTopupInfo(string msisdn)
        {
            List<RlxPaymentGetTopupInfoOutput> outputList = new List<RlxPaymentGetTopupInfoOutput>();
            try
            {
                log.Info("GetRlxPaymentGetTopupInfo : Input : " + msisdn);
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebPayment"].ConnectionString))
                {
                    conn.Open();
                    var sp = "RlxPaymentGetTopupInfo";
                    var result = conn.Query<RlxPaymentGetTopupInfoOutput>(
                            sp, new
                            {
                                @msisdn = msisdn
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("GetRlxPaymentGetTopupInfo : result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRlxPaymentGetTopupInfo() ", ex.Message);
                Console.WriteLine("GetRlxPaymentGetTopupInfo() ", ex.Message);
            }
            log.Info("GetRlxPaymentGetTopupInfo : Output : " + JsonConvert.SerializeObject(outputList));
            return outputList;
        }
        #endregion

        #region UpdateBundleRenewExeSuccess
        public static Output UpdateBundleRenewExeSuccess(BundleRenewExeSuccessInput input)
        {
            Output output = new Output();
            try
            {
                log.Info("UpdateBundleRenewExeSuccess : Input : " + JsonConvert.SerializeObject(input));
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "bundle_renew_exe_success";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sitecode = input.sitecode,
                                @mobileno = input.mobileno,
                                @bundleid = input.bundleid,
                                @processby = input.processby,
                                @datenow = input.datenow,
                                @paymode = input.paymode,
                                @payment_ref = input.payment_ref,
                                @payment_status = input.payment_status,
                                @payment_description = input.payment_description,
                                @purchased_ccno = input.purchased_ccno,
                                @price = input.price
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("UpdateBundleRenewExeSuccess : result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        output.errcode = result.ElementAt(0).errcode;
                        output.errmsg = result.ElementAt(0).errmsg;
                        output.renewaldate = result.ElementAt(0).renewaldate;
                        output.bundle_nat_call = result.ElementAt(0).bundle_nat_call;
                        output.bundle_inter_call = result.ElementAt(0).bundle_inter_call;
                        output.bundle_nat_inter_call = result.ElementAt(0).bundle_nat_inter_call;
                        output.bundle_v2v_min = result.ElementAt(0).bundle_v2v_min;
                        output.bundle_call = result.ElementAt(0).bundle_call;
                        output.bundle_sms = result.ElementAt(0).bundle_sms;
                        output.bundle_data = result.ElementAt(0).bundle_data;
                        output.bundle_name = result.ElementAt(0).bundle_name;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "Failure";
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("UpdateBundleRenewExeSuccess() ", ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            log.Info("UpdateBundleRenewExeSuccess : Output : " + JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region getPaymentRef
        public static string getPaymentRef(string sitecode, string appcode, string productcode, int payment_type)
        {
            string referenceId = "";
            try
            {
                log.Info("getPaymentRef : Input : " + sitecode + " , " + appcode + " , " + productcode);
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Rxpaym_generate_payment_reference";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sitecode = sitecode,
                                @applicationcode = appcode,
                                @productcode = productcode,
                                @payment_type = payment_type
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("getPaymentRef : result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        referenceId = result.ElementAt(0).ref_id;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRlxPaymentGetTopupInfo() ", ex.Message);
                Console.WriteLine("GetRlxPaymentGetTopupInfo() ", ex.Message);
            }
            log.Info("getPaymentRef : Output : " + referenceId);
            return referenceId;
        }
        #endregion

        #region UnsubscribeAutoTopup
        public static void UnsubscribeAutoTopup(string SubscriptionID, string Last6digitsCC)
        {
            try
            {
                log.Info("UnsubscribeAutoTopup : Input : " + SubscriptionID + " , " + Last6digitsCC);
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    conn.Open();
                    var sp = "payment_remove_subscribtion";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @subscribtionid = SubscriptionID,
                                @Last6digitsCC = Last6digitsCC
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("UnsubscribeAutoTopup : result : " + JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception ex)
            {
                log.Error("UnsubscribeAutoTopup() ", ex.Message);
                Console.WriteLine("UnsubscribeAutoTopup() ", ex.Message);
            }
        }
        #endregion

        #region InsertPaymentData
        public static void InsertPaymentData(string reference_id, string sitecode, string productcode, string paymentagent, string servicetype, string paymentmode, int paymentstep, int paymentstatus, string accountid, string last6digitccno, string totalamount, string currency, string subscriptionid, string merchantid, string providercode, string csreasoncode, string generalerrorcode, string email, string IPpaymentagent, string ReplyMessage)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    conn.Open();
                    var sp = "rxpaym_PaymentInsert_v1";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @reference_id = reference_id,
                                @sitecode = sitecode,
                                @productcode = productcode,
                                @paymentagent = paymentagent,
                                @servicetype = servicetype,
                                @paymentmode = paymentmode,
                                @paymentstep = paymentstep,
                                @paymentstatus = paymentstatus,
                                @accountid = accountid,
                                @last6digitccno = last6digitccno,
                                @totalamount = totalamount,
                                @currency = currency,
                                @subscriptionid = subscriptionid,
                                @merchantid = merchantid,
                                @providercode = providercode,
                                @csreasoncode = csreasoncode,
                                @generalerrorcode = generalerrorcode,
                                @email = email,
                                @IPpaymentagent = IPpaymentagent,
                                @ReplyMessage = ReplyMessage
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("InsertPaymentData : result : " + JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception ex)
            {
                log.Error("rxpaym_PaymentInsert_v1() ", ex.Message);
                Console.WriteLine("rxpaym_PaymentInsert_v1() ", ex.Message);
            }
        }
        #endregion

        #region GetRlxAutoRenewSubscribtionId
        public static List<RlxAutoRenewSubscribtionIdOutput> GetRlxAutoRenewSubscribtionId(string mobileno, string payment_reference)
        {
            List<RlxAutoRenewSubscribtionIdOutput> outputList = new List<RlxAutoRenewSubscribtionIdOutput>();
            try
            {
                log.Info("GetRlxAutoRenewSubscribtionId : Input : {0},{1}", mobileno, payment_reference);
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    conn.Open();
                    var sp = "rlx_auto_renew_subscribtion_id ";
                    var result = conn.Query<RlxAutoRenewSubscribtionIdOutput>(
                            sp, new
                            {
                                @mobileno = mobileno,
                                @payment_reference = payment_reference
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("GetRlxAutoRenewSubscribtionId : result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRlxAutoRenewSubscribtionId() ", ex.Message);
                Console.WriteLine("GetRlxAutoRenewSubscribtionId() ", ex.Message);
            }
            log.Info("GetRlxAutoRenewSubscribtionId : Output : " + JsonConvert.SerializeObject(outputList));
            return outputList;
        }
        #endregion

        #region SMSInsertRequestLog
        public static void SMSInsertRequestLog(string mobileno, string sms_url, string sms_id, string sms_sender, string request_type, string sms_text, string sitecode, string scnerio )
        {
            try
            {
                log.Info("SMSInsertRequestLog : Input : " + sitecode);
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "sms_insert_request_log";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @mobileno = mobileno,
                                @sms_url = sms_url,
                                @sms_id = sms_id,
                                @sms_sender = sms_sender,
                                @request_type = request_type,
                                @called_by = "Renew EXE",
                                @logid = 0,
                                @sms_text = sms_text,
                                @sitecode = sitecode,
                                @scnerio = scnerio
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("SMSInsertRequestLog : result : " + JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception ex)
            {
                log.Error("SMSInsertRequestLog() ", ex.Message);
                Console.WriteLine("SMSInsertRequestLog() ", ex.Message);
            }
        }
        #endregion

        #region GetRenewalMessage
        public static Output GetRenewalMessage(BundleRenewExeSuccessInput input)
        {
            Output output = new Output();
            try
            {
                log.Info("GetRenewalMessage : Input : " + JsonConvert.SerializeObject(input));
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "renew_exe_get_renewal_message";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sitecode = input.sitecode,
                                @mobileno = input.mobileno,
                                @bundleid = input.bundleid,
                                @payment_ref = input.payment_ref,
                                @bundle_price = input.price,
                                @process_by = input.processby,
                                @purchased_ccno = input.purchased_ccno,
                                @payment_desc = input.payment_description
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("GetRenewalMessage : result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        output.errcode = result.ElementAt(0).errcode;
                        output.errmsg = result.ElementAt(0).errmsg;
                        output.bundle_call = result.ElementAt(0).bundle_call;
                        output.bundle_sms = result.ElementAt(0).bundle_sms;
                        output.bundle_data = result.ElementAt(0).bundle_data;
                        output.bundle_name = result.ElementAt(0).bundle_name;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "Failure";
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRenewalMessage() ", ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            log.Info("GetRenewalMessage : Output : " + JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion
    }
    #endregion
}
#endregion
