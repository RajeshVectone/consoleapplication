﻿using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Net;
using System.IO;
using PushNotificationConsole.Helper;
using Newtonsoft.Json;

namespace PushNotificationConsole.Helper
{
    public class PushNotification
    {
        public class PushNotificationOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public static PushNotificationOutput Getnotification(string deviceToken, bool isvoip = false)
        {
            Log.Debug("Push Notification Controller");
            PushNotificationOutput output = new PushNotificationOutput();
            try
            {
                    if (deviceToken == "")
                    {
                        output.errcode = -1;
                        output.errmsg = "device not found";
                        Log.Debug("device not found");
                    }
                    else
                    {
                        Log.Debug("deviceToken : " + deviceToken);

                        string p12File = "";
                        string p12FilePwd = "";

                        if (isvoip)
                        {
                            p12File = APNSHelper.voip_key_file_Delight;
                            p12FilePwd = APNSHelper.voip_key_pwd_Delight;
                        }
                        else
                        {
                            p12File = APNSHelper.message_key_file_Delight;
                            p12FilePwd = APNSHelper.message_key_pwd_Delight;
                        }

                        //if (isvoip)
                        //{
                        //    p12File = APNSHelper.voip_key_file_VectoneXtra;
                        //    p12FilePwd = APNSHelper.voip_key_pwd_VectoneXtra;
                        //}
                        //else
                        //{
                        //    p12File = APNSHelper.message_key_file_VectoneXtra;
                        //    p12FilePwd = APNSHelper.message_key_pwd_VectoneXtra;
                        //}

                        ApnsConfiguration config = null;
                        if (APNSHelper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com")
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !isvoip);
                        else
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !isvoip);

                        // Create a new broker
                        var apnsBroker = new ApnsServiceBroker(config);

                        // Wire up events
                        apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                        {
                            aggregateEx.Handle(ex =>
                            {
                                output.errcode = -1;
                                // See what kind of exception it was to further diagnose
                                if (ex is ApnsNotificationException)
                                {
                                    var notificationException = (ApnsNotificationException)ex;
                                    var apnsNotification = notificationException.Notification;
                                    var statusCode = notificationException.ErrorStatusCode;
                                    Log.Debug(ex.InnerException.ToString());
                                    output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                    Log.Debug(output.errmsg);
                                    Log.Debug(apnsNotification.ToString());
                                }
                                else
                                {
                                    output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                    Log.Debug(output.errmsg);
                                }
                                return true;
                            });
                        };

                        apnsBroker.OnNotificationSucceeded += (notification) =>
                        {
                            output.errcode = 0;
                            output.errmsg = "Notification Sent!";
                            Log.Debug(output.errmsg);
                        };

                        apnsBroker.Start();

                        //string jsonString = "{\"aps\":{\"content-available\":\"" + "1" + "\",\"name\":\"" + name + "\",\"media\":\"" + media + "\",\"UUID\":\"" + deviceToken + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"phoneno\":\"" + FromNumber + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}";
                        string jsonString = "{\"aps\":{\"content-available\":\"" + "1" + "\",\"name\":\"" + "Rate updated" + "\"}}";
                        Log.Debug("Payload : " + jsonString);
                        apnsBroker.QueueNotification(new ApnsNotification
                        {
                            DeviceToken = deviceToken,
                            Payload = JObject.Parse(jsonString)
                            //Payload = JObject.Parse("{\"aps\":{\"content-available\":\"" + "1" + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                        });
                        apnsBroker.Stop();
                    }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            Log.Debug("result : " + JsonConvert.SerializeObject(output));
            return output;
        }

        public static PushNotificationOutput AndriodGetnotification(string deviceToken, bool isvoip = false)
        {
            Log.Debug("Android Push Notification Controller");
            PushNotificationOutput output = new PushNotificationOutput();

            try
            {
                    if (deviceToken == "")
                    {
                        output.errcode = -1;
                        output.errmsg = "device not found";
                        Log.Debug("device not found");
                    }
                    else
                    {
                        Log.Debug("deviceToken : " + deviceToken);
                        
                        //FCMSendnotification(deviceToken, message);
                        FCMSendnotification(deviceToken, "Rate");
                    }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }

            return output;
        }

        public static string FCMSendnotification(string deviceId, string message)
        {

            //string serverKey = "Your Server key";
            string serverKey = ConfigurationSettings.AppSettings["AndroidServerKey"];
            string result = "-1";
            try
            {
                var webAddr = ConfigurationSettings.AppSettings["GCMURL"];
                //var webAddr = "https://fcm.googleapis.com/fcm/send";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"to\": \"" + deviceId + "\",\"data\": {\"message\": \"" + message + "\",}}";
                    //string json = "{\"registration_ids\": [\"" + deviceId + "\"],\"data\": {\"message\": \"" + message + "\"}}";
                    Log.Debug("Post Data : " + json);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            Log.Debug("result : " + result);
            return result;

        }

        #region Not Used in This Proj
        //public static string GetDeviceToken(string deviceId, bool isvoip)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["apns_provider"].ToString()))
        //        {
        //            conn.Open();
        //            var sp = "usp_getdevicetoken";
        //            var result = conn.Query<dynamic>(
        //                    sp, new
        //                    {
        //                        @DeviceID = deviceId
        //                    },
        //                    commandType: CommandType.StoredProcedure);
        //            if (result != null && result.Count() > 0)
        //            {
        //                if (isvoip)
        //                    return Convert.ToString(result.ElementAt(0).DeviceType) + "|" + result.ElementAt(0).voip_token;
        //                else
        //                    return Convert.ToString(result.ElementAt(0).DeviceType) + "|" + result.ElementAt(0).message_token;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Debug(ex.Message);
        //        Log.Debug(ex.InnerException.ToString());
        //        Log.Debug(ex.StackTrace);
        //    }
        //    return "";
        //}

        //private string GetName(string accountid)
        //{
        //    string custName = "";
        //    try
        //    {
        //        using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["unifiedringappapi"].ToString()))
        //        {
        //            var sp = "ur_ma_push_notification_get_username";

        //            conn.Open();

        //            var result = conn.Query<dynamic>(
        //             sp, new
        //             {
        //                 @mobileno = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[1],
        //                 @domain_id = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[0]
        //             },
        //             commandType: CommandType.StoredProcedure);
        //            if (result != null && result.Count() > 0 && result.ElementAt(0).errcode != null && result.ElementAt(0).errcode == 0)
        //            {
        //                custName = result.ElementAt(0).name;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Debug(ex.Message);
        //        Log.Debug(ex.InnerException.ToString());
        //        Log.Debug(ex.StackTrace);
        //    }
        //    return custName;
        //}

        //private string GetMobileNo(string accountid)
        //{
        //    string custName = "";
        //    try
        //    {
        //        using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["MVNO.CTP"].ToString()))
        //        {
        //            var sp = "ctp_get_mobileno";

        //            conn.Open();

        //            var result = conn.Query<dynamic>(
        //             sp, new
        //             {
        //                 @accountid = accountid
        //             },
        //             commandType: CommandType.StoredProcedure);
        //            if (result != null && result.Count() > 0)
        //            {
        //                custName = result.ElementAt(0).mobileno;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Debug(ex.Message);
        //        Log.Debug(ex.InnerException.ToString());
        //        Log.Debug(ex.StackTrace);
        //    }
        //    return custName;
        //}
        #endregion
    }
}