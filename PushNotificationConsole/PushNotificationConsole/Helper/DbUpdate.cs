﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;

namespace PushNotificationConsole.Helper
{
    class DbUpdate
    {
        public class RatepushnotificationexegetcustomerOutput
        {
            //public string VOIPToken { get; set; }
            public string MessageToken { get; set; }
            public int DeviceType { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public static List<RatepushnotificationexegetcustomerOutput> Ratepushnotificationexegetcustomer()
        {
            Log.Debug("Ratepushnotificationexegetcustomer");
            string Responce = string.Empty;
            List<RatepushnotificationexegetcustomerOutput> OutputList = new List<RatepushnotificationexegetcustomerOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["Pushnotificationconsole"].ToString()))
                {
                    conn.Open();
                    var sp = "RATE_PUSH_NOTIFICATION_EXE_GET_CUSTOMER";
	
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new RatepushnotificationexegetcustomerOutput()
                        {
                            MessageToken = r.MessageToken,
                            DeviceType = r.DeviceType,
                            //VOIPToken = r.VOIPToken,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                        Responce = result.ElementAt(0).errmsg;
                    }
                    else
                    {
                        RatepushnotificationexegetcustomerOutput output = new RatepushnotificationexegetcustomerOutput();
                        output.errcode = -1;
                        output.errmsg = "No Records Found";
                        OutputList.Add(output);
                    }
                }
            }
            catch (Exception ex)
            {
                RatepushnotificationexegetcustomerOutput output = new RatepushnotificationexegetcustomerOutput();
                output.errcode = -1;
                output.errmsg = ex.Message;
                OutputList.Add(output);
                Log.Debug("Catch in RATE_PUSH_NOTIFICATION_EXE_GET_CUSTOMER : " + ex.Message);
            }
            return OutputList;
        }
    }
}
