﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushNotificationConsole.Helper
{
    class Log
    {
        public static void Debug(string _log)
        {
            string LogPath = ConfigurationSettings.AppSettings["LogPath"].ToString();

            if (!System.IO.Directory.Exists(LogPath + DateTime.Now.ToString("yyyyMMdd")))
            {
                System.IO.Directory.CreateDirectory(LogPath + DateTime.Now.ToString("yyyyMMdd"));
            }
            StreamWriter sw = new StreamWriter(string.Concat(LogPath + DateTime.Now.ToString("yyyyMMdd"), "\\Log.txt"), true);
            sw.WriteLine(_log);
            //sw.WriteLine("=================================START==========================================");
            //sw.WriteLine(MapClientArr[MCCount].ToString().Trim() + "\r\n");
            //sw.WriteLine(reqresult);
            //sw.WriteLine("=================================END============================================\r\n");
            sw.Close();
        }
    }
}
