﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushNotificationConsole.Helper;

namespace PushNotificationConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Debug("==========Push Message Start==========");

            try
            {
                List<DbUpdate.RatepushnotificationexegetcustomerOutput> OutputList = new List<DbUpdate.RatepushnotificationexegetcustomerOutput>();
                OutputList = DbUpdate.Ratepushnotificationexegetcustomer();

                if (OutputList != null)
                {
                    for (int iCount = 0; iCount < OutputList.Count(); iCount++)
                    {
                        if (OutputList[iCount].errcode == 0)
                        {
                            if (OutputList[iCount].DeviceType == 1)
                            {
                                PushNotification.Getnotification(OutputList[iCount].MessageToken);
                            }
                            else if (OutputList[iCount].DeviceType == 2)
                            {
                                PushNotification.AndriodGetnotification(OutputList[iCount].MessageToken);
                            }
                            else
                            {
                                Log.Debug("Unknown Device : " + OutputList[iCount].MessageToken);
                            }
                        }
                        else
                        {
                            Log.Debug("No Messages to PUSH");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Debug("Catch in Main() : " + ex.Message);
            }

            Log.Debug("==========Push Message End==========\r\n");
        }
    }
}
