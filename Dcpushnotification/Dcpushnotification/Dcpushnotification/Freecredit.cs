﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace Dcpushnotification
{
    public class Freecredit
    {
        //private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private static readonly Logger Log = LogManager.GetLogger("Utility");

        public class UsedFreecreditOutput
        {
            public string account_id { get; set; }
            public string cli { get; set; }
            public string message { get; set; }
            public string device_type { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public class NotusedFreecreditOutput
        {
            public string accountid { get; set; }
            public string cli { get; set; }
            public string message_content { get; set; }
            public string device_type { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
            //create_date
            //Home_country	
            //assigned_balance	
            //used_balance	
            //available_balance	
        }

        public List<UsedFreecreditOutput> UsedFreecredit()
        {
            Console.WriteLine("SP used_freecredit Iniated");
            Log.Debug("SP used_freecredit Iniated");
            List<UsedFreecreditOutput> UsedFreecreditOutputList = new List<UsedFreecreditOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ESP"]))
                {
                    conn.Open();
                    var sp = "push_notif_exe_dc_used_freecredit_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("UsedFreecredit SP Output : {0}", JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        UsedFreecreditOutputList.AddRange(result.Select(r => new UsedFreecreditOutput()
                        {
                            account_id = r.account_id,
                            cli = r.cli,
                            message = r.message,
                            device_type = r.device_type,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UsedFreecreditOutput outputobj = new UsedFreecreditOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        UsedFreecreditOutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UsedFreecreditOutput outputobj = new UsedFreecreditOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                UsedFreecreditOutputList.Add(outputobj);
            }
            return UsedFreecreditOutputList;
        }


        public List<NotusedFreecreditOutput> NotusedFreecredit()
        {
            Console.WriteLine("SP not_used_freecredit Iniated");
            Log.Debug("SP not_used_freecredit Iniated");
            List<NotusedFreecreditOutput> NotusedFreecreditOutputList = new List<NotusedFreecreditOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.AppSettings["ESP"]))
                {
                    conn.Open();
                    var sp = "push_notif_exe_dc_not_used_freecredit_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("NotusedFreecredit SP Output : {0}", JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        NotusedFreecreditOutputList.AddRange(result.Select(r => new NotusedFreecreditOutput()
                        {
                            accountid = r.accountid,
                            cli = r.cli,
                            message_content = r.message_content,
                            device_type = r.device_type,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        NotusedFreecreditOutput outputobj = new NotusedFreecreditOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        NotusedFreecreditOutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                NotusedFreecreditOutput outputobj = new NotusedFreecreditOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                NotusedFreecreditOutputList.Add(outputobj);
            }
            return NotusedFreecreditOutputList;
        }

    }
}
