﻿using Newtonsoft.Json.Linq;
using NLog;
using PushSharp.Apple;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Net;
using System.IO;

namespace Dcpushnotification.Helpers
{
    public class PushNotification
    {
        //private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private static readonly Logger Log = LogManager.GetLogger("Utility");

        public class PushNotificationOutput : Output
        {

        }

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public static PushNotificationOutput Getnotification(string origin, string target, bool isvoip, string media, string orgmessage)
        {
            Log.Info("Push Notification Controller");
            PushNotificationOutput output = new PushNotificationOutput();
            try
            {
                Log.Info("Input : &sender=" + origin + "&target=" + target + "&isvoip=" + isvoip + "&media=" + media);
                //Log.Info("sender : " + origin + " & receiver : " + target);

                string ToNumber = target; //GetMobileNo(target);
                string FromNumber = origin;

                if (ToNumber != "")
                {
                    Log.Info("To mobile no : " + ToNumber);
                    //string FromNumber = origin.IndexOf("_") > -1 ? origin.Split(new string[] { "_" }, StringSplitOptions.None)[1] : origin;//GetName(origin);origin; //GetMobileNo(origin);

                    Log.Info("From mobile no : " + FromNumber);
                    //Device Token
                    String deviceToken = GetDeviceToken(ToNumber, isvoip);
                    if (deviceToken == "")
                    {
                        output.errcode = -1;
                        output.errmsg = "device not found";
                        Log.Info("device not found");
                    }
                    else
                    {
                        Log.Info("deviceToken : " + deviceToken);

                        //string name = GetName(origin);
                        string name = origin;
                        //if (origin.IndexOf("_") > -1)
                        //name = GetName(origin);//target.Split(new string[] { "_" }, StringSplitOptions.None)[1];//GetName(origin);
                        //else
                        //    name = origin;
                        //name = origin;
                        //if (name.Length == 0)
                        //    name = origin;
                        Log.Info(string.Format("Extension : {0}", name));

                        //Todo : Anees added to get Contact name
                        //FromNumber = GetName(FromNumber);
                        FromNumber = String.IsNullOrEmpty(FromNumber) ? name : FromNumber;

                        String message = orgmessage;
                        //String message = (!iscancelled) ? string.Format(APNSHelper.push_message, name) : "Cancelled";
                        //String message = string.Format(APNSHelper.push_message, origin);

                        Log.Info("message : " + message);

                        string p12File = "";
                        string p12FilePwd = "";
                        if (isvoip)
                        {
                            p12File = APNSHelper.voip_key_file;
                            p12FilePwd = APNSHelper.voip_key_pwd;
                        }
                        else
                        {
                            p12File = APNSHelper.message_key_file;
                            p12FilePwd = APNSHelper.message_key_pwd;
                        }

                        ApnsConfiguration config = null;
                        if (APNSHelper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com")
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !isvoip);
                        else
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !isvoip);

                        // Create a new broker
                        var apnsBroker = new ApnsServiceBroker(config);

                        // Wire up events
                        apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                        {
                            aggregateEx.Handle(ex =>
                            {
                                output.errcode = -1;
                                // See what kind of exception it was to further diagnose
                                if (ex is ApnsNotificationException)
                                {
                                    var notificationException = (ApnsNotificationException)ex;
                                    var apnsNotification = notificationException.Notification;
                                    var statusCode = notificationException.ErrorStatusCode;
                                    Log.Error(ex.InnerException);
                                    output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                    Log.Error(output.errmsg);
                                    Log.Error(apnsNotification);
                                }
                                else
                                {
                                    output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                    Log.Error(output.errmsg);
                                }
                                return true;
                            });
                        };

                        apnsBroker.OnNotificationSucceeded += (notification) =>
                        {
                            output.errcode = 0;
                            output.errmsg = "Notification Sent!";
                            Log.Info(output.errmsg);
                        };

                        apnsBroker.Start();

                        string jsonString = "{\"aps\":{\"content-available\":\"" + "1" + "\",\"name\":\"" + name + "\",\"media\":\"" + media + "\",\"UUID\":\"" + deviceToken + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"phoneno\":\"" + FromNumber + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}";

                        apnsBroker.QueueNotification(new ApnsNotification
                        {
                            DeviceToken = deviceToken,
                            Payload = JObject.Parse(jsonString)
                            //Payload = JObject.Parse("{\"aps\":{\"content-available\":\"" + "1" + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                        });
                        apnsBroker.Stop();
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "mobile no not found";
                    Log.Info(output.errmsg);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }


        public static PushNotificationOutput AndriodGetnotification(string origin, string target, bool isvoip, string media, string orgmessage)
        {
            Log.Info("Android Push Notification Controller");
            PushNotificationOutput output = new PushNotificationOutput();
           
                try
                {
                    Log.Info("Input : &sender=" + origin + "&target=" + target + "&isvoip=" + isvoip + "&media=" + media);
                    //Log.Info("sender : " + origin + " & receiver : " + target);


                    string ToNumber = target;
                    if (ToNumber != "")
                    {
                        Log.Info("To mobile no : " + ToNumber);
                        //Device Token
                        String deviceToken = GetDeviceToken(ToNumber, isvoip);
                        if (deviceToken == "")
                        {
                            output.errcode = -1;
                            output.errmsg = "device not found";
                            Log.Info("device not found");
                        }
                        else
                        {
                            Log.Info("deviceToken : " + deviceToken);
                            String message = orgmessage;
                            Log.Info("message : " + message);


                            string FromNumber = origin;
                            Log.Info("MobileNo : " + FromNumber);

                            FCMSendnotification(deviceToken, message);
                        }
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "mobile no not found";
                        Log.Info(output.errmsg);
                    }

                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
                return output;
        }


        private static string GetDeviceToken(string deviceId, bool isvoip)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.AppSettings["apns_provider"]))
                {
                    conn.Open();
                    var sp = "usp_getdevicetoken";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @DeviceID = deviceId
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        if (isvoip)
                            return result.ElementAt(0).voip_token;
                        else
                            return result.ElementAt(0).message_token;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return "";
        }

        private static string GetName(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    var sp = "ur_ma_push_notification_get_username";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @mobileno = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[1],
                         @domain_id = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[0]
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode != null && result.ElementAt(0).errcode == 0)
                    {
                        custName = result.ElementAt(0).name;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }

        private static string GetMobileNo(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MVNO.CTP"].ConnectionString))
                {
                    var sp = "ctp_get_mobileno";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @accountid = accountid
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        custName = result.ElementAt(0).mobileno;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }

        public static string FCMSendnotification(string deviceId, string message)
        {

            //string serverKey = "Your Server key";
            string serverKey = ConfigurationManager.AppSettings["AndroidServerKey"];
            string result = "-1";
            try
            {
                var webAddr = ConfigurationManager.AppSettings["GCMURL"];
                //var webAddr = "https://fcm.googleapis.com/fcm/send";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"to\": \"" + deviceId + "\",\"data\": {\"message\": \"" + message + "\",}}";
                    //string json = "{\"registration_ids\": [\"" + deviceId + "\"],\"data\": {\"message\": \"" + message + "\"}}";
                    Log.Debug("Post Data : {0}",json);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error occoure in FCMSendnotification : {0}", ex.Message);
                result = ex.Message;
            }
            Log.Debug("result : {0}", result);
            return result;

        }
    }
}