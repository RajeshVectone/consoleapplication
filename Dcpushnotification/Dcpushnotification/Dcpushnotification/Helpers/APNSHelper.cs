﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using NLog;
using System.Web.Hosting;
using System.IO;

namespace Dcpushnotification.Helpers
{

    public static class APNSHelper
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        #region Declarations
        public static string username = GetAppSettings("username");
        public static string password = GetAppSettings("password");
        public static string push_hostname = GetAppSettings("push_hostname");
        public static string push_port = GetAppSettings("push_port");
        public static string voip_key_file = GetPushFile(GetAppSettings("voip_key_file"));
        public static string message_key_file = GetPushFile(GetAppSettings("message_key_file"));
        public static string voip_key_pwd = GetAppSettings("voip_key_pwd");
        public static string message_key_pwd = GetAppSettings("message_key_pwd");
        public static string push_message = GetAppSettings("push_message");
        public static string push_badge = GetAppSettings("push_badge");
        public static string push_sound = GetAppSettings("push_sound");
        public static string sync_message = GetAppSettings("sync_message");
        //public static string FCMSend = FCMSendnotification(deviceId, message);
        #endregion

        

        #region Methods
        static string GetAppSettings(string key)
        {
            if (ConfigurationManager.AppSettings[key] != null)
            {
                return ConfigurationManager.AppSettings[key];
            }
            else
            {
                return "";
            }
        }

        static string GetPushFile(string file)
        {
            //return System.Web.Hosting.HostingEnvironment.MapPath(file); --this wont work in console application
            //return Directory.GetCurrentDirectory() + file;
            return file;
        }

        public static byte[] ToByteArray(this string value)
        {
            return Encoding.Default.GetBytes(value);
        }

        public static string GetAuthToken()
        {
            return Convert.ToBase64String((APNSHelper.username + ":" + APNSHelper.password).ToByteArray());
        }

        public static string ToHexString(this byte[] bytes, int length = 0)
        {
            if (bytes == null || bytes.Length <= 0)
                return "";

            var sb = new StringBuilder();

            foreach (byte b in bytes)
            {
                sb.Append(b.ToString("x2"));

                if (length > 0 && sb.Length >= length)
                    break;
            }
            return sb.ToString();
        }
        #endregion

        
    }
}