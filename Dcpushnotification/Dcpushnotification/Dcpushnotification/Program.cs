﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Newtonsoft.Json;
using Dcpushnotification.Helpers;

namespace Dcpushnotification
{
    class Program
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //private static readonly Logger Log = LogManager.GetLogger("Utility");

        static void Main(string[] args)
        {
            Log.Debug("Dcpushnotification Start");
            Console.WriteLine("Dcpushnotification Start");
            #region Declaration
            List<Freecredit.NotusedFreecreditOutput> NotusedFreecreditOutputList = new List<Freecredit.NotusedFreecreditOutput>();
            List<Freecredit.UsedFreecreditOutput> UsedFreecreditOutputList = new List<Freecredit.UsedFreecreditOutput>();
            #endregion

            try
            {
                Freecredit Freecredit = new Freecredit();

                NotusedFreecreditOutputList = Freecredit.NotusedFreecredit();
                if (NotusedFreecreditOutputList != null && NotusedFreecreditOutputList.Count != 0)
                {
                    if (ConfigurationManager.AppSettings["Process"].ToLower() == "live")
                    {
                    //Live
                        Log.Debug("Not Used Count : {0}", NotusedFreecreditOutputList.Count());
                    for (int icount = 0; icount < NotusedFreecreditOutputList.Count(); icount++)
                    {
                        pushnotif(ConfigurationManager.AppSettings["sender"], NotusedFreecreditOutputList[icount].cli, "audio", NotusedFreecreditOutputList[icount].message_content, NotusedFreecreditOutputList[icount].device_type);
                    }
                    }
                    else
                    {
                    //Test
                        //pushnotif(ConfigurationManager.AppSettings["sender"], ConfigurationManager.AppSettings["Receiver"], "audio", ConfigurationManager.AppSettings["message"], ConfigurationManager.AppSettings["device_type"]);
                        pushnotif(ConfigurationManager.AppSettings["sender"], ConfigurationManager.AppSettings["Receiver"], "audio", "Your current balance is 0. You can top up now by tapping 'Account' from your App via debit or credit card, PayPal, Amazon Pay & more.", ConfigurationManager.AppSettings["device_type"]);
                    }
                }

                UsedFreecreditOutputList = Freecredit.UsedFreecredit();
                if (UsedFreecreditOutputList != null && UsedFreecreditOutputList.Count != 0)
                {
                    if (ConfigurationManager.AppSettings["Process"].ToLower() == "live")
                    {
                        //Live
                        Log.Debug("Used Count : {0}",UsedFreecreditOutputList.Count());
                        for (int icount = 0; icount < UsedFreecreditOutputList.Count(); icount++)
                        {
                            pushnotif(ConfigurationManager.AppSettings["sender"], UsedFreecreditOutputList[icount].cli, "audio", UsedFreecreditOutputList[icount].message, UsedFreecreditOutputList[icount].device_type);
                        }
                    }
                    else
                    {
                        //Test
                        pushnotif(ConfigurationManager.AppSettings["sender"], ConfigurationManager.AppSettings["Receiver"], "audio", ConfigurationManager.AppSettings["message"], ConfigurationManager.AppSettings["device_type"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
                Log.Error("Error : {0}",ex.Message);
            }
            Log.Debug("Dcpushnotification End");
            Console.WriteLine("Dcpushnotification End");
        }


        private static void pushnotif(string sender, string receiver, string media, string orgmessage, string calledy)
        {
            bool isvoip=true;
            int @switch = calledy.ToLower() == "ios" ? 1 : 2;

            switch (@switch)
            {
                case 1:
                    PushNotification.Getnotification(sender, receiver, isvoip, media, orgmessage);
                    break;
                case 2:
                    PushNotification.AndriodGetnotification(sender, receiver, isvoip, media, orgmessage);
                    break;
            }
        }

        //public static void Log(string message)
        //{
        //    System.IO.StreamWriter sw = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory+ "\\Logs\\" + "logs.txt");
        //    try
        //    {
        //        string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, message);
        //        sw.WriteLine(logLine);
        //    }
        //    finally
        //    {
        //        sw.Close();
        //    }
        //}
    }
}
