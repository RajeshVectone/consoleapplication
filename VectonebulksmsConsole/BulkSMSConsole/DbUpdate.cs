﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;

namespace VectoneBulkSMSConsole
{
    class DbUpdate
    {
        public static string Vsupdatecampaignsmssendcount(int CampID, string msisdn)
        {
            Log.Debug("Vsupdatecampaignsmssendcount");
            Log.Debug("CampID : " + CampID + ",msisdn : " + msisdn);
            string Responce = string.Empty;
            try
            {
                using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["Vectonebulksmsapi"].ToString()))
                {
                    conn.Open();
                    var sp = "VS_update_campaign_sms_send_count_V1";
                    //var sp = "VS_update_Campaign_sms_send_count";

                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @campid = CampID,
                                @mobileno = msisdn.Remove(msisdn.Length - 1)
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        Responce = result.ElementAt(0).errmsg;
                    }
                    else
                    {
                        Responce = "No Rec found";
                    }
                }
            }
            catch (Exception ex)
            {
                Responce = ex.Message;
                Log.Debug("Catch in VS_update_Campaign_sms_send_count: " + ex.Message);
            }
            return Responce;
        }

        public static string Vsupdatesentsmscount(int CampID, int Count)
        {
            Log.Debug("Vsupdatesentsmscount");
            Log.Debug("CampID : " + CampID + ",Count : " + Count);
            string Responce = string.Empty;
            try
            {
                using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["Vectonebulksmsapi"].ToString()))
                {
                    conn.Open();
                    var sp = "VS_update_sent_smscount";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @compid = CampID,
                                @sms_sent_count = Count
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        Responce = result.ElementAt(0).errmsg;
                    }
                    else
                    {
                        Responce = "No Rec found";
                    }
                }
            }
            catch (Exception ex)
            {
                Responce = ex.Message;
                Log.Debug("Catch in VS_update_sent_smscount: " + ex.Message);
            }
            return Responce;
        }
    }
}
