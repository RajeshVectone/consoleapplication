﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using VectoneBulkSMSConsole;
using System.Text.RegularExpressions;
using System.Threading;

namespace VectoneBulkSMSConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Debug("==========Start==========");
            #region Declaration
            string SourcePath = string.Empty;
            string LogPath = string.Empty;
            string[] SMSFiles;
            string MapClient = string.Empty;
            string[] MapClientArr;
            string reqresult = string.Empty;
            int errcode = -1;
            string Source = string.Empty;
            DateTime currenttime = DateTime.Now;
            DateTime Scheduldetime = DateTime.Now;
            int CampaignId = 0;
            int _SMS_Success_Count = 0;
            #endregion

            try
            {
                //Console.WriteLine("Start");
                SourcePath = ConfigurationSettings.AppSettings["SourcePath"].ToString();
                LogPath = ConfigurationSettings.AppSettings["LogPath"].ToString();
                SMSFiles = GetFileNames(SourcePath, "*.txt");

                if (SMSFiles.Count() > 0)
                {
                    Log.Debug("File Count : " + SMSFiles.Count().ToString());
                    //Console.WriteLine("SMSFiles strt");
                    for (int iCount = 0; iCount < SMSFiles.Count(); iCount++)
                    {
                        string msisdn = string.Empty;
                        _SMS_Success_Count = 0;
                        Log.Debug("iCount : " + iCount);
                        Source = SourcePath + SMSFiles[iCount];
                        Log.Debug("File Source" + Source);
                        CampaignId = Convert.ToInt32(SMSFiles[iCount].Substring(0, SMSFiles[iCount].IndexOf("bat")));
                        //StreamReader streamReader = new StreamReader(Source, Encoding.GetEncoding("windows-1252"));
                        //MapClient = streamReader.ReadToEnd();
                        var webClient = new WebClient();
                        MapClient = webClient.DownloadString(Source);
                        byte[] bytes = Encoding.GetEncoding(1252).GetBytes(MapClient);
                        MapClient = Encoding.UTF8.GetString(bytes);

                        MapClientArr = Regex.Split(MapClient, "\r\n");

                        //string[] MapClientArr = MapClient.Split(new[] { "http" }, StringSplitOptions.None);

                        if (MapClientArr != null && MapClientArr[0].ToString().StartsWith("Timing"))
                        {
                            Scheduldetime = Convert.ToDateTime(MapClientArr[0].Replace("Timing : ", ""));
                            if (currenttime >= Scheduldetime)
                            {
                                Log.Debug("=================================START==========================================");
                                for (int MCCount = 1; MCCount < MapClientArr.Count(); MCCount++)
                                {
                                    if (MapClientArr[MCCount] != null && !string.IsNullOrEmpty(MapClientArr[MCCount].ToString().Trim()) && (MapClientArr[MCCount].ToString().Trim() != ""))
                                    {
                                        //Console.WriteLine(MapClientArr[MCCount]);
                                        try
                                        {
                                            
                                            Log.Debug(MapClientArr[MCCount].ToString().Trim() + "\r\n");
                                             
                                            reqresult = Mapclient.callMapclient(MapClientArr[MCCount].ToString().Trim());
                                            //reqresult = "Test";
                                            Log.Debug("Map Client Status : " + reqresult);

                                            #region Commented for Update full SMS result in DB -- 07Jan2019:Anees
                                            //int index = reqresult.IndexOf(':');
                                            //if (index > 0)
                                            //    errcode = Convert.ToInt16(reqresult.Substring(0, index));

                                            //if (errcode != 0)
                                            //{
                                            //    Log.Debug("=================================START==========================================");
                                            //    Log.Debug(MapClientArr[MCCount].ToString().Trim() + "\r\n");
                                            //    Log.Debug(reqresult);
                                            //    Log.Debug("=================================END============================================\r\n");
                                            //}
                                            //else
                                            //{
                                            //    msisdn += MapClientArr[MCCount].ToString().Trim().Split('&')[2].Split('=')[1] + ',';
                                            //    //reqresult += reqresult;
                                            //    ++_SMS_Success_Count;
                                            //} 
                                            #endregion

                                            msisdn += MapClientArr[MCCount].ToString().Trim().Split('&')[2].Split('=')[1] + "|" + reqresult + ',';
                                            ++_SMS_Success_Count;

                                        }
                                        catch (Exception ex)
                                        {
                                            Log.Debug("Error Caught IN inner catch : " + ex.Message + "\r\n");
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    //"Sleep for 2 seconds"
                                    Thread.Sleep(2000);
                                }
                                Log.Debug("=================================END============================================\r\n");
                                //Console.WriteLine("MoveFile strt");
                                //To Move finished file
                                Log.Debug("MoveFile : " + SMSFiles[iCount]);
                                string FileMoveStatus = FileManage.MoveFile(SMSFiles[iCount], Source);
                                Log.Debug("FileMoveStatus : " + FileMoveStatus);
                                //Console.WriteLine("MoveFile stop");

                                #region BatFileEachUpdate
                                DbUpdate.Vsupdatecampaignsmssendcount(CampaignId, msisdn);
                                ////Exec each file bat update
                                //SMSFiles = GetFileNames(SourcePath, "*batfile*.bat");
                                //if (SMSFiles != null)
                                //{
                                //    for (int batCount = 0; batCount < SMSFiles.Count(); batCount++)
                                //    {
                                //        Source = SourcePath + SMSFiles[batCount];
                                //        System.Diagnostics.Process.Start(Source);
                                //        FileManage.MoveFile(SMSFiles[batCount], Source);
                                //    }
                                //} 
                                #endregion
                            }
                            else
                            {
                                Log.Debug("Scheduled later");
                            }
                        }


                    }
                    #region BatFileFinalUpdate
                    //if (currenttime >= Scheduldetime)
                    //{
                    //    DbUpdate.Vsupdatesentsmscount(CampaignId, _SMS_Success_Count);
                    //    ////exec SP
                    //    ////SMSFiles = GetFileNames(SourcePath, Convert.ToString(CampaignId) + "batSP.bat");
                    //    //SMSFiles = GetFileNames(SourcePath, "*batSP.bat");

                    //    //if (SMSFiles != null)
                    //    //{
                    //    //    for (int batCount = 0; batCount < SMSFiles.Count(); batCount++)
                    //    //    {
                    //    //        Source = SourcePath + SMSFiles[batCount];
                    //    //        System.Diagnostics.Process.Start(Source);
                    //    //        FileManage.MoveFile(SMSFiles[batCount], Source);
                    //    //    }
                    //    //}
                    //}
                    #endregion
                }
                //Console.WriteLine("File is null");
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                Log.Debug("Error Caught IN catch : " + ex.Message);
            }
            Log.Debug("==========END==========");
        }

        private static string[] GetFileNames(string path, string filter)
        {
            string[] files = Directory.GetFiles(path, filter);
            for (int i = 0; i < files.Length; i++)
                files[i] = Path.GetFileName(files[i]);
            return files;
        }
    }
}
