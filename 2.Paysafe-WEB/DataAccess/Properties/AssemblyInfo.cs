﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Switchlab Ltd")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © Switchlab Ltd 2007")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.2.*")]
[assembly: AssemblyProduct("Switchlab.DataAccessLayer")]
[assembly: AssemblyTitle("Switchlab.DataAccessLayer")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.2.27665")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: Guid("b0623b2b-3439-42a6-8798-ed0ac72bbaa0")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
