using System;
using System.Collections.Generic;
using System.Data;

namespace Switchlab.DataAccessLayer
{
	public delegate void GenerateListFromReader<T>(IDataReader returnData, ref List<T> tempList);
}