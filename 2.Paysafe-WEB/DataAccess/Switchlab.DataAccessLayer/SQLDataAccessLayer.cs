using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Switchlab.DataAccessLayer
{
	public class SQLDataAccessLayer
	{
		private string _connectionstring;

		public string ConnectionString
		{
			get
			{
				return this._connectionstring;
			}
			set
			{
				this._connectionstring = value;
			}
		}

		public SQLDataAccessLayer()
		{
			this._connectionstring = string.Empty;
		}

		public SQLDataAccessLayer(string strConnection)
		{
			if (string.IsNullOrEmpty("strConnection"))
			{
				throw new ArgumentNullException("strConnection");
			}
			this._connectionstring = strConnection;
		}

		public void AddParamToSQLCmd(SqlCommand sqlCmd, string paramId, SqlDbType sqlType, int paramSize, ParameterDirection paramDirection, object paramvalue)
		{
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			if (paramId == string.Empty)
			{
				throw new ArgumentOutOfRangeException("paramId");
			}
			SqlParameter newSqlParam = new SqlParameter()
			{
				ParameterName = paramId,
				SqlDbType = sqlType,
				Direction = paramDirection
			};
			if (paramSize > 0)
			{
				newSqlParam.Size = paramSize;
			}
			if (paramvalue != null)
			{
				newSqlParam.Value = paramvalue;
			}
			sqlCmd.Parameters.Add(newSqlParam);
		}

		public int ExecuteNonQuery(SqlCommand sqlCmd)
		{
			int result;
			if (this._connectionstring == string.Empty)
			{
				throw new ArgumentOutOfRangeException("ConnectionString");
			}
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			SqlConnection cn = new SqlConnection(this._connectionstring);
			try
			{
				sqlCmd.Connection = cn;
				cn.Open();
				result = sqlCmd.ExecuteNonQuery();
				cn.Close();
			}
			finally
			{
				if (cn != null)
				{
					((IDisposable)cn).Dispose();
				}
			}
			return result;
		}

		public DataSet ExecuteQuery(SqlCommand sqlCmd)
		{
			if (this._connectionstring == string.Empty)
			{
				throw new ArgumentOutOfRangeException("ConnectionString");
			}
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			DataSet result = new DataSet();
			SqlConnection cn = new SqlConnection(this._connectionstring);
			try
			{
				sqlCmd.Connection = cn;
				(new SqlDataAdapter(sqlCmd)).Fill(result);
			}
			finally
			{
				if (cn != null)
				{
					((IDisposable)cn).Dispose();
				}
			}
			return result;
		}

		public void ExecuteReaderCmd<T>(SqlCommand sqlCmd, GenerateListFromReader<T> glfr, ref List<T> List)
		{
			if (this._connectionstring == string.Empty)
			{
				throw new ArgumentOutOfRangeException("ConnectionString");
			}
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			SqlConnection cn = new SqlConnection(this._connectionstring);
			try
			{
				sqlCmd.Connection = cn;
				cn.Open();
				glfr(sqlCmd.ExecuteReader(), ref List);
				cn.Close();
			}
			finally
			{
				if (cn != null)
				{
					((IDisposable)cn).Dispose();
				}
			}
		}

		public object ExecuteScalarCmd(SqlCommand sqlCmd)
		{
			if (this._connectionstring == string.Empty)
			{
				throw new ArgumentOutOfRangeException("ConnectionString");
			}
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			object result = null;
			SqlConnection cn = new SqlConnection(this._connectionstring);
			try
			{
				sqlCmd.Connection = cn;
				cn.Open();
				result = sqlCmd.ExecuteScalar();
				cn.Close();
			}
			finally
			{
				if (cn != null)
				{
					((IDisposable)cn).Dispose();
				}
			}
			return result;
		}

		public void SetCommandType(SqlCommand sqlCmd, CommandType cmdType, string cmdText)
		{
			sqlCmd.CommandType = cmdType;
			sqlCmd.CommandText = cmdText;
		}
	}
}