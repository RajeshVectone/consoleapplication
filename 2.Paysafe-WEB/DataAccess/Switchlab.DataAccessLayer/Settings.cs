using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Switchlab.DataAccessLayer
{
	[DesignerCategory("code")]
	[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
	[HelpKeyword("vs.data.DataSet")]
	[Serializable]
	[ToolboxItem(true)]
	[XmlRoot("Settings")]
	[XmlSchemaProvider("GetTypedDataSetSchema")]
	public class Settings : DataSet
	{
		private Settings.ConnectionsDataTable tableConnections;

		private System.Data.SchemaSerializationMode _schemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;

		[Browsable(false)]
		[DebuggerNonUserCode]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public Settings.ConnectionsDataTable Connections
		{
			get
			{
				return this.tableConnections;
			}
		}

		[DebuggerNonUserCode]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new DataRelationCollection Relations
		{
			get
			{
				return base.Relations;
			}
		}

		[Browsable(true)]
		[DebuggerNonUserCode]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override System.Data.SchemaSerializationMode SchemaSerializationMode
		{
			get
			{
				return this._schemaSerializationMode;
			}
			set
			{
				this._schemaSerializationMode = value;
			}
		}

		[DebuggerNonUserCode]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new DataTableCollection Tables
		{
			get
			{
				return base.Tables;
			}
		}

		[DebuggerNonUserCode]
		public Settings()
		{
			base.BeginInit();
			this.InitClass();
			CollectionChangeEventHandler schemaChangedHandler = new CollectionChangeEventHandler(this.SchemaChanged);
			base.Tables.CollectionChanged += schemaChangedHandler;
			base.Relations.CollectionChanged += schemaChangedHandler;
			base.EndInit();
		}

		[DebuggerNonUserCode]
		protected Settings(SerializationInfo info, StreamingContext context) : base(info, context, false)
		{
			if (!base.IsBinarySerialized(info, context))
			{
				string strSchema = (string)info.GetValue("XmlSchema", typeof(string));
				if (base.DetermineSchemaSerializationMode(info, context) != System.Data.SchemaSerializationMode.IncludeSchema)
				{
					base.ReadXmlSchema(new XmlTextReader(new StringReader(strSchema)));
				}
				else
				{
					DataSet ds = new DataSet();
					ds.ReadXmlSchema(new XmlTextReader(new StringReader(strSchema)));
					if (ds.Tables["Connections"] != null)
					{
						base.Tables.Add(new Settings.ConnectionsDataTable(ds.Tables["Connections"]));
					}
					base.DataSetName = ds.DataSetName;
					base.Prefix = ds.Prefix;
					base.Namespace = ds.Namespace;
					base.Locale = ds.Locale;
					base.CaseSensitive = ds.CaseSensitive;
					base.EnforceConstraints = ds.EnforceConstraints;
					base.Merge(ds, false, MissingSchemaAction.Add);
					this.InitVars();
				}
				base.GetSerializationData(info, context);
				CollectionChangeEventHandler schemaChangedHandler = new CollectionChangeEventHandler(this.SchemaChanged);
				base.Tables.CollectionChanged += schemaChangedHandler;
				this.Relations.CollectionChanged += schemaChangedHandler;
			}
			else
			{
				this.InitVars(false);
				CollectionChangeEventHandler schemaChangedHandler1 = new CollectionChangeEventHandler(this.SchemaChanged);
				this.Tables.CollectionChanged += schemaChangedHandler1;
				this.Relations.CollectionChanged += schemaChangedHandler1;
			}
		}

		[DebuggerNonUserCode]
		public override DataSet Clone()
		{
			Settings cln = (Settings)base.Clone();
			cln.InitVars();
			cln.SchemaSerializationMode = this.SchemaSerializationMode;
			return cln;
		}

		[DebuggerNonUserCode]
		protected override XmlSchema GetSchemaSerializable()
		{
			MemoryStream stream = new MemoryStream();
			base.WriteXmlSchema(new XmlTextWriter(stream, null));
			stream.Position = (long)0;
			return XmlSchema.Read(new XmlTextReader(stream), null);
		}

		[DebuggerNonUserCode]
		public static XmlSchemaComplexType GetTypedDataSetSchema(XmlSchemaSet xs)
		{
			XmlSchemaComplexType xmlSchemaComplexType;
			Settings ds = new Settings();
			XmlSchemaComplexType type = new XmlSchemaComplexType();
			XmlSchemaSequence sequence = new XmlSchemaSequence();
			XmlSchemaAny any = new XmlSchemaAny()
			{
				Namespace = ds.Namespace
			};
			sequence.Items.Add(any);
			type.Particle = sequence;
			XmlSchema dsSchema = ds.GetSchemaSerializable();
			if (xs.Contains(dsSchema.TargetNamespace))
			{
				MemoryStream s1 = new MemoryStream();
				MemoryStream s2 = new MemoryStream();
				try
				{
					XmlSchema schema = null;
					dsSchema.Write(s1);
					IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator();
					while (schemas.MoveNext())
					{
						schema = (XmlSchema)schemas.Current;
						s2.SetLength((long)0);
						schema.Write(s2);
						if (s1.Length == s2.Length)
						{
							s1.Position = (long)0;
							s2.Position = (long)0;
							while (true)
							{
								if ((s1.Position == s1.Length ? true : s1.ReadByte() != s2.ReadByte()))
								{
									break;
								}
							}
							if (s1.Position == s1.Length)
							{
								xmlSchemaComplexType = type;
								return xmlSchemaComplexType;
							}
						}
					}
				}
				finally
				{
					if (s1 != null)
					{
						s1.Close();
					}
					if (s2 != null)
					{
						s2.Close();
					}
				}
			}
			xs.Add(dsSchema);
			xmlSchemaComplexType = type;
			return xmlSchemaComplexType;
		}

		[DebuggerNonUserCode]
		private void InitClass()
		{
			base.DataSetName = "Settings";
			base.Prefix = "Switchlab.BusinessLogicLayer";
			base.Namespace = "http://Switchlab.BusinessLogicLayer/setting.xsd";
			base.EnforceConstraints = false;
			this.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			this.tableConnections = new Settings.ConnectionsDataTable();
			base.Tables.Add(this.tableConnections);
		}

		[DebuggerNonUserCode]
		protected override void InitializeDerivedDataSet()
		{
			base.BeginInit();
			this.InitClass();
			base.EndInit();
		}

		[DebuggerNonUserCode]
		internal void InitVars()
		{
			this.InitVars(true);
		}

		[DebuggerNonUserCode]
		internal void InitVars(bool initTable)
		{
			this.tableConnections = (Settings.ConnectionsDataTable)base.Tables["Connections"];
			if (initTable)
			{
				if (this.tableConnections != null)
				{
					this.tableConnections.InitVars();
				}
			}
		}

		[DebuggerNonUserCode]
		protected override void ReadXmlSerializable(XmlReader reader)
		{
			if (base.DetermineSchemaSerializationMode(reader) != System.Data.SchemaSerializationMode.IncludeSchema)
			{
				base.ReadXml(reader);
				this.InitVars();
			}
			else
			{
				this.Reset();
				DataSet ds = new DataSet();
				ds.ReadXml(reader);
				if (ds.Tables["Connections"] != null)
				{
					base.Tables.Add(new Settings.ConnectionsDataTable(ds.Tables["Connections"]));
				}
				base.DataSetName = ds.DataSetName;
				base.Prefix = ds.Prefix;
				base.Namespace = ds.Namespace;
				base.Locale = ds.Locale;
				base.CaseSensitive = ds.CaseSensitive;
				base.EnforceConstraints = ds.EnforceConstraints;
				base.Merge(ds, false, MissingSchemaAction.Add);
				this.InitVars();
			}
		}

		[DebuggerNonUserCode]
		private void SchemaChanged(object sender, CollectionChangeEventArgs e)
		{
			if (e.Action == CollectionChangeAction.Remove)
			{
				this.InitVars();
			}
		}

		[DebuggerNonUserCode]
		private bool ShouldSerializeConnections()
		{
			return false;
		}

		[DebuggerNonUserCode]
		protected override bool ShouldSerializeRelations()
		{
			return false;
		}

		[DebuggerNonUserCode]
		protected override bool ShouldSerializeTables()
		{
			return false;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
		[Serializable]
		[XmlSchemaProvider("GetTypedTableSchema")]
		public class ConnectionsDataTable : DataTable, IEnumerable
		{
			private DataColumn columnconnectionName;

			private DataColumn columnconnectionString;

			private DataColumn columnproviderName;

			[DebuggerNonUserCode]
			public DataColumn connectionNameColumn
			{
				get
				{
					return this.columnconnectionName;
				}
			}

			[DebuggerNonUserCode]
			public DataColumn connectionStringColumn
			{
				get
				{
					return this.columnconnectionString;
				}
			}

			[Browsable(false)]
			[DebuggerNonUserCode]
			public int Count
			{
				get
				{
					return base.Rows.Count;
				}
			}

			[DebuggerNonUserCode]
			public Settings.ConnectionsRow this[int index]
			{
				get
				{
					return (Settings.ConnectionsRow)base.Rows[index];
				}
			}

			[DebuggerNonUserCode]
			public DataColumn providerNameColumn
			{
				get
				{
					return this.columnproviderName;
				}
			}

			[DebuggerNonUserCode]
			public ConnectionsDataTable()
			{
				base.TableName = "Connections";
				this.BeginInit();
				this.InitClass();
				this.EndInit();
			}

			[DebuggerNonUserCode]
			internal ConnectionsDataTable(DataTable table)
			{
				base.TableName = table.TableName;
				if (table.CaseSensitive != table.DataSet.CaseSensitive)
				{
					base.CaseSensitive = table.CaseSensitive;
				}
				if (table.Locale.ToString() != table.DataSet.Locale.ToString())
				{
					base.Locale = table.Locale;
				}
				if (table.Namespace != table.DataSet.Namespace)
				{
					base.Namespace = table.Namespace;
				}
				base.Prefix = table.Prefix;
				base.MinimumCapacity = table.MinimumCapacity;
			}

			[DebuggerNonUserCode]
			protected ConnectionsDataTable(SerializationInfo info, StreamingContext context) : base(info, context)
			{
				this.InitVars();
			}

			[DebuggerNonUserCode]
			public void AddConnectionsRow(Settings.ConnectionsRow row)
			{
				base.Rows.Add(row);
			}

			[DebuggerNonUserCode]
			public Settings.ConnectionsRow AddConnectionsRow(string connectionName, string connectionString, string providerName)
			{
				Settings.ConnectionsRow rowConnectionsRow = (Settings.ConnectionsRow)base.NewRow();
				rowConnectionsRow.ItemArray = new object[] { connectionName, connectionString, providerName };
				base.Rows.Add(rowConnectionsRow);
				return rowConnectionsRow;
			}

			[DebuggerNonUserCode]
			public override DataTable Clone()
			{
				Settings.ConnectionsDataTable cln = (Settings.ConnectionsDataTable)base.Clone();
				cln.InitVars();
				return cln;
			}

			[DebuggerNonUserCode]
			protected override DataTable CreateInstance()
			{
				return new Settings.ConnectionsDataTable();
			}

			[DebuggerNonUserCode]
			public Settings.ConnectionsRow FindByconnectionName(string connectionName)
			{
				return (Settings.ConnectionsRow)base.Rows.Find(new object[] { connectionName });
			}

			[DebuggerNonUserCode]
			public virtual IEnumerator GetEnumerator()
			{
				return base.Rows.GetEnumerator();
			}

			[DebuggerNonUserCode]
			protected override Type GetRowType()
			{
				return typeof(Settings.ConnectionsRow);
			}

			[DebuggerNonUserCode]
			public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
			{
				XmlSchemaComplexType xmlSchemaComplexType;
				XmlSchemaComplexType type = new XmlSchemaComplexType();
				XmlSchemaSequence sequence = new XmlSchemaSequence();
				Settings ds = new Settings();
				XmlSchemaAny any1 = new XmlSchemaAny()
				{
					Namespace = "http://www.w3.org/2001/XMLSchema",
					MinOccurs = new decimal(0),
					MaxOccurs = new decimal(-1, -1, -1, false, 0),
					ProcessContents = XmlSchemaContentProcessing.Lax
				};
				sequence.Items.Add(any1);
				XmlSchemaAny any2 = new XmlSchemaAny()
				{
					Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1",
					MinOccurs = new decimal(1),
					ProcessContents = XmlSchemaContentProcessing.Lax
				};
				sequence.Items.Add(any2);
				XmlSchemaAttribute attribute1 = new XmlSchemaAttribute()
				{
					Name = "namespace",
					FixedValue = ds.Namespace
				};
				type.Attributes.Add(attribute1);
				XmlSchemaAttribute attribute2 = new XmlSchemaAttribute()
				{
					Name = "tableTypeName",
					FixedValue = "ConnectionsDataTable"
				};
				type.Attributes.Add(attribute2);
				type.Particle = sequence;
				XmlSchema dsSchema = ds.GetSchemaSerializable();
				if (xs.Contains(dsSchema.TargetNamespace))
				{
					MemoryStream s1 = new MemoryStream();
					MemoryStream s2 = new MemoryStream();
					try
					{
						XmlSchema schema = null;
						dsSchema.Write(s1);
						IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator();
						while (schemas.MoveNext())
						{
							schema = (XmlSchema)schemas.Current;
							s2.SetLength((long)0);
							schema.Write(s2);
							if (s1.Length == s2.Length)
							{
								s1.Position = (long)0;
								s2.Position = (long)0;
								while (true)
								{
									if ((s1.Position == s1.Length ? true : s1.ReadByte() != s2.ReadByte()))
									{
										break;
									}
								}
								if (s1.Position == s1.Length)
								{
									xmlSchemaComplexType = type;
									return xmlSchemaComplexType;
								}
							}
						}
					}
					finally
					{
						if (s1 != null)
						{
							s1.Close();
						}
						if (s2 != null)
						{
							s2.Close();
						}
					}
				}
				xs.Add(dsSchema);
				xmlSchemaComplexType = type;
				return xmlSchemaComplexType;
			}

			[DebuggerNonUserCode]
			private void InitClass()
			{
				this.columnconnectionName = new DataColumn("connectionName", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnconnectionName);
				this.columnconnectionString = new DataColumn("connectionString", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnconnectionString);
				this.columnproviderName = new DataColumn("providerName", typeof(string), null, MappingType.Element);
				base.Columns.Add(this.columnproviderName);
				ConstraintCollection constraints = base.Constraints;
				DataColumn[] dataColumnArray = new DataColumn[] { this.columnconnectionName };
				constraints.Add(new UniqueConstraint("Constraint1", dataColumnArray, true));
				this.columnconnectionName.AllowDBNull = false;
				this.columnconnectionName.Unique = true;
				this.columnconnectionString.AllowDBNull = false;
				this.columnproviderName.AllowDBNull = false;
			}

			[DebuggerNonUserCode]
			internal void InitVars()
			{
				this.columnconnectionName = base.Columns["connectionName"];
				this.columnconnectionString = base.Columns["connectionString"];
				this.columnproviderName = base.Columns["providerName"];
			}

			[DebuggerNonUserCode]
			public Settings.ConnectionsRow NewConnectionsRow()
			{
				return (Settings.ConnectionsRow)base.NewRow();
			}

			[DebuggerNonUserCode]
			protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
			{
				return new Settings.ConnectionsRow(builder);
			}

			[DebuggerNonUserCode]
			protected override void OnRowChanged(DataRowChangeEventArgs e)
			{
				base.OnRowChanged(e);
				if (this.ConnectionsRowChanged != null)
				{
					this.ConnectionsRowChanged(this, new Settings.ConnectionsRowChangeEvent((Settings.ConnectionsRow)e.Row, e.Action));
				}
			}

			[DebuggerNonUserCode]
			protected override void OnRowChanging(DataRowChangeEventArgs e)
			{
				base.OnRowChanging(e);
				if (this.ConnectionsRowChanging != null)
				{
					this.ConnectionsRowChanging(this, new Settings.ConnectionsRowChangeEvent((Settings.ConnectionsRow)e.Row, e.Action));
				}
			}

			[DebuggerNonUserCode]
			protected override void OnRowDeleted(DataRowChangeEventArgs e)
			{
				base.OnRowDeleted(e);
				if (this.ConnectionsRowDeleted != null)
				{
					this.ConnectionsRowDeleted(this, new Settings.ConnectionsRowChangeEvent((Settings.ConnectionsRow)e.Row, e.Action));
				}
			}

			[DebuggerNonUserCode]
			protected override void OnRowDeleting(DataRowChangeEventArgs e)
			{
				base.OnRowDeleting(e);
				if (this.ConnectionsRowDeleting != null)
				{
					this.ConnectionsRowDeleting(this, new Settings.ConnectionsRowChangeEvent((Settings.ConnectionsRow)e.Row, e.Action));
				}
			}

			[DebuggerNonUserCode]
			public void RemoveConnectionsRow(Settings.ConnectionsRow row)
			{
				base.Rows.Remove(row);
			}

			public event Settings.ConnectionsRowChangeEventHandler ConnectionsRowChanged;

			public event Settings.ConnectionsRowChangeEventHandler ConnectionsRowChanging;

			public event Settings.ConnectionsRowChangeEventHandler ConnectionsRowDeleted;

			public event Settings.ConnectionsRowChangeEventHandler ConnectionsRowDeleting;
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
		public class ConnectionsRow : DataRow
		{
			private Settings.ConnectionsDataTable tableConnections;

			[DebuggerNonUserCode]
			public string connectionName
			{
				get
				{
					return (string)base[this.tableConnections.connectionNameColumn];
				}
				set
				{
					base[this.tableConnections.connectionNameColumn] = value;
				}
			}

			[DebuggerNonUserCode]
			public string connectionString
			{
				get
				{
					return (string)base[this.tableConnections.connectionStringColumn];
				}
				set
				{
					base[this.tableConnections.connectionStringColumn] = value;
				}
			}

			[DebuggerNonUserCode]
			public string providerName
			{
				get
				{
					return (string)base[this.tableConnections.providerNameColumn];
				}
				set
				{
					base[this.tableConnections.providerNameColumn] = value;
				}
			}

			[DebuggerNonUserCode]
			internal ConnectionsRow(DataRowBuilder rb) : base(rb)
			{
				this.tableConnections = (Settings.ConnectionsDataTable)base.Table;
			}
		}

		[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
		public class ConnectionsRowChangeEvent : EventArgs
		{
			private Settings.ConnectionsRow eventRow;

			private DataRowAction eventAction;

			[DebuggerNonUserCode]
			public DataRowAction Action
			{
				get
				{
					return this.eventAction;
				}
			}

			[DebuggerNonUserCode]
			public Settings.ConnectionsRow Row
			{
				get
				{
					return this.eventRow;
				}
			}

			[DebuggerNonUserCode]
			public ConnectionsRowChangeEvent(Settings.ConnectionsRow row, DataRowAction action)
			{
				this.eventRow = row;
				this.eventAction = action;
			}
		}

		public delegate void ConnectionsRowChangeEventHandler(object sender, Settings.ConnectionsRowChangeEvent e);
	}
}