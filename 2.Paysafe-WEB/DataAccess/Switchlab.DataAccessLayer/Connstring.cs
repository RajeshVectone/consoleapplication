using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.DataAccessLayer
{
	public class Connstring
	{
		private const string SP_GET_CONNSTRING = "wsvc_get_dbprofile";

		private string _IP;

		private string _DbName;

		private string _User;

		private string _Passwd;

		private string _TimeOut;

		public string DbName
		{
			get
			{
				return this._DbName;
			}
			set
			{
				this._DbName = value;
			}
		}

		public string IP
		{
			get
			{
				return this._IP;
			}
			set
			{
				this._IP = value;
			}
		}

		public string Passwd
		{
			get
			{
				return this._Passwd;
			}
			set
			{
				this._Passwd = value;
			}
		}

		public string TimeOut
		{
			get
			{
				return this._TimeOut;
			}
			set
			{
				this._TimeOut = value;
			}
		}

		public string User
		{
			get
			{
				return this._User;
			}
			set
			{
				this._User = value;
			}
		}

		public Connstring()
		{
		}

		public Connstring(string ip, string dbName, string user, string passwd, string timeOut)
		{
			this._IP = ip;
			this._DbName = dbName;
			this._User = user;
			this._Passwd = passwd;
			this._TimeOut = timeOut;
		}

		public static void GenerateListBundlePlanFromReader<T>(IDataReader returnData, ref List<Connstring> theList)
		{
			while (returnData.Read())
			{
				Connstring newItem = new Connstring(GeneralConverter.ToString(returnData["IP"]), GeneralConverter.ToString(returnData["dbname"]), GeneralConverter.ToString(returnData["user"]), GeneralConverter.ToString(returnData["passwd"]), GeneralConverter.ToString(returnData["timeout"]));
				theList.Add(newItem);
			}
		}

		public string GetConnstring(string siteCode, string key)
		{
			string str;
			SQLDataAccessLayer DAL = new SQLDataAccessLayer(ConfigurationManager.AppSettings["WSSVCConnectionString"].ToString());
			SqlCommand sqlCmd = new SqlCommand();
			DAL.SetCommandType(sqlCmd, CommandType.StoredProcedure, "wsvc_get_dbprofile");
			DAL.AddParamToSQLCmd(sqlCmd, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, siteCode);
			DAL.AddParamToSQLCmd(sqlCmd, "@key", SqlDbType.VarChar, 32, ParameterDirection.Input, key);
			List<Connstring> referenseList = new List<Connstring>();
			try
			{
				DAL.ExecuteReaderCmd<Connstring>(sqlCmd, new GenerateListFromReader<Connstring>(Connstring.GenerateListBundlePlanFromReader<Connstring>), ref referenseList);
				string[] p = new string[] { "server=", referenseList[0].IP, ";uid=", referenseList[0].User, ";pwd=", referenseList[0].Passwd, ";database=", referenseList[0].DbName, ";Min Pool Size=5;Max Pool Size=100;Connect Timeout=", referenseList[0].TimeOut, ";" };
				str = string.Concat(p);
			}
			catch (Exception exception)
			{
				throw null;
			}
			return str;
		}
	}
}