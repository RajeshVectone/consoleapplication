using System;
using System.Data.SqlTypes;

public static class GeneralConverter
{
	public static bool ToBoolean(object oBool)
	{
		return GeneralConverter.ToBoolean(oBool, false);
	}

	public static bool ToBoolean(object oBool, bool defValue)
	{
		return (Convert.IsDBNull(oBool) ? defValue : Convert.ToBoolean(oBool));
	}

	public static DateTime ToDateTime(object oDateTime)
	{
		return GeneralConverter.ToDateTime(oDateTime, DateTime.MinValue);
	}

	public static DateTime ToDateTime(object oDateTime, DateTime defValue)
	{
		return (Convert.IsDBNull(oDateTime) ? defValue : Convert.ToDateTime(oDateTime));
	}

	public static double ToDouble(object oDouble)
	{
		return GeneralConverter.ToDouble(oDouble, 0);
	}

	public static double ToDouble(object oDouble, double defValue)
	{
		return (Convert.IsDBNull(oDouble) ? defValue : Convert.ToDouble(oDouble));
	}

	public static int ToInt32(object oInt32)
	{
		return GeneralConverter.ToInt32(oInt32, 0);
	}

	public static int ToInt32(object oInt32, int defValue)
	{
		return (Convert.IsDBNull(oInt32) ? defValue : Convert.ToInt32(oInt32));
	}

	public static long ToLong(object oLong)
	{
		return GeneralConverter.ToLong(oLong, (long)0);
	}

	public static long ToLong(object oLong, long defValue)
	{
		return (Convert.IsDBNull(oLong) ? defValue : Convert.ToInt64(oLong));
	}

	public static float ToSingle(object oSingle)
	{
		return GeneralConverter.ToSingle(oSingle, 0f);
	}

	public static float ToSingle(object oSingle, float defValue)
	{
		return (Convert.IsDBNull(oSingle) ? defValue : Convert.ToSingle(oSingle));
	}

	public static DateTime ToSqlDateTime(object oDateTime)
	{
		return GeneralConverter.ToDateTime(oDateTime, SqlDateTime.MinValue.Value);
	}

	public static string ToString(object oString)
	{
		return GeneralConverter.ToString(oString, "");
	}

	public static string ToString(object oString, string defValue)
	{
		return (Convert.IsDBNull(oString) ? defValue : Convert.ToString(oString));
	}
}