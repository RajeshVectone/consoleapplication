﻿//-----------------------------------------------------------------------------------------
// IWcfRestSql.cs
// by Tom Glick 6/13
//
// Definition of public methods for service, also contains wcf_rest_sqlcommand definition
//-----------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;


namespace Vectone.Data
{
    // dynamic_sqlcommand matches class used to create json/xml client payload.    
    public class Dynamic_SqlCommand // simple class for stored proc or cmd info passed from client
    {        
        // connect string name, from config file
        public string connect { get; set; }
     
        // for stored proc, name of stored proc, for command text of command
        public string command { get; set; }
     
        // either "text" or "storedprocedure" default "" for stored procedure
        public string commandtype { get; set; }
     
        // param names delimited for sp only, empty string for text command
        public string paramnames { get; set; }
     
        // param values delimited for sp only, empty string for text command
        public string paramvalues { get; set; }
     
        // can be "scaler", "nonquery", or empty string for normal sp/command
        public string executetype { get; set; }

        public Dynamic_SqlCommand() // public constructor
        {
            command = "";       // name of stored procedure or text sql statement
            paramvalues = "";   // empty string for text queries, delimited with | for stored procedures
            paramnames = "";    // empty string for text queries, delimited with | for stored procedures
            connect = "";       // needs to match name of connection string in web.config
            executetype = "";   // default, will assume not executescaler and not executenonquery for sql call
            commandtype = "";   // default, unless set to "text" assume stored procedure
        }
    }   
}

