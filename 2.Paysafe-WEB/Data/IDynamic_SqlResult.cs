﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Vectone.Data
{
    // IDynamicSql contains definitions for 3 service methods based on required return types   
    public interface IDynamic_SqlResult
    {
        // runSqlSpDS call stored proc, returns Data Set
        DataSet runSqlDS(Dynamic_SqlCommand cmd);

        // runSqlSpXml call stored proc, returns Xml        
        String runSqlXml(Dynamic_SqlCommand cmd);

        // runSqlSpJson call stored proc, expects Json, returns Json        
        //String runSqlJson(Dynamic_SqlCommand cmd);

        // runSqlSpObjectJson call stored proc, expects Json, returns Json        
        String runSqlObjectJson(Dynamic_SqlCommand cmd);

        // runSqlSpCommand call stored proc, expects Json, returns IEnumerable<dynamic>
        IEnumerable<dynamic> runSqlCommandList(Dynamic_SqlCommand sqlcmd);
    }
}
