﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vectone.Core.Data;

namespace Vectone.Data
{
    public class Dynamic_SqlResult : IDynamic_SqlResult
    {
        #region IDynamic_SqlResult Methods
        public DataSet runSqlDS(Dynamic_SqlCommand sqlcmd)
        {
            DataSet ds = new DataSet();

            try
            {
                if (sqlcmd.executetype.ToLower() == "" || sqlcmd.executetype.ToLower() == "execute") // not scaler or non-query
                        return runSqlCommand(sqlcmd);
                else // scaler or nonquery stored procedure
                        return runSqlCommandSingleValue(sqlcmd);
            }
            catch (SqlException sqle)
            {
                DataTable dt = new DataTable("ERROR");
                DataColumn workCol = dt.Columns.Add("Message", typeof(string));
                DataRow dr = dt.NewRow();
                dr["Message"] = sqle.Message;
                dt.Rows.Add(dr);
                ds.Tables.Add(dt);
                return ds;
            }
            catch (Exception Exc)
            {
                DataTable dt = new DataTable("ERROR");
                DataColumn workCol = dt.Columns.Add("Message", typeof(string));
                DataRow dr = dt.NewRow();
                dr["Message"] = Exc.Message;
                dt.Rows.Add(dr);
                ds.Tables.Add(dt);
                return ds;
            }
            finally
            {
            }
        }

        public string runSqlXml(Dynamic_SqlCommand sqlcmd)
        {
            return runSqlDS(sqlcmd).GetXml(); // this runs tables together -- check if client can seperate
        }

        //public string runSqlJson(Dynamic_SqlCommand sqlcmd)
        //{
        //    DataSet ds = runSqlDS(sqlcmd);  // get results of cmd into dataset
        //    string jsonstring = "";
        //    foreach (DataTable dt in ds.Tables) // seperate tables 
        //        jsonstring += dt.dataTableToJSON();
        //    return jsonstring;
        //}

        // runSqlSpObjectJson call stored proc, expects Json, returns Json        
        public String runSqlObjectJson(Dynamic_SqlCommand cmd)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<dynamic> runSqlCommandList(Dynamic_SqlCommand sqlcmd)
        {
            throw new NotImplementedException();
        }
            #endregion

        DataSet runSqlCommand(Dynamic_SqlCommand sqlcmd)  // run command, return resultset(s) in DataSet  
        {
            DataSet ds = new DataSet();   // DataSet will hold returned rows, if any
            DataTable dt = new DataTable("OUTPUT"); // used if we have output param table 
            //string connectionString = ConfigurationManager.ConnectionStrings[sqlcmd.connect].ConnectionString;
            string connectionString = DataSettings.Current.DataConnectionString;
            bool haveOutputParams = false;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlDataAdapter da = new SqlDataAdapter(sqlcmd.command, conn))
                {
                    if (sqlcmd.commandtype.ToLower() == "text") // text query
                    {
                        da.SelectCommand.CommandType = CommandType.Text;
                        da.Fill(ds);
                    }
                    else // stored proc
                    {
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand = da.SelectCommand.processSqlParameters(sqlcmd);
                        da.Fill(ds);
                        foreach (SqlParameter p in da.SelectCommand.Parameters)
                        {
                            if (p.Direction != ParameterDirection.Input) // output param
                            {
                                haveOutputParams = true;
                                DataRow dr = dt.NewRow();
                                DataColumn dc = dt.Columns.Add(p.ParameterName, typeof(string));
                                dr[p.ParameterName] = p.Value; // get value of output param
                                dt.Rows.Add(dr);
                            }
                        }
                        if (haveOutputParams)
                            ds.Tables.Add(dt); // add OUTPUT table to dataset, allows results sets + output values for client
                    }

                    conn.Close();
                }
            }
            return ds;
        }

        DataSet runSqlCommandSingleValue(Dynamic_SqlCommand sqlcmd)  // run command, return scaler or non-query int value 
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataRow dr = dt.NewRow();


            DataTable dtOutput = new DataTable("OUTPUT"); // used if we have output param table 
            DataTable dtReturnValue = new DataTable("RETURNVALUE");
            DataRow drReturnValue = dtReturnValue.NewRow();

            int returnValue;
            //string connectionString = ConfigurationManager.ConnectionStrings[sqlcmd.connect].ConnectionString;
            string connectionString = DataSettings.Current.DataConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlcmd.command, conn);
                if (sqlcmd.commandtype.ToLower() == "text")
                    cmd.CommandType = CommandType.Text;
                else // stored proc, process parameters if any
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.processSqlParameters(sqlcmd); // extension method to get params from server
                    bool haveOutputParams = false;

                    foreach (SqlParameter p in cmd.Parameters)
                    {
                        if (p.Direction != ParameterDirection.Input) // output param
                        {
                            haveOutputParams = true;
                            DataRow drOutput = dtOutput.NewRow();
                            DataColumn dcOutput = dtOutput.Columns.Add(p.ParameterName, typeof(string));
                            drOutput[p.ParameterName] = p.Value; // get value of output param
                            dtOutput.Rows.Add(drOutput);
                        }
                    }
                    if (haveOutputParams)
                        ds.Tables.Add(dtOutput); // add OUTPUT table to dataset, allows results sets + output values for client
                }
                if (sqlcmd.executetype.ToLower() == "nonquery")
                {
                    returnValue = Convert.ToInt32(cmd.ExecuteNonQuery());
                    DataColumn dc = dtReturnValue.Columns.Add("ReturnValue", typeof(string));
                    drReturnValue["ReturnValue"] = Convert.ToString(returnValue);
                    dtReturnValue.Rows.Add(drReturnValue);
                    ds.Tables.Add(dtReturnValue);
                }
                else if (sqlcmd.executetype.ToLower() == "scaler")
                {
                    object scaler = cmd.ExecuteScalar();
                    DataColumn dc = dtReturnValue.Columns.Add("Scaler", typeof(string));
                    drReturnValue["Scaler"] = Convert.ToString(scaler);
                    dtReturnValue.Rows.Add(drReturnValue);
                    ds.Tables.Add(dtReturnValue);
                }
                conn.Close();
            }
            dt.Rows.Add(dr);
            ds.Tables.Add(dt);
            return ds;
        }




        }
    
}
