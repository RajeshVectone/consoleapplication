﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Vectone.Core.Data;

namespace Vectone.Data
{
    public interface IEfDataProvider : IDataProvider
    {
        /// <summary>
        /// Get connection factory
        /// </summary>
        /// <returns>Connection factory</returns>
        IDbConnectionFactory GetConnectionFactory();

    }
}
