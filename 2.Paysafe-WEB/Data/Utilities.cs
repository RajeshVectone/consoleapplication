﻿//-----------------------------------------------------------------------------------------
// Utilities.cs
// by Tom Glick 6/13
//
// Extension methods supporting WcfRestSql service operations
//-----------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Linq;
using System.Data.SqlClient;
//using System.Web.Script.Serialization;

namespace Vectone.Data
{
    public static class Utilities // extension methods for WcfRestSql service
    {
        //public static string dataTableToJSON(this DataTable table) 
        //{
        //    // REFERENCE: http://stackoverflow.com/questions/2312982/from-datatable-in-c-sharp-net-to-json?rq=1
        //    List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
        //    foreach (DataRow row in table.Rows)
        //    {
        //        Dictionary<string, object> dict = new Dictionary<string, object>();
        //        foreach (DataColumn col in table.Columns)
        //        {
        //            dict[col.ColumnName] = row[col];
        //        }
        //        list.Add(dict);
        //    }
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    return serializer.Serialize(list);
        //}
        public static List<T> toCollection<T>(this DataTable dt) 
        {
            List<T> lst = new System.Collections.Generic.List<T>();
            Type tClass = typeof(T);
            PropertyInfo[] pClass = tClass.GetProperties();
            List<DataColumn> dc = dt.Columns.Cast<DataColumn>().ToList();
            T cn;
            foreach (DataRow item in dt.Rows)
            {
                cn = (T)Activator.CreateInstance(tClass);
                foreach (PropertyInfo pi in pClass)
                {
                    try
                    {
                        DataColumn d = dc.Find(c => c.ColumnName == pi.Name);
                        if (d != null)
                            pi.SetValue(cn, item[pi.Name], null);
                    }
                    catch
                    {
                    }
                }
                lst.Add(cn);
            }
            return lst;
        }
        public static bool isDate(this String date) // is string valid date?  
        {
            try
            {
                if (string.IsNullOrEmpty(date))
                    return false;
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool isNumeric(this String val) // is string valid numeric?  
        {
            if (string.IsNullOrEmpty(val))
                return false;
            Double result;
            return Double.TryParse(val, out result);
        }
        public static bool isBoolean(this String val) // is string valid boolean true/false?  
        {
            if (string.IsNullOrEmpty(val))
                return false;
            if (val.ToLower().Trim() == "false" || val.ToLower().Trim() == "true") // true or false, valid
                return true;
            else
                return false; // will return true even if value is false, tests for valid boolean not t/f
        }
        public static List<SqlParameter> getSqlParametersFromHttpRequest(this Dynamic_SqlCommand cmd)
        {
            // get sql parameters from delimited paramnames/paramvalues 
            // in http client payload wcf_rest_sqlcommand
            char seperator = '|'; // used to seperate parameter names/values
            List<SqlParameter> sqlparams = new List<SqlParameter>();
            if (!string.IsNullOrEmpty(cmd.paramnames) || !string.IsNullOrEmpty(cmd.paramvalues))
            {
                string[] paramnamesArray = cmd.paramnames.Split(seperator);
                string[] paramvaluesArray = cmd.paramvalues.Split(Convert.ToChar(seperator));
                if (paramnamesArray.Count() == paramvaluesArray.Count()) // need to match count
                {
                    for (int i = 0; i < paramnamesArray.Count(); i++)
                    {
                        if (!string.IsNullOrEmpty(paramnamesArray[i]) && !string.IsNullOrEmpty(paramvaluesArray[i]))
                        {
                            SqlParameter p = new SqlParameter();
                            p.ParameterName = paramnamesArray[i].Trim();
                            p.Value = paramvaluesArray[i].Trim();
                            sqlparams.Add(p);
                        }
                    }
                }
            }
            return sqlparams;
        }
        public static SqlCommand processSqlParameters(this SqlCommand cmd, Dynamic_SqlCommand sqlcmd)
        {
            // update SqlCommand with parameter info from wcf_rest_sqlcommand
            SqlCommandBuilder.DeriveParameters(cmd);
            SqlParameterCollection spc = cmd.Parameters; // params from stored proc
            List<SqlParameter> spparams = sqlcmd.getSqlParametersFromHttpRequest();

            List<SqlParameter> newParams = new List<SqlParameter>();
            if (spparams != null && spparams.Count() > 0)
            {
                foreach (SqlParameter p in spparams)
                {
                    SqlParameter newP = p; // alter new param, can't alter p during enumeration of it's collection
                    string s = p.Value.ToString();// from client, all param values will be string
                    if (!string.IsNullOrEmpty(p.ParameterName)) //&& !string.IsNullOrEmpty(p.Value.ToString()))
                    {
                        // check parameters for stored proc retrieved from sql server to set direction
                        foreach (SqlParameter checkP in spc)
                        {
                            if (p.ParameterName.ToLower() == checkP.ParameterName.ToLower())
                            {
                                //p.Direction = checkP.Direction;
                                newP.Direction = checkP.Direction;
                                switch (checkP.DbType) // create form input control based on param type
                                {
                                    case DbType.String: // it's a string, just retain value
                                        break;
                                    case DbType.DateTime: // insure valid date string
                                        if (string.IsNullOrEmpty(s) || !s.isDate()) // use extension method
                                            newP.Value = null; // s = DateTime.Now.ToString();
                                        else
                                            newP.Value = Convert.ToDateTime(s);
                                        break;
                                    case DbType.Int16:
                                    case DbType.Int32:
                                    case DbType.Int64: // insure it's numeric
                                        if (string.IsNullOrEmpty(s) || !s.isNumeric())
                                            newP.Value = null; //s = "0";
                                        else
                                            newP.Value = Convert.ToInt32(s);
                                        break;
                                    case DbType.Boolean:
                                        if (string.IsNullOrEmpty(s) || !s.isBoolean())
                                            newP.Value = false; //s = "0";
                                        else
                                            newP.Value = s.ToLower() == "true" ? true : false;
                                        break;

                                    case DbType.Double:
                                    case DbType.Decimal:
                                    case DbType.Currency: // insure numeric
                                        if (string.IsNullOrEmpty(s) || s.isNumeric())
                                            newP.Value = null; //s = "0.00";
                                        else
                                            newP.Value = Convert.ToDecimal(s);
                                        break;
                                }
                                newParams.Add(newP);
                            }
                        }
                    }
                }
            }
            cmd.Parameters.Clear();
            foreach(SqlParameter sp in newParams)
                cmd.Parameters.Add(sp);
            return cmd;
        }
    }
 }

