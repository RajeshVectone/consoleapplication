using System.Data.Entity.ModelConfiguration;
using Vectone.Core.Domain.Tasks;

namespace Vectone.Data.Mapping.Tasks
{
    public partial class ScheduleTaskMap : EntityTypeConfiguration<ScheduleTask>
    {
        public ScheduleTaskMap()
        {
            this.ToTable("ScheduleTask");
            this.HasKey(t => t.Id);
            this.Property(t => t.Name).IsRequired();
            this.Property(t => t.Type).IsRequired();
			this.Property(t => t.LastError).HasMaxLength(1000);

			this.Ignore(t => t.IsRunning);
        }
    }
}