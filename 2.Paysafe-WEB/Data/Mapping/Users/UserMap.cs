using System.Data.Entity.ModelConfiguration;
using Vectone.Core.Domain.Common;
using Vectone.Core.Domain.Users;

namespace Vectone.Data.Mapping.Users
{
    public partial class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            //this.ToTable("Users");
            this.ToTable("User");            
            //this.HasKey(c => c.company_id);
            //this.Property(u => u.Username).HasMaxLength(1000);
            //this.Property(u => u.Email).HasMaxLength(1000);

            //this.Ignore(u => u.dir_user_id);
            this.Ignore(u => u.PasswordFormat);
            //this.Ignore(u => u.Active);
            //this.Ignore(u => u.Usr_EmailID_a);
            //this.Ignore(u => u.Usr_Pwd_a);     

            //this.Ignore(u => u.UserID);  
            //this.Ignore(u => u.UserStatus);  
            //this.Ignore(u => u.UserType);  
            //this.Ignore(u => u.UserName);



            this.HasMany(c => c.UserRoles)
                .WithMany()
                .Map(m => m.ToTable("User_UserRole_Mapping"));

            ////Added on 11-04-2015 by Hari
            //this.HasMany(c => c.Resellers)
            //    .WithMany()
            //    .Map(m => m.ToTable("SBA_User_Reseller_Mapping"));
            ////end added
        }
    }
}