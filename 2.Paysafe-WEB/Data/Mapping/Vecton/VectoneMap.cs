﻿using System.Data.Entity.ModelConfiguration;
using Vectone.Core.Domain.Vectones;

namespace Vectone.Data.Mapping.Vectones
{
    public partial class VectoneMap : EntityTypeConfiguration<VectoneSite>
	{
		public VectoneMap()
		{
			this.ToTable("Vectone");
			this.HasKey(s => s.Id);
			this.Property(s => s.Name).IsRequired().HasMaxLength(400);
			this.Property(s => s.Url).IsRequired().HasMaxLength(400);
			this.Property(s => s.ContentDeliveryNetwork).HasMaxLength(400);
			this.Property(s => s.SecureUrl).HasMaxLength(400);
			this.Property(s => s.Hosts).HasMaxLength(1000);
		}
	}
}
