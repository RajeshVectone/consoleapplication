﻿using System.Data.Entity.ModelConfiguration;
using Vectone.Core.Domain.Vectones;

namespace Vectone.Data.Mapping.Vectones
{
	public partial class VectoneMappingMap : EntityTypeConfiguration<VectoneMapping>
	{
		public VectoneMappingMap()
		{
			this.ToTable("VectoneMapping");
			this.HasKey(vt => vt.Id);

			this.Property(vt => vt.EntityName).IsRequired().HasMaxLength(400);
		}
	}
}