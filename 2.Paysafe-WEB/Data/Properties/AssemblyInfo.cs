﻿using System.Reflection;
using System.Runtime.CompilerServices;
[assembly: AssemblyTitle("Vectone.Data")]
[assembly: InternalsVisibleTo("Vectone.Web")]
[assembly: InternalsVisibleTo("Vectone.Data.Tests")]
