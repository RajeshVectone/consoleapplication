﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyProduct("Vectone.NET")]
[assembly: AssemblyDescription("Vectone.NET")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vectone AG")]
[assembly: AssemblyCopyright("Copyright © Vectone AG 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
