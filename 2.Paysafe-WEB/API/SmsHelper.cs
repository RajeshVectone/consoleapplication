﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using NLog;
using Newtonsoft.Json;


namespace Vectone.WebApi.Controllers.Api
{
    public class SmsHelper
    {

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();


        private static readonly string UKOTAUrl = System.Configuration.ConfigurationManager.AppSettings["UKOTAUrl"];
        private static readonly string UKSMSUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.UK"];
        private static readonly string FROTAUrl = System.Configuration.ConfigurationManager.AppSettings["FROTAUrl"];
        private static readonly string FRSMSUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.FR"];
        private static readonly string PTOTAUrl = System.Configuration.ConfigurationManager.AppSettings["PTOTAUrl"];
        private static readonly string PTSMSUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.PT"];
        private static readonly string FIOTAUrl = System.Configuration.ConfigurationManager.AppSettings["FIOTAUrl"];
        private static readonly string FISMSUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.FI"];
        private static readonly string MMISMSUrl = System.Configuration.ConfigurationManager.AppSettings["MMISMSUrl"];
        private static readonly string ATOTAUrl = System.Configuration.ConfigurationManager.AppSettings["ATOTAUrl"];
        private static readonly string ATSmsUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.AT"];
        private static readonly string NLOTAUrl = System.Configuration.ConfigurationManager.AppSettings["NLOTAUrl"];
        private static readonly string NLSmsUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.NL"];
        private static readonly string SEOTAUrl = System.Configuration.ConfigurationManager.AppSettings["NLOTAUrl"];
        private static readonly string SESmsUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.SE"];
        private static readonly string BEOTAUrl = System.Configuration.ConfigurationManager.AppSettings["NLOTAUrl"];
        private static readonly string BESmsUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.BE"];
        private static readonly string DKSMSUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.DK"];
        private static readonly string PLSMSUrl = System.Configuration.ConfigurationManager.AppSettings["SMS.HOST.PL"];
        private static readonly string DKOTAUrl = System.Configuration.ConfigurationManager.AppSettings["DKOTAUrl"];
       
        private const string Param = "?service-name={0}&charge-code={1}&destination-addr={2}&originator-addr-type={3}&originator-addr={4}&payload-type=text&message={5}";
        private const string USSDParam = "?msisdn={0}&data={1}";

        //! user this for free sms
        public async static Task<string> SendSms(string destinationNumber, string message)
        {
            return await SendFreeSmsWithSender("447457513437", destinationNumber, message);
        }

        // Generate the Random no.. Modified on the 14032014





        //public static string GenerateRandomNumber(int PasswordLength)
        //{
        //    string _allowedChars = "0123456789";

        //    Random randNum = new Random();
        //    char[] chars = new char[PasswordLength];
        //    int allowedCharCount = _allowedChars.Length;
        //    for (int i = 0; i < PasswordLength; i++)
        //    {
        //        chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
        //    }
        //    return new string(chars);
        //}


        //public static string GenerateRandomNumber(int PasswordLength)
        //{
        //    string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";

        //    Random randNum = new Random();
        //    char[] chars = new char[PasswordLength];
        //    int allowedCharCount = _allowedChars.Length;
        //    for (int i = 0; i < PasswordLength; i++)
        //    {
        //        chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
        //    }
        //    return new string(chars);
        //}













        //! user this for free sms
        public async static Task<string> SendFreeSmsWithSender(string sender, string destinationNumber, string message)
        {
            string baseAddress = null;
            string requestUri = null;
            sender = "447457513437";
            if (sender == "447457513437")
            {
                string SMSUrl = string.Empty;
                if (destinationNumber.StartsWith("44"))
                {
                    SMSUrl = "http://10.22.1.32/sms_iwmsc ";
                    sender = "447892120000";
                }
                if (destinationNumber.StartsWith("31"))
                {
                    SMSUrl = "http://10.30.2.57/sms_iwmsc ";
                    sender = "447892120000";
                }
                 if (destinationNumber.StartsWith("43"))
                 {
                     SMSUrl = "http://10.30.2.63/sms_iwmsc";
                     sender = "436889999799";
                 }
                 if (destinationNumber.StartsWith("46"))
                 {
                     SMSUrl = "http://10.30.2.67/sms_iwmsc ";
                     sender = "447892120000";
                 }
                 if (destinationNumber.StartsWith("45"))
                 {
                     SMSUrl = "http://10.30.2.44/sms_iwmsc";
                     sender = "447892120000";
                 }
                 if (destinationNumber.StartsWith("48"))
                 {
                     SMSUrl = "http://10.30.2.74/sms_iwmsc";
                     sender = "447892120000";
                 }
                 if (destinationNumber.StartsWith("351"))
                 {
                     SMSUrl = "http://10.30.2.188/sms_iwmsc";
                     sender = "447892120000";
                 }
                 if (destinationNumber.StartsWith("358"))
                 {
                     SMSUrl = "http://10.30.2.123/sms_iwmsc";
                     sender = " 447892120000";
                 }
                 if (destinationNumber.StartsWith("33"))
                 {
                     SMSUrl = "http://10.30.2.86/sms_iwmsc";
                     sender = " 447892120000";
                 }
                 if (destinationNumber.StartsWith("32"))
                 {
                     SMSUrl = "http://10.22.2.190/sms_iwmsc";
                     sender = " 447892120000";
                 }
                 if (SMSUrl == "")
                {
                    SMSUrl = "http://10.22.1.32/sms_iwmsc ";
                    sender = "447892120000";
                }
                else
                {
                    SMSUrl = SMSUrl;
                }
                requestUri = string.Format(Param,
                    "NTFY",
                    0,
                    destinationNumber,
                    1,
                    sender,
                    Uri.EscapeDataString(message));

                return await DoSendSms(SMSUrl, requestUri);

            }
            

            requestUri = string.Format(Param,
                "NTFY",
                0,
                destinationNumber,
                5,
                sender,
                Uri.EscapeDataString(message));

            return await DoSendSms(baseAddress, requestUri);
        }

        //! use this for charge sms
        //public async static Task<string> SendSms(string servicetype, string sender, string destinationNumber, string message)
        //{
        //    //handling of CTP SMS (the sender format should be: CT|<accountID>
        //    if (sender.ToUpper().StartsWith("CT"))
        //    {

        //        var senderParts = sender.Split(new char[] { '|' });
        //        //handle senderParts[1] and the CT gateway URL
        //        var accountId = senderParts[1];

        //        ////2014-01-28: instead of using account ID, use CLI
        //        var cli = "";
        //        try
        //        {
        //            if (accountId.Length == 7 || accountId.Length == 8)
        //            {
        //                cli = DbHelper.GetMobileNoByUserId(accountId, "chillitalk").MobileNo;
        //            }
        //            else //if CLI is supplied, do nothing
        //            {
        //                cli = accountId;
        //            }
        //        }
        //        catch
        //        {
        //            cli = accountId;
        //        }

        //        //get destination Account ID
        //        string AccID = DbHelper.GetAccIDByCLI(destinationNumber);
        //        if (AccID != "" || AccID != string.Empty)
        //        {
        //            destinationNumber = AccID;
        //        }
              
        //        //if (SMSUrl == null)
        //        //{
        //        //     baseAddress = General.GetApplicationSetting(string.Format("SMS.HOST.OTHER"));
        //        //}
        //        //else
        //        //{
        //        //     baseAddress = General.GetApplicationSetting(string.Format("SMSUrl"));
        //        //}
        //        var baseAddress = General.GetApplicationSetting("SMS.HOST.OTHER");
        //        var requestUri = string.Format(Param,
        //            servicetype,
        //            1,
        //            destinationNumber,
        //            1,
        //            cli,
        //            Uri.EscapeDataString(message));

        //        return await DoSendSms(baseAddress, requestUri);
        //    }
        //    else //handling of the rest
        //    {
        //        var mobileCountry = DbHelper.GetCountryByMobileNumber(sender);
        //        var baseAddress = General.GetApplicationSetting(string.Format("SMS.HOST.{0}", mobileCountry.Country.Id)) ??
        //                          DefaultBaseAddress;

        //        if (mobileCountry.Country.Id == "GB")
        //        {
        //            var mobileCountryDestination = DbHelper.GetCountryByMobileNumber(destinationNumber);
        //            var destinationAccount = DbHelper.GetMobileAccount(destinationNumber);

        //            if (mobileCountryDestination.Country.Id != "GB" || destinationAccount == null)
        //                baseAddress = DefaultBaseAddress;
        //        }

        //        bool isVecBolo = true;

        //        //override if GCM
        //        if (DbHelper.CheckIsGCM(1, mobileCountry.MobileNo))
        //        {
        //            baseAddress = General.GetApplicationSetting("SMS.HOST.GCM");
        //            isVecBolo = false;
        //        }

        //        if (mobileCountry.MobileNo.Substring(0, 4) == "4456")
        //        {
        //            isVecBolo = false;
        //        }


        //        var requestUri = string.Format(Param,
        //            isVecBolo ? "SMS-VB" : "SMS",
        //            isVecBolo ? 45 : 1,
        //            destinationNumber,
        //            1,
        //            sender,
        //            Uri.EscapeDataString(message));

        //        return await DoSendSms(baseAddress, requestUri);
        //    }
        //}

        private static async Task<string> DoSendSms(string baseAddress, string requestUri)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);

                var content = new StringContent("");
                content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

                try
                {
                    Log.Info("{0}{1}", baseAddress, requestUri);
                    var response = await client.PostAsync(requestUri, content);
                    response.EnsureSuccessStatusCode();
                    var rspString = await response.Content.ReadAsStringAsync();
                    Log.Info("SMS success: {0}", rspString);
                    return rspString;
                }
                catch (Exception ex)
                {
                    Log.Error("SMS Error: {0}", ex.Message);
                    return string.Format("SMS Error: {0}", ex.Message);
                }
            }
        }

        //public async static Task<string> SendUSSD(string dest, string message)
        //{
        //    var mobileCountry = DbHelper.GetCountryByMobileNumber(dest);

        //    string baseAddress = null;

        //    try
        //    {
        //        baseAddress = General.GetApplicationSetting(string.Format("USSD.HOST.{0}", mobileCountry.Country.Id));
        //    }
        //    catch { /* do nothing */ }

        //    try
        //    {
        //        if (DbHelper.CheckIsGCM(1, mobileCountry.MobileNo))
        //            baseAddress = General.GetApplicationSetting("USSD.HOST.GCM");
        //    }
        //    catch { /* do nothing */ }

        //    if (baseAddress == null)
        //        baseAddress = DefaultUSSDBaseAddress;

        //    var requestUri = string.Format(USSDParam, mobileCountry.MobileNo, Uri.EscapeDataString(message));

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(baseAddress);

        //        var content = new StringContent("");
        //        content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

        //        try
        //        {
        //            Log.Info("{0}{1}", baseAddress, requestUri);
        //            var response = await client.PostAsync(requestUri, content);
        //            response.EnsureSuccessStatusCode();
        //            return await response.Content.ReadAsStringAsync();
        //        }
        //        catch (Exception ex)
        //        {
        //            Log.Error("USSD Error: {0}", ex.Message);
        //            return string.Format("USSD Error: {0}", ex.Message);
        //        }
        //    }
        //}
        
    }
}