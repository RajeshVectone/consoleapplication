﻿<?xml version="1.0"?>
<configuration>
  <configSections>
    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false"/>
    <sectionGroup name="bundleTransformer">
      <section name="core" type="BundleTransformer.Core.Configuration.CoreSettings, BundleTransformer.Core"/>
      <section name="microsoftAjax" type="BundleTransformer.MicrosoftAjax.Configuration.MicrosoftAjaxSettings, BundleTransformer.MicrosoftAjax"/>
      <section name="less" type="BundleTransformer.Less.Configuration.LessSettings, BundleTransformer.Less"/>
    </sectionGroup>
    <sectionGroup name="jsEngineSwitcher">
      <section name="core" type="JavaScriptEngineSwitcher.Core.Configuration.CoreConfiguration, JavaScriptEngineSwitcher.Core"/>
      <section name="msie" type="JavaScriptEngineSwitcher.Msie.Configuration.MsieConfiguration, JavaScriptEngineSwitcher.Msie"/>
    </sectionGroup>
    <sectionGroup name="common">
      <section name="logging" type="Common.Logging.ConfigurationSectionHandler, Common.Logging"/>
    </sectionGroup>
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog"/>
  </configSections>
  <common>
    <logging>
      <factoryAdapter type="Common.Logging.NLog.NLogLoggerFactoryAdapter, Common.Logging.NLog30">
        <arg key="configType" value="INLINE"/>
      </factoryAdapter>
    </logging>
  </common>
  <nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <targets>
      <!-- <target name="file" xsi:type="File" layout="${date:format=HH\:MM\:ss} ${logger} ${message}" fileName="${basedir}/logs/logfile.txt" /> -->
      <target name="file" xsi:type="File" layout="${longdate} ${uppercase:${level}} ${message}" fileName="${basedir}/logs/logfile.txt" archiveEvery="Day" archiveNumbering="Rolling" maxArchiveFiles="30" concurrentWrites="true"/>
    </targets>
    <rules>
      <logger name="*" minlevel="Debug" writeTo="file"/>
    </rules>
  </nlog>
  <bundleTransformer xmlns="http://tempuri.org/BundleTransformer.Configuration.xsd">
    <core>
      <css defaultMinifier="MicrosoftAjaxCssMinifier" usePreMinifiedFiles="true" disableNativeCssRelativePathTransformation="false">
        <minifiers>
          <add name="NullMinifier" type="BundleTransformer.Core.Minifiers.NullMinifier, BundleTransformer.Core"/>
          <add name="MicrosoftAjaxCssMinifier" type="BundleTransformer.MicrosoftAjax.Minifiers.MicrosoftAjaxCssMinifier, BundleTransformer.MicrosoftAjax"/>
        </minifiers>
        <translators>
          <add name="NullTranslator" type="BundleTransformer.Core.Translators.NullTranslator, BundleTransformer.Core" enabled="false"/>
          <add name="LessTranslator" type="BundleTransformer.Less.Translators.LessTranslator, BundleTransformer.Less"/>
        </translators>
      </css>
      <js defaultMinifier="MicrosoftAjaxJsMinifier" usePreMinifiedFiles="true">
        <minifiers>
          <add name="NullMinifier" type="BundleTransformer.Core.Minifiers.NullMinifier, BundleTransformer.Core"/>
          <add name="MicrosoftAjaxJsMinifier" type="BundleTransformer.MicrosoftAjax.Minifiers.MicrosoftAjaxJsMinifier, BundleTransformer.MicrosoftAjax"/>
        </minifiers>
        <translators>
          <add name="NullTranslator" type="BundleTransformer.Core.Translators.NullTranslator, BundleTransformer.Core" enabled="false"/>
        </translators>
      </js>
      <assetHandler disableServerCache="false" serverCacheDurationInMinutes="1440" disableClientCache="false"/>
    </core>
    <microsoftAjax>
      <css allowEmbeddedAspNetBlocks="false" blocksStartOnSameLine="NewLine" ignoreAllErrors="false" ignoreErrorList="" indentSize="4" lineBreakThreshold="2147482647" outputMode="SingleLine" preprocessorDefineList="" termSemicolons="false" colorNames="Strict" commentMode="Important" minifyExpressions="true" severity="0"/>
      <js allowEmbeddedAspNetBlocks="false" blocksStartOnSameLine="NewLine" ignoreAllErrors="false" ignoreErrorList="" indentSize="4" lineBreakThreshold="2147482647" outputMode="SingleLine" preprocessorDefineList="" termSemicolons="false" alwaysEscapeNonAscii="false" collapseToLiteral="true" constStatementsMozilla="false" debugLookupList="Debug,$Debug,WAssert,Msn.Debug,Web.Debug" errorIfNotInlineSafe="false" evalLiteralExpressions="true" evalTreatment="Ignore" ignoreConditionalCompilation="false" ignorePreprocessorDefines="false" inlineSafeStrings="true" knownGlobalNamesList="" localRenaming="CrunchAll" macSafariQuirks="true" manualRenamesProperties="true" noAutoRenameList="$super" preserveFunctionNames="false" preserveImportantComments="true" quoteObjectLiteralProperties="false" removeFunctionExpressionNames="true" removeUnneededCode="true" renamePairs="" reorderScopeDeclarations="true" strictMode="false" stripDebugStatements="true" severity="0"/>
    </microsoftAjax>
    <less useNativeMinification="false" ieCompat="false" strictMath="false" strictUnits="false" dumpLineNumbers="None" javascriptEnabled="true">
      <!-- ATTENTION: If you experience problems with the "V8JsEngine", use "MsieJsEngine" instead -->
      <jsEngine name="V8JsEngine"/>
    </less>
  </bundleTransformer>
  <jsEngineSwitcher xmlns="http://tempuri.org/JavaScriptEngineSwitcher.Configuration.xsd">
    <core>
      <engines>
        <add name="V8JsEngine" type="JavaScriptEngineSwitcher.V8.V8JsEngine, JavaScriptEngineSwitcher.V8"/>
        <add name="MsieJsEngine" type="JavaScriptEngineSwitcher.Msie.MsieJsEngine, JavaScriptEngineSwitcher.Msie"/>
      </engines>
    </core>
  </jsEngineSwitcher>
  <appSettings>
    <!--15-Oct-2015 : Moorthy : Added for email template send functionality -->
    <!-- <add key="SMTP.SERVER" value="seramike.squay.com" /> -->
    <!-- <add key="SMTP.USERNAME" value="dev1@vectone.smtp" /> -->
    <!-- <add key="SMTP.PASSWORD" value="devsmtp1" /> -->
    <add key="SMTP.SERVER" value="82.113.72.234"/>
    <add key="SMTP.USERNAME" value="smtp@smart.squay.com"/>
    <add key="SMTP.PASSWORD" value="smart123"/>
    <add key="SMTP.SSL" value="false"/>
    <add key="apptype" value="1"/>
    <add key="config:EnableTiles" value="1"/>
    <add key="config:EnableLoader" value="1"/>
    <add key="config:Project" value="Vectone.Web"/>
    <add key="config:Company" value="Vectone Mobile"/>
    <add key="config:CurrentTheme" value="smart-style-5"/>
    <!--smart-style-1-->
    <!--<add key="Image.TempFolderName" value="Temp"/>-->
    <add key="Image.TempFolderName" value="images/profile"/>
    <add key="webpages:Version" value="3.0.0.0"/>
    <add key="webpages:Enabled" value="false"/>
    <add key="PreserveLoginUrl" value="true"/>
    <add key="ClientValidationEnabled" value="true"/>
    <add key="UnobtrusiveJavaScriptEnabled" value="true"/>
    <add key="vs:EnableBrowserLink" value="false"/>
    <add key="Glimpse:DisableAsyncSupport" value="true"/>
    <add key="vt:ApplicationName" value="Vectone.NET"/>
    <add key="vt:EnableDynamicDiscovery" value="true"/>
    <add key="vt:EngineType" value=""/>
    <add key="vt:ThemesBasePath" value="~/Themes/"/>
    <!-- add custom Area paths separated with comma (e.g.: "~/Plugins/, ~/Areas/") -->
    <add key="vt:AreaBasePaths" value="~/Plugins/"/>
    <!-- set this to "true", if the view engine should also try to resolve view files ending with {CurrentLanguage} (e.g.: MyView.de.cshtml > MyView.cshtml)  -->
    <add key="vt:EnableLocalizedViews" value="false"/>
    <add key="vt:ShowCustomErrors" value="true"/>
    <add key="vt:ClearPluginsShadowDirectoryOnStartup" value="false"/>
    <add key="vt:TempDirectory" value="~/App_Data/_temp"/>
    <add key="vt:BizImportMediaDirectory" value="~/Media/Uploaded/static"/>
    <!--<add key="connectionString" value="Data Source=10.32.2.20;Initial Catalog=vectone_bizz;Integrated Security=False;Persist Security Info=False;User ID=smepbx;Password=smeswitch;Enlist=False;Pooling=True;Min Pool Size=1;Max Pool Size=100;Connect Timeout=600;User Instance=False"/>-->
    <!--<add key="connectionString" value="Data Source=10.32.2.20;Initial Catalog=vectone_bizz_20150121;Integrated Security=False;Persist Security Info=False;User ID=smepbx;Password=smeswitch;Enlist=False;Pooling=True;Min Pool Size=1;Max Pool Size=100;Connect Timeout=600;User Instance=False"/>-->
    <!--<add key="connectionString" value="Data Source=10.32.2.20;Initial Catalog=MarketingAutomation_clone;Integrated Security=False;Persist Security Info=False;User ID=smepbx;Password=smeswitch;Enlist=False;Pooling=True;Min Pool Size=1;Max Pool Size=100;Connect Timeout=600;User Instance=False"/>-->
    <add key="connectionString" value="Data Source=10.22.2.151;Initial Catalog=MarketingAutomationnew_clone;Integrated Security=False;Persist Security Info=False;User ID=nohuman;Password=nohumansql;Enlist=False;Pooling=True;Min Pool Size=1;Max Pool Size=100;Connect Timeout=600;User Instance=False"/>
    <!--<add key="connectionString" value="Data Source=juaypby7r0.database.windows.net;Initial Catalog=mundio_db;Integrated Security=False;Persist Security Info=False;User ID=hbinduni@juaypby7r0;Password=4Aliali2010;Enlist=False;Pooling=True;Min Pool Size=1;Max Pool Size=100;Connect Timeout=600;User Instance=False"/>-->
    <!--<add key="SMTPServer" value="seramike.squay.com"/>
    <add key="SMTPLogin" value="dev1@vectone.smtp"/>
    <add key="SMTPPassword" value="devsmtp1"/>
    <add key="EMAILDONOTREPLY" value="noreply@vectonemobile.com"/>-->
    <!-- List of SMS Url -->
    <add key="PaysafeUsername" value="psc_Mundio_Mobile_H_MAN1458038482"/>
    <add key="PaysafePassword" value="Vectone123!"/>
    <add key="Mode" value="LIVE"/>
    <add key="PaysafeSubID" value=""/>
    <add key="PaysafeClientID" value="myClientI2d"/>
    <add key="PaysafeShopID" value="345435"/>
    <add key="PaysafeShopLabel" value="LIVE"/>
    <add key="PaysafePnUrl" value="http://api.chillitalk.com/api/v1/home"/>
    <add key="WSSVCConnectionString" value="server=10.30.2.79;uid=sa;pwd=abbeysql;database=wsvc;Min Pool Size=5;Max Pool Size=100;Connect Timeout=300;"/>
    <!--CONNECTION STRINGS-->
    <add key="FLEXICONSSTRING" value="server=10.22.2.151;uid=agent;pwd=agentsql;database=gt;Min Pool Size=5;Max Pool Size=100;Connect Timeout=300;"/>
    <add key="DBProfileKeyCTPRes" value="Res"/>
    <add key="DBProfileKeyCTPEsp" value="esp"/>
    <add key="sitecodedbCTP" value="CTP"/>
    <add key="timeoutdb" value="50000"/>
    <add key="TopupFreeMinutesFlag" value="0"/>
    <add key="MinMasterBal" value="0"/>
    <!--<add key="ESP.VCLOUD" value="server=10.22.2.51;uid=vcloud;pwd=vadmin;database=esp;Min Pool Size=5;Max Pool Size=100;Connect Timeout=300;"/>-->
    <!--<add key="ESP.VCLOUD" value="server=10.22.2.86;uid=xglogin;pwd=bagito;database=esp;Min Pool Size=5;Max Pool Size=100;Connect Timeout=3000;"/>-->
    <!--<add key="Payment" value="server=10.32.2.20;uid=nohuman;pwd=nohumansql;database=payment_119;Min Pool Size=5;Max Pool Size=100;Connect Timeout=3000;"/> -->
    <add key="Payment" value="server=10.22.2.130;uid=nohuman;pwd=nohumansql;database=Payment;Min Pool Size=5;Max Pool Size=100;Connect Timeout=3000;"/>
    <!-- List of SMS Url -->
    <add key="MMISMSUrl" value="http://10.32.6.4/sms_iwmsc"/>
    <add key="CDRServicesUrl" value="http://10.30.2.79:8060"/>
    <add key="PaymentUrl" value="http://82.113.74.28:8020"/>
    <add key="UKSMSUrl" value="http://82.113.74.26/tpgsvc/TPGPost.svc/sms"/>
    <!--<add key="UKOTAUrl" value="http://10.22.2.89/sms_iwmsc?orig-addr=44123&amp;orig-noa=0&amp;dest-noa=1&amp;tp-dcs=246&amp;tp-pid=127&amp;tp-udhi=1&amp;tp_oa=44123&amp;dest-addr={0}&amp;tp-udl={1}&amp;tp-ud={2}"/>-->
    <add key="UKHRLMgt" value="http://10.22.1.32/hlr_sd_mgt?"/>
    <add key="FRSMSUrl" value="http://10.22.2.195/sms_iwmsc"/>
    <add key="DKSMSUrl" value="http://10.22.3.165/sms_iwmsc"/>
    <!--<add key="FROTAUrl" value="http://10.22.2.195/sms_iwmsc?orig-addr=33123&amp;orig-noa=0&amp;dest-noa=1&amp;tp-dcs=246&amp;tp-pid=127&amp;tp-udhi=1&amp;tp_oa=33123&amp;dest-addr={0}&amp;tp-udl={1}&amp;tp-ud={2}"/>-->
    <add key="FRHRLMgt" value="http://10.30.2.86/hlr_sd_mgt?"/>
    <add key="PTSMSUrl" value="http://10.22.2.202/sms_iwmsc"/>
    <!--<add key="PTOTAUrl" value="http://10.22.2.202/sms_iwmsc?orig-addr=351123&amp;orig-noa=0&amp;dest-noa=1&amp;tp-dcs=246&amp;tp-pid=127&amp;tp-udhi=1&amp;tp_oa=351123&amp;dest-addr={0}&amp;tp-udl={1}&amp;tp-ud={2}"/>-->
    <add key="PTHRLMgt" value="http://10.22.2.202/hlr_sd_mgt?"/>
    <add key="FISMSUrl" value="http://10.22.2.203/sms_iwmsc"/>
    <!--<add key="FIOTAUrl" value="http://10.22.2.203/sms_iwmsc?orig-addr=358123&amp;orig-noa=0&amp;dest-noa=1&amp;tp-dcs=246&amp;tp-pid=127&amp;tp-udhi=1&amp;tp_oa=358123&amp;dest-addr={0}&amp;tp-udl={1}&amp;tp-ud={2}"/>-->
    <add key="FIHRLMgt" value="http://10.22.2.203/hlr_sd_mgt?"/>
    <add key="SmsServerUrl" value="http://80.74.227.106/tpgpost.svc/sms"/>
    <add key="SmsOriginatorVectone" value="Vectone"/>
    <add key="SmsOriginatorDelight" value="Delight"/>
    <add key="SmsOriginatorUk1" value="UK1"/>
  </appSettings>
  <connectionStrings>
    <!--<add name="DefaultConnection" connectionString="Data Source=(localdb)\V11.0;Initial Catalog=Vectone.Web;Integrated Security=SSPI" providerName="System.Data.SqlClient"/>-->
    <!-- <add name="DefaultConnection" connectionString="Data Source=(localdb)\ProjectsV12;Initial Catalog=Vectone.Web;Integrated Security=SSPI" providerName="System.Data.SqlClient"/>-->
    <!--<add name="CRM.CTP" connectionString="Data Source=10.22.2.151;Initial Catalog=CRM3;Integrated Security=false;User ID=nohuman;Password=nohumansql;Min Pool Size=5;Max Pool Size=60;Connect Timeout=3000" providerName="System.Data.SqlClient"/>-->
    <add name="VoGoContext" connectionString="server=10.30.2.169;uid=xgate;pwd=XG_VOg;database=votg" providerName="System.Data.SqlClient"/>
    <add name="VCloudContext" connectionString="server=10.22.2.51;uid=vcloud;pwd=vadmin;database=crm_vb" providerName="System.Data.SqlClient"/>
    <add name="GCMContext" connectionString="server=10.30.2.169;uid=nohuman;pwd=nohumansql;database=gocheap" providerName="System.Data.SqlClient"/>
    <add name="CTPSipContext" connectionString="server=10.22.2.51;uid=nohuman;pwd=nohumansql;database=CTFS" providerName="System.Data.SqlClient"/>
    <add name="AirtimesContext" connectionString="Password=nohumansql;Persist Security Info=True;User ID=nohuman;Initial Catalog=Payment;Data Source=10.22.2.130" providerName="System.Data.SqlClient"/>
    <!--<add name="AirtimesContext" connectionString="Password=nohumansql;Persist Security Info=True;User ID=nohuman;Initial Catalog=Payment;Data Source=10.22.2.245" providerName="System.Data.SqlClient" />-->
    <add name="ESP.GB" connectionString="server=10.22.2.47;uid=nohuman;pwd=;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.NL" connectionString="server=10.22.1.17;uid=sa;pwd=abbeysql;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.AT" connectionString="server=10.24.15.24;uid=sa;pwd=abbeysql;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.SE" connectionString="server=10.22.2.33;uid=nohuman;pwd=z0mbi;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.DK" connectionString="server=10.30.2.25;uid=nohuman;pwd=nohumansql;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.PL" connectionString="server=10.30.2.81;uid=nohuman;pwd=nohumansql;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.PT" connectionString="server=10.30.2.119;uid=nohuman;pwd=nohumansql;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.FI" connectionString="server=10.30.2.115;uid=nohuman;pwd=nohumansql;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.FR" connectionString="server=10.30.2.143;uid=nohuman;pwd=nohumansql;database=esp" providerName="System.Data.SqlClient"/>
    <!--<add name="ESP.VCLOUD" connectionString="server=10.22.2.51;uid=vcloud;pwd=vadmin;database=esp" providerName="System.Data.SqlClient" />-->
    <add name="ESP.CALLINGCARD" connectionString="server=10.20.2.9;uid=xglogin;pwd=bagito;database=Esp_GBR" providerName="System.Data.SqlClient"/>
    <add name="ESP.CTP" connectionString="server=10.22.2.51;uid=vcloud;pwd=vadmin;database=esp" providerName="System.Data.SqlClient"/>
    <add name="ESP.GCM" connectionString="server=10.22.2.130;uid=nohuman;pwd=nohumansql;database=esp" providerName="System.Data.SqlClient"/>
    <add name="MVNO.GB" connectionString="server=10.22.2.42;uid=nohuman;pwd=z0mbi;database=mvno" providerName="System.Data.SqlClient"/>
    <add name="MVNO.PT" connectionString="server=10.30.2.118;uid=nohuman;pwd=nohumansql;database=mvno" providerName="System.Data.SqlClient"/>
    <!--<add name="MVNO.CTP" connectionString="server=10.22.2.86;uid=nohuman;pwd=nohumansql;database=Residential" providerName="System.Data.SqlClient" />-->
    <add name="MVNO.CTP" connectionString="server=10.22.2.86;uid=agent;pwd=agentsql;database=Residential" providerName="System.Data.SqlClient"/>
    <!-- This Connection String Used for the Call and sms History-->
    <!--<add name="MVNO.CTP" connectionString="server=10.22.2.86;uid=nohuman;pwd=nohumansql;database=Residential" providerName="System.Data.SqlClient" />-->
    <add name="MVNO.CTPNEW" connectionString="server=10.22.2.84;uid=nohuman;pwd=nohumansql;database=om" providerName="System.Data.SqlClient"/>
    <add name="MVNO.CTPNEWPREV" connectionString="server=192.168.41.19;uid=nohuman;pwd=nohumansql;database=om" providerName="System.Data.SqlClient"/>
    <add name="HLR.GB" connectionString="server=10.22.2.42;uid=nohuman;pwd=z0mbi;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="HLR.PT" connectionString="server=10.30.2.118;uid=nohuman;pwd=nohumansql;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="HLR.SE" connectionString="server=10.22.2.32;uid=nohuman;pwd=z0mbi;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="HLR.NL" connectionString="server=10.22.2.21;uid=sa;pwd=abbeysql;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="HLR.FR" connectionString="server=10.30.2.142;uid=nohuman;pwd=nohumansql;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="HLR.FI" connectionString="server=10.30.2.114;uid=nohuman;pwd=nohumansql;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="HLR.DK" connectionString="server=10.30.2.26;uid=nohuman;pwd=nohumansql;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="HLR.AT" connectionString="server=10.24.15.19;uid=sa;pwd=abbeysql;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="HLR.GCM" connectionString="server=10.30.2.198;uid=nohuman;pwd=nohumansql;database=hlrdb" providerName="System.Data.SqlClient"/>
    <add name="CRM.UK" connectionString="server=10.22.2.42;uid=nohuman;pwd=z0mbi;database=crm" providerName="System.Data.SqlClient"/>
    <add name="CRM.GB" connectionString="server=10.22.2.42;uid=nohuman;pwd=z0mbi;database=crm" providerName="System.Data.SqlClient"/>
    <add name="CRM.PT" connectionString="server=10.30.2.118;uid=nohuman;pwd=nohumansql;database=crm" providerName="System.Data.SqlClient"/>
    <add name="CRM.SE" connectionString="server=10.22.2.32;uid=nohuman;pwd=z0mbi;database=crm" providerName="System.Data.SqlClient"/>
    <add name="CRM.NL" connectionString="server=10.22.2.21;uid=sa;pwd=abbeysql;database=crm" providerName="System.Data.SqlClient"/>
    <add name="CRM.FR" connectionString="server=10.30.2.142;uid=nohuman;pwd=nohumansql;database=crm" providerName="System.Data.SqlClient"/>
    <add name="CRM.FI" connectionString="server=10.30.2.114;uid=nohuman;pwd=nohumansql;database=crm" providerName="System.Data.SqlClient"/>
    <add name="CRM.DK" connectionString="server=10.30.2.26;uid=nohuman;pwd=nohumansql;database=crm" providerName="System.Data.SqlClient"/>
    <add name="CRM.AT" connectionString="server=10.24.15.19;uid=sa;pwd=abbeysql;database=crm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.UK" connectionString="server=10.22.2.42;uid=nohuman;pwd=z0mbi;database=vm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.GB" connectionString="server=10.22.2.42;uid=nohuman;pwd=z0mbi;database=vm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.PT" connectionString="server=10.30.2.118;uid=nohuman;pwd=nohumansql;database=vm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.SE" connectionString="server=10.22.2.32;uid=nohuman;pwd=z0mbi;database=vm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.NL" connectionString="server=10.22.2.21;uid=sa;pwd=abbeysql;database=vm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.FR" connectionString="server=10.30.2.142;uid=nohuman;pwd=nohumansql;database=vm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.FI" connectionString="server=10.30.2.114;uid=nohuman;pwd=nohumansql;database=vm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.DK" connectionString="server=10.30.2.26;uid=nohuman;pwd=nohumansql;database=vm" providerName="System.Data.SqlClient"/>
    <add name="VMDB.AT" connectionString="server=10.24.15.19;uid=sa;pwd=abbeysql;database=vm" providerName="System.Data.SqlClient"/>
    <add name="SMSDB" connectionString="server=10.22.2.86;uid=xglogin;pwd=bagito;database=smsdb" providerName="System.Data.SqlClient"/>
    <add name="WebPayment" connectionString="server=10.22.2.130;uid=nohuman;pwd=nohumansql;database=WebPayment;"/>
    <add name="Payment" connectionString="server=10.22.2.130;uid=nohuman;pwd=nohumansql;database=Payment;"/>
    <!--<add name="WebPayment" connectionString="server=10.22.2.245;uid=nohuman;pwd=nohumansql;database=WebPayment;" />
    <add name="Payment" connectionString="server=10.22.2.245;uid=nohuman;pwd=nohumansql;database=Payment;" />-->
    <add name="payment_20160301" connectionString="server=10.22.2.130;uid=nohuman;pwd=nohumansql;database=Payment;"/>
    <!--<add name="Payment" connectionString="server=10.32.2.20;uid=nohuman;pwd=nohumansql;database=payment_119;" />
  <add name="" connectionString="server=10.22.2.130;uid=nohuman;pwd=nohumansql;database=Payment;" />-->
    <!--<add name="payment_20160301" connectionString="server=10.30.2.117\development;uid=nohuman;pwd=nohumansql;database=payment_20160301;" /> -->
    <!--<add name="esp" connectionString="server=10.32.2.20;uid=nohuman;pwd=nohumansql;database=esp_20160301;Min Pool Size=5;Max Pool Size=100;Connect Timeout=3000;"/>-->
    <add name="esp" connectionString="server=10.22.2.47;uid=nohuman;pwd=z0mbi;database=esp;Min Pool Size=5;Max Pool Size=100;Connect Timeout=3000;"/>
    <add name="ESP_AT" connectionString="server=10.24.15.24;uid=nohuman;pwd=nohumansql;database=esp" providerName="System.Data.SqlClient"/>
    <add name="Rate" connectionString="server=10.30.2.79;uid=nohuman;pwd=nohumansql;database=rate;"/>
    <add name="CRM3" connectionString="Data Source=10.22.2.151;Initial Catalog=CRM3;Integrated Security=false;User ID=nohuman;Password=nohumansql;Min Pool Size=5;Max Pool Size=60;Connect Timeout=3000" providerName="System.Data.SqlClient"/>
    <add name="VB" connectionString="Data Source=10.22.2.86;Initial Catalog=Config;Integrated Security=false;User ID=smepbx;Password=smeswitch;Min Pool Size=5;Max Pool Size=60;Connect Timeout=3000" providerName="System.Data.SqlClient"/>
    <add name="ESP.VCLOUD" connectionString="Data Source=10.22.2.86;Initial Catalog=esp;Integrated Security=false;User ID=xglogin;Password=bagito;Min Pool Size=5;Max Pool Size=60;Connect Timeout=3000" providerName="System.Data.SqlClient"/>
    <add name="Residential" connectionString="Data Source=10.22.2.86;Initial Catalog=residential;Integrated Security=false;User ID=agent;Password=agentsql;Min Pool Size=5;Max Pool Size=60;Connect Timeout=3000" providerName="System.Data.SqlClient"/>
  </connectionStrings>
  <location path="Plugins/ImportBiz">
    <system.web>
      <httpRuntime maxRequestLength="1536000" executionTimeout="14400" maxQueryStringLength="16384"/>
    </system.web>
    <system.webServer>
      <security>
        <requestFiltering>
          <requestLimits maxAllowedContentLength="1572864000" maxQueryString="16384"/>
        </requestFiltering>
      </security>
      <httpProtocol>
        <customHeaders>
          <add name="Access-Control-Allow-Origin" value="*"/>
          <add name="Access-Control-Allow-Headers" value="Content-Type"/>
          <add name="Access-Control-Allow-Methods" value="GET, POST, PUT, DELETE, OPTIONS"/>
        </customHeaders>
      </httpProtocol>
      <validation validateIntegratedModeConfiguration="false"/>
      <handlers>
        <remove name="ExtensionlessUrlHandler-ISAPI-4.0_32bit"/>
        <remove name="ExtensionlessUrlHandler-ISAPI-4.0_64bit"/>
        <remove name="ExtensionlessUrlHandler-Integrated-4.0"/>
        <add name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness32" responseBufferLimit="0"/>
        <add name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness64" responseBufferLimit="0"/>
        <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0"/>
      </handlers>
    </system.webServer>
  </location>
  <system.web>
    <!--<customErrors mode="RemoteOnly"/>-->
    <sessionState mode="InProc" timeout="1200"/>
    <!--<trace enabled="true" localOnly="true" pageOutput="true" requestLimit="40"/>-->
    <compilation targetFramework="4.5" batch="false" numRecompilesBeforeAppRestart="250" optimizeCompilations="true">
      <assemblies>
        <add assembly="System.Web.Abstractions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        <add assembly="System.Web.Helpers, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        <add assembly="System.Web.Routing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        <add assembly="System.Web.Mvc, Version=5.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        <add assembly="System.Web.WebPages, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        <add assembly="System.IO.Compression.FileSystem, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
      </assemblies>
    </compilation>
    <!-- <customErrors mode="Off" defaultRedirect="~/account/error"/> -->
    <authentication mode="Forms">
      <forms name="Vectone.AUTH" loginUrl="~/account/login" protection="All" timeout="43200" path="/" requireSSL="false" slidingExpiration="true"/>
    </authentication>
    <pages>
      <namespaces>
        <add namespace="System.Web.Helpers"/>
        <add namespace="System.Web.Mvc"/>
        <add namespace="System.Web.Mvc.Ajax"/>
        <add namespace="System.Web.Mvc.Html"/>
        <add namespace="System.Web.Routing"/>
        <add namespace="System.Web.WebPages"/>
        <add namespace="System.Web.Optimization"/>
      </namespaces>
    </pages>
    <httpModules/>
    <httpRuntime targetFramework="4.5.1" maxRequestLength="1536000" executionTimeout="5400" maxQueryStringLength="16384"/>
  </system.web>
  <system.webServer>
    <!--Prevent IIS 7.0 from returning a custom 404/500 error page of its own. -->
    <httpErrors existingResponse="PassThrough"/>
    <validation validateIntegratedModeConfiguration="false"/>
    <modules runAllManagedModulesForAllRequests="true"/>
    <handlers accessPolicy="Script,Read">
      <!--Clear all handlers, prevents executing code file extensions or returning any file contents.-->
      <clear/>
      <!--sitemap.xml-->
      <add name="sitemap" path="sitemap.xml" verb="*" type="System.Web.Routing.UrlRoutingModule" resourceType="Unspecified" preCondition="integratedMode"/>
      <!--robots.txt-->
      <add name="robots" path="robots.txt" verb="*" type="System.Web.Routing.UrlRoutingModule" resourceType="Unspecified" preCondition="integratedMode"/>
      <!--Return 404 for all requests via a managed handler. The URL routing handler will substitute the MVC request handler when routes match.-->
      <add name="NotFound" path="*" verb="*" type="System.Web.HttpNotFoundHandler" preCondition="integratedMode" requireAccess="Script"/>
      <!--WebApi-->
      <remove name="ExtensionlessUrlHandler-ISAPI-4.0_32bit"/>
      <remove name="ExtensionlessUrlHandler-ISAPI-4.0_64bit"/>
      <remove name="ExtensionlessUrlHandler-Integrated-4.0"/>
      <add name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness32" responseBufferLimit="0"/>
      <add name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness64" responseBufferLimit="0"/>
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0"/>
    </handlers>
    <staticContent>
      <clientCache cacheControlMode="UseMaxAge" cacheControlMaxAge="24.00:00:00"/>
      <remove fileExtension=".svg"/>
      <remove fileExtension=".eot"/>
      <remove fileExtension=".woff"/>
      <remove fileExtension=".otf"/>
      <remove fileExtension=".json"/>
      <mimeMap fileExtension=".svg" mimeType="image/svg+xml"/>
      <mimeMap fileExtension=".eot" mimeType="application/octet-stream"/>
      <mimeMap fileExtension=".woff" mimeType="application/x-font-woff"/>
      <mimeMap fileExtension=".otf" mimeType="application/x-font-opentype"/>
      <mimeMap fileExtension=".json" mimeType="application/json"/>
    </staticContent>
    <urlCompression doStaticCompression="false" doDynamicCompression="true"/>
    <security>
      <requestFiltering>
        <hiddenSegments>
          <add segment="ClearScript.V8"/>
        </hiddenSegments>
      </requestFiltering>
    </security>
  </system.webServer>
  <system.serviceModel>
    <serviceHostingEnvironment aspNetCompatibilityEnabled="true" multipleSiteBindingsEnabled="true"/>
  </system.serviceModel>
  <system.diagnostics>
    <trace autoflush="true" indentsize="4"/>
  </system.diagnostics>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.2.0" newVersion="5.2.2.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.2.0" newVersion="5.2.2.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http.WebHost" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.2.0" newVersion="5.2.2.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-1.6.5135.21930" newVersion="1.6.5135.21930"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Autofac" publicKeyToken="17863af14b0044da" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.0" newVersion="3.5.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="JavaScriptEngineSwitcher.Core" publicKeyToken="c608b2a8cc9e4472" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-1.1.3.0" newVersion="1.1.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-6.0.0.0" newVersion="6.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.Edm" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.6.3.0" newVersion="5.6.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.OData" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.6.3.0" newVersion="5.6.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Spatial" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.6.3.0" newVersion="5.6.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Antlr3.Runtime" publicKeyToken="eb42632606e9261f" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.2" newVersion="3.5.0.2"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="AjaxMin" publicKeyToken="21ef50ce11b5d80f" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.13.5463.15277" newVersion="5.13.5463.15277"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="NLog" publicKeyToken="5120e14c03d0593c" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.data>
    <DbProviderFactories>
      <remove invariant="System.Data.SqlServerCe.4.0"/>
      <add name="Microsoft SQL Server Compact Data Provider 4.0" invariant="System.Data.SqlServerCe.4.0" description=".NET Framework Data Provider for Microsoft SQL Server Compact" type="System.Data.SqlServerCe.SqlCeProviderFactory, System.Data.SqlServerCe, Version=4.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"/>
    </DbProviderFactories>
  </system.data>
  <!-- Add the attribute 'codeConfigurationType' to the 'entityFramework' root element to overwrite the global DbConfiguration -->
  <entityFramework>
    <providers>
      <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer"/>
      <provider invariantName="System.Data.SqlServerCe.4.0" type="System.Data.Entity.SqlServerCompact.SqlCeProviderServices, EntityFramework.SqlServerCompact"/>
    </providers>
  </entityFramework>
</configuration>