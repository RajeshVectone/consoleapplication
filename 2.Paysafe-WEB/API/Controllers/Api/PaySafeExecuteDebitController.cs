﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net;
using System.Net.Http;
using Vectone.PSC;
using System.Globalization;
using Vectone.Core.Domain.PaySafe;
using System.Configuration;
using Vectone.Services.PaySafeServices;
using NLog;


namespace Vectone.WebApi.Controllers.Api
{
    [WebApiAuthenticate]
    public class PaySafeExecuteDebitController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        public HttpResponseMessage Post(PaySafeRequest input)
        {
            Log.Info("PaySafeExecuteDebit");
            PaySafeTransaction output = new PaySafeTransaction();
            try
            {
                if (input != null)
                {
                    Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(input));

                    Operation _operation = new Operation();
                    output = _operation.ExecuteDebit(input);
                }
                else
                {
                    Log.Info("Input details not found");
                    output = new PaySafeTransaction();
                    output.errcode = -1;
                    output.errmsg = "Input details not found!";
                }
            }
            catch (Exception ex)
            {
                Log.Error("PaySafeExecuteDebit : " + ex.Message);
                output = new PaySafeTransaction();
                output.errcode = -1;
                output.errmsg = ex.Message;
            }

            Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(output));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
