﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using Vectone.Services.PaySafeServices;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net;
using System.Net.Http;
using Vectone.PSC;
using System.Globalization;
using Vectone.Core.Domain.PaySafe;
using System.Configuration;
using NLog;
using System.Text.RegularExpressions;


namespace Vectone.WebApi.Controllers.Api
{
    [WebApiAuthenticate]
    public class PaySafeController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        public HttpResponseMessage Post(PaySafeRequest input)
        {
            Log.Info("PaySafeController");
            PaySafeTransaction output = new PaySafeTransaction();
            try
            {
                if (input != null)
                {
                    Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(input));

                    //30-Dec-2019: Moorthy : Added to avoid chillitalk payment
                    if (input.Applicationcode.StartsWith("CTP"))
                    {
                        Log.Debug("Chillitalk : Sorry, this service is temporarily unavailable");
                        output.errcode = -1;
                        output.errmsg = "Sorry, this service is temporarily unavailable";
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }

                    Operation _operation = new Operation();
                    output = _operation.CreateDisposition(input);
                }
                else
                {
                    Log.Info("Input details not found");
                    output = new PaySafeTransaction();
                    output.errCode = -1;
                    output.errMsg = "Input details not found!";
                }
            }
            catch (Exception ex)
            {
                Log.Error("PaySafeController : " + ex.Message);
                output = new PaySafeTransaction();
                output.errCode = -1;
                output.errMsg = ex.Message;
            }
            Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(output));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }


    }

}
