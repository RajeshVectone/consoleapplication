﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net;
using System.Net.Http;
using Vectone.PSC;
using System.Globalization;
using Vectone.Core.Domain.PaySafe;
using System.Configuration;
using NLog;


namespace Vectone.WebApi.Controllers.Api
{
    //[WebApiAuthenticate]
    public class PaySafeGetSerialController : ApiController
    {

        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private PSC.SOPGClassicMerchantClient client;
        #endregion

        public PaySafeGetSerialController()
        {
            try
            {
                // Create a new instance of MerchantClient. In this example a exception would be throwed in the
                // constructor, if the given language is invalid.
                client = new PSC.SOPGClassicMerchantClient(true, "EN", true, DevStatus.LIVE);

                // Each event from PSC SDK must be redirected to this application:
                client.logMessageEvent += new PSC.SOPGClassicMerchantClient.logMessageEventHandler(client_logMessageEvent);
            }

            catch (ArgumentException argEx)
            {
                Log.Error(argEx.Message);
            }
        }

        public HttpResponseMessage Post(PaySafeRequest _paysaferequest)
        {
            Log.Info("PaySafeGetSerial");
            try
            {
                Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(_paysaferequest));
                client.DeleteLogMessages(Logging.LogType.DEBUG);
                client.DeleteLogMessages(Logging.LogType.ERROR);
                client.DeleteLogMessages(Logging.LogType.INFO);
                client.DeleteLogMessages(Logging.LogType.WARNING);

                // Converts floating point numbers (float/double) to strings with two decimal digits:
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalDigits = 2;


                client.SetUsername(ConfigurationManager.AppSettings["PaysafeUsername"]);
                client.SetPassword(ConfigurationManager.AppSettings["PaysafePassword"]);
                client.SetMtID(_paysaferequest.MtId);
                client.SetSubID(ConfigurationManager.AppSettings["PaysafeSubID"]);
                client.SetCurrency(_paysaferequest.Currency);
                client.SetMerchantClientID(ConfigurationManager.AppSettings["PaysafeClientID"]);
                client.SetShopID(ConfigurationManager.AppSettings["PaysafeShopID"]);
                client.SetShopLabel(ConfigurationManager.AppSettings["PaysafeShopLabel"]);

                #region Commented
                // client.SetPnURL(_paysaferequest.Pnurl);
                //client.SetAmount(_paysaferequest.Total);
                //client.SetOkURL(_paysaferequest.OkUrl);
                //client.SetnOKURL(_paysaferequest.NokUrl);
                // client.SetPnURL(_paysaferequest.Pnurl);
                //client.SetRestrictedCountry("DE");
                //client.SetRestrictedCountry("GB");
                //client.SetMinAge("12");
                //client.SetMinKycLevel("SIMPLE"); 
                #endregion

                // In this example it's a test:
                client.SetStatus(ConfigurationManager.AppSettings["Mode"]);

                //14-Apr-2018 : Moorthy : Added for the Paysafe TLS 1.2 Update
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                SOPGClassicMerchantClient.com.paysafecard.soa.GetSerialNumbersReturn GsRet = client.GetSerialNumbers();


                var response = new PaySafeSerialResponse
                {
                    errcode = 0,
                    errmsg = "Success",
                    Amount = GsRet.amount,
                    Currency = GsRet.currency,
                    DispositionState = GsRet.dispositionState,
                    Errorcode = GsRet.errorCode,
                    MtID = GsRet.mtid,
                    Resultcode = GsRet.resultCode,
                    SerialNumbers = GsRet.serialNumbers,
                    SubID = GsRet.subId
                };
                
                Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(response));
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                Log.Error("PaySafeGetSerial : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new PaySafeSerialResponse() { errcode = -1, errmsg = ex.Message });
            }
        }



        void client_logMessageEvent(object sender, Logging.LogMessage logMessage)
        {
            string logTypeStr = "";

            switch (logMessage.logType)
            {
                case Logging.LogType.DEBUG: logTypeStr = "DEBUG"; break;
                case Logging.LogType.ERROR: logTypeStr = "ERROR"; break;
                case Logging.LogType.INFO: logTypeStr = "INFO"; break;
                case Logging.LogType.WARNING: logTypeStr = "WARNING"; break;
            }

            if (logMessage.logType == Logging.LogType.DEBUG)
            {


                return;
            }


        }
    }

}
