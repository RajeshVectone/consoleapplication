﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vectone.Core.Domain.CurrencyUpdate;
using Vectone.Core.Domain.ErrorMsgGet;
using Vectone.Services.GenericServices;

namespace Vectone.WebApi.Controllers.Api
{
    public class ErrorMsgGetController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private IGenericServices<ErrorMsgGetOutput, ErrorMsgGetInput> _GenericServices;
        #endregion

        #region Constructor
        public ErrorMsgGetController(IGenericServices<ErrorMsgGetOutput, ErrorMsgGetInput> GenericServices)
        {
            this._GenericServices = GenericServices;  //new GenericServices<ErrorMsgGetInputOutput.ErrorMsgGetOutput, ErrorMsgGetInputOutput.ErrorMsgGetInput>();
        }
        #endregion

        public HttpResponseMessage Post(ErrorMsgGetInput input)
        {
            Log.Info("ErrorMsgGet");
            List<ErrorMsgGetOutput> outputList = new List<ErrorMsgGetOutput>();
            try
            {
                if (input != null)
                {
                    Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(input));

                    outputList = _GenericServices.GenericMethod
                        (input, ErrorMsgGetDBMethods.GetParameters(input), ErrorMsgGetDBMethods.GetSpName(), ErrorMsgGetDBMethods.GetConnName());
                }
                else
                {
                    Log.Info("Input details not found");
                    ErrorMsgGetOutput output = new ErrorMsgGetOutput();
                    output.errcode = -1;
                    output.errmsg = "Input details not found!";
                    outputList.Add(output);
                }
            }
            catch (Exception ex)
            {
                Log.Error("ErrorMsgGet : " + ex.Message);
                ErrorMsgGetOutput output = new ErrorMsgGetOutput();
                output.errcode = -1;
                output.errmsg = ex.Message;
                outputList.Add(output);
            }
            Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(outputList));
            return Request.CreateResponse(HttpStatusCode.OK, outputList);
        }

    }
}
