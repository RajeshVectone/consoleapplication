﻿#region Assemblies
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vectone.Core.Domain.PaySafe;
using Vectone.Services.PaySafeServices;
using Vectone.Web.Framework.WebApi.Security; 
#endregion

#region NameSpace
//15-Oct-2015: Moorthy : Added for get country by mobileno
namespace Vectone.WebApi.Controllers.Api
{
    #region PaySafeGetCountryByMobileNoController
    [WebApiAuthenticate]
    public class PaySafeGetCountryByMobileNoController : ApiController
    {
        #region Contructor
        public PaySafeGetCountryByMobileNoController() { }
        #endregion

        #region Post
        public HttpResponseMessage Post(CommonInput input)
        {
            CommonOutput output = new CommonOutput();
            if (input != null && input.MobileNo != null && input.MobileNo.Trim().Length > 0)
            {
                PaySafeBL objPaySafeBL = new PaySafeBL();
                output = objPaySafeBL.GetCountryByMobileNo(input);
            }
            else
            {
                output.errcode = -1;
                output.errmsg = "input details required";
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        #endregion
    }
    #endregion
} 
#endregion
