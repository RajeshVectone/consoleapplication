﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vectone.Core.Domain.CurrencyUpdate;
using Vectone.Services.GenericServices;

namespace Vectone.WebApi.Controllers.Api
{
    public class CurrencyUpdateController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private IGenericServices<CurrencyUpdateOutput, CurrencyUpdateInput> _GenericServices;
        #endregion

        #region Constructor
        public CurrencyUpdateController(IGenericServices<CurrencyUpdateOutput, CurrencyUpdateInput> GenericServices)
        {
            this._GenericServices = GenericServices;  //new GenericServices<CurrencyUpdateInputOutput.CurrencyUpdateOutput, CurrencyUpdateInputOutput.CurrencyUpdateInput>();
        }
        #endregion

        public HttpResponseMessage Post(CurrencyUpdateInput input)
        {
            Log.Info("CurrencyUpdate");
            List<CurrencyUpdateOutput> outputList = new List<CurrencyUpdateOutput>();
            try
            {
                if (input != null)
                {
                    Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(input));

                    outputList = _GenericServices.GenericMethod
                        (input,
                        CurrencyUpdateDBMethods.GetParameters(input), CurrencyUpdateDBMethods.GetSpName(), CurrencyUpdateDBMethods.GetConnName());
                }
                else
                {
                    Log.Info("Input details not found");
                    CurrencyUpdateOutput output = new CurrencyUpdateOutput();
                    output.errcode = -1;
                    output.errmsg = "Input details not found!";
                    outputList.Add(output);
                }
            }
            catch (Exception ex)
            {
                Log.Error("CurrencyUpdate : " + ex.Message);
                CurrencyUpdateOutput output = new CurrencyUpdateOutput();
                output.errcode = -1;
                output.errmsg = ex.Message;
                outputList.Add(output);
            }
            Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(outputList));
            return Request.CreateResponse(HttpStatusCode.OK, outputList);
        }

    }
}
