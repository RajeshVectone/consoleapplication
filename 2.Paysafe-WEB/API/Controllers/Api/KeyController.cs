﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net.Http;
using System.Web;
using Vectone.Web.Framework.WebApi;
using System.Net;
using NLog;

namespace Vectone.WebApi.Controllers.Api
{
    //[WebApiAuthenticate]
	public class KeyController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        protected HmacAuthentication _hmac = new HmacAuthentication();
        public HttpResponseMessage Get()
        {
            try
            {
                //Log.Info("Key Controller");
                var request = HttpContext.Current.Request;
                string contentMd5 = request.Headers["Content-Md5"] ?? request.Headers["Content-MD5"];
                string publickey = Convert.ToBase64String(contentMd5.ToByteArray()).ToString();


                string headTimestamp = DateTime.Now.ToString();



                var context = new WebApiRequestContext()
                {
                    HttpMethod = request.HttpMethod,
                    HttpAcceptType = request.Headers["Accept"],
                    PublicKey = publickey,
                    ContentMd5 = contentMd5,
                    SecretKey = "chillitalkiosapiwithhash",
                    Url = HttpUtility.UrlDecode(request.Url.AbsoluteUri.ToLower())
                };




                string messageRepresentation = _hmac.CreateMessageRepresentation(context, contentMd5, headTimestamp);
                string getAuthkey = _hmac.CreateSignature("chillitalkiosapiwithhash", messageRepresentation);
                string postAuthkey = _hmac.CreateSignature("chillitalkiosapiwithhash", "post" + messageRepresentation.Substring(3, messageRepresentation.Length - 3));
                var query = new Keys { Publickey = publickey, GetAuthkey = getAuthkey, PostAuthkey = postAuthkey };
                //Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(query));
                return Request.CreateResponse(HttpStatusCode.OK, query);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            //return new HttpResponseMessage()
            //{
            //    Content = new StringContent(Newtonsoft.Json.Linq.JArray.FromObject(query).ToString(), System.Text.Encoding.UTF8, "application/json")
            //};
        }
	}
     
}
public class Keys
{
    private string _publickey;
    private string _getAuthkey;
    private string _postAuthkey;

    public string PostAuthkey
    {
        get { return _postAuthkey; }
        set { _postAuthkey = value; }
    }

    public string Publickey
    {
        get { return _publickey; }
        set { _publickey = value; }
    }
   

    public string GetAuthkey
    {
        get { return _getAuthkey; }
        set { _getAuthkey = value; }
    }
}