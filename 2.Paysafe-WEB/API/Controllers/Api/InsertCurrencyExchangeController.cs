﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vectone.Core.Domain.InsertCurrencyExchange;
using Vectone.Services.GenericServices;

namespace Vectone.WebApi.Controllers.Api
{
    public class InsertCurrencyExchangeController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private IGenericServices<InsertCurrencyExchangeOutput, InsertCurrencyExchangeInput> _GenericServices;
        #endregion

        #region Constructor
        public InsertCurrencyExchangeController(IGenericServices<InsertCurrencyExchangeOutput, InsertCurrencyExchangeInput> GenericServices)
        {
            this._GenericServices = GenericServices;  //new GenericServices<InsertCurrencyExchangeInputOutput.InsertCurrencyExchangeOutput, InsertCurrencyExchangeInputOutput.InsertCurrencyExchangeInput>();
        }
        #endregion

        public HttpResponseMessage Post(InsertCurrencyExchangeInput input)
        {
            Log.Info("InsertCurrencyExchange");
            List<InsertCurrencyExchangeOutput> outputList = new List<InsertCurrencyExchangeOutput>();
            try
            {
                if (input != null)
                {
                    Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(input));

                    string connName = InsertCurrencyExchangeDBMethods.GetConnName(input.siteCode);

                    outputList = _GenericServices.GenericMethod
                        (input, InsertCurrencyExchangeDBMethods.GetParameters(input), InsertCurrencyExchangeDBMethods.GetSpName(), connName);
                }
                else
                {
                    Log.Info("Input details not found");
                    InsertCurrencyExchangeOutput output = new InsertCurrencyExchangeOutput();
                    output.Errorcode = -1;
                    output.ErrorMsg = "Input details not found!";
                    outputList.Add(output);
                }
            }
            catch (Exception ex)
            {
                Log.Error("InsertCurrencyExchange : " + ex.Message);
                InsertCurrencyExchangeOutput output = new InsertCurrencyExchangeOutput();
                output.Errorcode = -1;
                output.ErrorMsg = ex.Message;
                outputList.Add(output);
            }
            Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(outputList));
            return Request.CreateResponse(HttpStatusCode.OK, outputList);
        }
    }
}
