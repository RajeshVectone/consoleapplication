﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using Vectone.Services.PaySafeServices;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net;
using System.Net.Http;
using Vectone.PSC;
using System.Globalization;
using Vectone.Core.Domain.PaySafe;
using System.Configuration;
using NLog;
using System.Text.RegularExpressions;


namespace Vectone.WebApi.Controllers.Api
{
    [WebApiAuthenticate]
    public class PaySafeCancelController : ApiController
    {

        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        public HttpResponseMessage Post(PaySafeRequest _paysaferequest)
        {
            Log.Info("PaySafeCancel");
            if (_paysaferequest != null)
            {
                Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(_paysaferequest));
                PaySafeService _paysafeServic = new PaySafeService();
                _paysaferequest.Paymentstatus = -1;
                _paysaferequest.Paymentstep = 12;
                _paysaferequest.Description = "Payment cancelled By user";
                _paysafeServic.UpdatePaymentData(_paysaferequest, "1", 1);
            }
            else
            {
                Log.Info("Input details not found");
                return Request.CreateResponse(HttpStatusCode.OK, "Input details not found");
            }
            Log.Info("Output : Payment Canceled by User");
            return Request.CreateResponse(HttpStatusCode.OK, "Payment Canceled by User");
        }


    }

}
