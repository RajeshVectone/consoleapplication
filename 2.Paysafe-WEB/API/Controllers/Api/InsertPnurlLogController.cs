﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vectone.Core.Domain.InsertPnurlLog;
using Vectone.Services.GenericServices;

namespace Vectone.WebApi.Controllers.Api
{
    public class InsertPnurlLogController : ApiController
    {               
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private IGenericServices<InsertPnurlLogOutput, InsertPnurlLogInput> _GenericServices;
        #endregion

        #region Constructor
        public InsertPnurlLogController(IGenericServices<InsertPnurlLogOutput, InsertPnurlLogInput> GenericServices)
        {
            this._GenericServices = GenericServices;  //new GenericServices<InsertPnurlLogInputOutput.InsertPnurlLogOutput, InsertPnurlLogInputOutput.InsertPnurlLogInput>();
        }
        #endregion

        public HttpResponseMessage Post(InsertPnurlLogInput input)
        {
            Log.Info("InsertPnurlLog");
            List<InsertPnurlLogOutput> outputList = new List<InsertPnurlLogOutput>();
            try
            {
                if (input != null)
                {
                    Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(input));

                    outputList = _GenericServices.GenericMethod
                        (input, InsertPnurlLogDBMethods.GetParameters(input), InsertPnurlLogDBMethods.GetSpName(),InsertPnurlLogDBMethods.GetConnName());
                }
                else
                {
                    Log.Info("Input details not found");
                    InsertPnurlLogOutput output = new InsertPnurlLogOutput();
                    output.errcode = -1;
                    output.errmsg = "Input details not found!";
                    outputList.Add(output);
                }
            }
            catch (Exception ex)
            {
                Log.Error("InsertPnurlLog : " + ex.Message);
                InsertPnurlLogOutput output = new InsertPnurlLogOutput();
                output.errcode = -1;
                output.errmsg = ex.Message;
                outputList.Add(output);
            }
            Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(outputList));
            return Request.CreateResponse(HttpStatusCode.OK, outputList);
        }

    }
}
