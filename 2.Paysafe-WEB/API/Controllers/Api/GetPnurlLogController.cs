﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vectone.Core.Domain.GetPnurlLog;
using Vectone.Services.GenericServices;

namespace Vectone.WebApi.Controllers.Api
{
    public class GetPnurlLogController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private IGenericServices<GetPnurlLogOutput, GetPnurlLogInput> _GenericServices;
        #endregion

        #region Constructor
        public GetPnurlLogController(IGenericServices<GetPnurlLogOutput, GetPnurlLogInput> GenericServices)
        {
            this._GenericServices = GenericServices;  //new GenericServices<GetPnurlLogInputOutput.GetPnurlLogOutput, GetPnurlLogInputOutput.GetPnurlLogInput>();
        }
        #endregion

        public HttpResponseMessage Post(GetPnurlLogInput input)
        {
            Log.Info("GetPnurlLogController");
            List<GetPnurlLogOutput> outputList = new List<GetPnurlLogOutput>();
            try
            {
                if (input != null)
                {
                    Log.Info("mobileno : " + input.mobileno + " , " + "refId : " + input.refId);
                    outputList = _GenericServices.GenericMethod
                        (input, GetPnurlLogDBMethods.GetParameters(input), GetPnurlLogDBMethods.GetSpName(), GetPnurlLogDBMethods.GetConnName());
                }
                else
                {
                    Log.Info("Input details not found");
                    GetPnurlLogOutput output = new GetPnurlLogOutput();
                    output.errcode = -1;
                    output.errmsg = "Input details not found!";
                    outputList.Add(output);
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetPnurlLog : " + ex.Message);
                GetPnurlLogOutput output = new GetPnurlLogOutput();
                output.errcode = -1;
                output.errmsg = "Input details not found!";
                outputList.Add(output);
            }
            return Request.CreateResponse(HttpStatusCode.OK, outputList);
        }

    }
}
