﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using Vectone.Services.PaySafeServices;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net;
using System.Net.Http;
using Vectone.PSC;
using System.Globalization;
using Vectone.Core.Domain.PaySafe;
using System.Configuration;
using NLog;
using System.Text.RegularExpressions;


namespace Vectone.WebApi.Controllers.Api
{
    [WebApiAuthenticate]
    public class PHPPaySafeController : ApiController
    {


        public HttpResponseMessage Post(PaySafeRequest _paysaferequest)
        {
            if (_paysaferequest != null)
            {
                Operation _operation = new Operation();
                //PaySafeService _paysafeService = new PaySafeService();
                //20-Jan-2016 : Moorthy modified for common payment
                var query = _operation.CreateDispositionPHP(_paysaferequest);
                return Request.CreateResponse(HttpStatusCode.OK, query);
            }
            else
            {
                PaySafeTransaction output = new PaySafeTransaction();
                output.errCode = -1;
                output.errMsg = "input details not found";
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }


    }

}
