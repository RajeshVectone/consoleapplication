﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vectone.Core.Domain.InsertMerchantRefDtls;
using Vectone.Services.GenericServices;

namespace Vectone.WebApi.Controllers.Api
{
    public class InsertMerchantRefDtlsController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private IGenericServices<InsertMerchantRefDtlsOutput, InsertMerchantRefDtlsInput> _GenericServices;
        #endregion

        #region Constructor
        public InsertMerchantRefDtlsController(IGenericServices<InsertMerchantRefDtlsOutput, InsertMerchantRefDtlsInput> GenericServices)
        {
            this._GenericServices = GenericServices;  //new GenericServices<InsertMerchantRefDtlsInputOutput.InsertMerchantRefDtlsOutput, InsertMerchantRefDtlsInputOutput.InsertMerchantRefDtlsInput>();
        }
        #endregion

        public HttpResponseMessage Post(InsertMerchantRefDtlsInput input)
        {
            Log.Info("InsertMerchantRefDtls");
            List<InsertMerchantRefDtlsOutput> outputList = new List<InsertMerchantRefDtlsOutput>();
            try
            {
                if (input != null)
                {
                    Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(input));
                    outputList = _GenericServices.GenericMethod
                        (input, InsertMerchantRefDtlsDBMethods.GetParameters(input), InsertMerchantRefDtlsDBMethods.GetSpName(), InsertMerchantRefDtlsDBMethods.GetConnName());
                }
                else
                {
                    Log.Info("Input details not found");
                    InsertMerchantRefDtlsOutput output = new InsertMerchantRefDtlsOutput();
                    output.errcode = -1;
                    output.errmsg = "Input details not found!";
                    outputList.Add(output);
                }
            }
            catch (Exception ex)
            {
                Log.Error("InsertMerchantRefDtls : " + ex.Message);
                InsertMerchantRefDtlsOutput output = new InsertMerchantRefDtlsOutput();
                output.errcode = -1;
                output.errmsg = ex.Message;
                outputList.Add(output);
            }
            Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(outputList));
            return Request.CreateResponse(HttpStatusCode.OK, outputList);
        }

    }
}
