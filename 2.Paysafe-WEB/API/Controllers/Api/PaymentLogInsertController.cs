﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vectone.Core.Domain.PaymentLogInsert;
using Vectone.Services.GenericServices;

namespace Vectone.WebApi.Controllers.Api
{
    public class PaymentLogInsertController : ApiController
    {
                #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private IGenericServices<PaymentLogInsertOutput, PaymentLogInsertInput> _GenericServices;
        #endregion

        #region Constructor
        public PaymentLogInsertController(IGenericServices<PaymentLogInsertOutput, PaymentLogInsertInput> GenericServices)
        {
            this._GenericServices = GenericServices;  //new GenericServices<PaymentLogInsertInputOutput.PaymentLogInsertOutput, PaymentLogInsertInputOutput.PaymentLogInsertInput>();
        }
        #endregion

        public HttpResponseMessage Post(PaymentLogInsertInput input)
        {
            List<PaymentLogInsertOutput> outputList = new List<PaymentLogInsertOutput>();
            try
            {
                if (input != null)
                {
                    outputList = _GenericServices.GenericMethod
                        (input, PaymentLogInsertDBMethods.GetParameters(input), PaymentLogInsertDBMethods.GetSpName(), PaymentLogInsertDBMethods.GetConnName());
                }
                else
                {
                    Log.Info("Input details not found");
                    PaymentLogInsertOutput output = new PaymentLogInsertOutput();
                    output.errcode = -1;
                    output.errmsg = "Input details not found!";
                    outputList.Add(output);
                }
            }
            catch (Exception ex)
            {
                PaymentLogInsertOutput output = new PaymentLogInsertOutput();
                output.errcode = -1;
                output.errmsg = "Input details not found!";
                outputList.Add(output);
            }
            return Request.CreateResponse(HttpStatusCode.OK, outputList);
        }

    }
}
