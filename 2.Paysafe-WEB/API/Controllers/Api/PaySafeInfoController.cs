﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using Vectone.Services.PaySafeServices;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net;
using System.Net.Http;
using Vectone.PSC;
using System.Globalization;
using Vectone.Core.Domain.PaySafe;
using System.Configuration;
using NLog;
using System.Text.RegularExpressions;


namespace Vectone.WebApi.Controllers.Api
{
    [WebApiAuthenticate]
    public class PaySafeInfoController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        public HttpResponseMessage Post(PaySafeRequest _paysaferequest)
        {
            Log.Info("PaySafeInfoController");
            PaySafeResponseInfo output = new PaySafeResponseInfo();
            try
            {
                if (_paysaferequest != null)
                {
                    Log.Info("Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(_paysaferequest));
                    PaySafeService _paysafeService = new PaySafeService();
                    var query = _paysafeService.GetInfo(_paysaferequest);
                }
                else
                {
                    Log.Info("Input details not found");
                    output = new PaySafeResponseInfo();
                    output.errcode = -1;
                    output.errmsg = "Input details not found!";
                }
            }
            catch (Exception ex)
            {
                Log.Error("PaySafeInfoController : " + ex.Message);
                output = new PaySafeResponseInfo();
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            Log.Info("Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(output));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }


    }

}
