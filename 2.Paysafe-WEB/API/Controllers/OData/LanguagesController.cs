﻿using System.Web.Http;
using Vectone.Core.Domain.Localization;
using Vectone.Services.Localization;
using Vectone.Web.Framework.WebApi;
using Vectone.Web.Framework.WebApi.OData;
using Vectone.Web.Framework.WebApi.Security;

namespace Vectone.WebApi.Controllers.OData
{
	[WebApiAuthenticate(Permission = "ManageLanguages")]
	public class LanguagesController : WebApiEntityController<Language, ILanguageService>
	{
		protected override void Insert(Language entity)
		{
			Service.InsertLanguage(entity);
		}
		protected override void Update(Language entity)
		{
			Service.UpdateLanguage(entity);
		}
		protected override void Delete(Language entity)
		{
			Service.DeleteLanguage(entity);
		}

		[WebApiQueryable]
		public SingleResult<Language> GetLanguage(int key)
		{
			return GetSingleResult(key);
		}
	}
}
