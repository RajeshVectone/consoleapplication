﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using Vectone.Services.PaySafeServices;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net;
using System.Net.Http;
using Vectone.PSC;
using System.Globalization;
using Vectone.Core.Domain.PaySafe;
using System.Configuration;
using NLog;
using System.Text.RegularExpressions;


namespace Vectone.WebApi.Controllers.Api
{
    [WebApiAuthenticate]
    public class PaySafeController : ApiController
    {
        private PSC.SOPGClassicMerchantClient client;
        //protected PaySafeService _paySafeService = new PaySafeService();

        private readonly IPaySafeService _paySafeService;

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public PaySafeController()
        {
            try
            {
                // Create a new instance of MerchantClient. In this example a exception would be throwed in the
                // constructor, if the given language is invalid.
                client = new PSC.SOPGClassicMerchantClient(true, "EN", true, DevStatus.TEST);

                // Each event from PSC SDK must be redirected to this application:
                client.logMessageEvent += new PSC.SOPGClassicMerchantClient.logMessageEventHandler(client_logMessageEvent);
            }

            catch (ArgumentException argEx)
            {

            }
        }

        public PaySafeController(IPaySafeService paySafeService)
        {
            this._paySafeService= paySafeService;
        }


        public HttpResponseMessage Post(PaySafeRequest _paysaferequest)
        {
            string datetime = System.DateTime.Now.ToShortDateString();

            Log.Debug("--Start--" + datetime);
            Log.Debug("Entering Pay Safe Card.");

            client.DeleteLogMessages(Logging.LogType.DEBUG);
            client.DeleteLogMessages(Logging.LogType.ERROR);
            client.DeleteLogMessages(Logging.LogType.INFO);
            client.DeleteLogMessages(Logging.LogType.WARNING);

            // Converts floating point numbers (float/double) to strings with two decimal digits:
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalDigits = 2;


            client.SetUsername(ConfigurationManager.AppSettings["PaysafeUsername"]);
            client.SetPassword(ConfigurationManager.AppSettings["PaysafePassword"]);
            client.SetMtID(_paysaferequest.MtId);
            client.SetSubID(ConfigurationManager.AppSettings["PaysafeSubID"]);
            client.SetAmount(_paysaferequest.Amount);
            client.SetCurrency(_paysaferequest.Currency);
            client.SetOkURL(_paysaferequest.OkUrl);
            client.SetnOKURL(_paysaferequest.NokUrl);
            client.SetMerchantClientID(ConfigurationManager.AppSettings["PaysafeClientID"]);
            client.SetPnURL(ConfigurationManager.AppSettings["PaysafePnUrl"]);
            client.SetShopID(ConfigurationManager.AppSettings["PaysafeShopID"]);
            client.SetShopLabel(ConfigurationManager.AppSettings["PaysafeShopLabel"]);

            client.SetRestrictedCountry("DE");
            client.SetRestrictedCountry("GB");

            //client.SetMinAge("12");
            //client.SetMinKycLevel("SIMPLE");

            // In this example it's a test:
            client.SetStatus(ConfigurationManager.AppSettings["Mode"]);

            Log.Debug("PaySafeRequest Successufully Passed.");

            Log.Debug("Calling SoapClient - Merchant Data.");

            SOPGClassicMerchantClient.com.paysafecard.soatest.GetMidReturn midRet = client.ConfirmMerchantData(_paysaferequest.Currency);


            if (midRet.resultCode == 0 && midRet.errorCode == 0)
            {
                Log.Debug("Creating Disposition.");

                

                var refId1 = "CTP" + _paysaferequest.Currency + "-PSC-" + _paysaferequest.Amount;
                var refId2 = GenerateId();

                var refId = refId1 + refId2;

                //No Error Log
                SOPGClassicMerchantClient.com.paysafecard.soatest.CreateDispositionReturn ret = client.CreateDisposition();

                List<PaySafeResponse> paySafeResponse = new List<PaySafeResponse>();

                _paysaferequest.siteCode = "LO2";
                _paysaferequest.appCode = "CTP" + _paysaferequest.Currency;

                _paysaferequest.Sitecode = _paysaferequest.siteCode;
                _paysaferequest.Applicationcode = _paysaferequest.appCode;

                string productCode = "PSC-" + _paysaferequest.Amount;

                _paysaferequest.Productcode = productCode;
                _paysaferequest.Paymentagent = "PS";
                _paysaferequest.Servicetype = "MVNO";
                _paysaferequest.Paymentmode = "PSC";
                _paysaferequest.Paymentstep = 1;
                _paysaferequest.Paymentstatus = 1;

                _paysaferequest.accountId = "02108934";

                _paysaferequest.Accountid = _paysaferequest.accountId;
                _paysaferequest.Totalamount = _paysaferequest.Amount;
                _paysaferequest.Currency = _paysaferequest.Currency;
                _paysaferequest.Merchantid = "";
                _paysaferequest.Providercode = "PS";
                _paysaferequest.Csreasoncode = "";
                _paysaferequest.Generalerrorcode = 0;
                _paysaferequest.Returnerror = 0;
                _paysaferequest.SendToProduction = "True";
                _paysaferequest.Email = _paysaferequest.email;
                _paysaferequest.IPpaymentagent = "192.168.2.102";
                _paysaferequest.IPClient = "192.168.2.102";

                //PaySafeService paySafeService = new PaySafeService();
                _paySafeService.InsertPaymentData(_paysaferequest);

                var response = new PaySafeResponse
                {
                    Errorcode = ret.errorCode,
                    MID = ret.mid,
                    MtID = ret.mtid,
                    Resultcode = ret.resultCode,
                    SubID = ret.subId,
                    ReturnCustomerURL = client.GetCustomerPanelURL()
                };


                Log.Debug("Disposition Process Completed." + "Error Code : " + ret.errorCode + "Result code : " + ret.resultCode);
                Log.Debug("--End--" + datetime);


                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, response);
            }
            else
            {
                Log.Debug("Wrong Username or password.");

                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, "Username or password wrong.");
            }
        }

        public static string GenerateId(int count = 4)
        {
            var guid = Guid.NewGuid().ToString("N");
            var rgx = new Regex("[^0-9]");
            var str = rgx.Replace(guid, string.Empty);

            return str.Substring(0, count);
        }

        void client_logMessageEvent(object sender, Logging.LogMessage logMessage)
        {
            string logTypeStr = "";

            switch (logMessage.logType)
            {
                case Logging.LogType.DEBUG: logTypeStr = "DEBUG"; break;
                case Logging.LogType.ERROR: logTypeStr = "ERROR"; break;
                case Logging.LogType.INFO: logTypeStr = "INFO"; break;
                case Logging.LogType.WARNING: logTypeStr = "WARNING"; break;
            }

            if (logMessage.logType == Logging.LogType.DEBUG)
            {


                return;
            }


        }
    }

}
