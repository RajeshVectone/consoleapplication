﻿using Vectone.Web.Framework.WebApi.Security;
using System.Web.Http;
using System.Collections.Generic;
using Vectone.Services.Users;
using System.Linq;
using Vectone.Web.Framework.WebApi.OData;
using System;
using Vectone.Core.Domain.Users;
using System.Net.Http;
using System.Net;

namespace Vectone.WebApi.Controllers.Api
{
    [WebApiAuthenticate]
    public class UsersController : ApiController
    {
        private readonly Lazy<IUserService> _userService;

        public UsersController(Lazy<IUserService> userService)
        {
            this._userService = userService;
        }

        ////[WebApiQueryable]
        ////public IQueryable<User> GetAllNewUsers()
        //public IEnumerable<User> GetAllNewUsers()
        //{

        //    var query = _userService.Value
        //        .GetAllUsers()

        //        .AsQueryable().Select(i=>new User {Id = i.Id, Username = i.Username});

        //    return query;
        //}

        public HttpResponseMessage GetAllNewUsers()
        {

            var query = _userService.Value
                .GetAllUsers()

                .AsQueryable().Select(i => new User { Id = i.Id, Username = i.Username });

            return new HttpResponseMessage()
            {
                Content = new StringContent(Newtonsoft.Json.Linq.JArray.FromObject(query).ToString(), System.Text.Encoding.UTF8, "application/json")
            };       
        }
        public HttpResponseMessage Post(User value)
        {

            var query = _userService.Value
                .GetAllUsers()

                .AsQueryable().Select(i => new User { Id = value.Id, Username = i.Username });

            return new HttpResponseMessage()
            {
                Content = new StringContent(Newtonsoft.Json.Linq.JArray.FromObject(query).ToString(), System.Text.Encoding.UTF8, "application/json")
            };
        }
        //[HttpGet]
        //public IEnumerable<User> GetTest()
        //{
        //    //IEnumerable<User> histories;
        //    //return _userService.Value
        //    //    .GetAllUsers().ToList();
        //    User user = new User();
        //    user.Id = 1;
        //    user.Username="test";
        //    List<User> lstUser = new List<User>();
        //    lstUser.Add(user);
        //    return lstUser;
        //}

        //[HttpGet]
        //public HttpResponseMessage GetTest()
        //{
        //    //IEnumerable<User> histories;
        //    //return _userService.Value
        //    //    .GetAllUsers().ToList();
        //    User user = new User();
        //    user.Id = 1;
        //    user.Username = "test";
        //    List<User> lstUser = new List<User>();
        //    lstUser.Add(user);
        //    return Request.CreateResponse(HttpStatusCode.OK, lstUser.ToArray());
        //    //return lstUser;
        //}

        //public HttpResponseMessage GetTest()
        //{
        //    //IEnumerable<User> histories;
        //    //return _userService.Value
        //    //    .GetAllUsers().ToList();
        //    User user = new User();
        //    user.Id = 1;
        //    user.Username = "test";
        //    List<User> lstUser = new List<User>();
        //    lstUser.Add(user);

        //    return new HttpResponseMessage()
        //    {
        //        Content = new StringContent(Newtonsoft.Json.Linq.JArray.FromObject(lstUser).ToString(), System.Text.Encoding.UTF8, "application/json")
        //    };       
        //    //return lstUser;
        //}

        //public HttpResponseMessage Get(string userId)
        //{
        //    try
        //    {
        //        var result = _userService.Value
        //        .GetAllUsers().ToList();
        //        return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        //    }
        //    catch (WebException ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<User>());
        //    }

        //}
    }

}
