﻿using System.Web.Mvc;
using System.Web.Routing;
using Vectone.Web.Framework.Mvc.Routes;

namespace Vectone.WebApi
{
	public partial class RouteProvider : IRouteProvider
	{
		public void RegisterRoutes(RouteCollection routes)
		{
            routes.MapRoute("Vectone.WebApi.Action",
				"API/Vectone.WebApi/{action}", 
				new { controller = "WebApi" }, 
				new[] { "Vectone.WebApi.Controllers" }
			)
			.DataTokens["area"] = "Vectone.WebApi";

            
		}
		public int Priority
		{
			get
			{
				return 0;
			}
		}
	}
}
