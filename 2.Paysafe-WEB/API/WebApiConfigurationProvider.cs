﻿using System.Web.Http.OData.Builder;
using Vectone.Core.Domain.Common;
using Vectone.Core.Domain.Configuration;
using Vectone.Core.Domain.Localization;
using Vectone.Core.Domain.Users;
using Vectone.Core.Domain.Vectones;
using Vectone.Web.Framework.WebApi.Configuration;

namespace Vectone.WebApi
{
    public partial class WebApiConfigurationProvider : IWebApiConfigurationProvider
    {
        private void AddActionsToUser(EntityTypeConfiguration<User> config)
        {
            //config.Action("AllUsers")
            //    .ReturnsCollectionFromEntitySet<User>(WebApiOdataEntitySet.Users);

           
        }
        public void Configure(WebApiConfigurationBroadcaster configData)
        {
            var m = configData.ModelBuilder;

            m.EntitySet<User>(WebApiOdataEntitySet.Users);
            m.EntitySet<GenericAttribute>(WebApiOdataEntitySet.GenericAttributes);
            m.EntitySet<Language>(WebApiOdataEntitySet.Languages);
            m.EntitySet<LocalizedProperty>(WebApiOdataEntitySet.LocalizedPropertys);
            m.EntitySet<Setting>(WebApiOdataEntitySet.Settings);
            m.EntitySet<VectoneSite>(WebApiOdataEntitySet.Vectones);
            m.EntitySet<VectoneMapping>(WebApiOdataEntitySet.VectoneMappings);
            AddActionsToUser(m.Entity<User>());
        }

        public int Priority { get { return 0; } }
    }

    public static class WebApiOdataEntitySet
    {
        public static string Users { get { return "Users"; } }

        public static string GenericAttributes { get { return "GenericAttributes"; } }

        public static string Languages { get { return "Languages"; } }

        public static string LocalizedPropertys { get { return "LocalizedPropertys"; } }

        public static string Settings { get { return "Settings"; } }

        public static string Vectones { get { return "Vectones"; } }

        public static string VectoneMappings { get { return "VectoneMappings"; } }

        public static string UrlRecords { get { return "UrlRecords"; } }

        public static string Automations { get { return "Automations"; } }
    }
}