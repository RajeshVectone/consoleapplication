﻿using Vectone.Core.Domain.Users;
using Vectone.Core.Domain.Security;
using Vectone.Services.Security;
using System.Collections.Generic;

namespace Vectone.WebApi.Security
{
	public partial class WebApiPermissionProvider : IPermissionProvider
	{
		public static readonly PermissionRecord ManageWebApi = new PermissionRecord { Name = "Manage Web-API.", SystemName = "ManageWebApi", Category = "API" };

		public virtual IEnumerable<PermissionRecord> GetPermissions()
		{
			return new[] { ManageWebApi };
		}

		public virtual IEnumerable<DefaultPermissionRecord> GetDefaultPermissions()
		{
			//return Enumerable.Empty<DefaultPermissionRecord>();

			//uncomment code below in order to give appropriate permissions to admin by default
			return new[] 
            {
                new DefaultPermissionRecord 
                {
                    UserRoleSystemName = SystemUserRoleNames.Administrators,
                    PermissionRecords = new[] { ManageWebApi }
                },
            };
		}
	}
}
