﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vectone.WebApi.Models
{
    public class QueryAPIResult
    {
        public string Mobile_No { get; set; }
        public string Email { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
    }
}