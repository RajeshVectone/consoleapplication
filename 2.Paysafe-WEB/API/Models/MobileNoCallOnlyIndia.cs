﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vectone.WebApi.Models
{
    public class MobileNoCallOnlyIndia
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int RuleId { get; set; }
        public int Year { get; set; }
        public int CallCount { get; set; }
    }
}