﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Vectone.Core")]
[assembly: InternalsVisibleTo("Vectone.Services.Tests")]
[assembly: InternalsVisibleTo("Vectone.Web.Mvc.Tests")]
