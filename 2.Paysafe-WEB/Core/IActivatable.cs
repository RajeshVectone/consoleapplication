﻿using System;

namespace Vectone.Core
{
	public interface IActivatable
	{
		bool IsActive { get; }
	}
}
