﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;


namespace Vectone.Core.Events
{
	/// <summary>
	/// to initialize scheduled tasks in Application_Start
	/// </summary>
	/// <remarks>codehint: vt-add</remarks>
	public class AppInitScheduledTasksEvent
	{
		
	}
}
