﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Vectone.Core.Events
{
	/// <summary>
	/// to register global filters in Application_Start
	/// </summary>
	/// <remarks>codehint: vt-add</remarks>
	public class AppRegisterGlobalFiltersEvent
	{
		public GlobalFilterCollection Filters { get; set; }
	}
}
