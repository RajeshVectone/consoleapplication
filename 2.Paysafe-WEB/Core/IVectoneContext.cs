﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vectone.Core.Domain.Vectones;

namespace Vectone.Core
{
	/// <summary>
	/// Vectone context
	/// </summary>
	public interface IVectoneContext
	{
		/// <summary>
		/// Gets or sets the current vectone
		/// </summary>
        VectoneSite CurrentVectone { get; }

		/// <summary>
		/// IsSingleVectoneMode ? 0 : CurrentVectone.Id
		/// </summary>
		/// <remarks>codehint: vt-add</remarks>
		int CurrentVectoneIdIfMultiVectoneMode { get; }
	}
}
