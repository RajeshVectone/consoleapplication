using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;

namespace Vectone.Core
{

    /// <summary>
    /// Base class for entities
    /// </summary>
    [DataContract]
    public abstract partial class BaseEntity2
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>


        public override bool Equals(object obj)
        {
            return Equals(obj as BaseEntity2);
        }

        private static bool IsTransient(BaseEntity2 obj)
        {
            return false;
        }

        public Type GetUnproxiedType()
        {
            var t = GetType();
            if (t.AssemblyQualifiedName.StartsWith("System.Data.Entity."))
            {
                // it's a proxied type
                t = t.BaseType;
            }
            return t;
        }

        public virtual bool Equals(BaseEntity2 other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!IsTransient(this) &&
                !IsTransient(other))
            {
                var otherType = other.GetUnproxiedType();
                var thisType = GetUnproxiedType();
                return thisType.IsAssignableFrom(otherType) ||
                        otherType.IsAssignableFrom(thisType);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public static bool operator ==(BaseEntity2 x, BaseEntity2 y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(BaseEntity2 x, BaseEntity2 y)
        {
            return !(x == y);
        }
    }
}
