﻿using System;

namespace Vectone.Core
{
    public enum HttpSecurityMode
    {
        Unsecured,
        Ssl,
        SharedSsl
    }
}
