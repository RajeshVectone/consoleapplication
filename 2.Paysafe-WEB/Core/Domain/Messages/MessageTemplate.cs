﻿using Vectone.Core.Domain.Localization;
using Vectone.Core.Domain.Vectones;

namespace Vectone.Core.Domain.Messages
{
    /// <summary>
    /// Represents a message template
    /// </summary>
	public partial class MessageTemplate : BaseEntity, ILocalizedEntity, IVectoneMappingSupported
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the BCC Email addresses
        /// </summary>
        public string BccEmailAddresses { get; set; }

        /// <summary>
        /// Gets or sets the subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the template is active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the used email account identifier
        /// </summary>
        public int EmailAccountId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the entity is limited/restricted to certain vectonesites
		/// </summary>
		public bool LimitedToVectones { get; set; }
    }
}
