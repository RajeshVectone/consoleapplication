﻿using Vectone.Core.Configuration;

namespace Vectone.Core.Domain.Messages
{
    public class EmailAccountSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a vectone default email account identifier
        /// </summary>
        public int DefaultEmailAccountId { get; set; }

    }

}
