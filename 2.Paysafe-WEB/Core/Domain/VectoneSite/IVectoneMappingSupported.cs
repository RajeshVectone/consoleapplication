﻿namespace Vectone.Core.Domain.Vectones
{
	/// <summary>
	/// Represents an entity which supports vectone mapping
	/// </summary>
	public partial interface IVectoneMappingSupported
	{
		/// <summary>
		/// Gets or sets a value indicating whether the entity is limited/restricted to certain vectonesites
		/// </summary>
		bool LimitedToVectones { get; set; }
	}
}
