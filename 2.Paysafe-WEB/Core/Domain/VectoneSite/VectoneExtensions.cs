﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Vectone.Core.Domain.Vectones
{
	public static class VectoneExtensions
	{
		/// <summary>
		/// Parse comma-separated Hosts
		/// </summary>
		/// <param name="vectone">Vectone</param>
		/// <returns>Comma-separated hosts</returns>
        public static string[] ParseHostValues(this VectoneSite vectone)
		{
			if (vectone == null)
				throw new ArgumentNullException("vectone");

			var parsedValues = new List<string>();
			if (!String.IsNullOrEmpty(vectone.Hosts))
			{
				string[] hosts = vectone.Hosts.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string host in hosts)
				{
					var tmp = host.Trim();
					if (!String.IsNullOrEmpty(tmp))
						parsedValues.Add(tmp);
				}
			}
			return parsedValues.ToArray();
		}

		/// <summary>
		/// Indicates whether a vectone contains a specified host
		/// </summary>
		/// <param name="vectone">Vectone</param>
		/// <param name="host">Host</param>
		/// <returns>true - contains, false - no</returns>
        public static bool ContainsHostValue(this VectoneSite vectone, string host)
		{
			if (vectone == null)
				throw new ArgumentNullException("vectone");

			if (String.IsNullOrEmpty(host))
				return false;

			var contains = vectone.ParseHostValues()
								.FirstOrDefault(x => x.Equals(host, StringComparison.InvariantCultureIgnoreCase)) != null;
			return contains;
		}
	}
}
