﻿using System.Runtime.Serialization;

namespace Vectone.Core.Domain.Vectones
{
	/// <summary>
	/// Represents a vectone mapping record
	/// </summary>
	[DataContract]
	public partial class VectoneMapping : BaseEntity
	{
		/// <summary>
		/// Gets or sets the entity identifier
		/// </summary>
		[DataMember]
		public virtual int EntityId { get; set; }

		/// <summary>
		/// Gets or sets the entity name
		/// </summary>
		[DataMember]
		public virtual string EntityName { get; set; }

		/// <summary>
		/// Gets or sets the vectone identifier
		/// </summary>
		[DataMember]
		public virtual int VectoneId { get; set; }
	}
}
