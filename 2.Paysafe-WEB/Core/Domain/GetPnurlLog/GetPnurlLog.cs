﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vectone.Core.Domain;



namespace Vectone.Core.Domain.GetPnurlLog
{
    public class GetPnurlLogInput
    {
        public string mobileno { get; set; }
        public string refId { get; set; }
    }

    public class GetPnurlLogOutput : Output
    {
        public string SerialNumbers { get; set; }
        public string EventType { get; set; }
        public int ISDebited { get; set; }

    }

    public static class GetPnurlLogDBMethods
    {
        public static object GetParameters(GetPnurlLogInput input)
        {
            object param = new
            {
                @mobileno = input.mobileno,
                @refId = input.refId

            };
            return param;
        }

        public static string GetSpName()
        {
            return "PaySafe_Get_Pnurl_Log";
        }

        public static string GetConnName()
        {
            return "payment_20160301";
        }
    }

}
