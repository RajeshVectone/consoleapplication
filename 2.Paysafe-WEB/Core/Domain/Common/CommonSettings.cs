﻿
using Vectone.Core.Configuration;

namespace Vectone.Core.Domain.Common
{
    public class CommonSettings : ISettings
    {
		public CommonSettings()
		{
			UseVectonedProceduresIfSupported = true;
			
		
			FullTextMode = FulltextSearchMode.ExactMatch;
		}
		
		public bool UseSystemEmailForContactUsForm { get; set; }

        public bool UseVectonedProceduresIfSupported { get; set; }

     
        /// <summary>
        /// Gets a sets a value indicating whether to display a warning if java-script is disabled
        /// </summary>
        public bool DisplayJavaScriptDisabledWarning { get; set; }

        /// <summary>
        /// Gets a sets a value indicating whether to full-text search is supported
        /// </summary>
        public bool UseFullTextSearch { get; set; }

        /// <summary>
        /// Gets a sets a Full-Text search mode
        /// </summary>
        public FulltextSearchMode FullTextMode { get; set; }

    }
}