﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.InsertCurrencyExchange
{

    public class InsertCurrencyExchangeInput
    {
        public string Reference_Id { get; set; }
        public string MobileNo { get; set; }
        public string acc_currency { get; set; }
        public string ccdc_currency { get; set; }
        public double Current_Rate { get; set; }
        public double Conversion_Amount { get; set; }
        public string siteCode { get; set; }
    }

    public class InsertCurrencyExchangeOutput
    {
        public int Errorcode { get; set; }
        public string ErrorMsg { get; set; }
    }

    public static class InsertCurrencyExchangeDBMethods
    {
        public static object GetParameters(InsertCurrencyExchangeInput input)
        {
            object param = new
            {
                @Reference_Id = input.Reference_Id,
                @MobileNo = input.MobileNo,
                @acc_currency = input.acc_currency,
                @ccdc_currency = input.ccdc_currency,
                @Current_Rate = input.Current_Rate,
                @Conversion_Amount = input.Conversion_Amount
            };
            return param;
        }

        public static string GetSpName()
        {
            return "paysaf_insert_Currency_Exchange";
        }

        public static string GetConnName(string siteCode)
        {
            if (siteCode == "BSE")
                return "ESP.SE";
            if(siteCode == "BAU")
                return "ESP.AT";
            if (siteCode == "BNL")
                return "ESP.NL";
            return "esp";
        }
    }

}
