﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.InsertPnurlLog
{
    public class InsertPnurlLogInput
    {
        public string mobileNo { get; set; }
        public string refId { get; set; }
        public string serialNumbers { get; set; }
        public string eventType { get; set; }
        public int ISDebited { get; set; }
    }

    public class InsertPnurlLogOutput : Output
    {

    }

    public static class InsertPnurlLogDBMethods
    {
        public static object GetParameters(InsertPnurlLogInput input)
        {
            object param = new
            {
                @mobileNo = input.mobileNo,
                @refId = input.refId,
                @serialNumbers = input.serialNumbers,
                @eventType = input.eventType,
                @ISDebited = input.ISDebited
            };
            return param;
        }

        public static string GetSpName()
        {
            return "PaySafe_Insert_Pnurl_Log";
        }

        public static string GetConnName()
        {
            return "payment_20160301";
        }
    }

}
