﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.InsertMerchantRefDtls
{
    public class InsertMerchantRefDtlsInput
    {
        public string sitecode { get; set; }
        public string applicationcode { get; set; }
        public string reference_id { get; set; }
        public string accountid { get; set; }
        public string email { get; set; }
        public float amount { get; set; }
        public float vat_amount { get; set; }
        public float vatPercentage { get; set; }
        public string total_amount { get; set; }
        public string IPClient { get; set; }
        public string currency { get; set; }
        public string OrderNumber { get; set; }
        public string mobile { get; set; }
        public string name { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string postCode { get; set; }
        public string country { get; set; }
        public string pnURL { get; set; }
        public string OkUrl { get; set; }
        public string NokUrl { get; set; }
    }

    public class InsertMerchantRefDtlsOutput : Output
    {
    }

    public static class InsertMerchantRefDtlsDBMethods
    {
        public static object GetParameters(InsertMerchantRefDtlsInput input)
        {
            object param = new
            {
                @sitecode = input.sitecode,
                @applicationcode = input.applicationcode,
                @reference_id = input.reference_id,
                @accountid = input.accountid,
                @email = input.@email,
                @amount = input.amount,
                @vat_amount = input.vat_amount,
                @vatPercentage = input.vatPercentage,
                @total_amount = input.total_amount,
                @IPClient = input.IPClient,
                @currency = input.currency,
                @OrderNumber = input.OrderNumber,
                @mobile = input.mobile,
                @name = input.name,
                @street = input.street,
                @city = input.city,
                @postCode = input.postCode,
                @country = input.country,
                @pnURL = input.pnURL,
                @OkUrl = input.OkUrl,
                @NokUrl = input.NokUrl

            };
            return param;
        }

        public static string GetSpName()
        {
            return "paysaf_insert_merchant_ref_details";
        }

        public static string GetConnName()
        {
            return "payment_20160301";
        }
    }

}
