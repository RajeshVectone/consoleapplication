﻿using Vectone.Core.Configuration;

namespace Vectone.Core.Domain
{
    public class VectoneInformationSettings : ISettings
    {
		public VectoneInformationSettings()
		{
			VectoneClosedAllowForAdmins = true;
		}
		
		/// <summary>
        /// Gets or sets a value indicating whether vectone is closed
        /// </summary>
        public bool VectoneClosed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether administrators can visit a closed vectone
        /// </summary>
        public bool VectoneClosedAllowForAdmins { get; set; }

        // codehint: vt-delete

        /// <summary>
        /// Gets or sets a value indicating whether mini profiler should be displayed in public vectone (used for debugging)
        /// </summary>
        public bool DisplayMiniProfilerInPublicVectone { get; set; }
    }
}
