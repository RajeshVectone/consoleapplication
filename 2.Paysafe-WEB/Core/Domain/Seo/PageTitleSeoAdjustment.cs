﻿namespace Vectone.Core.Domain.Seo
{
    /// <summary>
    /// Represents an editor type
    /// </summary>
    public enum PageTitleSeoAdjustment
    {
        /// <summary>
      
        /// </summary>
        PagenameAfterVectonename = 0,
        /// <summary>
        /// Vectonename comes after pagename
        /// </summary>
        VectonenameAfterPagename = 10
    }
}
