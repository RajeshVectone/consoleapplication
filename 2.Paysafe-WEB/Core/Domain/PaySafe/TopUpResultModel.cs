﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class TopUpResultModel
    {
        public int ErrCode { get; set; }
        public string Errmsg { get; set; }
        public string IncentifData { get; set; }
        public int? TopuplogId { get; set; }
        public string NotifMsg { get; set; }
    }
}
