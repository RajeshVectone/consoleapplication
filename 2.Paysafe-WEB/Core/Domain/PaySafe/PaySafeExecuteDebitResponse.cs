﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaySafeExecuteDebitResponse
    {
        private int _Errorcode;

        public int Errorcode
        {
            get { return _Errorcode; }
            set { _Errorcode = value; }
        }
       
        private string _mtID;

        public string MtID
        {
            get { return _mtID; }
            set { _mtID = value; }
        }
        private string[] _subID;

        public string[] SubID
        {
            get { return _subID; }
            set { _subID = value; }
        }
        private string _amount;

       
        private int _resultcode;

        public int Resultcode
        {
            get { return _resultcode; }
            set { _resultcode = value; }
        }

       

    }
}
