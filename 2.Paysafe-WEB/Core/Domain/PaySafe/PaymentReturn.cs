﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaymentReturn
    {

        private string _ref_id;

        public string Ref_id
        {
            get { return _ref_id; }
            set { _ref_id = value; }
        }
        private int _orderno;

        public int Orderno
        {
            get { return _orderno; }
            set { _orderno = value; }
        }
        private int _generalerror;

        public int Generalerror
        {
            get { return _generalerror; }
            set { _generalerror = value; }
        }
        private string _errorMessage;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
    }
}
