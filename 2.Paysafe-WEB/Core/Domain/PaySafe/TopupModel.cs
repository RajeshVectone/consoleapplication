﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class TopupModel
    {
        public string TopupId { get; set; }
        public string Pin { get; set; }
        public string MobileNo { get; set; }
        public string IccId { get; set; }
        public double? Amount { get; set; }
        public string Currency { get; set; }
        public string CountryId { get; set; }
        public string SiteCode { get; set; }
    }
}
