﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaySafeTopUpRedeem
    {
        [DataMember(Order = 3)]
        public string AccountNumber
        {
            get;
            set;
        }

        [DataMember(Order = 8)]
        public decimal AfterBalance
        {
            get;
            set;
        }

        [DataMember(Order = 7)]
        public string Currency
        {
            get;
            set;
        }

        [DataMember(Order = 99)]
        public int errCode
        {
            get;
            set;
        }

        [DataMember(Order = 100)]
        public string errMsg
        {
            get;
            set;
        }

        [DataMember(Order = 9)]
        public string Merchant_Ref_Code
        {
            get;
            set;
        }

        [DataMember(Order = 6)]
        public string Product
        {
            get;
            set;
        }

        [DataMember(Order = 2)]
        public string Sitecode
        {
            get;
            set;
        }

        [DataMember(Order = 5)]
        public float Topup_Amount
        {
            get;
            set;
        }

        [DataMember(Order = 4)]
        public string MtID
        {
            get;
            set;
        }

        //[DataMember(Order = 1)]
        //public string website
        //{
        //    get;
        //    set;
        //}
        [DataMember(Order = 12)]
        public string OrderId
        {
            get;
            set;
        }
        [DataMember(Order = 13)]
        public string transactionId
        {
            get;
            set;
        }
        [DataMember(Order = 18)]
        public string Name
        {
            get;
            set;
        }
        public PaySafeTopUpRedeem()
        {
        }

        public PaySafeTopUpRedeem(string sitecode, string accountNumber, string voucher_Number)
        {
            this.Sitecode = sitecode;
            this.AccountNumber = accountNumber;
            //this.Voucher_Number = voucher_Number;
           // this.website = webSite;
            this.Topup_Amount = 0f;
            this.errCode = 0;
            this.errMsg = "Ok";
        }

        public PaySafeTopUpRedeem(string sitecode, string accountNumber, string voucher_Number,  float voucher_Value, string currency, string product)
        {
            this.Sitecode = sitecode;
            this.AccountNumber = accountNumber;
            this.MtID = voucher_Number;
            //this.website = webSite;
            this.Topup_Amount = voucher_Value;
            this.Currency = currency;
            this.Product = product;
            this.AfterBalance = new decimal(0);
            this.errCode = 0;
            this.errMsg = "Ok";
        }

        public PaySafeRequest PaySafeRequest
        {
            get;
            set;
        }
        public PaySafeTransaction PaySafeTransaction
        {
            get;
            set;
        }
    }
}
