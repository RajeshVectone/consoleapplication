﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaySafeResponseInfo : Output
    {
        private string _sitecode;

        public string Sitecode
        {
            get { return _sitecode; }
            set { _sitecode = value; }
        }
        private string _applicationcode;

        public string Applicationcode
        {
            get { return _applicationcode; }
            set { _applicationcode = value; }
        }
        private string _reference_id;

        public string Reference_id
        {
            get { return _reference_id; }
            set { _reference_id = value; }
        }
        private string _accountid;

        public string Accountid
        {
            get { return _accountid; }
            set { _accountid = value; }
        }
        private string _email;


        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private double _amount;

        public double Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        private double _vat_amount;

        public double Vat_amount
        {
            get { return _vat_amount; }
            set { _vat_amount = value; }
        }
        private double _vatPercentage;

        public double VatPercentage
        {
            get { return _vatPercentage; }
            set { _vatPercentage = value; }
        }
        private double _total_amount;

        public double Total_amount
        {
            get { return _total_amount; }
            set { _total_amount = value; }
        }
        private string _IPClient;

        public string IPClient
        {
            get { return _IPClient; }
            set { _IPClient = value; }
        }
        private string _currency;

        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        private string _OrderNumber;

        public string OrderNumber
        {
            get { return _OrderNumber; }
            set { _OrderNumber = value; }
        }
        private string _mobile;

        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _street;

        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }
        private string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        private string _postCode;

        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }
        private string _country;

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        private string _pnURL;

        public string PnURL
        {
            get { return _pnURL; }
            set { _pnURL = value; }
        }
        private string _OkUrl;

        public string OkUrl
        {
            get { return _OkUrl; }
            set { _OkUrl = value; }
        }
        private string _NokUrl;

        public string NokUrl
        {
            get { return _NokUrl; }
            set { _NokUrl = value; }
        }


    }
}
