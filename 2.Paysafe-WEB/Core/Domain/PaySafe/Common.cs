﻿#region NameSpace
namespace Vectone.Core.Domain.PaySafe
{
    #region Business Objects
    //15-Oct-2015: Moorthy : Added for get country by mobileno
    public class CommonInput
    {
        public string MobileNo { get; set; }
    }
    //15-Oct-2015: Moorthy : Added for get country by mobileno
    public class CommonOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string Country { get; set; }
    } 
    #endregion
} 
#endregion
