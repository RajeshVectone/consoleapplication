﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaySafeTransaction : Output
    {
        public string changeIssueAmount
        {
            get;
            set;
        }

        public string changeIssueExpiryDate
        {
            get;
            set;
        }

        public string changeIssueVoucherCurr
        {
            get;
            set;
        }

        public string changeIssueVoucherNumber
        {
            get;
            set;
        }

        public string currencyConversion
        {
            get;
            set;
        }

        public int errCode
        {
            get;
            set;
        }
        
        public string errMsg
        {
            get;
            set;
        }

        public string resultCode
        {
            get;
            set;
        }
        //public string errDescription
        //{
        //    get;
        //    set;
        //}

        public string orderNumber
        {
            get;
            set;
        }

        public string settleAmount
        {
            get;
            set;
        }

        public string transactionId
        {
            get;
            set;
        }
        public string MtID
        {
            get;
            set;
        }
        public string txCode
        {
            get;
            set;
        }

        public string txDescription
        {
            get;
            set;
        }

        public string paysafeTransactionId
        {
            get;
            set;
        }
       
        public string ReturnCustomerURL
        {
            get;
            set;
        }
        public PaySafeTransaction()
        {
            
        }
        //private Vat _vat;

        //public Vat Vat
        //{
        //    get { return _vat; }
        //    set { _vat = value; }
        //}
        private string _amount;

        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
    }
}
