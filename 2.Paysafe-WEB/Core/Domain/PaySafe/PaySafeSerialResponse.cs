﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaySafeSerialResponse : Output
    {
        private double _amount;
        public double Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        private string _currency;

        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        private string _customerDetails;

        public string CustomerDetails
        {
            get { return _customerDetails; }
            set { _customerDetails = value; }
        }
        private string _dispositionState;

        public string DispositionState
        {
            get { return _dispositionState; }
            set { _dispositionState = value; }
        }
        private int _Errorcode;

        public int Errorcode
        {
            get { return _Errorcode; }
            set { _Errorcode = value; }
        }
      
        private string _mtID;

        public string MtID
        {
            get { return _mtID; }
            set { _mtID = value; }
        }
        private int _resultcode;
        public int Resultcode
        {
            get { return _resultcode; }
            set { _resultcode = value; }
        }
        private string _serialNumbers;

        public string SerialNumbers
        {
            get { return _serialNumbers; }
            set { _serialNumbers = value; }
        }
        private string[] _subID;

        public string[] SubID
        {
            get { return _subID; }
            set { _subID = value; }
        }
    }
}
