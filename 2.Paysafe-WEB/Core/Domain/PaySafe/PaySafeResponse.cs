﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaySafeResponse
    {

        private int _Errorcode;

        public int Errorcode
        {
            get { return _Errorcode; }
            set { _Errorcode = value; }
        }

        private string _ErrorMsg;

        public string ErrorMsg
        {
            get { return _ErrorMsg; }
            set { _ErrorMsg = value; }
        }

        private string _mID;

        public string MID
        {
            get { return _mID; }
            set { _mID = value; }
        }

        private string _mtID;

        public string MtID
        {
            get { return _mtID; }
            set { _mtID = value; }
        }
        private string[] _subID;

        public string[] SubID
        {
            get { return _subID; }
            set { _subID = value; }
        }
        private string _amount;

        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        private string _currency;

        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        private int _resultcode;

        public int Resultcode
        {
            get { return _resultcode; }
            set { _resultcode = value; }
        }

        private string _returnCustomerURL;

        public string ReturnCustomerURL
        {
            get { return _returnCustomerURL; }
            set { _returnCustomerURL = value; }
        }

        //private string _sitecode;

        //public string Sitecode
        //{
        //    get { return _sitecode; }
        //    set { _sitecode = value; }
        //}
        //private string _applicationcode;

        //public string Applicationcode
        //{
        //    get { return _applicationcode; }
        //    set { _applicationcode = value; }
        //}
        //private string _productcode;

        //public string Productcode
        //{
        //    get { return _productcode; }
        //    set { _productcode = value; }
        //}
        //private string _paymentagent;

        //public string Paymentagent
        //{
        //    get { return _paymentagent; }
        //    set { _paymentagent = value; }
        //}
        //private string _servicetype;

        //public string Servicetype
        //{
        //    get { return _servicetype; }
        //    set { _servicetype = value; }
        //}
        //private string _paymentmode;

        //public string Paymentmode
        //{
        //    get { return _paymentmode; }
        //    set { _paymentmode = value; }
        //}
        //private int _paymentstep;

        //public int Paymentstep
        //{
        //    get { return _paymentstep; }
        //    set { _paymentstep = value; }
        //}
        //private int _paymentstatus;

        //public int Paymentstatus
        //{
        //    get { return _paymentstatus; }
        //    set { _paymentstatus = value; }
        //}
        //private string _accountid;

        //public string Accountid
        //{
        //    get { return _accountid; }
        //    set { _accountid = value; }
        //}
        //private string _totalamount;

        //public string Totalamount
        //{
        //    get { return _totalamount; }
        //    set { _totalamount = value; }
        //}

        //private string _merchantid;

        //public string Merchantid
        //{
        //    get { return _merchantid; }
        //    set { _merchantid = value; }
        //}

        //private string _providercode;

        //public string Providercode
        //{
        //    get { return _providercode; }
        //    set { _providercode = value; }
        //}
        //private string _csreasoncode;

        //public string Csreasoncode
        //{
        //    get { return _csreasoncode; }
        //    set { _csreasoncode = value; }
        //}
        //private int _generalerrorcode;

        //public int Generalerrorcode
        //{
        //    get { return _generalerrorcode; }
        //    set { _generalerrorcode = value; }
        //}
        //private int _returnerror;

        //public int Returnerror
        //{
        //    get { return _returnerror; }
        //    set { _returnerror = value; }
        //}
        //private string _sendToProduction;

        //public string SendToProduction
        //{
        //    get { return _sendToProduction; }
        //    set { _sendToProduction = value; }
        //}
        //private string _email;

        //public string Email
        //{
        //    get { return _email; }
        //    set { _email = value; }
        //}
        //private string _IPpaymentagent;

        //public string IPpaymentagent
        //{
        //    get { return _IPpaymentagent; }
        //    set { _IPpaymentagent = value; }
        //}
        //private string _IPClient;

        //public string IPClient
        //{
        //    get { return _IPClient; }
        //    set { _IPClient = value; }
        //}

        //private string _reference_id;

        //public string Reference_id
        //{
        //    get { return _reference_id; }
        //    set { _reference_id = value; }
        //}

        //private string _requestid;

        //public string Requestid
        //{
        //    get { return _requestid; }
        //    set { _requestid = value; }
        //}


        //private string _requesttoken;

        //public string Requesttoken
        //{
        //    get { return _requesttoken; }
        //    set { _requesttoken = value; }
        //}


        //private string _replyMessage;

        //public string ReplyMessage
        //{
        //    get { return _replyMessage; }
        //    set { _replyMessage = value; }
        //}

    }
}
