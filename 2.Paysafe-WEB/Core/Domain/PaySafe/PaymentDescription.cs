﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaymentDescription
    {
        private string reference_id;

        public string Reference_id
        {
            get { return reference_id; }
            set { reference_id = value; }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private int errcode;

        public int Errcode
        {
            get { return errcode; }
            set { errcode = value; }
        }

        private string errmessage;

        public string Errmessage
        {
            get { return errmessage; }
            set { errmessage = value; }
        }
    }
}
