﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class ReasonCode
    {
        private string _General_Error_Code;

        public string General_Error_Code
        {
            get { return _General_Error_Code; }
            set { _General_Error_Code = value; }
        }

        private int _Provider_Reason_Code;

        public int Provider_Reason_Code
        {
            get { return _Provider_Reason_Code; }
            set { _Provider_Reason_Code = value; }
        }

        private string _Provider_Message;

        public string Provider_Message
        {
            get { return _Provider_Message; }
            set { _Provider_Message = value; }
        }

        private string _Possible_Action;

        public string Possible_Action
        {
            get { return _Possible_Action; }
            set { _Possible_Action = value; }
        }

        private string _Payment_Status;

        public string Payment_Status
        {
            get { return _Payment_Status; }
            set { _Payment_Status = value; }
        }

        private string Provider_Code;

        public string Provider_Code1
        {
            get { return Provider_Code; }
            set { Provider_Code = value; }
        }
    }
}
