﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaySafe
{
    public class PaySafeRequest
    {
        //private string _subID;
        private string _mtId;
        private string _amount;
        private string _currency;
        private string _okUrl;
        private string _nokUrl;
        private string _orderNumber;
        private string _vat;
        private string _pnurl;
        private bool _saverflag;

        public bool Saverflag
        {
            get { return _saverflag; }
            set { _saverflag = value; }
        }

        public string Pnurl
        {
            get { return _pnurl; }
            set { _pnurl = value; }
        }

        //public string Pnurl
        //{
        //    get { return _pnurl; }
        //    set { _pnurl = value; }
        //}

        public string Vat
        {
            get { return _vat; }
            set { _vat = value; }
        }
        private string _vatPercentage;

        public string VatPercentage
        {
            get { return _vatPercentage; }
            set { _vatPercentage = value; }
        }
        private string _total;

        public string Total
        {
            get { return _total; }
            set { _total = value; }
        }

        // private string _country;

        //public string Country
        //{
        //    get { return _country; }
        //    set { _country = value; }
        //}

        public string OrderNumber
        {
            get { return _orderNumber; }
            set { _orderNumber = value; }
        }
        //private string _MerchantClientID;
        //private string _PnUrl;
        //private string _shopID;
        //private string _ShopLabel;

        public string MtId
        {
            get { return _mtId; }
            set { _mtId = value; }
        }


        //public string SubID
        //{
        //    get { return _subID; }
        //    set { _subID = value; }
        //}

        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }


        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }

        public string OkUrl
        {
            get { return _okUrl; }
            set { _okUrl = value; }
        }


        public string NokUrl
        {
            get { return _nokUrl; }
            set { _nokUrl = value; }
        }



        private string _sitecode;

        public string Sitecode
        {
            get { return _sitecode; }
            set { _sitecode = value; }
        }
        private string _applicationcode;

        public string Applicationcode
        {
            get { return _applicationcode; }
            set { _applicationcode = value; }
        }
        private string _productcode;

        public string Productcode
        {
            get { return _productcode; }
            set { _productcode = value; }
        }
        private string _paymentagent;

        public string Paymentagent
        {
            get { return _paymentagent; }
            set { _paymentagent = value; }
        }
        private string _servicetype;

        public string Servicetype
        {
            get { return _servicetype; }
            set { _servicetype = value; }
        }
        private string _paymentmode;

        public string Paymentmode
        {
            get { return _paymentmode; }
            set { _paymentmode = value; }
        }
        private int _paymentstep;

        public int Paymentstep
        {
            get { return _paymentstep; }
            set { _paymentstep = value; }
        }
        private int _paymentstatus;

        public int Paymentstatus
        {
            get { return _paymentstatus; }
            set { _paymentstatus = value; }
        }
        private string _accountid;

        public string Accountid
        {
            get { return _accountid; }
            set { _accountid = value; }
        }
        private string _totalamount;

        public string Totalamount
        {
            get { return _totalamount; }
            set { _totalamount = value; }
        }

        private string _csreasoncode;

        public string Csreasoncode
        {
            get { return _csreasoncode; }
            set { _csreasoncode = value; }
        }


        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _IPpaymentagent;

        public string IPpaymentagent
        {
            get { return _IPpaymentagent; }
            set { _IPpaymentagent = value; }
        }
        private string _IPClient;

        public string IPClient
        {
            get { return _IPClient; }
            set { _IPClient = value; }
        }
        private string _webSite;
        public string WebSite
        {
            get { return _webSite; }
            set { _webSite = value; }
        }
        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _mobile;
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _street;
        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }

        private string _city;
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _postCode;
        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        private string _country;
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        //13-Feb-2019: Moorthy : Added for 'Some phone numbers are not captured in full' issue
        private string _aliasName;
        public string AliasName 
        { 
            get 
            { 
                return _aliasName; 
            } 
            set 
            { 
                _aliasName = value; 
            } 
        }
    }
}
