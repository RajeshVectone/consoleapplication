﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.CurrencyUpdate
{

    public class CurrencyUpdateInput
    {
        public string reference_id { get; set; }
        public string currency { get; set; }
    }

    public class CurrencyUpdateOutput : Output
    {
    }

    public static class CurrencyUpdateDBMethods
    {
        public static object GetParameters(CurrencyUpdateInput input)
        {
            object param = new
            {
                @reference_id=input.reference_id,
                @currency = input.currency
            };
            return param;
        }

        public static string GetSpName()
        {
            return "web_paysaf_currency_update";
        }

        public static string GetConnName()
        {
            return "payment_20160301";
        }
    }

}
