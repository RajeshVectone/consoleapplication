﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.Automation
{
     [DataContract]
    public class AutomationDomain 
    {
        [DataMember]
        public int Rule_Id { get; set; }
        public int User_Id { get; set; }
        public string Name { get; set; }
        public int Group_Id { get; set; }
        public AutomationRuleGroupDomain Group { get; set; }
        public bool Rule_Is_Active { get; set; }
        public int No_Of_Rules_Start_Up { get; set; }

        public bool Rule_Is_Shared { get; set; }
        public int Shared_Min_Intervel { get; set; }

        public bool Rule_Is_Recheck { get; set; }
        public int Recheck_Min_Intrvel { get; set; }

        public List<AutomationEventDomain> Events { get; set; }
        public List<AutomationConditionDomain> Conditions { get; set; }

        public List<AutomationTriggerDomain> Triggers { get; set; }

        public DateTime Create_Date { get; set; }
        public DateTime? Modify_Date { get; set; }

        public bool? IsDeleted { get; set; }
        public bool? IsCancel_Mails { get; set; }

        /*Added on 10 Aug 2015 - by Hari -for Queue Process*/
        //public bool? IsEventsDefined { get; set; }
        //public bool? IsConditionsDefined { get; set; }
        //public bool? IsTriggersDefined { get; set; }
        //public int Status { get; set; }
        public string Group_Name { get; set; }

        public string EventsDefined { get; set; }
        public string ConditionsDefined { get; set; }
        public string TriggersDefined { get; set; }
        public string Status { get; set; }
        public string Status_Updated_By { get; set; }
        public DateTime? Status_Updated_Date { get; set; }
        /*End added*/

        public int? ModifiedBy { get; set; }
        public string CreatedBy { get; set; }

    }
    public class AutomationRuleGroupDomain
    {
        public int Group_Id { get; set; }
        public string Group_Name { get; set; }
    }
    public class AutomationEventDomain
    {
        public int Id { get; set; }
        public AutomationEventTypeDomain Event { get; set; }
        public AutomationEventSiteDomain SiteUrl { get; set; }
    }

    public class AutomationConditionDomain
    {
        public int Id { get; set; }
        public AutomationConditionTypeDomain Condition { get; set; }
        public AutomationLoginStatusDomain Status { get; set; }
    }
    public class AutomationTriggerDomain
    {
        public int Id { get; set; }
        public AutomationTriggerTypeDomain Trigger { get; set; }
        public AutomationEmailTemplateDomain MailTemplate { get; set; }
        public AutomationSMSTemplateDomain SmsTemplate { get; set; }
        public AutomationTriggerTimerDomain Timer { get; set; }
    }

    public class AutomationEventSiteDomain
    {
        public int Url_Id { get; set; }
        public string Url_Name { get; set; }
        //11-Aug-2015 : Moorthy Added for Url Alias
        public string Url_Alias { get; set; }
        //11-Aug-2015 : Moorthy Added for Site Id
        public string Site_Id { get; set; }
    }

    //11-Aug-2015 : Moorthy Added for Api Details
    public class AutomationConditionApiDomain
    {
        public int Api_Id { get; set; }
        public string Api_Name { get; set; }
        public string Api_Url { get; set; }
        //19 Aug 2015 - Added by Hari - for Api Fields
        public string Api_Fields { get; set; }
        //09 Sep 2015 - Added by Hari - for Api Input fields
        public string Api_Input_Fields { get; set; }
        //10 Sep 2015 - Added by Hari - for Api Method
        public string Api_Method { get; set; }
    }

    //13-Aug-2015 : Moorthy Added for Event API mapping
    public class AutomationEventApiDomain
    {
        public int Event_Api_Id { get; set; }
        public int Event_Id { get; set; }
        public string Event_Name { get; set; }
        public int Api_Id { get; set; }
        public string Api_Name { get; set; }        
    }

    public class AutomationEventTypeDomain
    {
        public int Event_Id { get; set; }
        public string Event_Type { get; set; }
    }

    public class AutomationConditionTypeDomain
    {
        public int Condition_Id { get; set; }
        public string Condition_Type { get; set; }
        //11-Aug-2015 : Moorthy Added for Math_Required & Math_Value two fields
        public bool Math_Required { get; set; }
        public string Math_Value { get; set; }
        //13-Aug-2015 : Moorthy Added field for storing Event details
        public int Event_Id { get; set; }
        public string Event_Type { get; set; }
        public List<AutomationEventTypeDomain> Events { get; set; }
        //13-Aug-2015 : Moorthy Added for field api
        public string Api_Field { get; set; }
        //14-Aug-2015 : Moorthy Added field for api field
        public string Math_Field { get; set; }
    }

    public class AutomationLoginStatusDomain
    {
        public int Topup_Login_Id { get; set; }
        public string Status { get; set; }
    }

    public class AutomationEmailTemplateDomain
    {
        public int Email_Content_Id { get; set; }
        public string Email_Subject { get; set; }
        public string Email_Content { get; set; }
    }

    public class AutomationSMSTemplateDomain
    {
        public int SMS_Content_Id { get; set; }
        public string Sms_From { get; set; }
        public string Sms_Content { get; set; }
        public string Sms_Template { get; set; } //Added on 13 Aug 2015 - by Hari
        public string Sms_Content_Type { get; set; } //Added on 21 Aug 2015 - by Hari
    }
    public class AutomationTriggerTypeDomain
    {
        public int Trigger_Id { get; set; }
        public string Trigger_Type { get; set; }
    }
    public class AutomationTriggerTimerDomain
    {
        public int Timer_Id { get; set; }
        public string Timer_Type { get; set; }
        public int? Timer_In_Seconds { get; set; }
    }

    public class InsertRecordResult
    {
        public string Errormsg { get; set; }
        public int Errornum { get; set; }
        public int identity { get; set; }
    }

    public class Automation_Rule_Event
    {
        public int A_Rule_Id { get; set; }
        public int Rule_Id { get; set; }
        public int Event_Id { get; set; }
        public int Url_Id { get; set; }
    }
    public class Automation_Rule_Condition
    { 
        public int A_Rule_Condition_Id { get; set; }
        public int Rule_id { get; set; }
        public int Condition_Id { get; set; }
        public int Topup_Login_Id { get; set; }
    }

    public class Automation_Rule_Trigger
    { 
        public int A_Rule_Trigger_Id { get; set; }
        public int Rule_Id { get; set; }
        public int Email_Content_Id { get; set; }
        public int Sms_Content_Id { get; set; }
        public int Timer_Id { get; set; }
        public int Trigger_Type_Id { get; set; }
    }

    //14-Aug-2015 : Moorthy Added for Automation Rule Scheduler
    public class AutomationRuleDetails
    {
        public string Rule_Name { get; set; }
        public string Group_Name { get; set; }
        public string Event_Type { get; set; }
        public string Condition_Type { get; set; }
        public string Trigger_Type { get; set; }
        public string Site_Id { get; set; }
        public string Url_Name { get; set; }
        public string Api_Field { get; set; }
        //public string Math_Required { get; set; }
        public Boolean Math_Required { get; set; }
        public string Math_Value { get; set; }
        public int Email_Content_Id { get; set; }
        public int Sms_Content_Id { get; set; }
        public string Sms_Template { get; set; }
        public string Sms_From { get; set; }
        public string Sms_Content { get; set; }
        //public int Api_Id { get; set; }
        public string Api_Name { get; set; }
        public string Api_Url { get; set; }
    }

    //14-Aug-2015 : Moorthy Added for Automation Scheduler Rule
    public class SchedulerRule
    {
        public int Rule_Id { get; set; }
        public string Rule_Name { get; set; }
        public int Group_Id { get; set; }
        public Boolean IsCancel_Mails { get; set; }  //Added on 09-09-2015 - by Hari - for Cancel Scheduled Mails/ SMS
    }
    //14-Aug-2015 : Moorthy Added for Automation Scheduler Event
    public class SchedulerEvent
    {
        public int Event_Id { get; set; }
        public int Url_Id { get; set; }
        public string Event_Type { get; set; }
        public string Url_Name { get; set; }
        public string Api_Url { get; set; }
        public string Api_Fields { get; set; } //added on 20-08-2015 - by Hari
        public string Site_Id { get; set; } 
        public string Url_Alias { get; set; }
        public string Api_Input_Fields { get; set; } //added on 10-09-2015 - by Hari
        public string Api_Method { get; set; } //added on 10-09-2015 - by Hari
    }
    //14-Aug-2015 : Moorthy Added for Automation Scheduler Condition
    public class SchedulerCondition
    {
        public int Condition_Id { get; set; }
        public bool Math_Required { get; set; }
        public string Math_Value { get; set; }
        public string Api_Field { get; set; }
        public string Math_Field { get; set; }
        public string Status { get; set; }
        public int Event_Id { get; set; }
    }
    //14-Aug-2015 : Moorthy Added for Automation Scheduler Trigger
    public class SchedulerTrigger
    {
        public int? Rule_Trigger_Id { get; set; }
        public int? Sms_Content_Id { get; set; }
        public string Sms_From { get; set; }
        public string Sms_Content { get; set; }
        public string Sms_Template { get; set; }
        public int Timer_In_Seconds { get; set; }
        public int? Email_Content_Id { get; set; }
        public int? Trigger_Type_Id { get; set; }
        public int? Timer_Id { get; set; }
    }

    public class SchedulerSendContent
    {
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Email { get; set; }
        public string Postcode { get; set; }
        public string Mobile_No { get; set; }
        public bool isEmail { get; set; }
    }


    public class Automation_Log_Domain
    {
        public int? Automation_Log_Id { get; set; }
        public int? Automation_Group_Id { get; set; }
        public string Automation_Group_Name { get; set; }
        public int? Automation_Rule_Id { get; set; }
        public string Automation_Rule_Name { get; set; }
        public int? Event_Id { get; set; }
        public string Event_Type { get; set; }
        public int? Condition_Id { get; set; }
        public string Condition_Type { get; set; }
        public Boolean Math_Required { get; set; }
        public string Math_Field { get; set; }
        public string Math_Value { get; set; }
        public int? API_ID { get; set; }
        public string API_Field { get; set; }
        public int? URL_ID { get; set; }
        public string URL_Name { get; set; }
        public int? SMS_Content_Id { get; set; }
        public int? Email_Content_Id { get; set; }
        public int? Trigger_Id { get; set; }
        public int? Trigger_Type_Id { get; set; }
        public string Trigger_Type { get; set; }
        public DateTime Trigger_Date_Time { get; set; }
    }

    public class MailChimp_Automation_Domain
    {
        public int MailChimp_Automation_Id  { get; set; }
        public string Automation_Id  { get; set; }
        public string Automation_Title { get; set; }
    }

    /*Added on 20-08-2015 - by Hari*/
    public class PassedValuesEventConditions
    {
        public dynamic PassedValue { get; set; }
        public SchedulerEvent Event { get; set;}
        public List<SchedulerCondition> Conditions { get; set; }
        //public Boolean PassedStatus { get; set; }
    }
    /*End added*/
}
