﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vectone.Core.Domain.UserInfo
{
    public class SessionInfoDomain : BaseEntity, ISoftDeletable
    {
        public string user_login { get; set; }
        public string email { get; set; }        
        public string SiteCode { get; set; }
        public int ResellerId { get; set; }

        bool ISoftDeletable.Deleted
        {
            get { throw new NotImplementedException(); }
        }
    }
   
}