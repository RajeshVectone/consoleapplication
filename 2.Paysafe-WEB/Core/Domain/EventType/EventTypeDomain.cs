﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.EventType
{
    [DataContract]
    public class EventTypeDomain
    {
        [DataMember]
        public int Event_Id { get; set; }
        public string Event_Type { get; set; }
    }
}