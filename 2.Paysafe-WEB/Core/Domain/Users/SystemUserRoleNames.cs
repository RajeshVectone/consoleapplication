
namespace Vectone.Core.Domain.Users
{
    public static partial class SystemUserRoleNames
    {
		/// <remarks>codehint: vt-add</remarks>
		public static string SuperAdministrators { get { return "SuperAdmins"; } }

        public static string Administrators { get { return "Administrators"; } }
        
     

        public static string Registered { get { return "Registered"; } }

        public static string Guests { get { return "Guests"; } }
    }
}