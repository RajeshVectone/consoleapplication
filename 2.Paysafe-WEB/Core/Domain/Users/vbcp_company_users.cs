﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.Users
{
    /// <summary>
    /// Represents a user
    /// </summary>
    [DataContract]
    public partial class vbcp_company_users : BaseEntity, ISoftDeletable
    {
          private ICollection<UserRole> _userRoles;


        /// <summary>
        /// Ctor
        /// </summary>
          public vbcp_company_users()
        {
            this.UserGuid = Guid.NewGuid();
            this.PasswordFormat = PasswordFormat.Clear;
        }

        [DataMember]
        public int company_id { get; set; }

        public string username { get; set; }
        public string password { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public bool isActive { get; set; }
        public int dir_user_id { get; set; }
        public string external_contact_number { get; set; }
        public string email { get; set; }
        public string position { get; set; }
        public string Title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string full_name { get; set; }
        public string flag { get; set; }
        public int assign_ext_number { get; set; }
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the user Guid
        /// </summary>
        [DataMember]
        public Guid UserGuid { get; set; }

        /// <summary>
        /// Gets or sets the username
        /// </summary>
        [DataMember]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the password format
        /// </summary>
        public int PasswordFormatId { get; set; }

        /// <summary>
        /// Gets or sets the password format
        /// </summary>
        public PasswordFormat PasswordFormat
        {
            get { return (PasswordFormat)PasswordFormatId; }
            set { this.PasswordFormatId = (int)value; }
        }

        /// <summary>
        /// Gets or sets the password salt
        /// </summary>
        public string PasswordSalt { get; set; }

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        [DataMember]
        public string AdminComment { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether the user is active
        /// </summary>
        [DataMember]
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user account is system
        /// </summary>
        [DataMember]
        public bool IsSystemAccount { get; set; }

        /// <summary>
        /// Gets or sets the user system name
        /// </summary>
        [DataMember]
        public string SystemName { get; set; }

        /// <summary>
        /// Gets or sets the last IP address
        /// </summary>
        [DataMember]
        public string LastIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        [DataMember]
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last login
        /// </summary>
        [DataMember]
        public DateTime? LastLoginDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last activity
        /// </summary>
        [DataMember]
        public DateTime LastActivityDateUtc { get; set; }

        #region Navigation properties
        /// <summary>
        /// Gets or sets the user roles
        /// </summary>
        public virtual ICollection<UserRole> UserRoles
        {
            get { return _userRoles ?? (_userRoles = new List<UserRole>()); }
            protected set { _userRoles = value; }
        }

        //public string USerID { get; set; }
        //public int Company_id { get; set; }
        //public string UsreName { get; set; }
        //public string UPWD { get; set; }
        //public string User_Email { get; set; }
        //public string Company_Name { get; set; }
        //public string Comp_id { get; set; }
        //public string Com_Emailid { get; set; }
        //public string ComPWD { get; set; }
        //public string UserStatus { get; set; }
        //public string UserType { get; set; }

        public string Usr_EmailID_a { get; set; }
        public string Usr_Pwd_a { get; set; }


        //public string UserGuid { get; set; }
        //public int PasswordFormatId { get; set; }
        //public string PasswordSalt { get; set; }
        //public string AdminComment { get; set; }
        //public bool Deleted { get; set; }
        //public bool IsSystemAccount { get; set; }
        //public string SystemName { get; set; }
        //public string LastIpAddress { get; set; }
        //public DateTime CreatedOnUtc { get; set; }
        //public DateTime LastLoginDateUtc { get; set; }
        //public DateTime LastActivityDateUtc { get; set; }


        #endregion
    }
}
