using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Vectone.Core.Domain.Common;



namespace Vectone.Core.Domain.Users
{
    /// <summary>
    /// Represents a user
    /// </summary>
    [DataContract]
    public partial class User : BaseEntity, ISoftDeletable
    {

        private ICollection<UserRole> _userRoles;

        //private ICollection<Reseller> _resellers;

        /// <summary>
        /// Ctor
        /// </summary>
        public User()
        {
            this.UserGuid = Guid.NewGuid();
            this.PasswordFormat = PasswordFormat.Clear;
        }              

        /// <summary>
        /// Gets or sets the user Guid
        /// </summary>
        [DataMember]
        public Guid UserGuid { get; set; }

        /// <summary>
        /// Gets or sets the username
        /// </summary>
        [DataMember]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the password format
        /// </summary>
        public int? PasswordFormatId { get; set; }

        /// <summary>
        /// Gets or sets the password format
        /// </summary>
        public PasswordFormat PasswordFormat
        {
            get { return (PasswordFormat)PasswordFormatId; }
            set { this.PasswordFormatId = (int)value; }
        }

        /// <summary>
        /// Gets or sets the password salt
        /// </summary>
        public string PasswordSalt { get; set; }

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        [DataMember]
        public string AdminComment { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether the user is active
        /// </summary>
        [DataMember]
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has been deleted
        /// </summary>
        public bool? Deleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user account is system
        /// </summary>
        [DataMember]
        public bool? IsSystemAccount { get; set; }

        /// <summary>
        /// Gets or sets the user system name
        /// </summary>
        [DataMember]
        public string SystemName { get; set; }

        /// <summary>
        /// Gets or sets the last IP address
        /// </summary>
        [DataMember]
        public string LastIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        [DataMember]
        public DateTime? CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last login
        /// </summary>
        [DataMember]
        public DateTime? LastLoginDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last activity
        /// </summary>
        [DataMember]
        public DateTime? LastActivityDateUtc { get; set; }

        ///*Added on 27-04-2015 - by Hari*/
        ///// <summary>
        ///// Gets or sets the created by
        ///// </summary>
        //[DataMember]
        //public int CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the total logins
        ///// </summary>
        //[DataMember]
        //public int? TotalLogins { get; set; }
        /*end added*/

        #region Navigation properties                      
        /// <summary>
        /// Gets or sets the customer roles
        /// </summary>
        public virtual ICollection<UserRole> UserRoles
        {
            get { return _userRoles ?? (_userRoles = new HashSet<UserRole>()); }
            protected set { _userRoles = value; }
        }

        ///// <summary>
        ///// Gets or sets the resellers
        ///// </summary>
        //public virtual ICollection<Reseller> Resellers
        //{
        //    get { return _resellers ?? (_resellers = new HashSet<Reseller>()); }
        //    protected set { _resellers = value; }
        //}
        #endregion

        bool ISoftDeletable.Deleted
        {
            get { throw new NotImplementedException(); }
        }
    }
}