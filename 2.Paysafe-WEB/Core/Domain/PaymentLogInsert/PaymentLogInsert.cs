﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.PaymentLogInsert
{
    public class PaymentLogInsertInput
    {
        public string mobileno { get; set; }
        public string REFERENCE_ID { get; set; }
        public int Pay_step { get; set; }
        public int Pay_code { get; set; }
        public string Pay_decision { get; set; }
        public string Pay_description { get; set; }
        public string Pay_stage { get; set; }

    }

    public class PaymentLogInsertOutput : Output
    {

    }

    public static class PaymentLogInsertDBMethods
    {
        public static object GetParameters(PaymentLogInsertInput input)
        {
            object param = new
            {
                @mobileno=input.mobileno,
                @REFERENCE_ID=input.REFERENCE_ID,
                @Pay_step = input.Pay_step,
                @Pay_code = input.Pay_code,
                @Pay_decision = input.Pay_decision,
                @Pay_description = input.Pay_description,
                @Pay_stage = input.Pay_stage
            };
            return param;
        }

        public static string GetSpName()
        {
            return "paysafe_payment_log_insert";
        }

        public static string GetConnName()
        {
            return "payment_20160301";
        }
    }

}
