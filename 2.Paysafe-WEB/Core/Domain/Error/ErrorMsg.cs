﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.Error
{
    public class ErrorMsg : BaseEntity
    {
        public int? ErrCode { get; set; }
        public String ErrMsg { get; set; }
    }
}
