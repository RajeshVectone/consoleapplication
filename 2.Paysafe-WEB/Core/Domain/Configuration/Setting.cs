using System.Runtime.Serialization;
namespace Vectone.Core.Domain.Configuration
{
    /// <summary>
    /// Represents a setting
    /// </summary>
	[DataContract]
	public partial class Setting : BaseEntity
    {
        public Setting() { }

		public Setting(string name, string value, int vectoneid = 0)
		{
            this.Name = name;
            this.Value = value;
			this.VectoneId = vectoneid;
        }
        
        /// <summary>
        /// Gets or sets the name
        /// </summary>
		[DataMember]
		public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value
        /// </summary>
		[DataMember]
		public string Value { get; set; }

		/// <summary>
		/// Gets or sets the vectone for which this setting is valid. 0 is set when the setting is for all sites
		/// </summary>
		[DataMember]
		public int VectoneId { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
