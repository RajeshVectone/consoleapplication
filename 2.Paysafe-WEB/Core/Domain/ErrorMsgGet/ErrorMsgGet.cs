﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Core.Domain.ErrorMsgGet
{
    public class ErrorMsgGetInput
    {
        public string paysafe_code { get; set; }
    }

    public class ErrorMsgGetOutput : Output
    {
        public int idx { get; set; }
        public string paysafe_code { get; set; }
        public string paysafe_descript { get; set; }
    }

    public static class ErrorMsgGetDBMethods
    {
        public static object GetParameters(ErrorMsgGetInput input)
        {
            object param = new
            {
                @paysafe_code = input.paysafe_code
            };
            return param;
        }

        public static string GetSpName()
        {
            return "paysafe_error_msg_get";
        }

        public static string GetConnName()
        {
            return "payment_20160301";
        }
    }

}
