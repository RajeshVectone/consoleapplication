﻿using System;
using System.Threading.Tasks;

namespace Vectone.Core.Email
{    
    /// <summary>
    /// contract for email sender 
    /// </summary>
    public interface IEmailSender
    {
        void SendEmail(SmtpContext vttpContext, EmailMessage message);
		Task SendEmailAsync(SmtpContext vttpContext, EmailMessage message); 
    }
}
