﻿using Vectone.Core.Configuration;

namespace Vectone.Core.IO
{
    public class FileSystemSettings : ISettings
    {
        public string DirectoryName { get; set; }
    }
}