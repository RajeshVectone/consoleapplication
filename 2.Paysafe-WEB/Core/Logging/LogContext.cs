﻿using Vectone.Core.Domain.Users;
using Vectone.Core.Domain.Logging;

namespace Vectone.Core.Logging
{
	public class LogContext
	{
		public string ShortMessage { get; set; }
		public string FullMessage { get; set; }
		public LogLevel LogLevel { get; set; }
		public User User { get; set; }

		public bool HashNotFullMessage { get; set; }
		public bool HashIpAddress { get; set; }
	}
}
