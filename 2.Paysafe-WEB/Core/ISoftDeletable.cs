﻿using System;
using System.Collections.Generic;

namespace Vectone.Core
{
	public interface ISoftDeletable
	{
		bool Deleted { get; }
	}
}
