﻿using System;

namespace Vectone.Core.Data
{
	public class DbQuerySettings
	{
		private readonly static DbQuerySettings s_default = new DbQuerySettings(false, false);

		public DbQuerySettings(bool ignoreAcl, bool ignoreMultiVectone)
		{
			this.IgnoreAcl = ignoreAcl;
			this.IgnoreMultiVectone = ignoreMultiVectone;
		}

		public bool IgnoreAcl { get; private set; }
		public bool IgnoreMultiVectone { get; private set; }

		public static DbQuerySettings Default
		{
			get { return s_default; }
		}
	}
}
