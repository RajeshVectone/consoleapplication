﻿
using System.Data.Common;

namespace Vectone.Core.Data
{
    /// <summary>
    /// Data provider interface
    /// </summary>
    public interface IDataProvider
    {

        /// <summary>
        /// A value indicating whether this data provider supports vectoned procedures
        /// </summary>
        bool StoredProceduresSupported { get; }

        /// <summary>
        /// Gets a support database parameter object (used by Stored procedures)
        /// </summary>
        /// <returns>Parameter</returns>
        DbParameter GetParameter();

		/// <summary>
		/// Gets the db provider invariant name (e.g. <c>System.Data.SqlClient</c>)
		/// </summary>
		string ProviderInvariantName { get; }
    }
}
