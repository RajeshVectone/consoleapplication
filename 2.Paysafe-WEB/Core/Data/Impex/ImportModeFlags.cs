﻿using System;

namespace Vectone.Core.Data
{
    
    [Flags]
    public enum ImportModeFlags
    {
        Insert = 1,
        Update = 2
    }

}
