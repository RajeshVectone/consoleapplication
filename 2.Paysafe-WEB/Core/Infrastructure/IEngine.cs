﻿using System;
using System.Collections.Generic;
using Vectone.Core.Configuration;
using Vectone.Core.Infrastructure.DependencyManagement;

namespace Vectone.Core.Infrastructure
{
    /// <summary>
    /// Classes implementing this interface can serve as a portal for the 
    /// various services composing the Vectone engine. Edit functionality, modules
    /// and implementations access most Vectone functionality through this 
    /// interface.
    /// </summary>
    public interface IEngine
    {
        ContainerManager ContainerManager { get; }
        
        /// <summary>
        /// Initialize components and plugins in the Vectone environment.
        /// </summary>
        /// <param name="config">Config</param>
        void Initialize();

        T Resolve<T>(string name = null) where T : class;

        object Resolve(Type type, string name = null);

        T[] ResolveAll<T>();
    }
}
