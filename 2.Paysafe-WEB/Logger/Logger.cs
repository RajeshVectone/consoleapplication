﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vectone.Logging
{
    /// <summary>
    /// Represents a standard logtype
    /// </summary>
    public enum LogType
    {
        INFO = 0,
        WARNING,
        ERROR,
        DEBUG
    }

    public struct LogMessage
    {
        /// <summary>
        /// Represents a log message
        /// </summary>
        /// <param name="message">Plaintext log message</param>
        /// <param name="call">Name of the method that has generated the log message</param>
        /// <param name="call_params">Optional logmessage parameters</param>
        /// <param name="result">Any result code</param>
        /// <param name="type">Any type of LogType</param>
        public LogMessage(string message, string call, string call_params, string result, LogType type = LogType.ERROR)
        {
            logType = type;
            this.message = message;
            callerFunc = call;
            callParams = call_params;
            requResult = result;
        }

        public LogType logType;
        public string message;
        public string callerFunc;
        public string callParams;
        public string requResult;
    }

    /// <summary>
    /// Logger class to save log messages and redirect them to the application code
    /// </summary>
    public class Logger
    {
        public delegate void logMessageEventHandler(object sender, LogMessage logMessage);
        public event logMessageEventHandler logMessageEvent = null;

        private Dictionary<LogType, List<LogMessage>> m_logMessages;
        private bool m_logDebugMessages = false;

        public Logger()
        {
            m_logMessages = new Dictionary<LogType, List<LogMessage>>();

            m_logMessages.Add(LogType.DEBUG, new List<LogMessage>());
            m_logMessages.Add(LogType.WARNING, new List<LogMessage>());
            m_logMessages.Add(LogType.ERROR, new List<LogMessage>());
            m_logMessages.Add(LogType.INFO, new List<LogMessage>());
        }

        /// <summary>
        /// Specify if debug messages should be logged
        /// </summary>
        public bool logDebugMessages
        {
            set { m_logDebugMessages = value; }
        }

        /// <summary>
        /// Add a new log message
        /// </summary>
        /// <param name="msg_code">Plaintext log message</param>
        /// <param name="call">Name of the method that has generated the log message</param>
        /// <param name="call_params">Optional logmessage parameters</param>
        /// <param name="result">Any result code</param>
        /// <param name="type">Any type of LogType</param>
        public void addLog(string msg_code, string call, string call_params, string result, LogType type = LogType.ERROR)
        {
            if (type == LogType.DEBUG)
            {
                // Debug-logs should be ignored, abort:
                if (!m_logDebugMessages)
                    return;
            }

            LogMessage tmpLogMsg = new LogMessage(msg_code, call, call_params, result, type);
            m_logMessages[tmpLogMsg.logType].Add(tmpLogMsg);

            if (logMessageEvent != null)
                logMessageEvent(this, tmpLogMsg);
        }

        /// <summary>
        /// Add a new debug log
        /// </summary>
        /// <param name="key">Any string, as specified by the application</param>
        /// <param name="value">Any string, as specified by the application</param>
        public void addDebug(string key, string value)
        {
            // Debug-logs should be ignored, abort:
            if (!m_logDebugMessages)
                return;

            LogMessage tmpLogMsg = new LogMessage("0", key, null, value, Logging.LogType.DEBUG);
            m_logMessages[tmpLogMsg.logType].Add(tmpLogMsg);

            if (logMessageEvent != null)
                logMessageEvent(this, tmpLogMsg);
        }

        /// <summary>
        /// Returns all log messages of the specified type
        /// </summary>
        /// <param name="logType"></param>
        /// <returns>Sammlung mit LogMessage-Objekten</returns>
        public List<LogMessage> getLogMessages(LogType logType = LogType.INFO)
        {
            List<LogMessage> logMsgs = new List<LogMessage>();

            foreach (LogMessage msg in m_logMessages[logType])
                logMsgs.Add(msg);

            return logMsgs;
        }

        /// <summary>
        /// Deletes all log messages of the specified type
        /// </summary>
        /// <param name="logType"></param>
        public void deleteLogMessages(LogType logType = LogType.INFO)
        {
            m_logMessages[logType].Clear();
        }
    }
}
