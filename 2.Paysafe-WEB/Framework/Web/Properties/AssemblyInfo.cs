﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Vectone.Web.Framework")]

[assembly: InternalsVisibleTo("Vectone.Core.Tests")]
[assembly: InternalsVisibleTo("Vectone.Services.Tests")]
[assembly: InternalsVisibleTo("Vectone.Web.MVC.Tests")]
