using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Vectone.Core;
using Vectone.Core.Caching;
using Vectone.Core.Configuration;
using Vectone.Core.Data;
using Vectone.Core.Events;
using Vectone.Core.Fakes;
using Vectone.Core.Infrastructure;
using Vectone.Core.Infrastructure.DependencyManagement;
using Vectone.Core.Plugins;
using Vectone.Data;
using Vectone.Services.Authentication;

using Vectone.Services.Common;
using Vectone.Services.Configuration;
using Vectone.Services.Users;
using Vectone.Services.Helpers;
using Vectone.Services.Localization;

using Vectone.Services.UserManager; //added on 19 Mar 2015

using Vectone.Core.Logging;
using Vectone.Services.Security;
using Vectone.Services.Tasks;
using Vectone.Web.Framework.Mvc.Routes;
using Vectone.Web.Framework.Mvc.Bundles;

using Vectone.Web.Framework.WebApi;
using Vectone.Web.Framework.Plugins;
using Vectone.Web.Framework.Controllers;
using Vectone.Core.Data.Hooks;
using Vectone.Services.Vectones;
using Vectone.Web.Framework.WebApi.Configuration;
using Vectone.Services;
using Module = Autofac.Module;
using Vectone.Core.Localization;
using Vectone.Web.Framework.Localization;
using Vectone.Core.Email;

using Vectone.Services.Logging;
using Vectone.Core.IO.Media;
using Vectone.Core.IO.VirtualPath;
using Vectone.Core.IO.WebSite;
using Vectone.Utilities;
using Vectone.Core.Domain.Users;

using Vectone.Services.Events;
using Vectone.Services.PaySafeServices;
using Vectone.Core.Domain.InsertCurrencyExchange;
using Vectone.Services.GenericServices;
using Vectone.Core.Domain.InsertMerchantRefDtls;
using Vectone.Core.Domain.PaymentLogInsert;
using Vectone.Core.Domain.ErrorMsgGet;
using Vectone.Core.Domain.CurrencyUpdate;
using Vectone.Core.Domain.InsertPnurlLog;
using Vectone.Core.Domain.GetPnurlLog;


namespace Vectone.Web.Framework
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, bool isActiveModule)
        {
            // plugins
            var pluginFinder = new PluginFinder();
            builder.RegisterInstance(pluginFinder).As<IPluginFinder>().SingleInstance();
            builder.RegisterType<PluginMediator>();

            // modules
            builder.RegisterModule(new DbModule(typeFinder));
            builder.RegisterModule(new CachingModule());
            builder.RegisterModule(new LocalizationModule());
            builder.RegisterModule(new LoggingModule());
            builder.RegisterModule(new EventModule(typeFinder));
            builder.RegisterModule(new MessagingModule());
            builder.RegisterModule(new WebModule(typeFinder));
            builder.RegisterModule(new WebApiModule(typeFinder));
            builder.RegisterModule(new UiModule(typeFinder));
            builder.RegisterModule(new IOModule());

            builder.RegisterModule(new ProvidersModule(typeFinder, pluginFinder));

            //Added on 11-06-2015 - by Hari - Quartz scheduler
            builder.RegisterModule(new QuartzModule(typeFinder));
            //builder.RegisterModule(new QuartzNewModule());

            // sources
            builder.RegisterSource(new SettingsSource());

            // web helper
            builder.RegisterType<WebHelper>().As<IWebHelper>().InstancePerRequest();

            // work context
            builder.RegisterType<WebWorkContext>().As<IWorkContext>().WithStaticCache().InstancePerRequest();

            // vectone context
            builder.RegisterType<WebVectoneContext>().As<IVectoneContext>().InstancePerRequest();


            builder.RegisterType<GenericAttributeService>().As<IGenericAttributeService>().InstancePerRequest();

            builder.RegisterType<FormsAuthenticationService>().As<IAuthenticationService>().InstancePerRequest();
            builder.RegisterType<UserRegistrationService>().As<IUserRegistrationService>().InstancePerRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();

            //builder.RegisterType<Vectone.Services.Products.MobileService>().As<Vectone.Services.Products.IMobileService>().InstancePerRequest();
            //builder.RegisterType<Vectone.Services.Products.ProductService>().As<Vectone.Services.Products.IProductService>().InstancePerRequest();

            //builder.RegisterType<Vectone.Services.Subscription.SubscriptionService>().As<Vectone.Services.Subscription.ISubscriptionService>().InstancePerRequest();

            builder.RegisterType<PermissionService>().As<IPermissionService>().WithStaticCache().InstancePerRequest();

            builder.RegisterType<AclService>().As<IAclService>().WithStaticCache().InstancePerRequest();



            builder.RegisterType<VectoneService>().As<IVectoneService>().InstancePerRequest();
            builder.RegisterType<VectoneMappingService>().As<IVectoneMappingService>().WithStaticCache().InstancePerRequest();



            builder.RegisterType<SettingService>().As<ISettingService>().WithStaticCache().InstancePerRequest();



            builder.RegisterType<EncryptionService>().As<IEncryptionService>().InstancePerRequest();


            builder.RegisterType<DateTimeHelper>().As<IDateTimeHelper>().InstancePerRequest();



            builder.RegisterType<ScheduleTaskService>().As<IScheduleTaskService>().InstancePerRequest();


            builder.RegisterType<CommonServices>().As<ICommonServices>().WithStaticCache().InstancePerRequest();

            builder.RegisterType<UserManagerNew>().As<IUserManagerNew>().InstancePerRequest();   //added on 19 Mar 2015

            //added on 09 Apr 2015 - User Info
            builder.RegisterType<Vectone.Services.UserInfo.UserInfoService>().As<Vectone.Services.UserInfo.IUserInfoService>().InstancePerRequest();
            //end added


            //added on 15 Apr 2015 - Permission Service
            builder.RegisterType<Vectone.Services.Security.StandardPermissionProvider>().As<Vectone.Services.Security.IPermissionProvider>().InstancePerRequest();
            //end added

            //added on 27 Apr 2015 - User Settings
            builder.RegisterType<UserSettings>().As<ISettings>().InstancePerRequest();
            //end added
            builder.RegisterType<PaySafeService>().As<IPaySafeService>().InstancePerRequest();
            builder.RegisterType<Operation>().As<IOperation>().InstancePerRequest();
            builder.RegisterType<GenericServices<InsertCurrencyExchangeOutput, InsertCurrencyExchangeInput>>().As<IGenericServices<InsertCurrencyExchangeOutput, InsertCurrencyExchangeInput>>().InstancePerRequest();
            builder.RegisterType<GenericServices<CurrencyUpdateOutput, CurrencyUpdateInput>>().As<IGenericServices<CurrencyUpdateOutput, CurrencyUpdateInput>>().InstancePerRequest();
            builder.RegisterType<GenericServices<ErrorMsgGetOutput, ErrorMsgGetInput>>().As<IGenericServices<ErrorMsgGetOutput, ErrorMsgGetInput>>().InstancePerRequest();
            builder.RegisterType<GenericServices<PaymentLogInsertOutput, PaymentLogInsertInput>>().As<IGenericServices<PaymentLogInsertOutput, PaymentLogInsertInput>>().InstancePerRequest();
            builder.RegisterType<GenericServices<InsertMerchantRefDtlsOutput, InsertMerchantRefDtlsInput>>().As<IGenericServices<InsertMerchantRefDtlsOutput, InsertMerchantRefDtlsInput>>().InstancePerRequest();
            builder.RegisterType<GenericServices<InsertPnurlLogOutput, InsertPnurlLogInput>>().As<IGenericServices<InsertPnurlLogOutput, InsertPnurlLogInput>>().InstancePerRequest();
            builder.RegisterType<GenericServices<GetPnurlLogOutput, GetPnurlLogInput>>().As<IGenericServices<GetPnurlLogOutput, GetPnurlLogInput>>().InstancePerRequest();

        }

        public int Order
        {
            get { return -100; }
        }
    }

    #region Modules

    public class DbModule : Module
    {
        private readonly ITypeFinder _typeFinder;

        public DbModule(ITypeFinder typeFinder)
        {
            _typeFinder = typeFinder;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => DataSettings.Current).As<DataSettings>().InstancePerDependency();
            builder.Register(x => new EfDataProviderFactory(x.Resolve<DataSettings>())).As<DataProviderFactory>().InstancePerDependency();

            builder.Register(x => x.Resolve<DataProviderFactory>().LoadDataProvider()).As<IDataProvider>().InstancePerDependency();
            builder.Register(x => (IEfDataProvider)x.Resolve<DataProviderFactory>().LoadDataProvider()).As<IEfDataProvider>().InstancePerDependency();

            if (DataSettings.Current.IsValid())
            {
                // register DB Hooks (only when app was installed properly)

                Func<Type, Type> findHookedType = (t) =>
                {
                    var x = t;
                    while (x != null)
                    {
                        if (x.IsGenericType)
                        {
                            return x.GetGenericArguments()[0];
                        }
                        x = x.BaseType;
                    }

                    return typeof(object);
                };

                var hooks = _typeFinder.FindClassesOfType<IHook>(ignoreInactivePlugins: true);
                foreach (var hook in hooks)
                {
                    var hookedType = findHookedType(hook);

                    var registration = builder.RegisterType(hook)
                        .As(typeof(IPreActionHook).IsAssignableFrom(hook) ? typeof(IPreActionHook) : typeof(IPostActionHook))
                        .InstancePerRequest();

                    registration.WithMetadata<HookMetadata>(m =>
                    {
                        m.For(em => em.HookedType, hookedType);
                    });
                }

                builder.Register<IDbContext>(c => new VectoneObjectContext(DataSettings.Current.DataConnectionString))
                    .PropertiesAutowired(PropertyWiringOptions.None)
                    .InstancePerRequest();
            }
            else
            {
                builder.Register<IDbContext>(c => new VectoneObjectContext(DataSettings.Current.DataConnectionString)).InstancePerRequest();
            }

            builder.Register<Func<string, IDbContext>>(c =>
            {
                var cc = c.Resolve<IComponentContext>();
                return named => cc.ResolveNamed<IDbContext>(named);
            });

            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();

            builder.Register<DbQuerySettings>(c =>
            {
                var vectoneService = c.Resolve<IVectoneService>();
                var aclService = c.Resolve<IAclService>();
                //return new DbQuerySettings(!aclService.HasActiveAcl, vectoneService.IsSingleVectoneMode());
                var x = !aclService.HasActiveAcl;
                var y = vectoneService.IsSingleVectoneMode();
                return new DbQuerySettings(true, true);
            })
            .InstancePerRequest();
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            var querySettingsProperty = FindQuerySettingsProperty(registration.Activator.LimitType);

            if (querySettingsProperty == null)
                return;

            registration.Activated += (sender, e) =>
            {
                if (DataSettings.DatabaseIsInstalled())
                {
                    var querySettings = e.Context.Resolve<DbQuerySettings>();
                    querySettingsProperty.SetValue(e.Instance, querySettings, null);
                }
            };
        }

        private static PropertyInfo FindQuerySettingsProperty(Type type)
        {
            return type.GetProperty("QuerySettings", typeof(DbQuerySettings));
        }
    }

    public class LoggingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Notifier>().As<INotifier>().InstancePerRequest();
            builder.RegisterType<DefaultLogger>().As<ILogger>().InstancePerRequest();
            builder.RegisterType<UserActivityService>().As<IUserActivityService>().InstancePerRequest();
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            if (!DataSettings.DatabaseIsInstalled())
                return;

            var implementationType = registration.Activator.LimitType;

            // build an array of actions on this type to assign loggers to member properties
            var injectors = BuildLoggerInjectors(implementationType).ToArray();

            // if there are no logger properties, there's no reason to hook the activated event
            if (!injectors.Any())
                return;

            // otherwise, whan an instance of this component is activated, inject the loggers on the instance
            registration.Activated += (s, e) =>
            {
                foreach (var injector in injectors)
                    injector(e.Context, e.Instance);
            };
        }

        private IEnumerable<Action<IComponentContext, object>> BuildLoggerInjectors(Type componentType)
        {
            if (DataSettings.DatabaseIsInstalled())
            {
                // Look for settable properties of type "ILogger" 
                var loggerProperties = componentType
                    .GetProperties(BindingFlags.SetProperty | BindingFlags.Public | BindingFlags.Instance)
                    .Select(p => new
                    {
                        PropertyInfo = p,
                        p.PropertyType,
                        IndexParameters = p.GetIndexParameters(),
                        Accessors = p.GetAccessors(false)
                    })
                    .Where(x => x.PropertyType == typeof(ILogger)) // must be a logger
                    .Where(x => x.IndexParameters.Count() == 0) // must not be an indexer
                    .Where(x => x.Accessors.Length != 1 || x.Accessors[0].ReturnType == typeof(void)); //must have get/set, or only set

                // Return an array of actions that resolve a logger and assign the property
                foreach (var entry in loggerProperties)
                {
                    var propertyInfo = entry.PropertyInfo;

                    yield return (ctx, instance) =>
                    {
                        string component = componentType.ToString();
                        var logger = ctx.Resolve<ILogger>();
                        propertyInfo.SetValue(instance, logger, null);
                    };
                }
            }
        }
    }

    public class LocalizationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LanguageService>().As<ILanguageService>().InstancePerRequest();

            builder.RegisterType<TelerikLocalizationServiceFactory>().As<Telerik.Web.Mvc.Infrastructure.ILocalizationServiceFactory>().InstancePerRequest();
            builder.RegisterType<LocalizationService>().As<ILocalizationService>()
                .WithStaticCache() // pass StaticCache as ICache (cache settings between requests)
                .InstancePerRequest();

            builder.RegisterType<Text>().As<IText>().InstancePerRequest();

            builder.RegisterType<LocalizedEntityService>().As<ILocalizedEntityService>()
                .WithStaticCache() // pass StaticCache as ICache (cache settings between requests)
                .InstancePerRequest();
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            var userProperty = FindUserProperty(registration.Activator.LimitType);

            if (userProperty == null)
                return;

            registration.Activated += (sender, e) =>
            {
                if (DataSettings.DatabaseIsInstalled())
                {
                    Localizer localizer = e.Context.Resolve<IText>().Get;
                    userProperty.SetValue(e.Instance, localizer, null);
                }
            };
        }

        private static PropertyInfo FindUserProperty(Type type)
        {
            return type.GetProperty("T", typeof(Localizer));
        }
    }

    public class CachingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StaticCache>().Keyed<ICache>(typeof(StaticCache)).SingleInstance();
            builder.RegisterType<AspNetCache>().Keyed<ICache>(typeof(AspNetCache)).InstancePerRequest();
            builder.RegisterType<RequestCache>().Keyed<ICache>(typeof(RequestCache)).InstancePerRequest();

            builder.RegisterType<CacheManager<RequestCache>>()
                .As<ICacheManager>()
                .InstancePerRequest();
            builder.RegisterType<CacheManager<StaticCache>>()
                .Named<ICacheManager>("static")
                .SingleInstance();
            builder.RegisterType<CacheManager<AspNetCache>>()
                .Named<ICacheManager>("aspnet")
                .InstancePerRequest();
            builder.RegisterType<NullCache>()
                .Named<ICacheManager>("null")
                .SingleInstance();

            // Register resolving delegate
            builder.Register<Func<Type, ICache>>(c =>
            {
                var cc = c.Resolve<IComponentContext>();
                return keyed => cc.ResolveKeyed<ICache>(keyed);
            });

            builder.Register<Func<string, ICacheManager>>(c =>
            {
                var cc = c.Resolve<IComponentContext>();
                return named => cc.ResolveNamed<ICacheManager>(named);
            });

            builder.Register<Func<string, Lazy<ICacheManager>>>(c =>
            {
                var cc = c.Resolve<IComponentContext>();
                return named => cc.ResolveNamed<Lazy<ICacheManager>>(named);
            });
        }
    }

    public class EventModule : Module
    {
        private readonly ITypeFinder _typeFinder;

        public EventModule(ITypeFinder typeFinder)
        {
            _typeFinder = typeFinder;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EventPublisher>().As<IEventPublisher>().SingleInstance();
            builder.RegisterGeneric(typeof(DefaultConsumerFactory<>)).As(typeof(IConsumerFactory<>)).InstancePerDependency();

            // Register event consumers
            var consumerTypes = _typeFinder.FindClassesOfType(typeof(IConsumer<>));
            foreach (var consumerType in consumerTypes)
            {
                Type[] implementedInterfaces = consumerType.FindInterfaces(IsConsumerInterface, typeof(IConsumer<>));

                var registration = builder.RegisterType(consumerType).As(implementedInterfaces);

                var isActive = PluginManager.IsActivePluginAssembly(consumerType.Assembly);
                var shouldExecuteAsync = consumerType.GetAttribute<AsyncConsumerAttribute>(false) != null;

                registration.WithMetadata<EventConsumerMetadata>(m =>
                {
                    m.For(em => em.IsActive, isActive);
                    m.For(em => em.ExecuteAsync, shouldExecuteAsync);
                });

                if (!shouldExecuteAsync)
                    registration.InstancePerRequest();

            }
        }

        private static bool IsConsumerInterface(Type type, object criteria)
        {
            var isMatch = type.IsGenericType && ((Type)criteria).IsAssignableFrom(type.GetGenericTypeDefinition());
            return isMatch;
        }
    }

    public class MessagingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DefaultEmailSender>().As<IEmailSender>().SingleInstance(); // xxx (http)
        }
    }

    public class WebModule : Module
    {
        private readonly ITypeFinder _typeFinder;

        public WebModule(ITypeFinder typeFinder)
        {
            _typeFinder = typeFinder;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var foundAssemblies = _typeFinder.GetAssemblies(ignoreInactivePlugins: true).ToArray();

            builder.RegisterModule(new AutofacWebTypesModule());
            builder.Register(HttpContextBaseFactory).As<HttpContextBase>();

            // register all controllers
            builder.RegisterControllers(foundAssemblies);

            builder.RegisterType<RoutePublisher>().As<IRoutePublisher>().SingleInstance();
            builder.RegisterType<BundlePublisher>().As<IBundlePublisher>().SingleInstance();


            builder.RegisterFilterProvider();

            // global exception handling
            builder.RegisterType<HandleExceptionFilter>().AsActionFilterFor<Controller>();
        }

        static HttpContextBase HttpContextBaseFactory(IComponentContext ctx)
        {
            if (IsRequestValid())
            {
                return new HttpContextWrapper(HttpContext.Current);
            }

            // TODO: determine vectone url

            // register FakeHttpContext when HttpContext is not available
            return new FakeHttpContext("~/");
        }

        static bool IsRequestValid()
        {
            if (HttpContext.Current == null)
                return false;

            try
            {
                // The "Request" property throws at application startup on IIS integrated pipeline mode
                var req = HttpContext.Current.Request;
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }

    public class WebApiModule : Module
    {
        private readonly ITypeFinder _typeFinder;

        public WebApiModule(ITypeFinder typeFinder)
        {
            _typeFinder = typeFinder;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var foundAssemblies = _typeFinder.GetAssemblies(ignoreInactivePlugins: true).ToArray();

            // register all api controllers
            builder.RegisterApiControllers(foundAssemblies);

            builder.RegisterType<WebApiConfigurationPublisher>().As<IWebApiConfigurationPublisher>().SingleInstance();
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            if (!DataSettings.DatabaseIsInstalled())
                return;

            var baseType = typeof(WebApiEntityController<,>);
            var type = registration.Activator.LimitType;
            Type implementingType;

            if (!type.IsSubClass(baseType, out implementingType))
                return;

            var repoProperty = FindRepositoryProperty(type, implementingType.GetGenericArguments()[0]);
            var serviceProperty = FindServiceProperty(type, implementingType.GetGenericArguments()[1]);

            if (repoProperty != null || serviceProperty != null)
            {
                registration.Activated += (sender, e) =>
                {
                    if (repoProperty != null)
                    {
                        var repo = e.Context.Resolve(repoProperty.PropertyType);
                        repoProperty.SetValue(e.Instance, repo, null);
                    }

                    if (serviceProperty != null)
                    {
                        var service = e.Context.Resolve(serviceProperty.PropertyType);
                        serviceProperty.SetValue(e.Instance, service, null);
                    }
                };
            }
        }

        private static PropertyInfo FindRepositoryProperty(Type type, Type entityType)
        {
            var pi = type.GetProperty("Repository", typeof(IRepository<>).MakeGenericType(entityType));
            return pi;
        }

        private static PropertyInfo FindServiceProperty(Type type, Type serviceType)
        {
            var pi = type.GetProperty("Service", serviceType);
            return pi;
        }

    }

    public class UiModule : Module
    {
        private readonly ITypeFinder _typeFinder;

        public UiModule(ITypeFinder typeFinder)
        {
            _typeFinder = typeFinder;
        }

        protected override void Load(ContainerBuilder builder)
        {
            // register theming services



        }
    }

    public class IOModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FileSystemStorageProvider>().As<IStorageProvider>().InstancePerRequest();
            builder.RegisterType<DefaultVirtualPathProvider>().As<IVirtualPathProvider>().InstancePerRequest();
            builder.RegisterType<WebSiteFolder>().As<IWebSiteFolder>().InstancePerRequest();
        }
    }



    public class ProvidersModule : Module
    {
        private readonly ITypeFinder _typeFinder;
        private readonly IPluginFinder _pluginFinder;

        public ProvidersModule(ITypeFinder typeFinder, IPluginFinder pluginFinder)
        {
            _typeFinder = typeFinder;
            _pluginFinder = pluginFinder;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ProviderManager>().As<IProviderManager>().InstancePerRequest();

            if (!DataSettings.DatabaseIsInstalled())
                return;

            var providerTypes = _typeFinder.FindClassesOfType<IProvider>(ignoreInactivePlugins: true).ToList();

            foreach (var type in providerTypes)
            {
                var pluginDescriptor = _pluginFinder.GetPluginDescriptorByAssembly(type.Assembly);

                var systemName = GetSystemName(type, pluginDescriptor);
                var friendlyName = GetFriendlyName(type, pluginDescriptor);
                var displayOrder = GetDisplayOrder(type, pluginDescriptor);

                var resPattern = (pluginDescriptor != null ? "Plugins" : "Providers") + ".{1}.{0}"; // e.g. Plugins.FriendlyName.MySystemName
                var settingPattern = (pluginDescriptor != null ? "Plugins" : "Providers") + ".{0}.{1}"; // e.g. Plugins.MySystemName.DisplayOrder
                var isConfigurable = typeof(IConfigurable).IsAssignableFrom(type);
                var isEditable = typeof(IUserEditable).IsAssignableFrom(type);

                var registration = builder.RegisterType(type).Named<IProvider>(systemName).InstancePerRequest();
                registration.WithMetadata<ProviderMetadata>(m =>
                {
                    m.For(em => em.PluginDescriptor, pluginDescriptor);

                    m.For(em => em.SystemName, systemName);
                    m.For(em => em.ResourceKeyPattern, resPattern);
                    m.For(em => em.SettingKeyPattern, settingPattern);
                    m.For(em => em.FriendlyName, friendlyName.Item1);
                    m.For(em => em.Description, friendlyName.Item2);
                    m.For(em => em.DisplayOrder, displayOrder);

                    m.For(em => em.IsConfigurable, isConfigurable);
                    m.For(em => em.IsEditable, isEditable);
                });

                // register specific provider type



            }

        }

        #region Helpers

        private void RegisterAsSpecificProvider<T>(Type implType, string systemName, IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle> registration) where T : IProvider
        {
            if (typeof(T).IsAssignableFrom(implType))
            {
                try
                {
                    registration.As<T>().Named<T>(systemName);
                    registration.WithMetadata<ProviderMetadata>(m =>
                    {
                        m.For(em => em.ProviderType, typeof(T));
                    });
                }
                catch (Exception) { }
            }
        }

        private string GetSystemName(Type type, PluginDescriptor descriptor)
        {
            var attr = type.GetAttribute<SystemNameAttribute>(false);
            if (attr != null)
            {
                return attr.Name;
            }

            if (typeof(IPlugin).IsAssignableFrom(type) && descriptor != null)
            {
                return descriptor.SystemName;
            }

            return type.FullName;
            //throw Error.Application("The 'SystemNameAttribute' must be applied to a provider type if the provider does not implement 'IPlugin' (provider type: {0}, plugin: {1})".FormatInvariant(type.FullName, descriptor != null ? descriptor.SystemName : "-"));
        }

        private int GetDisplayOrder(Type type, PluginDescriptor descriptor)
        {
            var attr = type.GetAttribute<DisplayOrderAttribute>(false);
            if (attr != null)
            {
                return attr.DisplayOrder;
            }

            if (typeof(IPlugin).IsAssignableFrom(type) && descriptor != null)
            {
                return descriptor.DisplayOrder;
            }

            return 0;
        }

        private Tuple<string/*Name*/, string/*Description*/> GetFriendlyName(Type type, PluginDescriptor descriptor)
        {
            string name = null;
            string description = name;

            var attr = type.GetAttribute<FriendlyNameAttribute>(false);
            if (attr != null)
            {
                name = attr.Name;
                description = attr.Description;
            }
            else if (typeof(IPlugin).IsAssignableFrom(type) && descriptor != null)
            {
                name = descriptor.FriendlyName;
                description = descriptor.Description;
            }
            else
            {
                name = Inflector.Titleize(type.Name);
                //throw Error.Application("The 'FriendlyNameAttribute' must be applied to a provider type if the provider does not implement 'IPlugin' (provider type: {0}, plugin: {1})".FormatInvariant(type.FullName, descriptor != null ? descriptor.SystemName : "-"));
            }

            return new Tuple<string, string>(name, description);
        }



        #endregion

    }

    /// <summary>
    /// This represents an entity for Autofac module.
    /// </summary>
    public class QuartzModule : Module
    {


        private readonly ITypeFinder _typeFinder;

        public QuartzModule(ITypeFinder _typeFinder)
        {

            this._typeFinder = _typeFinder;
        }


        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            //this._container = builder.Build();

            base.Load(builder);

        }

      

    }

    #endregion

    #region Sources

    public class SettingsSource : IRegistrationSource
    {
        static readonly MethodInfo BuildMethod = typeof(SettingsSource).GetMethod(
            "BuildRegistration",
            BindingFlags.Static | BindingFlags.NonPublic);

        public IEnumerable<IComponentRegistration> RegistrationsFor(
                Service service,
                Func<Service, IEnumerable<IComponentRegistration>> registrations)
        {
            var ts = service as TypedService;
            if (ts != null && typeof(ISettings).IsAssignableFrom(ts.ServiceType))
            {
                //var buildMethod = BuildMethod.MakeGenericMethod(ts.ServiceType);
                //yield return (IComponentRegistration)buildMethod.Invoke(null, null);

                // Perf with Fasterflect
                yield return (IComponentRegistration)Fasterflect.TryInvokeWithValuesExtensions.TryCallMethodWithValues(
                    typeof(SettingsSource),
                    null,
                    "BuildRegistration",
                    new Type[] { ts.ServiceType },
                    BindingFlags.Static | BindingFlags.NonPublic);
            }
        }

        static IComponentRegistration BuildRegistration<TSettings>() where TSettings : ISettings, new()
        {
            return RegistrationBuilder
                .ForDelegate((c, p) =>
                {
                    int currentVectoneId = 0;
                    IVectoneContext vectoneContext;
                    if (c.TryResolve<IVectoneContext>(out vectoneContext))
                    {
                        var vectone = vectoneContext.CurrentVectone;

                        currentVectoneId = vectone.Id;
                        //uncomment the code below if you want load settings per vectone only when you have two vectonesites installed.
                        //var currentVectoneId = c.Resolve<IVectoneService>().GetAllVectones().Count > 1
                        //    c.Resolve<IVectoneContext>().CurrentVectone.Id : 0;

                        ////although it's better to connect to your database and execute the following SQL:
                        //DELETE FROM [Setting] WHERE [VectoneId] > 0

                        return c.Resolve<ISettingService>().LoadSetting<TSettings>(currentVectoneId);
                    }

                    // Unit tests
                    return new TSettings();
                })
                .InstancePerRequest()
                .CreateRegistration();
        }

        public bool IsAdapterForIndividualComponents { get { return false; } }
    }

    #endregion

}
