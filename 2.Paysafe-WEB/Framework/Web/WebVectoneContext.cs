﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vectone.Core;
using Vectone.Core.Domain.Vectones;
using Vectone.Services.Vectones;

namespace Vectone.Web.Framework
{
	/// <summary>
	/// Vectone context for web application
	/// </summary>
	public partial class WebVectoneContext : IVectoneContext
	{
		private readonly IVectoneService _vectoneService;
		private readonly IWebHelper _webHelper;

        private VectoneSite _cachedVectone;

		public WebVectoneContext(IVectoneService vectoneService, IWebHelper webHelper)
		{
			this._vectoneService = vectoneService;
			this._webHelper = webHelper;
		}

		/// <summary>
		/// Gets or sets the current vectone
		/// </summary>
        public VectoneSite CurrentVectone
		{
			get
			{
				if (_cachedVectone != null)
					return _cachedVectone;

				//ty to determine the current vectone by HTTP_HOST
				var host = _webHelper.ServerVariables("HTTP_HOST");
				var allVectones = _vectoneService.GetAllVectones();
				var vectone = allVectones.FirstOrDefault(s => s.ContainsHostValue(host));

				if (vectone == null)
				{
					//load the first found vectone
					vectone = allVectones.FirstOrDefault();
				}
				if (vectone == null)
					throw new Exception("No vectone could be loaded");

				_cachedVectone = vectone;
				return _cachedVectone;
			}
		}

		/// <summary>
		/// IsSingleVectoneMode ? 0 : CurrentVectone.Id
		/// </summary>
		public int CurrentVectoneIdIfMultiVectoneMode
		{
			get
			{
				return _vectoneService.IsSingleVectoneMode() ? 0 : CurrentVectone.Id;
			}
		}

	}
}
