﻿﻿using System.IO;
using System.Web.Mvc;

using System.Collections.Generic;
using Vectone.Services.Vectones;
using Vectone.Services.Common;
using Vectone.Core;
using Vectone.Core.Domain.Users;
using Vectone.Collections;

namespace Vectone.Web.Framework.Controllers
{
    public static class ContollerExtensions
    {
        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <returns>Result</returns>
        public static string RenderPartialViewToString(this Controller controller)
        {
            return RenderPartialViewToString(controller, null, null);
        }
        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="viewName">View name</param>
        /// <returns>Result</returns>
        public static string RenderPartialViewToString(this Controller controller, string viewName)
        {
            return RenderPartialViewToString(controller, viewName, null);
        }
        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Result</returns>
        public static string RenderPartialViewToString(this Controller controller, object model)
        {
            return RenderPartialViewToString(controller, null, model);
        }
        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="viewName">View name</param>
        /// <param name="model">Model</param>
        /// <returns>Result</returns>
        public static string RenderPartialViewToString(this Controller controller, string viewName, object model)
        {
            
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

		/// <summary>
		/// Get active vectone scope (for multi-vectone configuration mode)
		/// </summary>
		/// <param name="controller">Controller</param>
		/// <param name="vectoneService">Vectone service</param>
		/// <param name="workContext">Work context</param>
		/// <returns>Vectone ID; 0 if we are in a shared mode</returns>
		public static int GetActiveVectoneScopeConfiguration(this Controller controller, IVectoneService vectoneService, IWorkContext workContext)
		{
			//ensure that we have 2 (or more) vectonesites
			if (vectoneService.GetAllVectones().Count < 2)
				return 0;

			var vectoneid = workContext.CurrentUser.GetAttribute<int>(SystemUserAttributeNames.AdminAreaVectoneScopeConfiguration);
			var vectone = vectoneService.GetVectoneById(vectoneid);
			return vectone != null ? vectone.Id : 0;
		}
    }
}
