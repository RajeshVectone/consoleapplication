﻿using Vectone.Web.Framework.Controllers;
using Vectone.Web.Framework.Security;

namespace Vectone.Web.Framework.Controllers
{

    [UserLastActivity]
    [VectoneIpAddress]
    [VectoneLastVisitedPage]
   
    [VectoneClosedAttribute]
 
    [LanguageSeoCodeAttribute]
    [RequireHttpsByConfigAttribute(SslRequirement.Retain)]
    public abstract partial class PublicControllerBase : VectoneController
    {
    }
}
