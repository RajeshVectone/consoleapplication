﻿using System;
using System.Web.Mvc;
using Vectone.Core;
using Vectone.Core.Data;
using Vectone.Core.Infrastructure;
using Vectone.Services.Users;

namespace Vectone.Web.Framework.Controllers
{
    public class UserLastActivityAttribute : ActionFilterAttribute
    {

        public Lazy<IWorkContext> WorkContext { get; set; }
        public Lazy<IUserService> UserService { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!DataSettings.DatabaseIsInstalled())
                return;

            if (filterContext == null || filterContext.HttpContext == null || filterContext.HttpContext.Request == null)
                return;

            //don't apply filter to child methods
            if (filterContext.IsChildAction)
                return;

            //only GET requests
            if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                return;

            var workContext = WorkContext.Value;
            var user = workContext.CurrentUser;

            //update last activity date
            //if (user.LastActivityDateUtc.AddMinutes(1.0) < DateTime.UtcNow)
            {
                var userService = UserService.Value;
                user.LastActivityDateUtc = DateTime.UtcNow;
                userService.UpdateUser(user);
            }
        }
    }
}
