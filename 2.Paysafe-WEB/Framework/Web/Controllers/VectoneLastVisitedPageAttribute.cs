﻿using System;
using System.Web.Mvc;
using Vectone.Core;
using Vectone.Core.Data;
using Vectone.Core.Domain.Users;
using Vectone.Core.Infrastructure;
using Vectone.Services.Common;

namespace Vectone.Web.Framework.Controllers
{
    public class VectoneLastVisitedPageAttribute : ActionFilterAttribute
    {

		public Lazy<IWebHelper> WebHelper { get; set; }
		public Lazy<IWorkContext> WorkContext { get; set; }
		public Lazy<UserSettings> UserSettings { get; set; }
		public Lazy<IGenericAttributeService> GenericAttributeService { get; set; }
		
		public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
			if (!DataSettings.DatabaseIsInstalled())
                return;

            if (filterContext == null || filterContext.HttpContext == null || filterContext.HttpContext.Request == null)
                return;

            //don't apply filter to child methods
            if (filterContext.IsChildAction)
                return;

            //only GET requests
            if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                return;

            var userSettings = UserSettings.Value;
            if (!userSettings.VectoneLastVisitedPage)
                return;

            var webHelper = this.WebHelper.Value;
            var pageUrl = webHelper.GetThisPageUrl(true);
            if (!String.IsNullOrEmpty(pageUrl))
            {
                var workContext = WorkContext.Value;
                var genericAttributeService = GenericAttributeService.Value;

                var previousPageUrl = workContext.CurrentUser.GetAttribute<string>(SystemUserAttributeNames.LastVisitedPage);
                if (!pageUrl.Equals(previousPageUrl))
                {
                    genericAttributeService.SaveAttribute(workContext.CurrentUser, SystemUserAttributeNames.LastVisitedPage, pageUrl);
                }
            }
        }
    }
}
