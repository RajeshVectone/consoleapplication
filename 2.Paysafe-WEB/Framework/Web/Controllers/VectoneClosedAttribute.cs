﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Vectone.Core;
using Vectone.Core.Data;
using Vectone.Core.Domain;
using Vectone.Core.Infrastructure;
using Vectone.Services.Users;

namespace Vectone.Web.Framework.Controllers
{
    public class VectoneClosedAttribute : ActionFilterAttribute
    {
		private static readonly List<Tuple<string, string>> s_permittedRoutes = new List<Tuple<string, string>> 
		{
 			new Tuple<string, string>("Vectone.Web.Controllers.UserController", "Login"),
			new Tuple<string, string>("Vectone.Web.Controllers.UserController", "Logout"),
			new Tuple<string, string>("Vectone.Web.Controllers.CommonController", "VectoneClosed"),
			new Tuple<string, string>("Vectone.Web.Controllers.CommonController", "SetLanguage")
		};


		public Lazy<IWorkContext> WorkContext { get; set; }
		public Lazy<VectoneInformationSettings> VectoneInformationSettings { get; set; }
		
		public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null || filterContext.HttpContext == null)
                return;

            HttpRequestBase request = filterContext.HttpContext.Request;
            if (request == null)
                return;

			//don't apply filter to child methods
			if (filterContext.IsChildAction)
				return;

            string actionName = filterContext.ActionDescriptor.ActionName;
            if (String.IsNullOrEmpty(actionName))
                return;

            string controllerName = filterContext.Controller.ToString();
            if (String.IsNullOrEmpty(controllerName))
                return;

			if (!DataSettings.DatabaseIsInstalled())
                return;

            var vectoneInformationSettings = VectoneInformationSettings.Value;
            if (!vectoneInformationSettings.VectoneClosed)
                return;

            if (!IsPermittedRoute(controllerName, actionName)) 
			{
               
                    var vectoneClosedUrl = new UrlHelper(filterContext.RequestContext).RouteUrl("VectoneClosed");
                    filterContext.Result = new RedirectResult(vectoneClosedUrl);
                
            }
        }

		private static bool IsPermittedRoute(string controllerName, string actionName)
		{
			foreach (var route in s_permittedRoutes)
			{
				if (controllerName.IsCaseInsensitiveEqual(route.Item1) && actionName.IsCaseInsensitiveEqual(route.Item2))
				{
					return true;
				}
			}

			return false;
		}
    }
}
