﻿namespace Vectone.Web.Framework.Localization
{
    public interface ILocalizedModelLocal
    {
        int LanguageId { get; set; }
    }
}
