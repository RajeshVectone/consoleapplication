﻿using System;
using Vectone.Core.Localization;

namespace Vectone.Web.Framework.Localization
{
	public interface IText
	{
		LocalizedString Get(string key, params object[] args);
	}
}
