﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using Autofac;
using Vectone.Core;

using Vectone.Core.Domain.Localization;
using Vectone.Core.Plugins;

using Vectone.Services.Localization;
using Vectone.Utilities;

namespace Vectone.Web.Framework.Plugins
{
	public partial class PluginHelper
	{
		protected readonly IComponentContext _ctx;
		private PluginDescriptor _plugin;
		private string _interfaceVersion;
		private Language _language;
		private int? _currencyID;
		private string _currencyCode;
		
		private Dictionary<string, string> _resMap = new Dictionary<string, string>();

		public PluginHelper(IComponentContext componentContext, string systemName)
		{
			SystemName = systemName;
			_ctx = componentContext;
		}

		public static string NotSpecified
		{
			get
			{
				return "__nospec__";	// explicitly do not set a field
			}
		}

		public string SystemName { get; set; }

		public PluginDescriptor Plugin
		{
			get
			{
				if (_plugin == null)
				{
					_plugin = _ctx.Resolve<IPluginFinder>().GetPluginDescriptorBySystemName(SystemName);
					if (_plugin == null)
					{
						var provider = _ctx.Resolve<IProviderManager>().GetProvider(SystemName);
						if (provider != null)
						{
							_plugin = provider.Metadata.PluginDescriptor;
						}
					}
				}
				return _plugin;
			}
		}

		public string InterfaceVersion
		{
			get
			{
				if (_interfaceVersion == null)
				{
					_interfaceVersion = "{0}_v{1}".FormatWith(CommonHelper.GetAppSetting<string>("vt:ApplicationName"), Plugin.Version);
				}
				return _interfaceVersion;
			}
		}

		public Language Language
		{
			get
			{
				if (_language == null)
				{
					_language = _ctx.Resolve<IWorkContext>().WorkingLanguage;
				}
				return _language;
			}
		}

		

	

		

		

		public string GetResource(string keyOrShortKey)
		{
			string res = "";
			try
			{
				if (keyOrShortKey.HasValue())
				{
					if (!keyOrShortKey.Contains('.'))
					{
						if (Plugin.ResourceRootKey.HasValue())
						{
							keyOrShortKey = "{0}.{1}".FormatWith(Plugin.ResourceRootKey, keyOrShortKey);
						}
						else
						{
							keyOrShortKey = "Plugins.{0}.{1}".FormatWith(SystemName, keyOrShortKey);
						}
					}

					if (_resMap.ContainsKey(keyOrShortKey))
					{
						return _resMap[keyOrShortKey];
					}

					res = _ctx.Resolve<ILocalizationService>().GetResource(keyOrShortKey);

					if (res.IsNullOrEmpty())
						res = keyOrShortKey;

					_resMap.Add(keyOrShortKey, res);
				}
			}
			catch (Exception exc)
			{
				exc.Dump();
			}
			return res;
		}

		public XmlDocument CreateXmlDocument(Func<XmlWriter, bool> content)
		{
			XmlDocument doc = null;
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Encoding = new UTF8Encoding(false);

			using (MemoryStream ms = new MemoryStream())
			{
				using (XmlWriter xw = XmlWriter.Create(ms, settings))
				{
					if (content(xw))
					{
						xw.Flush();

						doc = new XmlDocument();
						doc.LoadXml(Encoding.UTF8.GetString(ms.ToArray()));
					}

					xw.Close();
					ms.Close();
					return doc;
				}
			}
		}

		

	}
}
