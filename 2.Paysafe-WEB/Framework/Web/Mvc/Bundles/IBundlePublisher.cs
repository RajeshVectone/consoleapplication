﻿using System.Web.Optimization;

namespace Vectone.Web.Framework.Mvc.Bundles
{
    /// <summary>
    /// <remarks>codehint: vt-add</remarks>
    /// </summary>
    public interface IBundlePublisher
    {
        void RegisterBundles(BundleCollection bundles);
    }
}
