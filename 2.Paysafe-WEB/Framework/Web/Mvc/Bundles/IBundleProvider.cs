﻿using System.Web.Optimization;

namespace Vectone.Web.Framework.Mvc.Bundles
{
    public interface IBundleProvider
    {
        void RegisterBundles(BundleCollection bundles);

        int Priority { get; }
    }
}
