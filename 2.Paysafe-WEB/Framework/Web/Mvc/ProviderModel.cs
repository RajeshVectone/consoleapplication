﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Vectone.Core.Plugins;
using Vectone.Web.Framework.Localization;

namespace Vectone.Web.Framework.Mvc
{
	public class ProviderModel : ModelBase, ILocalizedModel<ProviderLocalizedModel>
	{
		private IList<ProviderLocalizedModel> _locales;
		
		[VectoneResourceDisplayName("Common.SystemName")]
		public string SystemName { get; set; }

		[VectoneResourceDisplayName("Common.FriendlyName")]
		[AllowHtml]
		public string FriendlyName { get; set; }

		[VectoneResourceDisplayName("Common.Description")]
		[AllowHtml]
		public string Description { get; set; }

		[VectoneResourceDisplayName("Common.DisplayOrder")]
		public int DisplayOrder { get; set; }

		public bool IsEditable { get; set; }

		public bool IsConfigurable { get; set; }

		public RouteInfo ConfigurationRoute { get; set; }

		public PluginDescriptor PluginDescriptor { get; set; }

		[VectoneResourceDisplayName("Admin.Providers.ProvidingPlugin")]
		public string ProvidingPluginFriendlyName { get; set; }

		/// <summary>
		/// Returns the absolute path of the provider's icon url. 
		/// </summary>
		/// <remarks>
		/// The parent plugin's icon url is returned as a fallback, if provider icon cannot be resolved.
		/// </remarks>
		public string IconUrl { get; set; }

		public IList<ProviderLocalizedModel> Locales
		{
			get
			{
				if (_locales == null)
				{
					_locales = new List<ProviderLocalizedModel>();
				}
				return _locales;
			}
			set
			{
				_locales = value;
			}
		}
	}

	public class ProviderLocalizedModel : ILocalizedModelLocal
	{
		public int LanguageId { get; set; }

		[VectoneResourceDisplayName("Common.FriendlyName")]
		[AllowHtml]
		public string FriendlyName { get; set; }

		[VectoneResourceDisplayName("Common.Description")]
		[AllowHtml]
		public string Description { get; set; }
	}

}