﻿#region Using...

using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Web.Mvc;
using System.Web.WebPages;
using Vectone.Core;
using Vectone.Core.Localization;
using Vectone.Core.Data;
using Vectone.Core.Infrastructure;

using Vectone.Services.Localization;

using Vectone.Web.Framework.Localization;

using Vectone.Collections;
using System.Linq;
using Vectone.Core.Logging;
using Vectone.Web.Framework.Controllers;

#endregion

namespace Vectone.Web.Framework.ViewEngines.Razor
{
    public abstract class WebViewPage<TModel> : System.Web.Mvc.WebViewPage<TModel>
    {

		private IText _text;
        private IWorkContext _workContext;

		private IList<NotifyEntry> _internalNotifications;
      
        private ExpandoObject _themeVars;
        private bool? _isHomePage;
        protected bool IsLogin
        { get; set; }
      
        protected bool IsHomePage
        {
            get
            {
                if (!_isHomePage.HasValue)
                {
                    var routeData = Url.RequestContext.RouteData;
                    _isHomePage = routeData.GetRequiredString("controller").IsCaseInsensitiveEqual("Home") &&
                        routeData.GetRequiredString("action").IsCaseInsensitiveEqual("Index");
                }
                return _isHomePage.Value;
            }
        }

		protected bool HasMessages
		{
			get
			{
				return ResolveNotifications(null).Any();
			}
		}

		protected ICollection<LocalizedString> GetMessages(NotifyType type)
		{
			return ResolveNotifications(type).AsReadOnly();
		}

		private IEnumerable<LocalizedString> ResolveNotifications(NotifyType? type)
		{	
						
			IEnumerable<NotifyEntry> result = Enumerable.Empty<NotifyEntry>();

			if (_internalNotifications == null)
			{
				string key = NotifyAttribute.NotificationsKey;
				IList<NotifyEntry> entries;
				
				if (TempData.ContainsKey(key))
				{
					entries = TempData[key] as IList<NotifyEntry>;
					if (entries != null)
					{
						result = result.Concat(entries);
					}
				}

				if (ViewData.ContainsKey(key))
				{
					entries = ViewData[key] as IList<NotifyEntry>;
					if (entries != null)
					{
						result = result.Concat(entries);
					}
				}

				_internalNotifications = new List<NotifyEntry>(result);
			}

			if (type == null)
			{
				return _internalNotifications.Select(x => x.Message);
			}

			return _internalNotifications.Where(x => x.Type == type.Value).Select(x => x.Message);
		}

        /// <summary>
        /// Get a localized resource
        /// </summary>
        public Localizer T
        {
            get
            {
				return _text.Get;
            }
        }

        public IWorkContext WorkContext
        {
            get
            {
                return _workContext;
            }
        }
        
        public override void InitHelpers()
        {
            base.InitHelpers();

            if (DataSettings.DatabaseIsInstalled())
            {
				_text = EngineContext.Current.Resolve<IText>();
                _workContext = EngineContext.Current.Resolve<IWorkContext>();
            }
        }

        public HelperResult RenderWrappedSection(string name, object wrapperHtmlAttributes)
        {
            Action<TextWriter> action = delegate(TextWriter tw)
                                {
                                    var htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(wrapperHtmlAttributes);
                                    var tagBuilder = new TagBuilder("div");
                                    tagBuilder.MergeAttributes(htmlAttributes);

                                    var section = RenderSection(name, false);
                                    if (section != null)
                                    {
                                        tw.Write(tagBuilder.ToString(TagRenderMode.StartTag));
                                        section.WriteTo(tw);
                                        tw.Write(tagBuilder.ToString(TagRenderMode.EndTag));
                                    }
                                };
            return new HelperResult(action);
        }

        public HelperResult RenderSection(string sectionName, Func<object, HelperResult> defaultContent)
        {
            return IsSectionDefined(sectionName) ? RenderSection(sectionName) : defaultContent(new object());
        }

        public override string Layout
        {
            get
            {
                var layout = base.Layout;

                if (!string.IsNullOrEmpty(layout))
                {
                    var filename = System.IO.Path.GetFileNameWithoutExtension(layout);
                    ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindView(ViewContext.Controller.ControllerContext, filename, "");

                    if (viewResult.View != null && viewResult.View is RazorView)
                    {
                        layout = (viewResult.View as RazorView).ViewPath;
                    }
                }

                return layout;
            }
            set
            {
                base.Layout = value;
            }
        }

       

      
       

    }

    public abstract class WebViewPage : WebViewPage<dynamic>
    {
    }
}