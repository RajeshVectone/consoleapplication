﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vectone.Core.Caching;
using Vectone.Core.Domain.Configuration;
using Vectone.Core.Domain.Users;
using Vectone.Core.Domain.Localization;
using Vectone.Core.Domain.Vectones;

using Vectone.Core.Events;
using Vectone.Core.Infrastructure;



namespace Vectone.Web.Framework
{

    public class FrameworkCacheConsumer :
       
        IConsumer<EntityInserted<UserRole>>,
        IConsumer<EntityUpdated<UserRole>>,
        IConsumer<EntityDeleted<UserRole>>,
        IConsumer<EntityInserted<VectoneSite>>,
        IConsumer<EntityDeleted<VectoneSite>>,
        IConsumer<EntityInserted<Language>>,
        IConsumer<EntityUpdated<Language>>,
        IConsumer<EntityDeleted<Language>>,
        IConsumer<EntityUpdated<Setting>>
		
    {

    
		
       
        public const string VECTONESITE_LANGUAGE_MAP_KEY = "vt.fw.vectonelangmap";

        private readonly ICacheManager _cacheManager;
		private readonly ICacheManager _aspCache;

		public FrameworkCacheConsumer(Func<string, ICacheManager> cache)
        {
			this._cacheManager = cache("static");
			this._aspCache = cache("aspnet");
        }

      

        public void HandleEvent(EntityDeleted<UserRole> eventMessage)
        {
            
        }

        public void HandleEvent(EntityUpdated<UserRole> eventMessage)
        {
          
        }

        public void HandleEvent(EntityInserted<UserRole> eventMessage)
        {
           
        }


        public void HandleEvent(EntityInserted<VectoneSite> eventMessage)
        {
            _cacheManager.Remove(VECTONESITE_LANGUAGE_MAP_KEY);
        }

        public void HandleEvent(EntityDeleted<VectoneSite> eventMessage)
        {
            _cacheManager.Remove(VECTONESITE_LANGUAGE_MAP_KEY);
        }

        public void HandleEvent(EntityInserted<Language> eventMessage)
        {
            _cacheManager.Remove(VECTONESITE_LANGUAGE_MAP_KEY);
        }

        public void HandleEvent(EntityUpdated<Language> eventMessage)
        {
            _cacheManager.Remove(VECTONESITE_LANGUAGE_MAP_KEY);
        }

        public void HandleEvent(EntityDeleted<Language> eventMessage)
        {
            _cacheManager.Remove(VECTONESITE_LANGUAGE_MAP_KEY);
        }

        public void HandleEvent(EntityUpdated<Setting> eventMessage)
        {
            // clear models which depend on settings
          
        }

        #region Helpers

      
      

        #endregion

	}

}
