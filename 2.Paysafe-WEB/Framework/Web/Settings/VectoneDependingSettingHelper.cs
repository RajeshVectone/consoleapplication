﻿using System.Linq;
using System.Web.Mvc;
using Vectone.Services.Configuration;
using Fasterflect;
using System.Collections.Generic;

namespace Vectone.Web.Framework.Settings
{
	/// <remarks>codehint: vt-add</remarks>
	public class VectoneDependingSettingHelper
	{
		private ViewDataDictionary _viewData;

		public VectoneDependingSettingHelper(ViewDataDictionary viewData)
		{
			_viewData = viewData;
		}

		public static string ViewDataKey { get { return "VectoneDependingSettingData"; } }
		public VectoneDependingSettingData Data
		{
			get
			{
				return _viewData[ViewDataKey] as VectoneDependingSettingData;
			}
		}

		private bool IsOverrideChecked(string settingKey, FormCollection form)
		{
			var rawOverrideKey = form.AllKeys.FirstOrDefault(k => k.IsCaseInsensitiveEqual(settingKey + "_OverrideForVectone"));

			if (rawOverrideKey.HasValue())
			{
				var checkboxValue = form[rawOverrideKey].EmptyNull().ToLower();
				return checkboxValue.Contains("on") || checkboxValue.Contains("true");
			}
			return false;
		}
		public bool IsOverrideChecked(object settings, string name, FormCollection form)
		{
			var key = settings.GetType().Name + "." + name;
			return IsOverrideChecked(key, form);
		}
		public void AddOverrideKey(object settings, string name)
		{
			var key = settings.GetType().Name + "." + name;
			Data.OverrideSettingKeys.Add(key);
		}
		public void CreateViewDataObject(int activeVectoneScopeConfiguration, string rootSettingClass = null)
		{
			_viewData[ViewDataKey] = new VectoneDependingSettingData()
			{
				ActiveVectoneScopeConfiguration = activeVectoneScopeConfiguration,
				RootSettingClass = rootSettingClass
			};
		}

		public void GetOverrideKeys(object settings, object model, int vectoneid, ISettingService settingService, bool isRootModel = true)
		{
			if (vectoneid <= 0)
				return;		// single vectone mode -> there are no overrides

			var data = Data;
			if (data == null)
				data = new VectoneDependingSettingData();

			var settingName = settings.GetType().Name;
			var properties = settings.GetType().GetProperties();

            var modelType = model.GetType();

			foreach (var prop in properties)
			{
				var name = prop.Name;
                var modelProperty = modelType.GetProperty(name);

				if (modelProperty == null)
					continue;	// setting is not configurable or missing or whatever... however we don't need the override info

				var key = settingName + "." + name;
                var setting = settingService.GetSettingByKey<string>(key, vectoneid: vectoneid);

				if (setting != null)
					data.OverrideSettingKeys.Add(key);
			}

			if (isRootModel)
			{
				data.ActiveVectoneScopeConfiguration = vectoneid;
				data.RootSettingClass = settingName;

				_viewData[ViewDataKey] = data;
			}
		}
		public void UpdateSettings(object settings, FormCollection form, int vectoneid, ISettingService settingService)
		{
			var settingName = settings.GetType().Name;
			var properties = settings.GetType().GetProperties();

			foreach (var prop in properties)
			{
				var name = prop.Name;
				var key = settingName + "." + name;

				if (vectoneid == 0 || IsOverrideChecked(key, form))
				{
					dynamic value = settings.TryGetPropertyValue(name);
					settingService.SetSetting(key, value == null ? "" : value, vectoneid, false);
				}
				else if (vectoneid > 0)
				{
					settingService.DeleteSetting(key, vectoneid);
				}
			}
		}
	}
}
