﻿using System.Collections.Generic;

namespace Vectone.Web.Framework.Settings
{
	/// <remarks>codehint: vt-add</remarks>
	public class VectoneDependingSettingData
	{
		public VectoneDependingSettingData()
		{
			OverrideSettingKeys = new List<string>();
		}

		public int ActiveVectoneScopeConfiguration { get; set; }
		public List<string> OverrideSettingKeys { get; set; }
		public string RootSettingClass { get; set; }
	}
}
