﻿using System;
using System.Linq;
using System.Web;
using Vectone.Collections;
using Vectone.Core;
using Vectone.Core.Caching;
using Vectone.Core.Domain.Users;
using Vectone.Core.Domain.Localization;
using Vectone.Core.Fakes;
using Vectone.Services.Authentication;
using Vectone.Services.Common;
using Vectone.Services.Configuration;
using Vectone.Services.Users;
using Vectone.Services.Localization;
using Vectone.Services.Vectones;
using Vectone.Web.Framework.Localization;

namespace Vectone.Web.Framework
{
    /// <summary>
    /// Work context for web application
    /// </summary>
    public partial class WebWorkContext : IWorkContext
    {
        private const string UserCookieName = "Vectone.user";

        private readonly HttpContextBase _httpContext;
        private readonly IUserService _userService;
        private readonly IVectoneContext _vectoneContext;
        private readonly IAuthenticationService _authenticationService;
        private readonly ILanguageService _languageService;

        private readonly IGenericAttributeService _attrService;

        private readonly LocalizationSettings _localizationSettings;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;
        private readonly IVectoneService _vectoneService;
        private readonly ISettingService _settingService;



        private Language _cachedLanguage;
        private User _cachedUser;

        private User _originalUserIfImpersonated;

        public WebWorkContext(Func<string, ICacheManager> cacheManager,
            HttpContextBase httpContext,
            IUserService userService,
            IVectoneContext vectoneContext,
            IAuthenticationService authenticationService,
            ILanguageService languageService,

            IGenericAttributeService attrService,

            LocalizationSettings localizationSettings,
            IWebHelper webHelper, IVectoneService vectoneService, ISettingService settingService)
        {
            this._cacheManager = cacheManager("static");
            this._httpContext = httpContext;
            this._userService = userService;
            this._vectoneContext = vectoneContext;
            this._authenticationService = authenticationService;
            this._languageService = languageService;
            this._attrService = attrService;

            this._localizationSettings = localizationSettings;
            this._webHelper = webHelper;
            this._vectoneService = vectoneService;
            this._settingService = settingService;
        }

        protected HttpCookie GetUserCookie()
        {
            if (_httpContext == null || _httpContext.Request == null)
                return null;

            return _httpContext.Request.Cookies[UserCookieName];
        }

        protected void SetUserCookie(Guid userGuid)
        {
            if (_httpContext != null && _httpContext.Response != null)
            {
                var cookie = new HttpCookie(UserCookieName);
                cookie.HttpOnly = true;
                cookie.Value = userGuid.ToString();
                if (userGuid == Guid.Empty)
                {
                    cookie.Expires = DateTime.Now.AddMonths(-1);
                }
                else
                {
                    int cookieExpires = 24 * 365; //TODO make configurable
                    cookie.Expires = DateTime.Now.AddHours(cookieExpires);
                }

                try
                {
                    if (_httpContext.Response.Cookies[UserCookieName] != null)
                        _httpContext.Response.Cookies.Remove(UserCookieName);
                }
                catch (Exception) { }

                _httpContext.Response.Cookies.Add(cookie);
            }
        }



        /// <summary>
        /// Gets or sets the current user
        /// </summary>
        public User CurrentUser
        {
            get
            {
                if (_cachedUser != null)
                    return _cachedUser;

                User user = null;
                if (_httpContext == null || _httpContext.IsFakeContext())
                {
                    //check whether request is made by a background task
                    //in this case return built-in user record for background task
                    user = _userService.GetUserBySystemName(SystemUserNames.BackgroundTask);
                }

                //check whether request is made by a search engine
                //in this case return built-in user record for search engines 
                //or comment the following two lines of code in order to disable this functionality
                //if (user == null || user.Deleted || !user.Active)
                if (user == null)
                {
                    if (_webHelper.IsSearchEngine(_httpContext))
                        user = _userService.GetUserBySystemName(SystemUserNames.SearchEngine);
                }

                //registered user
                //if (user == null || user.Deleted || !user.Active)
                if (user == null)
                {
                    user = _authenticationService.GetAuthenticatedUser();
                }

                //impersonate user if required (currently used for 'phone order' support)
                //if (user != null && !user.Deleted && user.Active)
                if (user != null)
                {
                    int? impersonatedUserId = user.GetAttribute<int?>(SystemUserAttributeNames.ImpersonatedUserId);
                    if (impersonatedUserId.HasValue && impersonatedUserId.Value > 0)
                    {
                        var impersonatedUser = _userService.GetUserById(impersonatedUserId.Value);
                        //if (impersonatedUser != null && !impersonatedUser.Deleted && impersonatedUser.Active)
                        if (impersonatedUser != null)
                        {
                            //set impersonated user
                            _originalUserIfImpersonated = user;
                            user = impersonatedUser;
                        }
                    }
                }

                //load guest user
                //if (user == null || user.Deleted || !user.Active)
                if (user == null)
                {
                    var userCookie = GetUserCookie();
                    if (userCookie != null && !String.IsNullOrEmpty(userCookie.Value))
                    {
                        Guid userGuid;
                        if (Guid.TryParse(userCookie.Value, out userGuid))
                        {
                            var userByCookie = _userService.GetUserByGuid(userGuid);

                            user = userByCookie;
                        }
                    }
                }

                //create guest if not exists
                //if (user == null || user.Deleted || !user.Active)
                if (user == null)
                {
                    // user = _userService.InsertGuestUser();
                    user = _userService.GetUserById(1);
                }


                //validation
                //if (!user.Deleted && user.Active)
                if (user != null)
                {
                    //SetUserCookie(user.UserGuid);
                    _cachedUser = user;
                }

                return _cachedUser;
            }
            set
            {
                //SetUserCookie(value.UserGuid);
                _cachedUser = value;
            }
        }

        /// <summary>
        /// Gets or sets the original user (in case the current one is impersonated)
        /// </summary>
        public User OriginalUserIfImpersonated
        {
            get
            {
                return _originalUserIfImpersonated;
            }
        }

        /// <summary>
        /// Get or set current user working language
        /// </summary>
        public Language WorkingLanguage
        {
            get
            {
                if (_cachedLanguage != null)
                    return _cachedLanguage;

                int vectoneid = _vectoneContext.CurrentVectone.Id;
                int userLangId = 0;

                if (this.CurrentUser != null)
                {
                    userLangId = this.CurrentUser.GetAttribute<int>(
                        SystemUserAttributeNames.LanguageId,
                        _attrService,
                        _vectoneContext.CurrentVectone.Id);
                }

                #region Get language from URL (if possible)
                if (_localizationSettings.SeoFriendlyUrlsForLanguagesEnabled && _httpContext != null && _httpContext.Request != null)
                {
                    var helper = new LocalizedUrlHelper(_httpContext.Request, true);
                    string seoCode;
                    if (helper.IsLocalizedUrl(out seoCode))
                    {
                        if (this.IsPublishedLanguage(seoCode, vectoneid))
                        {
                            // the language is found. now we need to save it
                            var langBySeoCode = _languageService.GetLanguageBySeoCode(seoCode);
                            if (this.CurrentUser != null && userLangId != langBySeoCode.Id)
                            {
                                userLangId = langBySeoCode.Id;
                                this.SetUserLanguage(langBySeoCode.Id, vectoneid);
                            }
                            _cachedLanguage = langBySeoCode;
                            return langBySeoCode;
                        }
                    }
                }
                #endregion

                if (_localizationSettings.DetectBrowserUserLanguage && (userLangId == 0 || !this.IsPublishedLanguage(userLangId, vectoneid)))
                {
                    #region Get Browser UserLanguage

                    // Fallback to browser detected language
                    Language browserLanguage = null;

                    if (_httpContext != null && _httpContext.Request != null && _httpContext.Request.UserLanguages != null)
                    {
                        var userLangs = _httpContext.Request.UserLanguages.Select(x => x.Split(new[] { ';' }, 2, StringSplitOptions.RemoveEmptyEntries)[0]);
                        if (userLangs.HasItems())
                        {
                            foreach (var culture in userLangs)
                            {
                                browserLanguage = _languageService.GetLanguageByCulture(culture);
                                if (browserLanguage != null && this.IsPublishedLanguage(browserLanguage.Id, vectoneid))
                                {
                                    // the language is found. now we need to save it
                                    if (this.CurrentUser != null && userLangId != browserLanguage.Id)
                                    {
                                        userLangId = browserLanguage.Id;
                                        SetUserLanguage(userLangId, vectoneid);
                                    }
                                    _cachedLanguage = browserLanguage;
                                    return browserLanguage;
                                }
                            }
                        }
                    }

                    #endregion
                }

                if (userLangId > 0 && this.IsPublishedLanguage(userLangId, vectoneid))
                {
                    _cachedLanguage = _languageService.GetLanguageById(userLangId);
                    return _cachedLanguage;
                }

                // Fallback
                userLangId = this.GetDefaultLanguageId(vectoneid);
                SetUserLanguage(userLangId, vectoneid);

                _cachedLanguage = _languageService.GetLanguageById(userLangId);
                return _cachedLanguage;
            }
            set
            {
                var languageId = value != null ? value.Id : 0;
                this.SetUserLanguage(languageId, _vectoneContext.CurrentVectone.Id);
                _cachedLanguage = null;
            }
        }

        private void SetUserLanguage(int languageId, int vectoneid)
        {
            _attrService.SaveAttribute(
                this.CurrentUser,
                SystemUserAttributeNames.LanguageId,
                languageId,
                vectoneid);
        }







        /// <summary>
        /// Get or set value indicating whether we're in admin area
        /// </summary>
        public bool IsAdmin { get; set; }

        public bool IsPublishedLanguage(string seoCode, int vectoneid = 0)
        {
            if (vectoneid <= 0)
                vectoneid = _vectoneContext.CurrentVectone.Id;

            var map = this.GetVectoneLanguageMap();
            if (map.ContainsKey(vectoneid))
            {
                return map[vectoneid].Any(x => x.Item2 == seoCode);
            }

            return false;
        }

        internal bool IsPublishedLanguage(int languageId, int vectoneid = 0)
        {
            if (languageId <= 0)
                return false;

            if (vectoneid <= 0)
                vectoneid = _vectoneContext.CurrentVectone.Id;

            var map = this.GetVectoneLanguageMap();
            if (map.ContainsKey(vectoneid))
            {
                return map[vectoneid].Any(x => x.Item1 == languageId);
            }

            return false;
        }

        public string GetDefaultLanguageSeoCode(int vectoneid = 0)
        {
            if (vectoneid <= 0)
                vectoneid = _vectoneContext.CurrentVectone.Id;

            var map = this.GetVectoneLanguageMap();
            if (map.ContainsKey(vectoneid))
            {
                return map[vectoneid].FirstOrDefault().Item2;
            }

            return null;
        }

        internal int GetDefaultLanguageId(int vectoneid = 0)
        {
            if (vectoneid <= 0)
                vectoneid = _vectoneContext.CurrentVectone.Id;

            var map = this.GetVectoneLanguageMap();
            if (map.ContainsKey(vectoneid))
            {
                return map[vectoneid].FirstOrDefault().Item1;
            }

            return 0;
        }

        /// <summary>
        /// Gets a map of active/published vectone languages
        /// </summary>
        /// <returns>A map of vectone languages where key is the vectone id and values are tuples of lnguage ids and seo codes</returns>
        protected virtual Multimap<int, Tuple<int, string>> GetVectoneLanguageMap()
        {
            var result = _cacheManager.Get(FrameworkCacheConsumer.VECTONESITE_LANGUAGE_MAP_KEY, () =>
            {
                var map = new Multimap<int, Tuple<int, string>>();

                var allVectones = _vectoneService.GetAllVectones();
                foreach (var vectone in allVectones)
                {
                    var languages = _languageService.GetAllLanguages(false, vectone.Id);
                    if (!languages.Any())
                    {
                        // language-less vectonesites aren't allowed but could exist accidentally. Correct this.
                        var firstVectoneLang = _languageService.GetAllLanguages(true, vectone.Id).FirstOrDefault();
                        if (firstVectoneLang == null)
                        {
                            // absolute fallback
                            firstVectoneLang = _languageService.GetAllLanguages(true).FirstOrDefault();
                        }
                        map.Add(vectone.Id, new Tuple<int, string>(firstVectoneLang.Id, firstVectoneLang.UniqueSeoCode));
                    }
                    else
                    {
                        foreach (var lang in languages)
                        {
                            map.Add(vectone.Id, new Tuple<int, string>(lang.Id, lang.UniqueSeoCode));
                        }
                    }
                }

                return map;
            }, 1440 /* 24 hrs */);

            return result;
        }

    }
}
