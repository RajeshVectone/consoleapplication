﻿using Vectone.Core.Configuration;

namespace Vectone.Web.Framework.WebApi
{
	public class WebApiSettings : ISettings
	{
		public int ValidMinutePeriod { get; set; }
		public bool LogUnauthorized { get; set; }
	}
}
