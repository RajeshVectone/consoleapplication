﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Vectone.Services")]
[assembly: InternalsVisibleTo("Vectone.Services.Tests")]
