﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectone.Services.GenericServices
{
    public interface IGenericServices<T1, T2>
    {
        List<T1> GenericMethod(T2 input, Object obj, string spName, string strConn);
    }
}
