﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vectone.Data;

namespace Vectone.Services.GenericServices
{
    public class GenericServices<T1, T2> : IGenericServices<T1, T2>
    {
        public List<T1> GenericMethod(T2 input, Object obj, string spName, string strConn)
        {
            List<T1> outputList = new List<T1>();
            try
            {
                using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[strConn].ConnectionString))
                {
                    var sp = spName;
                    conn.Open();

                    var result = conn.Query<T1>(
                            sp, obj, commandType: CommandType.StoredProcedure);

                    outputList = result.ToList();

                    return outputList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

}
