﻿using System;
using System.Net;
using Vectone.Core;
using Vectone.Services.Tasks;

namespace Vectone.Services.Common
{
    /// <summary>
    /// Represents a task for keeping the site alive
    /// </summary>
    public partial class KeepAliveTask : ITask
    {
		private readonly IVectoneContext _vectoneContext;

		public KeepAliveTask(IVectoneContext vectoneContext)
        {
			this._vectoneContext = vectoneContext;
        }

		public void Execute(TaskExecutionContext ctx)
        {
			var vectoneUrl = _vectoneContext.CurrentVectone.Url.TrimEnd('\\').EnsureEndsWith("/");
            string url = vectoneUrl + "keepalive/index";

            try
            {
                using (var wc = new WebClient())
                {
                    //wc.Headers.Add("Vectone.NET"); // makes problems
                    if (!url.StartsWith("http://", StringComparison.OrdinalIgnoreCase) && !url.StartsWith("https://", StringComparison.OrdinalIgnoreCase)) 
					{
						url = "http://" + url;
					}
					wc.DownloadString(url);
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                {
                    var resp = (HttpWebResponse)ex.Response;
                    if (resp.StatusCode == HttpStatusCode.NotFound) // HTTP 404
                    {
                        // the page was not found (as it can be expected with some webservers)
                        return;
                    }
                }
                // throw any other exception - this should not occur
                throw;
            }
        }
    }
}
