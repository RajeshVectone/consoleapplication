using System;
using Vectone.Core.Domain.Users;

namespace Vectone.Services.Authentication
{
    /// <summary>
    /// Authentication service interface
    /// </summary>
    public partial interface IAuthenticationService 
    {
        void SignIn(User user, bool createPersistentCookie);
        void SignOut();
        User GetAuthenticatedUser();
        /*Added on 27-04-2015 - by Hari - for getting if Persistent connection*/
        Boolean? GetIsPersistent();
    }
}