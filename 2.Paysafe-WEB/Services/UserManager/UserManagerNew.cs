﻿#region Using

using System.Collections.Generic;
using Vectone.Core.Domain.Users;
using Vectone.Services.Users;

#endregion

namespace Vectone.Services.UserManager
{
    public class UserManagerNew : IUserManagerNew
    {                       
        private readonly IUserService _userService;     
        private readonly IUserRegistrationService _userRegistrationService;

        public UserManagerNew(IUserService userService, IUserRegistrationService userRegistrationService)         
        {
            _userService = userService;
            _userRegistrationService = userRegistrationService;
        }
        

        //public static void Seed()
        //{
        //    // Make sure we always have at least the demo user available to login with
        //    // this ensures the user does not have to explicitly register upon first use
        //    var demo = new IdentityUser
        //    {
        //        Id = "6bc8cee0-a03e-430b-9711-420ab0d6a596",
        //        Email = "demo@email.com",
        //        UserName = "John Doe",
        //        PasswordHash = "APc6/pVPfTnpG89SRacXjlT+sRz+JQnZROws0WmCA20+axszJnmxbRulHtDXhiYEuQ==",
        //        SecurityStamp = "18272ba5-bf6a-48a7-8116-3ac34dbb7f38"
        //    };

        //    UserStore.Context.Set<IdentityUser>().AddOrUpdate(demo);
        //    UserStore.Context.SaveChanges();
        //}


        public void Seed()
        {
            // Make sure we always have at least the demo user available to login with
            // this ensures the user does not have to explicitly register upon first use
            
            var demouser = new Vectone.Core.Domain.Users.User
            {                
                Email = "demo@email.com",
                Username = "John Doe",                
            };

            this._userService.InsertUser(demouser);
        }

        public Result Create(Vectone.Core.Domain.Users.User user, string password)
        {
            Result errResult = new Result();
            try
            {
                this._userService.InsertUser(user);
            }
            catch (System.Exception ex)
            {
                
                errResult.Errors = new List<string>();
                errResult.Errors.Add(ex.Message);
            }
            return errResult;
        }


        public User ValidateUser(string Email, string Password)
        {
            if (_userRegistrationService.ValidateUser(Email, Password))
            {                
                var user = _userService.GetUserByEmail(Email);
                return user;
            }
            return null;
        }
    }

    public interface IUserManagerNew
    {
        void Seed();
        Result Create(Vectone.Core.Domain.Users.User user, string password);
        User ValidateUser(string Email, string Password);
    }

    public class Result
    {
        public List<string> Errors { get; set; }
        public bool Succeeded { get; set; }
    }
}