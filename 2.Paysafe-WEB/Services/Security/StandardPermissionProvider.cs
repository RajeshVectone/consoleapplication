using System.Collections.Generic;
using Vectone.Core.Domain.Users;
using Vectone.Core.Domain.Security;

namespace Vectone.Services.Security
{
    public partial class StandardPermissionProvider : IPermissionProvider
    {
        //admin area permissions
        public static readonly PermissionRecord AccessAdminPanel = new PermissionRecord { Name = "Access admin area", SystemName = "AccessAdminPanel", Category = "Standard" };

        //public static readonly PermissionRecord AllowUserImpersonation = new PermissionRecord { Name = "Admin area. Allow User Impersonation", SystemName = "AllowUserImpersonation", Category = "Users" };

        //public static readonly PermissionRecord ManageUsers = new PermissionRecord { Name = "Admin area. Manage Users", SystemName = "ManageUsers", Category = "Users" };
        //public static readonly PermissionRecord ManageUserRoles = new PermissionRecord { Name = "Admin area. Manage User Roles", SystemName = "ManageUserRoles", Category = "Users" };
        //public static readonly PermissionRecord ManageLanguages = new PermissionRecord { Name = "Admin area. Manage Languages", SystemName = "ManageLanguages", Category = "Configuration" };
        //public static readonly PermissionRecord ManageSettings = new PermissionRecord { Name = "Admin area. Manage Settings", SystemName = "ManageSettings", Category = "Configuration" };
      
        //public static readonly PermissionRecord ManageActivityLog = new PermissionRecord { Name = "Admin area. Manage Activity Log", SystemName = "ManageActivityLog", Category = "Configuration" };
        //public static readonly PermissionRecord ManageAcl = new PermissionRecord { Name = "Admin area. Manage ACL", SystemName = "ManageACL", Category = "Configuration" };
        //public static readonly PermissionRecord ManageEmailAccounts = new PermissionRecord { Name = "Admin area. Manage Email Accounts", SystemName = "ManageEmailAccounts", Category = "Configuration" };
        //public static readonly PermissionRecord ManageVectones = new PermissionRecord { Name = "Admin area. Manage Vectones", SystemName = "ManageVectones", Category = "Configuration" };
        //public static readonly PermissionRecord ManageScheduleTasks = new PermissionRecord { Name = "Admin area. Manage Schedule Tasks", SystemName = "ManageScheduleTasks", Category = "Configuration" };             

        public static readonly PermissionRecord ManageAutomationRulesList = new PermissionRecord { Name = "User area.Manage Rules List", SystemName = "ManageRulesList", Category = "Automation" };
        public static readonly PermissionRecord ManageAutomationCreateRules = new PermissionRecord { Name = "User area. Manage Create Rule", SystemName = "ManageCreateRule", Category = "Automation" };
        public static readonly PermissionRecord ManageAutomationQueueRules = new PermissionRecord { Name = "User area. Manage Queue Rules", SystemName = "ManageQueueRules", Category = "Automation" };
        public static readonly PermissionRecord ManageAutomationRejectedRules = new PermissionRecord { Name = "User area. Manage Rejected Rules", SystemName = "ManageRejectedRules", Category = "Automation" };

        public static readonly PermissionRecord ManageEventList = new PermissionRecord { Name = "User area. Manage Event List", SystemName = "ManageEventList", Category = "Event" };
        public static readonly PermissionRecord ManageCreateEvent = new PermissionRecord { Name = "User area. Manage Create Event", SystemName = "ManageCreateEvent", Category = "Event" };
        public static readonly PermissionRecord ManageEventMappingList = new PermissionRecord { Name = "User area. Manage Mapping List", SystemName = "ManageMappingList", Category = "Event" };
        public static readonly PermissionRecord ManageEventCreateMapping = new PermissionRecord { Name = "User area. Manage Create Mapping", SystemName = "ManageCreateMapping", Category = "Event" };

        public static readonly PermissionRecord ManageURLList = new PermissionRecord { Name = "User area. Manage URL List", SystemName = "ManageURLList", Category = "URL" };
        public static readonly PermissionRecord ManageCreateURL = new PermissionRecord { Name = "User area. Manage Create URL", SystemName = "ManageCreateURL", Category = "URL" };

        public static readonly PermissionRecord ManageConditionList = new PermissionRecord { Name = "User area. Manage Condition List", SystemName = "ManageConditionList", Category = "Condition" };
        public static readonly PermissionRecord ManageCreateCondition = new PermissionRecord { Name = "User area. Manage Create Condition", SystemName = "ManageCreateCondition", Category = "Condition" };

        public static readonly PermissionRecord ManageAPIList = new PermissionRecord { Name = "User area. Manage API List", SystemName = "ManageAPIList", Category = "API" };
        public static readonly PermissionRecord ManageCreateAPI = new PermissionRecord { Name = "User area. Manage Create API", SystemName = "ManageCreateAPI", Category = "API" };

        public static readonly PermissionRecord ManageSMSContentList = new PermissionRecord { Name = "User area. Manage SMS Content List", SystemName = "ManageSMSContentList", Category = "SMSContent" };
        public static readonly PermissionRecord ManageCreateSMSContent = new PermissionRecord { Name = "User area. Manage Create SMS Content", SystemName = "ManageCreateSMSContent", Category = "SMSContent" };
        
        public static readonly PermissionRecord ManageUsers = new PermissionRecord { Name = "Admin area. Manage Users", SystemName = "ManageUsers", Category = "Settings" };
        public static readonly PermissionRecord ManageRoles = new PermissionRecord { Name = "Admin area. Manage Roles", SystemName = "ManageRoles", Category = "Settings" };
        public static readonly PermissionRecord ManagePermissions = new PermissionRecord { Name = "Admin area. Manage Permissions", SystemName = "ManagePermissions", Category = "Settings" };


        public static readonly PermissionRecord ViewAutomations = new PermissionRecord { Name = "User area. View Automation Report", SystemName = "ViewAutomationReport", Category = "Settings" };

        //public vectone permissions

        public static readonly PermissionRecord PublicVectoneAllowNavigation = new PermissionRecord { Name = "Public vectone. Allow navigation", SystemName = "PublicVectoneAllowNavigation", Category = "PublicVectone" };

        public virtual IEnumerable<PermissionRecord> GetPermissions()
        {
            return new[]
            {
                /*
                AccessAdminPanel,
                AllowUserImpersonation,

                ManageUsers,
                ManageUserRoles,

                ManageLanguages,
                ManageSettings,

             
                ManageActivityLog,
                ManageAcl,
                ManageEmailAccounts,
				ManageVectones,

                ManageScheduleTasks,
                */
                AccessAdminPanel,
               ManageAutomationRulesList,
               ManageAutomationCreateRules,
               ManageAutomationQueueRules,
               ManageAutomationRejectedRules,
               ManageEventList,
               ManageCreateEvent,
               ManageEventMappingList, 
               ManageEventCreateMapping,
               ManageURLList, 
               ManageCreateURL, 
               
               ManageConditionList,
               ManageCreateCondition,
               
               ManageAPIList, 
               ManageCreateAPI, 
               
               ManageSMSContentList,
               ManageCreateSMSContent,
               ViewAutomations,
               PublicVectoneAllowNavigation

            };
        }

        public virtual IEnumerable<DefaultPermissionRecord> GetDefaultPermissions()
        {
            return new[]
            {
                new DefaultPermissionRecord
                {
                    UserRoleSystemName = SystemUserRoleNames.Administrators,
                    PermissionRecords = new[]
                    {
                      ManageAutomationRulesList,
                       ManageAutomationCreateRules,
                       ManageAutomationQueueRules,
                       ManageAutomationRejectedRules,
                        ManageUsers,
                        ManageRoles,
                        ManagePermissions,
                        PublicVectoneAllowNavigation,
                    }
                },
              
                new DefaultPermissionRecord
                {
                    UserRoleSystemName = SystemUserRoleNames.Guests,
                    PermissionRecords = new[]
                    {
                        PublicVectoneAllowNavigation,
                    }
                },
                new DefaultPermissionRecord
                {
                    UserRoleSystemName = SystemUserRoleNames.Registered,
                    PermissionRecords = new[]
                    {
                        PublicVectoneAllowNavigation,
                    }
                },
            };
        }
    }
}