﻿using System.Collections.Generic;
using Vectone.Core.Domain.Security;

namespace Vectone.Services.Security
{
    public interface IPermissionProvider
    {
        IEnumerable<PermissionRecord> GetPermissions();
        IEnumerable<DefaultPermissionRecord> GetDefaultPermissions();
    }
}
