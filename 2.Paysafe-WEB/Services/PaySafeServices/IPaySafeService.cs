﻿using System;
using System.Collections.Generic;
using Vectone.Core;
using Vectone.Core.Domain.PaySafe;

namespace Vectone.Services.PaySafeServices
{
    public interface IPaySafeService
    {
        //void InsertPaymentData(PaySafeRequest paySafeRequest,PaySafeTransaction paysafeTransaction);
        PaymentReturn InsertPaymentData(PaySafeRequest paySafeRequest);

        void UpdatePaymentData(PaySafeRequest paySafeRequest, string reasoncode, int errorrcode);

        ReasonCode GetProviderReasonCode(string providercode, string @providerreasoncode);

        void InsertPaymentDescription(PaymentDescription paymentDescription);

        PaySafeResponse GetTransactionByRefID(string referenceid, int paymentstep);

        void PaymentUpdateValidate(PaySafeResponse paySafeResponse);
    }
}
