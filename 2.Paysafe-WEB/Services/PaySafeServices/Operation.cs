﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NLog;
using Vectone.Core.Domain.PaySafe;
using Vectone.PSC;
using System.Net.Mail;
using System.IO;
using Newtonsoft.Json;
namespace Vectone.Services.PaySafeServices
{

    public class Operation : IOperation
    {
        //15-Oct-2015 : Moorthy : Added for email template send functionality
        private readonly static string SmtpServer = ConfigurationManager.AppSettings["SMTP.SERVER"];
        private readonly static string SmtpUsername = ConfigurationManager.AppSettings["SMTP.USERNAME"];
        private readonly static string SmtpPasswd = ConfigurationManager.AppSettings["SMTP.PASSWORD"];
        private readonly static bool SmtpSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SMTP.SSL"]);

        private PSC.SOPGClassicMerchantClient client;
        PaySafeTransaction _paysafeTransaction = new PaySafeTransaction();
        PaySafeService _paySafeService = new PaySafeService();
        // private  IPaySafeService _paySafeService;

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //public Operation(IPaySafeService paySafeService)
        //{
        //    this._paySafeService = paySafeService;
        //}
        public Operation()
        {

            try
            {
                // Create a new instance of MerchantClient. In this example a exception would be throwed in the
                // constructor, if the given language is invalid.
                client = new PSC.SOPGClassicMerchantClient(true, "EN", true, DevStatus.LIVE);

                // Each event from PSC SDK must be redirected to this application:
                client.logMessageEvent += new PSC.SOPGClassicMerchantClient.logMessageEventHandler(client_logMessageEvent);
            }

            catch (ArgumentException argEx)
            {

            }
        }


        //public static PaySafeTransaction Redemption(string siteCode, string productCode, string voucherNumber, string voucherValue, string baseCurr, string ticketValue)
        public PaySafeTransaction CreateDisposition_Old(PaySafeRequest _paysaferequest)
        {

            try
            {


                string datetime = System.DateTime.Now.ToShortDateString();

                //Log.Debug("--Start--" + datetime);
                Log.Info("Entering Pay Safe Card.");

                //var refId1 = "CTP" + _paysaferequest.Currency + "-PSC-" + _paysaferequest.Amount;
                ////var refId2 = GenerateId();

                //var refId = refId1 + "-" + datetime;                

                //client.DeleteLogMessages(Vectone.Logging.LogType.DEBUG);
                //client.DeleteLogMessages(Vectone.Logging.LogType.ERROR);
                //client.DeleteLogMessages(Vectone.Logging.LogType.INFO);
                //client.DeleteLogMessages(Vectone.Logging.LogType.WARNING);

                // Converts floating point numbers (float/double) to strings with two decimal digits:


                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalDigits = 2;
                string productCode = "PSC-" + _paysaferequest.Amount;

                client.SetUsername(ConfigurationManager.AppSettings["PaysafeUsername"]);
                client.SetPassword(ConfigurationManager.AppSettings["PaysafePassword"]);
                client.SetStatus(ConfigurationManager.AppSettings["Mode"]);
                //try
                //{
                //    SOPGClassicMerchantClient.com.paysafecard.soa.GetMidReturn midRet1 = client.ConfirmMerchantData(_paysaferequest.Currency);

                //}
                //catch (Exception e)
                //{
                //    Log.Debug("Calling Disposition" + e.Message);
                //}

                SOPGClassicMerchantClient.com.paysafecard.soa.GetMidReturn midRet = client.ConfirmMerchantData(_paysaferequest.Currency);

                Log.Info("ConfirmMerchantData : output : " + Newtonsoft.Json.JsonConvert.SerializeObject(midRet));
                if (midRet.resultCode == 0 && midRet.errorCode == 0)
                {

                    //Log.Debug("PaySafeRequest Successufully Passed.");

                    //Log.Debug("Calling SoapClient - Merchant Data.");

                    _paysaferequest.Productcode = productCode;
                    _paysaferequest.Paymentagent = "PS";
                    _paysaferequest.Servicetype = "MVNO";
                    _paysaferequest.Paymentmode = "PSC";
                    _paysaferequest.Paymentstep = 1;
                    _paysaferequest.Paymentstatus = 1;
                    //_paysafeTransaction.MtID = refId;

                    //20-Jan-2016 : Moorthy modified for common payment
                    //_paysaferequest.IPpaymentagent = "192.168.2.102";
                    //_paysaferequest.IPClient = GetSystemIP();
                    _paysaferequest.IPpaymentagent = _paysaferequest.IPClient;

                    _paysaferequest.Sitecode = "LO2";
                    //20-Jan-2016 : Moorthy : Commented to form the reference code with country code
                    //_paysaferequest.Applicationcode = "CTP" + _paysaferequest.Currency;

                    //Log.Info("Payment Process Start in insert.");
                    var paymentreturn = _paySafeService.InsertPaymentData(_paysaferequest);

                    Log.Info("Creating Disposition.");
                    client.SetMtID(paymentreturn.Ref_id);
                    _paysaferequest.OrderNumber = paymentreturn.Orderno.ToString();
                    _paysaferequest.MtId = paymentreturn.Ref_id;
                    var info = _paySafeService.InsertInfo(_paysaferequest);
                    client.SetSubID(ConfigurationManager.AppSettings["PaysafeSubID"]);
                    client.SetAmount(_paysaferequest.Total);
                    client.SetCurrency(_paysaferequest.Currency);
                    client.SetOkURL(_paysaferequest.OkUrl);
                    client.SetnOKURL(_paysaferequest.NokUrl);
                    //  client.SetPnURL(_paysaferequest.Pnurl);
                    client.SetMerchantClientID(ConfigurationManager.AppSettings["PaysafeClientID"]);
                    client.SetPnURL(_paysaferequest.Pnurl);
                    client.SetShopID(ConfigurationManager.AppSettings["PaysafeShopID"]);
                    client.SetShopLabel(ConfigurationManager.AppSettings["PaysafeShopLabel"]);

                    //client.SetRestrictedCountry("DE");
                    //client.SetRestrictedCountry("GB");

                    //client.SetMinAge("12");
                    //client.SetMinKycLevel("SIMPLE");

                    // In this example it's a test:


                    //No Error Log

                    SOPGClassicMerchantClient.com.paysafecard.soa.CreateDispositionReturn ret = client.CreateDisposition();

                    //}
                    //catch (Exception e)
                    //{
                    //    Log.Debug("Calling Disposition"+e.Message);
                    //}

                    //PaySafeService paySafeService = new PaySafeService();

                    Log.Info("Disposition Process Completed." + "Error Code : " + ret.errorCode + "Result code : " + ret.resultCode);
                    //Log.Debug("--End--" + datetime);

                    _paysafeTransaction.errCode = ret.errorCode;
                    _paysafeTransaction.resultCode = ret.resultCode.ToString();
                    _paysafeTransaction.MtID = ret.mtid;
                    _paysafeTransaction.ReturnCustomerURL = client.GetCustomerPanelURL();
                    _paysaferequest.MtId = ret.mtid;
                    _paysafeTransaction.orderNumber = paymentreturn.Orderno.ToString();
                    _paysaferequest.Paymentstatus = 3;
                    _paysaferequest.Paymentstep = 6;
                    _paysaferequest.Description = "Disposition Completed";
                    _paySafeService.UpdatePaymentData(_paysaferequest, _paysafeTransaction.resultCode, _paysafeTransaction.errCode);

                    //var response = new PaySafeResponse
                    //{
                    //    Errorcode = ret.errorCode,
                    //    MID = ret.mid,
                    //    MtID = ret.mtid,
                    //    Resultcode = ret.resultCode,
                    //    SubID = ret.subId,
                    //    ReturnCustomerURL = client.GetCustomerPanelURL()
                    //};
                    //_paysafeTransaction.PaySafeResponse = response;



                    return _paysafeTransaction;

                }
            }

            catch (Exception exception1)
            {
                Log.Error("CreateDisposition_Old : " + exception1.Message);
                Exception exception = exception1;
                _paysafeTransaction.errCode = -1;
                _paysafeTransaction.errMsg = exception1.Message;
                //  _paysafeTransaction.errDescription = exception1.Message;
            }
            return _paysafeTransaction;
        }

        public PaySafeTransaction CreateDisposition(PaySafeRequest _paysaferequest)
        {
            Log.Info("CreateDisposition()");
            try
            {
                //string datetime = System.DateTime.Now.ToShortDateString();

                //Log.Debug("--Start--" + datetime);
                //Log.Debug("Entering Pay Safe Card.");
                //Log.Debug(JsonConvert.SerializeObject(_paysaferequest));

                #region Commented
                //var refId1 = "CTP" + _paysaferequest.Currency + "-PSC-" + _paysaferequest.Amount;
                ////var refId2 = GenerateId();

                //var refId = refId1 + "-" + datetime;                

                //client.DeleteLogMessages(Vectone.Logging.LogType.DEBUG);
                //client.DeleteLogMessages(Vectone.Logging.LogType.ERROR);
                //client.DeleteLogMessages(Vectone.Logging.LogType.INFO);
                //client.DeleteLogMessages(Vectone.Logging.LogType.WARNING);

                // Converts floating point numbers (float/double) to strings with two decimal digits: 
                #endregion

                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalDigits = 2;
                string productCode = "PSC-" + _paysaferequest.Amount;
                if (!string.IsNullOrEmpty(_paysaferequest.Productcode) && _paysaferequest.Productcode.Contains("BUN"))
                    productCode = _paysaferequest.Productcode;
                Log.Info("productCode : " + productCode);

                client.SetUsername(ConfigurationManager.AppSettings["PaysafeUsername"]);
                client.SetPassword(ConfigurationManager.AppSettings["PaysafePassword"]);
                client.SetStatus(ConfigurationManager.AppSettings["Mode"]);

                #region Commented
                //try
                //{
                //    SOPGClassicMerchantClient.com.paysafecard.soa.GetMidReturn midRet1 = client.ConfirmMerchantData(_paysaferequest.Currency);

                //}
                //catch (Exception e)
                //{
                //    Log.Debug("Calling Disposition" + e.Message);
                //} 
                #endregion

                //14-Apr-2018 : Moorthy : Added for the Paysafe TLS 1.2 Update
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                SOPGClassicMerchantClient.com.paysafecard.soa.GetMidReturn midRet = client.ConfirmMerchantData(_paysaferequest.Currency);

                Log.Info("ConfirmMerchantData : output : " + Newtonsoft.Json.JsonConvert.SerializeObject(midRet));

                if (midRet.resultCode == 0 && midRet.errorCode == 0)
                {

                    Log.Info("PaySafeRequest Successufully Passed.");

                    //Log.Debug("Calling SoapClient - Merchant Data.");

                    _paysaferequest.Productcode = productCode;

                    _paysaferequest.Paymentagent = "PS";
                    _paysaferequest.Servicetype = "MVNO";
                    _paysaferequest.Paymentmode = "PSC";

                    _paysaferequest.Paymentstep = 1;
                    _paysaferequest.Paymentstatus = 1;
                    _paysaferequest.IPpaymentagent = _paysaferequest.IPClient;
                    _paysaferequest.Sitecode = _paysaferequest.Sitecode;

                    #region Commented
                    //_paysafeTransaction.MtID = refId;
                    //_paysaferequest.IPpaymentagent = "192.168.2.102";
                    //_paysaferequest.IPClient = GetSystemIP();
                    //_paysaferequest.Sitecode = "LO2";
                    //_paysaferequest.Applicationcode = "CTP" + _paysaferequest.Currency;
                    //_paysaferequest.Applicationcode = _paysaferequest.Applicationcode + _paysaferequest.Currency;
                    //_paysaferequest.Accountid = "02108933";
                    //  _paysaferequest.Email = "test@gmail.com";
                    //Log.Debug("Payment Process Start in insert."); 
                    #endregion

                    var paymentreturn = _paySafeService.InsertPaymentData(_paysaferequest);

                    client.SetMtID(paymentreturn.Ref_id);
                    _paysaferequest.OrderNumber = paymentreturn.Orderno.ToString();
                    _paysaferequest.MtId = paymentreturn.Ref_id;
                    Log.Info("Reference Id : " + _paysaferequest.MtId);
                    var info = _paySafeService.InsertInfo(_paysaferequest);
                    client.SetSubID(ConfigurationManager.AppSettings["PaysafeSubID"]);
                    client.SetAmount(_paysaferequest.Total);
                    client.SetCurrency(_paysaferequest.Currency);
                    client.SetOkURL(_paysaferequest.OkUrl);
                    client.SetnOKURL(_paysaferequest.NokUrl);
                    client.SetMerchantClientID(ConfigurationManager.AppSettings["PaysafeClientID"]);
                    client.SetPnURL(_paysaferequest.Pnurl);
                    client.SetShopID(ConfigurationManager.AppSettings["PaysafeShopID"]);
                    client.SetShopLabel(ConfigurationManager.AppSettings["PaysafeShopLabel"]);

                    SOPGClassicMerchantClient.com.paysafecard.soa.CreateDispositionReturn ret = client.CreateDisposition();

                    Log.Info("Disposition Process Completed." + " , Error Code : " + ret.errorCode + " , Result code : " + ret.resultCode);
                    //Log.Debug("--End--" + datetime);

                    _paysafeTransaction.errCode = ret.errorCode;
                    _paysafeTransaction.errMsg = "Disposition Completed";
                    _paysafeTransaction.resultCode = ret.resultCode.ToString();
                    _paysafeTransaction.MtID = ret.mtid;
                    _paysafeTransaction.ReturnCustomerURL = client.GetCustomerPanelURL();
                    _paysaferequest.MtId = ret.mtid;
                    _paysafeTransaction.orderNumber = paymentreturn.Orderno.ToString();
                    _paysaferequest.Paymentstatus = 3;
                    _paysaferequest.Paymentstep = 6;
                    _paysaferequest.Description = "Disposition Completed";

                    //14-Feb-2019 : Moorthy : Modified for status 'Disposition Completed' to '1' instead of '0'
                    //_paySafeService.UpdatePaymentData(_paysaferequest, _paysafeTransaction.resultCode, _paysafeTransaction.errCode);
                    _paySafeService.UpdatePaymentData(_paysaferequest, _paysafeTransaction.resultCode, 1);

                    #region Commented
                    //var response = new PaySafeResponse
                    //{
                    //    Errorcode = ret.errorCode,
                    //    MID = ret.mid,
                    //    MtID = ret.mtid,
                    //    Resultcode = ret.resultCode,
                    //    SubID = ret.subId,
                    //    ReturnCustomerURL = client.GetCustomerPanelURL()
                    //};
                    //_paysafeTransaction.PaySafeResponse = response; 
                    #endregion

                    return _paysafeTransaction;

                }
            }
            catch (Exception ex)
            {
                Log.Error("CreateDisposition : " + ex.Message);
                Exception exception = ex;
                _paysafeTransaction.errCode = -1;
                _paysafeTransaction.errMsg = ex.Message;
            }
            return _paysafeTransaction;
        }

        private string GetSystemIP()
        {
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST
            // Get the IP
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
            return myIP;
        }


        public PaySafeTransaction ExecuteDebit(PaySafeRequest _paysaferequest)
        {
            Log.Info("ExecuteDebit()");
            try
            {
                Log.Info("Input : " + JsonConvert.SerializeObject(_paysaferequest));
                client.DeleteLogMessages(Vectone.Logging.LogType.DEBUG);
                client.DeleteLogMessages(Vectone.Logging.LogType.ERROR);
                client.DeleteLogMessages(Vectone.Logging.LogType.INFO);
                client.DeleteLogMessages(Vectone.Logging.LogType.WARNING);
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalDigits = 2;

                client.SetUsername(ConfigurationManager.AppSettings["PaysafeUsername"]);
                client.SetPassword(ConfigurationManager.AppSettings["PaysafePassword"]);
                client.SetMtID(_paysaferequest.MtId);
                client.SetSubID(ConfigurationManager.AppSettings["PaysafeSubID"]);
                client.SetAmount(_paysaferequest.Total);
                client.SetCurrency(_paysaferequest.Currency);
                client.SetOkURL(_paysaferequest.OkUrl);
                client.SetnOKURL(_paysaferequest.NokUrl);
                client.SetMerchantClientID(ConfigurationManager.AppSettings["PaysafeClientID"]);
                client.SetPnURL(_paysaferequest.Pnurl);
                client.SetShopID(ConfigurationManager.AppSettings["PaysafeShopID"]);
                client.SetShopLabel(ConfigurationManager.AppSettings["PaysafeShopLabel"]);
                client.SetStatus(ConfigurationManager.AppSettings["Mode"]);


                //14-Apr-2018 : Moorthy : Added for the Paysafe TLS 1.2 Update
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                SOPGClassicMerchantClient.com.paysafecard.soa.GetSerialNumbersReturn GsRet = client.GetSerialNumbers();
                if (GsRet.dispositionState.ToUpper() == "S")
                {
                    SOPGClassicMerchantClient.com.paysafecard.soa.ExecuteDebitReturn ExRet = client.ExecuteDebit();
                    PaySafeBL _paysafeBl = new PaySafeBL();
                    string productCode = "PSC-" + _paysaferequest.Amount;
                    if (!string.IsNullOrEmpty(_paysaferequest.Productcode) && _paysaferequest.Productcode.Contains("BUN"))
                        productCode = _paysaferequest.Productcode;
                    var _paySafeRedeem = new PaySafeTopUpRedeem
                    {
                        Sitecode = _paysaferequest.Sitecode,
                        Product = productCode,
                        Currency = _paysaferequest.Currency,
                        ///  website=_paysaferequest.WebSite,
                        AccountNumber = _paysaferequest.Accountid,
                        Name = _paysaferequest.Name
                    };

                    _paysafeTransaction.errCode = ExRet.errorCode;
                    _paysafeTransaction.resultCode = ExRet.resultCode.ToString();
                    _paysafeTransaction.MtID = ExRet.mtid;
                    _paysafeTransaction.orderNumber = _paysaferequest.OrderNumber;
                    _paysafeTransaction.settleAmount = _paysaferequest.Total;
                    _paysafeTransaction.Amount = _paysaferequest.Amount;
                    _paySafeRedeem.PaySafeRequest = _paysaferequest;
                    _paySafeRedeem.PaySafeTransaction = _paysafeTransaction;
                    var _retur = _paysafeBl.RequestExecuteDebitPayment(_paySafeRedeem);

                    #region Commented
                    // _paySafeRedeem.OrderId = _paysaferequest.OrderNumeber;
                    //if (_retur.errCode == 0)
                    //{
                    //    CreateEmail(_paysaferequest);
                    //} 
                    #endregion
                }
                else
                {
                    _paysafeTransaction.errCode = -1;
                    _paysafeTransaction.errMsg = "Failed";
                }
            }
            catch (Exception ex)
            {
                Log.Error("ExecuteDebit : " + ex.Message);
                _paysafeTransaction.errCode = -1;
                _paysafeTransaction.errMsg = ex.Message;
            }
            return _paysafeTransaction;
        }

        #region CreateEmail
        //15-Oct-2015 : Moorthy : Added for email template send functionality
        public void CreateEmail(PaySafeRequest _paysaferequest)
        {
            string mailSubject = "Topup Confirmation";
            string mailLocation;
            mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/paysafeemail.htm").TrimEnd('/');
            StreamReader mailContent_file = new System.IO.StreamReader(mailLocation);
            string mailContent = mailContent_file.ReadToEnd();
            mailContent_file.Close();
            mailContent = mailContent
                .Replace("[topupSvcRsp.MerchantRefCode]", _paysafeTransaction.MtID)
                            .Replace("[currency]", _paysaferequest.Currency)
                            .Replace("[amount]", _paysaferequest.Amount)
                            .Replace("[vate]", _paysaferequest.Vat)
                            .Replace("[mb]", _paysaferequest.Mobile)
                            .Replace("[firstname]", _paysaferequest.Name)
                             .Replace("[streetName]", _paysaferequest.Street)
                             .Replace("[city]", _paysaferequest.City)
                             .Replace("[postCode]", _paysaferequest.PostCode)
                             .Replace("[country]", _paysaferequest.Country)
                             .Replace("[total]", _paysaferequest.Total)
                            .Replace("[Date]", DateTime.Now.Date.ToString("dd/M/yyyy"))
                            .Replace("[Bonus]", "");

            MailAddress mailFrom = new MailAddress("noreply@vectone.com", "vectone");
            MailAddressCollection mailTo = new MailAddressCollection();
            mailTo.Add(new MailAddress(_paysaferequest.Email, "customer"));
            SendEmail(true, mailFrom, mailTo, null, null, mailSubject, mailContent);
        }
        #endregion

        #region SendEmail
        //15-Oct-2015 : Moorthy : Added for email template send functionality
        bool SendEmail(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;

                SmtpClient smtp = new SmtpClient();
                using (var client = new SmtpClient(SmtpServer))
                {
                    var loginInfo = new NetworkCredential(SmtpUsername, SmtpPasswd);
                    client.EnableSsl = SmtpSsl;
                    client.UseDefaultCredentials = false;
                    client.Credentials = loginInfo;
                    client.Send(email);
                }
                return true;
            }
            catch (Exception) { return false; }
        }
        #endregion

        #region client_logMessageEvent
        void client_logMessageEvent(object sender, Vectone.Logging.LogMessage logMessage)
        {
            string logTypeStr = "";

            switch (logMessage.logType)
            {
                case Vectone.Logging.LogType.DEBUG: logTypeStr = "DEBUG"; break;
                case Vectone.Logging.LogType.ERROR: logTypeStr = "ERROR"; break;
                case Vectone.Logging.LogType.INFO: logTypeStr = "INFO"; break;
                case Vectone.Logging.LogType.WARNING: logTypeStr = "WARNING"; break;
            }

            if (logMessage.logType == Vectone.Logging.LogType.DEBUG)
            {
                return;
            }
        }
        #endregion

        #region GenerateId
        public static string GenerateId(int count = 4)
        {
            var guid = Guid.NewGuid().ToString("N");
            var rgx = new Regex("[^0-9]");
            var str = rgx.Replace(guid, string.Empty);

            return str.Substring(0, count);
        }
        #endregion
    }
}

