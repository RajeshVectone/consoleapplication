﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Newtonsoft.Json;
using NLog;
using Switchlab.DataAccessLayer;
using Vectone.Core.Domain.PaySafe;
using Vectone.Data;
using Vectone.Services.PaySafeServices;

namespace Vectone.Services.PaySafeServices
{
    public class PaySafeBL
    {
        private static string SP_SAVEPAYMENT_LOG;
        private Operation _operation;
        private static string SP_TOPUP_ACCOUNT;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        static PaySafeBL()
        {
            PaySafeBL.SP_SAVEPAYMENT_LOG = "ctpPay_insert_payment_log";
            PaySafeBL.SP_TOPUP_ACCOUNT = "ctpPay_TopUpBalance_v3";
        }

        public PaySafeBL()
        {
        }

        #region GetCountryByMobileNo
        //15-Oct-2015: Moorthy : Added for get country by mobileno
        public CommonOutput GetCountryByMobileNo(CommonInput input)
        {
            Log.Info("GetCountryByMobileNo()");
            CommonOutput output = new CommonOutput();
            output.errcode = -1;
            output.errmsg = "Unknown Exception";
            string _connection = ConfigurationManager.ConnectionStrings["Residential"].ConnectionString;
            SQLDataAccessLayer sQLDataAccessLayer = new Switchlab.DataAccessLayer.SQLDataAccessLayer(_connection);
            SqlCommand sqlCommand = new SqlCommand();
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@MobileNO", SqlDbType.VarChar, 100, ParameterDirection.Input, input.MobileNo);
            sqlCommand.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["timeoutdb"].ToString());
            sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "ctp_get_country_by_mobileno");
            try
            {
                DataSet dataSet = sQLDataAccessLayer.ExecuteQuery(sqlCommand);
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    output.errcode = 0;
                    output.errmsg = "Success";
                    output.Country = Convert.ToString(dataSet.Tables[0].Rows[0]["Country"]);
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "No records found";
                }
            }
            catch (Exception exception)
            {
                output.errcode = -1;
                output.errmsg = exception.Message;
                Log.Error(exception.Message);
                Log.Error(exception.StackTrace.ToString());
                Log.Error(exception.InnerException.Message);
            }
            return output;
        }
        #endregion

        public PaySafeTopUpRedeem RequestExecuteDebitPayment(PaySafeTopUpRedeem request)
        {
            Log.Info("RequestExecuteDebitPayment()");

            try
            {
                PaySafeService _paySafeServices = new PaySafeService();

                double str = default(double);
                //string str1 = "";
                //string str2 = "";
                string sitecode = request.Sitecode;
                string product = request.Product;
                // string voucherNumber = request.Voucher_Number;
                //string topupAmount = request.PaySafeTransaction.settleAmount;
                string topupAmount = request.PaySafeTransaction.settleAmount;
                string topupAmountTop = request.PaySafeTransaction.Amount;
                //PaySafeTransaction _paySafeTransaction = _operation.CreateDisposition(request.PaySafeRequest);
                int num = request.PaySafeTransaction.errCode;
                Log.Info("request.PaySafeTransaction.errCode : " + request.PaySafeTransaction.errCode);
                //string str3 = (string.IsNullOrEmpty(_paySafeTransaction.errDescription) ? _paySafeTransaction.txDescription : _paySafeTransaction.errDescription);
                request.MtID = request.PaySafeTransaction.MtID;
                if (num != 0)
                {
                    Log.Info("Payment Failed");
                    Log.Info("request.MtID : " + request.MtID);
                    Log.Info("request.PaySafeTransaction.orderNumber : " + request.PaySafeTransaction.orderNumber);
                    Log.Info("request.AccountNumber : " + request.AccountNumber);
                    Log.Info("Paymentstep = 11");
                    Log.Info("Paymentstatus = -2");

                    request.errCode = num;
                    // request.errMsg = str3;
                    //  string str4 = request.website;
                    string str5 = request.PaySafeTransaction.orderNumber;
                    string str6 = request.MtID;
                    string accountNumber = request.AccountNumber;
                    double num1 = default(double);
                    //  topupAmount = request.amo;
                    try
                    {
                        num1 = double.Parse(topupAmount.ToString());

                    }
                    catch (Exception e)
                    {
                        Log.Debug(e.Message);
                    }
                    string currency = request.Currency;
                    int num2 = request.errCode;

                    request.errMsg = "Payment Failed";
                    //07-Jun-2017 : Moorthy : Commented for not inserting log in the Residential DB, it's only for Chillitalk
                    //PaySafeBL.SavePaymentLog(str5, -8007, str6, accountNumber, "-1", num1, currency, num2.ToString(), request.errMsg, str, "", 0);

                    var paysafe = new PaySafeRequest
                    {
                        MtId = request.MtID,
                        Paymentstep = 11,
                        Paymentstatus = -2,
                        Description = "Payment Failed"

                    };
                    _paySafeServices.UpdatePaymentData(paysafe, request.PaySafeTransaction.resultCode, num2);
                }
                else
                {
                    #region Commented
                    //var paysafe1 = new PaySafeRequest
                    //{
                    //    MtId = request.MtID,
                    //    Paymentstep = 11,
                    //    Paymentstatus = 3,
                    //}; 
                    #endregion

                    Log.Info("Payment Success");
                    Log.Info("request.MtID : " + request.MtID);
                    Log.Info("request.PaySafeTransaction.orderNumber : " + request.PaySafeTransaction.orderNumber);
                    Log.Info("request.AccountNumber : " + request.AccountNumber);
                    Log.Info("Paymentstep = 7");
                    Log.Info("Paymentstatus = 3");
                    Log.Info("request.PaySafeRequest.Saverflag  : " + request.PaySafeRequest.Saverflag);

                    var _topupModel = new TopupModel
                    {
                        SiteCode = request.Sitecode,
                        IccId = request.AccountNumber,
                        TopupId = request.MtID,
                        Currency = request.Currency,
                        Amount = double.Parse(topupAmountTop.ToString()),

                    };
                    if (request.PaySafeRequest.Saverflag == false)
                    {
                        //07-Jun-2017 : Moorthy : Commented for not inserting log in the Residential DB, it's only for Chillitalk
                        //var reurnvalue = PaySafeBL.DoTopupResident(_topupModel, request.PaySafeTransaction.resultCode);
                        Invoice(request);

                        request.errMsg = "Payment Success";
                        double num1 = double.Parse(topupAmount.ToString());

                        //07-Jun-2017 : Moorthy : Commented for not inserting log in the Residential DB, it's only for Chillitalk
                        //PaySafeBL.SavePaymentLog(request.PaySafeTransaction.orderNumber, 8007, request.PaySafeTransaction.MtID, request.AccountNumber, "-1", num1, request.Currency, "100", reurnvalue.Errmsg, str, "", reurnvalue.TopuplogId.GetValueOrDefault());
                        request.errCode = num;

                        var paysafe = new PaySafeRequest
                        {
                            MtId = _topupModel.TopupId,
                            Paymentstep = 7,
                            Paymentstatus = 3,
                            Description = "Payment Sucess, Top Up Success",
                        };

                        _paySafeServices.UpdatePaymentData(paysafe, request.PaySafeTransaction.resultCode, num);
                    }
                    request.AfterBalance = decimal.Parse(str.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.Error("RequestExecuteDebitPayment : " + ex.Message);
                request.errCode = -1;
                request.errMsg = ex.Message;
            }

            return request;
        }

        private static void SavePaymentLog(string orderID, int status, string note, string accountid, string custid, double amount, string currcode, string errcode, string errmsg, double afterbal, string totalnotif, int topuplogid)
        {
            Log.Info("SavePaymentLog()");

            string _connection = ConfigurationManager.ConnectionStrings["Residential"].ConnectionString;
            //   Connstring connstring = new Connstring();
            //SQLDataAccessLayer sQLDataAccessLayer = new Switchlab.DataAccessLayer.SQLDataAccessLayer(connstring.GetConnstring(ConfigurationManager.AppSettings["sitecodedbCTP"].ToString(), string.Concat(website, ConfigurationManager.AppSettings["DBProfileKeyCTPRes"].ToString())));
            SQLDataAccessLayer sQLDataAccessLayer = new Switchlab.DataAccessLayer.SQLDataAccessLayer(_connection);
            SqlCommand sqlCommand = new SqlCommand();
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderID", SqlDbType.VarChar, 20, ParameterDirection.Input, orderID);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 4, ParameterDirection.Input, status);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@note", SqlDbType.VarChar, 200, ParameterDirection.Input, note);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@accountid", SqlDbType.VarChar, 8, ParameterDirection.Input, accountid);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custid", SqlDbType.Int, 8, ParameterDirection.Input, custid);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@amount", SqlDbType.Float, 16, ParameterDirection.Input, amount);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, currcode);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@errcode", SqlDbType.Int, 4, ParameterDirection.Input, errcode);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@errmsg", SqlDbType.VarChar, 64, ParameterDirection.Input, errmsg);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@afterbal", SqlDbType.Float, 8, ParameterDirection.Input, afterbal);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@total_notif", SqlDbType.VarChar, 250, ParameterDirection.Input, totalnotif);
            sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@topuplogid", SqlDbType.Int, 4, ParameterDirection.Input, topuplogid);
            sqlCommand.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["timeoutdb"].ToString());
            sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "ctpPay_insert_payment_log");
            try
            {
                sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message);
                Log.Error(exception.StackTrace.ToString());
                Log.Error(exception.InnerException.Message);
                throw exception;
            }
        }
        private static TopUpResultModel DoTopupResident(TopupModel topupModel, string resultcode)
        {
            Log.Info("DoTopupResident();");
            Log.Info("topupModel: " + JsonConvert.SerializeObject(topupModel));
            //! NOW we support VCLOUD only, other residential e.g chillitalk wasn't supported yet
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ESP.VCLOUD"].ConnectionString))
            {
                conn.Open();
                var p = new DynamicParameters();

                p.Add("@sitecode", topupModel.SiteCode);
                p.Add("@accountid", topupModel.IccId);
                p.Add("@cli");
                p.Add("@paymentid", 0);
                p.Add("@paymentref", topupModel.TopupId);
                p.Add("@productid");
                p.Add("@amount", topupModel.Amount);
                p.Add("@ccdc_curr", topupModel.Currency);
                p.Add("@calledby", "Web");
                p.Add("@note", topupModel.TopupId);
                p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@errmsg", dbType: DbType.String, direction: ParameterDirection.Output, size: 64);
                p.Add("@afterbal", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@total_notif", dbType: DbType.String, direction: ParameterDirection.Output, size: 255);
                p.Add("@topuplogid", dbType: DbType.Int32, direction: ParameterDirection.Output);

                conn.Execute("tp_do_topupres_byccdc_main", p, commandType: CommandType.StoredProcedure);

                var errCode = p.Get<int>("@errcode");
                var errMsg = p.Get<string>("@errmsg");
                var topuplogid = p.Get<int?>("@topuplogid");
                var notifMsg = p.Get<string>("@total_notif");
                PaySafeService _paySafeServices = new PaySafeService();
                var paysafe = new PaySafeRequest
                {
                    MtId = topupModel.TopupId,
                    Paymentstep = 7,
                    Paymentstatus = 3,
                    Description = "Payment Sucess, Top Up Success",

                };


                //if (errCode == 0)
                //{
                _paySafeServices.UpdatePaymentData(paysafe, "100", 100);

                // _payasfeServices.u UpdatePaymentData(PaySafeRequest paySafeRequest, string reasoncode, int errorrcode)
                //}
                //else
                //{
                //    paysafe.Description = "Payment Sucess, Top Up Failed";
                //    _paySafeServices.UpdatePaymentData(paysafe, resultcode, errCode);
                //}
                return new TopUpResultModel
                {
                    ErrCode = errCode,
                    Errmsg = errMsg,
                    IncentifData = null,
                    TopuplogId = topuplogid,
                    NotifMsg = notifMsg
                };
            }
        }

        private void Invoice(PaySafeTopUpRedeem request)
        {
            Log.Info("Invoice()");
            //07-Jun-2017 : Moorthy : Commented for not inserting log in the Residential DB, it's only for Chillitalk
            //using (var connss = new SqlConnection(ConfigurationManager.ConnectionStrings["Residential"].ConnectionString))
            //{
            //    //var sp = "web_res_get_ctp_tpayment_invoice_details";
            //    try
            //    {
            //        var sp = "web_res_insert_ctp_tpayment_invoice";
            //        connss.Open();


            //        var result = connss.Execute(
            //                sp, new
            //                {

            //                    @accountid = request.AccountNumber,
            //                    @merchant_ref = request.MtID,
            //                    @first_name = request.PaySafeRequest.Name,
            //                    @last_name = "",
            //                    @street = request.PaySafeRequest.Street,
            //                    @city = request.PaySafeRequest.City,
            //                    @postal_code = request.PaySafeRequest.PostCode,
            //                    @country = request.PaySafeRequest.Country,
            //                    @last4 = "1111",
            //                    @topup_amount = request.PaySafeRequest.Amount,
            //                    @vat_amount = request.PaySafeRequest.Vat,
            //                    @total_amount = request.PaySafeRequest.Total,
            //                    @currency = request.Currency,
            //                    @email = request.PaySafeRequest.Email

            //                },
            //                commandType: CommandType.StoredProcedure);

            //    }
            //    catch (Exception ex)
            //    {
            //        Log.Error(ex.Message);
            //    }
            //}

            try
            {
                using (var conns = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    var sps = "paysaf_insert_ctp_transaction_log";
                    var results = conns.Execute(
                       sps, new
                       {
                           @reference_id = request.MtID,
                           @country = request.PaySafeRequest.Country,
                           @actual_amount = request.PaySafeRequest.Amount,
                           @vat_amount = request.PaySafeRequest.Vat,
                           @total_amount = request.PaySafeRequest.Total
                       }, commandType: CommandType.StoredProcedure);

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

    }
}
