﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using NLog;
using Vectone.Core;
using Vectone.Core.Caching;
using Vectone.Core.Data;
using Vectone.Core.Domain.Common;
using Vectone.Core.Domain.PaySafe;
using Vectone.Core.Events;
using Vectone.Core.Localization;
using Vectone.Data;
using Vectone.Services.Common;
using Newtonsoft.Json;

namespace Vectone.Services.PaySafeServices
{
    public class PaySafeService : IPaySafeService
    {

        #region Fields

        //private readonly IRepository<User> _userRepository;
        //private readonly IRepository<UserRole> _userRoleRepository;
        //private readonly IRepository<GenericAttribute> _gaRepository;
        //private readonly IGenericAttributeService _genericAttributeService;
        //private readonly ICacheManager _cacheManager;
        //private readonly IEventPublisher _eventPublisher;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        #region Ctor

        public PaySafeService() { }

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="userRepository">User repository</param>
        /// <param name="userRoleRepository">User role repository</param>
        /// <param name="gaRepository">Generic attribute repository</param>
        /// <param name="genericAttributeService">Generic attribute service</param>
        /// <param name="eventPublisher">Event published</param>
        public PaySafeService(

            IDbContext dbContext
            )
        {

            this._dbContext = dbContext;


        }

        #endregion


        public PaymentReturn InsertPaymentData(PaySafeRequest paySafeRequest)
        {
            Log.Info("InsertPaymentData()");
            if (paySafeRequest == null)
                throw new ArgumentNullException("paySafeResponse");

            // var paymentDataInsert = (dynamic)null;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
            {
                conn.Open();

                try
                {
                    //Log.Debug("PaySafeRequest Successufully Passed.");
                    //Log.Debug("Input : " + JsonConvert.SerializeObject(paySafeRequest));

                    var p = new DynamicParameters();

                    p.Add("@sitecode", paySafeRequest.Sitecode);
                    p.Add("@applicationcode", paySafeRequest.Applicationcode);
                    p.Add("@productcode", paySafeRequest.Productcode);
                    p.Add("@paymentagent", paySafeRequest.Paymentagent);
                    p.Add("@servicetype", paySafeRequest.Servicetype);
                    p.Add("@paymentmode", paySafeRequest.Paymentmode);
                    p.Add("@paymentstep", paySafeRequest.Paymentstep);
                    p.Add("@paymentstatus", paySafeRequest.Paymentstatus);
                    //13-Feb-2019: Moorthy : Added for 'Some phone numbers are not captured in full' issue
                    if (String.IsNullOrEmpty(paySafeRequest.AliasName))
                        p.Add("@accountid", paySafeRequest.Accountid);
                    else
                        p.Add("@accountid", paySafeRequest.Accountid);
                    //p.Add("@last6digitccno");
                    p.Add("@totalamount", paySafeRequest.Total);
                    p.Add("@currency", paySafeRequest.Currency);
                    //p.Add("@subscriptionid");
                    p.Add("@merchantid", string.Empty);
                    p.Add("@providercode", "PS");
                    p.Add("@csreasoncode", string.Empty);
                    p.Add("@generalerrorcode", 0);
                    p.Add("@returnerror", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@sendToProduction", "false");
                    p.Add("@email", paySafeRequest.Email);
                    p.Add("@IPpaymentagent", paySafeRequest.Paymentagent);
                    p.Add("@IPClient", paySafeRequest.IPClient);


                    // conn.Execute("csPaymentInsert_v3_safecard", p, commandType: CommandType.StoredProcedure);
                    var paymentReturn = conn.Query<PaymentReturn>("csPaymentInsert_v3_safecard", p, commandType: CommandType.StoredProcedure).FirstOrDefault();


                    return paymentReturn;
                }
                catch (Exception e)
                {
                    Log.Error("Insert Error in Tpayment:" + e.Message.ToString());

                    return new PaymentReturn() { ErrorMessage = e.Message };
                }

                //return userLogin;

            }
        }
        public PaymentReturn InsertInfo(PaySafeRequest paySafeRequest)
        {
            Log.Info("InsertInfo()");

            if (paySafeRequest == null)
                throw new ArgumentNullException("paySafeResponse");

            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    conn.Open();

                    var p = new DynamicParameters();

                    p.Add("@sitecode", paySafeRequest.Sitecode);
                    p.Add("@applicationcode", paySafeRequest.Applicationcode);
                    p.Add("@reference_id", paySafeRequest.MtId);
                    p.Add("@accountid", paySafeRequest.Accountid);
                    p.Add("@email", paySafeRequest.Email);
                    p.Add("@amount", paySafeRequest.Amount);
                    p.Add("@vat_amount", paySafeRequest.Vat);
                    p.Add("@vatPercentage", paySafeRequest.VatPercentage);
                    p.Add("@total_amount", paySafeRequest.Total);
                    p.Add("@IPClient", paySafeRequest.IPClient);
                    p.Add("@currency", paySafeRequest.Currency);
                    p.Add("@OrderNumber", paySafeRequest.OrderNumber);
                    p.Add("@mobile", paySafeRequest.Mobile);
                    p.Add("@name", paySafeRequest.Name);
                    p.Add("@street", paySafeRequest.Street);
                    p.Add("@city", paySafeRequest.City);
                    p.Add("@postcode", paySafeRequest.PostCode);
                    p.Add("@country", paySafeRequest.Country);
                    p.Add("@pnURL", paySafeRequest.Pnurl);
                    p.Add("@OkUrl", paySafeRequest.OkUrl);
                    p.Add("@NokUrl", paySafeRequest.NokUrl);

                    var paymentReturn = conn.Query<PaymentReturn>("paysaf_insert_merchant_ref_details", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return paymentReturn;
                }
            }
            catch (Exception e)
            {
                Log.Info("Insert Error in Tpayment:" + e.Message);

                return new PaymentReturn() { ErrorMessage = e.Message };
            }
        }
        public PaySafeResponseInfo GetInfo(PaySafeRequest paySafeRequest)
        {
            Log.Info("GetInfo()");

            if (paySafeRequest == null)
                throw new ArgumentNullException("PaySafeRequest");
            PaySafeResponseInfo output = new PaySafeResponseInfo();
            try
            {
                Log.Info("paysaf_get_merchant_ref_details paySafeRequest.MtId : " + paySafeRequest.MtId);

                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@reference_id", paySafeRequest.MtId);
                    output = conn.Query<PaySafeResponseInfo>("paysaf_get_merchant_ref_details", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (output != null)
                        output.errcode = 0; output.errmsg = "Success";
                }
            }
            catch (Exception ex)
            {
                Log.Error("Insert Error in Tpayment:" + ex.Message.ToString());
                return new PaySafeResponseInfo() { Reference_id = "No data found", errcode = -1, errmsg = ex.Message };
            }
            return output;
        }


        public ReasonCode GetProviderReasonCode(string providercode, string providerreasoncode)
        {
            string sql = string.Format("reasoncode_get_by_provider_reason_code '{0}', '{1}'", providercode, providerreasoncode);
            var result = _dbContext.ExecuteStoredProcedureListT<ReasonCode>(sql).FirstOrDefault();

            return result;
        }

        public void InsertPaymentDescription(PaymentDescription paymentDescription)
        {
            string sql = string.Format("csPaymentAddDescription '{0}', '{1}'", paymentDescription.Reference_id, paymentDescription.Description);
            var result = _dbContext.ExecuteStoredProcedureListT<PaymentDescription>(sql).FirstOrDefault();
        }

        public void UpdatePaymentData(PaySafeRequest paySafeRequest, string reasoncode, int errorrcode)
        {
            Log.Info("UpdatePaymentData()");
            try
            {
                Log.Info(string.Format("csPaymentUpdate_safecard MtId : '{0}', Paymentstep : '{1}', Paymentstatus : '{2}', reasoncode : '{3}', errorrcode : '{4}', Description : '{5}'", paySafeRequest.MtId, paySafeRequest.Paymentstep, paySafeRequest.Paymentstatus, reasoncode, errorrcode, paySafeRequest.Description));
                //Log.Info("Payment step " + paySafeRequest.Paymentstep);
                if (paySafeRequest == null)
                    throw new ArgumentNullException("paySafeResponse");

                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@referenceid", paySafeRequest.MtId);
                    p.Add("@paymentstep", paySafeRequest.Paymentstep);
                    p.Add("@paymentstatus", paySafeRequest.Paymentstatus);
                    p.Add("@csreasoncode", reasoncode);
                    p.Add("@generalerrorcode", errorrcode);
                    p.Add("@description", paySafeRequest.Description);
                    conn.Execute("csPaymentUpdate_safecard", p, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        #region Commented
        //    public Vat Vat(PaySafeRequest _paysaferequest)
        //    {
        //        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MVNO.CTP"].ConnectionString))
        //        {
        //            conn.Open();
        //            var code2BySite4 = conn.Query<dynamic>(
        //                            "countrywise_vat_calculation", new
        //                            {
        //                                @country = _paysaferequest.Country,
        //                                @amount = _paysaferequest.Amount
        //                            },
        //                            commandType: CommandType.StoredProcedure);
        //            var _vat=new Vat
        //            {
        //            Amounts = code2BySite4.ElementAt(0).price,
        //            Vatprice = code2BySite4.ElementAt(0).vat_price,
        //            Total = code2BySite4.ElementAt(0).totalamount,
        //            Percent = code2BySite4.ElementAt(0).percent
        //            };

        //      return _vat;
        //        }
        //} 
        #endregion

        public PaySafeResponse GetTransactionByRefID(string referenceid, int paymentstep)
        {
            string sql = string.Format("csTransactionGetByRefIDStep,'{0}',{1}", referenceid, paymentstep);
            var result = _dbContext.ExecuteStoredProcedureListT<PaySafeResponse>(sql).FirstOrDefault();

            return result;
        }

        public void PaymentUpdateValidate(PaySafeResponse paySafeResponse)
        {
            //string sql = string.Format("csPaymentUpdateValidate, '{0}', {1}, {2}, '{3}','{4}','{5}','{6}',{7},'{8}'", paySafeResponse.Reference_id, paySafeResponse.Paymentstep, paySafeResponse.Paymentstatus, paySafeResponse.Providercode, paySafeResponse.Csreasoncode, paySafeResponse.Requestid, paySafeResponse.Requesttoken, paySafeResponse.Generalerrorcode, paySafeResponse.ReplyMessage);
            //var result = _dbContext.ExecuteStoredProcedureListT<PaySafeResponse>(sql).FirstOrDefault();
        }

        private TopUpResultModel DoTopupResident(TopupModel topupModel)
        {
            Log.Info("DoTopupResident()");
            Log.Info("topupModel: " +  JsonConvert.SerializeObject(topupModel));
            //! NOW we support VCLOUD only, other residential e.g chillitalk wasn't supported yet
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ESP.VCLOUD"].ConnectionString))
            {
                conn.Open();
                var p = new DynamicParameters();

                p.Add("@sitecode", topupModel.SiteCode);
                p.Add("@accountid", topupModel.IccId);
                p.Add("@cli");
                p.Add("@paymentid", 0);
                p.Add("@paymentref", topupModel.TopupId);
                p.Add("@productid");
                p.Add("@amount", topupModel.Amount);
                p.Add("@ccdc_curr", topupModel.Currency);
                p.Add("@calledby", "web");
                p.Add("@note", "Top Up by SWS");
                p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@errmsg", dbType: DbType.String, direction: ParameterDirection.Output, size: 64);
                p.Add("@afterbal", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@total_notif", dbType: DbType.String, direction: ParameterDirection.Output, size: 255);
                p.Add("@topuplogid", dbType: DbType.Int32, direction: ParameterDirection.Output);

                conn.Execute("tp_do_topupres_byccdc_main", p, commandType: CommandType.StoredProcedure);

                var errCode = p.Get<int>("@errcode");
                var errMsg = p.Get<string>("@errmsg");
                var topuplogid = p.Get<int?>("@topuplogid");
                var notifMsg = p.Get<string>("@total_notif");

                return new TopUpResultModel
                {
                    ErrCode = errCode,
                    Errmsg = errMsg,
                    IncentifData = null,
                    TopuplogId = topuplogid,
                    NotifMsg = notifMsg
                };
            }
        }
    }
}
