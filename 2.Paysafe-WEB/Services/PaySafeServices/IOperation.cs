﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vectone.Core.Domain.PaySafe;

namespace Vectone.Services.PaySafeServices
{
    public interface IOperation
    {
         PaySafeTransaction CreateDisposition(PaySafeRequest _paysaferequest);
         PaySafeTransaction ExecuteDebit(PaySafeRequest _paysaferequest);
    }
}
