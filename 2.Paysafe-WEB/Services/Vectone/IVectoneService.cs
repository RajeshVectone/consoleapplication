﻿using System.Collections.Generic;
using System.Web.Mvc;
using Vectone.Core.Domain.Vectones;

namespace Vectone.Services.Vectones
{
	/// <summary>
	/// Vectone service interface
	/// </summary>
	public partial interface IVectoneService
	{
		/// <summary>
		/// Deletes a vectone
		/// </summary>
		/// <param name="vectone">Vectone</param>
        void DeleteVectone(VectoneSite vectone);

		/// <summary>
		/// Gets all vectonesites
		/// </summary>
		/// <returns>Vectone collection</returns>
        IList<VectoneSite> GetAllVectones();

		/// <summary>
		/// Gets a vectone 
		/// </summary>
		/// <param name="vectoneid">Vectone identifier</param>
		/// <returns>Vectone</returns>
        VectoneSite GetVectoneById(int vectoneid);

		/// <summary>
		/// Inserts a vectone
		/// </summary>
		/// <param name="vectone">Vectone</param>
        void InsertVectone(VectoneSite vectone);

		/// <summary>
		/// Updates the vectone
		/// </summary>
		/// <param name="vectone">Vectone</param>
        void UpdateVectone(VectoneSite vectone);

		/// <summary>
		/// True if there's only one vectone. Otherwise False.
		/// </summary>
		/// <remarks>codehint: vt-add</remarks>
		bool IsSingleVectoneMode();
	}
}