﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Vectone.Core.Caching;
using Vectone.Core.Data;
using Vectone.Core.Domain.Vectones;
using Vectone.Core.Events;
using Vectone.Services.Localization;

namespace Vectone.Services.Vectones
{
	/// <summary>
	/// Vectone service
	/// </summary>
	public partial class VectoneService : IVectoneService
	{
		#region Constants
		private const string VECTONES_ALL_KEY = "Vectone.sites.all";
		private const string VECTONES_PATTERN_KEY = "Vectone.sites.";
        private const string VECTONES_BY_ID_KEY = "Vectone.sites.id-{0}";
		#endregion

		#region Fields

        private readonly IRepository<VectoneSite> _vectoneRepository;
		private readonly IEventPublisher _eventPublisher;
		private readonly ICacheManager _cacheManager;
		private bool? _isSingleVectoneMode = null;

		#endregion

		#region Ctor

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="cacheManager">Cache manager</param>
		/// <param name="vectoneRepository">Vectone repository</param>
		/// <param name="eventPublisher">Event published</param>
		public VectoneService(ICacheManager cacheManager,
            IRepository<VectoneSite> vectoneRepository,
			IEventPublisher eventPublisher)
		{
			this._cacheManager = cacheManager;
			this._vectoneRepository = vectoneRepository;
			this._eventPublisher = eventPublisher;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Deletes a vectone
		/// </summary>
		/// <param name="vectone">Vectone</param>
        public virtual void DeleteVectone(VectoneSite vectone)
		{
			if (vectone == null)
				throw new ArgumentNullException("vectone");

			var allVectones = GetAllVectones();
			if (allVectones.Count == 1)
				throw new Exception("You cannot delete the only configured vectone.");

			_vectoneRepository.Delete(vectone);

			_cacheManager.RemoveByPattern(VECTONES_PATTERN_KEY);

			//event notification
			_eventPublisher.EntityDeleted(vectone);
		}

		/// <summary>
		/// Gets all vectonesites
		/// </summary>
		/// <returns>Vectone collection</returns>
        public virtual IList<VectoneSite> GetAllVectones()
		{
			string key = VECTONES_ALL_KEY;
			return _cacheManager.Get(key, () =>
			{
				var query = from s in _vectoneRepository.Table
							orderby s.DisplayOrder, s.Name
							select s;
				var vectonesites = query.ToList();
				return vectonesites;
			});
		}

		/// <summary>
		/// Gets a vectone 
		/// </summary>
		/// <param name="vectoneid">Vectone identifier</param>
		/// <returns>Vectone</returns>
        public virtual VectoneSite GetVectoneById(int vectoneid)
		{
			if (vectoneid == 0)
				return null;

            string key = string.Format(VECTONES_BY_ID_KEY, vectoneid);
            return _cacheManager.Get(key, () => 
            { 
                return _vectoneRepository.GetById(vectoneid); 
            });
		}

		/// <summary>
		/// Inserts a vectone
		/// </summary>
		/// <param name="vectone">Vectone</param>
        public virtual void InsertVectone(VectoneSite vectone)
		{
			if (vectone == null)
				throw new ArgumentNullException("vectone");

			_vectoneRepository.Insert(vectone);

			_cacheManager.RemoveByPattern(VECTONES_PATTERN_KEY);

			//event notification
			_eventPublisher.EntityInserted(vectone);
		}

		/// <summary>
		/// Updates the vectone
		/// </summary>
		/// <param name="vectone">Vectone</param>
        public virtual void UpdateVectone(VectoneSite vectone)
		{
			if (vectone == null)
				throw new ArgumentNullException("vectone");

			_vectoneRepository.Update(vectone);

			_cacheManager.RemoveByPattern(VECTONES_PATTERN_KEY);

			//event notification
			_eventPublisher.EntityUpdated(vectone);
		}

		/// <summary>
		/// True if there's only one vectone. Otherwise False.
		/// </summary>
		public virtual bool IsSingleVectoneMode()
		{
			if (!_isSingleVectoneMode.HasValue)
			{
				var query = from s in _vectoneRepository.Table
							select s;
				_isSingleVectoneMode = query.Count() <= 1;
			}

			return _isSingleVectoneMode.Value;
		}

		#endregion
	}
}