﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vectone.Core;
using Vectone.Core.Caching;
using Vectone.Core.Data;
using Vectone.Core.Domain.Users;
using Vectone.Core.Domain.Security;
using Vectone.Core.Domain.Vectones;
using Vectone.Services.Security;

namespace Vectone.Services.Vectones
{
	/// <summary>
	/// Vectone mapping service interface
	/// </summary>
	public partial interface IVectoneMappingService
	{
		/// <summary>
		/// Deletes a vectone mapping record
		/// </summary>
		/// <param name="vectoneMapping">Vectone mapping record</param>
		void DeleteVectoneMapping(VectoneMapping vectoneMapping);

		/// <summary>
		/// Gets a vectone mapping record
		/// </summary>
		/// <param name="vectoneMappingId">Vectone mapping record identifier</param>
		/// <returns>Vectone mapping record</returns>
		VectoneMapping GetVectoneMappingById(int vectoneMappingId);

		/// <summary>
		/// Gets vectone mapping records
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="entity">Entity</param>
		/// <returns>Vectone mapping records</returns>
		IList<VectoneMapping> GetVectoneMappings<T>(T entity) where T : BaseEntity, IVectoneMappingSupported;

		/// <summary>
		/// Save the vectone napping for an entity
		/// </summary>
		/// <typeparam name="T">Entity type</typeparam>
		/// <param name="entity">The entity</param>
		/// <param name="selectedVectoneIds">Array of selected vectone ids</param>
		void SaveVectoneMappings<T>(T entity, int[] selectedVectoneIds) where T : BaseEntity, IVectoneMappingSupported;

		/// <summary>
		/// Inserts a vectone mapping record
		/// </summary>
		/// <param name="vectoneMapping">Vectone mapping</param>
		void InsertVectoneMapping(VectoneMapping vectoneMapping);

		/// <summary>
		/// Inserts a vectone mapping record
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="vectoneid">Vectone id</param>
		/// <param name="entity">Entity</param>
		void InsertVectoneMapping<T>(T entity, int vectoneid) where T : BaseEntity, IVectoneMappingSupported;

		/// <summary>
		/// Updates the vectone mapping record
		/// </summary>
		/// <param name="vectoneMapping">Vectone mapping</param>
		void UpdateVectoneMapping(VectoneMapping vectoneMapping);

		/// <summary>
		/// Find vectone identifiers with granted access (mapped to the entity)
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="entity">Wntity</param>
		/// <returns>Vectone identifiers</returns>
		int[] GetVectonesIdsWithAccess<T>(T entity) where T : BaseEntity, IVectoneMappingSupported;

		/// <summary>
		/// Authorize whether entity could be accessed in the current vectone (mapped to this vectone)
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="entity">Wntity</param>
		/// <returns>true - authorized; otherwise, false</returns>
		bool Authorize<T>(T entity) where T : BaseEntity, IVectoneMappingSupported;

		/// <summary>
		/// Authorize whether entity could be accessed in a vectone (mapped to this vectone)
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="entity">Entity</param>
		/// <param name="vectoneid">Vectone identifier</param>
		/// <returns>true - authorized; otherwise, false</returns>
		bool Authorize<T>(T entity, int vectoneid) where T : BaseEntity, IVectoneMappingSupported;
	}
}