﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vectone.Core;
using Vectone.Core.Caching;
using Vectone.Core.Data;
using Vectone.Core.Domain.Users;
using Vectone.Core.Domain.Security;
using Vectone.Core.Domain.Vectones;
using Vectone.Services.Security;

namespace Vectone.Services.Vectones
{
	/// <summary>
	/// Vectone mapping service
	/// </summary>
	public partial class VectoneMappingService : IVectoneMappingService
	{
		#region Constants

		private const string VECTONEMAPPING_BY_ENTITYID_NAME_KEY = "Vectone.sitemapping.entityid-name-{0}-{1}";
		private const string VECTONEMAPPING_PATTERN_KEY = "Vectone.sitemapping.";

		#endregion

		#region Fields

		private readonly IRepository<VectoneMapping> _vectoneMappingRepository;
		private readonly IVectoneContext _vectoneContext;
		private readonly IVectoneService _vectoneService;
		private readonly ICacheManager _cacheManager;

		#endregion

		#region Ctor

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="cacheManager">Cache manager</param>
		/// <param name="vectoneContext">Vectone context</param>
		/// <param name="vectoneMappingRepository">Vectone mapping repository</param>
		public VectoneMappingService(ICacheManager cacheManager,
			IVectoneContext vectoneContext,
			IVectoneService vectoneService,
			IRepository<VectoneMapping> vectoneMappingRepository)
		{
			this._cacheManager = cacheManager;
			this._vectoneContext = vectoneContext;
			this._vectoneService = vectoneService;
			this._vectoneMappingRepository = vectoneMappingRepository;

			this.QuerySettings = DbQuerySettings.Default;
		}

		public DbQuerySettings QuerySettings { get; set; }

		#endregion

		#region Methods

		/// <summary>
		/// Deletes a vectone mapping record
		/// </summary>
		/// <param name="vectoneMapping">Vectone mapping record</param>
		public virtual void DeleteVectoneMapping(VectoneMapping vectoneMapping)
		{
			if (vectoneMapping == null)
				throw new ArgumentNullException("vectoneMapping");

			_vectoneMappingRepository.Delete(vectoneMapping);

			//cache
			_cacheManager.RemoveByPattern(VECTONEMAPPING_PATTERN_KEY);
		}

		/// <summary>
		/// Gets a vectone mapping record
		/// </summary>
		/// <param name="vectoneMappingId">Vectone mapping record identifier</param>
		/// <returns>Vectone mapping record</returns>
		public virtual VectoneMapping GetVectoneMappingById(int vectoneMappingId)
		{
			if (vectoneMappingId == 0)
				return null;

			var vectoneMapping = _vectoneMappingRepository.GetById(vectoneMappingId);
			return vectoneMapping;
		}

		/// <summary>
		/// Gets vectone mapping records
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="entity">Entity</param>
		/// <returns>Vectone mapping records</returns>
		public virtual IList<VectoneMapping> GetVectoneMappings<T>(T entity) where T : BaseEntity, IVectoneMappingSupported
		{
			if (entity == null)
				throw new ArgumentNullException("entity");

			int entityId = entity.Id;
			string entityName = typeof(T).Name;

			var query = from vt in _vectoneMappingRepository.Table
						where vt.EntityId == entityId &&
						vt.EntityName == entityName
						select vt;
			var vectoneMappings = query.ToList();
			return vectoneMappings;
		}

		/// <summary>
		/// Save the vectone napping for an entity
		/// </summary>
		/// <typeparam name="T">Entity type</typeparam>
		/// <param name="entity">The entity</param>
		/// <param name="selectedVectoneIds">Array of selected vectone ids</param>
		public virtual void SaveVectoneMappings<T>(T entity, int[] selectedVectoneIds) where T : BaseEntity, IVectoneMappingSupported
		{
			var existingVectoneMappings = GetVectoneMappings(entity);
			var allVectones = _vectoneService.GetAllVectones();

			foreach (var vectone in allVectones)
			{
				if (selectedVectoneIds != null && selectedVectoneIds.Contains(vectone.Id))
				{
					if (existingVectoneMappings.Where(vt => vt.VectoneId == vectone.Id).Count() == 0)
						InsertVectoneMapping(entity, vectone.Id);
				}
				else
				{
					var vectoneMappingToDelete = existingVectoneMappings.Where(vt => vt.VectoneId == vectone.Id).FirstOrDefault();
					if (vectoneMappingToDelete != null)
						DeleteVectoneMapping(vectoneMappingToDelete);
				}
			}
		}

		/// <summary>
		/// Inserts a vectone mapping record
		/// </summary>
		/// <param name="vectoneMapping">Vectone mapping</param>
		public virtual void InsertVectoneMapping(VectoneMapping vectoneMapping)
		{
			if (vectoneMapping == null)
				throw new ArgumentNullException("vectoneMapping");

			_vectoneMappingRepository.Insert(vectoneMapping);

			//cache
			_cacheManager.RemoveByPattern(VECTONEMAPPING_PATTERN_KEY);
		}

		/// <summary>
		/// Inserts a vectone mapping record
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="vectoneid">Vectone id</param>
		/// <param name="entity">Entity</param>
		public virtual void InsertVectoneMapping<T>(T entity, int vectoneid) where T : BaseEntity, IVectoneMappingSupported
		{
			if (entity == null)
				throw new ArgumentNullException("entity");

			if (vectoneid == 0)
				throw new ArgumentOutOfRangeException("vectoneid");

			int entityId = entity.Id;
			string entityName = typeof(T).Name;

			var vectoneMapping = new VectoneMapping()
			{
				EntityId = entityId,
				EntityName = entityName,
				VectoneId = vectoneid
			};

			InsertVectoneMapping(vectoneMapping);
		}

		/// <summary>
		/// Updates the vectone mapping record
		/// </summary>
		/// <param name="vectoneMapping">Vectone mapping</param>
		public virtual void UpdateVectoneMapping(VectoneMapping vectoneMapping)
		{
			if (vectoneMapping == null)
				throw new ArgumentNullException("vectoneMapping");

			_vectoneMappingRepository.Update(vectoneMapping);

			//cache
			_cacheManager.RemoveByPattern(VECTONEMAPPING_PATTERN_KEY);
		}

		/// <summary>
		/// Find vectone identifiers with granted access (mapped to the entity)
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="entity">Wntity</param>
		/// <returns>Vectone identifiers</returns>
		public virtual int[] GetVectonesIdsWithAccess<T>(T entity) where T : BaseEntity, IVectoneMappingSupported
		{
			if (entity == null)
				throw new ArgumentNullException("entity");

			int entityId = entity.Id;
			string entityName = typeof(T).Name;

			string key = string.Format(VECTONEMAPPING_BY_ENTITYID_NAME_KEY, entityId, entityName);
			return _cacheManager.Get(key, () =>
			{
				var query = from vt in _vectoneMappingRepository.Table
							where vt.EntityId == entityId &&
							vt.EntityName == entityName
							select vt.VectoneId;
				var result = query.ToArray();
				//little hack here. nulls aren't cacheable so set it to ""
				if (result == null)
					result = new int[0];
				return result;
			});
		}

		/// <summary>
		/// Authorize whether entity could be accessed in the current vectone (mapped to this vectone)
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="entity">Wntity</param>
		/// <returns>true - authorized; otherwise, false</returns>
		public virtual bool Authorize<T>(T entity) where T : BaseEntity, IVectoneMappingSupported
		{
			return Authorize(entity, _vectoneContext.CurrentVectone.Id);
		}

		/// <summary>
		/// Authorize whether entity could be accessed in a vectone (mapped to this vectone)
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="entity">Entity</param>
		/// <param name="vectone">Vectone</param>
		/// <returns>true - authorized; otherwise, false</returns>
		public virtual bool Authorize<T>(T entity, int vectoneid) where T : BaseEntity, IVectoneMappingSupported
		{
			if (entity == null)
				return false;

			if (vectoneid == 0)
				//return true if no vectone specified/found
				return true;

			if (QuerySettings.IgnoreMultiVectone)
				return true;

			if (!entity.LimitedToVectones)
				return true;

			foreach (var vectoneidWithAccess in GetVectonesIdsWithAccess(entity))
				if (vectoneid == vectoneidWithAccess)
					//yes, we have such permission
					return true;

			//no permission found
			return false;
		}

		#endregion
	}
}