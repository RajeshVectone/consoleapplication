using System;
using System.Linq;
using Vectone.Core;
using Vectone.Core.Domain.Users;
using Vectone.Services.Localization;

using Vectone.Services.Security;

namespace Vectone.Services.Users
{
    /// <summary>
    /// User registration service
    /// </summary>
    public partial class UserRegistrationService : IUserRegistrationService
    {
        #region Fields

        private readonly IUserService _userService;
        private readonly IEncryptionService _encryptionService;
        private readonly UserSettings _userSettings;
        private readonly ILocalizationService _localizationService;


        private readonly IVectoneContext _vectoneContext;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        public UserRegistrationService(IUserService userService,
            IEncryptionService encryptionService,

            ILocalizationService localizationService,
          UserSettings userSettings,
            IVectoneContext vectoneContext)
        {
            this._userService = userService;
            this._encryptionService = encryptionService;
            this._localizationService = localizationService;
            this._userSettings = userSettings;
            this._userService = userService;
            this._vectoneContext = vectoneContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validate user
        /// </summary>
        /// <param name="usernameOrEmail">Username or email</param>
        /// <param name="password">Password</param>
        /// <returns>Result</returns>
        public virtual bool ValidateUser(string usernameOrEmail, string password)
        {
            User user = null;

            user = _userService.GetUserByEmail(usernameOrEmail);

            if (user == null || (user.Deleted != null && user.Deleted.Value) || !user.Active)
            //if (user == null )
                return false;


            //user.PasswordFormat = 0;
            string pwd = "";
            switch (user.PasswordFormat)
            {
                case PasswordFormat.Encrypted:
                    pwd = _encryptionService.EncryptText(password);
                    break;
                case PasswordFormat.Hashed:
                    pwd = _encryptionService.CreatePasswordHash(password, user.PasswordSalt, _userSettings.HashedPasswordFormat);
                    break;
                default:
                    pwd = password;
                    break;
            }
            
            //changed on 17 Apr 2015 - by Hari
            //bool isValid = pwd.ToUpper() == user.Password;
            //bool isValid = pwd.ToUpper() == user.Password.ToUpper();
            bool isValid = pwd == user.Password;
            //end changed


            //save last login date
            if (isValid)
            {
                user.LastLoginDateUtc = DateTime.UtcNow;
                ////added on 27-04-2015 by Hari - set total no of logins
                //if (user.TotalLogins == null)
                //    user.TotalLogins = 1;
                //else
                //    user.TotalLogins = user.TotalLogins + 1;
                ////end added
                _userService.UpdateUser(user);
            }
         

            return isValid;
        }

        /// <summary>
     
        #endregion
    }
}