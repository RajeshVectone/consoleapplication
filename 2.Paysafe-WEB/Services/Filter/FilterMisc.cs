﻿using System.Collections.Generic;
using System.Text;

namespace Vectone.Services.Filter
{
	public class FilterSql
	{
		public List<FilterCriteria> Criteria { get; set; }
		public List<object> Values { get; set; }
		public StringBuilder WhereClause { get; set; }

		public override string ToString()
		{
			if (WhereClause != null && Values != null)
				return "{0}, {1}".FormatWith(WhereClause.ToString(), string.Join(", ", Values.ToArray()));
			return "";
		}
	}


	
}
