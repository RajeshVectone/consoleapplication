﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vectone.Core;
using Vectone.Core.Caching;
using Vectone.Core.Data;
using Vectone.Core.Events;
using Vectone.Services.Localization;
using Vectone.Core.Logging;
using Vectone.Services.Security;
using Vectone.Services.Configuration;

namespace Vectone.Services
{
	
	public interface ICommonServices
	{
		ICacheManager Cache 
		{ 
			get;
		}

		IDbContext DbContext
		{
			get;
		}

		IVectoneContext VectoneContext
		{
			get;
		}

		IWebHelper WebHelper
		{
			get;
		}

		IWorkContext WorkContext
		{
			get;
		}

		IEventPublisher EventPublisher
		{
			get;
		}

		ILocalizationService Localization
		{
			get;
		}

		IUserActivityService UserActivity
		{
			get;
		}

		INotifier Notifier
		{
			get;
		}

		IPermissionService Permissions
		{
			get;
		}

		ISettingService Settings
		{
			get;
		}
	}

}
