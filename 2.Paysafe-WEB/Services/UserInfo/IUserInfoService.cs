﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vectone.Core.Domain.UserInfo;

namespace Vectone.Services.UserInfo
{
    public interface IUserInfoService
    {
        /// <summary>
        /// Gets User Info
        /// </summary>              
        /// <returns>User Info</returns>
        SessionInfoDomain GetSessionInfo(string user_name, string email_id);
    }
}
