﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Vectone.Core;
using Vectone.Core.Caching;
using Vectone.Core.Data;
using Vectone.Core.Domain.Common;

using Vectone.Core.Events;
using Vectone.Core.Localization;
using Vectone.Services.Common;
using Vectone.Core.Domain.Error;
using Vectone.Core.Domain.UserInfo;

namespace Vectone.Services.UserInfo
{
    public class UserInfoService : IUserInfoService
    {
        /// <summary>
        /// Gets Session Info
        /// </summary>              
        /// <returns>User Info</returns>
        
        #region Constants        
        private const string FETCH_RESELLER_LOGIN_DETAIL = "SBA_Fetch_Reseller_Login_Detail_SP";
        #endregion

        

     #region Fields

        private readonly IRepository<SessionInfoDomain> _sessionInfoRepository;
        private readonly IRepository<GenericAttribute> _gaRepository;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="sessionInfoRepository">User Info repository</param>        
        /// <param name="gaRepository">Generic attribute repository</param>
        /// <param name="genericAttributeService">Generic attribute service</param>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="dbContext">Db context</param>
        /// <param name="dataProvider">Data provider</param>
        public UserInfoService(ICacheManager cacheManager,
            IRepository<SessionInfoDomain> sessionInfoRepository,            
            IRepository<GenericAttribute> gaRepository,
            IGenericAttributeService genericAttributeService,
            IEventPublisher eventPublisher, IDbContext dbContext,
            IDataProvider dataProvider
            )
        {
            this._cacheManager = cacheManager;
            this._sessionInfoRepository = sessionInfoRepository;            
            this._gaRepository = gaRepository;
            this._genericAttributeService = genericAttributeService;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._dataProvider = dataProvider;

            T = NullLocalizer.Instance;
        }

        #endregion


        #region Properties

        public Localizer T { get; set; }

        #endregion

        #region Methods

        #region UserInfo

        /// <summary>
        /// Gets User Info
        /// </summary>        
        /// <param name="user_Name">User Name</param>
        /// <param name="email_Id">Email Id</param>        
        /// <returns>List of Available SIM Cards</returns>
        public SessionInfoDomain GetSessionInfo(string user_name, string email_id)
        {         
            var userNameParm = _dataProvider.GetParameter();
            userNameParm.ParameterName = "User_Name";
            userNameParm.Value = user_name;
            userNameParm.DbType = System.Data.DbType.String;

            var emailidParm = _dataProvider.GetParameter();
            emailidParm.ParameterName = "Email_Id";
            emailidParm.Value = email_id;
            emailidParm.DbType = System.Data.DbType.String;

            var sessionInfo = _dbContext.ExecuteStoredProcedureListT<SessionInfoDomain>(FETCH_RESELLER_LOGIN_DETAIL, 300, userNameParm, emailidParm).FirstOrDefault();

            return sessionInfo;
        }
        #endregion
        
        #endregion

    }
}
