﻿
using Vectone.Core.Configuration;

namespace Vectone.Services.Helpers
{
    public class DateTimeSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a default vectone time zone identifier
        /// </summary>
        public string DefaultVectoneTimeZoneId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether users are allowed to select their time zone
        /// </summary>
        public bool AllowUsersToSetTimeZone { get; set; }
    }
}