﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using SOPGClassicMerchantClient.com.paysafecard.soa;
using NLog;

/**
 * SOPGClassicMerchantClient
 * 
 * @package psc-sdk
 * @copyright 2013 Paysafecard.com Wertkarten AG 
 * @version 1.2
 * @since 0.1
 */

namespace Vectone.PSC
{
    public enum DevStatus
    {
        LIVE = 0,
        TEST
    };

    /// <summary>
    /// PSC SOAP Merchant Client Class
    /// </summary>
    public class SOPGClassicMerchantClient
    {
        public delegate void logMessageEventHandler(object sender, Logging.LogMessage logMessage);
        public event logMessageEventHandler logMessageEvent = null;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Contains all transaction values
        /// </summary>
        private struct TransactionVals
        {
            public List<string> RestrictedCountries;
            public string MinAge;
            public string MinKycLevel;
            public string Password;
            public string Username;
            public string Amount;
            public string MerchantClientId;
            public string Currency;
            public string ShopId;
            public string Mtid;
            public string SubId;
            public string OkURL;
            public string NOkURL;
            public string PnURL;
            public string URL;
            public string RedirURL;
            public string ShopLabel;
            public bool Close;
        }

        private TransactionVals m_TransactionValues;
        private Dictionary<string, Dictionary<string, string>> m_ErrorResponses;

        private string[] m_KycLevels = new string[] { "SIMPLE", "FULL" };
        private string m_Language;
        private string m_mId;

        private bool m_DebugStatus;
        private bool m_AutoCorrect = false;

        private Logging.Logger m_Log;

        /// <summary>
        /// Creates a new instance of SOPGClassicMerchantClient
        /// </summary>
        /// <param name="debugStatus">Tells the SDK, if debug messages should be logged</param>
        /// <param name="sysLang">Tells the SDK, in which language log messages should be logged</param>
        /// <param name="autoCorrect">Automatic correction of transaction values</param>
        /// <param name="status">Use PSC live- or test-URL</param>
        public SOPGClassicMerchantClient(bool debugStatus, string sysLang, bool autoCorrect = false, DevStatus status = DevStatus.LIVE)
        {
            m_Log = new Logging.Logger();
            m_Log.logMessageEvent += new Logging.Logger.logMessageEventHandler(m_Log_logMessageEvent);
            m_Log.logDebugMessages = debugStatus;

            m_Language = sysLang.ToUpper();

            m_ErrorResponses = new Dictionary<string, Dictionary<string, string>>();
            m_ErrorResponses.Add("DE", new Dictionary<string, string>());
            m_ErrorResponses.Add("EN", new Dictionary<string, string>());

            if (!m_ErrorResponses.ContainsKey(m_Language))
                throw new ArgumentException("The given system language is not supported");

            m_TransactionValues.RestrictedCountries = new List<string>();

            m_DebugStatus = debugStatus;
            m_AutoCorrect = autoCorrect;

            #region ErrorResponses
            // Language DE
            m_ErrorResponses["DE"].Add("username_empty", "Benutzername ist leer!");
            m_ErrorResponses["DE"].Add("amount_invalid", "Ungültige Betragsangabe. Max. 11 Stellen vor Dezimalpunkt, exakt 2 Stellen nach Dezimalpunkt.");
            m_ErrorResponses["DE"].Add("username_length", "Der Benutzername muss mehr als 3 Zeichen haben.");
            m_ErrorResponses["DE"].Add("password_empty", "Das Passwort ist leer!");
            m_ErrorResponses["DE"].Add("passwor_length", "Das Passwort muss mehr als 5 Zeichen haben.");
            m_ErrorResponses["DE"].Add("invalid_client_id", "Die angegebene Merchant-Client-ID ist ungültig. Die Merchant-Client-ID muss zwischen 1 und 50 Zeichen lang sein. Erlaubte Zeichen: AZ, az, 0-9 sowie - (Bindestrich) und _ (Unterstrich).");
            m_ErrorResponses["DE"].Add("no_auto_correct", "Es wurde keine Auto-Korrektur festgelegt.");
            m_ErrorResponses["DE"].Add("no_sys_lang", "Es wurde keine Systemsprache festgelegt.");
            m_ErrorResponses["DE"].Add("no_debug", "Es wurde kein Debug-Status angegeben.");
            m_ErrorResponses["DE"].Add("shopid_oversize", "ShopId ist ungültig. Die ShopId darf nicht mehr als 60 Zeichen haben.");
            m_ErrorResponses["DE"].Add("shopid_undersize", "ShopId ist ungültig. Die ShopId muss mehr als 20 Zeichen haben.");
            m_ErrorResponses["DE"].Add("shopid_invalid", "Die Shop-ID ist ungültig. Erlaubte Zeichen A-Z, a-z, 0-9 und – (Bindestrich) und _ (Unterstrich).");
            m_ErrorResponses["DE"].Add("shoplabel_oversize", "Shoplabel ist ungültig. Shoplabel darf nicht mehr als 60 Zeichen haben.");
            m_ErrorResponses["DE"].Add("shoplabel_undersize", "Shoplabel ist ungültig. Shoplabel muss mehr als 1 Zeichen haben.");
            m_ErrorResponses["DE"].Add("shoplabel_invalid", "Shoplabel ist ungültig. Erlaubte Zeichen A-Z, a-z, 0-9 und – (Bindestrich) und _ (Unterstrich).");
            m_ErrorResponses["DE"].Add("subid_oversize", "subID ist ungültig. subID darf nicht mehr als 8 Zeichen haben.");
            m_ErrorResponses["DE"].Add("mtid_oversize", "Mtid ist ungültig. Mtid darf nicht mehr als 60 Zeichen haben.");
            m_ErrorResponses["DE"].Add("mtid_undersize", "Mtid ist ungültig. Mtid muss mehr als 20 Zeichen haben.");
            m_ErrorResponses["DE"].Add("mtid_invalid", "Mtid ist ungültig. Erlaubte Zeichen A-Z, a-z, 0-9 und – (Bindestrich) und _ (Unterstrich).");
            m_ErrorResponses["DE"].Add("error_validation_value", "Fehler bei der Validierung. Es wurde kein Wert zur Validierung übergeben!");
            m_ErrorResponses["DE"].Add("error_validation_type", "Fehler bei der Validierung. Es wurde kein gültiger Typ zur Validierung angegeben!");
            m_ErrorResponses["DE"].Add("error_validation", "Fehler bei der Validierung.");
            m_ErrorResponses["DE"].Add("min_age_invalide", "Ungültige Altersbeschränkung. Das Alter muss ein positiver Zahlenwert sein.");
            m_ErrorResponses["DE"].Add("min_kyc_level_invalide", "Ungültige Einschränkung. Das Level muss SIMPLE, DOCUMENT oder POSTIDENT sein.");
            m_ErrorResponses["DE"].Add("restricted_country_invalide", "Invalid restricted country. 2 characters required. The value accepts ISO 3166-1 country codes.");
            m_ErrorResponses["DE"].Add("restricted_country_case", "Ungültige Ländereinschränkung. Es sind nur Großbuchstaben erlaubt. Der Wert akzeptiert ISO 3166-1 Ländercodes.");
            m_ErrorResponses["DE"].Add("invalide_status", "Ungültiger Status. Der Status kann \"test\" oder \"live\" sein.");
            m_ErrorResponses["DE"].Add("no_status", "Es wurde keine Status angegeben.");
            m_ErrorResponses["DE"].Add("create_disp_is_error", "createDisposition wurde abgebrochen. Bitte erst alle Fehler beseitigen.");
            m_ErrorResponses["DE"].Add("execute_debit_is_error", "executeDebit wurde abgebrochen. Bitte erst alle Fehler beseitigen.");
            m_ErrorResponses["DE"].Add("wrong_currency", "Ungültige Währung. Die Währung muss 3 Zeichen lange sein (ISO 4217).");
            m_ErrorResponses["DE"].Add("wrong_currency_case", "Ungültige Währung. Die Währung darf nur in Großbuchstaben angegeben werden.");
            m_ErrorResponses["DE"].Add("dot_amount", "Der Betrag muss mit einem Punkt getrennt werden.");
            m_ErrorResponses["DE"].Add("invalid_nok_url", "Die angegebene nok-URL ist ungültig!");
            m_ErrorResponses["DE"].Add("invalid_ok_url", "Die angegebene ok-URL ist ungültig!");
            m_ErrorResponses["DE"].Add("invalid_pn_url", "Die angegebene pn-URL ist ungültig!");
            m_ErrorResponses["DE"].Add("merchantid_invalid", "Die merchantClientId ist ungültig. Erlaubte Zeichen A-Z, a-z, 0-9.");
            m_ErrorResponses["DE"].Add("merchantid_oversize", "merchantClientId ist ungültig. merchantClientId darf nicht mehr als 50 Zeichen haben.");
            m_ErrorResponses["DE"].Add("merchantid_undersize", "merchantClientId is invalid. merchantClientId muss mindestens 1 Zeichen haben.");
            m_ErrorResponses["DE"].Add("auto_correct_set_pn_url", "Die angegebene pn-URL wurde mit AutoCorrect berichtigt. Bitte Eingabe überarbeiten!");
            m_ErrorResponses["DE"].Add("auto_correct_set_nok_url", "Die angegebene nok-URL wurde mit AutoCorrect berichtigt. Bitte Eingabe überarbeiten!");
            m_ErrorResponses["DE"].Add("auto_correct_set_ok_url", "Die angegebene ok-URL wurde mit AutoCorrect berichtigt. Bitte Eingabe überarbeiten!");
            m_ErrorResponses["DE"].Add("auto_correct_res_country", "Die Eingabe zu den Ländereinschränkungen wurden mit AutoCorrect berichtig. Bitte Eingabe überarbeiten!");
            m_ErrorResponses["DE"].Add("auto_correct_ammount_warning", "Der angegebene Betrag wurde mit AutoCorrect berichtigt. Der angegebene Betrag entspricht nicht der vorgeschriebenen Formatierung!");
            m_ErrorResponses["DE"].Add("get_serial_num_is_error", "getSerialNumbers wurde abgebrochen. Bitte erst alle Fehler beseitigen.");
            m_ErrorResponses["DE"].Add("error_get_serial_num", "getSerialNumbers konnte nicht erfolgreich ausgeführt werden!");
            m_ErrorResponses["DE"].Add("error_create_disposition", "createDisposition konnte nicht erfolgreich ausgeführt werden!");

            // Customer messages
            m_ErrorResponses["DE"].Add("msg_create_disposition", "Bei der Bezahlung ist ein Fehler aufgetreten. Vermutlich ist dies ein temporärer Fehler und die Bezahlung kann durch Neu-Laden der Seite abgeschlossen werden. Falls dieses Problem weiterhin besteht, kontaktieren Sie bitte unseren Support.");
            m_ErrorResponses["DE"].Add("msg_execute_debit", "Die Zahlung konnte nicht abgeschlossen werden. Es besteht ein temporäres Verbindungsproblem. Bitte drücken Sie den „reload“ Botton im Browser oder den folgenden Link um die Zahlung abzuschließen. <<neu laden>> Falls dieses Problem weiterhin besteht wenden Sie sich bitte an den Support Sie können auf der paysafecard Guthabenübersicht (https://customer.cc.at.paysafecard.com/psccustomer/GetWelcomePanelServlet?language=DE) Erfahren wann der reservierte Betrag wieder freigegeben wird.");
            m_ErrorResponses["DE"].Add("payment_invalide", "Der Bezahlvorgang wurde nicht ordnungsgemäß abgeschlossen");
            m_ErrorResponses["DE"].Add("payment_cancelled", "Der Bezahlvorgang wurde auf Ihren Wunsch abgebrochen");
            m_ErrorResponses["DE"].Add("payment_expired", "Das Zeitfenster ist abgelaufen. Bitte starten Sie den Bezahlvorgang erneut.");
            m_ErrorResponses["DE"].Add("payment_unknown_error", "Unbekannter Fehler bei der Zahlung. Bitte starten Sie den Bezahlvorgang erneut. Sollte der Fehler weiter auftreten, wenden Sie sich bitte an unseren Support");
            m_ErrorResponses["DE"].Add("payment_done", "Der Zahlvorgang wurde erfolgreich abgeschlossen.");

            // Language EN
            m_ErrorResponses["EN"].Add("amount_invalid", "Incorrect syntax in disposition amount (max. 11 digit before the decimal point – exactly 2 digit after the decimal point).");
            m_ErrorResponses["EN"].Add("username_empty", "Username is empty!");
            m_ErrorResponses["EN"].Add("username_length", "The username must have more than 3 characters.");
            m_ErrorResponses["EN"].Add("password_empty", "Password is empty!");
            m_ErrorResponses["EN"].Add("passwor_length", "The password must have more than 5 characters.");
            m_ErrorResponses["EN"].Add("invalid_client_id", "The specified Merchant-Client-ID is invalid. The Merchant-Client-ID must be between 1 and 20 characters. Only the following is allowed A-Z, a-z, 0-9 as well as – (hypen) and _ (underline).");
            m_ErrorResponses["EN"].Add("no_auto_correct", "No auto-correct specified.");
            m_ErrorResponses["EN"].Add("no_sys_lang", "No system language specified.");
            m_ErrorResponses["EN"].Add("no_debug", "No debug-status specified.");
            m_ErrorResponses["EN"].Add("shopid_oversize", "ShopID is invalid. ShopID maximum length is 60 characters.");
            m_ErrorResponses["EN"].Add("shopid_undersize", "ShopID is invalid. ShopID must have at least 20 characters.");
            m_ErrorResponses["EN"].Add("merchantid_oversize", "merchantClientId is invalid. merchantClientId maximum length is 50 characters.");
            m_ErrorResponses["EN"].Add("merchantid_undersize", "merchantClientId is invalid. merchantClientId must have at least 1 character.");
            m_ErrorResponses["EN"].Add("shopid_invalid", "ShopID is invalid. Only the following is allowed A-Z, a-z, 0-9 as well as – (hypen) and _ (underline).");
            m_ErrorResponses["EN"].Add("merchantid_invalid", "Incorrect merchantClientId. Allowed characters A-Z, a-z, 0-9.");
            m_ErrorResponses["EN"].Add("shoplabel_oversize", "Shoplabel is invalid. Shoplabel maximum length is 60 characters.");
            m_ErrorResponses["EN"].Add("shoplabel_undersize", "Shoplabel is invalid. Shoplabel must have at least 1 characters.");
            m_ErrorResponses["EN"].Add("shoplabel_invalid", "Shoplabel is invalid. Only the following is allowed A-Z, a-z, 0-9 as well as – (hypen), _ (underline) and spaces.");
            m_ErrorResponses["EN"].Add("subid_oversize", "subID is invalid. subID maximum length is 8 characters.");
            m_ErrorResponses["EN"].Add("mtid_oversize", "Mtid is invalid. Mtid maximum length is 60 characters.");
            m_ErrorResponses["EN"].Add("mtid_undersize", "Mtid is invalid. Mtid must have at least 1 characters.");
            m_ErrorResponses["EN"].Add("mtid_invalid", "Mtid is invalid. Only the following is allowed A-Z, a-z, 0-9 as well as – (hypen), _ (underline) and spaces.");
            m_ErrorResponses["EN"].Add("error_validation_value", "Validation errors. There was no value is passed to the validation!");
            m_ErrorResponses["EN"].Add("error_validation_type", "Validation errors. It was not specified a valid type for the validation!");
            m_ErrorResponses["EN"].Add("error_validation", "Validation errors.");
            m_ErrorResponses["EN"].Add("min_age_invalide", "Invalid restricted age. The age must be a positive numbervalue.");
            m_ErrorResponses["EN"].Add("min_kyc_level_invalide", "Invalid restricted level. The level must be SIMPLE, DOCUMENT or POSTIDENT.");
            m_ErrorResponses["EN"].Add("restricted_country_invalide", "Invalid restricted country. 2 characters required. The value accepts ISO 3166-1 country codes.");
            m_ErrorResponses["EN"].Add("restricted_country_case", "Invalid restricted country. There are only capital letters allowed. The value accepts ISO 3166-1 country codes.");
            m_ErrorResponses["EN"].Add("invalide_status", "Invalid module status. Status can only be \"live\" or \"test\".");
            m_ErrorResponses["EN"].Add("no_status", "It was not specified a status.");
            m_ErrorResponses["EN"].Add("create_disp_is_error", "create disposition was aborted. Please remove all errors.");
            m_ErrorResponses["EN"].Add("execute_debit_is_error", "executeDebit was aborted. Please remove all errors.");
            m_ErrorResponses["EN"].Add("wrong_currency", "Invalid currency. The currency must be 3 characters long (ISO 4217).");
            m_ErrorResponses["EN"].Add("wrong_currency_case", "Invalid currency. The currency may only be specified in uppercase.");
            m_ErrorResponses["EN"].Add("dot_amount", "The amount must be separated with a dot.");
            m_ErrorResponses["EN"].Add("invalid_nok_url", "Specified nok-URL is invalid!");
            m_ErrorResponses["EN"].Add("invalid_ok_url", "Specified ok-URL is invalid!");
            m_ErrorResponses["EN"].Add("invalid_pn_url", "Specified pn-URL is invalid!");
            m_ErrorResponses["EN"].Add("auto_correct_set_pn_url", "Specified pn-URL was corrected with AutoCorrect. Please revise entry!");
            m_ErrorResponses["EN"].Add("auto_correct_set_nok_url", "Specified nok-URL was corrected with AutoCorrect. Please revise entry!");
            m_ErrorResponses["EN"].Add("auto_correct_set_ok_url", "Specified ok-URL was corrected with AutoCorrect. Please revise entry!");
            m_ErrorResponses["EN"].Add("auto_correct_res_country", "Country restrictions entry was corrected with AutoCorrect. Please revise entry!");
            m_ErrorResponses["EN"].Add("auto_correct_ammount_warning", "Specified amount was corrected with AutoCorrect. The specified entry does not have the required formatting!");
            m_ErrorResponses["EN"].Add("get_serial_num_is_error", "getSerialNumbers was aborted. Resolve all errors before proceeding.");
            m_ErrorResponses["EN"].Add("error_get_serial_num", "getSerialNumbers was not executed successfully!");
            m_ErrorResponses["EN"].Add("error_create_disposition", "createDisposition was not executed successfully!");

            // Customer messages
            m_ErrorResponses["EN"].Add("msg_create_disposition", "Transaction could not be initiated due to connection problems. If the problem persists, please contact our support.");
            m_ErrorResponses["EN"].Add("msg_execute_debit", "Payment could not be completed. There is a temporary connection problem. Please press the 'reload' button in your browser or click the following link to complete payment. If this issue persists, please contact Support On the paysafecard credit overview (https://customer.cc.at.paysafecard.com/psccustomer/GetWelcomePanelServlet?language=de) find out when the reserved amount is released again.");
            m_ErrorResponses["EN"].Add("payment_invalide", "Failed to complete the payment transaction properly");
            m_ErrorResponses["EN"].Add("payment_cancelled", "Payment transaction was aborted at your request");
            m_ErrorResponses["EN"].Add("payment_expired", "Timeout. Please restart the payment transaction.");
            m_ErrorResponses["EN"].Add("payment_unknown_error", "Unknown error during payment. Please restart the payment transaction. If this issue persists, please contact Support");
            m_ErrorResponses["EN"].Add("payment_done", "Payment transaction was completed successfully.");
            #endregion

            if (!autoCorrect) m_Log.addLog(m_ErrorResponses[m_Language]["no_auto_correct"], "__construct", "", "", Logging.LogType.WARNING);
            if (sysLang == "") m_Log.addLog(m_ErrorResponses[m_Language]["no_sys_lang"], "__construct", "", "", Logging.LogType.WARNING);
            if (!debugStatus) m_Log.addLog(m_ErrorResponses[m_Language]["no_debug"], "__construct", "", "", Logging.LogType.WARNING);
        }

        /// <summary>
        /// This method is called from Logging.Logger if a new log message was generated
        /// </summary>
        /// <param name="sender">Instance of the sender object</param>
        /// <param name="logMessage">Logparameters</param>
        void m_Log_logMessageEvent(object sender, Logging.LogMessage logMessage)
        {
            if (logMessageEvent != null)
                logMessageEvent(this, logMessage);
        }

        #region Setter-Methods

        /// <summary>
        /// Enable/disable automatic syntax correction of transaction values
        /// </summary>
        public bool AutoCorrect
        {
            get { return m_AutoCorrect; }
            set { m_AutoCorrect = value; }
        }

        /// <summary>
        /// Set and validate customer data
        /// </summary>
        /// <param name="amount">Transaction amount</param>
        /// <param name="currency">Currency</param>
        /// <param name="mtID">Valid mtID</param>
        /// <param name="merchantClientID">Valid merchantClientID</param>
        public void SetCustomer(string amount, string currency, string mtID, string merchantClientID)
        {
            CheckValidationReturn(ValidateAmount(amount), "Amount", amount, Logging.LogType.ERROR);
            m_TransactionValues.Amount = amount;

            CheckValidationReturn(ValidateCurrency(currency), "Currency", currency, Logging.LogType.ERROR);
            m_TransactionValues.Currency = currency;

            CheckValidationReturn(ValidateMtId(mtID), "MtId", mtID, Logging.LogType.ERROR);
            m_TransactionValues.Mtid = mtID;

            CheckValidationReturn(ValidateMerchantClientID(merchantClientID), "MerchantClientID", merchantClientID, Logging.LogType.ERROR);
            m_TransactionValues.MerchantClientId = merchantClientID;
        }

        /// <summary>
        /// Set and validate a password
        /// </summary>
        /// <param name="password">Valid password</param>
        public void SetPassword(string password)
        {
            CheckValidationReturn(ValidatePassword(password), "Password", password, Logging.LogType.ERROR);
            m_TransactionValues.Password = password;
        }

        /// <summary>
        /// Set and validate a user name
        /// </summary>
        /// <param name="username">Valid user name</param>
        public void SetUsername(string username)
        {
            CheckValidationReturn(ValidateUsername(username), "Username", username, Logging.LogType.ERROR);
            m_TransactionValues.Username = username;
        }

        /// <summary>
        /// Set and validate an amount
        /// </summary>
        /// <param name="amount">Valid amount</param>
        public void SetAmount(string amount)
        {
            CheckValidationReturn(ValidateAmount(amount), "Amount", amount, Logging.LogType.ERROR);
            m_TransactionValues.Amount = amount;
        }

        /// <summary>
        /// Set and validate a MerchantClient-ID
        /// </summary>
        /// <param name="merchantClientID">Valid MerchantClient-ID</param>
        public void SetMerchantClientID(string merchantClientID)
        {
            CheckValidationReturn(ValidateMerchantClientID(merchantClientID), "MerchantClientID", merchantClientID, Logging.LogType.ERROR);
            m_TransactionValues.MerchantClientId = merchantClientID;
        }

        /// <summary>
        /// Set and validate a currency
        /// </summary>
        /// <param name="currency">Valid currency</param>
        public void SetCurrency(string currency)
        {
            CheckValidationReturn(ValidateCurrency(currency), "Currency", currency, Logging.LogType.ERROR);
            m_TransactionValues.Currency = currency;
        }

        /// <summary>
        /// Set and validate a shop ID
        /// </summary>
        /// <param name="shopID">Valid Shop-ID</param>
        public void SetShopID(string shopID)
        {
            CheckValidationReturn(ValidateShopId(shopID), "ShopId", shopID, Logging.LogType.ERROR);
            m_TransactionValues.ShopId = shopID;
        }

        /// <summary>
        /// Set and validate a Mt-ID
        /// </summary>
        /// <param name="mtID">Valid Mt-ID</param>
        public void SetMtID(string mtID)
        {
            CheckValidationReturn(ValidateMtId(mtID), "MtId", mtID, Logging.LogType.ERROR);
            m_TransactionValues.Mtid = mtID;
        }

        /// <summary>
        /// Set and validate a Sub-ID
        /// </summary>
        /// <param name="subID">Valid Sub-ID</param>
        public void SetSubID(string subID)
        {
            CheckValidationReturn(ValidateSubId(subID), "SubId", subID, Logging.LogType.ERROR);
            m_TransactionValues.SubId = subID;
        }

        /// <summary>
        /// Set and validate a OK-URL
        /// </summary>
        /// <param name="okURL">Valid OK-URL</param>
        public void SetOkURL(string okURL)
        {
            CheckValidationReturn(ValidateOkUrl(okURL), "OkUrl", okURL, Logging.LogType.ERROR);
            m_TransactionValues.OkURL = okURL;
        }

        /// <summary>
        /// Set and validate a nOK-URL
        /// </summary>
        /// <param name="nOkURL">Valid nOK-URL</param>
        public void SetnOKURL(string nOkURL)
        {
            CheckValidationReturn(ValidateNokUrl(nOkURL), "NokUrl", nOkURL, Logging.LogType.ERROR);
            m_TransactionValues.NOkURL = nOkURL;
        }

        /// <summary>
        /// Set and validate a pn-URL
        /// </summary>
        /// <param name="pnURL">Valid pn-URL</param>
        public void SetPnURL(string pnURL)
        {
            CheckValidationReturn(ValidatePnUrl(pnURL), "PnUrl", pnURL, Logging.LogType.ERROR);
            m_TransactionValues.PnURL = pnURL;
        }

        /// <summary>
        /// Set and validate a shop label
        /// </summary>
        /// <param name="shopLabel">Valid shop label</param>
        public void SetShopLabel(string shopLabel)
        {
            CheckValidationReturn(ValidateShopLabel(shopLabel), "ShopLabel", shopLabel, Logging.LogType.ERROR);
            m_TransactionValues.ShopLabel = shopLabel;
        }

        /// <summary>
        /// Set and validate a country restriction. This method can be called several times. At each call
        /// a new country restriction will be added to the internal collection
        /// </summary>
        /// <param name="country">Valid country restriction</param>
        public void SetRestrictedCountry(string country)
        {
            CheckValidationReturn(ValidateRestrictedCountry(country), "RestrictedCountry", country, Logging.LogType.ERROR);

            if (!m_TransactionValues.RestrictedCountries.Contains(country))
                m_TransactionValues.RestrictedCountries.Add(country);
        }

        /// <summary>
        /// Set and validate a Minimum-Kyc-Level
        /// </summary>
        /// <param name="minKycLevel">Valid Minimum-Kyc-Level</param>
        public void SetMinKycLevel(string minKycLevel)
        {
            CheckValidationReturn(ValidateMinKycLevel(minKycLevel), "MinKycLevel", minKycLevel.ToString(), Logging.LogType.ERROR);
            m_TransactionValues.MinKycLevel = minKycLevel;
        }

        /// <summary>
        /// Set and validate a minimum age
        /// </summary>
        /// <param name="minAge">Valid minimum age</param>
        public void SetMinAge(string minAge)
        {
            CheckValidationReturn(ValidateMinAge(minAge), "MinAge", minAge, Logging.LogType.ERROR);
            m_TransactionValues.MinAge = minAge;
        }

        /// <summary>
        /// Sets an indication of whether the current transaction is the last transaction
        /// </summary>
        /// <param name="Close">Valid Close-parameter</param>
        public void SetClose(bool Close)
        {
            m_TransactionValues.Close = Close;
        }

        /// <summary>
        /// 'LIVE'-mode transmits each SOAP-Request to PSC live URL, 'TEST'-mode to PSC test URL
        /// </summary>
        /// <param name="status">LIVE / TEST</param>
        public void SetStatus(string status)
        {
            if (status == "LIVE")
            {
                m_TransactionValues.URL = "https://soa.paysafecard.com/psc/services/PscService?wsdl";
                m_TransactionValues.RedirURL = "https://customer.cc.at.paysafecard.com/psccustomer/GetCustomerPanelServlet";
                return;
            }

            m_TransactionValues.URL = "https://soatest.paysafecard.com/psc/services/PscService?wsdl";
            m_TransactionValues.RedirURL = "https://customer.test.at.paysafecard.com/psccustomer/GetCustomerPanelServlet";
        }
        #endregion

        private void CheckValidationReturn(KeyValuePair<bool, string> validationRet, string caller, string value, Logging.LogType logType)
        {
            if (!validationRet.Key)
            {
                m_Log.addLog(validationRet.Value, "validate_" + caller, value, "0");
                throw new ArgumentException(validationRet.Value);
            }

            m_Log.addDebug("set" + caller, value);
        }

        /// <summary>
        /// SOAP createDisposition
        /// </summary>
        /// <returns>createDispositionReturn-Object</returns>
        public CreateDispositionReturn CreateDisposition()
        {
            DispositionRestriction[] dispositionRestrictions = new DispositionRestriction[m_TransactionValues.RestrictedCountries.Count + 2];
            CreateDispositionReturn createDispoRtrn = new CreateDispositionReturn();
            Dictionary<string, string> retVals = new Dictionary<string, string>();

            if (m_Log.getLogMessages(Logging.LogType.ERROR).Count > 0)
            {
                m_Log.deleteLogMessages();

                m_Log.addLog(m_ErrorResponses[m_Language]["create_disp_is_error"], "createDisposition", "Error-Log",
                        m_Log.getLogMessages(Logging.LogType.ERROR).Count.ToString());

                m_Log.addLog(m_ErrorResponses[m_Language]["msg_create_disposition"], "", null, "", Logging.LogType.INFO);
                m_Log.addDebug("createDisposition", "Canceled");

                throw new InvalidOperationException(m_ErrorResponses[m_Language]["create_disp_is_error"]);
            }

            global::SOPGClassicMerchantClient.com.paysafecard.soa.DispositionRestriction minAge =
                new global::SOPGClassicMerchantClient.com.paysafecard.soa.DispositionRestriction();

            global::SOPGClassicMerchantClient.com.paysafecard.soa.DispositionRestriction minKycLevel =
                new global::SOPGClassicMerchantClient.com.paysafecard.soa.DispositionRestriction();

            global::SOPGClassicMerchantClient.com.paysafecard.soa.DispositionRestriction[] restrictedCountries =
                new global::SOPGClassicMerchantClient.com.paysafecard.soa.DispositionRestriction[m_TransactionValues.RestrictedCountries.Count];

            minAge.key = "MIN_AGE";
            minAge.value = m_TransactionValues.MinAge;

            minKycLevel.key = "MIN_KYC_LEVEL";
            minKycLevel.value = m_TransactionValues.MinKycLevel;

            for (short i = 0; i < m_TransactionValues.RestrictedCountries.Count; i++)
            {
                dispositionRestrictions[i] = new DispositionRestriction();
                dispositionRestrictions[i].key = "COUNTRY";
                dispositionRestrictions[i].value = m_TransactionValues.RestrictedCountries[i];
            }

            dispositionRestrictions[m_TransactionValues.RestrictedCountries.Count] = minAge;
            dispositionRestrictions[m_TransactionValues.RestrictedCountries.Count + 1] = minKycLevel;

            System.Globalization.NumberFormatInfo provider = new System.Globalization.NumberFormatInfo();
            provider.CurrencyDecimalDigits = 2;

            PscService srvc = new PscService();
            createDispoRtrn = srvc.createDisposition(m_TransactionValues.Username,
                                    m_TransactionValues.Password,
                                    m_TransactionValues.Mtid,
                                    new string[] { m_TransactionValues.SubId },
                                    Convert.ToDouble(m_TransactionValues.Amount, provider),
                                    m_TransactionValues.Currency,
                                    m_TransactionValues.OkURL,
                                    m_TransactionValues.NOkURL,
                                    m_TransactionValues.MerchantClientId,
                                    m_TransactionValues.PnURL,
                                    "",
                                    dispositionRestrictions,
                                    null,
                                    m_TransactionValues.ShopId,
                                    m_TransactionValues.ShopLabel,
                                    null,
                                    false,
                                    false
                                );

            // Save the mID to generate a redirect-URL later:
            m_mId = createDispoRtrn.mid;

            if (createDispoRtrn.resultCode == 0 && createDispoRtrn.errorCode == 0)
            {
                m_Log.addDebug("createDisposition_mid", createDispoRtrn.mid);
                return createDispoRtrn;
            }

            string createDispositionParams =
                "Username: >>empty<< \r\n" +
                "Password: >>empty<< \r\n" +
                "mid: " + createDispoRtrn.mid + "\r\n" +
                "mtid: " + createDispoRtrn.mtid + "\r\n" +
                "subId: " + createDispoRtrn.subId + "\r\n";

            m_Log.addDebug("createDisposition_mid", "0");
            m_Log.addLog(m_ErrorResponses[m_Language]["error_create_disposition"],
                "createDisposition",
                createDispositionParams,
                "ResultCode: " + createDispoRtrn.resultCode +
                " - ErrorCode: " + createDispoRtrn.errorCode
                );

            m_Log.addDebug("createDisposition_params", createDispositionParams);
            m_Log.addDebug("createDisposition_response", "ResultCode: " + createDispoRtrn.resultCode +
                " - ErrorCode: " + createDispoRtrn.errorCode);

            m_Log.deleteLogMessages();
            m_Log.addLog(m_ErrorResponses[m_Language]["msg_create_disposition"],
                "", "", "", Logging.LogType.INFO);

            throw new Exception(m_ErrorResponses[m_Language]["error_create_disposition"]);
        }

        /// <summary>
        /// Generate the Redirect-URL
        /// </summary>
        /// <returns></returns>
        public string GetCustomerPanelURL()
        {
            System.Globalization.NumberFormatInfo provider = new System.Globalization.NumberFormatInfo();
            provider.NumberDecimalDigits = 2;
            provider.NumberDecimalSeparator = ".";

            string Url = m_TransactionValues.RedirURL +
                "?mid=" + m_mId +
                "&mtid=" + m_TransactionValues.Mtid +
                "&amount=" + m_TransactionValues.Amount +
                "&currency=" + m_TransactionValues.Currency +
                "&language=" + m_Language;

            m_Log.addDebug("getCustomerPanel_generated_url", Url);
            return Url;
        }

        /// <summary>
        /// SOAP getSerialNumbers
        /// </summary>
        /// <returns>getSerialNumbersReturn-Object</returns>
        public GetSerialNumbersReturn GetSerialNumbers()
        {
            Log.Info("GetSerialNumbers()");

            if (m_Log.getLogMessages(Logging.LogType.ERROR).Count > 0)
            {
                m_Log.deleteLogMessages();

                m_Log.addLog(m_ErrorResponses[m_Language]["get_serial_num_is_error"], "getSerialNumbers", "Error-Log",
                        m_Log.getLogMessages(Logging.LogType.ERROR).Count.ToString());

                m_Log.addLog(m_ErrorResponses[m_Language]["payment_unknown_error"], "", "", "", Logging.LogType.INFO);
                m_Log.addDebug("createDisposition", "Canceled");

                throw new InvalidOperationException(m_ErrorResponses[m_Language]["get_serial_num_is_error"]);
            }

            PscService srvc = new PscService();
            Log.Info("srvc.Url : " + srvc.Url);
            GetSerialNumbersReturn rtrn = srvc.getSerialNumbers(m_TransactionValues.Username,
                                            m_TransactionValues.Password,
                                            m_TransactionValues.Mtid,
                                            new string[] { m_TransactionValues.SubId },
                                            m_TransactionValues.Currency,
                                            null);

            string getSerialNumbersParams =
                "Username: >>empty<< \r\n" +
                "Password: >>empty<< \r\n" +
                "dispositionState: " + rtrn.dispositionState + "\r\n" +
                "customerDetails: " + rtrn.customerDetails + "\r\n" +
                "currency: " + rtrn.currency + "\r\n" +
                "amount: " + rtrn.amount + "\r\n" +
                "serialNumbers: " + rtrn.serialNumbers + "\r\n" +
                "mtid: " + rtrn.mtid + "\r\n" +
                "subId: " + rtrn.subId + "\r\n";

            Log.Info("getSerialNumbersParams : " + getSerialNumbersParams);
            #region Commented
            //if (rtrn.resultCode == 0 && rtrn.errorCode == 0)
            //{
            //    m_Log.addDebug("getSerialNumbers_errorCode", "0,0");
            //    m_Log.addDebug("getSerialNumbers_dispositionState", rtrn.dispositionState);

            //    if (rtrn.dispositionState == "O" || rtrn.dispositionState == "S" || rtrn.dispositionState == "E")
            //        return rtrn;

            //    m_Log.deleteLogMessages();

            //    if (rtrn.dispositionState == "R")
            //    {
            //        m_Log.addLog(m_ErrorResponses[m_Language]["payment_invalide"], "", "", "", Logging.LogType.INFO);
            //        m_Log.addLog(m_ErrorResponses[m_Language]["payment_invalide"], "getSerialNumbers",
            //            getSerialNumbersParams, "R");

            //        throw new Exception(m_ErrorResponses[m_Language]["payment_invalide"]);
            //    }

            //    else if (rtrn.dispositionState == "L")
            //    {
            //        m_Log.addLog(m_ErrorResponses[m_Language]["payment_cancelled"], "", "", "", Logging.LogType.INFO);
            //        m_Log.addLog(m_ErrorResponses[m_Language]["payment_cancelled"], "getSerialNumbers",
            //            getSerialNumbersParams, "L");

            //        throw new Exception(m_ErrorResponses[m_Language]["payment_cancelled"]);
            //    }

            //    else if (rtrn.dispositionState == "X")
            //    {
            //        m_Log.addLog(m_ErrorResponses[m_Language]["payment_expired"], "", "", "", Logging.LogType.INFO);
            //        m_Log.addLog(m_ErrorResponses[m_Language]["payment_expired"], "getSerialNumbers",
            //            getSerialNumbersParams, "X");

            //        throw new Exception(m_ErrorResponses[m_Language]["payment_expired"]);
            //    }

            //    else
            //    {
            //        m_Log.addLog(m_ErrorResponses[m_Language]["msg_execute_debit"], "", "", "", Logging.LogType.INFO);
            //        m_Log.addLog(m_ErrorResponses[m_Language]["msg_execute_debit"], "getSerialNumbers",
            //            getSerialNumbersParams, "ERROR");

            //        throw new Exception(m_ErrorResponses[m_Language]["payment_unknown_error"]);
            //    }
            //}

            //m_Log.deleteLogMessages();

            //// If an error occured (resultCode & errorCode != 0):
            //m_Log.addDebug("getSerialNumbers", "ResultCode: " + rtrn.resultCode +
            //    " - ErrorCode: " + rtrn.errorCode);

            //m_Log.addLog(m_ErrorResponses[m_Language]["error_get_serial_num"], "getSerialNumbers",
            //    getSerialNumbersParams, "ResultCode: " + rtrn.resultCode + " - ErrorCode: " + rtrn.errorCode);

            //m_Log.addLog(m_ErrorResponses[m_Language]["msg_execute_debit"], "", "", "", Logging.LogType.INFO);
            //m_Log.addDebug("getSerialNumbers_params", getSerialNumbersParams);
            //m_Log.addDebug("getSerialNumbers_response", "ResultCode: " + rtrn.resultCode + " - ErrorCode: " + rtrn.errorCode);

            //throw new Exception(m_ErrorResponses[m_Language]["error_get_serial_num"]); 
            #endregion
            return rtrn;
        }

        /// <summary>
        /// SOAP executeDebit
        /// </summary>
        /// <returns>ExecuteDebitReturn-Object</returns>
        public ExecuteDebitReturn ExecuteDebit()
        {
            if (m_Log.getLogMessages(Logging.LogType.ERROR).Count > 0)
            {
                m_Log.deleteLogMessages();

                m_Log.addLog(m_ErrorResponses[m_Language]["execute_debit_is_error"], "executeDebit", "Error-Log",
                        m_Log.getLogMessages(Logging.LogType.ERROR).Count.ToString());

                m_Log.addLog(m_ErrorResponses[m_Language]["msg_execute_debit"], "", "", "", Logging.LogType.INFO);
                m_Log.addDebug("executeDebit", "Canceled");

                throw new InvalidOperationException(m_ErrorResponses[m_Language]["execute_debit_is_error"]);
            }

            System.Globalization.NumberFormatInfo provider = new System.Globalization.NumberFormatInfo();
            provider.CurrencyDecimalDigits = 2;

            PscService srvc = new PscService();
            ExecuteDebitReturn rtrn = srvc.executeDebit(m_TransactionValues.Username,
                                    m_TransactionValues.Password,
                                    m_TransactionValues.Mtid,
                                    new string[] { m_TransactionValues.SubId },
                                    Convert.ToDouble(m_TransactionValues.Amount, provider),
                   m_TransactionValues.Currency,
                                    Convert.ToInt32(m_TransactionValues.Close),
                                    "");

            //m_Log.deleteLogMessages();

            //if (rtrn.errorCode == 0 && rtrn.resultCode == 0)
            //{
            //    m_Log.addDebug("executeDebit_response", "ResultCode: " + rtrn.resultCode + " - ErrorCode: " + rtrn.errorCode);
            //    m_Log.addLog("payment_done", "", "", "", Logging.LogType.INFO);

            //    return rtrn;
            //}

            //string executeDebitParams =
            //    "Username: >>empty<< \r\n" +
            //    "Password: >>empty<< \r\n" +
            //    "mtid: " + rtrn.mtid + "\r\n" +
            //    "subId: " + rtrn.subId + "\r\n" +
            //    "amount: " + m_TransactionValues.Amount + "\r\n" +
            //    "currency: " + m_TransactionValues.Currency + "\r\n" +
            //    "close: " + m_TransactionValues.Close;

            //m_Log.addLog(m_ErrorResponses[m_Language]["execute_debit_error"], "executeDebit",
            //    executeDebitParams, "ResultCode: " + rtrn.resultCode + " - ErrorCode: " + rtrn.errorCode);

            //m_Log.addLog(m_ErrorResponses[m_Language]["msg_execute_debit"], "", "", "", Logging.LogType.INFO);
            //m_Log.addDebug("executeDebit_params", executeDebitParams);
            //m_Log.addDebug("executeDebit_response", "ResultCode: " + rtrn.resultCode + " - ErrorCode: " + rtrn.errorCode);

            //throw new Exception(m_ErrorResponses[m_Language]["execute_debit_error"]);
            return rtrn;
        }

        private KeyValuePair<bool, string> ValidateUsername(string username)
        {
            string ErrorDescription = "";
            if (username.Length < 4) ErrorDescription = m_ErrorResponses[m_Language]["username_length"];
            return new KeyValuePair<bool, string>(ErrorDescription == "", ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidatePassword(string password)
        {
            string ErrorDescription = "";
            if (password.Length < 6) ErrorDescription = m_ErrorResponses[m_Language]["passwor_length"];
            return new KeyValuePair<bool, string>(ErrorDescription == "", ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateMtId(string mtID)
        {
            string ErrorDescription = "";
            if (mtID.Length > 60) ErrorDescription = m_ErrorResponses[m_Language]["mtid_oversize"];
            if (mtID.Length < 1) ErrorDescription = m_ErrorResponses[m_Language]["mtid_undersize"];

            Match match = Regex.Match(mtID, "^.*");

            if (!match.Success && ErrorDescription == "") ErrorDescription = m_ErrorResponses[m_Language]["mtid_invalid"];
            return new KeyValuePair<bool, string>(match.Success, ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateSubId(string subId)
        {
            string ErrorDescription = "";
            if (subId.Length > 8) ErrorDescription = m_ErrorResponses[m_Language]["subid_oversize"];
            return new KeyValuePair<bool, string>(ErrorDescription.Length == 0, ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateAmount(string amount)
        {
            // Optional auto correct:
            if (m_AutoCorrect)
            {
                string tmpAmount = amount;
                tmpAmount.Replace(',', '.');

                m_Log.addDebug("AutoCorrect_Amount", amount + " => " + tmpAmount);

                if (amount != tmpAmount)
                {
                    m_Log.addLog(m_ErrorResponses[m_Language]["auto_correct_ammount_warning"], "setAmount_AutoCorrect",
                        amount, tmpAmount, Logging.LogType.WARNING);
                }
            }

            string ErrorDescription = "";
            Match match = Regex.Match(amount, @"(?:^\d{1,11}(?:\.\d{2}$))");

            if (!match.Success && ErrorDescription == "") ErrorDescription = m_ErrorResponses[m_Language]["amount_invalid"];
            return new KeyValuePair<bool, string>(match.Success, ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateCurrency(string currency)
        {
            string ErrorDescription = "";

            if (currency.Length != 3)
            {
                ErrorDescription = m_ErrorResponses[m_Language]["wrong_currency"];
                return new KeyValuePair<bool, string>(ErrorDescription.Length == 3, ErrorDescription);
            }

            Match match = Regex.Match(currency, @"\b[A-Z]+\b");

            if (!match.Success) ErrorDescription = m_ErrorResponses[m_Language]["wrong_currency_case"];
            return new KeyValuePair<bool, string>(match.Success, ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateUrl(string url)
        {
            return new KeyValuePair<bool, string>(!(url == "" || url.Length < 10), "");
        }

        private KeyValuePair<bool, string> ValidateOkUrl(string okUrl)
        {
            KeyValuePair<bool, string> retVal = ValidateUrl(okUrl);
            if (!retVal.Key) return new KeyValuePair<bool, string>(retVal.Key, m_ErrorResponses[m_Language]["invalid_ok_url"]);
            else return retVal;
        }

        private KeyValuePair<bool, string> ValidateNokUrl(string nOkUrl)
        {
            KeyValuePair<bool, string> retVal = ValidateUrl(nOkUrl);
            if (!retVal.Key) return new KeyValuePair<bool, string>(retVal.Key, m_ErrorResponses[m_Language]["invalid_nok_url"]);
            else return retVal;
        }

        private KeyValuePair<bool, string> ValidateMerchantClientID(string merchantId)
        {
            string ErrorDescription = "";
            if (merchantId.Length > 50) ErrorDescription = m_ErrorResponses[m_Language]["merchantid_oversize"];
            if (merchantId.Length < 1) ErrorDescription = m_ErrorResponses[m_Language]["merchantid_undersize"];

            Match match = Regex.Match(merchantId, "^.*");

            if (!match.Success && ErrorDescription == "") ErrorDescription = m_ErrorResponses[m_Language]["merchantid_invalid"];
            return new KeyValuePair<bool, string>(match.Success, ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidatePnUrl(string pnUrl)
        {
            KeyValuePair<bool, string> retVal = ValidateUrl(pnUrl);
            if (!retVal.Key) return new KeyValuePair<bool, string>(retVal.Key, m_ErrorResponses[m_Language]["invalid_pn_url"]);
            else return retVal;
        }

        private KeyValuePair<bool, string> ValidateShopId(string shopId)
        {
            string ErrorDescription = "";
            if (shopId.Length > 60) ErrorDescription = m_ErrorResponses[m_Language]["shopid_oversize"];
            if (shopId.Length < 1) ErrorDescription = m_ErrorResponses[m_Language]["shopid_undersize"];

            Match match = Regex.Match(shopId, @"^[A-Z0-9a-z\-_]{1,60}$");

            if (!match.Success && ErrorDescription == "") ErrorDescription = m_ErrorResponses[m_Language]["shopid_invalid"];
            return new KeyValuePair<bool, string>(match.Success, ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateShopLabel(string shopLabel)
        {
            string ErrorDescription = "";
            if (shopLabel.Length > 60) ErrorDescription = m_ErrorResponses[m_Language]["shoplabel_oversize"];
            if (shopLabel.Length < 1) ErrorDescription = m_ErrorResponses[m_Language]["shoplabel_undersize"];

            Match match = Regex.Match(shopLabel, @"^[A-Z0-9a-z\-_]{1,60}$");

            if (!match.Success && ErrorDescription == "") ErrorDescription = m_ErrorResponses[m_Language]["shoplabel_invalid"];
            return new KeyValuePair<bool, string>(match.Success, ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateRestrictedCountry(string country)
        {
            string tmpCountry = country;

            if (m_AutoCorrect)
            {
                tmpCountry.ToUpper();
                m_Log.addDebug("autoCorrect_RestricedCountry", country + " => " + tmpCountry);

                if (tmpCountry != country)
                {
                    m_Log.addLog(m_ErrorResponses[m_Language]["auto_correct_res_country"], "SetRestricedCountry_AutoCorrect",
                        country, tmpCountry, Logging.LogType.WARNING);
                }
            }

            string ErrorDescription = "";
            if (country.Length != 2) ErrorDescription = m_ErrorResponses[m_Language]["restricted_country_invalide"];

            tmpCountry = country;
            tmpCountry = tmpCountry.ToUpper();

            if (tmpCountry != country) ErrorDescription = m_ErrorResponses[m_Language]["restricted_country_case"];
            return new KeyValuePair<bool, string>(ErrorDescription == "", ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateMinKycLevel(string minKycLevel)
        {
            string ErrorDescription = "";
            bool kycLevelFound = false;

            foreach (string kycLevel in m_KycLevels)
            {
                if (kycLevel == minKycLevel)
                {
                    kycLevelFound = true;
                    break;
                }
            }

            if (!kycLevelFound) ErrorDescription = m_ErrorResponses[m_Language]["min_kyc_level_invalide"];
            return new KeyValuePair<bool, string>(ErrorDescription == "", ErrorDescription);
        }

        private KeyValuePair<bool, string> ValidateMinAge(string minAge)
        {
            string ErrorDescription = "";
            Match match = Regex.Match(minAge, @"^[0-9]{1,2}$");

            if (!match.Success) ErrorDescription = m_ErrorResponses[m_Language]["min_age_invalide"];
            return new KeyValuePair<bool, string>(match.Success, ErrorDescription);
        }

        /// <summary>
        /// Check if merchant exists and credentials are valid
        /// </summary>
        /// <param name="currency"></param>
        /// <returns>getMid return values, as specified in wsdl-file</returns>
        public GetMidReturn ConfirmMerchantData(string currency = "EUR")
        {
            Log.Info("ConfirmMerchantData");
            GetMidReturn output = new GetMidReturn();
            try
            {
                m_TransactionValues.Currency = currency;
                if (m_Log.getLogMessages(Logging.LogType.ERROR).Count > 0)
                {
                    m_Log.addLog(m_ErrorResponses[m_Language]["error_confirm_merchant"],
                        "confirmMerchantData",
                        "Error-Log",
                        m_Log.getLogMessages(Logging.LogType.ERROR).Count.ToString());

                    m_Log.addDebug("confirmMerchantData", "Canceled");
                    throw new InvalidOperationException(m_ErrorResponses[m_Language]["error_confirm_merchant"]);
                }

                PscService srvc = new PscService();
                Log.Info("PscService Url : " + srvc.Url);
                output = srvc.getMid(m_TransactionValues.Username, m_TransactionValues.Password, m_TransactionValues.Currency);
            }
            catch (Exception ex)
            {
                Log.Error("ConfirmMerchantData : " + ex.Message);
                throw;
            }
            return output;
        }

        /// <summary>
        /// Deletes all log messages of the specified type
        /// </summary>
        public void DeleteLogMessages(Logging.LogType logType = Logging.LogType.INFO)
        {
            m_Log.deleteLogMessages(logType);
        }

        /// <summary>
        /// Returns all log messages of the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns>LogMessage-Collection</returns>
        public List<Logging.LogMessage> GetLog(Logging.LogType type)
        {
            return m_Log.getLogMessages(type);
        }
    }
}
