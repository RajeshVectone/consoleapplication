﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;

namespace SimRegistrationAPI
{
    /// <summary>
    /// Summary description for Test12345
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Test12345 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string ping(Ping requestdata)
        {
            return "Success";
        }  
        

        [XmlRoot(ElementName = "ping", Namespace = "")]
        public class Ping
        {
            [XmlElement(ElementName = "requestdata")]
            public Requestdata Requestdata { get; set; }
        } 

    }
}
