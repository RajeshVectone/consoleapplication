﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;
using Newtonsoft.Json;
using NLog;
using SimRegistrationAPI.Models;
using SimRegistrationAPI.Services;

namespace SimRegistrationAPI
{
    /// <summary>
    /// Webservice for Sim Registration
    /// </summary>
    [WebService(Namespace = "http://api.simregistration.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    //[System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        #region ping
        [WebMethod(Description = "The Ping message is intended for the check of supplier interface availability.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public pingResponseDto ping([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)] pingRequestDto requestdata)
        {
            Log.Info("ping : Input : {0}", JsonConvert.SerializeObject(requestdata));
            pingResponseDto output = new pingResponseDto();
            output = new pingResponseDto();
            try
            {
                if (requestdata != null)
                {
                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = "en";
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (output.resultCode != 0)
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.errorCode = 1001;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ping : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.errorCode = 1000;
                output.errorDescription = ex.Message;
            }
            Log.Info("Ping : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region  validateActivatedCard
        [WebMethod(Description = "The ValidateActivatedCard message is intended for the validation of already activated SIM card. Customer will be asked to provide data (Phone number, PUK) which will identifies SIM Card and these data has to be validated on supplier side prior registration is executed.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public validateActivatedCardResponseDto validateActivatedCard([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]validateActivatedCardRequestDto requestdata)
        {
            Log.Info("validateActivatedCard : Input : {0}", JsonConvert.SerializeObject(requestdata));
            validateActivatedCardResponseDto output = new validateActivatedCardResponseDto();
            try
            {
                if (requestdata != null)
                {
                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = requestdata.lang ?? "en";
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (commonModel.ResultCode == 0)
                    {
                        output =  DBHelper.ValidateActivatedCard(requestdata);
                    }
                    else
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.errorCode = 1004;
                    output.errorCodeSpecified = true;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("validateActivatedCard : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1005;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            Log.Info("validateActivatedCard : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region  validateNewCard
        [WebMethod(Description = "The ValidateNewCard message is intended for the validation of new SIM cards, i.e. for case when merchant has physical card. Physical card data (Serial, Phone Number) has to be validated on supplier side prior registration is executed.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public validateNewCardResponseDto validateNewCard([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]validateNewCardRequestDto requestdata)
        {
            Log.Info("validateNewCard : Input : {0}", JsonConvert.SerializeObject(requestdata));
            validateNewCardResponseDto output = new validateNewCardResponseDto();
            try
            {
                if (requestdata != null)
                {
                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = requestdata.lang ?? "en";
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (commonModel.ResultCode == 0)
                    {
                        output = DBHelper.ValidateNewCard(requestdata);
                    }
                    else
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.errorCode = 1004;
                    output.errorCodeSpecified = true;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("validateNewCard : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1005;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            Log.Info("validateNewCard : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region  getRegistrationData
        [WebMethod(Description = "The GetRegistrationData message is intended for the gaining list of fields required for customer registration. List of customer registration fields is variable and can differ between various supplier.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public getRegistrationDataResponseDto getRegistrationData([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]getRegistrationDataRequestDto requestdata)
        {
            Log.Info("getRegistrationData : Input : {0}", JsonConvert.SerializeObject(requestdata));
            getRegistrationDataResponseDto output = new getRegistrationDataResponseDto();
            try
            {
                if (requestdata != null)
                {
                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = requestdata.lang ?? "en";
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (commonModel.ResultCode == 0)
                    {
                        output = DBHelper.GetRegistrationData(requestdata);
                    }
                    else
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.errorCode = 1004;
                    output.errorCodeSpecified = true;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("getRegistrationData : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1005;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            Log.Info("getRegistrationData : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region  registerCard
        [WebMethod(Description = "The RegisterCard message is intended for the SIM card registration. Registration request will include all data gathered during registration process.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public registerCardResponseDto registerCard([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]registerCardRequestDto requestdata)
        {
            Log.Info("registerCard : Input : {0}", JsonConvert.SerializeObject(requestdata));
            registerCardResponseDto output = new registerCardResponseDto();
            try
            {
                if (requestdata != null)
                {
                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = requestdata.lang ?? "en";
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (commonModel.ResultCode == 0)
                    {
                        output = DBHelper.RegisterCard(requestdata);
                    }
                    else
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.errorCode = 1004;
                    output.errorCodeSpecified = true;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("registerCard : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1005;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            Log.Info("registerCard : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region  cancel
        [WebMethod(Description = "The Cancel message is intended for the rollback of previous registration regardless was successful or not. It is executed based on the TransactionId. It will be called in several separate cases: - RegisterCard request failed due to network or any other interruption or error and response has not been received. In this case, Cancel will assure that transaction status is same on both sides; POS and Supplier. - Customer provided wrong data and want to rollback registration in order to do it again - Customer changed his mind and do not want to register card in this moment ")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public cancelResponseDto cancel([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)] cancelRequestDto requestdata)
        {
            Log.Info("cancel : Input : {0}", JsonConvert.SerializeObject(requestdata));
            cancelResponseDto output = new cancelResponseDto();
            try
            {
                if (requestdata != null)
                {
                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = requestdata.lang ?? "en";
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (commonModel.ResultCode == 0)
                    {
                        output = DBHelper.Cancel(requestdata);
                    }
                    else
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.errorCode = 1004;
                    output.errorCodeSpecified = true;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("cancel : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1005;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            Log.Info("cancel : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion
    }
}
