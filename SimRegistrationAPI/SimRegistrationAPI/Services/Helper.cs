﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using SimRegistrationAPI.Models;
using SimRegistrationAPI.Services;

namespace SimRegistrationAPI
{
    public static class Helper
    {
        public static string Username
        {
            get
            {
                return ConfigurationManager.AppSettings["Username"];
            }
        }

        public static string Password
        {
            get
            {
                return ConfigurationManager.AppSettings["Password"];
            }
        }

        public static CommonOutput ValidateLogin(CommonInput input)
        {
            CommonOutput output = new CommonOutput();
            if (!string.IsNullOrEmpty(input.Username))
            {
                if (!string.IsNullOrEmpty(input.Password))
                {
                    if (Username != input.Username || Password != input.Password)
                    {
                        output.ResultCode = 1;
                        output.ErrorCode = 1001;
                        output.ErrorDescription = MessageHelper.GetMessage("USERNAMEORPASSWORDDOESNOTMATCH", input.Lang);
                    }
                    else
                    {
                        output.ResultCode = 0;
                    }
                }
                else
                {
                    output.ResultCode = 1;
                    output.ErrorCode = 1002;
                    output.ErrorDescription = MessageHelper.GetMessage("PASSWORDISREQUIRED", input.Lang);
                }
            }
            else
            {
                output.ResultCode = 1;
                output.ErrorCode = 1003;
                output.ErrorDescription = MessageHelper.GetMessage("USERNAMEISREQUIRED", input.Lang);
            }
            return output;
        }
    }
}