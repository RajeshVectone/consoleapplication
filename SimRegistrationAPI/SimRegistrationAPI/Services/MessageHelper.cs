﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Xml.XPath;

namespace SimRegistrationAPI.Services
{
    public static class MessageHelper
    {
        #region GetMessage
        //Used to get Message with the name and language code
        public static string GetMessage(string messageName, string language)
        {
            string basePath = HttpContext.Current.Server.MapPath("/Content/lang/lang");
            string filename = string.Format("{0}.{1}.xml", basePath, language ?? "en");
            if (!File.Exists(filename))
            {
                filename = string.Format("{0}.{1}.xml", basePath, "en");
            }
            XDocument xml = XDocument.Load(filename);
            XElement response = xml.XPathSelectElement(string.Concat("/messages/", messageName));
            return response == null ? "" : response.Value;
        }
        #endregion
    }

}