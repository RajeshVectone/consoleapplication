﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using NLog;
using System.Data;
using SimRegistrationAPI.Models;
using SimRegistrationAPI.Services;
using Newtonsoft.Json;


namespace SimRegistrationAPI
{
    public static class DBHelper
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        #region ValidateActivatedCard
        public static validateActivatedCardResponseDto ValidateActivatedCard(validateActivatedCardRequestDto input)
        {
            validateActivatedCardResponseDto output = new validateActivatedCardResponseDto();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "simactivation_validate_activated_card";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @ean = input.ean,
                                @retailer = input.retailer,
                                @transactionid = input.transactionId,
                                @timestamp = input.timestamp,
                                @terminalid = input.terminalId,
                                @callid = input.callId,
                                @puk = input.puk,
                                @lang = input.lang,
                                @request_type = 1
                            },
                            commandType: CommandType.StoredProcedure);

                    Log.Info("validateActivatedCard : SP Output : {0}", JsonConvert.SerializeObject(result));

                    if (result != null)
                    {
                        output.resultCode = result.ElementAt(0).errcode;
                        output.resultCodeSpecified = true;
                        output.transactionId = input.transactionId;
                        if (output.resultCode != 0)
                        {
                            output.errorCode = 1006;
                            output.errorCodeSpecified = true;
                            output.errorDescription = result.ElementAt(0).errmsg;
                        }
                    }
                    else
                    {
                        output.resultCode = 1;
                        output.resultCodeSpecified = true;
                        output.errorCode = 1007;
                        output.errorCodeSpecified = true;
                        output.errorDescription = MessageHelper.GetMessage("FAILURE", input.lang);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("ValidateActivatedCard : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1008;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            return output;
        }
        #endregion

        #region ValidateNewCard
        public static validateNewCardResponseDto ValidateNewCard(validateNewCardRequestDto input)
        {
            validateNewCardResponseDto output = new validateNewCardResponseDto();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "simactivation_validate_activated_card";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @ean = input.ean,
                                @retailer = input.retailer,
                                @transactionid = input.transactionId,
                                @timestamp = input.timestamp,
                                @terminalid = input.terminalId,
                                @callid = input.callId,
                                @serial = input.serial,
                                @lang = input.lang,
                                @request_type = 2
                            },
                            commandType: CommandType.StoredProcedure);

                    Log.Info("ValidateNewCard : SP Output : {0}", JsonConvert.SerializeObject(result));

                    if (result != null)
                    {
                        output.resultCode = result.ElementAt(0).errcode;
                        output.resultCodeSpecified = true;
                        output.transactionId = input.transactionId;
                        if (output.resultCode != 0)
                        {
                            output.errorCode = 1006;
                            output.errorCodeSpecified = true;
                            output.errorDescription = result.ElementAt(0).errmsg;
                        }
                    }
                    else
                    {
                        output.resultCode = 1;
                        output.resultCodeSpecified = true;
                        output.errorCode = 1007;
                        output.errorCodeSpecified = true;
                        output.errorDescription = MessageHelper.GetMessage("FAILURE", input.lang);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("ValidateNewCard : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1008;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            return output;
        }
        #endregion

        #region GetRegistrationData
        public static getRegistrationDataResponseDto GetRegistrationData(getRegistrationDataRequestDto input)
        {
            getRegistrationDataResponseDto output = new getRegistrationDataResponseDto();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "simactivation_API_reponse_config_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @retailer = input.retailer,
                                @transactionid = input.transactionId,
                                @timestamp = input.timestamp,
                                @terminalid = input.terminalId,
                                @callid = input.callId,
                                @lang = input.lang,
                                @request_type = 3
                            },
                            commandType: CommandType.StoredProcedure);

                    Log.Info("GetRegistrationData : SP Output : {0}", JsonConvert.SerializeObject(result));

                    if (result != null)
                    {
                        output.resultCode = result.ElementAt(0).errcode;
                        output.resultCodeSpecified = true;
                        output.transactionId = input.transactionId;

                        List<userRegistrationDataInput> lstuserRegistrationDataInput = new List<userRegistrationDataInput>();

                        foreach (var item in result)
                        {
                            userRegistrationDataInput urdt = new userRegistrationDataInput();
                            urdt.fieldLabel = item.fieldLabel;
                            urdt.fieldName = item.fieldName;
                            urdt.fieldType = item.fieldType;
                            urdt.fieldOptions = item.fieldOptions;
                            urdt.fieldRequired = item.fieldRequired;
                            urdt.fieldValidation = item.fieldValidation;
                            lstuserRegistrationDataInput.Add(urdt);
                        }
                        output.registrationData = lstuserRegistrationDataInput;
                        if (output.resultCode != 0)
                        {
                            output.errorCode = 1006;
                            output.errorCodeSpecified = true;
                            output.errorDescription = result.ElementAt(0).errmsg;
                        }
                    }
                    else
                    {
                        output.resultCode = 1;
                        output.resultCodeSpecified = true;
                        output.errorCode = 1007;
                        output.errorCodeSpecified = true;
                        output.errorDescription = MessageHelper.GetMessage("FAILURE", input.lang);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetRegistrationData : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1008;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            return output;
        }
        #endregion

        #region RegisterCard
        public static registerCardResponseDto RegisterCard(registerCardRequestDto input)
        {
            registerCardResponseDto output = new registerCardResponseDto();
            try
            {
                string first_name = "", last_name = "", postcode = "", address = "", docu_no = "", docu_type = "";
                foreach (var item in input.registrationData)
                {
                    switch (item.fieldName)
                    {
                        case "firstName":
                            first_name = item.fieldValue;
                            break;
                        case "lastName":
                            last_name = item.fieldValue;
                            break;
                        case "postcode":
                            postcode = item.fieldValue;
                            break;
                        case "address":
                            address = item.fieldValue;
                            break;
                        case "identityCardType":
                            docu_type = item.fieldValue;
                            break;
                        case "identityCardNum":
                            docu_no = item.fieldValue;
                            break;
                        default:
                            break;
                    }
                }

                if (!String.IsNullOrEmpty(first_name) && !String.IsNullOrEmpty(last_name) && !String.IsNullOrEmpty(postcode) && !String.IsNullOrEmpty(address) && !String.IsNullOrEmpty(docu_type) && !String.IsNullOrEmpty(docu_no))
                {
                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        conn.Open();
                        var sp = "simactivation_validate_activated_card";
                        var result = conn.Query<dynamic>(
                                sp, new
                                {
                                    @ean = input.ean,
                                    @retailer = input.retailer,
                                    @transactionid = input.transactionId,
                                    @timestamp = input.timestamp,
                                    @terminalid = input.terminalId,
                                    @callid = input.callId,
                                    @datausageconsent = input.dataUsageConsent,
                                    @lang = input.lang,
                                    @first_name = first_name,
                                    @last_name = last_name,
                                    @address = address,
                                    @postcode = postcode,
                                    @docu_type = docu_type,
                                    @docu_no = docu_no,
                                    @request_type = 4
                                },
                                commandType: CommandType.StoredProcedure);

                        Log.Info("RegisterCard : SP Output : {0}", JsonConvert.SerializeObject(result));

                        if (result != null)
                        {
                            output.resultCode = result.ElementAt(0).errcode;
                            output.resultCodeSpecified = true;
                            output.transactionId = input.transactionId;
                            if (output.resultCode != 0)
                            {
                                output.errorCode = 1006;
                                output.errorCodeSpecified = true;
                                output.errorDescription = result.ElementAt(0).errmsg;
                            }
                        }
                        else
                        {
                            output.resultCode = 1;
                            output.resultCodeSpecified = true;
                            output.errorCode = 1007;
                            output.errorCodeSpecified = true;
                            output.errorDescription = MessageHelper.GetMessage("FAILURE", input.lang);
                        }
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.transactionId = input.transactionId;
                    output.errorCode = 1009;
                    output.errorCodeSpecified = true;
                    if (String.IsNullOrEmpty(first_name))
                        output.errorDescription = MessageHelper.GetMessage(string.Format("FIRSTNAMEREQUIRED"), input.lang);
                    if (String.IsNullOrEmpty(last_name))
                        output.errorDescription = MessageHelper.GetMessage(string.Format("LASTNAMEREQUIRED"), input.lang);
                    if (String.IsNullOrEmpty(postcode))
                        output.errorDescription = MessageHelper.GetMessage(string.Format("POSTCODEREQUIRED"), input.lang);
                    if (String.IsNullOrEmpty(address))
                        output.errorDescription = MessageHelper.GetMessage(string.Format("ADDRESSREQUIRED"), input.lang);
                    if (String.IsNullOrEmpty(docu_type))
                        output.errorDescription = MessageHelper.GetMessage(string.Format("DOCTYPEREQUIRED"), input.lang);
                    if (String.IsNullOrEmpty(docu_no))
                        output.errorDescription = MessageHelper.GetMessage(string.Format("DOCNOREQUIRED"), input.lang);
                }
            }
            catch (Exception ex)
            {
                Log.Error("RegisterCard : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1008;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            return output;
        }
        #endregion

        #region Cancel
        public static cancelResponseDto Cancel(cancelRequestDto input)
        {
            cancelResponseDto output = new cancelResponseDto();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "simactivation_validate_activated_card";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @transactionid = input.transactionId,
                                @timestamp = input.timestamp,
                                @lang = input.lang,
                                @request_type = 5
                            },
                            commandType: CommandType.StoredProcedure);

                    Log.Info("Cancel : SP Output : {0}", JsonConvert.SerializeObject(result));

                    if (result != null)
                    {
                        output.resultCode = result.ElementAt(0).errcode;
                        output.resultCodeSpecified = true;
                        output.transactionId = input.transactionId;
                        if (output.resultCode != 0)
                        {
                            output.errorCode = 1006;
                            output.errorCodeSpecified = true;
                            output.errorDescription = result.ElementAt(0).errmsg;
                        }
                    }
                    else
                    {
                        output.resultCode = 1;
                        output.resultCodeSpecified = true;
                        output.errorCode = 1007;
                        output.errorCodeSpecified = true;
                        output.errorDescription = MessageHelper.GetMessage("FAILURE", input.lang);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Cancel : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1008;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            return output;
        }
        #endregion
    }
}