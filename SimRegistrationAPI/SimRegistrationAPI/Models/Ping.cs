﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SimRegistrationAPI.Models
{
    [XmlRoot(ElementName = "pingRequest")]
    public class PingRequest : CommonInput
    {
        //[XmlElement(ElementName = "username", IsNullable = false)]
        //public string Username { get; set; }
        //[XmlElement(ElementName = "password", IsNullable = false)]
        //public string Password { get; set; }
    }

    [XmlRoot(ElementName = "pingResponse")]
    public class PingResponse
    {
        [XmlElement(ElementName = "return", IsNullable = false)]
        public PingReturn Return { get; set; }
    }

    [XmlRoot(ElementName = "return")]
    public class PingReturn : CommonOutput
    {
        
    }
}