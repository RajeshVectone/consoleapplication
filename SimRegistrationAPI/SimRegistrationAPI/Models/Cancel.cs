﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SimRegistrationAPI.Models
{
    [XmlRoot(ElementName = "cancelRequest")]
    public class CancelRequest : CommonInput
    {
        [XmlElement(ElementName = "transactionid", IsNullable = false)]
        public string TransactionId { get; set; }
        [XmlElement(ElementName = "timestamp", IsNullable = false)]
        public string Timestamp { get; set; }
    }

    [XmlRoot(ElementName = "cancelResponse")]
    public class CancelResponse
    {
        [XmlElement(ElementName = "return", IsNullable = false)]
        public CancelReturn Return { get; set; }
        
    }

    [XmlRoot(ElementName = "return")]
    public class CancelReturn : CommonOutput
    {
        [XmlElement(ElementName = "transactionId", IsNullable = false)]
        public string TransactionId { get; set; }
    }

    
}