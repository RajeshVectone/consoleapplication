﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SimRegistrationAPI.Models
{
    [XmlRoot(ElementName = "validateActivatedCardRequest")]
    public class ValidateActivatedCardRequest : CommonInput
    {
        [XmlElement(ElementName = "ean", IsNullable = false)]
        public string Ean { get; set; }
        [XmlElement(ElementName = "retailer", IsNullable = false)]
        public string Retailer { get; set; }
        [XmlElement(ElementName = "transactionid", IsNullable = false)]
        public string TransactionId { get; set; }
        [XmlElement(ElementName = "timestamp", IsNullable = false)]
        public string Timestamp { get; set; }
        [XmlElement(ElementName = "terminalid", IsNullable = false)]
        public string TerminalId { get; set; }
        [XmlElement(ElementName = "callid", IsNullable = false)]
        public string CallId { get; set; }
        [XmlElement(ElementName = "puk", IsNullable = false)]
        public string Puk { get; set; }
    }

    [XmlRoot(ElementName = "validateActivatedCardResponse")]
    public class ValidateActivatedCardResponse : CommonOutput
    {
        [XmlElement(ElementName = "transactionid", IsNullable = false)]
        public string TransactionId { get; set; }
    }
}