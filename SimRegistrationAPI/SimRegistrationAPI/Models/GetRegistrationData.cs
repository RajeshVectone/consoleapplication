﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SimRegistrationAPI.Models
{
    [XmlRoot(ElementName = "getRegistrationDataRequest")]
    public class GetRegistrationDataRequest : CommonInput
    {
        [XmlElement(ElementName = "retailer", IsNullable = false)]
        public string Retailer { get; set; }
        [XmlElement(ElementName = "transactionid", IsNullable = false)]
        public string TransactionId { get; set; }
        [XmlElement(ElementName = "timestamp", IsNullable = false)]
        public string Timestamp { get; set; }
        [XmlElement(ElementName = "terminalid", IsNullable = false)]
        public string TerminalId { get; set; }
        [XmlElement(ElementName = "callid", IsNullable = false)]
        public string CallId { get; set; }
    }

    [XmlRoot(ElementName = "getRegistrationDataResponse")]
    public class GetRegistrationDataResponse : CommonOutput
    {
        [XmlElement(ElementName = "transactionid", IsNullable = false)]
        public string TransactionId { get; set; }
        [XmlElement(ElementName = "registrationData", IsNullable = true)]
        public List<GetRegistrationData> lstRegistrationData { get; set; }
    }

    [XmlRoot(ElementName = "registrationData")]
    public class GetRegistrationData
    {
        [XmlElement(ElementName = "fieldLabel", IsNullable = true)]
        public string FieldLabel { get; set; }
        [XmlElement(ElementName = "fieldName", IsNullable = true)]
        public string FieldName { get; set; }
        [XmlElement(ElementName = "fieldType", IsNullable = true)]
        public string FieldType { get; set; }
        [XmlElement(ElementName = "fieldOptions", IsNullable = true)]
        public string FieldOptions { get; set; }
        [XmlElement(ElementName = "fieldRequired", IsNullable = true)]
        public string FieldRequired { get; set; }
        [XmlElement(ElementName = "fieldValidation", IsNullable = true)]
        public string FieldValidation { get; set; }
    }
}