﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SimRegistrationAPI.Models
{
    [XmlRoot(ElementName = "registerCardRequest")]
    public class RegisterCardRequest : CommonInput
    {
        [XmlElement(ElementName = "ean", IsNullable = false)]
        public string Ean { get; set; }
        [XmlElement(ElementName = "retailer", IsNullable = false)]
        public string Retailer { get; set; }
        [XmlElement(ElementName = "transactionid", IsNullable = false)]
        public string TransactionId { get; set; }
        [XmlElement(ElementName = "timestamp", IsNullable = false)]
        public string Timestamp { get; set; }
        [XmlElement(ElementName = "terminalid", IsNullable = false)]
        public string TerminalId { get; set; }
        [XmlElement(ElementName = "callid", IsNullable = false)]
        public string CallId { get; set; }
        [XmlElement(ElementName = "dataUsageConsent", IsNullable = true)]
        public string DataUsageConsent { get; set; }
        [XmlElement(ElementName = "registrationData", IsNullable = false)]
        public List<RegisterCardRegistrationData> lstRegistrationData { get; set; }
    }

    [XmlRoot(ElementName = "registrationData")]
    public class RegisterCardRegistrationData
    {
        [XmlElement(ElementName = "fieldName", IsNullable = true)]
        public string FieldName { get; set; }
        [XmlElement(ElementName = "fieldValue", IsNullable = true)]
        public string FieldValue { get; set; }
    }

    [XmlRoot(ElementName = "registerCardResponse")]
    public class RegisterCardResponse : CommonOutput
    {
        [XmlElement(ElementName = "transactionid", IsNullable = false)]
        public string TransactionId { get; set; }
    }
}