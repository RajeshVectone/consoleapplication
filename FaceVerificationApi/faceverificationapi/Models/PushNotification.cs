﻿namespace paypalpaymentapi
{
    public class PushNotificationInput
    {
        public string sender { get; set; }
        public string receiver { get; set; }
        public string message { get; set; }
        public bool isvoip { get; set; }
    }

    public class PushNotificationOutput : CommonOutput
    {
       
    }
} 