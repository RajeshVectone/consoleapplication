﻿namespace faceverificationapi
{
    public class CommonOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class FaceVerificationOutput : CommonOutput
    {
        public bool IsIdentical { get; set; }
        public double Confidence { get; set; }
    }

    public class FaceVerificationInput 
    {
        public string ImageBase64String1 { get; set; }
        public string ImageBase64String2 { get; set; }
        public string calledby { get; set; }
        public string UID { get; set; }
        public string email { get; set; }
        public string mobileno { get; set; }
        public string iccid { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
    }
}