﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Azure.CognitiveServices.Vision.Face;
using Microsoft.Azure.CognitiveServices.Vision.Face.Models;
using Newtonsoft.Json;
using NLog;

namespace faceverificationapi
{
    public class FaceVerificationController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private string subscriptionKey = ConfigurationManager.AppSettings["SUBSCRIPTIONKEY"];
        private string faceEndpoint = ConfigurationManager.AppSettings["FACEENDPOINT"];
        #endregion

        public HttpResponseMessage Post(FaceVerificationInput input)
        {
            Log.Info("FaceVerificationController");
            FaceVerificationOutput output = new FaceVerificationOutput();
            if (input != null && !String.IsNullOrEmpty(input.ImageBase64String1) && !String.IsNullOrEmpty(input.ImageBase64String2))
            {
                Log.Info("Input : " + JsonConvert.SerializeObject(input));
                try
                {
                    FaceClient faceClient = new FaceClient(new ApiKeyServiceClientCredentials(subscriptionKey), new System.Net.Http.DelegatingHandler[] { });
                    faceClient.Endpoint = faceEndpoint;

                    byte[] bytes1 = Convert.FromBase64String(input.ImageBase64String1);
                    byte[] bytes2 = Convert.FromBase64String(input.ImageBase64String2);


                    var faceList1 = GetFaceList(faceClient, bytes1);
                    if (faceList1 != null && faceList1.Count > 0)
                    {
                        var faceList2 = GetFaceList(faceClient, bytes2);
                        if (faceList2 != null && faceList2.Count > 0)
                        {
                            var result = faceClient.Face.VerifyFaceToFaceAsync((Guid)faceList1[0].FaceId, (Guid)faceList2[0].FaceId).Result;

                            output.Confidence = result.Confidence;
                            output.IsIdentical = result.IsIdentical;
                            if (output.IsIdentical)
                            {
                                output.errcode = 0;
                                output.errmsg = "Success";
                            }
                            else
                            {
                                output.errcode = -1;
                                output.errmsg = "Failure";
                            }
                        }
                        else
                        {
                            output.errcode = -1;
                            output.errmsg = "Unable to recognize Image 2";
                        }
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "Unable to recognize Image 1";
                    }
                }
                catch (Exception ex)
                {
                    output = new FaceVerificationOutput() { errcode = -1, errmsg = ex.Message };
                    Log.Error(ex.Message);
                }
            }
            else
            {
                output = new FaceVerificationOutput() { errcode = -1, errmsg = "Input details not found!" };
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(output));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        #region GetFaceList
        private static IList<DetectedFace> GetFaceList(FaceClient faceClient, byte[] bytes)
        {
            using (Stream imageStream = new MemoryStream(bytes))
            {
                IList<DetectedFace> faceList = faceClient.Face.DetectWithStreamAsync(imageStream, true, false).Result;
                return faceList;
            }
        }
        #endregion

        //For Testing
        //SaveImage(input.ImageBase64String1)
        public string SaveImage(string fileContent)
        {
            bool bSuccess = false;
            string fileName = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + ".jpg";
            try
            {
                byte[] bytes = Convert.FromBase64String(fileContent);
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms);
                    image.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\Images", fileName), System.Drawing.Imaging.ImageFormat.Jpeg);
                    bSuccess = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            if (bSuccess)
                return Path.Combine(@"Images\", fileName);
            else
                throw new Exception("Unable to save image");
        }
    }
}
