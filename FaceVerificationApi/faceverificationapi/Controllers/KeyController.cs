﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;

namespace apnsproviderapi
{
	public class KeyController : ApiController
	{
        protected HmacAuthentication _hmac = new HmacAuthentication();

		/// <summary>
		/// Get Keys
		/// </summary>
		/// <remarks>Purpose :  Based on user generating public,Auth keys  </remarks>
		/// <response code="400">Bad request</response>
		/// <response code="500">Internal Server Error</response>
		/// <response code="200">Success</response>
		[ResponseType(typeof(Keys))]
		public HttpResponseMessage Get()
		{
			var request = HttpContext.Current.Request;
			string contentMd5 = request.Headers["Content-Md5"] ?? request.Headers["Content-MD5"];
            string publickey = Convert.ToBase64String(Helper.ToByteArray(contentMd5)).ToString();


			string headTimestamp = DateTime.Now.ToString();



			var context = new WebApiRequestContext()
			{
				HttpMethod = request.HttpMethod,
				HttpAcceptType = request.Headers["Accept"],
				PublicKey = publickey,
				ContentMd5 = contentMd5,
				SecretKey = "apnsproviderapiwithhash",
				Url = HttpUtility.UrlDecode(request.Url.AbsoluteUri.ToLower())
			};


			string messageRepresentation = _hmac.CreateMessageRepresentation(context, contentMd5, headTimestamp);
			string getAuthkey = _hmac.CreateSignature("apnsproviderapiwithhash", messageRepresentation);
			string postAuthkey = _hmac.CreateSignature("apnsproviderapiwithhash", "post" + messageRepresentation.Substring(3, messageRepresentation.Length - 3));
			var query = new Keys { Publickey = publickey, GetAuthkey = getAuthkey, PostAuthkey = postAuthkey };
			return Request.CreateResponse(HttpStatusCode.OK, query); 
		}
	}
	public class Keys
	{
        private string _publickey;
        private string _getAuthkey;
        private string _postAuthkey;

        public string PostAuthkey
        {
            get { return _postAuthkey; }
            set { _postAuthkey = value; }
        }

        public string Publickey
        {
            get { return _publickey; }
            set { _publickey = value; }
        }

        public string GetAuthkey
        {
            get { return _getAuthkey; }
            set { _getAuthkey = value; }
        }

		//Added : 02-Aug-2016
		private string _authtoken;

		public string auth_token
		{
			get { return _authtoken; }
			set { _authtoken = value; }
		}
	}
}
