using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace apnsproviderapi.Controllers
{
    public class RwbuserloginspController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class RwbuserloginspInput
        {
			public string username  { get; set; }
			public string password  { get; set; }  	                   	    
        }
        public class RwbuserloginspOutput
        {
			public int Id  { get; set; }
			public string username  { get; set; }
			public string useremail  { get; set; }
			public int? usertype  { get; set; }
			public int errcode  { get; set; }
			public string errmsg  { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, RwbuserloginspInput req)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Roaming_wb"].ConnectionString))
                {
                    conn.Open();
                    var sp ="RWB_User_Login_Sp";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
							@username=req.username,
							@password=req.password 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    List<RwbuserloginspOutput> OutputList = new List<RwbuserloginspOutput>();
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new RwbuserloginspOutput()
                        {
						Id=r.Id==null?0:r.Id,
						username=r.username,
						useremail=r.useremail,
						usertype=r.usertype==null?0:r.usertype,
						errcode=r.errcode==null?0:r.errcode,
						errmsg=r.errmsg==null?"Success":r.errmsg
                        }));
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                    else
                    {
                        RwbuserloginspOutput outputobj = new RwbuserloginspOutput();
						outputobj.errcode=-1;
						outputobj.errmsg="No Rec found";                     
                        OutputList.Add(outputobj);
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
}
