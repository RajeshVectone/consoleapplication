using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;

namespace apnsproviderapi
{
	public class RomingwhiteblacklistuploadController : ApiController
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();
		public class RomingwhiteblacklistuploadInput
		{
			public string login  { get; set; }
			public string srcfile  { get; set; }
			public int isstaging  { get; set; }  	                   	    
		}
		public class RomingwhiteblacklistuploadOutput
		{
			public int errcode  { get; set; }
			public string errmsg  { get; set; }	            	    
		}

		[Authorize]
		public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, RomingwhiteblacklistuploadInput req)
		{
			try
			{
				using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Roaming_wb"].ConnectionString))
				{
					conn.Open();
					var sp ="Roming_White_BlackList_Upload";
					var result = conn.Query<dynamic>(
							sp, new
							{
							@login=req.login,
							@srcfile=req.srcfile,
							@isstaging=req.isstaging 			                                                       
							},
							commandType: CommandType.StoredProcedure);
					List<RomingwhiteblacklistuploadOutput> OutputList = new List<RomingwhiteblacklistuploadOutput>();
					if (result != null && result.Count() > 0)
					{
						OutputList.AddRange(result.Select(r => new RomingwhiteblacklistuploadOutput()
						{
						errcode=r.errcode==null?0:r.errcode,
						errmsg=r.errmsg==null?"Success":r.errmsg
						}));
						return Request.CreateResponse(HttpStatusCode.OK, OutputList);
					}
					else
					{
						RomingwhiteblacklistuploadOutput outputobj = new RomingwhiteblacklistuploadOutput();
						outputobj.errcode=-1;
						outputobj.errmsg="No Rec found";                     
						OutputList.Add(outputobj);
						return Request.CreateResponse(HttpStatusCode.OK, OutputList);
					}
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			return Request.CreateResponse(HttpStatusCode.Unauthorized);
		}
	}
}
