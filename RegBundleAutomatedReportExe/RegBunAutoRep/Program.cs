﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace RegBunAutoRep
{
    #region TariffAutomationBundleReport
    public class TariffAutomationBundleReport
    {
        public string category { get; set; }
        public string bundle_name { get; set; }
        public string Denomination { get; set; }
        public string Numberofsubscriptions { get; set; }
        public string NumberofRenewals { get; set; }
        public string NewCLIsSubscribed { get; set; }
        public string ActiveClis { get; set; }
        public string Offline { get; set; }
        public string online { get; set; }
        public int? unique_active_cli { get; set; }
        public int? unique_subscribed_cli { get; set; }
        public int? unique_new_cli { get; set; }
        public int? bonus_count { get; set; }
        public int? del_unique_active_cli { get; set; }
        public int? del_unique_subscribed_cli { get; set; }
        public int? del_unique_new_cli { get; set; }
    }
    #endregion

    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                DoProcess();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region DoProcess
        static void DoProcess()
        {
            try
            {
                string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile"]);
                string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName"]+ "_" + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";

                Console.WriteLine("docName : " + docName);
                log.Info("docName : " + docName);

                File.Copy(templateName, docName);

                FillUKRecord(docName);

                FillATRecord(docName);

                FillBERecord(docName);

                FillFRRecord(docName);

                SendEmail(docName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region FillUKRecord
        static void FillUKRecord(string docName)
        {
            Console.WriteLine("FillUKRecord()");
            log.Info("FillUKRecord()");
            try
            {
                //UK
                int iRecordCount = 0;
                while (iRecordCount == 0)
                {
                    var records = DataAccess.GetUKRecord();
                    if (records != null && records.Count > 0)
                    {
                        iRecordCount = records.Count;
                        Console.WriteLine("No Of Records found : " + iRecordCount);
                        log.Info("No Of Records found : " + iRecordCount);

                        //UK Sheet
                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "UK").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            //Date - A2
                            InsertValue("A", 2, DateTime.Now.AddDays(-1).ToString("dd-MM-yyyy"), CellValues.String, wsPart);

                            //No. of Recommend Bonus on {0} - A4
                            InsertValue("A", 4, string.Format("No.of Recommend Bonus on {0} ", DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy")), CellValues.String, wsPart);

                            //No. of Recommend Bonus on - C4
                            InsertValue("C", 4, Convert.ToString(records.ElementAt(0).bonus_count), CellValues.String, wsPart);

                            //30 Days Rolling -Pay Monthly SIM - UK - C8
                            uint iRowIndex = 7;
                            var record = records.Where(r => r.category.ToUpper().Trim() == "UK").ToList();
                            if (record != null && record.Count > 0)
                            {
                                for (int index = 0; index < record.Count(); index++)
                                {
                                    iRowIndex++;
                                    InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                    InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                    InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                }
                            }

                            //30 Days Rolling -Pay Monthly SIM - UK(OLD) - C17
                            record = records.Where(r => r.category.ToUpper().Trim() == "UK (OLD)").ToList();
                            iRowIndex = 16;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //UK  Pocket  Saver - C27
                            record = records.Where(r => r.category.ToUpper().Trim() == "UK POCKET SAVER").ToList();
                            iRowIndex = 26;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Data Bundles - D44
                            record = records.Where(r => r.category.ToUpper().Trim() == "DATA BUNDLES").ToList();
                            iRowIndex = 43;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //UK All in One Bundles - C58
                            record = records.Where(r => r.category.ToUpper().Trim() == "UK ALL IN ONE BUNDLES").ToList();
                            iRowIndex = 57;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Online Exclusive Bundles -C75
                            record = records.Where(r => r.category.ToUpper().Trim() == "ONLINE EXCLUSIVE BUNDLES").ToList();
                            iRowIndex = 74;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Discounted Bundle -C93
                            record = records.Where(r => r.category.ToUpper().Trim() == "DISCOUNTED BUNDLE").ToList();
                            iRowIndex = 92;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Special Bundles -C104
                            record = records.Where(r => r.category.ToUpper().Trim() == "SPECIAL BUNDLES").ToList();
                            iRowIndex = 103;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //UK & International Twin Bundles - C112
                            iRowIndex = 111;
                            record = records.Where(r => r.category.ToUpper().Trim() == "UK & INTERNATIONAL TWIN BUNDLES").ToList();
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //International Bundle-1 - D120
                            record = records.Where(r => r.category.ToUpper().Trim() == "INTERNATIONAL BUNDLE-1").ToList();
                            iRowIndex = 119;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //International Bundle-2 - D174 
                            record = records.Where(r => r.category.ToUpper().Trim() == "INTERNATIONAL BUNDLE-2").ToList();
                            iRowIndex = 173;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //SL Hutch Bundle - C213
                            record = records.Where(r => r.category.ToUpper().Trim() == "SL HUTCH BUNDLE").ToList();
                            iRowIndex = 212;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Tigo Ghana Bundle - C222
                            record = records.Where(r => r.category.ToUpper().Trim() == "TIGO GHANA BUNDLE").ToList();
                            iRowIndex = 221;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Tigo Senegal Bundle - C230
                            record = records.Where(r => r.category.ToUpper().Trim() == "TIGO SENEGAL BUNDLE").ToList();
                            iRowIndex = 229;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Pakistan Pocket Saver - C236
                            record = records.Where(r => r.category.ToUpper().Trim() == "PAKISTAN POCKET SAVER").ToList();
                            iRowIndex = 235;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Online Pocket Saver - C245
                            record = records.Where(r => r.category.ToUpper().Trim() == "ONLINE POCKET SAVER").ToList();
                            iRowIndex = 244;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Country Saver - C253
                            record = records.Where(r => r.category.ToUpper().Trim() == "COUNTRY SAVER").ToList();
                            iRowIndex = 252;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //VMUK Airtel India Test Bun 10£ New - C260
                            record = records.Where(r => r.category.ToUpper().Trim() == "VMUK AIRTEL INDIA TEST BUN 10£ NEW").ToList();
                            iRowIndex = 259;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //VMUK Philippines G Bun 15£ New - C266
                            record = records.Where(r => r.category.ToUpper().Trim() == "VMUK PHILIPPINES G BUN 15£ NEW").ToList();
                            iRowIndex = 265;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //VMUK Philippines S Bun 20£ New - C272
                            record = records.Where(r => r.category.ToUpper().Trim() == "VMUK PHILIPPINES S BUN 20£ NEW").ToList();
                            iRowIndex = 271;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Vxtra GBP UL Bundle 9.90GBP - C278
                            record = records.Where(r => r.category.ToUpper().Trim() == "VXTRA GBP UL BUNDLE 9.90GBP").ToList();
                            iRowIndex = 277;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            //Total No of Unique UK active PAYG Bundle Subscribers - D285
                            InsertValue("C", 285, Convert.ToString(records.ElementAt(0).unique_active_cli), CellValues.Number, wsPart);

                            //Total No. of unique subscribed CLIs on {0} - A287
                            InsertValue("A", 287, string.Format("Total No. of unique subscribed CLIs on {0}", DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy")), CellValues.String, wsPart);

                            //Total No. of unique subscribed CLIs - C287
                            InsertValue("C", 287, Convert.ToString(records.ElementAt(0).unique_subscribed_cli), CellValues.Number, wsPart);

                            //Total No. of unique subscribed CLIs on {0} - C289
                            InsertValue("C", 289, Convert.ToString(records.ElementAt(0).unique_new_cli), CellValues.Number, wsPart);

                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("UK - No records found!");
                        log.Debug("UK - No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("UK : " + ex.Message);
                log.Error("UK : " + ex.Message);
            }
        }
        #endregion

        #region FillATRecord
        static void FillATRecord(string docName)
        {
            Console.WriteLine("FillATRecord()");
            log.Info("FillATRecord()");
            try
            {
                //AT
                int iRecordCount = 0;
                while (iRecordCount == 0)
                {
                    var recordsAT = DataAccess.GetATRecord();
                    if (recordsAT != null && recordsAT.Count > 0)
                    {
                        iRecordCount = recordsAT.Count;
                        Console.WriteLine("No Of Records found : " + iRecordCount);
                        log.Info("No Of Records found : " + iRecordCount);

                        //AT Sheet
                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "AT").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            //VMAT PAY MONTHLY - C2
                            uint iRowIndex = 1;
                            var record = recordsAT.Where(r => r.category.ToUpper().Trim() == "VMAT PAY MONTHLY").ToList();
                            if (record != null && record.Count > 0)
                            {
                                for (int index = 0; index < record.Count(); index++)
                                {
                                    iRowIndex++;
                                    InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                    InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                    InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                    InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                }
                            }

                            //VMAT(12 Month Bundle) - C11
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "VMAT(12 MONTH BUNDLE)").ToList();
                            iRowIndex = 10;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //DMAT Pay Monthly - C17
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "DMAT PAY MONTHLY").ToList();
                            iRowIndex = 16;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //AT Pocket Saver - C25
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "AT POCKET SAVER").ToList();
                            iRowIndex = 24;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Onnet Bundle - C32
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "ONNET BUNDLE").ToList();
                            iRowIndex = 31;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //AT Data Packet - C38
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "AT DATA PACKET").ToList();
                            iRowIndex = 37;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //All in One Bundle - C50
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "ALL IN ONE BUNDLE").ToList();
                            iRowIndex = 49;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //All In One Maxi Bundle - C58
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "ALL IN ONE MAXI BUNDLE").ToList();
                            iRowIndex = 57;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Online All In one Pocket Saver  - C66
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "ONLINE ALL IN ONE POCKET SAVER").ToList();
                            iRowIndex = 65;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Online National Pocket Saver   - C70
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "ONLINE NATIONAL POCKET SAVER").ToList();
                            iRowIndex = 69;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //EU Plan   - C81
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "EU PLAN").ToList();
                            iRowIndex = 80;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Country saver  - C88
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "COUNTRY SAVER").ToList();
                            iRowIndex = 87;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Tigo_Ghana_MT Bundle  - C134
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "TIGO_GHANA_MT BUNDLE").ToList();
                            iRowIndex = 133;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //TIGO Senegal Bundle  - C140
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "TIGO SENEGAL BUNDLE").ToList();
                            iRowIndex = 139;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Turkey Unite Bundle  - C146
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "TURKEY UNITE BUNDLE").ToList();
                            iRowIndex = 145;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Hutch Bundle  - C153
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "HUTCH BUNDLE").ToList();
                            iRowIndex = 152;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Vxtra AUS EUR UL Bundle 9.90EUR - C161
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "VXTRA AUS EUR UL BUNDLE 9.90EUR").ToList();
                            iRowIndex = 160;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Delight Data bundles  - C176
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "DELIGHT DATA BUNDLES").ToList();
                            iRowIndex = 175;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //National  Delight Bundles  - C187
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "NATIONAL  DELIGHT BUNDLES").ToList();
                            iRowIndex = 186;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //International Bundle   - C198
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "INTERNATIONAL BUNDLE").ToList();
                            iRowIndex = 197;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //European Bundle   - C209
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "EUROPEAN BUNDLE").ToList();
                            iRowIndex = 208;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //All In One Bundle   - C217
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "ALL IN ONE BUNDLE-DEL").ToList();
                            iRowIndex = 216;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }

                            //Talk n Text Bundle  - C226
                            record = recordsAT.Where(r => r.category.ToUpper().Trim() == "TALK N TEXT BUNDLE").ToList();
                            iRowIndex = 225;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                InsertValue("G", iRowIndex, Convert.ToString(record.ElementAt(index).Offline), CellValues.Number, wsPart);
                                InsertValue("H", iRowIndex, Convert.ToString(record.ElementAt(index).online), CellValues.Number, wsPart);
                            }


                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            //Vectone

                            //Total No. of Unique Austria Vectone active PAYG Bundle Subscribers:  - D167
                            InsertValue("D", 167, Convert.ToString(recordsAT.ElementAt(0).unique_active_cli), CellValues.Number, wsPart);

                            //Total No. of unique subscribed CLIs on {0} - A169
                            InsertValue("A", 169, string.Format("Total No. of unique subscribed CLIs on {0}", DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy")), CellValues.String, wsPart);

                            InsertValue("D", 169, Convert.ToString(recordsAT.ElementAt(0).unique_subscribed_cli), CellValues.Number, wsPart);

                            //Total No. Of unique New subscribed CLIs - D171
                            InsertValue("D", 171, Convert.ToString(recordsAT.ElementAt(0).unique_new_cli), CellValues.Number, wsPart);

                            //Delight

                            //Total No. of Unique Austria Delight active PAYG Bundle Subscribers: - D233
                            InsertValue("D", 233, Convert.ToString(recordsAT.ElementAt(0).del_unique_active_cli), CellValues.Number, wsPart);

                            //Total No. of unique subscribed CLIs on {0} - D235
                            InsertValue("A", 235, string.Format("Total No. of unique subscribed CLIs on {0}", DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy")), CellValues.String, wsPart);

                            InsertValue("D", 235, Convert.ToString(recordsAT.ElementAt(0).del_unique_subscribed_cli), CellValues.Number, wsPart);

                            //Total No. of Unique Austria Delight active PAYG Bundle Subscribers: - D237
                            InsertValue("D", 237, Convert.ToString(recordsAT.ElementAt(0).del_unique_new_cli), CellValues.Number, wsPart);

                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("AT - No records found!");
                        log.Debug("AT - No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("AT : " + ex.Message);
                log.Error("AT : " + ex.Message);
            }
        }
        #endregion

        #region FillBERecord
        static void FillBERecord(string docName)
        {
            Console.WriteLine("FillBERecord()");
            log.Info("FillBERecord()");
            try
            {
                //BE
                int iRecordCount = 0;
                while (iRecordCount == 0)
                {
                    var recordsBE = DataAccess.GetBERecord();
                    if (recordsBE != null && recordsBE.Count > 0)
                    {
                        iRecordCount = recordsBE.Count;
                        Console.WriteLine("No Of Records found : " + iRecordCount);
                        log.Info("No Of Records found : " + iRecordCount);

                        //AT Sheet
                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "BE").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            //Pay Monthly - C2
                            uint iRowIndex = 1;
                            var record = recordsBE.Where(r => r.category.ToUpper().Trim() == "PAY MONTHLY").ToList();
                            if (record != null && record.Count > 0)
                            {
                                for (int index = 0; index < record.Count(); index++)
                                {
                                    iRowIndex++;
                                    InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                    InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                    InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                    InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                }
                            }

                            //National Bundle - C12
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "NATIONAL BUNDLE").ToList();
                            iRowIndex = 11;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Data Bundle - C24
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "DATA BUNDLE").ToList();
                            iRowIndex = 23;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //All one Bundle - C36
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "ALL ONE BUNDLE").ToList();
                            iRowIndex = 35;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //International Bundle - C47
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "INTERNATIONAL BUNDLE").ToList();
                            iRowIndex = 46;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //EU Plan Plus - C56
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "EU PLAN PLUS").ToList();
                            iRowIndex = 55;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Super Onnet Bundle - C61
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "SUPER ONNET BUNDLE").ToList();
                            iRowIndex = 60;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Special Bundle - C68
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "SPECIAL BUNDLE").ToList();
                            iRowIndex = 67;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Nigeria Bundle    - C73
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "NIGERIA BUNDLE").ToList();
                            iRowIndex = 72;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Ghana Bundle   - C78
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "GHANA BUNDLE").ToList();
                            iRowIndex = 77;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Tigo_Ghana Bundle  - C83
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "TIGO_GHANA BUNDLE").ToList();
                            iRowIndex = 82;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Senegal Tigo Bundle  - C89
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "SENEGAL TIGO BUNDLE").ToList();
                            iRowIndex = 88;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Friends & Family Bundle (Hutch)  - C95
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "FRIENDS & FAMILY BUNDLE (HUTCH)").ToList();
                            iRowIndex = 94;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Turkey Unite Bundle    - C102
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "TURKEY UNITE BUNDLE").ToList();
                            iRowIndex = 101;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Vxtra BE EUR UL Bundle 9.90EUR  - C106
                            record = recordsBE.Where(r => r.category.ToUpper().Trim() == "VXTRA BE EUR UL BUNDLE 9.90EUR").ToList();
                            iRowIndex = 105;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            //Vectone

                            //Total No of Unique Belgium active PAYG Bundle Subscribers -C111
                            InsertValue("C", 111, Convert.ToString(recordsBE.ElementAt(0).unique_active_cli), CellValues.Number, wsPart);

                            //Total No. of unique subscribed {0} - A113
                            InsertValue("A", 113, string.Format("Total No. of unique subscribed {0}", DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy")), CellValues.String, wsPart);

                            InsertValue("C", 113, Convert.ToString(recordsBE.ElementAt(0).unique_subscribed_cli), CellValues.Number, wsPart);

                            //Total No. of unique New CLI's subscribed  - C115
                            InsertValue("C", 115, Convert.ToString(recordsBE.ElementAt(0).unique_new_cli), CellValues.Number, wsPart);

                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("BE - No records found!");
                        log.Debug("BE - No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("BE : " + ex.Message);
                log.Error("BE : " + ex.Message);
            }
        }
        #endregion

        #region FillFRRecord
        static void FillFRRecord(string docName)
        {
            Console.WriteLine("FillFRRecord()");
            log.Info("FillFRRecord()");
            try
            {
                //FR
                int iRecordCount = 0;
                while (iRecordCount == 0)
                {
                    var recordsFR = DataAccess.GetFRRecord();
                    if (recordsFR != null && recordsFR.Count > 0)
                    {
                        iRecordCount = recordsFR.Count;
                        Console.WriteLine("No Of Records found : " + iRecordCount);
                        log.Info("No Of Records found : " + iRecordCount);

                        //AT Sheet
                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "FR").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            //Pay Monthly - C2
                            uint iRowIndex = 1;
                            var record = recordsFR.Where(r => r.category.ToUpper().Trim() == "PAY MONTHLY").ToList();
                            if (record != null && record.Count > 0)
                            {
                                for (int index = 0; index < record.Count(); index++)
                                {
                                    iRowIndex++;
                                    InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                    InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                    InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                    InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                                }
                            }

                            //National Bundle - C11
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "NATIONAL BUNDLE").ToList();
                            iRowIndex = 10;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Data Bundle - C23
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "DATA BUNDLE").ToList();
                            iRowIndex = 22;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //All one Bundle - C33
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "ALL IN ONE BUNDLE").ToList();
                            iRowIndex = 32;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Online Nat Bundle  - C40
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "ONLINE NAT BUNDLE").ToList();
                            iRowIndex = 39;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //AIO Bundle (Online) - C53
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "AIO BUNDLE (ONLINE)").ToList();
                            iRowIndex = 52;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //International Bundle  - C61
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "INTERNATIONAL BUNDLE").ToList();
                            iRowIndex = 60;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Super Onnet Bundle - C80
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "SUPER ONNET BUNDLE").ToList();
                            iRowIndex = 79;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //France Special Bundle    - C86
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "FRANCE SPECIAL BUNDLE").ToList();
                            iRowIndex = 85;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Nigeria Bundle (Online)   - C96
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "NIGERIA BUNDLE (ONLINE)").ToList();
                            iRowIndex = 95;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Senegal Tigo Bundle   - C103
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "SENEGAL TIGO BUNDLE").ToList();
                            iRowIndex = 102;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Senegal Bundle  - C108
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "SENEGAL BUNDLE").ToList();
                            iRowIndex = 107;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Tigo_Ghana Bundle  - C112
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "TIGO_GHANA BUNDLE").ToList();
                            iRowIndex = 111;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Somalia SomTEL Bundle     - C117
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "SOMALIA SOMTEL BUNDLE").ToList();
                            iRowIndex = 116;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Friends & Family Bundle - C121
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "FRIENDS & FAMILY BUNDLE").ToList();
                            iRowIndex = 120;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Vxtra FR EUR UL Bundle 9.90EUR - C128
                            record = recordsFR.Where(r => r.category.ToUpper().Trim() == "VXTRA FR EUR UL BUNDLE 9.90EUR").ToList();
                            iRowIndex = 127;
                            for (int index = 0; index < record.Count(); index++)
                            {
                                iRowIndex++;
                                InsertValue("C", iRowIndex, Convert.ToString(record.ElementAt(index).NewCLIsSubscribed), CellValues.Number, wsPart);
                                InsertValue("D", iRowIndex, Convert.ToString(record.ElementAt(index).NumberofRenewals), CellValues.Number, wsPart);
                                InsertValue("E", iRowIndex, Convert.ToString(record.ElementAt(index).Numberofsubscriptions), CellValues.Number, wsPart);
                                InsertValue("F", iRowIndex, Convert.ToString(record.ElementAt(index).ActiveClis), CellValues.Number, wsPart);
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            //Total No. of unique subscribed CLIs on {0} - A133
                            InsertValue("A", 133, string.Format("Total No. of unique subscribed {0}", DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy")), CellValues.String, wsPart);

                            InsertValue("C", 133, Convert.ToString(recordsFR.ElementAt(0).unique_subscribed_cli), CellValues.Number, wsPart);

                            //Total No. Of unique New subscribed CLIs  - C134
                            InsertValue("C", 134, Convert.ToString(recordsFR.ElementAt(0).unique_new_cli), CellValues.Number, wsPart);

                            //Total No. of Unique Vectone France active PAYG Bundle Subscribers: -C135
                            InsertValue("C", 135, Convert.ToString(recordsFR.ElementAt(0).unique_active_cli), CellValues.Number, wsPart);

                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("FR - No records found!");
                        log.Debug("FR - No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FR : " + ex.Message);
                log.Error("FR : " + ex.Message);
            }
        }
        #endregion

        #region SendEmail
        static void SendEmail(string docName)
        {
            try
            {
                //Mail Sending 
                string mailContent = string.Empty;
                string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(';').ToList();
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item));
                }
                log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mail Sending Error : " + ex.Message);
                log.Debug("Mail Sending Error : " + ex.Message);
            }
        } 
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, string value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(value);
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
