﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace RegBunAutoRep
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetUKRecord
        public static List<TariffAutomationBundleReport> GetUKRecord()
        {
            List<TariffAutomationBundleReport> OutputList = new List<TariffAutomationBundleReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_UK"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_automation_bundle_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationBundleReport()
                        {
                            category = r.category,
                            bundle_name = r.bundle_name,
                            Denomination = r.Denomination,
                            Numberofsubscriptions = r.Numberofsubscriptions,
                            NumberofRenewals = r.NumberofRenewals,
                            NewCLIsSubscribed = r.NewCLIsSubscribed,
                            ActiveClis = r.ActiveClis,
                            Offline = r.Offline,
                            online = r.online,
                            unique_active_cli = r.unique_active_cli,
                            unique_subscribed_cli = r.unique_subscribed_cli,
                            unique_new_cli = r.unique_new_cli,
                            bonus_count = r.bonus_count
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetUKRecord() ex : " + ex.Message);
                log.Error("GetUKRecord() ex : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetATRecord
        public static List<TariffAutomationBundleReport> GetATRecord()
        {
            List<TariffAutomationBundleReport> OutputList = new List<TariffAutomationBundleReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_AT"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_automation_bundle_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationBundleReport()
                        {
                            category = r.category,
                            bundle_name = r.bundle_name,
                            Denomination = r.Denomination,
                            Numberofsubscriptions = r.Numberofsubscriptions,
                            NumberofRenewals = r.NumberofRenewals,
                            NewCLIsSubscribed = r.NewCLIsSubscribed,
                            ActiveClis = r.ActiveClis,
                            Offline = r.Offline,
                            online = r.online,
                            unique_active_cli = r.unique_active_cli,
                            unique_subscribed_cli = r.unique_subscribed_cli,
                            unique_new_cli = r.unique_new_cli,
                            del_unique_active_cli = r.del_unique_active_cli,
                            del_unique_subscribed_cli = r.del_unique_subscribed_cli,
                            del_unique_new_cli = r.del_unique_new_cli
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetATRecord() ex : " + ex.Message);
                log.Error("GetATRecord() ex : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetBERecord
        public static List<TariffAutomationBundleReport> GetBERecord()
        {
            List<TariffAutomationBundleReport> OutputList = new List<TariffAutomationBundleReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_BE"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_automation_bundle_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationBundleReport()
                        {
                            category = r.category,
                            bundle_name = r.bundle_name,
                            Denomination = r.Denomination,
                            Numberofsubscriptions = r.Numberofsubscriptions,
                            NumberofRenewals = r.NumberofRenewals,
                            NewCLIsSubscribed = r.NewCLIsSubscribed,
                            ActiveClis = r.ActiveClis,
                            Offline = r.Offline,
                            online = r.online,
                            unique_active_cli = r.unique_active_cli,
                            unique_subscribed_cli = r.unique_subscribed_cli,
                            unique_new_cli = r.unique_new_cli,
                            bonus_count = r.bonus_count
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetBERecord() ex : " + ex.Message);
                log.Error("GetBERecord() ex : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetFRRecord
        public static List<TariffAutomationBundleReport> GetFRRecord()
        {
            List<TariffAutomationBundleReport> OutputList = new List<TariffAutomationBundleReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_FR"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_automation_bundle_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationBundleReport()
                        {
                            category = r.category,
                            bundle_name = r.bundle_name,
                            Denomination = r.Denomination,
                            Numberofsubscriptions = r.Numberofsubscriptions,
                            NumberofRenewals = r.NumberofRenewals,
                            NewCLIsSubscribed = r.NewCLIsSubscribed,
                            ActiveClis = r.ActiveClis,
                            Offline = r.Offline,
                            online = r.online,
                            unique_active_cli = r.unique_active_cli,
                            unique_subscribed_cli = r.unique_subscribed_cli,
                            unique_new_cli = r.unique_new_cli,
                            bonus_count = r.bonus_count
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetFRRecord() ex : " + ex.Message);
                log.Error("GetFRRecord() ex : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}