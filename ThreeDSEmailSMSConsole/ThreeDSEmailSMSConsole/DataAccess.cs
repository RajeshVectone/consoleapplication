﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Newtonsoft.Json;
using NLog;
#endregion

namespace ThreeDSEmailSMSConsole
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetCustomerInfo
        public static List<GetCustomerInfoOutput> GetCustomerInfo()
        {
            List<GetCustomerInfoOutput> outputList = new List<GetCustomerInfoOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "SENDSMS_THREED_TRANSACTION_FAILURE_DAEMON";
                    var result = conn.Query<GetCustomerInfoOutput>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetCustomerInfo() ", ex.Message);
            }
            return outputList;
        }
        #endregion

        #region UpdateStatus
        public static void UpdateStatus(int id, int email_sent_flag, int sms_id)
        {
            try
            {
                log.Info("UpdateStatus : {0} - {1} - {2} ", id, email_sent_flag, sms_id);
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "SENDSMS_THREED_TRANSACTION_UPDATE_FAILURE";
                    var result = conn.Query<GetCustomerInfoOutput>(
                            sp, new
                            {
                                @ID = id,
                                @email_sent_flag = email_sent_flag,
                                @sms_id = sms_id
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                log.Error("UpdateOtaStatus() ", ex.Message);
            }
        }
        #endregion

        #region GetReport
        public static List<ReportOutput> GetReport()
        {
            List<ReportOutput> outputList = new List<ReportOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "SENDSMS_THREED_TRANSACTION_FAILURE_DAEMON_REPORT";
                    var result = conn.Query<ReportOutput>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetReport() ", ex.Message);
            }
            return outputList;
        }
        #endregion
    }
}
