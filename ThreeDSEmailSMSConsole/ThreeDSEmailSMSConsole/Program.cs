﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;
#endregion

namespace ThreeDSEmailSMSConsole
{
    #region Declarations
    public class GetCustomerInfoOutput
    {
        public int ID { get; set; }
        public string ACCOUNT_ID { get; set; }
        public string sms_url { get; set; }
        public string message_text { get; set; }
        public string email { get; set; }
    }

    public class Output
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class ReportOutput
    {
        public string AccountId { get; set; }
        public string SMSSent { get; set; }
        public string EmailSent { get; set; }
    }
    #endregion

    class Program
    {
        #region Declarations
        static Logger log = LogManager.GetCurrentClassLogger();
        #endregion

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            try
            {
                DoProcess();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
        }
        #endregion

        #region DoProcess
        private static void DoProcess()
        {
            try
            {
                List<GetCustomerInfoOutput> outputList = DataAccess.GetCustomerInfo();
                if (outputList != null && outputList.Count > 0)
                {
                    Console.WriteLine("Total Records : " + outputList.Count);
                    log.Info("Total Records : " + outputList.Count);

                    log.Info("Records : " + JsonConvert.SerializeObject(outputList));

                    for (int index = 0; index < outputList.Count; index++)
                    {
                        try
                        {
                            log.Info("Input " + index + " : " + JsonConvert.SerializeObject(outputList[index]));

                            //Default value to 5
                            int emailSent = 5; 
                            int smsSent = 5;
                            try
                            {
                                if (!String.IsNullOrEmpty(outputList[index].email))
                                {
                                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                                    var message = new MandrillMessage();
                                    message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"];
                                    message.AddTo(outputList[index].email, "Customer");
                                    message.Subject = ConfigurationManager.AppSettings["MAILCHIMPSUBJECT"];
                                    var resultMail = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPTEMPLATENAME"]).Result;
                                    log.Info("Mail Response : " + JsonConvert.SerializeObject(resultMail));
                                    if (resultMail != null && resultMail.Count > 0)
                                        emailSent = (int)resultMail[0].Status;
                                    else
                                        emailSent = -1;
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("Mail Error : " + ex.Message);
                                emailSent = -1;
                            }

                            try
                            {
                                if (!String.IsNullOrEmpty(outputList[index].ACCOUNT_ID) && !String.IsNullOrEmpty(outputList[index].sms_url))
                                {
                                    var resultSMS = Utility.SendSMS(outputList[index].sms_url, outputList[index].ACCOUNT_ID, ConfigurationManager.AppSettings["SMSSENDER"], outputList[index].message_text);
                                    log.Info("SMS Response : " + JsonConvert.SerializeObject(resultSMS));
                                    if (resultSMS != null && resultSMS.Count() > 0)
                                        smsSent = Convert.ToInt32(resultSMS[0]);
                                    else
                                        smsSent = -1;
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("SMS Error : " + ex.Message);
                                smsSent = -1;
                            }
                            DataAccess.UpdateStatus(outputList[index].ID, emailSent, smsSent);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            log.Error(ex.Message);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No records found");
                    log.Info("No records found");
                }

                //For Reporting
                List<ReportOutput> outputReport = DataAccess.GetReport();
                if (outputReport != null)
                {
                    string mailMessage = "";
                    if (outputReport.Count > 0)
                    {
                        mailMessage = "<table style='border:1px solid black;'><tr><th style='border:1px solid black;'>Account Id</th><th style='border:1px solid black;'>SMS Sent</th><th style='border:1px solid black;'>Email Sent</th></tr>";
                        for (int i = 0; i < outputReport.Count; i++)
                        {
                            mailMessage += string.Format("<tr><td style='border:1px solid black;'>{0}</td><td style='border:1px solid black;'>{1}</td><td style='border:1px solid black;'>{2}</td></tr>", outputReport[i].AccountId, outputReport[i].SMSSent, outputReport[i].EmailSent);
                        }
                        mailMessage += "</table>";
                    }
                    else
                    {
                        mailMessage = "No records found!";
                    }

                    //var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                    //var message = new MandrillMessage();
                    //message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"];
                    //message.Subject = ConfigurationManager.AppSettings["REPORTMAILSUBJECT"] + DateTime.Now.AddDays(-1).ToString("dd-MMM-yyyy");
                    //string[] arrTo = ConfigurationManager.AppSettings["REPORTMAILTO"].Split(';');
                    //if (arrTo.Length > 0)
                    //{
                    //    for (int i = 0; i < arrTo.Length; i++)
                    //    {
                    //        message.AddTo(arrTo[i]);
                    //    }
                    //}
                    //message.AutoHtml = true;
                    //message.Html = mailMessage;
                    //var resultMail = api.Messages.SendAsync(message).Result;
                    //log.Info("Mail Response : " + JsonConvert.SerializeObject(resultMail));

                    string mailSubject = ConfigurationManager.AppSettings["REPORTMAILSUBJECT"] + DateTime.Now.AddDays(-1).ToString("dd-MMM-yyyy");
                    MailAddressCollection mailTo = new MailAddressCollection();
                    string[] arrTo = ConfigurationManager.AppSettings["REPORTMAILTO"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    if (arrTo.Length > 0)
                    {
                        for (int i = 0; i < arrTo.Length; i++)
                        {
                            mailTo.Add(arrTo[i]);
                        }
                    }
                    MailAddressCollection mailCC = new MailAddressCollection();
                    string[] arrCc = ConfigurationManager.AppSettings["REPORTMAILCC"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    if (arrCc.Length > 0)
                    {
                        for (int i = 0; i < arrCc.Length; i++)
                        {
                            mailCC.Add(arrCc[i]);
                        }
                    }
                    bool bMailRetrun = Utility.SendMail(true, new MailAddress(ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"]), mailTo, mailCC, null, mailSubject, mailMessage);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error("ERROR : " + ex.Message);
            }
            Console.WriteLine("Completed");
        }
        #endregion
    }
}
