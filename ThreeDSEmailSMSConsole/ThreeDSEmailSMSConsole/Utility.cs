﻿#region Assemblies
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using NLog;
#endregion

namespace ThreeDSEmailSMSConsole
{
    public class Utility
    {
        #region Declarations
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        #endregion

        #region Send
        public static bool SendMail(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string filePath = "")
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                if (filePath != "")
                {
                    System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType();
                    contentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Octet;
                    contentType.Name = Path.GetFileName(filePath);
                    email.Attachments.Add(new Attachment(filePath, contentType));
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return false;
            }
        }
        #endregion

        #region SendSMS
        public static string[] SendSMS(string smsurl, string destination, string originator, string message)
        {
            string[] strArrays = new string[] { "-1", "SMS Send Failed!" };
            string postData = "";
            try
            {
                postData = string.Format(string.Concat(smsurl, "?service-name=port&destination-addr={0}&originator-addr-type=5&originator-addr={1}&payload-type=text&message={2}"), destination, originator, HttpUtility.UrlPathEncode(message));
                Logger.Info("postData : " + postData);
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(postData);
                httpWebRequest.Method = "POST";
                httpWebRequest.UserAgent = "TPGWeb";
                httpWebRequest.ContentLength = (long)0;
                StreamReader streamReader = new StreamReader(((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream());
                strArrays = streamReader.ReadToEnd().Trim().Split(":".ToCharArray(), 2);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                try
                {
                    MailAddressCollection mailTo = new MailAddressCollection();
                    mailTo.Add(new MailAddress("a.dhakshinamoorthy@vectone.com"));
                    Utility.SendMail(false, new MailAddress("noreply@vectonemobile.co.uk"), mailTo, null, null, ConfigurationManager.AppSettings["MAILSUBJECT"], ex.Message + "  -  " + postData);
                }
                catch { }
            }
            Logger.Info(string.Join(",", strArrays));
            return strArrays;
        }
        #endregion
    }
}
