﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace TariffAutomationOnlineTopupBreakageRep
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetRecord
        public static TariffAutomationOnlineTopupBreakageReport GetRecord()
        {
            TariffAutomationOnlineTopupBreakageReport output = new TariffAutomationOnlineTopupBreakageReport();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_Online_topup_breakage_report";
                    using (var result = conn.QueryMultiple(sp, new { }, commandType: CommandType.StoredProcedure, commandTimeout: 0))
                    {
                        var denominationWise = result.Read<DenominationWise>().ToList();
                        var typeWise = result.Read<TypeWise>().ToList();
                        var paymentrefWise = result.Read<PaymentrefWise>().ToList();
                        var bundlesWise = result.Read<BundlesWise>().ToList();
                        var resellerWise = result.Read<ResellerWise>().ToList();
                        output.denominationWise = denominationWise;
                        output.typeWise = typeWise;
                        output.paymentrefWise = paymentrefWise;
                        output.bundlesWise = bundlesWise;
                        output.resellerWise = resellerWise;
                    }
                    return output;
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRecord() ex : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}
