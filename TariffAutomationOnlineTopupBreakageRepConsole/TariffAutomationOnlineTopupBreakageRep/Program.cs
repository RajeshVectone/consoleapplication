﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace TariffAutomationOnlineTopupBreakageRep
{
    #region 
    public class TariffAutomationOnlineTopupBreakageReport
    {
        public List<DenominationWise> denominationWise { get; set; }
        public List<TypeWise> typeWise { get; set; }
        public List<PaymentrefWise> paymentrefWise { get; set; }
        public List<BundlesWise> bundlesWise { get; set; }
        public List<ResellerWise> resellerWise { get; set; }
    }

    public class DenominationWise
    {
        public string Amount { get; set; }
        public int? Count { get; set; }
        public float? Revenue { get; set; }
    }

    public class TypeWise
    {
        public string Type { get; set; }
        public int? Count { get; set; }
    }

    public class PaymentrefWise
    {
        public string PaymentType { get; set; }
        public int? Count { get; set; }
    }

    public class BundlesWise
    {
        public string Bundles { get; set; }
        public int? CLI { get; set; }
        public float? Price { get; set; }
        public float? Revenue { get; set; }
    }

    public class ResellerWise
    {
        public string Reseller { get; set; }
        public int? Bundles { get; set; }
    }

    #endregion

    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                DoProcess();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region InsertRow
        //InsertRow(iRowIndex, wsPart);
        static void InsertRow(uint rowIndex, WorksheetPart wrksheetPart)
        {
            Worksheet worksheet = wrksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row = null;
            if (sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).Count() != 0)
            {
                Row refRow = sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).First();
                //Copy row from refRow and insert it
                row = CopyToLine(refRow, rowIndex, sheetData);
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }
        }

        //Copy an existing row and insert it
        //We don't need to copy styles of a refRow because a CloneNode() or Clone() methods do it for us
        static Row CopyToLine(Row refRow, uint rowIndex, SheetData sheetData)
        {
            uint newRowIndex;
            var newRow = (Row)refRow.CloneNode(true);
            // Loop through all the rows in the worksheet with higher row 
            // index values than the one you just added. For each one,
            // increment the existing row index.
            IEnumerable<Row> rows = sheetData.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);
            foreach (Row row in rows)
            {
                newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value + 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }
                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }

            sheetData.InsertBefore(newRow, refRow);
            return newRow;
        } 
        #endregion

        #region DoProcess
        static void DoProcess()
        {
            Console.WriteLine("DoProcess()");
            log.Info("DoProcess()");
            try
            {
                string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile"]);
                string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName"] + "_" + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";

                Console.WriteLine("docName : " + docName);
                log.Info("docName : " + docName);

                File.Copy(templateName, docName);

                FillRecord(docName);

                SendEmail(docName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoProcess() : " + ex.Message);
                log.Error("DoProcess() : " + ex.Message);
            }
        }
        #endregion


        #region FillRecord
        static void FillRecord(string docName)
        {
            Console.WriteLine("FillRecord()");
            log.Info("FillRecord()");
            try
            {
                //UK
                int iRecordCount = 0;
                while (iRecordCount == 0)
                {
                    var records = DataAccess.GetRecord();
                    if (records != null)
                    {
                        iRecordCount = 1;
                        Console.WriteLine("Records found : " + iRecordCount);
                        log.Info("Records found : " + iRecordCount);

                        //UK Sheet
                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "OnlineTopupBreakage").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            //A1 
                            InsertValue("A", 1, string.Format("Online 1st Top-Up: {0}", DateTime.Now.AddDays(-1).ToString("dd-MM-yyyy")), CellValues.String, wsPart);

                            uint iRowIndex = 5;
                            uint iStartIndex = 5;
                            //Denomination Wise
                            var denominationWise = records.denominationWise;
                            if (denominationWise != null && denominationWise.Count > 0)
                            {
                                for (int i = 0; i < denominationWise.Count; i++)
                                {
                                    InsertRow(iRowIndex, wsPart);
                                    InsertValue("A", iRowIndex, Convert.ToString(denominationWise[i].Amount), CellValues.Number, wsPart);
                                    InsertValue("B", iRowIndex, Convert.ToString(Convert.ToInt32(denominationWise[i].Count)), CellValues.Number, wsPart);
                                    InsertValue("C", iRowIndex, Convert.ToString(denominationWise[i].Revenue), CellValues.Number, wsPart);
                                    iRowIndex++;
                                }
                                Cell cellFormula = GetCell("B", iRowIndex + 1, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(B{0}:B{1})", iStartIndex, iRowIndex - 1);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("C", iRowIndex + 1, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(C{0}:C{1})", iStartIndex, iRowIndex - 1);
                                cellFormula.CellFormula = cellformula;
                            }

                            //Type Wise
                            iStartIndex = iRowIndex += 5;
                            var typeWise = records.typeWise;
                            if (typeWise != null && typeWise.Count > 0)
                            {
                                for (int i = 0; i < typeWise.Count; i++)
                                {
                                    InsertRow(iRowIndex, wsPart);
                                    InsertValue("A", iRowIndex, Convert.ToString(typeWise[i].Type), CellValues.String, wsPart);
                                    InsertValue("B", iRowIndex, Convert.ToString(Convert.ToInt32(typeWise[i].Count)), CellValues.Number, wsPart);
                                    iRowIndex++;
                                }
                                Cell cellFormula = GetCell("B", iRowIndex + 1, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(B{0}:B{1})", iStartIndex, iRowIndex - 1);
                                cellFormula.CellFormula = cellformula;
                            }

                            //Paymentref Wise
                            iStartIndex = iRowIndex += 5;
                            var paymentrefWise = records.paymentrefWise;
                            if (paymentrefWise != null && paymentrefWise.Count > 0)
                            {
                                for (int i = 0; i < paymentrefWise.Count; i++)
                                {
                                    InsertRow(iRowIndex, wsPart);
                                    InsertValue("A", iRowIndex, Convert.ToString(paymentrefWise[i].PaymentType), CellValues.String, wsPart);
                                    InsertValue("B", iRowIndex, Convert.ToString(Convert.ToInt32(paymentrefWise[i].Count)), CellValues.Number, wsPart);
                                    iRowIndex++;
                                }
                                Cell cellFormula = GetCell("B", iRowIndex + 1, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(B{0}:B{1})", iStartIndex, iRowIndex - 1);
                                cellFormula.CellFormula = cellformula;
                            }

                            //Bundles Wise
                            iStartIndex = iRowIndex += 4;
                            var bundlesWise = records.bundlesWise;
                            if (bundlesWise != null && bundlesWise.Count > 0)
                            {
                                for (int i = 0; i < bundlesWise.Count; i++)
                                {
                                    InsertRow(iRowIndex, wsPart);
                                    InsertValue("A", iRowIndex, Convert.ToString(bundlesWise[i].Bundles), CellValues.String, wsPart);
                                    InsertValue("B", iRowIndex, Convert.ToString(Convert.ToInt32(bundlesWise[i].CLI)), CellValues.Number, wsPart);
                                    InsertValue("C", iRowIndex, Convert.ToString(bundlesWise[i].Price), CellValues.Number, wsPart);
                                    InsertValue("D", iRowIndex, Convert.ToString(bundlesWise[i].Revenue), CellValues.Number, wsPart);
                                    iRowIndex++;
                                }
                                Cell cellFormula = GetCell("B", iRowIndex + 1, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(B{0}:B{1})", iStartIndex, iRowIndex - 1);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("C", iRowIndex + 1, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(C{0}:C{1})", iStartIndex, iRowIndex - 1);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("D", iRowIndex + 1, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(D{0}:D{1})", iStartIndex, iRowIndex - 1);
                                cellFormula.CellFormula = cellformula;
                            }

                            //Reseller Wise
                            iStartIndex = iRowIndex += 4;
                            var resellerWise = records.resellerWise;
                            if (resellerWise != null && resellerWise.Count > 0)
                            {
                                for (int i = 0; i < resellerWise.Count; i++)
                                {
                                    InsertRow(iRowIndex, wsPart);
                                    InsertValue("A", iRowIndex, Convert.ToString(resellerWise[i].Reseller), CellValues.String, wsPart);
                                    InsertValue("B", iRowIndex, Convert.ToString(Convert.ToInt32(resellerWise[i].Bundles)), CellValues.Number, wsPart);
                                    iRowIndex++;
                                }
                                Cell cellFormula = GetCell("B", iRowIndex + 1, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(B{0}:B{1})", iStartIndex, iRowIndex - 1);
                                cellFormula.CellFormula = cellformula;
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }
                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No records found!");
                        log.Debug("No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillRecord : " + ex.Message);
                log.Error("FillRecord : " + ex.Message);
            }
        }
        #endregion

        #region SendEmail
        static void SendEmail(string docName)
        {
            try
            {
                //Mail Sending 
                string mailContent = string.Empty;
                string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(';').ToList();
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item));
                }
                log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mail Sending Error : " + ex.Message);
                log.Debug("Mail Sending Error : " + ex.Message);
            }
        }
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, string value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(value);
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }
        #endregion

        #region GetCell
        private static Cell GetCell(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
            return null;
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
