﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace DelightCallingTopUp
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetDCTariffAutomatedTopupCurrencyReport
        public static List<DCTariffAutomatedTopupCurrencyReport> GetDCTariffAutomatedTopupCurrencyReport()
        {
            List<DCTariffAutomatedTopupCurrencyReport> OutputList = new List<DCTariffAutomatedTopupCurrencyReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection1"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automated_DC_Minute_Summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                               
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new DCTariffAutomatedTopupCurrencyReport()
                        {
                            date = r.date,
                            currcode = r.currcode,
                            Type = r.Type,
                            talktime = r.talktime,
                            cli = r.cli,
                            Mins_percentage = r.Mins_percentage
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetDCTariffAutomatedTopupCurrencyReport() : " + ex.Message);
                log.Error("GetDCTariffAutomatedTopupCurrencyReport() : " + ex.Message);
            }
            return null;
        }
        #endregion


        #region GetTariffAutomationDCMinutesCountrywise
        public static List<TariffAutomationDCMinutesCountrywise> GetTariffAutomationDCMinutesCountrywise()
        {
            List<TariffAutomationDCMinutesCountrywise> OutputList = new List<TariffAutomationDCMinutesCountrywise>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection2"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_DC_Minutes_Countrywise";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationDCMinutesCountrywise()
                        {
                            calldate = r.calldate,
                            Days = r.Days,
                            Country = r.Country,
                            currcode = r.currcode,
                            ani = r.ani,
                            Talktime = r.Talktime
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTariffAutomationDCMinutesCountrywise() : " + ex.Message);
                log.Error("GetTariffAutomationDCMinutesCountrywise() : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}
