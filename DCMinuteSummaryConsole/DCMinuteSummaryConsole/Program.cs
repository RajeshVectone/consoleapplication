﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace DelightCallingTopUp
{
    #region DCTariffAutomatedTopupCurrencyReport
    public class DCTariffAutomatedTopupCurrencyReport
    {
        public DateTime? date { get; set; }
        public string currcode { get; set; }
        public string Type { get; set; }
        public double? talktime { get; set; }
        public double? cli { get; set; }
        public double? Mins_percentage { get; set; }
    }
    #endregion

    #region TariffAutomationDCMinutesCountrywise
    public class TariffAutomationDCMinutesCountrywise
    {
        public string calldate { get; set; }
        public string Days { get; set; }
        public string Country { get; set; }
        public string currcode { get; set; }
        public double? ani { get; set; }
        public double? Talktime { get; set; }
    }
    #endregion

    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile"]);
                string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                File.Copy(templateName, docName);

                Console.Write("docName : " + docName);
                log.Info("docName : " + docName);

                DoDCTariffAutomatedTopupCurrencyReport(docName);

                DoTariffAutomationDCMinutesCountrywise(docName);

                SendMail(docName);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region DoTariffAutomationDCMinutesCountrywise
        static void DoTariffAutomationDCMinutesCountrywise(string docName)
        {
            Console.WriteLine("DoTariffAutomationDCMinutesCountrywise()");
            log.Info("DoTariffAutomationDCMinutesCountrywise()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.GetTariffAutomationDCMinutesCountrywise();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "CountryWise").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct();
                        uint rowIndex = 5;
                        foreach (var item in distinctDates)
                        {
                            if (rowIndex == 5)
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 24, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 32, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 42, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 52, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 60, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 68, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 76, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 84, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 94, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 104, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 114, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 124, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 134, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 144, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 154, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                rowIndex++;
                            }
                            else
                            {
                                InsertValue("A", rowIndex, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 8, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 16, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 24, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 32, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 42, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 52, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 60, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 68, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 76, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 84, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 94, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 104, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 114, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 124, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 134, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 144, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", rowIndex + 154, Convert.ToDateTime(item.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                            }
                        }

                        var distinctDays = (from ld in records select new { id = ld.Days }).ToList().Distinct();
                        rowIndex = 5;
                        foreach (var item in distinctDays)
                        {
                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 24, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 32, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 42, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 52, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 60, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 68, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 76, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 84, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 94, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 104, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 114, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 124, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 134, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 144, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 154, item.id, CellValues.String, wsPart);

                            rowIndex++;

                            InsertValue("B", rowIndex, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 8, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 16, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 24, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 32, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 42, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 52, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 60, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 68, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 76, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 84, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 94, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 104, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 114, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 124, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 134, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 144, item.id, CellValues.String, wsPart);
                            InsertValue("B", rowIndex + 154, item.id, CellValues.String, wsPart);
                        }

                        var distinctCurrCode = (from ld in records select new { id = ld.currcode }).ToList().Distinct();
                        foreach (var currCode in distinctCurrCode)
                        {
                            if (currCode.id.Trim().ToUpper() == "GBP")
                                rowIndex = 3;
                            else if (currCode.id.Trim().ToUpper() == "USD")
                                rowIndex = 45;
                            else if (currCode.id.Trim().ToUpper() == "EUR")
                                rowIndex = 55;
                            else if (currCode.id.Trim().ToUpper() == "REST OF THE WORLD")
                                rowIndex = 97;
                            else if (currCode.id.Trim().ToUpper() == "REST OF THE WORLD MIDDLE EAST")
                                rowIndex = 107;
                            else if (currCode.id.Trim().ToUpper() == "DKK")
                                rowIndex = 117;
                            else if (currCode.id.Trim().ToUpper() == "SEK")
                                rowIndex = 127;
                            else if (currCode.id.Trim().ToUpper() == "SDG")
                                rowIndex = 137;
                            else if (currCode.id.Trim().ToUpper() == "AUD")
                                rowIndex = 147;
                            else if (currCode.id.Trim().ToUpper() == "CHF")
                                rowIndex = 157;
                            else
                                continue;

                            var itemCurrCode = records.Where(r => r.currcode == currCode.id).Select(r => r).ToList();

                            var distinctCountrys = (from ld in itemCurrCode select new { id = ld.Country }).ToList().Distinct();
                            int countryCount = 0;
                            string columnName1 = "";
                            string columnName2 = "";
                            foreach (var item in distinctCountrys)
                            {
                                switch (countryCount)
                                {
                                    case 0:
                                        columnName1 = "C";
                                        columnName2 = "D";
                                        break;
                                    case 1:
                                        columnName1 = "E";
                                        columnName2 = "F";
                                        break;
                                    case 2:
                                        columnName1 = "G";
                                        columnName2 = "H";
                                        break;
                                    case 3:
                                        columnName1 = "I";
                                        columnName2 = "J";
                                        break;
                                    case 4:
                                        columnName1 = "K";
                                        columnName2 = "L";
                                        break;
                                    case 5:
                                        columnName1 = "M";
                                        columnName2 = "N";
                                        break;
                                    case 6:
                                        columnName1 = "O";
                                        columnName2 = "P";
                                        break;
                                    case 7:
                                        columnName1 = "Q";
                                        columnName2 = "R";
                                        break;
                                    case 8:
                                        columnName1 = "S";
                                        columnName2 = "T";
                                        countryCount = -1;
                                        break;
                                    default:
                                        break;
                                }
                                InsertValue(columnName1, rowIndex, item.id, CellValues.String, wsPart);

                                for (int i = 0; i < distinctDates.Count(); i++)
                                {
                                    var item2 = records.Where(r => r.Country == item.id && r.currcode == currCode.id && r.calldate == distinctDates.ElementAt(i).id).Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        if (i == 0)
                                        {
                                            InsertValue(columnName1, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).ani)), CellValues.Number, wsPart);
                                            InsertValue(columnName2, rowIndex + 2, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        }
                                        else
                                        {
                                            InsertValue(columnName1, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).ani)), CellValues.Number, wsPart);
                                            InsertValue(columnName2, rowIndex + 3, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).Talktime)), CellValues.Number, wsPart);
                                        }
                                    }
                                }

                                if (countryCount == -1)
                                    rowIndex += 8;
                                countryCount++;
                            }
                        }


                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoTariffAutomationDCMinutesCountrywise : " + ex.Message);
                log.Error("DoTariffAutomationDCMinutesCountrywise : " + ex.Message);
            }
        }
        #endregion

        #region DoDCTariffAutomatedTopupCurrencyReport
        static void DoDCTariffAutomatedTopupCurrencyReport(string docName)
        {
            Console.WriteLine("DoDCTariffAutomatedTopupCurrencyReport()");
            log.Info("DoDCTariffAutomatedTopupCurrencyReport()");

            try
            {
                int iRowCount = 0;

                //while (iRowCount == 0)
                //{
                var records = DataAccess.GetDCTariffAutomatedTopupCurrencyReport();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;

                    Console.Write("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "MinuteSummary").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.date }).ToList().Distinct().OrderBy(x => x.id);

                        var distinctCurr = (from ld in records select new { id = ld.currcode }).ToList().Distinct();

                        int index2 = 0;
                        foreach (var itemDates in distinctDates)
                        {
                            if (index2 == 0)
                            {
                                for (uint i = 1; i <= 181;)
                                {
                                    InsertValue("C", i, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                    i += 10;
                                }

                                foreach (var itemCurr in distinctCurr)
                                {
                                    uint iRowIndex = 0;
                                    string currency = itemCurr.id.ToUpper();
                                    if (currency == "GBP")
                                        iRowIndex = 3;
                                    else if (currency == "USD")
                                        iRowIndex = 13;
                                    else if (currency == "EUR")
                                        iRowIndex = 23;
                                    else if (currency == "REST OF THE WORLD")
                                        iRowIndex = 33;
                                    else if (currency == "REST OF THE WORLD MIDDLE EAST")
                                        iRowIndex = 43;
                                    else if (currency == "DKK")
                                        iRowIndex = 53;
                                    else if (currency == "SEK")
                                        iRowIndex = 63;
                                    else if (currency == "CAD")
                                        iRowIndex = 73;
                                    else if (currency == "AUD")
                                        iRowIndex = 83;
                                    else if (currency == "CHF")
                                        iRowIndex = 93;
                                    else if (currency == "SDG")
                                        iRowIndex = 103;
                                    else if (currency == "NZD")
                                        iRowIndex = 113;
                                    else if (currency == "CZK")
                                        iRowIndex = 123;
                                    else if (currency == "HKD")
                                        iRowIndex = 133;
                                    else if (currency == "JYP")
                                        iRowIndex = 143;
                                    else if (currency == "NOK")
                                        iRowIndex = 153;
                                    else if (currency == "RSD")
                                        iRowIndex = 163;
                                    else if (currency == "RUB")
                                        iRowIndex = 173;
                                    else if (currency == "ZAR")
                                        iRowIndex = 183;

                                    //Standard_Minutes
                                    var item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "STANDARD_MINUTES").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("E", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Free_Credit 
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "FREE_CREDIT").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("E", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Bundle Minutes 
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "BUNDLE_MINUTES").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("E", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Total Minutes & Unique CLIs 
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "TOTALMINUTESUNIQUECLIS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("E", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }
                                }
                            }
                            else if (index2 == 1)
                            {
                                for (uint i = 1; i <= 181;)
                                {
                                    InsertValue("G", i, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                    i += 10;
                                }

                                foreach (var itemCurr in distinctCurr)
                                {
                                    uint iRowIndex = 0;
                                    string currency = itemCurr.id.ToUpper();
                                    if (currency == "GBP")
                                        iRowIndex = 3;
                                    else if (currency == "USD")
                                        iRowIndex = 13;
                                    else if (currency == "EUR")
                                        iRowIndex = 23;
                                    else if (currency == "REST OF THE WORLD")
                                        iRowIndex = 33;
                                    else if (currency == "REST OF THE WORLD MIDDLE EAST")
                                        iRowIndex = 43;
                                    else if (currency == "DKK")
                                        iRowIndex = 53;
                                    else if (currency == "SEK")
                                        iRowIndex = 63;
                                    else if (currency == "CAD")
                                        iRowIndex = 73;
                                    else if (currency == "AUD")
                                        iRowIndex = 83;
                                    else if (currency == "CHF")
                                        iRowIndex = 93;
                                    else if (currency == "SDG")
                                        iRowIndex = 103;
                                    else if (currency == "NZD")
                                        iRowIndex = 113;
                                    else if (currency == "CZK")
                                        iRowIndex = 123;
                                    else if (currency == "HKD")
                                        iRowIndex = 133;
                                    else if (currency == "JYP")
                                        iRowIndex = 143;
                                    else if (currency == "NOK")
                                        iRowIndex = 153;
                                    else if (currency == "RSD")
                                        iRowIndex = 163;
                                    else if (currency == "RUB")
                                        iRowIndex = 173;
                                    else if (currency == "ZAR")
                                        iRowIndex = 183;

                                    //Standard_Minutes
                                    var item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "STANDARD_MINUTES").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("I", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Free_Credit
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "FREE_CREDIT").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("I", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Bundle Minutes
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "BUNDLE_MINUTES").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("I", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Total Minutes & Unique CLIs
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "TOTALMINUTESUNIQUECLIS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("I", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }
                                }

                            }
                            else if (index2 == 2)
                            {
                                for (uint i = 1; i <= 181;)
                                {
                                    InsertValue("K", i, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                    i += 10;
                                }

                                foreach (var itemCurr in distinctCurr)
                                {
                                    uint iRowIndex = 0;
                                    string currency = itemCurr.id.ToUpper();
                                    if (currency == "GBP")
                                        iRowIndex = 3;
                                    else if (currency == "USD")
                                        iRowIndex = 13;
                                    else if (currency == "EUR")
                                        iRowIndex = 23;
                                    else if (currency == "REST OF THE WORLD")
                                        iRowIndex = 33;
                                    else if (currency == "REST OF THE WORLD MIDDLE EAST")
                                        iRowIndex = 43;
                                    else if (currency == "DKK")
                                        iRowIndex = 53;
                                    else if (currency == "SEK")
                                        iRowIndex = 63;
                                    else if (currency == "CAD")
                                        iRowIndex = 73;
                                    else if (currency == "AUD")
                                        iRowIndex = 83;
                                    else if (currency == "CHF")
                                        iRowIndex = 93;
                                    else if (currency == "SDG")
                                        iRowIndex = 103;
                                    else if (currency == "NZD")
                                        iRowIndex = 113;
                                    else if (currency == "CZK")
                                        iRowIndex = 123;
                                    else if (currency == "HKD")
                                        iRowIndex = 133;
                                    else if (currency == "JYP")
                                        iRowIndex = 143;
                                    else if (currency == "NOK")
                                        iRowIndex = 153;
                                    else if (currency == "RSD")
                                        iRowIndex = 163;
                                    else if (currency == "RUB")
                                        iRowIndex = 173;
                                    else if (currency == "ZAR")
                                        iRowIndex = 183;

                                    //Standard_Minutes
                                    var item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "STANDARD_MINUTES").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("M", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Free_Credit
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "FREE_CREDIT").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("M", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Bundle Minutes
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "BUNDLE_MINUTES").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("M", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Total Minutes & Unique CLIs
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "TOTALMINUTESUNIQUECLIS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("M", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }
                                }

                            }
                            else if (index2 == 3)
                            {
                                for (uint i = 1; i <= 181;)
                                {
                                    InsertValue("O", i, Convert.ToDateTime(itemDates.id).ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                    i += 10;
                                }

                                foreach (var itemCurr in distinctCurr)
                                {
                                    uint iRowIndex = 0;
                                    string currency = itemCurr.id.ToUpper();
                                    if (currency == "GBP")
                                        iRowIndex = 3;
                                    else if (currency == "USD")
                                        iRowIndex = 13;
                                    else if (currency == "EUR")
                                        iRowIndex = 23;
                                    else if (currency == "REST OF THE WORLD")
                                        iRowIndex = 33;
                                    else if (currency == "REST OF THE WORLD MIDDLE EAST")
                                        iRowIndex = 43;
                                    else if (currency == "DKK")
                                        iRowIndex = 53;
                                    else if (currency == "SEK")
                                        iRowIndex = 63;
                                    else if (currency == "CAD")
                                        iRowIndex = 73;
                                    else if (currency == "AUD")
                                        iRowIndex = 83;
                                    else if (currency == "CHF")
                                        iRowIndex = 93;
                                    else if (currency == "SDG")
                                        iRowIndex = 103;
                                    else if (currency == "NZD")
                                        iRowIndex = 113;
                                    else if (currency == "CZK")
                                        iRowIndex = 123;
                                    else if (currency == "HKD")
                                        iRowIndex = 133;
                                    else if (currency == "JYP")
                                        iRowIndex = 143;
                                    else if (currency == "NOK")
                                        iRowIndex = 153;
                                    else if (currency == "RSD")
                                        iRowIndex = 163;
                                    else if (currency == "RUB")
                                        iRowIndex = 173;
                                    else if (currency == "ZAR")
                                        iRowIndex = 183;

                                    //Standard_Minutes
                                    var item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "STANDARD_MINUTES").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("Q", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Free_Credit
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "FREE_CREDIT").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("Q", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Bundle Minutes
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "BUNDLE_MINUTES").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("Q", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }

                                    //Total Minutes & Unique CLIs
                                    iRowIndex += 2;
                                    item2 = records.Where(r => r.date == itemDates.id && r.currcode.ToUpper().Trim() == currency && r.Type.ToUpper().Trim() == "TOTALMINUTESUNIQUECLIS").Select(r => r).ToList();
                                    if (item2 != null && item2.Count() > 0)
                                    {
                                        InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).talktime)), CellValues.Number, wsPart);
                                        InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item2.ElementAt(0).cli)), CellValues.Number, wsPart);
                                        InsertValue("Q", iRowIndex, Convert.ToString(Convert.ToDouble(item2.ElementAt(0).Mins_percentage) / 100), CellValues.Number, wsPart);
                                    }
                                }
                            }
                            index2++;
                        }
                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoDCTariffAutomatedTopupCurrencyReport() : " + ex.Message);
                log.Error("DoDCTariffAutomatedTopupCurrencyReport() : " + ex.Message);
            }
        }
        #endregion

        #region SendMail
        private static void SendMail(string docName)
        {
            try
            {
                //Mail Sending 
                string mailContent = string.Empty;
                string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(';').ToList();
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item));
                }
                log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mail Sending Error : " + ex.Message);
                log.Debug("Mail Sending Error : " + ex.Message);
            }
        }
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, object value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(Convert.ToString(value));
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
