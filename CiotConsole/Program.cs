using Switchlab.Product.BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;

namespace crmServiceConsole
{
	internal class Program
	{
		public Program()
		{
		}

		private void Log(string msg)
		{
			this.Log(msg, string.Empty);
		}

		private void Log(string msg, string prefix)
		{
			string str;
			DateTime dateTime = DateTime.Now;
			if (ConfigurationManager.AppSettings["logFolder"].ToString() == null)
			{
				str = "";
			}
			else
			{
				string str1 = ConfigurationManager.AppSettings["logFolder"].ToString();
				string str2 = dateTime.Year.ToString("0000");
				int month = dateTime.Month;
				str = string.Concat(str1, str2, month.ToString("00"), "\\");
			}
			string directory = str;
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			string[] strArrays = new string[] { directory, "CIOT_Log_", null, null, null };
			strArrays[2] = dateTime.Month.ToString("00");
			strArrays[3] = dateTime.Day.ToString("00");
			strArrays[4] = ".txt";
			StreamWriter writer = new StreamWriter(string.Concat(strArrays), true);
			object[] objArray = new object[] { prefix, dateTime, " : ", msg };
			string text = string.Concat(objArray);
			writer.WriteLine(text);
			writer.Close();
			Console.WriteLine(text);
		}

		private static void Main(string[] args)
		{
			Program prog = new Program();
			string XMLFolder = ConfigurationManager.AppSettings["XMLFolder"].ToString();
			CIOTDetailData oCiotDetail = new CIOTDetailData();
			GenerateXML oGenXML = new GenerateXML();
			int year = DateTime.Now.Year;
			string xmlFile = string.Format("BARABLU_TFN_{0}{1}.xml", year.ToString().Substring(2, 2), oGenXML.GetCounter());
			string sFileName = string.Format("{0}{1}", XMLFolder, xmlFile);
			prog.Log("=================================================");
			prog.Log("XML Generating Process");
			try
			{
				prog.Log("Starting...");
				List<CIOTDetailData> lstDetail = oCiotDetail.GetDetailData();
				prog.Log("Finished getting data. Writing to file xml");
				oGenXML.AddAbonne(lstDetail);
				oGenXML.Save(sFileName);
				if (!File.Exists(sFileName))
				{
					prog.Log("Failed generated xml File");
				}
				else
				{
					prog.Log(string.Concat("File ", sFileName, " has been generated"));
				}
			}
			catch (Exception exception)
			{
				prog.Log(exception.Message);
			}
			prog.Log("Stop...");
			prog.Log("=================================================");
		}
	}
}