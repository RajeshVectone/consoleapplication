﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Switchlab")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © Switchlab 2008")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("crmServiceConsole")]
[assembly: AssemblyTitle("crmServiceConsole")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: Guid("4eebfeca-4a2b-475b-9942-e0a60b0eaa0e")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
