﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace DelightCallingTopUp
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetBeforeFifteenRecord
        public static DCTariffAutomatedTopupCurrencyReport GetBeforeFifteenRecord()
        {
            DCTariffAutomatedTopupCurrencyReport output = new DCTariffAutomatedTopupCurrencyReport();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "DC_Tariff_Automated_Topup_Currency_Report";
                    using (var result = conn.QueryMultiple(sp, new { }, commandType: CommandType.StoredProcedure, commandTimeout: 0))
                    {
                        var prevMonthMain = result.Read<DCTariffAutomatedTopupCurrencyReportPrevMonthMain>().ToList();
                        var prevMonthSub = result.Read<DCTariffAutomatedTopupCurrencyReportPrevMonthSub>().ToList();
                        var currMonthMain = result.Read<DCTariffAutomatedTopupCurrencyReportCurrMonthMain>().ToList();
                        var currMonthSub = result.Read<DCTariffAutomatedTopupCurrencyReportCurrMonthSub>().ToList();
                        output.prevMonthMain = prevMonthMain;
                        output.prevMonthSub = prevMonthSub;
                        output.currMonthMain = currMonthMain;
                        output.currMonthSub = currMonthSub;
                    }
                    return output;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetBeforeFifteenRecord() : " + ex.Message);
                log.Error("GetBeforeFifteenRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetAfterFifteenRecord
        public static DCTariffAutomatedTopupCurrencyReport GetAfterFifteenRecord()
        {
            DCTariffAutomatedTopupCurrencyReport output = new DCTariffAutomatedTopupCurrencyReport();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "DC_Tariff_Automated_Topup_Currency_Report";
                    using (var result = conn.QueryMultiple(sp, new { }, commandType: CommandType.StoredProcedure, commandTimeout: 0))
                    {
                        var prevMonthMain = result.Read<DCTariffAutomatedTopupCurrencyReportPrevMonthMain>().ToList();
                        var prevMonthSub = result.Read<DCTariffAutomatedTopupCurrencyReportPrevMonthSub>().ToList();
                        var currMonthMain = result.Read<DCTariffAutomatedTopupCurrencyReportCurrMonthMain>().ToList();
                        var currMonthSub = result.Read<DCTariffAutomatedTopupCurrencyReportCurrMonthSub>().ToList();
                        output.currMonthMain = currMonthMain;
                        output.currMonthSub = currMonthSub;
                    }
                    return output;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetAfterFifteenRecord() : " + ex.Message);
                log.Error("GetAfterFifteenRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetCurrentMonthSubRecord
        public static DCTariffAutomatedTopupCurrencySubReport GetCurrentMonthSubRecord()
        {
            DCTariffAutomatedTopupCurrencySubReport output = new DCTariffAutomatedTopupCurrencySubReport();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_DC_topup_currency_totalcount";
                    var result = conn.Query<DCTariffAutomatedTopupCurrencySubReport>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        output.GBP_amount = result.ElementAt(0).GBP_amount;
                        output.GBP_topup = result.ElementAt(0).GBP_topup;
                        output.USD_amount = result.ElementAt(0).USD_amount;
                        output.USD_topup = result.ElementAt(0).USD_topup;
                        output.EUR_amount = result.ElementAt(0).EUR_amount;
                        output.EUR_topup = result.ElementAt(0).EUR_topup;
                        output.SEK_amount = result.ElementAt(0).SEK_amount;
                        output.SEK_topup = result.ElementAt(0).SEK_topup;
                        output.HKD_amount = result.ElementAt(0).HKD_amount;
                        output.HKD_topup = result.ElementAt(0).HKD_topup;
                        output.CAD_amount = result.ElementAt(0).CAD_amount;
                        output.CAD_topup = result.ElementAt(0).CAD_topup;
                        output.AUD_amount = result.ElementAt(0).AUD_amount;
                        output.AUD_topup = result.ElementAt(0).AUD_topup;
                        output.CHF_amount = result.ElementAt(0).CHF_amount;
                        output.CHF_topup = result.ElementAt(0).CHF_topup;
                        output.CZK_amount = result.ElementAt(0).CZK_amount;
                        output.CZK_topup = result.ElementAt(0).CZK_topup;
                        output.DKK_amount = result.ElementAt(0).DKK_amount;
                        output.DKK_topup = result.ElementAt(0).DKK_topup;
                        output.JYP_amount = result.ElementAt(0).JYP_amount;
                        output.JYP_topup = result.ElementAt(0).JYP_topup;
                        output.NOK_amount = result.ElementAt(0).NOK_amount;
                        output.NOK_topup = result.ElementAt(0).NOK_topup;
                        output.NZD_amount = result.ElementAt(0).NZD_amount;
                        output.NZD_topup = result.ElementAt(0).NZD_topup;
                        output.RSD_amount = result.ElementAt(0).RSD_amount;
                        output.RSD_topup = result.ElementAt(0).RSD_topup;
                        output.RUB_amount = result.ElementAt(0).RUB_amount;
                        output.RUB_topup = result.ElementAt(0).RUB_topup;
                        output.SGD_amount = result.ElementAt(0).SGD_amount;
                        output.SGD_topup = result.ElementAt(0).SGD_topup;
                        output.ZAR_amount = result.ElementAt(0).ZAR_amount;
                        output.ZAR_topup = result.ElementAt(0).ZAR_topup;
                        output.Total_amount = result.ElementAt(0).Total_amount;
                        output.Total_topup = result.ElementAt(0).Total_topup;
                    }
                    return output;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetCurrentMonthSubRecord() : " + ex.Message);
                log.Error("GetCurrentMonthSubRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}
