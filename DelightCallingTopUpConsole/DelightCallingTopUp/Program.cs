﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace DelightCallingTopUp
{
    #region DCTariffAutomatedTopupCurrencyReport

    public class DCTariffAutomatedTopupCurrencyReport
    {
        public List<DCTariffAutomatedTopupCurrencyReportPrevMonthMain> prevMonthMain { get; set; }
        public List<DCTariffAutomatedTopupCurrencyReportPrevMonthSub> prevMonthSub { get; set; }
        public List<DCTariffAutomatedTopupCurrencyReportCurrMonthMain> currMonthMain { get; set; }
        public List<DCTariffAutomatedTopupCurrencyReportCurrMonthSub> currMonthSub { get; set; }
    }

    public class DCTariffAutomatedTopupCurrencyReportPrevMonthMain
    {
        public string currcode { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public int Nooftopup { get; set; }
    }

    public class DCTariffAutomatedTopupCurrencyReportPrevMonthSub
    {
        public string Prev_month { get; set; }
        public string currcode { get; set; }
        public int Amount { get; set; }
        public int Nooftopup { get; set; }
        public double Total_amount { get; set; }
        public int Total_topup { get; set; }
    }

    public class DCTariffAutomatedTopupCurrencyReportCurrMonthMain
    {
        public string currcode { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public int Nooftopup { get; set; }
    }

    public class DCTariffAutomatedTopupCurrencyReportCurrMonthSub
    {
        public string Current_month { get; set; }
        public string currcode { get; set; }
        public int Amount { get; set; }
        public int Nooftopup { get; set; }
        public double Total_amount { get; set; }
        public int Total_topup { get; set; }
    }
    #endregion

    #region DCTariffAutomatedTopupCurrencySubReport
    public class DCTariffAutomatedTopupCurrencySubReport
    {
        public int GBP_amount { get; set; }
        public int GBP_topup { get; set; }
        public int USD_amount { get; set; }
        public int USD_topup { get; set; }
        public int EUR_amount { get; set; }
        public int EUR_topup { get; set; }
        public int SEK_amount { get; set; }
        public int SEK_topup { get; set; }
        public int HKD_amount { get; set; }
        public int HKD_topup { get; set; }
        public int CAD_amount { get; set; }
        public int CAD_topup { get; set; }
        public int AUD_amount { get; set; }
        public int AUD_topup { get; set; }
        public int CHF_amount { get; set; }
        public int CHF_topup { get; set; }
        public int CZK_amount { get; set; }
        public int CZK_topup { get; set; }
        public int DKK_amount { get; set; }
        public int DKK_topup { get; set; }
        public int JYP_amount { get; set; }
        public int JYP_topup { get; set; }
        public int NOK_amount { get; set; }
        public int NOK_topup { get; set; }
        public int NZD_amount { get; set; }
        public int NZD_topup { get; set; }
        public int RSD_amount { get; set; }
        public int RSD_topup { get; set; }
        public int RUB_amount { get; set; }
        public int RUB_topup { get; set; }
        public int SGD_amount { get; set; }
        public int SGD_topup { get; set; }
        public int ZAR_amount { get; set; }
        public int ZAR_topup { get; set; }
        public int Total_amount { get; set; }
        public int Total_topup { get; set; }
    }
    #endregion

    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                if (DateTime.Now.Day < 16)
                {
                    string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["BeforeFifteenTemplateFile"]);
                    string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                    File.Copy(templateName, docName);

                    Console.WriteLine("docName : " + docName);
                    log.Info("docName : " + docName);

                    DoBeforeFifteenProcess(docName);

                    SendMail(docName);
                }
                else
                {
                    string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AfterFifteenTemplateFile"]);
                    string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                    File.Copy(templateName, docName);

                    Console.WriteLine("docName : " + docName);
                    log.Info("docName : " + docName);

                    DoAfterFifteenProcess(docName);

                    SendMail(docName);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region DoBeforeFifteenProcess
        static void DoBeforeFifteenProcess(string docName)
        {
            Console.WriteLine("DoBeforeFifteenProcess");
            log.Info("DoBeforeFifteenProcess");
            try
            {
                var records = DataAccess.GetBeforeFifteenRecord();
                if (records != null)
                {
                    Console.WriteLine("No of records : " + 1);
                    log.Info("No of records : " + 1);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "DelightCallingTopUp").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        //Previous Month
                        uint iRowIndex = 4;
                        int grpByCountPrev = 4;

                        var prevMonthMain = records.prevMonthMain;
                        if (prevMonthMain != null && prevMonthMain.Count > 0)
                        {
                            var grpBy = prevMonthMain.GroupBy(g => g.Date);
                            grpByCountPrev = grpBy.Count();

                            Console.WriteLine("prevMonthMain Date Count : " + grpByCountPrev);
                            log.Info("prevMonthMain Date Count : " + grpByCountPrev);

                            foreach (var items in grpBy)
                            {
                                InsertValue("A", iRowIndex, items.ElementAt(0).Date.ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", iRowIndex + 58, items.ElementAt(0).Date.ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                uint colIndex = 0;
                                foreach (var item in items)
                                {
                                    string cnAmount = "";
                                    string cnNoOfTopup = "";
                                    switch (item.currcode.ToUpper())
                                    {
                                        case "GBP":
                                            cnAmount = "B";
                                            cnNoOfTopup = "C";
                                            colIndex = 0;
                                            break;
                                        case "USD":
                                            cnAmount = "D";
                                            cnNoOfTopup = "E";
                                            colIndex = 0;
                                            break;
                                        case "EUR":
                                            cnAmount = "F";
                                            cnNoOfTopup = "G";
                                            colIndex = 0;
                                            break;
                                        case "SEK":
                                            cnAmount = "H";
                                            cnNoOfTopup = "I";
                                            colIndex = 0;
                                            break;
                                        case "HKD":
                                            cnAmount = "J";
                                            cnNoOfTopup = "K";
                                            colIndex = 0;
                                            break;
                                        case "CAD":
                                            cnAmount = "L";
                                            cnNoOfTopup = "M";
                                            colIndex = 0;
                                            break;
                                        case "AUD":
                                            cnAmount = "N";
                                            cnNoOfTopup = "O";
                                            colIndex = 0;
                                            break;
                                        case "CHF":
                                            cnAmount = "P";
                                            cnNoOfTopup = "Q";
                                            colIndex = 0;
                                            break;
                                        case "CZK":
                                            cnAmount = "R";
                                            cnNoOfTopup = "S";
                                            colIndex = 0;
                                            break;
                                        case "DKK":
                                            cnAmount = "B";
                                            cnNoOfTopup = "C";
                                            colIndex = 58;
                                            break;
                                        case "JYP":
                                            cnAmount = "D";
                                            cnNoOfTopup = "E";
                                            colIndex = 58;
                                            break;
                                        case "NOK":
                                            cnAmount = "F";
                                            cnNoOfTopup = "G";
                                            colIndex = 58;
                                            break;
                                        case "NZD":
                                            cnAmount = "H";
                                            cnNoOfTopup = "I";
                                            colIndex = 58;
                                            break;
                                        case "RSD":
                                            cnAmount = "J";
                                            cnNoOfTopup = "K";
                                            colIndex = 58;
                                            break;
                                        case "RUB":
                                            cnAmount = "L";
                                            cnNoOfTopup = "M";
                                            colIndex = 58;
                                            break;
                                        case "SGD":
                                            cnAmount = "N";
                                            cnNoOfTopup = "O";
                                            colIndex = 58;
                                            break;
                                        case "ZAR":
                                            cnAmount = "P";
                                            cnNoOfTopup = "Q";
                                            colIndex = 58;
                                            break;
                                        default:
                                            break;
                                    }
                                    InsertValue(cnAmount, iRowIndex + colIndex, Convert.ToInt32(item.Amount), CellValues.Number, wsPart);
                                    InsertValue(cnNoOfTopup, iRowIndex + colIndex, Convert.ToInt32(item.Nooftopup), CellValues.Number, wsPart);
                                }
                                iRowIndex++;
                            }
                        }
                        else
                        {
                            log.Info("prevMonthMain is empty");
                        }

                        var prevMonthSub = records.prevMonthSub;
                        if (prevMonthSub != null && prevMonthSub.Count > 0)
                        {
                            uint iSubRowIndex = 20;
                            for (int index = 0; index < prevMonthSub.Count; index++)
                            {
                                switch (prevMonthSub.ElementAt(index).currcode.ToUpper())
                                {
                                    case "GBP":
                                        iSubRowIndex = 20;
                                        InsertValue("B", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("C", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "USD":
                                        iSubRowIndex = 20;
                                        InsertValue("D", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("E", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "EUR":
                                        iSubRowIndex = 20;
                                        InsertValue("F", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("G", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "SEK":
                                        iSubRowIndex = 20;
                                        InsertValue("H", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("I", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "HKD":
                                        iSubRowIndex = 20;
                                        InsertValue("J", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("K", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CAD":
                                        iSubRowIndex = 20;
                                        InsertValue("L", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("M", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "AUD":
                                        iSubRowIndex = 20;
                                        InsertValue("N", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("O", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CHF":
                                        iSubRowIndex = 20;
                                        InsertValue("P", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("Q", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CZK":
                                        iSubRowIndex = 20;
                                        InsertValue("R", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("S", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "DKK":
                                        iSubRowIndex = 78;
                                        InsertValue("B", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("C", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "JYP":
                                        iSubRowIndex = 78;
                                        InsertValue("D", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("E", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "NOK":
                                        iSubRowIndex = 78;
                                        InsertValue("F", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("G", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "NZD":
                                        iSubRowIndex = 78;
                                        InsertValue("H", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("I", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "RSD":
                                        iSubRowIndex = 78;
                                        InsertValue("J", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("K", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "RUB":
                                        iSubRowIndex = 78;
                                        InsertValue("L", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("M", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "SGD":
                                        iSubRowIndex = 78;
                                        InsertValue("N", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("O", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "ZAR":
                                        iSubRowIndex = 78;
                                        InsertValue("P", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("Q", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    default:
                                        continue;
                                }
                            }

                            iSubRowIndex = 78;
                            InsertValue("R", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(0).Total_amount), CellValues.Number, wsPart);
                            InsertValue("S", iSubRowIndex, Convert.ToInt32(prevMonthSub.ElementAt(0).Total_topup), CellValues.Number, wsPart);
                        }
                        else
                        {
                            log.Info("prevMonthSub is empty");
                        }

                        InsertValue("A", 20, DateTime.Now.AddMonths(-1).ToString("MMM") + "'" + DateTime.Now.AddMonths(-1).ToString("yy"), CellValues.String, wsPart);
                        InsertValue("A", 78, DateTime.Now.AddMonths(-1).ToString("MMM") + "'" + DateTime.Now.AddMonths(-1).ToString("yy"), CellValues.String, wsPart);

                        //Current Month
                        iRowIndex = 22;
                        int grpByCountCurr = 22;

                        var currMonthMain = records.currMonthMain;
                        if (currMonthMain != null && currMonthMain.Count > 0)
                        {
                            var grpBy = currMonthMain.GroupBy(g => g.Date);
                            grpByCountCurr = grpBy.Count();

                            Console.WriteLine("currMonthMain Date Count : " + grpByCountCurr);
                            log.Info("currMonthMain Date Count : " + grpByCountCurr);

                            foreach (var items in grpBy)
                            {
                                InsertValue("A", iRowIndex, items.ElementAt(0).Date.ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", iRowIndex + 58, items.ElementAt(0).Date.ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                uint colIndex = 0;
                                foreach (var item in items)
                                {
                                    string cnAmount = "";
                                    string cnNoOfTopup = "";
                                    switch (item.currcode.ToUpper())
                                    {
                                        case "GBP":
                                            cnAmount = "B";
                                            cnNoOfTopup = "C";
                                            colIndex = 0;
                                            break;
                                        case "USD":
                                            cnAmount = "D";
                                            cnNoOfTopup = "E";
                                            colIndex = 0;
                                            break;
                                        case "EUR":
                                            cnAmount = "F";
                                            cnNoOfTopup = "G";
                                            colIndex = 0;
                                            break;
                                        case "SEK":
                                            cnAmount = "H";
                                            cnNoOfTopup = "I";
                                            colIndex = 0;
                                            break;
                                        case "HKD":
                                            cnAmount = "J";
                                            cnNoOfTopup = "K";
                                            colIndex = 0;
                                            break;
                                        case "CAD":
                                            cnAmount = "L";
                                            cnNoOfTopup = "M";
                                            colIndex = 0;
                                            break;
                                        case "AUD":
                                            cnAmount = "N";
                                            cnNoOfTopup = "O";
                                            colIndex = 0;
                                            break;
                                        case "CHF":
                                            cnAmount = "P";
                                            cnNoOfTopup = "Q";
                                            colIndex = 0;
                                            break;
                                        case "CZK":
                                            cnAmount = "R";
                                            cnNoOfTopup = "S";
                                            colIndex = 0;
                                            break;
                                        case "DKK":
                                            cnAmount = "B";
                                            cnNoOfTopup = "C";
                                            colIndex = 58;
                                            break;
                                        case "JYP":
                                            cnAmount = "D";
                                            cnNoOfTopup = "E";
                                            colIndex = 58;
                                            break;
                                        case "NOK":
                                            cnAmount = "F";
                                            cnNoOfTopup = "G";
                                            colIndex = 58;
                                            break;
                                        case "NZD":
                                            cnAmount = "H";
                                            cnNoOfTopup = "I";
                                            colIndex = 58;
                                            break;
                                        case "RSD":
                                            cnAmount = "J";
                                            cnNoOfTopup = "K";
                                            colIndex = 58;
                                            break;
                                        case "RUB":
                                            cnAmount = "L";
                                            cnNoOfTopup = "M";
                                            colIndex = 58;
                                            break;
                                        case "SGD":
                                            cnAmount = "N";
                                            cnNoOfTopup = "O";
                                            colIndex = 58;
                                            break;
                                        case "ZAR":
                                            cnAmount = "P";
                                            cnNoOfTopup = "Q";
                                            colIndex = 58;
                                            break;
                                        default:
                                            break;
                                    }
                                    InsertValue(cnAmount, iRowIndex + colIndex, Convert.ToInt32(item.Amount), CellValues.Number, wsPart);
                                    InsertValue(cnNoOfTopup, iRowIndex + colIndex, Convert.ToInt32(item.Nooftopup), CellValues.Number, wsPart);
                                }
                                iRowIndex++;
                            }
                        }
                        else
                        {
                            log.Info("currMonthMain is empty");
                        }

                        var currMonthSub = records.currMonthSub;
                        if (currMonthSub != null && currMonthSub.Count > 0)
                        {
                            uint iSubRowIndex = 54;
                            for (int index = 0; index < currMonthSub.Count; index++)
                            {
                                switch (currMonthSub.ElementAt(index).currcode.ToUpper())
                                {
                                    case "GBP":
                                        iSubRowIndex = 54;
                                        InsertValue("B", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("C", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "USD":
                                        iSubRowIndex = 54;
                                        InsertValue("D", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("E", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "EUR":
                                        iSubRowIndex = 54;
                                        InsertValue("F", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("G", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "SEK":
                                        iSubRowIndex = 54;
                                        InsertValue("H", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("I", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "HKD":
                                        iSubRowIndex = 54;
                                        InsertValue("J", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("K", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CAD":
                                        iSubRowIndex = 54;
                                        InsertValue("L", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("M", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "AUD":
                                        iSubRowIndex = 54;
                                        InsertValue("N", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("O", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CHF":
                                        iSubRowIndex = 54;
                                        InsertValue("P", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("Q", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CZK":
                                        iSubRowIndex = 54;
                                        InsertValue("R", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("S", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "DKK":
                                        iSubRowIndex = 112;
                                        InsertValue("B", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("C", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "JYP":
                                        iSubRowIndex = 112;
                                        InsertValue("D", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("E", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "NOK":
                                        iSubRowIndex = 112;
                                        InsertValue("F", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("G", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "NZD":
                                        iSubRowIndex = 112;
                                        InsertValue("H", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("I", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "RSD":
                                        iSubRowIndex = 112;
                                        InsertValue("J", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("K", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "RUB":
                                        iSubRowIndex = 112;
                                        InsertValue("L", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("M", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "SGD":
                                        iSubRowIndex = 112;
                                        InsertValue("N", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("O", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "ZAR":
                                        iSubRowIndex = 112;
                                        InsertValue("P", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("Q", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    default:
                                        continue;
                                }
                            }

                            iSubRowIndex = 112;
                            InsertValue("R", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(0).Total_amount), CellValues.Number, wsPart);
                            InsertValue("S", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(0).Total_topup), CellValues.Number, wsPart);
                        }
                        else
                        {
                            log.Info("currMonthSub is empty");
                        }

                        InsertValue("A", 54, DateTime.Now.ToString("MMM") + "'" + DateTime.Now.ToString("yy"), CellValues.String, wsPart);
                        InsertValue("A", 112, DateTime.Now.ToString("MMM") + "'" + DateTime.Now.ToString("yy"), CellValues.String, wsPart);

                        DCTariffAutomatedTopupCurrencySubReport subReport = DataAccess.GetCurrentMonthSubRecord();
                        if (subReport != null)
                        {
                            uint iSubRowIndex = 56;
                            InsertValue("B", iSubRowIndex, Convert.ToInt32(subReport.GBP_amount), CellValues.Number, wsPart);
                            InsertValue("C", iSubRowIndex, Convert.ToInt32(subReport.GBP_topup), CellValues.Number, wsPart);
                            InsertValue("D", iSubRowIndex, Convert.ToInt32(subReport.USD_amount), CellValues.Number, wsPart);
                            InsertValue("E", iSubRowIndex, Convert.ToInt32(subReport.USD_topup), CellValues.Number, wsPart);
                            InsertValue("F", iSubRowIndex, Convert.ToInt32(subReport.EUR_amount), CellValues.Number, wsPart);
                            InsertValue("G", iSubRowIndex, Convert.ToInt32(subReport.EUR_topup), CellValues.Number, wsPart);
                            InsertValue("H", iSubRowIndex, Convert.ToInt32(subReport.SEK_amount), CellValues.Number, wsPart);
                            InsertValue("I", iSubRowIndex, Convert.ToInt32(subReport.SEK_topup), CellValues.Number, wsPart);
                            InsertValue("J", iSubRowIndex, Convert.ToInt32(subReport.HKD_amount), CellValues.Number, wsPart);
                            InsertValue("K", iSubRowIndex, Convert.ToInt32(subReport.HKD_topup), CellValues.Number, wsPart);
                            InsertValue("L", iSubRowIndex, Convert.ToInt32(subReport.CAD_amount), CellValues.Number, wsPart);
                            InsertValue("M", iSubRowIndex, Convert.ToInt32(subReport.CAD_topup), CellValues.Number, wsPart);
                            InsertValue("N", iSubRowIndex, Convert.ToInt32(subReport.AUD_amount), CellValues.Number, wsPart);
                            InsertValue("O", iSubRowIndex, Convert.ToInt32(subReport.AUD_topup), CellValues.Number, wsPart);
                            InsertValue("P", iSubRowIndex, Convert.ToInt32(subReport.CHF_amount), CellValues.Number, wsPart);
                            InsertValue("Q", iSubRowIndex, Convert.ToInt32(subReport.CHF_topup), CellValues.Number, wsPart);
                            InsertValue("R", iSubRowIndex, Convert.ToInt32(subReport.CZK_amount), CellValues.Number, wsPart);
                            InsertValue("S", iSubRowIndex, Convert.ToInt32(subReport.CZK_topup), CellValues.Number, wsPart);

                            iSubRowIndex = 114;
                            InsertValue("B", iSubRowIndex, Convert.ToInt32(subReport.DKK_amount), CellValues.Number, wsPart);
                            InsertValue("C", iSubRowIndex, Convert.ToInt32(subReport.DKK_topup), CellValues.Number, wsPart);
                            InsertValue("D", iSubRowIndex, Convert.ToInt32(subReport.JYP_amount), CellValues.Number, wsPart);
                            InsertValue("E", iSubRowIndex, Convert.ToInt32(subReport.JYP_topup), CellValues.Number, wsPart);
                            InsertValue("F", iSubRowIndex, Convert.ToInt32(subReport.NOK_amount), CellValues.Number, wsPart);
                            InsertValue("G", iSubRowIndex, Convert.ToInt32(subReport.NOK_topup), CellValues.Number, wsPart);
                            InsertValue("H", iSubRowIndex, Convert.ToInt32(subReport.NZD_amount), CellValues.Number, wsPart);
                            InsertValue("I", iSubRowIndex, Convert.ToInt32(subReport.NZD_topup), CellValues.Number, wsPart);
                            InsertValue("J", iSubRowIndex, Convert.ToInt32(subReport.RSD_amount), CellValues.Number, wsPart);
                            InsertValue("K", iSubRowIndex, Convert.ToInt32(subReport.RSD_topup), CellValues.Number, wsPart);
                            InsertValue("L", iSubRowIndex, Convert.ToInt32(subReport.RUB_amount), CellValues.Number, wsPart);
                            InsertValue("M", iSubRowIndex, Convert.ToInt32(subReport.RUB_topup), CellValues.Number, wsPart);
                            InsertValue("N", iSubRowIndex, Convert.ToInt32(subReport.SGD_amount), CellValues.Number, wsPart);
                            InsertValue("O", iSubRowIndex, Convert.ToInt32(subReport.SGD_topup), CellValues.Number, wsPart);
                            InsertValue("P", iSubRowIndex, Convert.ToInt32(subReport.ZAR_amount), CellValues.Number, wsPart);
                            InsertValue("Q", iSubRowIndex, Convert.ToInt32(subReport.ZAR_topup), CellValues.Number, wsPart);
                            InsertValue("R", iSubRowIndex, Convert.ToInt32(subReport.Total_amount), CellValues.Number, wsPart);
                            InsertValue("S", iSubRowIndex, Convert.ToInt32(subReport.Total_topup), CellValues.Number, wsPart);
                        }
                        else
                        {
                            log.Info("subReport is empty");
                        }

                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        //Remove Rows Sub
                        for (int i = 110; i >= 80 + grpByCountCurr; i--)
                        {
                            Worksheet worksheet = wsPart.Worksheet;
                            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                            if (sheetData.Elements<Row>().Where(r => r.RowIndex == i).Count() != 0)
                            {
                                Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == i).First();
                                if (row != null)
                                {
                                    row.Hidden = true;
                                }
                            }
                        }

                        for (int i = 76; i >= 62 + grpByCountPrev; i--)
                        {
                            Worksheet worksheet = wsPart.Worksheet;
                            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                            if (sheetData.Elements<Row>().Where(r => r.RowIndex == i).Count() != 0)
                            {
                                Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == i).First();
                                if (row != null)
                                {
                                    row.Hidden = true;
                                }
                            }
                        }

                        //Remove Rows Main
                        for (int i = 52; i >= 22 + grpByCountCurr; i--)
                        {
                            Worksheet worksheet = wsPart.Worksheet;
                            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                            if (sheetData.Elements<Row>().Where(r => r.RowIndex == i).Count() != 0)
                            {
                                Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == i).First();
                                if (row != null)
                                {
                                    row.Hidden = true;
                                }
                            }
                        }

                        for (int i = 18; i >= 4 + grpByCountPrev; i--)
                        {
                            Worksheet worksheet = wsPart.Worksheet;
                            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                            if (sheetData.Elements<Row>().Where(r => r.RowIndex == i).Count() != 0)
                            {
                                Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == i).First();
                                if (row != null)
                                {
                                    row.Hidden = true;
                                }
                            }
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoBeforeFifteenProcess : " + ex.Message);
                log.Error("DoBeforeFifteenProcess : " + ex.Message);
            }
        }
        #endregion

        #region DoAfterFifteenProcess
        static void DoAfterFifteenProcess(string docName)
        {
            Console.WriteLine("DoAfterFifteenProcess");
            log.Info("DoAfterFifteenProcess");
            try
            {
                var records = DataAccess.GetAfterFifteenRecord();
                if (records != null)
                {
                    Console.WriteLine("No of records : " + 1);
                    log.Info("No of records : " + 1);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "DelightCallingTopUp").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        //Current Month
                        uint iRowIndex = 4;
                        int grpByCount = 4;
                        var currMonthMain = records.currMonthMain;
                        if (currMonthMain != null && currMonthMain.Count > 0)
                        {
                            var grpBy = currMonthMain.GroupBy(g => g.Date);
                            grpByCount = grpBy.Count();

                            Console.WriteLine("currMonthMain Date Count : " + grpByCount);
                            log.Info("currMonthMain Date Count : " + grpByCount);

                            foreach (var items in grpBy)
                            {
                                InsertValue("A", iRowIndex, items.ElementAt(0).Date.ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                InsertValue("A", iRowIndex + 40, items.ElementAt(0).Date.ToString("dd-MMM-yyyy"), CellValues.String, wsPart);
                                uint colIndex = 0;
                                foreach (var item in items)
                                {
                                    string cnAmount = "";
                                    string cnNoOfTopup = "";
                                    switch (item.currcode.ToUpper())
                                    {
                                        case "GBP":
                                            cnAmount = "B";
                                            cnNoOfTopup = "C";
                                            colIndex = 0;
                                            break;
                                        case "USD":
                                            cnAmount = "D";
                                            cnNoOfTopup = "E";
                                            colIndex = 0;
                                            break;
                                        case "EUR":
                                            cnAmount = "F";
                                            cnNoOfTopup = "G";
                                            colIndex = 0;
                                            break;
                                        case "SEK":
                                            cnAmount = "H";
                                            cnNoOfTopup = "I";
                                            colIndex = 0;
                                            break;
                                        case "HKD":
                                            cnAmount = "J";
                                            cnNoOfTopup = "K";
                                            colIndex = 0;
                                            break;
                                        case "CAD":
                                            cnAmount = "L";
                                            cnNoOfTopup = "M";
                                            colIndex = 0;
                                            break;
                                        case "AUD":
                                            cnAmount = "N";
                                            cnNoOfTopup = "O";
                                            colIndex = 0;
                                            break;
                                        case "CHF":
                                            cnAmount = "P";
                                            cnNoOfTopup = "Q";
                                            colIndex = 0;
                                            break;
                                        case "CZK":
                                            cnAmount = "R";
                                            cnNoOfTopup = "S";
                                            colIndex = 0;
                                            break;
                                        case "DKK":
                                            cnAmount = "B";
                                            cnNoOfTopup = "C";
                                            colIndex = 40;
                                            break;
                                        case "JYP":
                                            cnAmount = "D";
                                            cnNoOfTopup = "E";
                                            colIndex = 40;
                                            break;
                                        case "NOK":
                                            cnAmount = "F";
                                            cnNoOfTopup = "G";
                                            colIndex = 40;
                                            break;
                                        case "NZD":
                                            cnAmount = "H";
                                            cnNoOfTopup = "I";
                                            colIndex = 40;
                                            break;
                                        case "RSD":
                                            cnAmount = "J";
                                            cnNoOfTopup = "K";
                                            colIndex = 40;
                                            break;
                                        case "RUB":
                                            cnAmount = "L";
                                            cnNoOfTopup = "M";
                                            colIndex = 40;
                                            break;
                                        case "SGD":
                                            cnAmount = "N";
                                            cnNoOfTopup = "O";
                                            colIndex = 40;
                                            break;
                                        case "ZAR":
                                            cnAmount = "P";
                                            cnNoOfTopup = "Q";
                                            colIndex = 40;
                                            break;
                                        default:
                                            break;
                                    }
                                    InsertValue(cnAmount, iRowIndex + colIndex, Convert.ToInt32(item.Amount), CellValues.Number, wsPart);
                                    InsertValue(cnNoOfTopup, iRowIndex + colIndex, Convert.ToInt32(item.Nooftopup), CellValues.Number, wsPart);
                                }
                                iRowIndex++;
                            }
                        }
                        else
                        {
                            log.Info("currMonthMain is empty");
                        }

                        var currMonthSub = records.currMonthSub;
                        if (currMonthSub != null && currMonthSub.Count > 0)
                        {
                            uint iSubRowIndex = 36;
                            for (int index = 0; index < currMonthSub.Count; index++)
                            {
                                switch (currMonthSub.ElementAt(index).currcode.ToUpper())
                                {
                                    case "GBP":
                                        iSubRowIndex = 36;
                                        InsertValue("B", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("C", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "USD":
                                        iSubRowIndex = 36;
                                        InsertValue("D", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("E", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "EUR":
                                        iSubRowIndex = 36;
                                        InsertValue("F", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("G", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "SEK":
                                        iSubRowIndex = 36;
                                        InsertValue("H", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("I", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "HKD":
                                        iSubRowIndex = 36;
                                        InsertValue("J", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("K", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CAD":
                                        iSubRowIndex = 36;
                                        InsertValue("L", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("M", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "AUD":
                                        iSubRowIndex = 36;
                                        InsertValue("N", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("O", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CHF":
                                        iSubRowIndex = 36;
                                        InsertValue("P", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("Q", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "CZK":
                                        iSubRowIndex = 36;
                                        InsertValue("R", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("S", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "DKK":
                                        iSubRowIndex = 76;
                                        InsertValue("B", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("C", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "JYP":
                                        iSubRowIndex = 76;
                                        InsertValue("D", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("E", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "NOK":
                                        iSubRowIndex = 76;
                                        InsertValue("F", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("G", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "NZD":
                                        iSubRowIndex = 76;
                                        InsertValue("H", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("I", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "RSD":
                                        iSubRowIndex = 76;
                                        InsertValue("J", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("K", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "RUB":
                                        iSubRowIndex = 76;
                                        InsertValue("L", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("M", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "SGD":
                                        iSubRowIndex = 76;
                                        InsertValue("N", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("O", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    case "ZAR":
                                        iSubRowIndex = 76;
                                        InsertValue("P", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Amount), CellValues.Number, wsPart);
                                        InsertValue("Q", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(index).Nooftopup), CellValues.Number, wsPart);
                                        break;
                                    default:
                                        continue;
                                }
                            }

                            iSubRowIndex = 76;
                            InsertValue("R", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(0).Total_amount), CellValues.Number, wsPart);
                            InsertValue("S", iSubRowIndex, Convert.ToInt32(currMonthSub.ElementAt(0).Total_topup), CellValues.Number, wsPart);
                        }
                        else
                        {
                            log.Info("currMonthSub is empty");
                        }

                        InsertValue("A", 36, DateTime.Now.AddDays(-1).ToString("MMM") + "'" + DateTime.Now.AddDays(-1).ToString("yy"), CellValues.String, wsPart);
                        InsertValue("A", 76, DateTime.Now.AddDays(-1).ToString("MMM") + "'" + DateTime.Now.AddDays(-1).ToString("yy"), CellValues.String, wsPart);

                        DCTariffAutomatedTopupCurrencySubReport subReport = DataAccess.GetCurrentMonthSubRecord();
                        if (subReport != null)
                        {
                            uint iSubRowIndex = 38;
                            InsertValue("B", iSubRowIndex, Convert.ToInt32(subReport.GBP_amount), CellValues.Number, wsPart);
                            InsertValue("C", iSubRowIndex, Convert.ToInt32(subReport.GBP_topup), CellValues.Number, wsPart);
                            InsertValue("D", iSubRowIndex, Convert.ToInt32(subReport.USD_amount), CellValues.Number, wsPart);
                            InsertValue("E", iSubRowIndex, Convert.ToInt32(subReport.USD_topup), CellValues.Number, wsPart);
                            InsertValue("F", iSubRowIndex, Convert.ToInt32(subReport.EUR_amount), CellValues.Number, wsPart);
                            InsertValue("G", iSubRowIndex, Convert.ToInt32(subReport.EUR_topup), CellValues.Number, wsPart);
                            InsertValue("H", iSubRowIndex, Convert.ToInt32(subReport.SEK_amount), CellValues.Number, wsPart);
                            InsertValue("I", iSubRowIndex, Convert.ToInt32(subReport.SEK_topup), CellValues.Number, wsPart);
                            InsertValue("J", iSubRowIndex, Convert.ToInt32(subReport.HKD_amount), CellValues.Number, wsPart);
                            InsertValue("K", iSubRowIndex, Convert.ToInt32(subReport.HKD_topup), CellValues.Number, wsPart);
                            InsertValue("L", iSubRowIndex, Convert.ToInt32(subReport.CAD_amount), CellValues.Number, wsPart);
                            InsertValue("M", iSubRowIndex, Convert.ToInt32(subReport.CAD_topup), CellValues.Number, wsPart);
                            InsertValue("N", iSubRowIndex, Convert.ToInt32(subReport.AUD_amount), CellValues.Number, wsPart);
                            InsertValue("O", iSubRowIndex, Convert.ToInt32(subReport.AUD_topup), CellValues.Number, wsPart);
                            InsertValue("P", iSubRowIndex, Convert.ToInt32(subReport.CHF_amount), CellValues.Number, wsPart);
                            InsertValue("Q", iSubRowIndex, Convert.ToInt32(subReport.CHF_topup), CellValues.Number, wsPart);
                            InsertValue("R", iSubRowIndex, Convert.ToInt32(subReport.CZK_amount), CellValues.Number, wsPart);
                            InsertValue("S", iSubRowIndex, Convert.ToInt32(subReport.CZK_topup), CellValues.Number, wsPart);

                            iSubRowIndex = 78;
                            InsertValue("B", iSubRowIndex, Convert.ToInt32(subReport.DKK_amount), CellValues.Number, wsPart);
                            InsertValue("C", iSubRowIndex, Convert.ToInt32(subReport.DKK_topup), CellValues.Number, wsPart);
                            InsertValue("D", iSubRowIndex, Convert.ToInt32(subReport.JYP_amount), CellValues.Number, wsPart);
                            InsertValue("E", iSubRowIndex, Convert.ToInt32(subReport.JYP_topup), CellValues.Number, wsPart);
                            InsertValue("F", iSubRowIndex, Convert.ToInt32(subReport.NOK_amount), CellValues.Number, wsPart);
                            InsertValue("G", iSubRowIndex, Convert.ToInt32(subReport.NOK_topup), CellValues.Number, wsPart);
                            InsertValue("H", iSubRowIndex, Convert.ToInt32(subReport.NZD_amount), CellValues.Number, wsPart);
                            InsertValue("I", iSubRowIndex, Convert.ToInt32(subReport.NZD_topup), CellValues.Number, wsPart);
                            InsertValue("J", iSubRowIndex, Convert.ToInt32(subReport.RSD_amount), CellValues.Number, wsPart);
                            InsertValue("K", iSubRowIndex, Convert.ToInt32(subReport.RSD_topup), CellValues.Number, wsPart);
                            InsertValue("L", iSubRowIndex, Convert.ToInt32(subReport.RUB_amount), CellValues.Number, wsPart);
                            InsertValue("M", iSubRowIndex, Convert.ToInt32(subReport.RUB_topup), CellValues.Number, wsPart);
                            InsertValue("N", iSubRowIndex, Convert.ToInt32(subReport.SGD_amount), CellValues.Number, wsPart);
                            InsertValue("O", iSubRowIndex, Convert.ToInt32(subReport.SGD_topup), CellValues.Number, wsPart);
                            InsertValue("P", iSubRowIndex, Convert.ToInt32(subReport.ZAR_amount), CellValues.Number, wsPart);
                            InsertValue("Q", iSubRowIndex, Convert.ToInt32(subReport.ZAR_topup), CellValues.Number, wsPart);
                            InsertValue("R", iSubRowIndex, Convert.ToInt32(subReport.Total_amount), CellValues.Number, wsPart);
                            InsertValue("S", iSubRowIndex, Convert.ToInt32(subReport.Total_topup), CellValues.Number, wsPart);
                        }
                        else
                        {
                            log.Info("subReport is empty");
                        }



                        //Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        //Remove Rows Sub
                        for (int i = 74; i >= 44 + grpByCount; i--)
                        {
                            Worksheet worksheet = wsPart.Worksheet;
                            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                            if (sheetData.Elements<Row>().Where(r => r.RowIndex == i).Count() != 0)
                            {
                                Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == i).First();
                                if (row != null)
                                {
                                    row.Hidden = true;
                                }
                            }
                        }
                        //Remove Rows Main
                        for (int i = 34; i >= 4 + grpByCount; i--)
                        {
                            Worksheet worksheet = wsPart.Worksheet;
                            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                            if (sheetData.Elements<Row>().Where(r => r.RowIndex == i).Count() != 0)
                            {
                                Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == i).First();
                                if (row != null)
                                {
                                    row.Hidden = true;
                                }
                            }
                        }

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoAfterFifteenProcess : " + ex.Message);
                log.Error("DoAfterFifteenProcess : " + ex.Message);
            }
        }
        #endregion

        #region SendMail
        private static void SendMail(string docName)
        {

            try
            {
                //Mail Sending 
                string mailContent = string.Empty;
                string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item));
                }
                log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mail Sending Error : " + ex.Message);
                log.Debug("Mail Sending Error : " + ex.Message);
            }
        }
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, object value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(Convert.ToString(value));
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
