﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Newtonsoft.Json;
using NLog;
#endregion

namespace PaymentReconcileConsole
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region FinanaceReconcilationExeProcess
        public static List<FinanaceReconcilationExeProcessOutput> FinanaceReconcilationExeProcess(string sitecode = "MCM")
        {
            List<FinanaceReconcilationExeProcessOutput> outputList = new List<FinanaceReconcilationExeProcessOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "finanace_reconcilation_exe_process";
                    var result = conn.Query<FinanaceReconcilationExeProcessOutput>(
                            sp, new
                            {
                                @sitecode = sitecode
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FinanaceReconcilationExeProcess() " + ex.Message);
            }
            return outputList;
        }
        #endregion

        #region FinanceReconcilateExeDoServiceProcess
        public static List<FinanceReconcilateExeDoServiceProcessOutput> FinanceReconcilateExeDoServiceProcess(FinanaceReconcilationExeProcessOutput input)
        {
            List<FinanceReconcilateExeDoServiceProcessOutput> outputList = new List<FinanceReconcilateExeDoServiceProcessOutput>();
            try
            {
                log.Info("FinanceReconcilateExeDoServiceProcess : {0} ", JsonConvert.SerializeObject(input));
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "finance_reconcilate_exe_do_service_process";
                    var result = conn.Query<FinanceReconcilateExeDoServiceProcessOutput>(
                            sp, new
                            {
                                mobileno = input.Mobileno,
                                paymentid = input.paymentid,
                                paymentref = input.Payment_ref,
                                productid = input.productid,
                                amount = input.Amount,
                                ccdc_curr = input.currency,
                                calledby = input.calledby,
                                sitecode = input.sitecode,
                                brand = input.brand
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FinanceReconcilateExeDoServiceProcess() " + ex.Message);
            }
            return outputList;
        }
        #endregion

        #region FinanceReconcilateExeDoBundleSubscribtion
        public static List<FinanceReconcilateExeDoBundleSubscribtionOutput> FinanceReconcilateExeDoBundleSubscribtion(FinanaceReconcilationExeProcessOutput input)
        {
            List<FinanceReconcilateExeDoBundleSubscribtionOutput> outputList = new List<FinanceReconcilateExeDoBundleSubscribtionOutput>();
            try
            {
                log.Info("FinanceReconcilateExeDoBundleSubscribtion : {0} ", JsonConvert.SerializeObject(input));
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "finance_reconcilate_exe_do_bundle_subscribtion";
                    var result = conn.Query<FinanceReconcilateExeDoBundleSubscribtionOutput>(
                            sp, new
                            {
                                mobileno = input.Mobileno,
                                paymentref = input.Payment_ref,
                                amount = input.Amount,
                                bundleid = input.bundle_id,
                                ccdc_curr = input.currency,
                                calledby = input.calledby,
                                sitecode = input.sitecode,
                                brand = input.brand
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        outputList.AddRange(result);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FinanceReconcilateExeDoBundleSubscribtion() " + ex.Message);
            }
            return outputList;
        }
        #endregion

        #region GetSMSUrl
        //Used to get the SMS URL from the DB
        public static string GetSMSUrl(string countryCode = "MCM", string msisdn = "")
        {
            string sServerApiUrl = System.Configuration.ConfigurationManager.AppSettings["ServerName" + "_" + countryCode];
            try
            {
                using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();

                    var result = conn.Query<dynamic>(
                            "website_central_get_sms_iwmsc_url", new
                            {
                                @sitecode = countryCode,
                                @msisdn = msisdn
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0 && !String.IsNullOrEmpty(result.ElementAt(0).sms_iwmsc_url))
                    {
                        sServerApiUrl = result.ElementAt(0).sms_iwmsc_url;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetSMSUrl : " + ex.Message);
            }
            return sServerApiUrl;
        }
        #endregion

        #region SMSInsertRequestLog
        public static void SMSInsertRequestLog(string mobileno, string sms_url, string sms_id, string sms_sender, string request_type, string sms_text, string sitecode)
        {
            try
            {
                log.Info("SMSInsertRequestLog : Input : " + sitecode);
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "sms_insert_request_log";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @mobileno = mobileno,
                                @sms_url = sms_url,
                                @sms_id = sms_id,
                                @sms_sender = sms_sender,
                                @request_type = request_type,
                                @called_by = ConfigurationManager.AppSettings["CALLEDBY"],
                                @logid = 0,
                                @sms_text = sms_text,
                                @sitecode = sitecode
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    log.Info("SMSInsertRequestLog : result : " + JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception ex)
            {
                log.Error("SMSInsertRequestLog() ", ex.Message);
                Console.WriteLine("SMSInsertRequestLog() ", ex.Message);
            }
        }
        #endregion
    }
}
