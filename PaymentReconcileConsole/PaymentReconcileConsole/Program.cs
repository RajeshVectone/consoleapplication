﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;
#endregion

namespace PaymentReconcileConsole
{
    #region Declarations

    public class FinanaceReconcilationExeProcessOutput
    {
        public string sitecode { get; set; }
        public string Createdate { get; set; }
        public string Payment_ref { get; set; }
        public string TopupType { get; set; }
        public int CC_NO { get; set; }
        public string Mobileno { get; set; }
        public double Amount { get; set; }
        public string Replymessage { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public string currency { get; set; }
        public string email { get; set; }
        public string TYPE { get; set; }
        public int paymentid { get { return 2; } }
        public int productid { get { return 0; } }
        public string calledby { get { return ConfigurationManager.AppSettings["CALLEDBY"]; } }
        public int brand { get { return Brand == "VMUK" ? 1 : 2; } }
        public int bundle_id { get; set; }
    }

    public class FinanceReconcilateExeDoServiceProcessOutput : Output
    {
        public string incentif_data { get; set; }
        public string total_notif { get; set; }
        public double freecredit { get; set; }
        public string sms_sender { get; set; }
        public string sms_text { get; set; }
    }

    public class Output
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class FinanceReconcilateExeDoBundleSubscribtionOutput : Output
    {
        public string incentif_data { get; set; }
        public string total_notif { get; set; }
        public double freecredit { get; set; }
        public string sms_sender { get; set; }
        public string sms_text { get; set; }
    }
    #endregion

    class Program
    {
        #region Declarations
        static Logger log = LogManager.GetCurrentClassLogger();
        #endregion

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            try
            {
                DoProcess();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
        }
        #endregion

        #region DoProcess
        private static void DoProcess()
        {
            log.Info("DoProcess Started");
            Console.WriteLine("DoProcess Started");
            try
            {
                string[] arrSiteCodes = ConfigurationManager.AppSettings["SITECODE"].Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in arrSiteCodes)
                {
                    List<FinanaceReconcilationExeProcessOutput> outputList = DataAccess.FinanaceReconcilationExeProcess(item);
                    if (outputList != null && outputList.Count > 0)
                    {
                        Console.WriteLine("Total Records : " + item + " : " + outputList.Count);
                        log.Info("Total Records : " + item + " : " + outputList.Count);

                        log.Info("Records : " + item + " : " + JsonConvert.SerializeObject(outputList));

                        for (int index = 0; index < outputList.Count; index++)
                        {
                            try
                            {
                                outputList[index].sitecode = item;
                                log.Info("Input " + item + " : " + index + " : " + JsonConvert.SerializeObject(outputList[index]));
                                if (outputList[index].TYPE.ToUpper() == "TOPUP")
                                {
                                    List<FinanceReconcilateExeDoServiceProcessOutput> result = DataAccess.FinanceReconcilateExeDoServiceProcess(outputList[index]);
                                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode == 0)
                                    {
                                        try
                                        {
                                            if (!String.IsNullOrEmpty(outputList[index].Mobileno))
                                            {
                                                log.Info("Sending SMS");
                                                string smsUrl = DataAccess.GetSMSUrl(item);
                                                SendSMS(smsUrl, outputList[index].Mobileno.Trim(), result.ElementAt(0).sms_sender, result.ElementAt(0).sms_text);
                                                DataAccess.SMSInsertRequestLog(outputList[index].Mobileno.Trim(), smsUrl, "0", result.ElementAt(0).sms_sender, "1", result.ElementAt(0).sms_text, item);
                                            }
                                            else
                                            {
                                                log.Info("Mobile No is empty!");
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            log.Info("SMS Error : " + ex.Message);
                                        }
                                    }
                                }
                                else if (outputList[index].TYPE.ToUpper() == "BUNDLE")
                                {
                                    List<FinanceReconcilateExeDoBundleSubscribtionOutput> result = DataAccess.FinanceReconcilateExeDoBundleSubscribtion(outputList[index]);
                                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode == 0)
                                    {
                                        try
                                        {
                                            if (!String.IsNullOrEmpty(outputList[index].Mobileno))
                                            {
                                                log.Info("Sending SMS");
                                                string smsUrl = DataAccess.GetSMSUrl(item);
                                                SendSMS(smsUrl, outputList[index].Mobileno.Trim(), result.ElementAt(0).sms_sender, result.ElementAt(0).sms_text);
                                                DataAccess.SMSInsertRequestLog(outputList[index].Mobileno.Trim(), smsUrl, "0", result.ElementAt(0).sms_sender, "1", result.ElementAt(0).sms_text, item);
                                            }
                                            else
                                            {
                                                log.Info("Mobile No is empty!");
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            log.Info("SMS Error : " + ex.Message);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                log.Error(ex.Message);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("No records found");
                        log.Info("No records found");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error("DoProcess : ERROR : " + ex.Message);
            }
            log.Info("DoProcess Completed");
            Console.WriteLine("DoProcess Completed");
        }
        #endregion

        #region SendSMS
        private static string[] SendSMS(string smsurl, string destination, string originator, string message)
        {
            string[] strArrays = new string[] { "-1", "Failed connecting to the TPG Service!" };
            string postData = "";
            try
            {
                postData = string.Format(string.Concat(smsurl, "?service-name=port&destination-addr={0}&originator-addr-type=5&originator-addr={1}&payload-type=text&message={2}"), destination, originator, HttpUtility.UrlPathEncode(message));
                log.Info("postData : " + postData);
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(postData);
                httpWebRequest.Method = "POST";
                httpWebRequest.UserAgent = "TPGWeb";
                httpWebRequest.ContentLength = (long)0;
                StreamReader streamReader = new StreamReader(((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream());
                strArrays = streamReader.ReadToEnd().Trim().Split(":".ToCharArray(), 2);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                try
                {
                    MailAddressCollection mailTo = new MailAddressCollection();
                    mailTo.Add(new MailAddress("a.dhakshinamoorthy@vectone.com"));
                    Utility.SendMail(false, new MailAddress("noreply@vectonemobile.co.uk"), mailTo, null, null, ConfigurationManager.AppSettings["MAILSUBJECT"], ex.Message + "  -  " + postData);
                }
                catch { }
            }
            log.Info(string.Join(",", strArrays));
            return strArrays;
        }
        #endregion
    }
}
