﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace WholeSaleSalesReportConsole
{
    #region TariffAutomationDCCountryWiseTotalReport
    public class WholeSaleSalesReport
    {
        public int? ID { get; set; }
        public string reseller_name { get; set; }
        public string channel_type { get; set; }
        public int? tcount { get; set; }
        public string TYPE { get; set; }
    }
    #endregion

    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                DoProcess();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region DoProcess
        static void DoProcess()
        {
            try
            {
                string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile"]);
                string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";

                Console.WriteLine("docName : " + docName);
                log.Info("docName : " + docName);

                File.Copy(templateName, docName);

                //UK
                FillUKRecord(docName);

                //VMAT
                FillATRecord(docName, 1);

                //DMAT
                FillATRecord(docName, 2);

                //BE
                FillBERecord(docName);

                //Fr
                FillFRRecord(docName);

                SendEmail(docName);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }
        #endregion

        #region FillUKRecord
        static void FillUKRecord(string docName)
        {
            Console.WriteLine("FillUKRecord()");
            log.Info("FillUKRecord()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.GetUKRecord();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "VMUK").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        uint rowIndex = 2;
                        foreach (var item in records)
                        {
                            //Insert Rows
                            InsertRow(rowIndex, wsPart);

                            InsertValue("A", rowIndex, item.ID, CellValues.Number, wsPart);
                            InsertValue("B", rowIndex, item.reseller_name, CellValues.String, wsPart);
                            InsertValue("C", rowIndex, item.tcount, CellValues.Number, wsPart);
                            InsertValue("D", rowIndex, item.TYPE, CellValues.String, wsPart);
                            rowIndex++;
                        }

                        ////Used to execute the formula in all the cells
                        //foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        //{
                        //    cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        //}

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillUKRecord : " + ex.Message);
                log.Error("FillUKRecord : " + ex.Message);
            }
        }
        #endregion

        #region FillATRecord
        static void FillATRecord(string docName, int bundle)
        {
            Console.WriteLine("FillATRecord()");
            log.Info("FillATRecord()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.GetATRecord(bundle);
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s;
                        if (bundle == 1)
                            s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "VMAT").FirstOrDefault();
                        else
                            s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "DMAT").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        uint rowIndex = 2;
                        foreach (var item in records)
                        {
                            //Insert Rows
                            InsertRow(rowIndex, wsPart);

                            InsertValue("A", rowIndex, item.ID, CellValues.Number, wsPart);
                            InsertValue("B", rowIndex, item.reseller_name, CellValues.String, wsPart);
                            InsertValue("C", rowIndex, item.tcount, CellValues.Number, wsPart);
                            InsertValue("D", rowIndex, item.TYPE, CellValues.String, wsPart);
                            rowIndex++;
                        }

                        ////Used to execute the formula in all the cells
                        //foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        //{
                        //    cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        //}

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillATRecord : " + ex.Message);
                log.Error("FillATRecord : " + ex.Message);
            }
        }
        #endregion

        #region FillBERecord
        static void FillBERecord(string docName)
        {
            Console.WriteLine("FillBERecord()");
            log.Info("FillBERecord()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.GetBERecord();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "VMBE").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        uint rowIndex = 2;
                        foreach (var item in records)
                        {
                            //Insert Rows
                            InsertRow(rowIndex, wsPart);

                            InsertValue("A", rowIndex, item.ID, CellValues.Number, wsPart);
                            InsertValue("B", rowIndex, item.reseller_name, CellValues.String, wsPart);
                            InsertValue("C", rowIndex, item.tcount, CellValues.Number, wsPart);
                            InsertValue("D", rowIndex, item.TYPE, CellValues.String, wsPart);
                            rowIndex++;
                        }

                        ////Used to execute the formula in all the cells
                        //foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        //{
                        //    cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        //}

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillBERecord : " + ex.Message);
                log.Error("FillBERecord : " + ex.Message);
            }
        }
        #endregion

        #region FillFRRecord
        static void FillFRRecord(string docName)
        {
            Console.WriteLine("FillFRRecord()");
            log.Info("FillFRRecord()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.GetFRRecord();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "VMFR").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        uint rowIndex = 2;
                        foreach (var item in records)
                        {
                            //Insert Rows
                            InsertRow(rowIndex, wsPart);

                            InsertValue("A", rowIndex, item.ID, CellValues.Number, wsPart);
                            InsertValue("B", rowIndex, item.reseller_name, CellValues.String, wsPart);
                            InsertValue("C", rowIndex, item.tcount, CellValues.Number, wsPart);
                            InsertValue("D", rowIndex, item.TYPE, CellValues.String, wsPart);
                            rowIndex++;
                        }

                        ////Used to execute the formula in all the cells
                        //foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        //{
                        //    cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        //}

                        wsPart.Worksheet.Save();
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillFRRecord : " + ex.Message);
                log.Error("FillFRRecord : " + ex.Message);
            }
        }
        #endregion

        #region InsertRow
        //InsertRow(iRowIndex, wsPart);
        static void InsertRow(uint rowIndex, WorksheetPart wrksheetPart)
        {
            Worksheet worksheet = wrksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row = null;
            if (sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).Count() != 0)
            {
                Row refRow = sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).First();
                //Copy row from refRow and insert it
                row = CopyToLine(refRow, rowIndex, sheetData);
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }
        }

        //Copy an existing row and insert it
        //We don't need to copy styles of a refRow because a CloneNode() or Clone() methods do it for us
        static Row CopyToLine(Row refRow, uint rowIndex, SheetData sheetData)
        {
            uint newRowIndex;
            var newRow = (Row)refRow.CloneNode(true);
            // Loop through all the rows in the worksheet with higher row 
            // index values than the one you just added. For each one,
            // increment the existing row index.
            IEnumerable<Row> rows = sheetData.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);
            foreach (Row row in rows)
            {
                newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value + 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }
                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }

            sheetData.InsertBefore(newRow, refRow);
            return newRow;
        }
        #endregion

        #region SendEmail
        static void SendEmail(string docName)
        {
            try
            {
                //Mail Sending 
                string mailContent = string.Empty;
                string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item));
                }
                log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mail Sending Error : " + ex.Message);
                log.Debug("Mail Sending Error : " + ex.Message);
            }
        }
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, object value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(Convert.ToString(value));
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region GetCell
        private static Cell GetCell(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
            return null;
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
