﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace WholeSaleSalesReportConsole
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetUKRecord
        public static List<WholeSaleSalesReport> GetUKRecord()
        {
            List<WholeSaleSalesReport> OutputList = new List<WholeSaleSalesReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_UK"].ConnectionString))
                {
                    conn.Open();
                    var sp = "wholesale_sales_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                               
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new WholeSaleSalesReport()
                        {
                            ID = r.ID,
                            reseller_name = r.reseller_name,
                            channel_type = r.channel_type,
                            tcount = r.tcount,
                            TYPE = r.TYPE
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetUKRecord() : " + ex.Message);
                log.Error("GetUKRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetATRecord
        public static List<WholeSaleSalesReport> GetATRecord(int brand)
        {
            List<WholeSaleSalesReport> OutputList = new List<WholeSaleSalesReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_AT"].ConnectionString))
                {
                    conn.Open();
                    var sp = "wholesale_sales_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @brand = brand
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new WholeSaleSalesReport()
                        {
                            ID = r.ID,
                            reseller_name = r.reseller_name,
                            channel_type = r.channel_type,
                            tcount = r.tcount,
                            TYPE = r.TYPE
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetATRecord() : " + ex.Message);
                log.Error("GetATRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetBERecord
        public static List<WholeSaleSalesReport> GetBERecord()
        {
            List<WholeSaleSalesReport> OutputList = new List<WholeSaleSalesReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_BE"].ConnectionString))
                {
                    conn.Open();
                    var sp = "wholesale_sales_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new WholeSaleSalesReport()
                        {
                            ID = r.ID,
                            reseller_name = r.reseller_name,
                            channel_type = r.channel_type,
                            tcount = r.tcount,
                            TYPE = r.TYPE
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetBERecord() : " + ex.Message);
                log.Error("GetBERecord() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetFRRecord
        public static List<WholeSaleSalesReport> GetFRRecord()
        {
            List<WholeSaleSalesReport> OutputList = new List<WholeSaleSalesReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_FR"].ConnectionString))
                {
                    conn.Open();
                    var sp = "wholesale_sales_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new WholeSaleSalesReport()
                        {
                            ID = r.ID,
                            reseller_name = r.reseller_name,
                            channel_type = r.channel_type,
                            tcount = r.tcount,
                            TYPE = r.TYPE
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetFRRecord() : " + ex.Message);
                log.Error("GetFRRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}
