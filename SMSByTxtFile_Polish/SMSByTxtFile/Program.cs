﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace SMSByTxtFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "input.txt";
            DoProcess(fileName);
            //Console.WriteLine("Completed");
            //Console.ReadLine();
        }

        private static void DoProcess(string fileName)
        {
            try
            {
                string line;
                int iLine = 0;
                using (System.IO.StreamReader file = new System.IO.StreamReader(fileName))
                {
                    int iCount = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        iCount++;
                        Console.WriteLine("Line No : " + iLine++);
                        try
                        {
                            //if (Convert.ToString(ConfigurationManager.AppSettings["run"]) == "1")
                            //{
                            Thread.Sleep(100);
                            Console.WriteLine("Sms loop starts");
                            Console.WriteLine(line);

                            string url = "";
                            url = ConfigurationManager.AppSettings["mapurl" + iCount];
                            if (iCount == 9)
                                iCount = 0;

                            Console.WriteLine(url);

                            //string sText = "Bitte senden Sie ein Foto Ihres Gesichts, ein Foto Ihres Ausweises und Ihre Handynummer an support@vectonemobile.at und wir registrieren Ihre SIM-Karte für Sie.";
                            //string[] arrText = new string[6];
                            //arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=05000301030100440072006F0067006900200055017C00790074006B006F0077006E0069006B0075002C00200061006200790020006B006F006E00740079006E0075006F0077006101070020006B006F0072007A0079007300740061006E006900650020007A0020006E00610073007A007900630068002000750073014200750067002C0020007A00610072&tp-udhi=1&sequence-id=1";
                            //arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500030103020065006A00650073007400720075006A002000620065007A0070014200610074006E00690065002000730077006F006A01050020006B0061007200740119002000530049004D00200070006F00620069006500720061006A010500630020006E00610073007A0105002000610070006C0069006B00610063006A011900200068007400740070&tp-udhi=1&sequence-id=2";
                            //arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500030103030073003A002F002F006200690074002E006C0079002F0032006D00420076004700520045&tp-udhi=1&sequence-id=3";

                            //arrText[3] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=05000364030100500072007A0065015B006C0069006A0020007A0064006A0119006300690065002000730077006F006A0065006A00200074007700610072007A0079002C0020007A0064006A011900630069006500200064006F006B0075006D0065006E0074007500200072006F017C00730061006D006F015B006300690020006F00720061007A0020006E&tp-udhi=1&sequence-id=1";
                            //arrText[4] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036403020075006D00650072002000740065006C00650066006F006E00750020006E006100200061006400720065007300200065002D006D00610069006C00200073007500700070006F0072007400400076006500630074006F006E0065006D006F00620069006C0065002E00610074002C002000610020007A006100720065006A0065007300740072&tp-udhi=1&sequence-id=2";
                            //arrText[5] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036403030075006A0065006D0079002000540077006F006A01050020006B0061007200740119002000530049004D002E&tp-udhi=1&sequence-id=3";

                            string[] arrText = new string[3];
                            arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=05000363030100440072006F0067006900200055017C00790074006B006F0077006E0069006B0075002C00200061006200790020006B006F006E00740079006E0075006F0077006101070020006B006F0072007A0079007300740061006E006900650020007A0020006E00610073007A007900630068002000750073014200750067002C0020007A00610072&tp-udhi=1&sequence-id=1";
                            arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036303020065006A00650073007400720075006A002000620065007A0070014200610074006E00690065002000730077006F006A01050020006B0061007200740119002000530049004D00200070006F00620069006500720061006A010500630020006E00610073007A0105002000610070006C0069006B00610063006A011900200068007400740070&tp-udhi=1&sequence-id=2";
                            arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=0500036303030073003A002F002F006200690074002E006C0079002F0032006E00570062004E0038004E&tp-udhi=1&sequence-id=3";

                            for (int i = 0; i < arrText.Length; i++)
                            {
                                ASCIIEncoding encoding = new ASCIIEncoding();
                                string postData = string.Format(arrText[i], line.Trim());

                                Console.WriteLine(postData);

                                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
                                smsReq.Method = "POST";
                                smsReq.ContentType = "application/x-www-form-urlencoded";
                                smsReq.ServicePoint.Expect100Continue = false;
                                ASCIIEncoding enc = new ASCIIEncoding();
                                byte[] data = enc.GetBytes(postData);
                                smsReq.ContentLength = data.Length;

                                Stream newStream = smsReq.GetRequestStream();
                                newStream.Write(data, 0, data.Length);
                                newStream.Close();

                                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                                Stream resStream = smsRes.GetResponseStream();

                                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                                StreamReader readStream = new StreamReader(resStream, encode);
                                string sResponse = readStream.ReadToEnd().Trim();

                                Console.WriteLine("Sms ends");
                                Console.WriteLine(i);
                                Log(line + "--" + sResponse);
                            }
                            //}
                            //else
                            //    break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Log(ex.Message);
                        }
                    }
                    //file.Close();

                    //Used to move the file
                    //File.Move(fileName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Backups", DateTime.Now.ToString("MMddyyyyHHmmss")) + ".txt");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log("ERROR : " + ex.Message);
            }
        }

        public static void Log(string message)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + DateTime.Now.ToString("ddMMyyyy") + "_logs.txt");
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
