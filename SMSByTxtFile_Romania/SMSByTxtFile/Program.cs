﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace SMSByTxtFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "input.txt";
            DoProcess(fileName);
            //Console.WriteLine("Completed");
            //Console.ReadLine();
        }

        private static void DoProcess(string fileName)
        {
            try
            {
                string line;
                int iLine = 0;
                using (System.IO.StreamReader file = new System.IO.StreamReader(fileName))
                {
                    int iCount = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        iCount++;
                        Console.WriteLine("Line No : " + iLine++);
                        try
                        {
                            //if (Convert.ToString(ConfigurationManager.AppSettings["run"]) == "1")
                            //{
                            Thread.Sleep(100);
                            Console.WriteLine("Sms loop starts");
                            Console.WriteLine(line);

                            string url = "";
                            url = ConfigurationManager.AppSettings["mapurl" + iCount];
                            if (iCount == 9)
                                iCount = 0;

                            Console.WriteLine(url);

                            //string sText = "Bitte senden Sie ein Foto Ihres Gesichts, ein Foto Ihres Ausweises und Ihre Handynummer an support@vectonemobile.at und wir registrieren Ihre SIM-Karte für Sie.";
                            //string[] arrText = new string[6];
                            //arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003D30301005300740069006D00610074006500200063006C00690065006E0074002C002000700065006E0074007200750020006100200063006F006E00740069006E007500610020007500740069006C0069007A0061007200650061002000730065007200760069006300690069006C006F0072002C00200074006500200072007500670103006D0020&tp-udhi=1&sequence-id=1";
                            //arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003D3030200730103002000EE006E00720065006700690073007400720065007A0069002000530049004D002D0075006C0020007401030075002000670072006100740075006900740020007000720069006E002000640065007300630103007200630061007200650061002000610070006C006900630061021B0069006500690020006E006F00610073&tp-udhi=1&sequence-id=2";
                            //arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003D30303007400720065002000680074007400700073003A002F002F006200690074002E006C0079002F0032006D00420076004700520045&tp-udhi=1&sequence-id=3";

                            //arrText[3] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003FC03010054006500200072007500670103006D0020007400720069006D0069007400650020006F00200066006F0074006F00670072006100660069006500200061002000660065021B00650069002000740061006C0065002C0020006F00200066006F0074006F006700720061006600690065002000610020004300490020021900690020006E0075&tp-udhi=1&sequence-id=1";
                            //arrText[4] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003FC0302006D010300720075006C00200074010300750020006400650020006D006F00620069006C0020006C006100200073007500700070006F0072007400400076006500630074006F006E0065006D006F00620069006C0065002E0061007400200219006900200076006F006D002000EE006E00720065006700690073007400720061002000630061&tp-udhi=1&sequence-id=2";
                            //arrText[5] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003FC0303007200740065006C0061002000530049004D002000700065006E007400720075002000740069006E0065002E&tp-udhi=1&sequence-id=3";

                            string[] arrText = new string[3];
                            arrText[0] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003280301005300740069006D00610074006500200063006C00690065006E0074002C002000700065006E0074007200750020006100200063006F006E00740069006E007500610020007500740069006C0069007A0061007200650061002000730065007200760069006300690069006C006F0072002C00200074006500200072007500670103006D0020&tp-udhi=1&sequence-id=1";
                            arrText[1] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=05000328030200730103002000EE006E00720065006700690073007400720065007A0069002000530049004D002D0075006C0020007401030075002000670072006100740075006900740020007000720069006E002000640065007300630103007200630061007200650061002000610070006C006900630061021B0069006500690020006E006F00610073&tp-udhi=1&sequence-id=2";
                            arrText[2] = "orig-addr=Vectone&orig-noa=5&dest-noa=1&dest-addr={0}&tp-dcs=8&tp-ud=050003280303007400720065002000680074007400700073003A002F002F006200690074002E006C0079002F0032006E00570062004E0038004E&tp-udhi=1&sequence-id=3";

                            for (int i = 0; i < arrText.Length; i++)
                            {
                                ASCIIEncoding encoding = new ASCIIEncoding();
                                string postData = string.Format(arrText[i], line.Trim());

                                Console.WriteLine(postData);

                                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
                                smsReq.Method = "POST";
                                smsReq.ContentType = "application/x-www-form-urlencoded";
                                smsReq.ServicePoint.Expect100Continue = false;
                                ASCIIEncoding enc = new ASCIIEncoding();
                                byte[] data = enc.GetBytes(postData);
                                smsReq.ContentLength = data.Length;

                                Stream newStream = smsReq.GetRequestStream();
                                newStream.Write(data, 0, data.Length);
                                newStream.Close();

                                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                                Stream resStream = smsRes.GetResponseStream();

                                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                                StreamReader readStream = new StreamReader(resStream, encode);
                                string sResponse = readStream.ReadToEnd().Trim();

                                Console.WriteLine("Sms ends");
                                Console.WriteLine(iCount);
                                Log(line + "--" + sResponse);
                            }
                            //}
                            //else
                            //    break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Log(ex.Message);
                        }
                    }
                    //file.Close();

                    //Used to move the file
                    //File.Move(fileName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Backups", DateTime.Now.ToString("MMddyyyyHHmmss")) + ".txt");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log("ERROR : " + ex.Message);
            }
        }

        public static void Log(string message)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + DateTime.Now.ToString("ddMMyyyy") + "_logs.txt");
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
