using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace apnsproviderapi.Controllers
{
    public class RwbupdatestatusrejectfullopenController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class RwbupdatestatusrejectfullopenInput
        {
			public int type  { get; set; }
			public int usertype  { get; set; }
			public string id  { get; set; }
			public int userid  { get; set; }  	                   	    
        }
        public class RwbupdatestatusrejectfullopenOutput
        {
			public int errcode  { get; set; }
			public string errmsg  { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, RwbupdatestatusrejectfullopenInput req)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Roaming_wb"].ConnectionString))
                {
                    conn.Open();
                    var sp ="rwb_updatestatus_reject_fullopen";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
							@type=req.type,
							@usertype=req.usertype,
							@id=req.id,
							@userid=req.userid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    List<RwbupdatestatusrejectfullopenOutput> OutputList = new List<RwbupdatestatusrejectfullopenOutput>();
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new RwbupdatestatusrejectfullopenOutput()
                        {
						errcode=r.errcode==null?0:r.errcode,
						errmsg=r.errmsg==null?"Success":r.errmsg
                        }));
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                    else
                    {
                        RwbupdatestatusrejectfullopenOutput outputobj = new RwbupdatestatusrejectfullopenOutput();
						outputobj.errcode=-1;
						outputobj.errmsg="No Rec found";                     
                        OutputList.Add(outputobj);
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
}
