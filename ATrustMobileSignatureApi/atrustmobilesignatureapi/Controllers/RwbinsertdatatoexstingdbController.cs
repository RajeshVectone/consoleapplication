using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace apnsproviderapi.Controllers
{
    public class RwbinsertdatatoexstingdbController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class RwbinsertdatatoexstingdbInput
        {
        }
        public class RwbinsertdatatoexstingdbOutput
        {
			public int errcode  { get; set; }
			public string errmsg  { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, RwbinsertdatatoexstingdbInput req)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Roaming_wb"].ConnectionString))
                {
                    conn.Open();
                    var sp ="rwb_insertdata_to_exstingdb";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    List<RwbinsertdatatoexstingdbOutput> OutputList = new List<RwbinsertdatatoexstingdbOutput>();
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new RwbinsertdatatoexstingdbOutput()
                        {
						errcode=r.errcode==null?0:r.errcode,
						errmsg=r.errmsg==null?"Success":r.errmsg
                        }));
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                    else
                    {
                        RwbinsertdatatoexstingdbOutput outputobj = new RwbinsertdatatoexstingdbOutput();
						outputobj.errcode=-1;
						outputobj.errmsg="No Rec found";                     
                        OutputList.Add(outputobj);
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
}
