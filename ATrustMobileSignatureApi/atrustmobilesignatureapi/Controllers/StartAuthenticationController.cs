﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using NLog;

namespace atrustmobilesignatureapi
{
    public class StartAuthenticationController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private string subscriptionKey = ConfigurationManager.AppSettings["SUBSCRIPTIONKEY"];
        private string faceEndpoint = ConfigurationManager.AppSettings["FACEENDPOINT"];
        #endregion

        public HttpResponseMessage Post(StartAuthenticationInput input)
        {
            Log.Info("StartAuthenticationController");
            StartAuthenticationOutput output = new StartAuthenticationOutput();
            if (input != null)
            {
                Log.Info("Input : " + JsonConvert.SerializeObject(input));
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

                    using (WebClient wc = new WebClient())
                    {
                        var json = wc.DownloadString(string.Format(ConfigurationManager.AppSettings["STARTAUTHENTICATIONURL"], input.OAURL));
                        output = JsonConvert.DeserializeObject<StartAuthenticationOutput>(json);
                        if (output != null && output.ErrCode == 0)
                        {
                            output.errcode = 0;
                            output.errmsg = "Success";
                        }
                        else
                        {
                            output.errcode = output == null || output.ErrCode == null ? - 1 : output.ErrCode;
                            output.errmsg = "Failure";
                        }
                    }
                }
                catch (Exception ex)
                {
                    output = new StartAuthenticationOutput() { errcode = -1, errmsg = ex.Message };
                    Log.Error(ex.Message);
                }
            }
            else
            {
                output = new StartAuthenticationOutput() { errcode = -1, errmsg = "Input details not found!" };
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(output));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
