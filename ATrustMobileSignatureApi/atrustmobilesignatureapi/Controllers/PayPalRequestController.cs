﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;
using Braintree;
using NLog;

namespace paypalpaymentapi
{
    public class PayPalRequestController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        [BasicAuthenticationFilter]
        public HttpResponseMessage Post(PayPalRequest input)
        {
            Log.Info("PayPal Request Controller");
            PayPalTransaction output = new PayPalTransaction();
            try
            {
                
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
                Log.Error(ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
