﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using atrustmobilesignatureapi.Services;
using Newtonsoft.Json;
using NLog;

namespace atrustmobilesignatureapi
{
    public class GetAuthenticationDataController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private string subscriptionKey = ConfigurationManager.AppSettings["SUBSCRIPTIONKEY"];
        private string faceEndpoint = ConfigurationManager.AppSettings["FACEENDPOINT"];
        #endregion

        public HttpResponseMessage Post(GetAuthenticationDataInput input)
        {
            Log.Info("GetAuthenticationDataController");
            GetAuthenticationDataOutput output = new GetAuthenticationDataOutput();
            if (input != null)
            {
                Log.Info("Input : " + JsonConvert.SerializeObject(input));
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

                    ServiceReference1.GetAuthenticationDataSoapClient client = new ServiceReference1.GetAuthenticationDataSoapClient();

                    var response = client.GetDataAsync(input.SamlArtifact).Result;
                    if (response != null && response.Body != null && !String.IsNullOrEmpty(response.Body.GetDataResult))
                    {
                        Log.Info("DataResult : " + response.Body.GetDataResult);
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(response.Body.GetDataResult);

                        var manager = new XmlNamespaceManager(xmlDoc.NameTable);
                        manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:1.0:protocol");
                        var assertion = (XmlElement)xmlDoc.SelectSingleNode("//samlp:Status", manager);


                        var statusCode = xmlDoc.SelectSingleNode("//samlp:Status//samlp:StatusCode", manager).Attributes["Value"].Value;
                        if (statusCode == "samlp:Requester")
                            statusCode = xmlDoc.SelectSingleNode("//samlp:Status//samlp:StatusCode//samlp:StatusCode", manager).Attributes["Value"].Value;
                        statusCode = statusCode.Replace("samlp:", "");

                        var statusMessage = xmlDoc.SelectSingleNode("//samlp:Status//samlp:StatusMessage", manager).InnerText;

                        manager = new XmlNamespaceManager(xmlDoc.NameTable);
                        manager.AddNamespace("pr", "http://reference.e-government.gv.at/namespace/persondata/20020228#");

                        string givenName = xmlDoc.SelectSingleNode("//pr:GivenName", manager) != null ? xmlDoc.SelectSingleNode("//pr:GivenName", manager).InnerText : "";
                        string familyName = xmlDoc.SelectSingleNode("//pr:FamilyName", manager) != null ? xmlDoc.SelectSingleNode("//pr:FamilyName", manager).InnerText : "";
                        string dateOfBirth = xmlDoc.SelectSingleNode("//pr:DateOfBirth", manager) != null ? xmlDoc.SelectSingleNode("//pr:DateOfBirth", manager).InnerText : "";

                        output = new GetAuthenticationDataOutput() { errcode = (statusCode.ToUpper().Trim() == "SUCCESS" ? 0 : -1), errmsg = (statusCode.ToUpper().Trim() == "SUCCESS" ? "Success" : "Failure"), DataResult = response.Body.GetDataResult, StatusCode = statusCode, StatusMessage = statusMessage, GivenName = givenName, FamilyName = familyName, DateOfBirth = dateOfBirth };
                    }
                    else
                        output = new GetAuthenticationDataOutput() { errcode = -1, errmsg = "Failure" };
                }
                catch (Exception ex)
                {
                    output = new GetAuthenticationDataOutput() { errcode = -1, errmsg = ex.Message };
                    Log.Error(ex.Message);
                }
            }
            else
            {
                output = new GetAuthenticationDataOutput() { errcode = -1, errmsg = "Input details not found!" };
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(output));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
