﻿using System.Security.Principal;

namespace atrustmobilesignatureapi
{
    public interface IProvidePrincipal
    {
        IPrincipal ReturnTokenStatus(string projectName, string tokenId);

        IPrincipal CreatePrincipal(string username, string password, string projectId);
    }
}