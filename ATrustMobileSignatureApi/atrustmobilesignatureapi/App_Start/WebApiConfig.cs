using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
namespace atrustmobilesignatureapi
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
           "StartAuthentication",
           routeTemplate: "v1/startauthentication",
           defaults: new { controller = "startauthentication" });

            config.Routes.MapHttpRoute(
           "GetAuthenticationData",
           routeTemplate: "v1/getauthenticationdata",
           defaults: new { controller = "getauthenticationdata" });

        }
	}
}





















































































































































