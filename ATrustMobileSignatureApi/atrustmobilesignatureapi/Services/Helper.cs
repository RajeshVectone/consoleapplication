﻿using NLog;
using System;
using System.Text;

namespace atrustmobilesignatureapi
{
    public static class Helper
    {
        #region Declarations
        public static string username = GetAppSettings("username");
        public static string password = GetAppSettings("password");
        public static string CLIENT_ACCESS_TOKEN = GetAppSettings("CLIENTACCESSTOKEN");
        public static string BRAINTREE_ACCOUNT = GetAppSettings("BRAINTREE_ACCOUNT"); 
        #endregion

        #region Methods
        public static string GetAppSettings(string key)
        {
            if (System.Configuration.ConfigurationManager.AppSettings[key] != null)
                return System.Configuration.ConfigurationManager.AppSettings[key];
            return "";
        }

        static string GetPushFile(string file)
        {
            return System.Web.Hosting.HostingEnvironment.MapPath(file);
        }

        public static byte[] ToByteArray(this string value)
        {
            return Encoding.Default.GetBytes(value);
        }

        public static string GetAuthToken()
        {
            return Convert.ToBase64String((Helper.username + ":" + Helper.password).ToByteArray());
        }

        public static string ToHexString(this byte[] bytes, int length = 0)
        {
            if (bytes == null || bytes.Length <= 0)
                return "";

            var sb = new StringBuilder();

            foreach (byte b in bytes)
            {
                sb.Append(b.ToString("x2"));

                if (length > 0 && sb.Length >= length)
                    break;
            }
            return sb.ToString();
        }
        #endregion
    }
}