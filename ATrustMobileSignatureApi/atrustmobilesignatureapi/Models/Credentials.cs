﻿namespace atrustmobilesignatureapi
{
    public class Credentials
    {
        public string username { get; set; }
        public string password { get; set; }
        public string url_key { get; set; }
    }
} 