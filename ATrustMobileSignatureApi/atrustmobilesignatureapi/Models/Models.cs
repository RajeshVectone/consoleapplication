﻿namespace atrustmobilesignatureapi
{
    public class CommonOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class StartAuthenticationInput 
    {
        public string OAURL { get; set; }
        public string calledby { get; set; }
        public string iccid { get; set; }
    }

    public class StartAuthenticationOutput : CommonOutput
    {
        public int ErrCode { get; set; }
        public string DataURL { get; set; }
        public string XMLRequest { get; set; }
        public string BKUUrl { get; set; }
    }

    public class GetAuthenticationDataInput
    {
        public string SamlArtifact { get; set; }
        public string msisdn { get; set; }
        public string calledby { get; set; }
        public string iccid { get; set; }

        public string email { get; set; }
        public string mobileno { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
    }

    public class GetAuthenticationDataOutput : CommonOutput
    {
        public string DataResult { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string DateOfBirth { get; set; }
    }
}