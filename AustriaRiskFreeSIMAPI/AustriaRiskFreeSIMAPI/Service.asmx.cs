﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;
using Newtonsoft.Json;
using NLog;
using AustriaRiskFreeSIMAPI.Models;
using AustriaRiskFreeSIMAPI.Services;

namespace AustriaRiskFreeSIMAPI
{
    /// <summary>
    /// Webservice for Austria Risk Free SIM API
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    //[System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        #region ping
        [WebMethod(Description = "The Ping message is intended for the check of supplier interface availability.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public pingResponseDto ping([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)] pingRequestDto requestdata)
        {
            Log.Info("ping : Input : {0}", JsonConvert.SerializeObject(requestdata));
            pingResponseDto output = new pingResponseDto();
            output = new pingResponseDto();
            try
            {
                if (requestdata != null)
                {
                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = "en";
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (output.resultCode != 0)
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.errorCode = 1001;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ping : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.errorCode = 1000;
                output.errorDescription = ex.Message;
            }
            Log.Info("Ping : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region  activation
        [WebMethod(Description = "The Activation message is intended for the activation of the SIM.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ActivationResponseDto activation([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]ActivationRequestDto requestdata)
        {
            Log.Info("activation : Input : {0}", JsonConvert.SerializeObject(requestdata));
            ActivationResponseDto output = new ActivationResponseDto();
            try
            {
                if (requestdata != null)
                {
                    requestdata.lang = String.IsNullOrEmpty(requestdata.lang) ? "en" : requestdata.lang;

                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = requestdata.lang; 

                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (commonModel.ResultCode == 0)
                    {
                        output = DBHelper.Activation(requestdata);
                    }
                    else
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.errorCode = 1004;
                    output.errorCodeSpecified = true;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("activation : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1005;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            Log.Info("activation : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region  validation
        [WebMethod(Description = "The Validation message is intended for the validation of the SIM.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ActivationResponseDto validation([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]ActivationRequestDto requestdata)
        {
            Log.Info("validation : Input : {0}", JsonConvert.SerializeObject(requestdata));
            ActivationResponseDto output = new ActivationResponseDto();
            try
            {
                if (requestdata != null)
                {
                    requestdata.lang = String.IsNullOrEmpty(requestdata.lang) ? "en" : requestdata.lang;

                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = requestdata.lang;
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (commonModel.ResultCode == 0)
                    {
                        output = DBHelper.Validation(requestdata);
                    }
                    else
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.errorCode = 1004;
                    output.errorCodeSpecified = true;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("validation : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1005;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            Log.Info("validation : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion

        #region  deactivation
        [WebMethod(Description = "The Deactivation message is intended for the deactivation of the SIM.")]
        [return: XmlElement("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ActivationResponseDto deactivation([System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]ActivationRequestDto requestdata)
        {
            Log.Info("deactivation : Input : {0}", JsonConvert.SerializeObject(requestdata));
            ActivationResponseDto output = new ActivationResponseDto();
            try
            {
                if (requestdata != null)
                {
                    requestdata.lang = String.IsNullOrEmpty(requestdata.lang) ? "en" : requestdata.lang;

                    CommonInput commonInput = new CommonInput();
                    commonInput.Username = requestdata.username;
                    commonInput.Password = requestdata.password;
                    commonInput.Lang = requestdata.lang;
                    CommonOutput commonModel = Helper.ValidateLogin(commonInput);
                    output.resultCode = commonModel.ResultCode;
                    output.resultCodeSpecified = true;
                    if (commonModel.ResultCode == 0)
                    {
                        output = DBHelper.Deactivation(requestdata);
                    }
                    else
                    {
                        output.errorCode = commonModel.ErrorCode;
                        output.errorCodeSpecified = true;
                        output.errorDescription = commonModel.ErrorDescription;
                    }
                }
                else
                {
                    output.resultCode = 1;
                    output.resultCodeSpecified = true;
                    output.errorCode = 1004;
                    output.errorCodeSpecified = true;
                    output.errorDescription = MessageHelper.GetMessage("INPUTDETAILSNOTFOUND", "en");
                }
            }
            catch (Exception ex)
            {
                Log.Error("deactivation : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1005;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            Log.Info("deactivation : Output : {0}", JsonConvert.SerializeObject(output));
            return output;
        }
        #endregion
    }
}
