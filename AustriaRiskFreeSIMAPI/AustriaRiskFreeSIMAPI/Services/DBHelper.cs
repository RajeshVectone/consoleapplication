﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using NLog;
using System.Data;
using AustriaRiskFreeSIMAPI.Models;
using AustriaRiskFreeSIMAPI.Services;
using Newtonsoft.Json;

namespace AustriaRiskFreeSIMAPI
{
    public static class DBHelper
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        #region Activation
        public static ActivationResponseDto Activation(ActivationRequestDto input)
        {
            ActivationResponseDto output = new ActivationResponseDto();
            // Added for taking EAN code
             string ICCID =  string.Empty;
             string EAN = string.Empty;

             if (input.iccid.Split(',').Count() > 1)
             {
                 ICCID = input.iccid.Split(',')[1].ToString();
                 EAN = input.iccid.Split(',')[0].ToString();
             }
             else
             {
                 ICCID = input.iccid;
             }    
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "evita_risk_free_sim_process";
                    var result = conn.Query<dynamic>(
                            //sp, new
                            //{
                            //    @iccid = input.iccid,
                            //    @lang = input.lang
                            //},
                             sp, new
                             {
                                 @iccid = ICCID,
                                 @ean_code = EAN,
                                 @lang = input.lang
                             },
                            commandType: CommandType.StoredProcedure);

                    Log.Info("Activation : SP Output : {0}", JsonConvert.SerializeObject(result));

                    if (result != null)
                    {
                        output.resultCode = result.ElementAt(0).errcode;
                        output.resultCodeSpecified = true;
                        output.transactionId = result.ElementAt(0).@transactionid;
                        if (output.resultCode != 0)
                        {
                            output.errorCode = 1006;
                            output.errorCodeSpecified = true;
                            output.errorDescription = result.ElementAt(0).errmsg;
                        }
                    }
                    else
                    {
                        output.resultCode = 1;
                        output.resultCodeSpecified = true;
                        output.errorCode = 1007;
                        output.errorCodeSpecified = true;
                        output.errorDescription = MessageHelper.GetMessage("FAILURE", input.lang);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Activation : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1008;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            return output;
        }
        #endregion

        #region Validation
        public static ActivationResponseDto Validation(ActivationRequestDto input)
        {
            ActivationResponseDto output = new ActivationResponseDto();
            // Added for taking EAN code
            string ICCID = string.Empty;
            string EAN = string.Empty;

            if (input.iccid.Split(',').Count() > 1)
            {
                ICCID = input.iccid.Split(',')[1].ToString();
                EAN = input.iccid.Split(',')[0].ToString();
            }
            else
            {
                ICCID = input.iccid;
            }
            Log.Info("Validation : SP Input ICCID: {0}", ICCID);
            Log.Info("Validation : SP Input EAN: {0}", EAN);
            Log.Info("Validation : SP Input laung: {0}", input.lang);
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "evita_risk_free_sim_validation";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @iccid = ICCID,
                                @lang = input.lang,
                                @ean_code = EAN,
                            },
                            commandType: CommandType.StoredProcedure);

                    Log.Info("Validation : SP Output : {0}", JsonConvert.SerializeObject(result));

                    if (result != null)
                    {
                        output.resultCode = result.ElementAt(0).errcode;
                        output.resultCodeSpecified = true;
                        //output.transactionId = result.ElementAt(0).@transactionid;
                        if (output.resultCode != 0)
                        {
                            output.errorCode = 1006;
                            output.errorCodeSpecified = true;
                            output.errorDescription = result.ElementAt(0).errmsg;
                        }
                    }
                    else
                    {
                        output.resultCode = 1;
                        output.resultCodeSpecified = true;
                        output.errorCode = 1007;
                        output.errorCodeSpecified = true;
                        output.errorDescription = MessageHelper.GetMessage("FAILURE", input.lang);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Validation : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1008;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            return output;
        }
        #endregion

        #region Deactivation
        public static ActivationResponseDto Deactivation(ActivationRequestDto input)
        {
            ActivationResponseDto output = new ActivationResponseDto();
            // Added for taking EAN code
            string ICCID = string.Empty;
            string EAN = string.Empty;

            if (input.iccid.Split(',').Count() > 1)
            {
                ICCID = input.iccid.Split(',')[1].ToString();
                EAN = input.iccid.Split(',')[0].ToString();
            }
            else
            {
                ICCID = input.iccid;
            } 
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "evita_risk_free_deactivation";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @iccid = ICCID,
                                @lang = input.lang,
                                @ean_code = EAN
                            },
                            commandType: CommandType.StoredProcedure);

                    Log.Info("Deactivation : SP Output : {0}", JsonConvert.SerializeObject(result));

                    if (result != null)
                    {
                        output.resultCode = result.ElementAt(0).errcode;
                        output.resultCodeSpecified = true;
                        //output.transactionId = result.ElementAt(0).@transactionid;
                        if (output.resultCode != 0)
                        {
                            output.errorCode = 1006;
                            output.errorCodeSpecified = true;
                            output.errorDescription = result.ElementAt(0).errmsg;
                        }
                    }
                    else
                    {
                        output.resultCode = 1;
                        output.resultCodeSpecified = true;
                        output.errorCode = 1007;
                        output.errorCodeSpecified = true;
                        output.errorDescription = MessageHelper.GetMessage("FAILURE", input.lang);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Deactivation : Error : {0}", ex.Message);
                output.resultCode = 2;
                output.resultCodeSpecified = true;
                output.errorCode = 1008;
                output.errorCodeSpecified = true;
                output.errorDescription = ex.Message;
            }
            return output;
        }
        #endregion
    }
}