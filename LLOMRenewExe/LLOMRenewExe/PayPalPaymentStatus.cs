﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace LLOMRenewExe
{
    public class PayPalPaymentStatus
    {
        public PayPalPaymentStatus()
		{
		}

        private DataTable _tblReasonCodes;

		private string _refcode = string.Empty;

		private string _subscriptionID = string.Empty;

		private string _decision = string.Empty;

		private string _requestID = string.Empty;

		private string _currency = string.Empty;

		private double _amount = 0;

		private int _reasonCode = -1;

		private string _authorizationCode = string.Empty;

		private string _requestTime = string.Empty;

		private string _reconciliationID = string.Empty;

		private string _reasonDescription = string.Empty;

		public double Amount
		{
			get;
			set;
		}

		public string AuthorizationCode
		{
			get;
			set;
		}

		public string Currency
		{
			get;
			set;
		}

		public string Decision
		{
			get;
			set;
		}

		public int ReasonCode
		{
			get;
			set;
		}

		public string ReasonDescription
		{
			get;
			set;
		}

		public string ReconciliationID
		{
			get;
			set;
		}

		public string ReferenceCode
		{
			get;
			set;
		}

		public string RequestID
		{
			get;
			set;
		}

		public string RequestTime
		{
			get;
			set;
		}

		public string SubscriptionID
		{
			get;
			set;
		}

		public override string ToString()
		{
			return string.Concat(new object[] { "\nRequestId : ", this.RequestID, "\nRequestTime : ", this.RequestTime, "\nReferenceCode : ", this.ReferenceCode, "\nSubscriptionID : ", this.SubscriptionID, "\nDecision : ", this.Decision, "\nAmount : ", this.Amount, "\nCurrency : ", this.Currency, "\nAuthorizationCode : ", this.AuthorizationCode, "\nReconciliationID : ", this.ReconciliationID, "\nReasonCode : ", this.ReasonCode, "\nReasonDescription : ", this.ReasonDescription });
		}
    }

    public class PPLPaymentInput
    {
        public string subscription_id { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string country { get; set; }
        public string referencecode { get; set; }
        public string accountid { get; set; }
        public string sitecode { get; set; }
        public string product_code { get; set; }
        public string application_code { get; set; }
        public string payment_agent { get; set; }
        public string service_type { get; set; }
        public string payment_mode { get; set; }
        public string client_ip { get; set; }
        public string email { get; set; }
        public string account_type { get; set; }
        public string last6digitccno { get; set; }
    }

    public class PPLPaymentOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string transaction_id { get; set; }
        public string order_id { get; set; }
    }
}
