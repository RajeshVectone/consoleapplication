using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SearchService
{
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("xsd", "2.0.50727.42")]
	[Serializable]
	[XmlType(AnonymousType=true)]
	public class OutputSearchService09Messages
	{
		private int remainingQueriesField;

		private DateTime dateExpiresField;

		private object errorMessageField;

		private string versionSearchEngineField;

		private string versionWebServiceField;

		[XmlElement(DataType="date")]
		public DateTime DateExpires
		{
			get
			{
				return this.dateExpiresField;
			}
			set
			{
				this.dateExpiresField = value;
			}
		}

		public object ErrorMessage
		{
			get
			{
				return this.errorMessageField;
			}
			set
			{
				this.errorMessageField = value;
			}
		}

		public int RemainingQueries
		{
			get
			{
				return this.remainingQueriesField;
			}
			set
			{
				this.remainingQueriesField = value;
			}
		}

		public string VersionSearchEngine
		{
			get
			{
				return this.versionSearchEngineField;
			}
			set
			{
				this.versionSearchEngineField = value;
			}
		}

		public string VersionWebService
		{
			get
			{
				return this.versionWebServiceField;
			}
			set
			{
				this.versionWebServiceField = value;
			}
		}

		public OutputSearchService09Messages()
		{
		}
	}
}