using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SearchService
{
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("xsd", "2.0.50727.42")]
	[Serializable]
	[XmlRoot(Namespace="", IsNullable=false)]
	[XmlType(AnonymousType=true)]
	public class OutputSearchService09
	{
		private OutputSearchService09SearchResultItems searchResultItemsField;

		private OutputSearchService09Messages messagesField;

		private OutputSearchService09CustomerInfo customerInfoField;

		public OutputSearchService09CustomerInfo CustomerInfo
		{
			get
			{
				return this.customerInfoField;
			}
			set
			{
				this.customerInfoField = value;
			}
		}

		public OutputSearchService09Messages Messages
		{
			get
			{
				return this.messagesField;
			}
			set
			{
				this.messagesField = value;
			}
		}

		public OutputSearchService09SearchResultItems SearchResultItems
		{
			get
			{
				return this.searchResultItemsField;
			}
			set
			{
				this.searchResultItemsField = value;
			}
		}

		public OutputSearchService09()
		{
		}
	}
}