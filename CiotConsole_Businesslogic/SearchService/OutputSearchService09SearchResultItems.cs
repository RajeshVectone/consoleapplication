using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SearchService
{
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("xsd", "2.0.50727.42")]
	[Serializable]
	[XmlType(AnonymousType=true)]
	public class OutputSearchService09SearchResultItems
	{
		private OutputSearchService09SearchResultItemsSearchResultItem searchResultItemField;

		public OutputSearchService09SearchResultItemsSearchResultItem SearchResultItem
		{
			get
			{
				return this.searchResultItemField;
			}
			set
			{
				this.searchResultItemField = value;
			}
		}

		public OutputSearchService09SearchResultItems()
		{
		}
	}
}