using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SearchService
{
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("xsd", "2.0.50727.42")]
	[Serializable]
	[XmlType(AnonymousType=true)]
	public class OutputSearchService09SearchResultItemsSearchResultItemResultRow
	{
		private byte rowIDField;

		private byte hitTypeField;

		private uint unitIDField;

		private byte sequenceField;

		private byte subUnitIDField;

		private byte subSequenceField;

		private string unitTypeField;

		private uint sourceIDField;

		private object organizationNoField;

		private uint bornField;

		private byte genderField;

		private string name1Field;

		private string name2Field;

		private object name3Field;

		private string streetNameField;

		private byte houseNoField;

		private string entranceField;

		private object addrExtField;

		private ushort zipCodeField;

		private string cityField;

		private ushort municipField;

		private string countryField;

		private ushort fieldAreaField;

		private ushort streetNumberField;

		private uint coordinateXField;

		private uint coordinateYField;

		private byte coordinateZField;

		private object boxTextField;

		private object boxNumberField;

		private object boxOfficeField;

		private object boxZipCodeField;

		private object boxCityField;

		private object boxMunicipField;

		private object boxCountryField;

		private string phoneNoField;

		private byte numberTypeField;

		private byte equipmentTypeField;

		private byte serviceTypeField;

		private object elAddressField;

		private string reservationCodeField;

		private uint displayField;

		private uint qualityIndicatorField;

		private ushort sourceField;

		private uint insertDateField;

		private uint updateDateField;

		public object AddrExt
		{
			get
			{
				return this.addrExtField;
			}
			set
			{
				this.addrExtField = value;
			}
		}

		public uint Born
		{
			get
			{
				return this.bornField;
			}
			set
			{
				this.bornField = value;
			}
		}

		public object BoxCity
		{
			get
			{
				return this.boxCityField;
			}
			set
			{
				this.boxCityField = value;
			}
		}

		public object BoxCountry
		{
			get
			{
				return this.boxCountryField;
			}
			set
			{
				this.boxCountryField = value;
			}
		}

		public object BoxMunicip
		{
			get
			{
				return this.boxMunicipField;
			}
			set
			{
				this.boxMunicipField = value;
			}
		}

		public object BoxNumber
		{
			get
			{
				return this.boxNumberField;
			}
			set
			{
				this.boxNumberField = value;
			}
		}

		public object BoxOffice
		{
			get
			{
				return this.boxOfficeField;
			}
			set
			{
				this.boxOfficeField = value;
			}
		}

		public object BoxText
		{
			get
			{
				return this.boxTextField;
			}
			set
			{
				this.boxTextField = value;
			}
		}

		public object BoxZipCode
		{
			get
			{
				return this.boxZipCodeField;
			}
			set
			{
				this.boxZipCodeField = value;
			}
		}

		public string City
		{
			get
			{
				return this.cityField;
			}
			set
			{
				this.cityField = value;
			}
		}

		public uint CoordinateX
		{
			get
			{
				return this.coordinateXField;
			}
			set
			{
				this.coordinateXField = value;
			}
		}

		public uint CoordinateY
		{
			get
			{
				return this.coordinateYField;
			}
			set
			{
				this.coordinateYField = value;
			}
		}

		public byte CoordinateZ
		{
			get
			{
				return this.coordinateZField;
			}
			set
			{
				this.coordinateZField = value;
			}
		}

		public string Country
		{
			get
			{
				return this.countryField;
			}
			set
			{
				this.countryField = value;
			}
		}

		public uint Display
		{
			get
			{
				return this.displayField;
			}
			set
			{
				this.displayField = value;
			}
		}

		public object ElAddress
		{
			get
			{
				return this.elAddressField;
			}
			set
			{
				this.elAddressField = value;
			}
		}

		public string Entrance
		{
			get
			{
				return this.entranceField;
			}
			set
			{
				this.entranceField = value;
			}
		}

		public byte EquipmentType
		{
			get
			{
				return this.equipmentTypeField;
			}
			set
			{
				this.equipmentTypeField = value;
			}
		}

		public ushort FieldArea
		{
			get
			{
				return this.fieldAreaField;
			}
			set
			{
				this.fieldAreaField = value;
			}
		}

		public byte Gender
		{
			get
			{
				return this.genderField;
			}
			set
			{
				this.genderField = value;
			}
		}

		public byte HitType
		{
			get
			{
				return this.hitTypeField;
			}
			set
			{
				this.hitTypeField = value;
			}
		}

		public byte HouseNo
		{
			get
			{
				return this.houseNoField;
			}
			set
			{
				this.houseNoField = value;
			}
		}

		public uint InsertDate
		{
			get
			{
				return this.insertDateField;
			}
			set
			{
				this.insertDateField = value;
			}
		}

		public ushort Municip
		{
			get
			{
				return this.municipField;
			}
			set
			{
				this.municipField = value;
			}
		}

		public string Name1
		{
			get
			{
				return this.name1Field;
			}
			set
			{
				this.name1Field = value;
			}
		}

		public string Name2
		{
			get
			{
				return this.name2Field;
			}
			set
			{
				this.name2Field = value;
			}
		}

		public object Name3
		{
			get
			{
				return this.name3Field;
			}
			set
			{
				this.name3Field = value;
			}
		}

		public byte NumberType
		{
			get
			{
				return this.numberTypeField;
			}
			set
			{
				this.numberTypeField = value;
			}
		}

		public object OrganizationNo
		{
			get
			{
				return this.organizationNoField;
			}
			set
			{
				this.organizationNoField = value;
			}
		}

		public string PhoneNo
		{
			get
			{
				return this.phoneNoField;
			}
			set
			{
				this.phoneNoField = value;
			}
		}

		public uint QualityIndicator
		{
			get
			{
				return this.qualityIndicatorField;
			}
			set
			{
				this.qualityIndicatorField = value;
			}
		}

		public string ReservationCode
		{
			get
			{
				return this.reservationCodeField;
			}
			set
			{
				this.reservationCodeField = value;
			}
		}

		public byte RowID
		{
			get
			{
				return this.rowIDField;
			}
			set
			{
				this.rowIDField = value;
			}
		}

		public byte Sequence
		{
			get
			{
				return this.sequenceField;
			}
			set
			{
				this.sequenceField = value;
			}
		}

		public byte ServiceType
		{
			get
			{
				return this.serviceTypeField;
			}
			set
			{
				this.serviceTypeField = value;
			}
		}

		public ushort Source
		{
			get
			{
				return this.sourceField;
			}
			set
			{
				this.sourceField = value;
			}
		}

		public uint SourceID
		{
			get
			{
				return this.sourceIDField;
			}
			set
			{
				this.sourceIDField = value;
			}
		}

		public string StreetName
		{
			get
			{
				return this.streetNameField;
			}
			set
			{
				this.streetNameField = value;
			}
		}

		public ushort StreetNumber
		{
			get
			{
				return this.streetNumberField;
			}
			set
			{
				this.streetNumberField = value;
			}
		}

		public byte SubSequence
		{
			get
			{
				return this.subSequenceField;
			}
			set
			{
				this.subSequenceField = value;
			}
		}

		public byte SubUnitID
		{
			get
			{
				return this.subUnitIDField;
			}
			set
			{
				this.subUnitIDField = value;
			}
		}

		public uint UnitID
		{
			get
			{
				return this.unitIDField;
			}
			set
			{
				this.unitIDField = value;
			}
		}

		public string UnitType
		{
			get
			{
				return this.unitTypeField;
			}
			set
			{
				this.unitTypeField = value;
			}
		}

		public uint UpdateDate
		{
			get
			{
				return this.updateDateField;
			}
			set
			{
				this.updateDateField = value;
			}
		}

		public ushort ZipCode
		{
			get
			{
				return this.zipCodeField;
			}
			set
			{
				this.zipCodeField = value;
			}
		}

		public OutputSearchService09SearchResultItemsSearchResultItemResultRow()
		{
		}
	}
}