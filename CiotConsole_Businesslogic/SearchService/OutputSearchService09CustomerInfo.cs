using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SearchService
{
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("xsd", "2.0.50727.42")]
	[Serializable]
	[XmlType(AnonymousType=true)]
	public class OutputSearchService09CustomerInfo
	{
		private byte customerNoField;

		private object customerNameField;

		public object CustomerName
		{
			get
			{
				return this.customerNameField;
			}
			set
			{
				this.customerNameField = value;
			}
		}

		public byte CustomerNo
		{
			get
			{
				return this.customerNoField;
			}
			set
			{
				this.customerNoField = value;
			}
		}

		public OutputSearchService09CustomerInfo()
		{
		}
	}
}