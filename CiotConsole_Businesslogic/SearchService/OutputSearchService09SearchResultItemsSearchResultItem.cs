using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SearchService
{
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("xsd", "2.0.50727.42")]
	[Serializable]
	[XmlType(AnonymousType=true)]
	public class OutputSearchService09SearchResultItemsSearchResultItem
	{
		private OutputSearchService09SearchResultItemsSearchResultItemResultRow[] resultRowsField;

		private OutputSearchService09SearchResultItemsSearchResultItemResultInfo resultInfoField;

		private byte idField;

		[XmlAttribute]
		public byte ID
		{
			get
			{
				return this.idField;
			}
			set
			{
				this.idField = value;
			}
		}

		public OutputSearchService09SearchResultItemsSearchResultItemResultInfo ResultInfo
		{
			get
			{
				return this.resultInfoField;
			}
			set
			{
				this.resultInfoField = value;
			}
		}

		[XmlArrayItem("ResultRow", IsNullable=false)]
		public OutputSearchService09SearchResultItemsSearchResultItemResultRow[] ResultRows
		{
			get
			{
				return this.resultRowsField;
			}
			set
			{
				this.resultRowsField = value;
			}
		}

		public OutputSearchService09SearchResultItemsSearchResultItem()
		{
		}
	}
}