using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace SearchService
{
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("xsd", "2.0.50727.42")]
	[Serializable]
	[XmlType(AnonymousType=true)]
	public class OutputSearchService09SearchResultItemsSearchResultItemResultInfo
	{
		private ushort elapsedMillisecondsField;

		private byte accuracyField;

		public byte Accuracy
		{
			get
			{
				return this.accuracyField;
			}
			set
			{
				this.accuracyField = value;
			}
		}

		public ushort ElapsedMilliseconds
		{
			get
			{
				return this.elapsedMillisecondsField;
			}
			set
			{
				this.elapsedMillisecondsField = value;
			}
		}

		public OutputSearchService09SearchResultItemsSearchResultItemResultInfo()
		{
		}
	}
}