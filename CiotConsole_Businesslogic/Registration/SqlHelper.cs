using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Registration
{
	public static class SqlHelper
	{
		public static void AddParamToSQLCmd(SqlCommand sqlCmd, string paramId, SqlDbType sqlType, int paramSize, ParameterDirection paramDirection, object paramvalue)
		{
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			if (paramId == string.Empty)
			{
				throw new ArgumentOutOfRangeException("paramId");
			}
			SqlParameter sqlParameter = new SqlParameter()
			{
				ParameterName = paramId,
				SqlDbType = sqlType,
				Direction = paramDirection
			};
			if (paramSize > 0)
			{
				sqlParameter.Size = paramSize;
			}
			if (paramvalue != null)
			{
				sqlParameter.Value = paramvalue;
			}
			sqlCmd.Parameters.Add(sqlParameter);
		}

		public static int ExecuteNonQuery(string connectionString, SqlCommand sqlCmd)
		{
			int num;
			if (connectionString == string.Empty)
			{
				throw new ArgumentOutOfRangeException("ConnectionString");
			}
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlCmd.Connection = sqlConnection;
				sqlConnection.Open();
				num = sqlCmd.ExecuteNonQuery();
				sqlConnection.Close();
			}
			return num;
		}

		public static void ExecuteReaderCmd<T>(string connectionString, SqlCommand sqlCmd, GenerateListFromReader<T> glfr, ref List<T> List)
		{
			if (connectionString == string.Empty)
			{
				throw new ArgumentOutOfRangeException("ConnectionString");
			}
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlCmd.Connection = sqlConnection;
				sqlConnection.Open();
				glfr(sqlCmd.ExecuteReader(), ref List);
				sqlConnection.Close();
			}
		}

		public static object ExecuteScalarCmd(string connectionString, SqlCommand sqlCmd)
		{
			if (connectionString == string.Empty)
			{
				throw new ArgumentOutOfRangeException("ConnectionString");
			}
			if (sqlCmd == null)
			{
				throw new ArgumentNullException("sqlCmd");
			}
			object obj = null;
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				sqlCmd.Connection = sqlConnection;
				sqlConnection.Open();
				obj = sqlCmd.ExecuteScalar();
				sqlConnection.Close();
			}
			return obj;
		}

		public static void SetCommandType(SqlCommand sqlCmd, CommandType cmdType, string cmdText)
		{
			sqlCmd.CommandType = cmdType;
			sqlCmd.CommandText = cmdText;
		}
	}
}