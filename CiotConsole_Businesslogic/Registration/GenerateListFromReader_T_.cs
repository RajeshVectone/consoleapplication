using System;
using System.Collections.Generic;
using System.Data;

namespace Registration
{
	public delegate void GenerateListFromReader<T>(IDataReader returnData, ref List<T> tempList);
}