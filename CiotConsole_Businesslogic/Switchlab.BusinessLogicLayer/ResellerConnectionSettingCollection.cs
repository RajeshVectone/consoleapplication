using System;
using System.Configuration;
using System.Reflection;

namespace Switchlab.BusinessLogicLayer
{
	public class ResellerConnectionSettingCollection : ConfigurationElementCollection
	{
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}

		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}

		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}

		public new int Count
		{
			get
			{
				return base.Count;
			}
		}

		public ResellerConnectionSetting this[int index]
		{
			get
			{
				return (ResellerConnectionSetting)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}

		public new ResellerConnectionSetting this[string resellerId]
		{
			get
			{
				return (ResellerConnectionSetting)base.BaseGet(resellerId);
			}
		}

		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
		}

		public ResellerConnectionSettingCollection()
		{
			this.Add((ResellerConnectionSetting)this.CreateNewElement());
		}

		public void Add(ResellerConnectionSetting value)
		{
			this.BaseAdd(value);
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return new ResellerConnectionSetting();
		}

		protected override ConfigurationElement CreateNewElement(string elementName)
		{
			return new ResellerConnectionSetting(elementName);
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((ResellerConnectionSetting)element).ResellerId;
		}

		public int IndexOf(ResellerConnectionSetting value)
		{
			return base.BaseIndexOf(value);
		}

		public void Remove(ResellerConnectionSetting value)
		{
			base.BaseRemove(value);
		}

		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
	}
}