using System;
using System.Configuration;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CustomConnectionStringSection : ConfigurationSection
	{
		[ConfigurationProperty("cdrConnectionString", IsRequired=true)]
		public string CDRConnectionString
		{
			get
			{
				return (string)base["cdrConnectionString"];
			}
			set
			{
				base["cdrConnectionString"] = value;
			}
		}

		[ConfigurationProperty("Connections")]
		[ConfigurationValidator(typeof(ConnectionStringValidation))]
		public ConnectionStringSettingsCollection Connections
		{
			get
			{
				return (ConnectionStringSettingsCollection)base["Connections"];
			}
		}

		[ConfigurationProperty("ResellerConnections")]
		[ConfigurationValidator(typeof(ResellerConnectionSettingValidation))]
		public ResellerConnectionSettingCollection ResellerConnections
		{
			get
			{
				return (ResellerConnectionSettingCollection)base["ResellerConnections"];
			}
		}

		[ConfigurationProperty("rpcpConnectionString", IsRequired=true)]
		public string RPCPConnectionString
		{
			get
			{
				return (string)base["rpcpConnectionString"];
			}
			set
			{
				base["rpcpConnectionString"] = value;
			}
		}

		public CustomConnectionStringSection()
		{
		}
	}
}