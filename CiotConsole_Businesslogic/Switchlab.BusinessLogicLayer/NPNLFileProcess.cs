using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Switchlab.BusinessLogicLayer
{
	public class NPNLFileProcess
	{
		private const string SP_UNZIP_NP_FILE = "unzip_np_data";

		private const string SP_FILE_IS_EXIST = "file_is_exist";

		private const string SP_FILE_LIST_ON_FOLDER = "file_list_on_folder";

		public NPNLFileProcess()
		{
		}

		public static bool FileIsExistFromSQL(string filename)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNLTempConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@file", SqlDbType.VarChar, 0, ParameterDirection.Input, filename);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "file_is_exist");
			if (GeneralConverter.ToInt32(sQLDataAccessLayer.ExecuteQuery(sqlCommand).Tables[0].Rows[0]["isExist"]) == 1)
			{
				return true;
			}
			return false;
		}

		public static DataSet FileListOnFolder(string foldername)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNLTempConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@folder", SqlDbType.VarChar, 0, ParameterDirection.Input, foldername);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "file_list_on_folder");
			return sQLDataAccessLayer.ExecuteQuery(sqlCommand);
		}

		public static bool MoveFileToFolder(string fromFullFileName, string toFullFileName, ref string errDesc)
		{
			if (NPNLFileProcess.FileIsExistFromSQL(fromFullFileName))
			{
				errDesc = string.Format("file {0} are not exists.", fromFullFileName);
				return false;
			}
			File.Move(fromFullFileName, toFullFileName);
			if (!NPNLFileProcess.FileIsExistFromSQL(toFullFileName))
			{
				return true;
			}
			errDesc = "move file failed.";
			return false;
		}

		public static bool UnZFileFromSQL(string fixfilenamez, string mobfilenamez, ref string errDesc)
		{
			bool flag;
			if (fixfilenamez.Substring(fixfilenamez.Length - 3, 2).ToLower() == ".z")
			{
				errDesc = "fix filename not proper. unZ applied to .Z file only";
				return false;
			}
			if (mobfilenamez.Substring(mobfilenamez.Length - 3, 2).ToLower() == ".z")
			{
				errDesc = "mobile filename not proper. unZ applied to .Z file only";
				return false;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNLTempConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fixfilez", SqlDbType.VarChar, 0, ParameterDirection.Input, fixfilenamez);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobfilez", SqlDbType.VarChar, 0, ParameterDirection.Input, mobfilenamez);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@svcfilez", SqlDbType.VarChar, 0, ParameterDirection.Input, "");
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "unzip_np_data");
			try
			{
				sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
				return true;
			}
			catch (Exception exception)
			{
				errDesc = exception.Message;
				flag = false;
			}
			return flag;
		}
	}
}