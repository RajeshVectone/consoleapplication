using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMModulesPages
	{
		private const string SP_LIST_MODULE_PAGE = "modules_listmodulepages";

		private const string SP_CLEAR_MODULE_PAGE = "modules_clearmodulepages";

		private const string SP_CREATE_MODULE_PAGE = "modules_createmodulepages";

		private const string SP_LIST_GROUP_MODULE_PAGE = "list_modul_page";

		private int _imodulepage_id;

		private int _imodule_id;

		private int _ipage_id;

		private string _ititle;

		private string _iurl;

		private int _iparentmenu;

		private string _idisplayname;

		private int _isortorder;

		public string DisplayName
		{
			get
			{
				if (this._idisplayname == null)
				{
					return string.Empty;
				}
				return this._idisplayname;
			}
		}

		public int ModuleID
		{
			get
			{
				return this._imodule_id;
			}
			set
			{
				this._imodule_id = value;
			}
		}

		public int ModulePageID
		{
			get
			{
				return this._imodulepage_id;
			}
			set
			{
				this._imodulepage_id = value;
			}
		}

		public int PageID
		{
			get
			{
				return this._ipage_id;
			}
			set
			{
				this._ipage_id = value;
			}
		}

		public int ParentMenu
		{
			get
			{
				return this._iparentmenu;
			}
			set
			{
				this._iparentmenu = value;
			}
		}

		public int SortOrder
		{
			get
			{
				return this._isortorder;
			}
			set
			{
				this._isortorder = value;
			}
		}

		public string Title
		{
			get
			{
				if (this._ititle == null)
				{
					return string.Empty;
				}
				return this._ititle;
			}
		}

		public string Url
		{
			get
			{
				if (this._iurl == null)
				{
					return string.Empty;
				}
				return this._iurl;
			}
		}

		public CRMModulesPages()
		{
		}

		public CRMModulesPages(int iModulePageID, int iModuleID, int iPageID)
		{
			if (iModulePageID < 0)
			{
				throw new ArgumentOutOfRangeException("Module Page ID");
			}
			if (iModuleID < 0)
			{
				throw new ArgumentOutOfRangeException("Module ID");
			}
			if (iPageID < 0)
			{
				throw new ArgumentOutOfRangeException("Page ID");
			}
			this._imodulepage_id = iModulePageID;
			this._imodule_id = iModuleID;
			this._ipage_id = iPageID;
		}

		public CRMModulesPages(int iModulePageID, int iModuleID, int iPageID, string title, string url, int parentMenu, string displayName, int sortOrder)
		{
			if (iModulePageID < 0)
			{
				throw new ArgumentOutOfRangeException("Module Page ID");
			}
			if (iModuleID < 0)
			{
				throw new ArgumentOutOfRangeException("Module ID");
			}
			if (iPageID < 0)
			{
				throw new ArgumentOutOfRangeException("Page ID");
			}
			this._imodulepage_id = iModulePageID;
			this._imodule_id = iModuleID;
			this._ipage_id = iPageID;
			this._ititle = title;
			this._iurl = url;
			this._iparentmenu = parentMenu;
			this._idisplayname = displayName;
			this._isortorder = sortOrder;
		}

		public static bool ClearModulePageByModuleID(int iModuleID)
		{
			if (iModuleID < 0)
			{
				throw new ArgumentOutOfRangeException("ModuleID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@module_id", SqlDbType.Int, 0, ParameterDirection.Input, iModuleID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 0, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_clearmodulepages");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			if (Convert.ToInt32(sqlCommand.Parameters["@ReturnValue"].Value) == 1)
			{
				return true;
			}
			return false;
		}

		public static bool CreateModulePage(int iModuleID, int iPageID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@module_id", SqlDbType.Int, 0, ParameterDirection.Input, iModuleID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@page_id", SqlDbType.Int, 0, ParameterDirection.Input, iPageID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_createmodulepages");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		private static void GenerateListModulePageFromReader<T>(IDataReader returnData, ref List<CRMModulesPages> listItem)
		{
			while (returnData.Read())
			{
				CRMModulesPages cRMModulesPage = new CRMModulesPages(GeneralConverter.ToInt32(returnData["modulepage_id"]), GeneralConverter.ToInt32(returnData["module_id"]), GeneralConverter.ToInt32(returnData["page_id"]));
				listItem.Add(cRMModulesPage);
			}
		}

		private static void GenerateListModulePagesFromReader<T>(IDataReader returnData, ref List<CRMModulesPages> listItem)
		{
			while (returnData.Read())
			{
				CRMModulesPages cRMModulesPage = new CRMModulesPages(GeneralConverter.ToInt32(returnData["modulepage_id"]), GeneralConverter.ToInt32(returnData["module_id"]), GeneralConverter.ToInt32(returnData["page_id"]), GeneralConverter.ToString(returnData["title"]), GeneralConverter.ToString(returnData["url"]), GeneralConverter.ToInt32(returnData["parentmenu"]), GeneralConverter.ToString(returnData["displayname"]), GeneralConverter.ToInt32(returnData["sortorder"]));
				listItem.Add(cRMModulesPage);
			}
		}

		public static List<CRMModulesPages> GetByModuleID(int iModuleID)
		{
			if (iModuleID < 0)
			{
				throw new ArgumentOutOfRangeException("ModuleID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@module_id", SqlDbType.Int, 0, ParameterDirection.Input, iModuleID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_listmodulepages");
			List<CRMModulesPages> cRMModulesPages = new List<CRMModulesPages>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMModulesPages>(sqlCommand, new GenerateListFromReader<CRMModulesPages>(CRMModulesPages.GenerateListModulePageFromReader<CRMModulesPages>), ref cRMModulesPages);
			return cRMModulesPages;
		}

		public static List<CRMModulesPages> listAllPagesModule(int iParentID, int iModuleID)
		{
			if (iModuleID < 0)
			{
				throw new ArgumentOutOfRangeException("ModuleID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@parentid", SqlDbType.Int, 0, ParameterDirection.Input, iParentID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@moduleid", SqlDbType.Int, 0, ParameterDirection.Input, iModuleID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "list_modul_page");
			List<CRMModulesPages> cRMModulesPages = new List<CRMModulesPages>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMModulesPages>(sqlCommand, new GenerateListFromReader<CRMModulesPages>(CRMModulesPages.GenerateListModulePagesFromReader<CRMModulesPages>), ref cRMModulesPages);
			return cRMModulesPages;
		}
	}
}