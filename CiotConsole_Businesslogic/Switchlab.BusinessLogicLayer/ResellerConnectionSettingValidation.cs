using System;
using System.Collections;
using System.Configuration;

namespace Switchlab.BusinessLogicLayer
{
	public class ResellerConnectionSettingValidation : ConfigurationValidatorBase
	{
		public ResellerConnectionSettingValidation()
		{
		}

		public override bool CanValidate(Type type)
		{
			return type == typeof(ResellerConnectionSettingCollection);
		}

		public override void Validate(object value)
		{
			ResellerConnectionSettingCollection resellerConnectionSettingCollection = value as ResellerConnectionSettingCollection;
			if (resellerConnectionSettingCollection != null)
			{
				foreach (ResellerConnectionSetting resellerConnectionSetting in resellerConnectionSettingCollection)
				{
					if (string.IsNullOrEmpty(resellerConnectionSetting.EspConnectionString))
					{
						throw new ConfigurationErrorsException("ESP was not defined in the connection string.");
					}
					if (!string.IsNullOrEmpty(resellerConnectionSetting.ResellerId))
					{
						continue;
					}
					throw new InvalidOperationException("Reseller ID could not be found.");
				}
			}
		}
	}
}