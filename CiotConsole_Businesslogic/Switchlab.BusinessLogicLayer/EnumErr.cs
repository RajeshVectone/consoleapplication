using System;
using System.Collections;

namespace Switchlab.BusinessLogicLayer
{
	public class EnumErr
	{
		public EnumErr()
		{
		}

		public static ArrayList RetrieveErrStatus()
		{
			ArrayList arrayLists = new ArrayList();
			arrayLists.Add(new EnumErr.ErrStatus(0, "OK"));
			arrayLists.Add(new EnumErr.ErrStatus(101, "Rufnummer nicht aktiv"));
			arrayLists.Add(new EnumErr.ErrStatus(102, "Kundendaten falsch"));
			arrayLists.Add(new EnumErr.ErrStatus(103, "Kunden kategorie falsch"));
			arrayLists.Add(new EnumErr.ErrStatus(104, "RN korrespondiertnicht mit Portiercode"));
			arrayLists.Add(new EnumErr.ErrStatus(105, "Portiercode ungültig"));
			arrayLists.Add(new EnumErr.ErrStatus(106, "Portierdatum nicht möglich"));
			arrayLists.Add(new EnumErr.ErrStatus(107, "Portiercode abgelaufen"));
			arrayLists.Add(new EnumErr.ErrStatus(108, "CancellationCode ungültig"));
			arrayLists.Add(new EnumErr.ErrStatus(109, "PointOfNoReturn 1 erreicht"));
			arrayLists.Add(new EnumErr.ErrStatus(110, "Portierkapazität überschritten"));
			arrayLists.Add(new EnumErr.ErrStatus(111, "Portierung bereits angestoßen"));
			arrayLists.Add(new EnumErr.ErrStatus(112, "Kein Prepaid Guthaben"));
			arrayLists.Add(new EnumErr.ErrStatus(113, "RN ungültig"));
			arrayLists.Add(new EnumErr.ErrStatus(114, "NÜV-Info verweigert"));
			arrayLists.Add(new EnumErr.ErrStatus(115, "Portierung verweigert"));
			arrayLists.Add(new EnumErr.ErrStatus(116, "Fehlende Haupt-MSISDN"));
			arrayLists.Add(new EnumErr.ErrStatus(117, "Fehlende VoiceBox-MSISDN"));
			arrayLists.Add(new EnumErr.ErrStatus(118, "Ungültige Routing Action"));
			arrayLists.Add(new EnumErr.ErrStatus(119, "Fehler in Routingliste aufgetreten"));
			arrayLists.Add(new EnumErr.ErrStatus(120, "Fehler in Routingliste aufgetreten"));
			arrayLists.Add(new EnumErr.ErrStatus(121, "Fehlender Routing Request"));
			arrayLists.Add(new EnumErr.ErrStatus(122, "Rückgabe nicht vollständig"));
			arrayLists.Add(new EnumErr.ErrStatus(123, "Falscher Range Holder"));
			arrayLists.Add(new EnumErr.ErrStatus(124, "Durchführungsauftrag unvollständig"));
			arrayLists.Add(new EnumErr.ErrStatus(125, "Rückgabe nicht erlaubt"));
			arrayLists.Add(new EnumErr.ErrStatus(200, "Strecke ungültig"));
			arrayLists.Add(new EnumErr.ErrStatus(201, "Nummer nicht portierbar"));
			arrayLists.Add(new EnumErr.ErrStatus(202, "GK NÜVInfo verweigert"));
			arrayLists.Add(new EnumErr.ErrStatus(203, "Prüfrufnummer nicht gültig"));
			arrayLists.Add(new EnumErr.ErrStatus(204, "Vollmacht nicht gültig"));
			arrayLists.Add(new EnumErr.ErrStatus(205, "E-Mail falsch"));
			arrayLists.Add(new EnumErr.ErrStatus(206, "Kein Großkunde"));
			arrayLists.Add(new EnumErr.ErrStatus(999, "Sonstige"));
			return arrayLists;
		}

		public static ArrayList RetrieveErrStatus(int blankItemPosition, string blankItemText)
		{
			ArrayList arrayLists = EnumErr.RetrieveErrStatus();
			arrayLists.Insert(blankItemPosition, new EnumErr.ErrStatus(-1, blankItemText));
			return arrayLists;
		}

		public static string ToText(int oValue)
		{
			string text;
			IEnumerator enumerator = EnumErr.RetrieveErrStatus().GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					EnumErr.ErrStatus current = (EnumErr.ErrStatus)enumerator.Current;
					if (current.Value != oValue)
					{
						continue;
					}
					text = current.Text;
					return text;
				}
				throw new Exception("Value not found.");
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return text;
		}

		public static int ToValue(string oName)
		{
			int value;
			IEnumerator enumerator = EnumErr.RetrieveErrStatus().GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					EnumErr.ErrStatus current = (EnumErr.ErrStatus)enumerator.Current;
					if (current.Text != oName)
					{
						continue;
					}
					value = current.Value;
					return value;
				}
				throw new Exception("Text not found.");
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return value;
		}

		public enum EnumErrStatus
		{
			OK = 0,
			Rufnummernichtaktiv = 101,
			Kundendatenfalsch = 102,
			Kundenkategoriefalsch = 103,
			RNkorrespondiertnichtmitPortiercode = 104,
			Portiercodeungültig = 105,
			Portierdatumnichtmöglich = 106,
			Portiercodeabgelaufen = 107,
			CancellationCodeungültig = 108,
			PointOfNoReturn1erreicht = 109,
			Portierkapazitätüberschritten = 110,
			Portierungbereitsangestoßen = 111,
			KeinPrepaidGuthaben = 112,
			RNungültig = 113,
			NÜVInfoverweigert = 114,
			Portierungverweigert = 115,
			FehlendeHauptMSISDN = 116,
			FehlendeVoiceBoxMSISDN = 117,
			UngültigeRoutingAction = 118,
			FehlerinRoutinglisteaufgetreten = 119,
			FehlenderPortrequest = 120,
			FehlenderRoutingRequest = 121,
			Rückgabenichtvollständig = 122,
			FalscherRangeHolder = 123,
			Durchführungsauftragunvollständig = 124,
			Rückgabenichterlaubt = 125,
			Streckeungültig = 200,
			Nummernichtportierbar = 201,
			GKNÜVInfoverweigert = 202,
			Prüfrufnummernichtgültig = 203,
			Vollmachtnichtgültig = 204,
			EMailfalsch = 205,
			KeinGroßkunde = 206,
			Sonstige = 999
		}

		public class ErrStatus
		{
			private int _value;

			private string _text;

			public string Text
			{
				get
				{
					return this._text;
				}
				set
				{
					this._text = value;
				}
			}

			public int Value
			{
				get
				{
					return this._value;
				}
				set
				{
					this._value = value;
				}
			}

			public ErrStatus(int value, string text)
			{
				this._value = value;
				this._text = text;
			}
		}
	}
}