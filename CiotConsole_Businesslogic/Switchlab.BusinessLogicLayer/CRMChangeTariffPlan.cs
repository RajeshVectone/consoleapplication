using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class CRMChangeTariffPlan
	{
		private const string SP_LIST_BY_NO = "cs_logTariffPlan_load";

		private const string SP_CHECK_LIMIT = "cs_check_account_limit";

		private const string SP_REF_TARIFF = "cs_Ref_TariffClass";

		private const string SP_LOAD_BALANCE = "cs_account_balance_load";

		private const string SP_CREATE = "cs_change_tariffplan_insert";

		private const string SP_UPDATE = "cs_tariffclass_mvnoaccount_updt";

		private int _logid;

		private DateTime _date;

		private double _amount;

		private string _currcode;

		private int _transid;

		private int _transactiontype;

		private int _transstatus;

		private string _username;

		private string _notes;

		private int _limit;

		private string _trffclass;

		private string _trffclassname;

		private string _balance;

		public double Amount
		{
			get
			{
				return this._amount;
			}
		}

		public string Balance
		{
			get
			{
				if (this._balance == null)
				{
					return string.Empty;
				}
				return this._balance;
			}
		}

		public string CurrCode
		{
			get
			{
				if (this._currcode == null)
				{
					return string.Empty;
				}
				return this._currcode;
			}
		}

		public DateTime Date
		{
			get
			{
				return this._date;
			}
		}

		public int Limit
		{
			get
			{
				return this._limit;
			}
		}

		public int LogId
		{
			get
			{
				return this._logid;
			}
		}

		public string Notes
		{
			get
			{
				if (this._notes == null)
				{
					return string.Empty;
				}
				return this._notes;
			}
		}

		public int TransId
		{
			get
			{
				return this._transid;
			}
		}

		public int TransStatus
		{
			get
			{
				return this._transstatus;
			}
		}

		public int TransType
		{
			get
			{
				return this._transactiontype;
			}
		}

		public string Trffclass
		{
			get
			{
				if (this._trffclass == null)
				{
					return string.Empty;
				}
				return this._trffclass;
			}
		}

		public string TrffclassName
		{
			get
			{
				if (this._trffclassname == null)
				{
					return string.Empty;
				}
				return this._trffclassname;
			}
		}

		public string UserName
		{
			get
			{
				if (this._username == null)
				{
					return string.Empty;
				}
				return this._username;
			}
		}

		public CRMChangeTariffPlan()
		{
		}

		public CRMChangeTariffPlan(int logId, DateTime date, double amount, string currCode, int transId, int transType, int transStatus, string userName, string Notes)
		{
			this._logid = this.LogId;
			this._date = date;
			this._amount = amount;
			this._currcode = currCode;
			this._transid = transId;
			this._transactiontype = transType;
			this._transstatus = transStatus;
			this._username = userName;
			this._notes = Notes;
		}

		public CRMChangeTariffPlan(int limit)
		{
			this._limit = limit;
		}

		public CRMChangeTariffPlan(string trffclass, string trffclassName)
		{
			this._trffclass = trffclass;
			this._trffclassname = trffclassName;
		}

		public CRMChangeTariffPlan(string trffclass, string trffclassName, string balance)
		{
			this._trffclass = trffclass;
			this._trffclassname = trffclassName;
			this._balance = balance;
		}

		public static CRMChangeTariffPlan CheckLimit(string iMobileNo)
		{
			if (iMobileNo == string.Empty)
			{
				iMobileNo = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileNo", SqlDbType.VarChar, 16, ParameterDirection.Input, iMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cs_logTariffPlan_load");
			List<CRMChangeTariffPlan> cRMChangeTariffPlans = new List<CRMChangeTariffPlan>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMChangeTariffPlan>(sqlCommand, new GenerateListFromReader<CRMChangeTariffPlan>(CRMChangeTariffPlan.GenerateLimitListFromReader<CRMChangeTariffPlan>), ref cRMChangeTariffPlans);
			if (cRMChangeTariffPlans.Count <= 0)
			{
				return null;
			}
			return cRMChangeTariffPlans[0];
		}

		public static bool CreateLog(string iMobileNo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileNo", SqlDbType.VarChar, 16, ParameterDirection.Input, iMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cs_change_tariffplan_insert");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		private static void GenerateLimitListFromReader<T>(IDataReader returnData, ref List<CRMChangeTariffPlan> listNew)
		{
			while (returnData.Read())
			{
				if (returnData.IsDBNull(0))
				{
					continue;
				}
				CRMChangeTariffPlan cRMChangeTariffPlan = new CRMChangeTariffPlan(GeneralConverter.ToInt32(returnData["accountlimit"]));
				listNew.Add(cRMChangeTariffPlan);
			}
		}

		private static void GenerateLogListFromReader<T>(IDataReader returnData, ref List<CRMChangeTariffPlan> listNew)
		{
			while (returnData.Read())
			{
				CRMChangeTariffPlan cRMChangeTariffPlan = new CRMChangeTariffPlan(GeneralConverter.ToInt32(returnData["logID"]), GeneralConverter.ToDateTime(returnData["date"]), GeneralConverter.ToDouble(returnData["amount"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToInt32(returnData["transID"]), GeneralConverter.ToInt32(returnData["transactiontype"]), GeneralConverter.ToInt32(returnData["transstatus"]), GeneralConverter.ToString(returnData["username"]), GeneralConverter.ToString(returnData["notes"]));
				listNew.Add(cRMChangeTariffPlan);
			}
		}

		private static void GenerateRefTariffListFromReader<T>(IDataReader returnData, ref List<CRMChangeTariffPlan> listNew)
		{
			while (returnData.Read())
			{
				CRMChangeTariffPlan cRMChangeTariffPlan = new CRMChangeTariffPlan(GeneralConverter.ToString(returnData["trffclass"]), GeneralConverter.ToString(returnData["name"]), GeneralConverter.ToString(returnData["balance"]));
				listNew.Add(cRMChangeTariffPlan);
			}
		}

		public static List<CRMChangeTariffPlan> ListByNo(string iMobileNo)
		{
			if (iMobileNo == string.Empty)
			{
				iMobileNo = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileNo", SqlDbType.VarChar, 16, ParameterDirection.Input, iMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cs_logTariffPlan_load");
			List<CRMChangeTariffPlan> cRMChangeTariffPlans = new List<CRMChangeTariffPlan>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMChangeTariffPlan>(sqlCommand, new GenerateListFromReader<CRMChangeTariffPlan>(CRMChangeTariffPlan.GenerateLogListFromReader<CRMChangeTariffPlan>), ref cRMChangeTariffPlans);
			return cRMChangeTariffPlans;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMChangeTariffPlan> listRefTariff(string iMobileNo)
		{
			if (iMobileNo == string.Empty)
			{
				iMobileNo = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileNo", SqlDbType.VarChar, 16, ParameterDirection.Input, iMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cs_Ref_TariffClass");
			List<CRMChangeTariffPlan> cRMChangeTariffPlans = new List<CRMChangeTariffPlan>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMChangeTariffPlan>(sqlCommand, new GenerateListFromReader<CRMChangeTariffPlan>(CRMChangeTariffPlan.GenerateRefTariffListFromReader<CRMChangeTariffPlan>), ref cRMChangeTariffPlans);
			return cRMChangeTariffPlans;
		}

		public static CRMChangeTariffPlan LoadBalance(string iMobileNo)
		{
			if (iMobileNo == string.Empty)
			{
				iMobileNo = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileNo", SqlDbType.VarChar, 16, ParameterDirection.Input, iMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cs_account_balance_load");
			List<CRMChangeTariffPlan> cRMChangeTariffPlans = new List<CRMChangeTariffPlan>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMChangeTariffPlan>(sqlCommand, new GenerateListFromReader<CRMChangeTariffPlan>(CRMChangeTariffPlan.GenerateRefTariffListFromReader<CRMChangeTariffPlan>), ref cRMChangeTariffPlans);
			if (cRMChangeTariffPlans.Count <= 0)
			{
				return null;
			}
			return cRMChangeTariffPlans[0];
		}

		public static bool updatePlan(string iMobileNo, string iTariffClass)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileNo", SqlDbType.VarChar, 16, ParameterDirection.Input, iMobileNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@tariffclass", SqlDbType.VarChar, 5, ParameterDirection.Input, iTariffClass);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cs_tariffclass_mvnoaccount_updt");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}
	}
}