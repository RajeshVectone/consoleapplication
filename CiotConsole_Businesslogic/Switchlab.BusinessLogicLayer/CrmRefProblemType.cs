using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class CrmRefProblemType
	{
		private const string SP_LIST = "r_problem_type";

		private int _problemtype;

		private string _ptypename;

		private string _ptypedesc;

		public int ProblemType
		{
			get
			{
				return this._problemtype;
			}
			set
			{
				this._problemtype = value;
			}
		}

		public string ProblemTypeDesc
		{
			get
			{
				if (this._ptypedesc == null)
				{
					return string.Empty;
				}
				return this._ptypedesc;
			}
			set
			{
				this._ptypedesc = value;
			}
		}

		public string ProblemTypeName
		{
			get
			{
				if (this._ptypename == null)
				{
					return string.Empty;
				}
				return this._ptypename;
			}
			set
			{
				this._ptypename = value;
			}
		}

		public CrmRefProblemType()
		{
		}

		public CrmRefProblemType(int ProblemType, string ProblemTypeName, string ProblemTypeDesc)
		{
			this._problemtype = ProblemType;
			this._ptypename = ProblemTypeName;
			this._ptypedesc = ProblemTypeDesc;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CrmRefProblemType> listRef)
		{
			while (returnData.Read())
			{
				CrmRefProblemType crmRefProblemType = new CrmRefProblemType(GeneralConverter.ToInt32(returnData["ProblemType"]), GeneralConverter.ToString(returnData["TypeName"]), GeneralConverter.ToString(returnData["TypeDesc"]));
				listRef.Add(crmRefProblemType);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmRefProblemType> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_problem_type");
			List<CrmRefProblemType> crmRefProblemTypes = new List<CrmRefProblemType>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmRefProblemType>(sqlCommand, new GenerateListFromReader<CrmRefProblemType>(CrmRefProblemType.GenerateListFromReader<CrmRefProblemType>), ref crmRefProblemTypes);
			return crmRefProblemTypes;
		}
	}
}