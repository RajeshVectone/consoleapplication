using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class SampleClass
	{
		private const string SP_LISTALL = "sample_listall";

		private int _SampleID;

		private string _SampleStr;

		public int SampleID
		{
			get
			{
				return this._SampleID;
			}
		}

		public string SampleStr
		{
			get
			{
				if (this._SampleStr == null)
				{
					return string.Empty;
				}
				return this._SampleStr;
			}
		}

		public SampleClass()
		{
		}

		public SampleClass(int SampleID, string SampleStr)
		{
			this._SampleID = SampleID;
			this._SampleStr = SampleStr;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<SampleClass> listNew)
		{
			while (returnData.Read())
			{
				SampleClass sampleClass = new SampleClass(GeneralConverter.ToInt32(returnData["sampleid"]), GeneralConverter.ToString(returnData["samplestr"]));
				listNew.Add(sampleClass);
			}
		}

		public static List<SampleClass> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sample_listall");
			List<SampleClass> sampleClasses = new List<SampleClass>();
			sQLDataAccessLayer.ExecuteReaderCmd<SampleClass>(sqlCommand, new GenerateListFromReader<SampleClass>(SampleClass.GenerateListFromReader<SampleClass>), ref sampleClasses);
			return sampleClasses;
		}
	}
}