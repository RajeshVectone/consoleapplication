using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	[Serializable]
	public class CrmSalesOrder
	{
		protected const string SP_SELECT = "sales_order_select";

		protected const string SP_SELECT_BY_ORDER_STATUS = "sales_order_get_by_status";

		protected const string SP_SELECT_BY_SUBS_NAME = "sales_order_get_by_name";

		protected const string SP_DETAIL = "sales_order_detail";

		protected const string SP_DELETE = "sales_order_delete";

		protected const string SP_INSERT = "sales_order_insert";

		protected const string SP_UPDATE = "sales_order_update";

		protected const string SP_CANCEL_ORDER = "sales_order_cancel";

		protected const string SP_SELECT_COURRIER = "courrier_select";

		protected const string SP_SELECT_STATUS = "ref_order_status_select";

		protected const string SP_SELECT_SUBSCRIBER = "subscriber_select";

		protected const string SP_SELECT_PAYMENT = "payment_select";

		protected const string SP_SELECT_ORDERID = "sales_order_select_orderid";

		private int _orderid;

		private string _deliveryno;

		private string _currcode;

		private int _courrierid;

		private string _deliverytrackno;

		private int _orderstatus;

		private string _deliveryurl;

		private DateTime _deliverydate;

		private double _amount;

		private DateTime _createdate;

		private string _createby;

		private DateTime _validatedate;

		private string _validateby;

		private DateTime _lastupdate;

		private int _subscriberid;

		private string _courriername;

		private string _statusname;

		private string _firstname;

		private string _lastname;

		private string _subscriber_name;

		private double _deliverycost;

		private double _taxcost;

		private double _finalcost;

		private int _paymentid;

		private string _ref_no;

		private int _dibsorderid;

		private long _transactid;

		public double amount
		{
			get
			{
				return this._amount;
			}
		}

		public int courrierid
		{
			get
			{
				return this._courrierid;
			}
		}

		public string courriername
		{
			get
			{
				if (this._courriername == null)
				{
					return string.Empty;
				}
				return this._courriername;
			}
		}

		public string createby
		{
			get
			{
				if (this._createby == null)
				{
					return string.Empty;
				}
				return this._createby;
			}
		}

		public DateTime createdate
		{
			get
			{
				return this._createdate;
			}
		}

		public string currcode
		{
			get
			{
				if (this._currcode == null)
				{
					return string.Empty;
				}
				return this._currcode;
			}
		}

		public double deliverycost
		{
			get
			{
				return this._deliverycost;
			}
		}

		public DateTime deliverydate
		{
			get
			{
				return this._deliverydate;
			}
		}

		public string deliveryno
		{
			get
			{
				if (this._deliveryno == null)
				{
					return string.Empty;
				}
				return this._deliveryno;
			}
		}

		public string deliverytrackno
		{
			get
			{
				if (this._deliverytrackno == null)
				{
					return string.Empty;
				}
				return this._deliverytrackno;
			}
		}

		public string deliveryurl
		{
			get
			{
				if (this._deliveryurl == null)
				{
					return string.Empty;
				}
				return this._deliveryurl;
			}
		}

		public int DIBSorderid
		{
			get
			{
				return this._dibsorderid;
			}
		}

		public double finalcost
		{
			get
			{
				return this._finalcost;
			}
		}

		public string firstname
		{
			get
			{
				if (this._firstname == null)
				{
					return string.Empty;
				}
				return this._firstname;
			}
		}

		public string lastname
		{
			get
			{
				if (this._lastname == null)
				{
					return string.Empty;
				}
				return this._lastname;
			}
		}

		public DateTime lastupdate
		{
			get
			{
				return this._lastupdate;
			}
		}

		public int orderid
		{
			get
			{
				return this._orderid;
			}
		}

		public int orderstatus
		{
			get
			{
				return this._orderstatus;
			}
		}

		public int paymentid
		{
			get
			{
				return this._paymentid;
			}
		}

		public string ref_no
		{
			get
			{
				if (this._ref_no == null)
				{
					return string.Empty;
				}
				return this._ref_no;
			}
		}

		public string statusname
		{
			get
			{
				if (this._statusname == null)
				{
					return string.Empty;
				}
				return this._statusname;
			}
		}

		public string subscriber_name
		{
			get
			{
				if (this._subscriber_name == null)
				{
					return string.Empty;
				}
				return this._subscriber_name;
			}
		}

		public int subscriberid
		{
			get
			{
				return this._subscriberid;
			}
		}

		public double taxcost
		{
			get
			{
				return this._taxcost;
			}
		}

		public long transactid
		{
			get
			{
				return this._transactid;
			}
		}

		public string validateby
		{
			get
			{
				if (this._validateby == null)
				{
					return string.Empty;
				}
				return this._validateby;
			}
		}

		public DateTime validatedate
		{
			get
			{
				return this._validatedate;
			}
		}

		public CrmSalesOrder(int __orderid, int __courrierid, int __orderstatus, string __deliveryno, string __currcode, string __deliverytrackno, string __deliveryurl, DateTime __deliverydate, double __amount, DateTime __createdate, string __createby, DateTime __validatedate, string __validateby, DateTime __lastupdate, int __subscriberid)
		{
			this._orderid = __orderid;
			this._courrierid = __courrierid;
			this._orderstatus = __orderstatus;
			this._deliveryno = __deliveryno;
			this._currcode = __currcode;
			this._deliverytrackno = __deliverytrackno;
			this._deliveryurl = __deliveryurl;
			this._deliverydate = __deliverydate;
			this._amount = __amount;
			this._createdate = __createdate;
			this._createby = __createby;
			this._validatedate = __validatedate;
			this._validateby = __validateby;
			this._lastupdate = __lastupdate;
			this._subscriberid = __subscriberid;
		}

		public CrmSalesOrder(int __orderid, int __courrierid, int __orderstatus, string __deliveryno, string __currcode, string __deliverytrackno, string __deliveryurl, DateTime __deliverydate, double __amount, DateTime __createdate, string __createby, DateTime __validatedate, string __validateby, DateTime __lastupdate, int __subscriberid, string __courriername, string __statusname, string __firstname, string __lastname, string __subscriber_name) : this(__orderid, __courrierid, __orderstatus, __deliveryno, __currcode, __deliverytrackno, __deliveryurl, __deliverydate, __amount, __createdate, __createby, __validatedate, __validateby, __lastupdate, __subscriberid)
		{
			this._courriername = __courriername;
			this._statusname = __statusname;
			this._firstname = __firstname;
			this._lastname = __lastname;
			this._subscriber_name = __subscriber_name;
		}

		public CrmSalesOrder(int __orderid, int __courrierid, int __orderstatus, string __deliveryno, string __currcode, string __deliverytrackno, string __deliveryurl, DateTime __deliverydate, double __amount, DateTime __createdate, string __createby, DateTime __validatedate, string __validateby, DateTime __lastupdate, int __subscriberid, string __courriername, string __statusname, string __firstname, string __lastname, string __subscriber_name, double __deliverycost, double __taxcost, double __finalcost, int __paymentid) : this(__orderid, __courrierid, __orderstatus, __deliveryno, __currcode, __deliverytrackno, __deliveryurl, __deliverydate, __amount, __createdate, __createby, __validatedate, __validateby, __lastupdate, __subscriberid, __courriername, __statusname, __firstname, __lastname, __subscriber_name)
		{
			this._deliverycost = __deliverycost;
			this._taxcost = __taxcost;
			this._finalcost = __finalcost;
			this._paymentid = __paymentid;
		}

		public CrmSalesOrder(int __orderid, int __courrierid, int __orderstatus, string __deliveryno, string __currcode, string __deliverytrackno, string __deliveryurl, DateTime __deliverydate, double __amount, DateTime __createdate, string __createby, DateTime __validatedate, string __validateby, DateTime __lastupdate, int __subscriberid, string __courriername, string __statusname, string __firstname, string __lastname, string __subscriber_name, double __deliverycost, double __taxcost, double __finalcost, int __paymentid, int __dibsorderid, long __transactid) : this(__orderid, __courrierid, __orderstatus, __deliveryno, __currcode, __deliverytrackno, __deliveryurl, __deliverydate, __amount, __createdate, __createby, __validatedate, __validateby, __lastupdate, __subscriberid, __courriername, __statusname, __firstname, __lastname, __subscriber_name)
		{
			this._deliverycost = __deliverycost;
			this._taxcost = __taxcost;
			this._finalcost = __finalcost;
			this._paymentid = __paymentid;
			this._dibsorderid = __dibsorderid;
			this._transactid = __transactid;
		}

		public CrmSalesOrder(int __courrierid, int __orderstatus, string __deliveryno, string __currcode, string __deliverytrackno, string __deliveryurl, DateTime __deliverydate, double __amount, DateTime __createdate, string __createby, DateTime __validatedate, string __validateby, DateTime __lastupdate, int __subscriberid) : this(0, __courrierid, __orderstatus, __deliveryno, __currcode, __deliverytrackno, __deliveryurl, __deliverydate, __amount, __createdate, __createby, __validatedate, __validateby, __lastupdate, __subscriberid)
		{
		}

		public CrmSalesOrder(int __courrierid, int __orderstatus, string __deliveryno, string __currcode, string __deliverytrackno, string __deliveryurl, DateTime __deliverydate, double __amount, DateTime __createdate, string __createby, DateTime __validatedate, string __validateby, DateTime __lastupdate, int __subscriberid, double __deliverycost, double __taxcost, double __finalcost, int __paymentid) : this(__courrierid, __orderstatus, __deliveryno, __currcode, __deliverytrackno, __deliveryurl, __deliverydate, __amount, __createdate, __createby, __validatedate, __validateby, __lastupdate, __subscriberid)
		{
			this._deliverycost = __deliverycost;
			this._taxcost = __taxcost;
			this._finalcost = __finalcost;
			this._paymentid = __paymentid;
		}

		public CrmSalesOrder() : this(0, 0, null, null, null, null, DateTime.MinValue, 0, DateTime.MinValue, null, DateTime.MinValue, null, DateTime.MinValue, 0)
		{
		}

		protected CrmSalesOrder(CrmSalesOrder.TABLE __table, int __id, string __name)
		{
			switch (__table)
			{
				case CrmSalesOrder.TABLE.COURRIER:
				{
					this._courrierid = __id;
					this._courriername = __name;
					return;
				}
				case CrmSalesOrder.TABLE.STATUS:
				{
					this._orderstatus = __id;
					this._statusname = __name;
					return;
				}
				case CrmSalesOrder.TABLE.SUBSCRIBER:
				{
					this._subscriberid = __id;
					this._subscriber_name = __name;
					return;
				}
				case CrmSalesOrder.TABLE.PAYMENT:
				{
					this._paymentid = __id;
					this._ref_no = __name;
					return;
				}
				default:
				{
					return;
				}
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static IListSource _getAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_select");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(CrmSalesOrder.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static IListSource _getById(int __orderid)
		{
			if (__orderid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_detail");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(CrmSalesOrder.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Delete, true)]
		public static bool Cancel(int __orderid)
		{
			if (__orderid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_cancel");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Delete, true)]
		public static bool Delete(int __orderid)
		{
			if (__orderid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_delete");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateDetailFromReader<T>(IDataReader returnData, ref List<CrmSalesOrder> theList)
		{
			while (returnData.Read())
			{
				CrmSalesOrder crmSalesOrder = new CrmSalesOrder(GeneralConverter.ToInt32(returnData["orderid"]), GeneralConverter.ToInt32(returnData["courrierid"]), GeneralConverter.ToInt32(returnData["orderstatus"]), GeneralConverter.ToString(returnData["deliveryno"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToString(returnData["deliverytrackno"]), GeneralConverter.ToString(returnData["deliveryurl"]), GeneralConverter.ToDateTime(returnData["deliverydate"]), GeneralConverter.ToDouble(returnData["amount"]), GeneralConverter.ToDateTime(returnData["createdate"]), GeneralConverter.ToString(returnData["createby"]), GeneralConverter.ToDateTime(returnData["validatedate"]), GeneralConverter.ToString(returnData["validateby"]), GeneralConverter.ToDateTime(returnData["lastupdate"]), GeneralConverter.ToInt32(returnData["subscriberid"]), GeneralConverter.ToString(returnData["courriername"]), GeneralConverter.ToString(returnData["statusname"]), GeneralConverter.ToString(returnData["firstname"]), GeneralConverter.ToString(returnData["lastname"]), GeneralConverter.ToString(returnData["subscriber_name"]), GeneralConverter.ToDouble(returnData["deliverycost"]), GeneralConverter.ToDouble(returnData["taxcost"]), GeneralConverter.ToDouble(returnData["finalcost"]), GeneralConverter.ToInt32(returnData["paymentid"]), GeneralConverter.ToInt32(returnData["DIBS_order_id"]), GeneralConverter.ToLong(returnData["transact_id"]));
				theList.Add(crmSalesOrder);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CrmSalesOrder> theList)
		{
			while (returnData.Read())
			{
				CrmSalesOrder crmSalesOrder = new CrmSalesOrder(GeneralConverter.ToInt32(returnData["orderid"]), GeneralConverter.ToInt32(returnData["courrierid"]), GeneralConverter.ToInt32(returnData["orderstatus"]), GeneralConverter.ToString(returnData["deliveryno"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToString(returnData["deliverytrackno"]), GeneralConverter.ToString(returnData["deliveryurl"]), GeneralConverter.ToDateTime(returnData["deliverydate"]), GeneralConverter.ToDouble(returnData["amount"]), GeneralConverter.ToDateTime(returnData["createdate"]), GeneralConverter.ToString(returnData["createby"]), GeneralConverter.ToDateTime(returnData["validatedate"]), GeneralConverter.ToString(returnData["validateby"]), GeneralConverter.ToDateTime(returnData["lastupdate"]), GeneralConverter.ToInt32(returnData["subscriberid"]), GeneralConverter.ToString(returnData["courriername"]), GeneralConverter.ToString(returnData["statusname"]), GeneralConverter.ToString(returnData["firstname"]), GeneralConverter.ToString(returnData["lastname"]), GeneralConverter.ToString(returnData["subscriber_name"]), GeneralConverter.ToDouble(returnData["deliverycost"]), GeneralConverter.ToDouble(returnData["taxcost"]), GeneralConverter.ToDouble(returnData["finalcost"]), GeneralConverter.ToInt32(returnData["paymentid"]));
				theList.Add(crmSalesOrder);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<IListSource> theList)
		{
			DataTable dataTable = new DataTable();
			dataTable.Load(returnData);
			theList.Add(dataTable);
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmSalesOrder> GetAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_select");
			List<CrmSalesOrder> crmSalesOrders = new List<CrmSalesOrder>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrder>(sqlCommand, new GenerateListFromReader<CrmSalesOrder>(CrmSalesOrder.GenerateListFromReader<CrmSalesOrder>), ref crmSalesOrders);
			return crmSalesOrders;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmSalesOrder> GetByOrderStatus(int iOrderStatus)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderstatus", SqlDbType.Int, 0, ParameterDirection.Input, iOrderStatus);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_get_by_status");
			List<CrmSalesOrder> crmSalesOrders = new List<CrmSalesOrder>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrder>(sqlCommand, new GenerateListFromReader<CrmSalesOrder>(CrmSalesOrder.GenerateListFromReader<CrmSalesOrder>), ref crmSalesOrders);
			return crmSalesOrders;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmSalesOrder> GetBySubscriberName(string sName)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 61, ParameterDirection.Input, sName);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_get_by_name");
			List<CrmSalesOrder> crmSalesOrders = new List<CrmSalesOrder>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrder>(sqlCommand, new GenerateListFromReader<CrmSalesOrder>(CrmSalesOrder.GenerateListFromReader<CrmSalesOrder>), ref crmSalesOrders);
			return crmSalesOrders;
		}

		[DataObjectMethod(DataObjectMethodType.Insert, true)]
		public static int Insert(int iCourierID, int iOrderStatus, double dAmmount, double dDeliveryCost, double dTaxCost, double dFinalCost, string sCurrCode, int iSubscriberID, int iPaymentID, string sCreateBy, int iDibsID, long iTransactID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@courrierid", SqlDbType.Int, 0, ParameterDirection.Input, iCourierID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderstatus", SqlDbType.Int, 0, ParameterDirection.Input, iOrderStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@amount", SqlDbType.Float, 0, ParameterDirection.Input, dAmmount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@createby", SqlDbType.VarChar, 16, ParameterDirection.Input, sCreateBy);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscriberid", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@deliverycost", SqlDbType.Float, 0, ParameterDirection.Input, dDeliveryCost);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@taxcost", SqlDbType.Float, 0, ParameterDirection.Input, dTaxCost);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@finalcost", SqlDbType.Float, 0, ParameterDirection.Input, dFinalCost);
			if (iPaymentID == 0)
			{
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@paymentid", SqlDbType.Int, 0, ParameterDirection.Input, DBNull.Value);
			}
			else
			{
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@paymentid", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentID);
			}
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@dibsID", SqlDbType.Int, 0, ParameterDirection.Input, iDibsID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transactID", SqlDbType.BigInt, 0, ParameterDirection.Input, iTransactID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_insert");
			return Convert.ToInt32(sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand));
		}

		[DataObjectMethod(DataObjectMethodType.Update, true)]
		public static bool Update(int __orderid, int __orderstatus, string __deliveryno, string __deliverytrackno, string __deliveryurl, string __validateby)
		{
			if (__orderid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 0, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderstatus", SqlDbType.Int, 0, ParameterDirection.Input, __orderstatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@deliveryno", SqlDbType.VarChar, 32, ParameterDirection.Input, __deliveryno);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@deliverytrackno", SqlDbType.VarChar, 32, ParameterDirection.Input, __deliverytrackno);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@deliveryurl", SqlDbType.VarChar, 64, ParameterDirection.Input, __deliveryurl);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@validateby", SqlDbType.VarChar, 16, ParameterDirection.Input, __validateby);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_update");
			int num = sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int num1 = GeneralConverter.ToInt32(sqlCommand.Parameters["@ReturnValue"].Value);
			if (num > 0 && num1 != -1)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static CrmSalesOrder ViewById(int __orderid)
		{
			if (__orderid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_detail");
			List<CrmSalesOrder> crmSalesOrders = new List<CrmSalesOrder>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrder>(sqlCommand, new GenerateListFromReader<CrmSalesOrder>(CrmSalesOrder.GenerateDetailFromReader<CrmSalesOrder>), ref crmSalesOrders);
			if (crmSalesOrders.Count <= 0)
			{
				return null;
			}
			return crmSalesOrders[0];
		}

		protected enum TABLE
		{
			COURRIER,
			STATUS,
			SUBSCRIBER,
			PAYMENT
		}
	}
}