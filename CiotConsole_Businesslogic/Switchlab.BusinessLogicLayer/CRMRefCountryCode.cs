using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class CRMRefCountryCode
	{
		private const string SP_GET_COUNTRY_LIST = "country_list";

		private const string SP_GET_COUNTRY_BY_ID = "country_get_by_id";

		private string _scountrycode;

		private string _scountryname;

		public string CountryCode
		{
			get
			{
				if (this._scountrycode == null)
				{
					return string.Empty;
				}
				return this._scountrycode;
			}
			set
			{
				this._scountrycode = value;
			}
		}

		public string CountryName
		{
			get
			{
				if (this._scountryname == null)
				{
					return string.Empty;
				}
				return this._scountryname;
			}
			set
			{
				this._scountryname = value;
			}
		}

		public CRMRefCountryCode()
		{
		}

		public CRMRefCountryCode(string sCountryCode, string sCountryName)
		{
			this._scountrycode = sCountryCode;
			this._scountryname = sCountryName;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CRMRefCountryCode> listRef)
		{
			while (returnData.Read())
			{
				CRMRefCountryCode cRMRefCountryCode = new CRMRefCountryCode(GeneralConverter.ToString(returnData["countrycode"]), GeneralConverter.ToString(returnData["countryname"]));
				listRef.Add(cRMRefCountryCode);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static string GetCountryNameByID(string sCountryCode)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countrycode", SqlDbType.VarChar, 2, ParameterDirection.Input, sCountryCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "country_get_by_id");
			List<CRMRefCountryCode> cRMRefCountryCodes = new List<CRMRefCountryCode>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRefCountryCode>(sqlCommand, new GenerateListFromReader<CRMRefCountryCode>(CRMRefCountryCode.GenerateListFromReader<CRMRefCountryCode>), ref cRMRefCountryCodes);
			if (cRMRefCountryCodes.Count <= 0)
			{
				return string.Empty;
			}
			return cRMRefCountryCodes[0].CountryName;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMRefCountryCode> GetListCountry()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "country_list");
			List<CRMRefCountryCode> cRMRefCountryCodes = new List<CRMRefCountryCode>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRefCountryCode>(sqlCommand, new GenerateListFromReader<CRMRefCountryCode>(CRMRefCountryCode.GenerateListFromReader<CRMRefCountryCode>), ref cRMRefCountryCodes);
			return cRMRefCountryCodes;
		}
	}
}