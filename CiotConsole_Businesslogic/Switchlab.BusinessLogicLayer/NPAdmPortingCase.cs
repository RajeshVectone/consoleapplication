using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class NPAdmPortingCase
	{
		private const string SP_GET_PORTING_CASE_LIST = "portingCase_list";

		private string _portingCaseID;

		private string _portingCaseName;

		public string PortingCaseID
		{
			get
			{
				if (this._portingCaseID == null)
				{
					return string.Empty;
				}
				return this._portingCaseID;
			}
			set
			{
				this._portingCaseID = value;
			}
		}

		public string PortingCaseName
		{
			get
			{
				if (this._portingCaseName == null)
				{
					return string.Empty;
				}
				return this._portingCaseName;
			}
			set
			{
				this._portingCaseName = value;
			}
		}

		public NPAdmPortingCase()
		{
		}

		public NPAdmPortingCase(string portingCaseID, string portingCaseName)
		{
			this._portingCaseID = portingCaseID;
			this._portingCaseName = portingCaseName;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<NPAdmPortingCase> listRef)
		{
			while (returnData.Read())
			{
				NPAdmPortingCase nPAdmPortingCase = new NPAdmPortingCase(GeneralConverter.ToString(returnData["portingCaseID"]), GeneralConverter.ToString(returnData["portingCaseName"]));
				listRef.Add(nPAdmPortingCase);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<NPAdmPortingCase> GetListPortingCase()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "portingCase_list");
			List<NPAdmPortingCase> nPAdmPortingCases = new List<NPAdmPortingCase>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmPortingCase>(sqlCommand, new GenerateListFromReader<NPAdmPortingCase>(NPAdmPortingCase.GenerateListFromReader<NPAdmPortingCase>), ref nPAdmPortingCases);
			return nPAdmPortingCases;
		}
	}
}