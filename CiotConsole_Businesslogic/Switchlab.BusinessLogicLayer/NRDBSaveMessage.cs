using System;

namespace Switchlab.BusinessLogicLayer
{
	public class NRDBSaveMessage
	{
		private NRDBSaveMessage.E_NRDB_NP _messageType;

		public NRDBNpMessage NpMessage
		{
			get
			{
				NRDBSaveMessage.E_NRDB_NP eNRDBNP = this._messageType;
				if (eNRDBNP > NRDBSaveMessage.E_NRDB_NP.E_NP_FERD)
				{
					switch (eNRDBNP)
					{
						case NRDBSaveMessage.E_NRDB_NP.E_NP_OPPS:
						{
							return new NRDBNpOppsMessage();
						}
						case NRDBSaveMessage.E_NRDB_NP.E_NP_FORE | NRDBSaveMessage.E_NRDB_NP.E_NP_IVER | NRDBSaveMessage.E_NRDB_NP.E_NP_FERD | NRDBSaveMessage.E_NRDB_NP.E_NP_OPPS:
						case NRDBSaveMessage.E_NRDB_NP.E_NP_BEKRE | NRDBSaveMessage.E_NRDB_NP.E_NP_IVER:
						{
							break;
						}
						case NRDBSaveMessage.E_NRDB_NP.E_NP_TILB:
						{
							return new NRDBNpTilbMessage();
						}
						case NRDBSaveMessage.E_NRDB_NP.E_NP_ANBES:
						{
							return new NRDBNpAnbesMessage();
						}
						default:
						{
							switch (eNRDBNP)
							{
								case NRDBSaveMessage.E_NRDB_NP.E_NP_FBES:
								case NRDBSaveMessage.E_NRDB_NP.E_NP_FEND:
								case NRDBSaveMessage.E_NRDB_NP.E_NP_FIVE:
								case NRDBSaveMessage.E_NRDB_NP.E_NP_FOPS:
								case NRDBSaveMessage.E_NRDB_NP.E_NP_FFOR:
								{
									return new NRDBNpErrorMessage();
								}
								case NRDBSaveMessage.E_NRDB_NP.E_NP_BEKRE | NRDBSaveMessage.E_NRDB_NP.E_NP_ENDR | NRDBSaveMessage.E_NRDB_NP.E_NP_IVER | NRDBSaveMessage.E_NRDB_NP.E_NP_OPPS | NRDBSaveMessage.E_NRDB_NP.E_NP_ANBES:
								case 128:
								case 130:
								case 132:
								{
									break;
								}
								default:
								{
									if (eNRDBNP == NRDBSaveMessage.E_NRDB_NP.E_NP_KABES)
									{
										return new NRDBNpKabesMessage();
									}
									break;
								}
							}
							break;
						}
					}
				}
				else
				{
					switch (eNRDBNP)
					{
						case NRDBSaveMessage.E_NRDB_NP.E_NP_FORE:
						{
							return new NRDBNpForeMessage();
						}
						case 102:
						case 103:
						{
							break;
						}
						case NRDBSaveMessage.E_NRDB_NP.E_NP_BEKRE:
						{
							return new NRDBNpBekreMessage();
						}
						case NRDBSaveMessage.E_NRDB_NP.E_NP_BEST:
						{
							return new NRDBNpBestMessage();
						}
						default:
						{
							switch (eNRDBNP)
							{
								case NRDBSaveMessage.E_NRDB_NP.E_NP_ENDR:
								{
									return new NRDBNpEndrMessage();
								}
								case NRDBSaveMessage.E_NRDB_NP.E_NP_FORE | NRDBSaveMessage.E_NRDB_NP.E_NP_BEKRE | NRDBSaveMessage.E_NRDB_NP.E_NP_BEST | NRDBSaveMessage.E_NRDB_NP.E_NP_ENDR:
								case NRDBSaveMessage.E_NRDB_NP.E_NP_BEKRE | NRDBSaveMessage.E_NRDB_NP.E_NP_ENDR:
								{
									break;
								}
								case NRDBSaveMessage.E_NRDB_NP.E_NP_GODK:
								{
									return new NRDBNpGodkMessage();
								}
								case NRDBSaveMessage.E_NRDB_NP.E_NP_IVER:
								{
									return new NRDBNpIverMessage();
								}
								default:
								{
									if (eNRDBNP == NRDBSaveMessage.E_NRDB_NP.E_NP_FERD)
									{
										return new NRDBNpFerdMessage();
									}
									break;
								}
							}
							break;
						}
					}
				}
				return null;
			}
		}

		public NRDBSaveMessage(int messageType)
		{
			this._messageType = (NRDBSaveMessage.E_NRDB_NP)messageType;
		}

		private enum E_NRDB_NP
		{
			E_NP_FORE = 101,
			E_NP_BEKRE = 104,
			E_NP_BEST = 105,
			E_NP_ENDR = 108,
			E_NP_GODK = 111,
			E_NP_IVER = 112,
			E_NP_FERD = 115,
			E_NP_OPPS = 118,
			E_NP_TILB = 121,
			E_NP_ANBES = 122,
			E_NP_FBES = 125,
			E_NP_FEND = 127,
			E_NP_FIVE = 129,
			E_NP_FOPS = 131,
			E_NP_FFOR = 133,
			E_NP_KABES = 141
		}
	}
}