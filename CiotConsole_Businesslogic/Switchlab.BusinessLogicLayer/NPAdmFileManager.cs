using Switchlab.DataAccessLayer;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmFileManager
	{
		private const string SP_NET_USE = "Net_use";

		public NPAdmFileManager()
		{
		}

		public static bool CopyFile(string webserverIp, string priority, string srcfile)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@webserverIp", SqlDbType.VarChar, 20, ParameterDirection.Input, webserverIp);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, priority);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@srcFile", SqlDbType.VarChar, 200, ParameterDirection.Input, srcfile);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Net_use");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			return true;
		}

		public static StringBuilder GetHtmlCSV(ArrayList headers, ArrayList rows)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int count = headers.Count;
			for (int i = 0; i < count; i++)
			{
				stringBuilder.Append(headers[i]);
				if (i < count - 1)
				{
					stringBuilder.Append(",");
				}
			}
			stringBuilder.AppendLine();
			int num = rows.Count;
			for (int j = 0; j < num; j++)
			{
				string[] item = (string[])rows[j];
				int length = (int)item.Length;
				for (int k = 0; k < length; k++)
				{
					stringBuilder.Append(item[k]);
					if (k < length - 1)
					{
						stringBuilder.Append(",");
					}
				}
				stringBuilder.AppendLine();
			}
			return stringBuilder;
		}

		private static StreamWriter GetNP(string fileName, string s)
		{
			StreamWriter streamWriter = File.CreateText(fileName);
			string[] strArrays = new string[] { "<br />" };
			string[] strArrays1 = s.Split(strArrays, StringSplitOptions.RemoveEmptyEntries);
			for (int i = 0; i < (int)strArrays1.Length; i++)
			{
				streamWriter.WriteLine(strArrays1[i]);
			}
			return streamWriter;
		}

		public static StreamWriter GetNPCancel(string fileName, string date, string time, string priority, string senderID, string transactionType, string telephoneNumber, string originatingOrderNumber, string ochOrderNumber, string uniqueId)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPCancelString(date, time, priority, senderID, transactionType, telephoneNumber, originatingOrderNumber, ochOrderNumber, uniqueId));
		}

		public static string GetNPCancelString(string date, string time, string priority, string senderID, string transactionType, string telephoneNumber, string originatingOrderNumber, string ochOrderNumber, string uniqueId)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("SentDate=", date, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", time, ";<br />"));
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderID, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			stringBuilder.Append(string.Concat("TelephoneNumber=", telephoneNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OCHOrderNumber=", ochOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("UniqueID=", uniqueId, ";<br />"));
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		public static StreamWriter GetNPChange(string fileName, string sendDate, string sendTime, string priority, string senderId, string transactionType, string telpNumber, string originatingOrderNumber, string currServiceOp, string currNetworkOp, string recServiceOp, string portingCase, string spc, string municipality, string routingInfo, string chargingInfo, string newNumberType, string numberPorted, string seriesCount, string[] series)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPChangeString(sendDate, sendTime, priority, senderId, transactionType, telpNumber, originatingOrderNumber, currServiceOp, currNetworkOp, recServiceOp, portingCase, spc, municipality, routingInfo, chargingInfo, newNumberType, numberPorted, seriesCount, series));
		}

		public static string GetNPChangeString(string sendDate, string sendTime, string priority, string senderId, string transactionType, string telpNumber, string originatingOrderNumber, string currServiceOp, string currNetworkOp, string recServiceOp, string portingCase, string spc, string municipality, string routingInfo, string chargingInfo, string newNumberType, string numberPorted, string seriesCount, string[] series)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("SentDate=", sendDate, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", sendTime, ";<br />"));
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderId, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			stringBuilder.Append(string.Concat("TelephoneNumber=", telpNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("NumberPorted=", numberPorted, ";<br />"));
			if (!string.IsNullOrEmpty(currServiceOp))
			{
				stringBuilder.Append(string.Concat("CurrentServiceOperator=", currServiceOp, ";<br />"));
			}
			if (!string.IsNullOrEmpty(currNetworkOp))
			{
				stringBuilder.Append(string.Concat("CurrentNetworkOperator=", currNetworkOp, ";<br />"));
			}
			if (!string.IsNullOrEmpty(recServiceOp))
			{
				stringBuilder.Append(string.Concat("RecipientServiceOperator=", recServiceOp, ";<br />"));
			}
			if (!string.IsNullOrEmpty(portingCase))
			{
				stringBuilder.Append(string.Concat("PortingCase=", portingCase, ";<br />"));
			}
			if (!string.IsNullOrEmpty(spc))
			{
				stringBuilder.Append(string.Concat("SPC=", spc, ";<br />"));
			}
			if (!string.IsNullOrEmpty(municipality))
			{
				stringBuilder.Append(string.Concat("Municipality=", municipality, ";<br />"));
			}
			if (!string.IsNullOrEmpty(routingInfo))
			{
				stringBuilder.Append(string.Concat("RoutingInfo=", routingInfo, ";<br />"));
			}
			if (!string.IsNullOrEmpty(chargingInfo))
			{
				stringBuilder.Append(string.Concat("ChargingInfo=", chargingInfo, ";<br />"));
			}
			if (!string.IsNullOrEmpty(newNumberType))
			{
				stringBuilder.Append(string.Concat("NewNumberType=", newNumberType, ";<br />"));
			}
			stringBuilder.Append(string.Concat("SeriesCount=", seriesCount, ";<br />"));
			if (GeneralConverter.ToInt32(seriesCount) > 0)
			{
				stringBuilder.Append(NPAdmFileManager.GetSeriesString(series));
			}
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		public static StreamWriter GetNPCompletion(string fileName, string date, string time, string priority, string senderID, string transactionType, string originatingOrderNumber, string ochOrderNumber, string uniqueId, string recServiceOperator, string recNetworkOperator, string portingCase, string spc, string municipality, string routingInfo, string chargingInfo, string newNumberType, string numberPorted, string telephoneNumber, string seriesCount, string[] series, string[] comment)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPCompletionString(date, time, priority, senderID, transactionType, originatingOrderNumber, ochOrderNumber, uniqueId, recServiceOperator, recNetworkOperator, portingCase, spc, municipality, routingInfo, chargingInfo, newNumberType, numberPorted, telephoneNumber, seriesCount, series, comment));
		}

		public static string GetNPCompletionString(string date, string time, string priority, string senderID, string transactionType, string originatingOrderNumber, string ochOrderNumber, string uniqueId, string recServiceOperator, string recNetworkOperator, string portingCase, string spc, string municipality, string routingInfo, string chargingInfo, string newNumberType, string numberPorted, string telephoneNumber, string seriesCount, string[] series, string[] comment)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("SentDate=", date, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", time, ";<br />"));
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderID, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OCHOrderNumber=", ochOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("UniqueID=", uniqueId, ";<br />"));
			stringBuilder.Append(string.Concat("RecipientServiceOperator=", recServiceOperator, ";<br />"));
			stringBuilder.Append(string.Concat("RecipientNetworkOperator=", recNetworkOperator, ";<br />"));
			stringBuilder.Append(string.Concat("PortingCase=", portingCase, ";<br />"));
			stringBuilder.Append(string.Concat("SPC=", spc, ";<br />"));
			stringBuilder.Append(string.Concat("Municipality=", municipality, ";<br />"));
			stringBuilder.Append(string.Concat("RoutingInfo=", routingInfo, ";<br />"));
			stringBuilder.Append(string.Concat("ChargingInfo=", chargingInfo, ";<br />"));
			stringBuilder.Append(string.Concat("NewNumberType=", newNumberType, ";<br />"));
			stringBuilder.Append(string.Concat("NumberPorted=", numberPorted, ";<br />"));
			stringBuilder.Append(string.Concat("TelephoneNumber=", telephoneNumber, ";<br />"));
			stringBuilder.Append(string.Concat("SeriesCount=", seriesCount, ";<br />"));
			stringBuilder.Append(NPAdmFileManager.GetSeriesString(series));
			for (int i = 0; i < (int)comment.Length; i++)
			{
				object[] objArray = new object[] { "Comment[", i + 1, "]=", comment[i], ";<br />" };
				stringBuilder.Append(string.Concat(objArray));
			}
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		public static StreamWriter GetNPConfirmation(string fileName, string date, string time, string priority, string senderId, string transactionType, string telephoneNumber, string ochOrderNumber, string uniqueId, string originatingOrderNumber, string confirmedExecutionDate, string confirmationStatus, string seriesCount, string[] series, string[] comment)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPConfirmationString(date, time, priority, senderId, transactionType, telephoneNumber, ochOrderNumber, uniqueId, originatingOrderNumber, confirmedExecutionDate, confirmationStatus, seriesCount, series, comment));
		}

		public static string GetNPConfirmationString(string date, string time, string priority, string senderId, string transactionType, string telephoneNumber, string ochOrderNumber, string uniqueId, string originatingOrderNumber, string confirmedExecutionDate, string confirmationStatus, string seriesCount, string[] series, string[] comment)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("SentDate=", date, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", time, ";<br />"));
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderId, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			stringBuilder.Append(string.Concat("TelephoneNumber=", telephoneNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OCHOrderNumber=", ochOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("UniqueID=", uniqueId, ";<br />"));
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("ConfirmedExecutionDate=", confirmedExecutionDate, ";<br />"));
			if (confirmationStatus.Trim().Length > 0)
			{
				stringBuilder.Append(string.Concat("ConfirmationStatus=", confirmationStatus, ";<br />"));
			}
			stringBuilder.Append(string.Concat("SeriesCount=", seriesCount, ";<br />"));
			stringBuilder.Append(NPAdmFileManager.GetSeriesString(series));
			for (int i = 0; i < (int)comment.Length; i++)
			{
				object[] objArray = new object[] { "Comment[", i + 1, "]=", comment[i], ";<br />" };
				stringBuilder.Append(string.Concat(objArray));
			}
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		public static StreamWriter GetNPCreate(string fileName, string date, string time, string priority, string senderId, string transactionType, string requestedExecutionDate, string originatingOrderNumber, string recipientServiceOperator, string recipientNetworkOperator, string pointOfConnection, string telephoneNumber, string iccid, string seriesCount, string[] series, string[] comment)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPCreateString(date, time, priority, senderId, transactionType, requestedExecutionDate, originatingOrderNumber, recipientServiceOperator, recipientNetworkOperator, pointOfConnection, telephoneNumber, iccid, seriesCount, series, comment));
		}

		public static string GetNPCreateString(string date, string time, string priority, string senderId, string transactionType, string requestedExecutionDate, string originatingOrderNumber, string recipientServiceOperator, string recipientNetworkOperator, string pointOfConnection, string telephoneNumber, string iccid, string seriesCount, string[] series, string[] comment)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("SentDate=", date, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", time, ";<br />"));
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderId, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			if (requestedExecutionDate.Trim() != string.Empty)
			{
				stringBuilder.Append(string.Concat("RequestedExecutionDate=", requestedExecutionDate, ";<br />"));
			}
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("RecipientServiceOperator=", recipientServiceOperator, ";<br />"));
			stringBuilder.Append(string.Concat("RecipientNetworkOperator=", recipientNetworkOperator, ";<br />"));
			stringBuilder.Append(string.Concat("PointOfConnection=", pointOfConnection, ";<br />"));
			stringBuilder.Append(string.Concat("TelephoneNumber=", telephoneNumber, ";<br />"));
			if (iccid.Length > 0)
			{
				stringBuilder.Append(string.Concat("ICC=", iccid, ";<br />"));
			}
			stringBuilder.Append(string.Concat("SeriesCount=", seriesCount, ";<br />"));
			stringBuilder.Append(NPAdmFileManager.GetSeriesString(series));
			for (int i = 0; i < (int)comment.Length; i++)
			{
				object[] objArray = new object[] { "Comment[", i + 1, "]=", comment[i], ";<br />" };
				stringBuilder.Append(string.Concat(objArray));
			}
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		public static StreamWriter GetNPPortingResponse(string fileName, string sendDate, string sendTime, string priority, string senderId, string transactionType, string telephoneNumber, string ochOrderNumber, string uniqueId, string originatingOrderNumber)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPPortingResponseString(sendDate, sendTime, priority, senderId, transactionType, telephoneNumber, ochOrderNumber, uniqueId, originatingOrderNumber));
		}

		public static string GetNPPortingResponseString(string sendDate, string sendTime, string priority, string senderId, string transactionType, string telephoneNumber, string ochOrderNumber, string uniqueId, string originatingOrderNumber)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderId, ";<br />"));
			stringBuilder.Append(string.Concat("SentDate=", sendDate, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", sendTime, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			stringBuilder.Append(string.Concat("TelephoneNumber=", telephoneNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OCHOrderNumber=", ochOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("UniqueID=", uniqueId, ";<br />"));
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		public static StreamWriter GetNPRangeUpdate(string fileName, string sendDate, string sendTime, string priority, string senderId, string originatingOrderNumber, string rangeUpdateType, string range, string currentRangeHolder, string currentServiceOperator, string currentNetworkOperator, string portingCase, string spc, string municipality, string routingInfo, string chargingInfo, string newNumberType, string otherOperator, string transactionType)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPRangeUpdateString(sendDate, sendTime, priority, senderId, originatingOrderNumber, rangeUpdateType, range, currentRangeHolder, currentServiceOperator, currentNetworkOperator, portingCase, spc, municipality, routingInfo, chargingInfo, newNumberType, otherOperator, transactionType));
		}

		public static string GetNPRangeUpdateString(string sendDate, string sendTime, string priority, string senderId, string originatingOrderNumber, string rangeUpdateType, string range, string currentRangeHolder, string currentServiceOperator, string currentNetworkOperator, string portingCase, string spc, string municipality, string routingInfo, string chargingInfo, string newNumberType, string otherOperator, string transactionType)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderId, ";<br />"));
			stringBuilder.Append(string.Concat("SentDate=", sendDate, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", sendTime, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("RangeUpdateType=", rangeUpdateType, ";<br />"));
			stringBuilder.Append(string.Concat("Range=", range, ";<br />"));
			stringBuilder.Append(string.Concat("CurrentRangeHolder=", currentRangeHolder, ";<br />"));
			stringBuilder.Append(string.Concat("CurrentServiceOperator=", currentServiceOperator, ";<br />"));
			stringBuilder.Append(string.Concat("CurrentNetworkOperator=", currentNetworkOperator, ";<br />"));
			stringBuilder.Append(string.Concat("PortingCase=", portingCase, ";<br />"));
			stringBuilder.Append(string.Concat("SPC=", spc, ";<br />"));
			stringBuilder.Append(string.Concat("Municipality=", municipality, ";<br />"));
			stringBuilder.Append(string.Concat("RoutingInfo=", routingInfo, ";<br />"));
			stringBuilder.Append(string.Concat("ChargingInfo=", chargingInfo, ";<br />"));
			stringBuilder.Append(string.Concat("NewNumberType=", newNumberType, ";<br />"));
			stringBuilder.Append(string.Concat("OtherOperator=", otherOperator, ";<br />"));
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		public static StreamWriter GetNPReject(string fileName, string sendDate, string sendTime, string priority, string senderId, string originatingOrderNumber, string otherOperator, string transactionType, string telephoneNumber, string ochOrderNumber, string uniqueId, object[] rejectCode, object[] rejectText)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPRejectString(sendDate, sendTime, priority, senderId, originatingOrderNumber, otherOperator, transactionType, telephoneNumber, ochOrderNumber, uniqueId, rejectCode, rejectText));
		}

		public static string GetNPRejectString(string sendDate, string sendTime, string priority, string senderId, string originatingOrderNumber, string otherOperator, string transactionType, string telephoneNumber, string ochOrderNumber, string uniqueId, object[] rejectCode, object[] rejectText)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderId, ";<br />"));
			stringBuilder.Append(string.Concat("SentDate=", sendDate, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", sendTime, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			stringBuilder.Append(string.Concat("TelephoneNumber=", telephoneNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OCHOrderNumber=", ochOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("UniqueID=", uniqueId, ";<br />"));
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OtherOperator=", otherOperator, ";<br />"));
			if (rejectCode != null && rejectText != null)
			{
				for (int i = 0; i < (int)rejectCode.Length; i++)
				{
					stringBuilder.Append("RejectCode[").Append(i + 1).Append("]=").Append(rejectCode[i].ToString()).Append(";<br />");
				}
				for (int j = 0; j < (int)rejectText.Length; j++)
				{
					stringBuilder.Append("RejectText[").Append(j + 1).Append("]=").Append(rejectText[j].ToString()).Append(";<br />");
				}
			}
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		public static StreamWriter GetNPReturn(string fileName, string sendDate, string sendTime, string priority, string senderId, string originatingOrderNumber, string transactionType, string telephoneNumber, string seriesCount, string[] series)
		{
			return NPAdmFileManager.GetNP(fileName, NPAdmFileManager.GetNPReturnString(sendDate, sendTime, priority, senderId, originatingOrderNumber, transactionType, telephoneNumber, seriesCount, series));
		}

		public static string GetNPReturnString(string sendDate, string sendTime, string priority, string senderId, string originatingOrderNumber, string transactionType, string telephoneNumber, string seriesCount, string[] series)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[Header]<br />");
			stringBuilder.Append("TransactionGroup=NumberPortability;<br />");
			stringBuilder.Append(string.Concat("Priority=", priority, ";<br />"));
			stringBuilder.Append(string.Concat("SenderID=", senderId, ";<br />"));
			stringBuilder.Append(string.Concat("SentDate=", sendDate, ";<br />"));
			stringBuilder.Append(string.Concat("SentTime=", sendTime, ";<br />"));
			stringBuilder.Append("[Message]<br />");
			stringBuilder.Append(string.Concat("TransactionType=", transactionType, ";<br />"));
			stringBuilder.Append(string.Concat("TelephoneNumber=", telephoneNumber, ";<br />"));
			stringBuilder.Append(string.Concat("OriginatingOrderNumber=", originatingOrderNumber, ";<br />"));
			stringBuilder.Append(string.Concat("SeriesCount=", seriesCount, ";<br />"));
			stringBuilder.Append(NPAdmFileManager.GetSeriesString(series));
			stringBuilder.Append("[Trailer]<br />");
			stringBuilder.Append("MessageCount=1;");
			return stringBuilder.ToString();
		}

		private static string GetSeriesString(string[] series)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int length = (int)series.Length;
			for (int i = 1; i <= length; i++)
			{
				object[] objArray = new object[] { "Series[", i, "]=", series[i - 1], ";<br />" };
				stringBuilder.Append(string.Concat(objArray));
			}
			return stringBuilder.ToString();
		}
	}
}