using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	[Serializable]
	public class CrmPages
	{
		protected const string SP_SELECT = "pages_select";

		protected const string SP_DETAIL = "pages_detail";

		protected const string SP_DELETE = "pages_delete";

		protected const string SP_INSERT = "pages_insert";

		protected const string SP_UPDATE = "pages_update";

		private int _page_id;

		private string _title;

		private string _url;

		private int _parentmenu;

		private string _displayname;

		private int _sortorder;

		public string displayname
		{
			get
			{
				if (this._displayname == null)
				{
					return string.Empty;
				}
				return this._displayname;
			}
		}

		public int page_id
		{
			get
			{
				return this._page_id;
			}
		}

		public int parentmenu
		{
			get
			{
				return this._parentmenu;
			}
		}

		public int sortorder
		{
			get
			{
				return this._sortorder;
			}
		}

		public string title
		{
			get
			{
				if (this._title == null)
				{
					return string.Empty;
				}
				return this._title;
			}
		}

		public string url
		{
			get
			{
				if (this._url == null)
				{
					return string.Empty;
				}
				return this._url;
			}
		}

		public CrmPages(int __page_id, string __title, string __url, int __parentmenu, string __displayname, int __sortorder)
		{
			this._page_id = __page_id;
			this._title = __title;
			this._url = __url;
			this._parentmenu = __parentmenu;
			this._displayname = __displayname;
			this._sortorder = __sortorder;
		}

		public CrmPages(string __title, string __url, int __parentmenu, string __displayname, int __sortorder) : this(0, __title, __url, __parentmenu, __displayname, __sortorder)
		{
		}

		public CrmPages() : this(null, null, 0, null, 0)
		{
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static IListSource _getAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_select");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(CrmPages.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static IListSource _getById(int __page_id)
		{
			if (__page_id < 0)
			{
				throw new ArgumentOutOfRangeException("page_id");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@page_id", SqlDbType.Int, 0, ParameterDirection.Input, __page_id);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_detail");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(CrmPages.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Delete, true)]
		public static bool Delete(int __page_id)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@page_id", SqlDbType.Int, 0, ParameterDirection.Input, __page_id);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_delete");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CrmPages> theList)
		{
			while (returnData.Read())
			{
				CrmPages crmPage = new CrmPages((int)returnData["page_id"], Convert.ToString(returnData["title"]), Convert.ToString(returnData["url"]), GeneralConverter.ToInt32(returnData["parentmenu"]), GeneralConverter.ToString(returnData["displayname"]), (int)returnData["sortorder"]);
				theList.Add(crmPage);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<IListSource> theList)
		{
			DataTable dataTable = new DataTable();
			dataTable.Load(returnData);
			theList.Add(dataTable);
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmPages> GetAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_select");
			List<CrmPages> crmPages = new List<CrmPages>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmPages>(sqlCommand, new GenerateListFromReader<CrmPages>(CrmPages.GenerateListFromReader<CrmPages>), ref crmPages);
			return crmPages;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static CrmPages GetById(int __page_id)
		{
			if (__page_id < 0)
			{
				throw new ArgumentOutOfRangeException("page_id");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@page_id", SqlDbType.Int, 0, ParameterDirection.Input, __page_id);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_detail");
			List<CrmPages> crmPages = new List<CrmPages>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmPages>(sqlCommand, new GenerateListFromReader<CrmPages>(CrmPages.GenerateListFromReader<CrmPages>), ref crmPages);
			if (crmPages.Count <= 0)
			{
				return null;
			}
			return crmPages[0];
		}

		[DataObjectMethod(DataObjectMethodType.Insert, true)]
		public bool Insert()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 128, ParameterDirection.Input, this._title);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@url", SqlDbType.VarChar, 256, ParameterDirection.Input, this._url);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@parentmenu", SqlDbType.Int, 0, ParameterDirection.Input, this._parentmenu);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@displayname", SqlDbType.VarChar, 32, ParameterDirection.Input, this._displayname);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_insert");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Update, true)]
		public static bool Update(int __page_id, string __title, string __url, int __parentmenu, string __displayname, int __sortorder)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@page_id", SqlDbType.Int, 0, ParameterDirection.Input, __page_id);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 128, ParameterDirection.Input, __title);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@url", SqlDbType.VarChar, 256, ParameterDirection.Input, __url);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@parentmenu", SqlDbType.Int, 0, ParameterDirection.Input, __parentmenu);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@displayname", SqlDbType.VarChar, 32, ParameterDirection.Input, __displayname);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_update");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}
	}
}