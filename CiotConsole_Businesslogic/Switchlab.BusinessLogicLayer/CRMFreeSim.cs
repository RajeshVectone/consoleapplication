using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class CRMFreeSim
	{
		private const string SP_update_FreeSIM = "updatefreesim";

		private const string SP_check_ICCD = "checkfreesimICCID";

		private const string SP_getfreesim = "getfreesim";

		private const string SP_getfreesimbyid = "getfreesimById";

		private const string SP_getfreesimfordownload = "getfreesimfordownload";

		public string errmsg = "";

		public CRMFreeSim()
		{
		}

		public int checkICCID(string iccid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, iccid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 4, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "checkfreesimICCID");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return -1;
			}
			if (sqlCommand.Parameters["@ReturnValue"] == null)
			{
				return GeneralConverter.ToInt32(obj);
			}
			return GeneralConverter.ToInt32(sqlCommand.Parameters["@ReturnValue"].Value);
		}

		public DataView GetFreeSim(string datefrom, string dateto, string flddate)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			DateTime dateTime = DateTime.Parse(dateto).AddDays(1);
			object[] year = new object[] { dateTime.Year, "/", dateTime.Month, "/", dateTime.Day };
			string str = string.Concat(year);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@FieldName", SqlDbType.VarChar, 32, ParameterDirection.Input, flddate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@DateFrom", SqlDbType.VarChar, 32, ParameterDirection.Input, datefrom);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@DateTo", SqlDbType.VarChar, 32, ParameterDirection.Input, str);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "getfreesim");
			return sQLDataAccessLayer.ExecuteQuery(sqlCommand).Tables[0].DefaultView;
		}

		public DataView GetFreeSimByfreesimid(string freesimid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@FieldName", SqlDbType.VarChar, 32, ParameterDirection.Input, "freesimid");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@Fielvalue", SqlDbType.VarChar, 32, ParameterDirection.Input, freesimid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "getfreesimById");
			return sQLDataAccessLayer.ExecuteQuery(sqlCommand).Tables[0].DefaultView;
		}

		public DataView GetFreeSimBysubscriberid(string subscriberid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@FieldName", SqlDbType.VarChar, 32, ParameterDirection.Input, "subscriberid");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@Fielvalue", SqlDbType.VarChar, 32, ParameterDirection.Input, subscriberid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "getfreesimById");
			return sQLDataAccessLayer.ExecuteQuery(sqlCommand).Tables[0].DefaultView;
		}

		public DataSet GetFreeSimDataset(string datefrom, string dateto, string flddate)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			DateTime dateTime = DateTime.Parse(dateto).AddDays(1);
			object[] year = new object[] { dateTime.Year, "/", dateTime.Month, "/", dateTime.Day };
			string str = string.Concat(year);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@FieldName", SqlDbType.VarChar, 32, ParameterDirection.Input, flddate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@DateFrom", SqlDbType.VarChar, 32, ParameterDirection.Input, datefrom);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@DateTo", SqlDbType.VarChar, 32, ParameterDirection.Input, str);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "getfreesim");
			return sQLDataAccessLayer.ExecuteQuery(sqlCommand);
		}

		public DataView GetFreeSimForDownload(string datefrom, string dateto, string flddate)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			DateTime dateTime = DateTime.Parse(dateto).AddDays(1);
			object[] year = new object[] { dateTime.Year, "/", dateTime.Month, "/", dateTime.Day };
			string str = string.Concat(year);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@FieldName", SqlDbType.VarChar, 32, ParameterDirection.Input, flddate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@DateFrom", SqlDbType.VarChar, 32, ParameterDirection.Input, datefrom);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@DateTo", SqlDbType.VarChar, 32, ParameterDirection.Input, str);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "getfreesimfordownload");
			return sQLDataAccessLayer.ExecuteQuery(sqlCommand).Tables[0].DefaultView;
		}

		public int updateiccid(int freesimid, int subscriberid, string iccid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@freesimid", SqlDbType.Int, 4, ParameterDirection.Input, freesimid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscriberid", SqlDbType.Int, 4, ParameterDirection.Input, subscriberid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, iccid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 4, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "updatefreesim");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return -1;
			}
			if (sqlCommand.Parameters["@ReturnValue"] == null)
			{
				return GeneralConverter.ToInt32(obj);
			}
			return GeneralConverter.ToInt32(sqlCommand.Parameters["@ReturnValue"].Value);
		}
	}
}