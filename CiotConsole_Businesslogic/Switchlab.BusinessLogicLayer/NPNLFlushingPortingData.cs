using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class NPNLFlushingPortingData
	{
		private const string SP_FLUSH_PORTING_DATA = "load_np_data";

		private string _zippedMobileFileName;

		private string _zippedFixedFileName;

		public string ZippedFixedFileName
		{
			get
			{
				return this._zippedFixedFileName;
			}
			set
			{
				this._zippedFixedFileName = value;
			}
		}

		public string ZippedMobileFileName
		{
			get
			{
				return this._zippedMobileFileName;
			}
			set
			{
				this._zippedMobileFileName = value;
			}
		}

		public NPNLFlushingPortingData()
		{
		}

		public NPNLFlushingPortingData(string zippedmobilefilename, string zippedfixedfilename)
		{
			this._zippedMobileFileName = zippedmobilefilename;
			this._zippedFixedFileName = zippedfixedfilename;
		}

		public void doFlushPortingData(ref string errDesc)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNLTempConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "load_np_data");
			try
			{
				errDesc = "done.";
				sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			}
			catch (Exception exception)
			{
				errDesc = exception.Message;
			}
		}
	}
}