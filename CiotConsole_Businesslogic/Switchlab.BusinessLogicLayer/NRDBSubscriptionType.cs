using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class NRDBSubscriptionType
	{
		private const string SP_GET_SUBSCRIPTION_TYPE_LIST = "subscription_type_list";

		private string _code;

		private string _name;

		public string Code
		{
			get
			{
				if (this._code == null)
				{
					return string.Empty;
				}
				return this._code;
			}
			set
			{
				this._code = value;
			}
		}

		public string Name
		{
			get
			{
				if (this._name == null)
				{
					return string.Empty;
				}
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public NRDBSubscriptionType()
		{
		}

		public NRDBSubscriptionType(string code, string name)
		{
			this._code = code;
			this._name = name;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<NRDBSubscriptionType> listRef)
		{
			while (returnData.Read())
			{
				NRDBSubscriptionType nRDBSubscriptionType = new NRDBSubscriptionType(GeneralConverter.ToString(returnData["code"]), GeneralConverter.ToString(returnData["name"]));
				listRef.Add(nRDBSubscriptionType);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<NRDBSubscriptionType> GetSubscriptionTypeList()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNRDBConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subscription_type_list");
			List<NRDBSubscriptionType> nRDBSubscriptionTypes = new List<NRDBSubscriptionType>();
			sQLDataAccessLayer.ExecuteReaderCmd<NRDBSubscriptionType>(sqlCommand, new GenerateListFromReader<NRDBSubscriptionType>(NRDBSubscriptionType.GenerateListFromReader<NRDBSubscriptionType>), ref nRDBSubscriptionTypes);
			return nRDBSubscriptionTypes;
		}
	}
}