using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class ESPRefCurrency
	{
		private const string SP_LIST = "rp1_getrefccy";

		private const string SP_LIST_ALL = "rp1_getrefccyall";

		private string _scurrcode;

		private string _sname;

		public string CurrCode
		{
			get
			{
				if (this._scurrcode == null)
				{
					return string.Empty;
				}
				return this._scurrcode;
			}
			set
			{
				this._scurrcode = value;
			}
		}

		public string Name
		{
			get
			{
				if (this._sname == null)
				{
					return string.Empty;
				}
				return this._sname;
			}
			set
			{
				this._sname = value;
			}
		}

		public ESPRefCurrency()
		{
		}

		public ESPRefCurrency(string iCurrCode, string sName)
		{
			this._scurrcode = iCurrCode;
			this._sname = sName;
		}

		private static void GenerateRefListFromReader<T>(IDataReader returnData, ref List<ESPRefCurrency> listRef)
		{
			while (returnData.Read())
			{
				ESPRefCurrency eSPRefCurrency = new ESPRefCurrency(Convert.ToString(returnData["currcode"]), Convert.ToString(returnData["name"]));
				listRef.Add(eSPRefCurrency);
			}
		}

		public static List<ESPRefCurrency> GetList(int ResellerID, string SiteCode)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, SiteCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_getrefccy");
			List<ESPRefCurrency> eSPRefCurrencies = new List<ESPRefCurrency>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPRefCurrency>(sqlCommand, new GenerateListFromReader<ESPRefCurrency>(ESPRefCurrency.GenerateRefListFromReader<ESPRefCurrency>), ref eSPRefCurrencies);
			return eSPRefCurrencies;
		}

		public static List<ESPRefCurrency> GetListAll(int ResellerID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_getrefccyall");
			List<ESPRefCurrency> eSPRefCurrencies = new List<ESPRefCurrency>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPRefCurrency>(sqlCommand, new GenerateListFromReader<ESPRefCurrency>(ESPRefCurrency.GenerateRefListFromReader<ESPRefCurrency>), ref eSPRefCurrencies);
			return eSPRefCurrencies;
		}
	}
}