using System;
using System.Collections.Specialized;
using System.Configuration;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class BusinessFacade
	{
		public BusinessFacade()
		{
		}

		public static string GetBarcodeDir()
		{
			return ConfigurationSettings.AppSettings["BarcodeDIR"].ToString();
		}

		public static string GetBlankImage()
		{
			return ConfigurationSettings.AppSettings["Blank"].ToString();
		}

		public static ConnectionStringSettings GetCDRConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["CDR"];
		}

		public static ConnectionStringSettings GetCIOTConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["CIOT"];
		}

		public static ConnectionStringSettings GetConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["NPSPAIN"];
		}

		public static ConnectionStringSettings GetCRMAUConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["CRMAU"];
		}

		public static ConnectionStringSettings GetCRMConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["CRM"];
		}

		public static string GetCRMConnectionString(int resellerId)
		{
			CustomConnectionStringSection section = ConfigurationManager.GetSection("CustomConnectionString") as CustomConnectionStringSection;
			return section.ResellerConnections[resellerId.ToString()].CrmConnectionString;
		}

		public static ConnectionStringSettings GetCustXConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["CustX"];
		}

		public static ConnectionStringSettings GetDIBSConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["DIBS"];
		}

		public static string GetDirectory()
		{
			return ConfigurationSettings.AppSettings["XMLDir"].ToString();
		}

		public static int GetEnvFlag()
		{
			return Convert.ToInt32(ConfigurationSettings.AppSettings["EnvFlag"]);
		}

		public static ConnectionStringSettings GetESPConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["ESP"];
		}

		public static string GetFooter()
		{
			return ConfigurationSettings.AppSettings["Footer"].ToString();
		}

		public static string GetFtpDirUpload()
		{
			return ConfigurationSettings.AppSettings["FtpDirUpload"].ToString();
		}

		public static string GetFtpPassword()
		{
			return ConfigurationSettings.AppSettings["FtpPassword"].ToString();
		}

		public static string GetFtpServerIP()
		{
			return ConfigurationSettings.AppSettings["FtpServerIP"].ToString();
		}

		public static string GetFtpUserID()
		{
			return ConfigurationSettings.AppSettings["FtpUserID"].ToString();
		}

		public static ConnectionStringSettings GetHLRConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["HLR"];
		}

		public static ConnectionStringSettings GetMNPAConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["NPAUSTRIA"];
		}

		public static ConnectionStringSettings GetMSDBConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["MSDB"];
		}

		public static ConnectionStringSettings GetNLTempConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["NLTemp"];
		}

		public static ConnectionStringSettings GetNPAdmConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["NPAdm"];
		}

		public static ConnectionStringSettings GetNRDBConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["NRDB"];
		}

		public static string GetPrefixFile()
		{
			return ConfigurationSettings.AppSettings["Operator"].ToString();
		}

		public static string GetRPCPConnectionString()
		{
			return (ConfigurationManager.GetSection("CustomConnectionString") as CustomConnectionStringSection).RPCPConnectionString;
		}

		public static ConnectionStringSettings GetStockActivationConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["StockActivation"];
		}

		public static string GetVersion()
		{
			return ConfigurationSettings.AppSettings["Version"].ToString();
		}

		public static ConnectionStringSettings GetVoucherConnectionString()
		{
			return (ConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection).ConnectionStrings["Voucher"];
		}
	}
}