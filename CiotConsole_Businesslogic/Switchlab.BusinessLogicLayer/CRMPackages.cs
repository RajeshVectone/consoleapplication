using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMPackages
	{
		private const string SP_LIST_BY_CODE = "product_search_bycode";

		private const string SP_LIST_BY_TYPE = "product_search_bytype";

		private const string SP_LIST_BY_NAME = "plan_search_byname";

		private const string SP_LIST_BY_ID = "plan_search_byid";

		private const string SP_LIST_ITEM = "product_list_item";

		private const string SP_UNSELECT_ITEM = "unselect_item";

		private const string SP_LIST_NUMBER = "select_number_available";

		private const string SP_SELECT_NUMBER = "select_number";

		private const string SP_CREATE = "plan_create";

		private const string SP_UPDATE = "plan_update";

		private const string SP_CREATE_ITEM = "item_create";

		private int _planid;

		private string _planname;

		private string _plandesc;

		private string _currcode;

		private int _voicemins;

		private int _messagetexts;

		private int _videomins;

		private int _messagevideos;

		private int _downloads;

		private int _contractmonths;

		private double _monthprice;

		public int ContractMonths
		{
			get
			{
				return this._contractmonths;
			}
		}

		public string CurrCode
		{
			get
			{
				if (this._currcode == null)
				{
					return string.Empty;
				}
				return this._currcode;
			}
		}

		public int Downloads
		{
			get
			{
				return this._downloads;
			}
		}

		public int MessageTexts
		{
			get
			{
				return this._messagetexts;
			}
		}

		public int MessageVideos
		{
			get
			{
				return this._messagevideos;
			}
		}

		public double MonthPrice
		{
			get
			{
				return this._monthprice;
			}
		}

		public string PlanDesc
		{
			get
			{
				if (this._plandesc == null)
				{
					return string.Empty;
				}
				return this._plandesc;
			}
		}

		public int PlanId
		{
			get
			{
				return this._planid;
			}
		}

		public string PlanName
		{
			get
			{
				if (this._planname == null)
				{
					return string.Empty;
				}
				return this._planname;
			}
		}

		public int VideoMins
		{
			get
			{
				return this._videomins;
			}
		}

		public int VoiceMins
		{
			get
			{
				return this._voicemins;
			}
		}

		public CRMPackages()
		{
		}

		public CRMPackages(int planId, string planName, string planDesc, string currCode, double monthPrice, int voiceMins, int messageTexts, int videoMins, int messageVideos, int downloads, int contractMonths)
		{
			this._planid = planId;
			this._planname = planName;
			this._plandesc = planDesc;
			this._currcode = currCode;
			this._monthprice = monthPrice;
			this._voicemins = voiceMins;
			this._messagetexts = messageTexts;
			this._videomins = videoMins;
			this._messagevideos = messageVideos;
			this._downloads = downloads;
			this._contractmonths = contractMonths;
		}

		public static bool CreatePlan(string iPlanName, string iPlanDesc, string iCurrCode, double iMonthPrice, int iVoiceMins, int iMsgTexts, int iVideoMins, int iMsgVideos, int iDownloads, int iContractMonths)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@planname", SqlDbType.VarChar, 64, ParameterDirection.Input, iPlanName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@plandesc", SqlDbType.VarChar, 256, ParameterDirection.Input, iPlanDesc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, iCurrCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@monthprice", SqlDbType.Float, 0, ParameterDirection.Input, iMonthPrice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@voicemins", SqlDbType.Int, 0, ParameterDirection.Input, iVoiceMins);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msgtexts", SqlDbType.Int, 0, ParameterDirection.Input, iMsgTexts);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@videomins", SqlDbType.Int, 0, ParameterDirection.Input, iVideoMins);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msgvideos", SqlDbType.Int, 0, ParameterDirection.Input, iMsgVideos);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@downloads", SqlDbType.Int, 0, ParameterDirection.Input, iDownloads);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@contractmonths", SqlDbType.Int, 0, ParameterDirection.Input, iContractMonths);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "plan_create");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		private static void GeneratePlanFromReader<T>(IDataReader returnData, ref List<CRMPackages> listNew)
		{
			while (returnData.Read())
			{
				if (returnData.IsDBNull(0))
				{
					continue;
				}
				CRMPackages cRMPackage = new CRMPackages(GeneralConverter.ToInt32(returnData["planid"]), GeneralConverter.ToString(returnData["planname"]), GeneralConverter.ToString(returnData["plandesc"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToDouble(returnData["month_price"]), GeneralConverter.ToInt32(returnData["voice_mins"]), GeneralConverter.ToInt32(returnData["message_texts"]), GeneralConverter.ToInt32(returnData["video_mins"]), GeneralConverter.ToInt32(returnData["message_videos"]), GeneralConverter.ToInt32(returnData["downloads"]), GeneralConverter.ToInt32(returnData["contract_months"]));
				listNew.Add(cRMPackage);
			}
		}

		private static void GeneratePlanListFromReader<T>(IDataReader returnData, ref List<CRMPackages> listNew)
		{
			while (returnData.Read())
			{
				CRMPackages cRMPackage = new CRMPackages(GeneralConverter.ToInt32(returnData["planid"]), GeneralConverter.ToString(returnData["planname"]), GeneralConverter.ToString(returnData["plandesc"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToDouble(returnData["month_price"]), GeneralConverter.ToInt32(returnData["voice_mins"]), GeneralConverter.ToInt32(returnData["message_texts"]), GeneralConverter.ToInt32(returnData["video_mins"]), GeneralConverter.ToInt32(returnData["message_videos"]), GeneralConverter.ToInt32(returnData["downloads"]), GeneralConverter.ToInt32(returnData["contract_months"]));
				listNew.Add(cRMPackage);
			}
		}

		public static List<CRMPackages> listById(int iPlanId)
		{
			if (iPlanId < 0)
			{
				iPlanId = 0;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@planid", SqlDbType.Int, 0, ParameterDirection.Input, iPlanId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "plan_search_byid");
			List<CRMPackages> cRMPackages = new List<CRMPackages>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPackages>(sqlCommand, new GenerateListFromReader<CRMPackages>(CRMPackages.GeneratePlanListFromReader<CRMPackages>), ref cRMPackages);
			return cRMPackages;
		}

		public static List<CRMPackages> listByName(string iPlanName)
		{
			if (iPlanName == string.Empty)
			{
				iPlanName = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@planname", SqlDbType.VarChar, 64, ParameterDirection.Input, iPlanName);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "plan_search_byname");
			List<CRMPackages> cRMPackages = new List<CRMPackages>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPackages>(sqlCommand, new GenerateListFromReader<CRMPackages>(CRMPackages.GeneratePlanListFromReader<CRMPackages>), ref cRMPackages);
			return cRMPackages;
		}

		public static CRMPackages SearchByID(int iPlanId)
		{
			if (iPlanId < 0)
			{
				iPlanId = 0;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@planid", SqlDbType.Int, 0, ParameterDirection.Input, iPlanId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "plan_search_byid");
			List<CRMPackages> cRMPackages = new List<CRMPackages>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPackages>(sqlCommand, new GenerateListFromReader<CRMPackages>(CRMPackages.GeneratePlanListFromReader<CRMPackages>), ref cRMPackages);
			if (cRMPackages.Count <= 0)
			{
				return null;
			}
			return cRMPackages[0];
		}

		public static bool UpdatePlan(int iPlanId, string iPlanName, string iPlanDesc, string iCurrCode, double iMonthPrice, int iVoiceMins, int iMsgTexts, int iVideoMins, int iMsgVideos, int iDownloads, int iContractMonths)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@planid", SqlDbType.Int, 0, ParameterDirection.Input, iPlanId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@planname", SqlDbType.VarChar, 64, ParameterDirection.Input, iPlanName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@plandesc", SqlDbType.VarChar, 256, ParameterDirection.Input, iPlanDesc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, iCurrCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@monthprice", SqlDbType.Float, 0, ParameterDirection.Input, iMonthPrice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@voicemins", SqlDbType.Int, 0, ParameterDirection.Input, iVoiceMins);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msgtexts", SqlDbType.Int, 0, ParameterDirection.Input, iMsgTexts);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@videomins", SqlDbType.Int, 0, ParameterDirection.Input, iVideoMins);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msgvideos", SqlDbType.Int, 0, ParameterDirection.Input, iMsgVideos);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@downloads", SqlDbType.Int, 0, ParameterDirection.Input, iDownloads);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@contractmonths", SqlDbType.Int, 0, ParameterDirection.Input, iContractMonths);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "plan_update");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}
	}
}