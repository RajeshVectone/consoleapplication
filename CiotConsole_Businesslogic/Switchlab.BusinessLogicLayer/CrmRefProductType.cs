using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class CrmRefProductType
	{
		private const string SP_LIST = "r_product_type";

		private int _producttype;

		private string _producttypename;

		private string _producttypedesc;

		public int ProblemType
		{
			get
			{
				return this._producttype;
			}
		}

		public string ProblemTypeDesc
		{
			get
			{
				if (this._producttypedesc == null)
				{
					return string.Empty;
				}
				return this._producttypedesc;
			}
		}

		public string ProblemTypeName
		{
			get
			{
				if (this._producttypename == null)
				{
					return string.Empty;
				}
				return this._producttypename;
			}
		}

		public CrmRefProductType()
		{
		}

		public CrmRefProductType(int productType, string productTypeName, string productTypeDesc)
		{
			this._producttype = productType;
			this._producttypename = productTypeName;
			this._producttypedesc = productTypeDesc;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CrmRefProductType> listRef)
		{
			while (returnData.Read())
			{
				CrmRefProductType crmRefProductType = new CrmRefProductType(GeneralConverter.ToInt32(returnData["producttype"]), GeneralConverter.ToString(returnData["typename"]), GeneralConverter.ToString(returnData["typedesc"]));
				listRef.Add(crmRefProductType);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmRefProductType> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_product_type");
			List<CrmRefProductType> crmRefProductTypes = new List<CrmRefProductType>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmRefProductType>(sqlCommand, new GenerateListFromReader<CrmRefProductType>(CrmRefProductType.GenerateListFromReader<CrmRefProductType>), ref crmRefProductTypes);
			return crmRefProductTypes;
		}
	}
}