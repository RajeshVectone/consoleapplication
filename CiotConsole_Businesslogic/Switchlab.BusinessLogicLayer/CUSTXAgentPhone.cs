using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CUSTXAgentPhone
	{
		private const string SP_GET_AGENT_PHONE_STATUS = "agents_resCS_getPhoneStat3";

		private const string SP_CHANGE_AGENT_PHONE_STATUS = "cs_updateagentstatus";

		private string _ssessionid;

		private string _sserviceid;

		private string _sddi;

		private string _spromo;

		private string _scli;

		private string _sservice;

		private string _slanguage;

		private string _saccno;

		private string _smenu;

		private string _sextension;

		private string _sagentid;

		private int _istatus;

		private string _sstatusname;

		public string AccNo
		{
			get
			{
				if (this._saccno == null)
				{
					return string.Empty;
				}
				return this._saccno;
			}
			set
			{
				this._saccno = value;
			}
		}

		public string AgentID
		{
			get
			{
				if (this._sagentid == null)
				{
					return string.Empty;
				}
				return this._sagentid;
			}
			set
			{
				this._sagentid = value;
			}
		}

		public string CLI
		{
			get
			{
				if (this._scli == null)
				{
					return string.Empty;
				}
				return this._scli;
			}
			set
			{
				this._scli = value;
			}
		}

		public string DDI
		{
			get
			{
				if (this._sddi == null)
				{
					return string.Empty;
				}
				return this._sddi;
			}
			set
			{
				this._sddi = value;
			}
		}

		public string Extension
		{
			get
			{
				if (this._sextension == null)
				{
					return string.Empty;
				}
				return this._sextension;
			}
			set
			{
				this._sextension = value;
			}
		}

		public string Language
		{
			get
			{
				if (this._slanguage == null)
				{
					return string.Empty;
				}
				return this._slanguage;
			}
			set
			{
				this._slanguage = value;
			}
		}

		public string Menu
		{
			get
			{
				if (this._smenu == null)
				{
					return string.Empty;
				}
				return this._smenu;
			}
			set
			{
				this._smenu = value;
			}
		}

		public string Promo
		{
			get
			{
				if (this._spromo == null)
				{
					return string.Empty;
				}
				return this._spromo;
			}
			set
			{
				this._spromo = value;
			}
		}

		public string Service
		{
			get
			{
				if (this._sservice == null)
				{
					return string.Empty;
				}
				return this._sservice;
			}
			set
			{
				this._sservice = value;
			}
		}

		public string ServiceID
		{
			get
			{
				if (this._sserviceid == null)
				{
					return string.Empty;
				}
				return this._sserviceid;
			}
			set
			{
				this._sserviceid = value;
			}
		}

		public string SessionID
		{
			get
			{
				if (this._ssessionid == null)
				{
					return string.Empty;
				}
				return this._ssessionid;
			}
			set
			{
				this._ssessionid = value;
			}
		}

		public int Status
		{
			get
			{
				return this._istatus;
			}
			set
			{
				this._istatus = value;
			}
		}

		public string StatusName
		{
			get
			{
				if (this._sstatusname == null)
				{
					return string.Empty;
				}
				return this._sstatusname;
			}
			set
			{
				this._sstatusname = value;
			}
		}

		public CUSTXAgentPhone()
		{
		}

		public CUSTXAgentPhone(string sSessionID, string sServiceID, string sDDI, string sPromo, string sCLI, string sService, string sLanguage, string sAccNo, string sMenu, string sExtension, string sAgentID, int iStatus, string sStatusName)
		{
			this._ssessionid = sSessionID;
			this._sserviceid = sServiceID;
			this._sddi = sDDI;
			this._spromo = sPromo;
			this._scli = sCLI;
			this._sservice = sService;
			this._slanguage = sLanguage;
			this._saccno = sAccNo;
			this._smenu = sMenu;
			this._sextension = sExtension;
			this._sagentid = sAgentID;
			this._istatus = iStatus;
			this._sstatusname = sStatusName;
		}

		public static bool ChangeAgentPhoneStatusByLogin(string sLogin, int iStatus)
		{
			if (string.IsNullOrEmpty(sLogin))
			{
				throw new ArgumentOutOfRangeException("sLogin");
			}
			if (iStatus < 0)
			{
				throw new ArgumentOutOfRangeException("iStatus");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCustXConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@login", SqlDbType.VarChar, 16, ParameterDirection.Input, sLogin);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, iStatus);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cs_updateagentstatus");
			List<CUSTXAgentPhone> cUSTXAgentPhones = new List<CUSTXAgentPhone>();
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) == 1)
			{
				return true;
			}
			return false;
		}

		private static void GenerateAgentPhoneFromReader<T>(IDataReader returnData, ref List<CUSTXAgentPhone> listNew)
		{
			while (returnData.Read())
			{
				CUSTXAgentPhone cUSTXAgentPhone = new CUSTXAgentPhone(GeneralConverter.ToString(returnData["sessionid"]), GeneralConverter.ToString(returnData["serviceid"]), GeneralConverter.ToString(returnData["ddi"]), GeneralConverter.ToString(returnData["promo"]), GeneralConverter.ToString(returnData["cli"]), GeneralConverter.ToString(returnData["service"]), GeneralConverter.ToString(returnData["language"]), GeneralConverter.ToString(returnData["accno"]), GeneralConverter.ToString(returnData["menu"]), GeneralConverter.ToString(returnData["extension"]), GeneralConverter.ToString(returnData["agentid"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToString(returnData["statusname"]));
				listNew.Add(cUSTXAgentPhone);
			}
		}

		public static CUSTXAgentPhone GetAgentPhoneByLogin(string sLogin)
		{
			if (string.IsNullOrEmpty(sLogin))
			{
				throw new ArgumentOutOfRangeException("sLogin");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCustXConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@Agentid", SqlDbType.VarChar, 20, ParameterDirection.Input, sLogin);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "agents_resCS_getPhoneStat3");
			List<CUSTXAgentPhone> cUSTXAgentPhones = new List<CUSTXAgentPhone>();
			sQLDataAccessLayer.ExecuteReaderCmd<CUSTXAgentPhone>(sqlCommand, new GenerateListFromReader<CUSTXAgentPhone>(CUSTXAgentPhone.GenerateAgentPhoneFromReader<CUSTXAgentPhone>), ref cUSTXAgentPhones);
			if (cUSTXAgentPhones.Count != 1)
			{
				return null;
			}
			return cUSTXAgentPhones[0];
		}
	}
}