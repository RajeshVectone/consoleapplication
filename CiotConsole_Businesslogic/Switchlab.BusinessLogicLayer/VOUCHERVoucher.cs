using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class VOUCHERVoucher
	{
		private const string SP_GET_BY_PIN = "voucher_get_by_Pin";

		private const string SP_EDIT = "voucher_edit";

		private const string SP_VOUCHER_USE = "tp_do_topup_process";

		private const string SP_VOUCHER_USE_NL = "tp_do_topup_process_v2";

		private int _ivoucher_id;

		private double _dprice;

		private string _scurrcode;

		private string _spin;

		private string _sserialNumber;

		private int _ivoucherstatusid;

		private string _svoucherstatusname;

		private DateTime _dtcreatedate;

		private DateTime _dtexpdate;

		private DateTime _dtusedate;

		private string _scustcode;

		private int _ibatchcode;

		private int _iserialcode;

		public int BatchCode
		{
			get
			{
				return this._ibatchcode;
			}
			set
			{
				this._ibatchcode = value;
			}
		}

		public DateTime CreateDate
		{
			get
			{
				return this._dtcreatedate;
			}
			set
			{
				this._dtcreatedate = value;
			}
		}

		public string CurrencyCode
		{
			get
			{
				if (this._scurrcode == null)
				{
					return string.Empty;
				}
				return this._scurrcode;
			}
			set
			{
				this._scurrcode = value;
			}
		}

		public string CustomerCode
		{
			get
			{
				if (this._scustcode == null)
				{
					return string.Empty;
				}
				return this._scustcode;
			}
			set
			{
				this._scustcode = value;
			}
		}

		public DateTime ExpiredDate
		{
			get
			{
				return this._dtexpdate;
			}
			set
			{
				this._dtexpdate = value;
			}
		}

		public string Pin
		{
			get
			{
				if (this._spin == null)
				{
					return string.Empty;
				}
				return this._spin;
			}
			set
			{
				this._spin = value;
			}
		}

		public double Price
		{
			get
			{
				return this._dprice;
			}
			set
			{
				this._dprice = value;
			}
		}

		public int SerialCode
		{
			get
			{
				return this._iserialcode;
			}
			set
			{
				this._iserialcode = value;
			}
		}

		public string SerialNumber
		{
			get
			{
				if (this._sserialNumber == null)
				{
					return string.Empty;
				}
				return this._sserialNumber;
			}
			set
			{
				this._sserialNumber = value;
			}
		}

		public DateTime UseDate
		{
			get
			{
				return this._dtusedate;
			}
			set
			{
				this._dtusedate = value;
			}
		}

		public int VoucherID
		{
			get
			{
				return this._ivoucher_id;
			}
			set
			{
				this._ivoucher_id = value;
			}
		}

		public int VoucherStatusID
		{
			get
			{
				return this._ivoucherstatusid;
			}
			set
			{
				this._ivoucherstatusid = value;
			}
		}

		public string VoucherStatusName
		{
			get
			{
				if (this._svoucherstatusname == null)
				{
					return string.Empty;
				}
				return this._svoucherstatusname;
			}
			set
			{
				this._svoucherstatusname = value;
			}
		}

		public VOUCHERVoucher()
		{
		}

		public VOUCHERVoucher(int iVoucherID, double dPrice, string sCurrencyCode, string sPin, string sSerialNumber, int iVoucherStatusID, string sVoucherStatusName, DateTime dtCreateDate, DateTime dtExpiredDate, DateTime dtUseDate, string sCustomerCode, int iBatchCode, int iSerialCode)
		{
			this._ivoucher_id = iVoucherID;
			this._dprice = dPrice;
			this._scurrcode = sCurrencyCode;
			this._spin = sPin;
			this._sserialNumber = sSerialNumber;
			this._ivoucherstatusid = iVoucherStatusID;
			this._svoucherstatusname = sVoucherStatusName;
			this._dtcreatedate = dtCreateDate;
			this._dtexpdate = dtExpiredDate;
			this._dtusedate = dtUseDate;
			this._scustcode = sCustomerCode;
			this._ibatchcode = iBatchCode;
			this._iserialcode = iSerialCode;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListVoucherFromReader<T>(IDataReader returnData, ref List<VOUCHERVoucher> theList)
		{
			while (returnData.Read())
			{
				VOUCHERVoucher vOUCHERVoucher = new VOUCHERVoucher(GeneralConverter.ToInt32(returnData["voucher_id"]), GeneralConverter.ToDouble(returnData["price"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToString(returnData["pin"]), GeneralConverter.ToString(returnData["serialNumber"]), GeneralConverter.ToInt32(returnData["voucherStatusID"]), GeneralConverter.ToString(returnData["voucherStatusName"]), GeneralConverter.ToDateTime(returnData["createDate"]), GeneralConverter.ToDateTime(returnData["expDate"]), GeneralConverter.ToDateTime(returnData["useDate"]), GeneralConverter.ToString(returnData["custcode"]), GeneralConverter.ToInt32(returnData["batchcode"]), GeneralConverter.ToInt32(returnData["serialcode"]));
				theList.Add(vOUCHERVoucher);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static VOUCHERVoucher GetByPin(string sPin)
		{
			if (sPin == string.Empty)
			{
				throw new ArgumentOutOfRangeException("Pin");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetVoucherConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pin", SqlDbType.VarChar, 16, ParameterDirection.Input, sPin);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "voucher_get_by_Pin");
			List<VOUCHERVoucher> vOUCHERVouchers = new List<VOUCHERVoucher>();
			sQLDataAccessLayer.ExecuteReaderCmd<VOUCHERVoucher>(sqlCommand, new GenerateListFromReader<VOUCHERVoucher>(VOUCHERVoucher.GenerateListVoucherFromReader<VOUCHERVoucher>), ref vOUCHERVouchers);
			if (vOUCHERVouchers == null || vOUCHERVouchers.Count != 1)
			{
				return null;
			}
			return vOUCHERVouchers[0];
		}

		public static bool UseVoucher(string sMobileNo, string sPin, ref int iErrorCode, ref string sErrorMessage, ref double dAfterBalance, ref string sCurrencyCode, ref double dPrice, ref string sVoucherCurrency, ref string sIncentifData)
		{
			if (sPin == string.Empty)
			{
				throw new ArgumentOutOfRangeException("Pin");
			}
			if (sMobileNo == string.Empty)
			{
				throw new ArgumentOutOfRangeException("Mobile No");
			}
			if (VOUCHERVoucher.GetByPin(sPin) == null)
			{
				iErrorCode = -1;
				sErrorMessage = "Incorrect pin";
				return true;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msisdn", SqlDbType.VarChar, 16, ParameterDirection.Input, sMobileNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pin", SqlDbType.VarChar, 10, ParameterDirection.Input, sPin);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@errcode", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@errmsg", SqlDbType.VarChar, 100, ParameterDirection.Output, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@afterbal", SqlDbType.Float, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 3, ParameterDirection.Output, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@price", SqlDbType.Float, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@vou_curr", SqlDbType.VarChar, 3, ParameterDirection.Output, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@incentif_data", SqlDbType.VarChar, 10, ParameterDirection.Output, null);
			if (!sMobileNo.StartsWith("31"))
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "tp_do_topup_process");
			}
			else
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "tp_do_topup_process_v2");
			}
			if ((object)sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) == DBNull.Value)
			{
				return false;
			}
			iErrorCode = GeneralConverter.ToInt32(sqlCommand.Parameters["@errcode"].Value);
			sErrorMessage = sqlCommand.Parameters["@errmsg"].Value.ToString();
			dAfterBalance = GeneralConverter.ToDouble(sqlCommand.Parameters["@afterbal"].Value);
			sCurrencyCode = sqlCommand.Parameters["@currcode"].Value.ToString();
			dPrice = GeneralConverter.ToDouble(sqlCommand.Parameters["@price"].Value);
			sVoucherCurrency = sqlCommand.Parameters["@vou_curr"].Value.ToString();
			sIncentifData = sqlCommand.Parameters["@incentif_data"].Value.ToString();
			return true;
		}
	}
}