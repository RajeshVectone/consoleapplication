using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class NRDBMessageType
	{
		private const string SP_GET_MESSAGE_TYPE_NEW_CASE_LIST = "message_type_new_case_list";

		private int _id;

		private string _name;

		public int Id
		{
			get
			{
				return this._id;
			}
			set
			{
				this._id = value;
			}
		}

		public string Name
		{
			get
			{
				if (this._name == null)
				{
					return string.Empty;
				}
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public NRDBMessageType()
		{
		}

		public NRDBMessageType(int id, string name)
		{
			this._id = id;
			this._name = name;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<NRDBMessageType> listRef)
		{
			while (returnData.Read())
			{
				NRDBMessageType nRDBMessageType = new NRDBMessageType(GeneralConverter.ToInt32(returnData["id"]), GeneralConverter.ToString(returnData["name"]));
				listRef.Add(nRDBMessageType);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<NRDBMessageType> GetMessageTypeNewCaseList()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNRDBConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "message_type_new_case_list");
			List<NRDBMessageType> nRDBMessageTypes = new List<NRDBMessageType>();
			sQLDataAccessLayer.ExecuteReaderCmd<NRDBMessageType>(sqlCommand, new GenerateListFromReader<NRDBMessageType>(NRDBMessageType.GenerateListFromReader<NRDBMessageType>), ref nRDBMessageTypes);
			return nRDBMessageTypes;
		}
	}
}