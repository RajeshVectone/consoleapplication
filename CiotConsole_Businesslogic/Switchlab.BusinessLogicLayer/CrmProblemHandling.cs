using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CrmProblemHandling
	{
		private const string SP_LIST_BY_ID = "prob_search_byID";

		private const string SP_LIST_BY_TYPE = "prob_search_bytype";

		private const string SP_LIST_BY_NAME = "prob_search_byname";

		private const string SP_LIST_BY_STATUS = "prob_search_bystatus";

		private const string SP_LIST_BY_GROUPS = "prob_list_by_group";

		private const string SP_LIST_HISTORY = "list_prob_history";

		private const string SP_LIST_ALL = "list_prob_all";

		private const string SP_LIST_TOP100 = "prob_list_top100";

		private const string SP_LIST_TOP_LAST_EDIT = "prob_top_last_edit";

		private const string SP_CREATE = "prob_create";

		private const string SP_UPDATE = "prob_update";

		private int _subscriberid;

		private int _problemid;

		private int _groupid;

		private int _problemtype;

		private int _problemstatus;

		private string _problemdesc;

		private string _submitby;

		private string _firstname;

		private string _lastname;

		private string _typename;

		private string _statusname;

		private string _solutiondesc;

		private DateTime _submitdate;

		private DateTime _editdate;

		private string _smobileno;

		private string _siccid;

		public DateTime EdittDate
		{
			get
			{
				return this._editdate;
			}
			set
			{
				this._editdate = value;
			}
		}

		public string FirstName
		{
			get
			{
				if (this._firstname == null)
				{
					return string.Empty;
				}
				return this._firstname;
			}
			set
			{
				this._firstname = value;
			}
		}

		public int GroupID
		{
			get
			{
				return this._groupid;
			}
			set
			{
				this._groupid = value;
			}
		}

		public string ICCID
		{
			get
			{
				if (this._siccid == null)
				{
					return string.Empty;
				}
				return this._siccid;
			}
			set
			{
				this._siccid = value;
			}
		}

		public string LastName
		{
			get
			{
				if (this._lastname == null)
				{
					return string.Empty;
				}
				return this._lastname;
			}
			set
			{
				this._lastname = value;
			}
		}

		public string MobileNo
		{
			get
			{
				if (this._smobileno == null)
				{
					return string.Empty;
				}
				return this._smobileno;
			}
			set
			{
				this._smobileno = value;
			}
		}

		public string ProblemDesc
		{
			get
			{
				if (this._problemdesc == null)
				{
					return string.Empty;
				}
				return this._problemdesc;
			}
			set
			{
				this._problemdesc = value;
			}
		}

		public int ProblemID
		{
			get
			{
				return this._problemid;
			}
			set
			{
				this._problemid = value;
			}
		}

		public int ProblemStatus
		{
			get
			{
				return this._problemstatus;
			}
			set
			{
				this._problemstatus = value;
			}
		}

		public int ProblemType
		{
			get
			{
				return this._problemtype;
			}
			set
			{
				this._problemtype = value;
			}
		}

		public string SolutionDesc
		{
			get
			{
				if (this._solutiondesc == null)
				{
					return string.Empty;
				}
				return this._solutiondesc;
			}
			set
			{
				this._solutiondesc = value;
			}
		}

		public string StatusName
		{
			get
			{
				if (this._statusname == null)
				{
					return string.Empty;
				}
				return this._statusname;
			}
			set
			{
				this._statusname = value;
			}
		}

		public string SubmitBy
		{
			get
			{
				if (this._submitby == null)
				{
					return string.Empty;
				}
				return this._submitby;
			}
			set
			{
				this._submitby = value;
			}
		}

		public DateTime SubmitDate
		{
			get
			{
				return this._submitdate;
			}
			set
			{
				this._submitdate = value;
			}
		}

		public int SubscriberID
		{
			get
			{
				return this._subscriberid;
			}
			set
			{
				this._subscriberid = value;
			}
		}

		public string TypeName
		{
			get
			{
				if (this._typename == null)
				{
					return string.Empty;
				}
				return this._typename;
			}
			set
			{
				this._typename = value;
			}
		}

		public CrmProblemHandling()
		{
		}

		public CrmProblemHandling(int problemId, string typeName, int subscriberID, DateTime submitDate, string statusName, string solutionDesc, string firstName, string lastName)
		{
			this._subscriberid = subscriberID;
			this._typename = typeName;
			this._submitdate = submitDate;
			this._problemid = problemId;
			this._statusname = statusName;
			this._solutiondesc = solutionDesc;
			this._firstname = firstName;
			this._lastname = lastName;
		}

		public CrmProblemHandling(int subscriberID, string firstName, string lastName, int problemId, string typeName, string statusName, string solutionDesc, DateTime submitDate, DateTime editDate, string submitBy, string problemDesc, int problemType)
		{
			this._subscriberid = subscriberID;
			this._firstname = firstName;
			this._lastname = lastName;
			this._problemid = problemId;
			this._typename = typeName;
			this._statusname = statusName;
			this._solutiondesc = solutionDesc;
			this._submitdate = submitDate;
			this._editdate = editDate;
			this._submitby = submitBy;
			this._problemdesc = problemDesc;
			this._problemtype = problemType;
		}

		public CrmProblemHandling(int problemId, DateTime editDate, string statusName, string solutionDesc)
		{
			this._problemid = problemId;
			this._editdate = editDate;
			this._statusname = statusName;
			this._solutiondesc = solutionDesc;
		}

		public CrmProblemHandling(int iProblemId, DateTime dtEditDate, int iSubscriberID, string sMobileNo, string sICCID, string sFirstName, string sLastName)
		{
			this._problemid = iProblemId;
			this._editdate = dtEditDate;
			this._subscriberid = iSubscriberID;
			this._smobileno = sMobileNo;
			this._siccid = sICCID;
			this._firstname = sFirstName;
			this._lastname = sLastName;
		}

		public static bool CreateProblem(int iproblemType, int iSubscriberId, string iProblemDesc, string iSubmitBy, int iProblemStatus, string sSolution)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemtype", SqlDbType.Int, 0, ParameterDirection.Input, iproblemType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscriberid", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemdesc", SqlDbType.VarChar, 1024, ParameterDirection.Input, iProblemDesc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@submitby", SqlDbType.VarChar, 16, ParameterDirection.Input, iSubmitBy);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemstatus", SqlDbType.Int, 0, ParameterDirection.Input, iProblemStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@solutiondesc", SqlDbType.VarChar, 1024, ParameterDirection.Input, sSolution);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_create");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		private static void GenerateProblemHistoryFromReader<T>(IDataReader returnData, ref List<CrmProblemHandling> listNew)
		{
			while (returnData.Read())
			{
				CrmProblemHandling crmProblemHandling = new CrmProblemHandling(GeneralConverter.ToInt32(returnData["problemid"]), GeneralConverter.ToDateTime(returnData["editdate"]), GeneralConverter.ToString(returnData["status"]), GeneralConverter.ToString(returnData["solutiondesc"]));
				listNew.Add(crmProblemHandling);
			}
		}

		private static void GenerateProblemListFromReader<T>(IDataReader returnData, ref List<CrmProblemHandling> listNew)
		{
			while (returnData.Read())
			{
				CrmProblemHandling crmProblemHandling = new CrmProblemHandling(GeneralConverter.ToInt32(returnData["problemid"]), GeneralConverter.ToString(returnData["typeName"]), GeneralConverter.ToInt32(returnData["subscriberid"]), GeneralConverter.ToDateTime(returnData["submitdate"]), GeneralConverter.ToString(returnData["statusName"]), GeneralConverter.ToString(returnData["solutiondesc"]), GeneralConverter.ToString(returnData["firstName"]), GeneralConverter.ToString(returnData["lastName"]));
				listNew.Add(crmProblemHandling);
			}
		}

		private static void GenerateProblemReminderFromReader<T>(IDataReader returnData, ref List<CrmProblemHandling> listNew)
		{
			while (returnData.Read())
			{
				CrmProblemHandling crmProblemHandling = new CrmProblemHandling(GeneralConverter.ToInt32(returnData["problemid"]), GeneralConverter.ToDateTime(returnData["editdate"]), GeneralConverter.ToInt32(returnData["subscriberid"]), GeneralConverter.ToString(returnData["mobileno"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToString(returnData["firstname"]), GeneralConverter.ToString(returnData["lastname"]));
				listNew.Add(crmProblemHandling);
			}
		}

		private static void GenerateSearchProblemListFromReader<T>(IDataReader returnData, ref List<CrmProblemHandling> listNew)
		{
			while (returnData.Read())
			{
				CrmProblemHandling crmProblemHandling = new CrmProblemHandling(GeneralConverter.ToInt32(returnData["subscriberid"]), GeneralConverter.ToString(returnData["firstname"]), GeneralConverter.ToString(returnData["lastname"]), GeneralConverter.ToInt32(returnData["problemid"]), GeneralConverter.ToString(returnData["typename"]), GeneralConverter.ToString(returnData["statusname"]), GeneralConverter.ToString(returnData["solutiondesc"]), GeneralConverter.ToDateTime(returnData["submitdate"]), GeneralConverter.ToDateTime(returnData["editdate"]), GeneralConverter.ToString(returnData["submitby"]), GeneralConverter.ToString(returnData["problemdesc"]), GeneralConverter.ToInt32(returnData["problemtype"]));
				listNew.Add(crmProblemHandling);
			}
		}

		public static CrmProblemHandling GetProblemById(int iProblemId)
		{
			if (iProblemId < 0)
			{
				throw new ArgumentOutOfRangeException("iProblemId");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ProblemId", SqlDbType.Int, 0, ParameterDirection.Input, iProblemId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_search_byID");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateSearchProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			if (crmProblemHandlings.Count <= 0)
			{
				return null;
			}
			return crmProblemHandlings[0];
		}

		public static List<CrmProblemHandling> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "list_prob_all");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			return crmProblemHandlings;
		}

		public static List<CrmProblemHandling> ListByGroups(int iGroup_id)
		{
			if (iGroup_id < 0)
			{
				throw new ArgumentOutOfRangeException("group_id");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@group_id", SqlDbType.Int, 0, ParameterDirection.Input, iGroup_id);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_list_by_group");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateSearchProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			return crmProblemHandlings;
		}

		public static List<CrmProblemHandling> ListById(int iProblemId, int flag)
		{
			if (iProblemId < 0)
			{
				throw new ArgumentOutOfRangeException("iProblemId");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemid", SqlDbType.Int, 0, ParameterDirection.Input, iProblemId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_search_byID");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			if (flag != 0)
			{
				sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			}
			else
			{
				sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateSearchProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			}
			return crmProblemHandlings;
		}

		public static List<CrmProblemHandling> ListByName(string sName)
		{
			if (sName == string.Empty)
			{
				sName = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 61, ParameterDirection.Input, sName);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_search_byname");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateSearchProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			return crmProblemHandlings;
		}

		public static List<CrmProblemHandling> ListByStatus(int sstatus)
		{
			if (sstatus < 0)
			{
				sstatus = 0;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemstatus", SqlDbType.Int, 0, ParameterDirection.Input, sstatus);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_search_bystatus");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateSearchProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			return crmProblemHandlings;
		}

		public static List<CrmProblemHandling> ListByType(int stype)
		{
			if (stype < 0)
			{
				stype = 0;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemtype", SqlDbType.Int, 0, ParameterDirection.Input, stype);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_search_bytype");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateSearchProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			return crmProblemHandlings;
		}

		public static List<CrmProblemHandling> ListHistory(int iProblemId)
		{
			if (iProblemId < 0)
			{
				iProblemId = 0;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemid", SqlDbType.Int, 0, ParameterDirection.Input, iProblemId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "list_prob_history");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateProblemHistoryFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			return crmProblemHandlings;
		}

		public static List<CrmProblemHandling> ListTop100Problem()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_list_top100");
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateSearchProblemListFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			return crmProblemHandlings;
		}

		public static List<CrmProblemHandling> ListUpdated(string username)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_top_last_edit");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@submitby", SqlDbType.VarChar, 16, ParameterDirection.Input, username);
			List<CrmProblemHandling> crmProblemHandlings = new List<CrmProblemHandling>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmProblemHandling>(sqlCommand, new GenerateListFromReader<CrmProblemHandling>(CrmProblemHandling.GenerateProblemReminderFromReader<CrmProblemHandling>), ref crmProblemHandlings);
			if (crmProblemHandlings.Count > 0)
			{
				return crmProblemHandlings;
			}
			return null;
		}

		public static bool UpdateProblem(int iProblemId, int iproblemType, string iProblemDesc, int iProblemStatus, string iSolutionDesc)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemid", SqlDbType.Int, 0, ParameterDirection.Input, iProblemId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemtype", SqlDbType.Int, 0, ParameterDirection.Input, iproblemType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemdesc", SqlDbType.VarChar, 1024, ParameterDirection.Input, iProblemDesc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@problemstatus", SqlDbType.Int, 0, ParameterDirection.Input, iProblemStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@solutiondesc", SqlDbType.VarChar, 1024, ParameterDirection.Input, iSolutionDesc);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "prob_update");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}
	}
}