using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMRefUserStatus
	{
		private const string SP_LIST = "r_user_stat_list";

		private int _istatusid;

		private string _sstatusname;

		public int StatusID
		{
			get
			{
				return this._istatusid;
			}
		}

		public string StatusName
		{
			get
			{
				if (this._sstatusname == null)
				{
					return string.Empty;
				}
				return this._sstatusname;
			}
		}

		public CRMRefUserStatus()
		{
		}

		public CRMRefUserStatus(int iStatusID, string sStatusName)
		{
			this._istatusid = iStatusID;
			this._sstatusname = sStatusName;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CRMRefUserStatus> listRef)
		{
			while (returnData.Read())
			{
				CRMRefUserStatus cRMRefUserStatu = new CRMRefUserStatus(GeneralConverter.ToInt32(returnData["status"]), Convert.ToString(returnData["name"]));
				listRef.Add(cRMRefUserStatu);
			}
		}

		public static List<CRMRefUserStatus> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_user_stat_list");
			List<CRMRefUserStatus> cRMRefUserStatuses = new List<CRMRefUserStatus>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRefUserStatus>(sqlCommand, new GenerateListFromReader<CRMRefUserStatus>(CRMRefUserStatus.GenerateListFromReader<CRMRefUserStatus>), ref cRMRefUserStatuses);
			return cRMRefUserStatuses;
		}
	}
}