using System;

namespace Switchlab.BusinessLogicLayer
{
	public class NRDBMessage
	{
		private int _id;

		private int _typeId;

		private string _sender;

		private long? _caseId;

		private int _sequence;

		private string _authRef;

		private string _custId;

		private string _custName;

		private string _subscribeTypeCode;

		private string _subscribeTypeName;

		private string _telephoneNumberMain;

		private string[] _telephoneNumberAdd;

		private int? _portEnded;

		private int? _portEndedMain;

		private int[] _portEndedAdd;

		private DateTime? _portingDate;

		private string _municipFrom;

		private string _municipTo;

		private string _serviceFrom;

		private string _serviceTo;

		private string _rtsp;

		private string _status;

		private string _messageTypeName;

		private string _sentDateTime;

		private string _contactName;

		private string _contactTelephone;

		private string _contactEmail;

		private int? _coordinatePorting;

		private int? _errorCode;

		private string _errorComment;

		public string AuthRef
		{
			get
			{
				if (this._authRef == null)
				{
					return string.Empty;
				}
				return this._authRef;
			}
			set
			{
				this._authRef = value;
			}
		}

		public long? CaseId
		{
			get
			{
				return this._caseId;
			}
			set
			{
				this._caseId = value;
			}
		}

		public string ContactEmail
		{
			get
			{
				if (this._contactEmail == null)
				{
					return string.Empty;
				}
				return this._contactEmail;
			}
			set
			{
				this._contactEmail = value;
			}
		}

		public string ContactName
		{
			get
			{
				if (this._contactName == null)
				{
					return string.Empty;
				}
				return this._contactName;
			}
			set
			{
				this._contactName = value;
			}
		}

		public string ContactTelephone
		{
			get
			{
				if (this._contactTelephone == null)
				{
					return string.Empty;
				}
				return this._contactTelephone;
			}
			set
			{
				this._contactTelephone = value;
			}
		}

		public int? CoordinatePorting
		{
			get
			{
				return this._coordinatePorting;
			}
			set
			{
				this._coordinatePorting = value;
			}
		}

		public string CustId
		{
			get
			{
				if (this._custId == null)
				{
					return string.Empty;
				}
				return this._custId;
			}
			set
			{
				this._custId = value;
			}
		}

		public string CustName
		{
			get
			{
				if (this._custName == null)
				{
					return string.Empty;
				}
				return this._custName;
			}
			set
			{
				this._custName = value;
			}
		}

		public int? ErrorCode
		{
			get
			{
				return this._errorCode;
			}
			set
			{
				this._errorCode = value;
			}
		}

		public string ErrorComment
		{
			get
			{
				return this._errorComment;
			}
			set
			{
				this._errorComment = value;
			}
		}

		public int Id
		{
			get
			{
				return this._id;
			}
		}

		public string MessageTypeName
		{
			get
			{
				if (this._messageTypeName == null)
				{
					return string.Empty;
				}
				return this._messageTypeName;
			}
		}

		public string MunicipFrom
		{
			get
			{
				if (this._municipFrom == null)
				{
					return string.Empty;
				}
				return this._municipFrom;
			}
			set
			{
				this._municipFrom = value;
			}
		}

		public string MunicipTo
		{
			get
			{
				if (this._municipTo == null)
				{
					return string.Empty;
				}
				return this._municipTo;
			}
			set
			{
				this._municipTo = value;
			}
		}

		public int? PortEnded
		{
			get
			{
				return this._portEnded;
			}
			set
			{
				this._portEnded = value;
			}
		}

		public int[] PortEndedAdd
		{
			get
			{
				return this._portEndedAdd;
			}
			set
			{
				this._portEndedAdd = value;
			}
		}

		public int? PortEndedMain
		{
			get
			{
				return this._portEndedMain;
			}
			set
			{
				this._portEndedMain = value;
			}
		}

		public DateTime? PortingDate
		{
			get
			{
				return this._portingDate;
			}
			set
			{
				this._portingDate = value;
			}
		}

		public string Rtsp
		{
			get
			{
				if (this._rtsp == null)
				{
					return string.Empty;
				}
				return this._rtsp;
			}
			set
			{
				this._rtsp = value;
			}
		}

		public string Sender
		{
			get
			{
				if (this._sender == null)
				{
					return string.Empty;
				}
				return this._sender;
			}
			set
			{
				this._sender = value;
			}
		}

		public string SentDateTime
		{
			get
			{
				if (this._sentDateTime == null)
				{
					return string.Empty;
				}
				return this._sentDateTime;
			}
			set
			{
				this._sentDateTime = value;
			}
		}

		public int Sequence
		{
			get
			{
				return this._sequence;
			}
			set
			{
				this._sequence = value;
			}
		}

		public string ServiceFrom
		{
			get
			{
				if (this._serviceFrom == null)
				{
					return string.Empty;
				}
				return this._serviceFrom;
			}
			set
			{
				this._serviceFrom = value;
			}
		}

		public string ServiceTo
		{
			get
			{
				if (this._serviceTo == null)
				{
					return string.Empty;
				}
				return this._serviceTo;
			}
			set
			{
				this._serviceTo = value;
			}
		}

		public string Status
		{
			get
			{
				if (this._status == null)
				{
					return string.Empty;
				}
				return this._status;
			}
			set
			{
				this._status = value;
			}
		}

		public string SubscribeTypeCode
		{
			get
			{
				if (this._subscribeTypeCode == null)
				{
					return string.Empty;
				}
				return this._subscribeTypeCode;
			}
			set
			{
				this._subscribeTypeCode = value;
			}
		}

		public string SubscribeTypeName
		{
			get
			{
				if (this._subscribeTypeName == null)
				{
					return string.Empty;
				}
				return this._subscribeTypeName;
			}
		}

		public string[] TelephoneNumberAdd
		{
			get
			{
				return this._telephoneNumberAdd;
			}
			set
			{
				this._telephoneNumberAdd = value;
			}
		}

		public string TelephoneNumberMain
		{
			get
			{
				if (this._telephoneNumberMain == null)
				{
					return string.Empty;
				}
				return this._telephoneNumberMain;
			}
			set
			{
				this._telephoneNumberMain = value;
			}
		}

		public int TypeId
		{
			get
			{
				return this._typeId;
			}
			set
			{
				this._typeId = value;
			}
		}

		public NRDBMessage()
		{
		}

		public NRDBMessage(int id, string sender, long caseId, string authRef, string subscribeTypeName, string telpNumberMain, DateTime portingDate, string status, string messageTypeName)
		{
			this._id = id;
			this._typeId = 0;
			this._sender = sender;
			this._caseId = new long?(caseId);
			this._authRef = authRef;
			this._subscribeTypeName = subscribeTypeName;
			this._telephoneNumberMain = telpNumberMain;
			if (portingDate == DateTime.MinValue)
			{
				this._portingDate = null;
			}
			this._status = status;
			this._messageTypeName = messageTypeName;
		}

		public NRDBMessage(int id, string messageTypeName, int sequence, string sentDateTime, string subscribeTypeName, string telpNumberMain, string authRef)
		{
			this._id = id;
			this._messageTypeName = messageTypeName;
			this._sequence = sequence;
			this._sentDateTime = sentDateTime;
			this._subscribeTypeName = subscribeTypeName;
			this._telephoneNumberMain = telpNumberMain;
			this._authRef = authRef;
		}
	}
}