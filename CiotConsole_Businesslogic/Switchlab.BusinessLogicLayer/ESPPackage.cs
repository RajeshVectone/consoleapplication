using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	internal class ESPPackage
	{
		private const string SP_VIEW_PACKAGE_BY_PACKAGEID = "rp1_view_package_by_packageid";

		private int _ipackage_id;

		private string _spackagename;

		private int _ipackagetypeid;

		private double _dprice;

		private string _scurrcode;

		private int _iduration;

		private string _sdescription;

		private string _sprompt;

		private string _spromptbal;

		private string _spromptunit;

		private int _ipriority;

		private string _stariffclass;

		private int _icardid;

		public int Card_ID
		{
			get
			{
				return this._icardid;
			}
			set
			{
				this._icardid = value;
			}
		}

		public string CurrencyCode
		{
			get
			{
				if (this._scurrcode == null)
				{
					return string.Empty;
				}
				return this._scurrcode;
			}
			set
			{
				this._scurrcode = value;
			}
		}

		public string Description
		{
			get
			{
				if (this._sdescription == null)
				{
					return string.Empty;
				}
				return this._sdescription;
			}
			set
			{
				this._sdescription = value;
			}
		}

		public int Duration
		{
			get
			{
				return this._iduration;
			}
			set
			{
				this._iduration = value;
			}
		}

		public int Package_ID
		{
			get
			{
				return this._ipackage_id;
			}
			set
			{
				this._ipackage_id = value;
			}
		}

		public string PackageName
		{
			get
			{
				if (this._spackagename == null)
				{
					return string.Empty;
				}
				return this._spackagename;
			}
			set
			{
				this._spackagename = value;
			}
		}

		public int PackageTypeID
		{
			get
			{
				return this._ipackagetypeid;
			}
			set
			{
				this._ipackagetypeid = value;
			}
		}

		public double Price
		{
			get
			{
				return this._dprice;
			}
			set
			{
				this._dprice = value;
			}
		}

		public int Priority
		{
			get
			{
				return this._ipriority;
			}
			set
			{
				this._ipriority = value;
			}
		}

		public string Prompt
		{
			get
			{
				if (this._sprompt == null)
				{
					return string.Empty;
				}
				return this._sprompt;
			}
			set
			{
				this._sprompt = value;
			}
		}

		public string PromptBal
		{
			get
			{
				if (this._spromptbal == null)
				{
					return string.Empty;
				}
				return this._spromptbal;
			}
			set
			{
				this._spromptbal = value;
			}
		}

		public string PromptUnit
		{
			get
			{
				if (this._spromptunit == null)
				{
					return string.Empty;
				}
				return this._spromptunit;
			}
			set
			{
				this._spromptunit = value;
			}
		}

		public string TariffClass
		{
			get
			{
				if (this._stariffclass == null)
				{
					return string.Empty;
				}
				return this._stariffclass;
			}
			set
			{
				this._stariffclass = value;
			}
		}

		public ESPPackage()
		{
		}

		public ESPPackage(int iPackageID, string sPackageName, int iPackageTypeID, double dPrice, string sCurrCode, int iDuration, string sDescription, string sPrompt, string sPromptBal, string sPromptUnit, int iPriority, string sTariffClass, int iCardID)
		{
			this._ipackage_id = iPackageID;
			this._spackagename = sPackageName;
			this._ipackagetypeid = iPackageTypeID;
			this._dprice = dPrice;
			this._scurrcode = sCurrCode;
			this._iduration = iDuration;
			this._sdescription = sDescription;
			this._sprompt = sPrompt;
			this._spromptbal = sPromptBal;
			this._spromptunit = sPromptUnit;
			this._ipriority = iPriority;
			this._stariffclass = sTariffClass;
			this._icardid = iCardID;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<ESPPackage> theList)
		{
			while (returnData.Read())
			{
				ESPPackage eSPPackage = new ESPPackage(GeneralConverter.ToInt32(returnData["package_id"]), GeneralConverter.ToString(returnData["name"]), GeneralConverter.ToInt32(returnData["packageTypeID"]), GeneralConverter.ToDouble(returnData["price"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToInt32(returnData["duration"]), GeneralConverter.ToString(returnData["deskription"]), GeneralConverter.ToString(returnData["prompt"]), GeneralConverter.ToString(returnData["promptBal"]), GeneralConverter.ToString(returnData["promptUnit"]), GeneralConverter.ToInt32(returnData["priority"]), GeneralConverter.ToString(returnData["tariffClass"]), GeneralConverter.ToInt32(returnData["card_id"]));
				theList.Add(eSPPackage);
			}
		}

		public static ESPPackage getPackageByPackageID(int iPackageID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@package_id", SqlDbType.Int, 0, ParameterDirection.Input, iPackageID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_view_package_by_packageid");
			List<ESPPackage> eSPPackages = new List<ESPPackage>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPPackage>(sqlCommand, new GenerateListFromReader<ESPPackage>(ESPPackage.GenerateListFromReader<ESPPackage>), ref eSPPackages);
			if (eSPPackages.Count != 1)
			{
				return null;
			}
			return eSPPackages[0];
		}
	}
}