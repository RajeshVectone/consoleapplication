using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class HLRLocationUpdate
	{
		private const string SP_LOCATION_UPDATE = "hlr_get_upd_loc";

		private string _iccid;

		private string _imsi;

		private string _alertMsisdn;

		private string _vlrNumber;

		private string _mscNumber;

		private string _firstUpdate;

		private string _lastUpdate;

		public string AlertMsisdn
		{
			get
			{
				return this._alertMsisdn;
			}
			set
			{
				this._alertMsisdn = value;
			}
		}

		public string FirstUpdate
		{
			get
			{
				return this._firstUpdate;
			}
			set
			{
				this._firstUpdate = value;
			}
		}

		public string ICCID
		{
			get
			{
				return this._iccid;
			}
			set
			{
				this._iccid = value;
			}
		}

		public string IMSI
		{
			get
			{
				return this._imsi;
			}
			set
			{
				this._imsi = value;
			}
		}

		public string LastUpdate
		{
			get
			{
				return this._lastUpdate;
			}
			set
			{
				this._lastUpdate = value;
			}
		}

		public string MscNumber
		{
			get
			{
				return this._mscNumber;
			}
			set
			{
				this._mscNumber = value;
			}
		}

		public string VlrNumber
		{
			get
			{
				return this._vlrNumber;
			}
			set
			{
				this._vlrNumber = value;
			}
		}

		public HLRLocationUpdate()
		{
		}

		public HLRLocationUpdate(string iccid, string imsi, string alertMsisdn, string vlrNumber, string mscNumber, string firstUpdate, string lastUpdate)
		{
			this._iccid = iccid;
			this._imsi = imsi;
			this._alertMsisdn = alertMsisdn;
			this._vlrNumber = vlrNumber;
			this._mscNumber = mscNumber;
			this._firstUpdate = firstUpdate;
			this._lastUpdate = lastUpdate;
		}

		private static void GenerateHlrListFromReader<T>(IDataReader returnData, ref List<HLRLocationUpdate> listNew)
		{
			while (returnData.Read())
			{
				HLRLocationUpdate hLRLocationUpdate = new HLRLocationUpdate(Convert.ToString(returnData["iccid"]), Convert.ToString(returnData["imsi"]), Convert.ToString(returnData["alertmsisdn"]), Convert.ToString(returnData["vlrNumber"]), Convert.ToString(returnData["mscNumber"]), Convert.ToString(returnData["firstUpdate"]), Convert.ToString(returnData["lastUpdate"]));
				listNew.Add(hLRLocationUpdate);
			}
		}

		public static List<HLRLocationUpdate> GetLocationUpdate(string countryPrefix)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetHLRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countryprefix", SqlDbType.Char, 2, ParameterDirection.Input, countryPrefix);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "hlr_get_upd_loc");
			List<HLRLocationUpdate> hLRLocationUpdates = new List<HLRLocationUpdate>();
			sQLDataAccessLayer.ExecuteReaderCmd<HLRLocationUpdate>(sqlCommand, new GenerateListFromReader<HLRLocationUpdate>(HLRLocationUpdate.GenerateHlrListFromReader<HLRLocationUpdate>), ref hLRLocationUpdates);
			return hLRLocationUpdates;
		}
	}
}