using System;
using System.Configuration;
using System.Xml;

namespace Switchlab.BusinessLogicLayer
{
	public class ResellerConnectionSetting : ConfigurationElement
	{
		[ConfigurationProperty("CRMConnectionString")]
		public string CrmConnectionString
		{
			get
			{
				return (string)base["CRMConnectionString"];
			}
			set
			{
				base["CRMConnectionString"] = value;
			}
		}

		[ConfigurationProperty("ESPConnectionString")]
		public string EspConnectionString
		{
			get
			{
				return (string)base["ESPConnectionString"];
			}
			set
			{
				base["ESPConnectionString"] = value;
			}
		}

		[ConfigurationProperty("ESPParamConnectionString")]
		public string EspParamConnectionString
		{
			get
			{
				return (string)base["ESPParamConnectionString"];
			}
			set
			{
				base["ESPParamConnectionString"] = value;
			}
		}

		[ConfigurationProperty("resellerId", IsRequired=true, IsKey=true)]
		public string ResellerId
		{
			get
			{
				return (string)base["resellerId"];
			}
			set
			{
				base["resellerId"] = value;
			}
		}

		public ResellerConnectionSetting(string resellerId, string espAccount, string espParam, string crmConn)
		{
			if (resellerId == "" || int.Parse(resellerId) < 0)
			{
				throw new ArgumentOutOfRangeException("resellerId");
			}
			this.ResellerId = resellerId;
			this.EspConnectionString = espAccount;
			this.EspParamConnectionString = espParam;
			this.CrmConnectionString = crmConn;
		}

		public ResellerConnectionSetting(string resellerId, string espConn, string crmConn) : this(resellerId, espConn, "", crmConn)
		{
		}

		public ResellerConnectionSetting(string resellerId, string espConn) : this(resellerId, espConn, "")
		{
		}

		public ResellerConnectionSetting(string resellerId) : this(resellerId, "", "")
		{
		}

		public ResellerConnectionSetting() : this("0", "", "")
		{
		}

		protected override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
		{
			base.DeserializeElement(reader, serializeCollectionKey);
		}

		protected override bool IsModified()
		{
			return base.IsModified();
		}

		protected override bool SerializeElement(XmlWriter writer, bool serializeCollectionKey)
		{
			return base.SerializeElement(writer, serializeCollectionKey);
		}
	}
}