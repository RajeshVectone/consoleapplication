using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class HLRSubscriber
	{
		private const string SP_GET_DETAIL = "crm_get_subscriber_detail";

		private string _msisdn;

		private string _iccid;

		private string _actimsi;

		private string _masimsi;

		private int _substat;

		private string _strsubstat;

		private int _mnrf;

		private int _mcef;

		private DateTime _firstupd;

		private DateTime _firstauth;

		private DateTime _lastupd;

		private string _sitecode;

		private string _vlr;

		private int _odbout;

		private int _odbinc;

		public string CurrentVLR
		{
			get
			{
				if (this._vlr == null)
				{
					return string.Empty;
				}
				return this._vlr;
			}
		}

		public DateTime FirstAuthentic
		{
			get
			{
				return this._firstauth;
			}
		}

		public DateTime FirstUpdate
		{
			get
			{
				return this._firstupd;
			}
		}

		public string ICCID
		{
			get
			{
				if (this._iccid == null)
				{
					return string.Empty;
				}
				return this._iccid;
			}
		}

		public string IMSIActive
		{
			get
			{
				if (this._actimsi == null)
				{
					return string.Empty;
				}
				return this._actimsi;
			}
		}

		public string IMSIMaster
		{
			get
			{
				if (this._masimsi == null)
				{
					return string.Empty;
				}
				return this._masimsi;
			}
		}

		public DateTime LastUpdate
		{
			get
			{
				return this._lastupd;
			}
		}

		public int MCEF
		{
			get
			{
				return this._mcef;
			}
		}

		public int MNRF
		{
			get
			{
				return this._mnrf;
			}
		}

		public string MSISDN
		{
			get
			{
				if (this._msisdn == null)
				{
					return string.Empty;
				}
				return this._msisdn;
			}
		}

		public int ODBIncoming
		{
			get
			{
				return this._odbinc;
			}
		}

		public int ODBOutgoing
		{
			get
			{
				return this._odbout;
			}
		}

		public string Sitecode
		{
			get
			{
				if (this._sitecode == null)
				{
					return string.Empty;
				}
				return this._sitecode;
			}
		}

		public int SubscriberStatus
		{
			get
			{
				return this._substat;
			}
		}

		public string SubscriberStatusStr
		{
			get
			{
				if (this._strsubstat == null)
				{
					return string.Empty;
				}
				return this._strsubstat;
			}
		}

		public HLRSubscriber()
		{
		}

		public HLRSubscriber(string MSISDN, string ICCID, string IMSIActive, string IMSIMaster, int SubscriberStatus, string SubscriberStatusStr, int MNRF, int MCEF, DateTime FirstUpdate, DateTime FirstAuthentic, DateTime LastUpdate, string Sitecode, string CurrentVLR, int ODBOutgoing, int ODBIncoming)
		{
			this._msisdn = MSISDN;
			this._iccid = ICCID;
			this._actimsi = IMSIActive;
			this._masimsi = IMSIMaster;
			this._substat = SubscriberStatus;
			this._strsubstat = SubscriberStatusStr;
			this._mnrf = MNRF;
			this._mcef = MCEF;
			this._firstupd = FirstUpdate;
			this._firstauth = FirstAuthentic;
			this._lastupd = LastUpdate;
			this._sitecode = Sitecode;
			this._vlr = CurrentVLR;
			this._odbout = ODBOutgoing;
			this._odbinc = ODBIncoming;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<HLRSubscriber> listNew)
		{
			while (returnData.Read())
			{
				HLRSubscriber hLRSubscriber = new HLRSubscriber(GeneralConverter.ToString(returnData["msisdn"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToString(returnData["activeimsi"]), GeneralConverter.ToString(returnData["masterimsi"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToString(returnData["strstatus"]), GeneralConverter.ToInt32(returnData["mnrf"]), GeneralConverter.ToInt32(returnData["mcef"]), GeneralConverter.ToDateTime(returnData["firstupd"]), GeneralConverter.ToDateTime(returnData["firstauth"]), GeneralConverter.ToDateTime(returnData["lastupd"]), GeneralConverter.ToString(returnData["sitecode"]), GeneralConverter.ToString(returnData["vlr"]), GeneralConverter.ToInt32(returnData["odbout"]), GeneralConverter.ToInt32(returnData["odbin"]));
				listNew.Add(hLRSubscriber);
			}
		}

		public static HLRSubscriber GetDetail(string MSISDN)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetHLRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msisdn", SqlDbType.VarChar, 15, ParameterDirection.Input, MSISDN);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "crm_get_subscriber_detail");
			List<HLRSubscriber> hLRSubscribers = new List<HLRSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<HLRSubscriber>(sqlCommand, new GenerateListFromReader<HLRSubscriber>(HLRSubscriber.GenerateListFromReader<HLRSubscriber>), ref hLRSubscribers);
			if (hLRSubscribers.Count <= 0)
			{
				return null;
			}
			return hLRSubscribers[0];
		}
	}
}