using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class NPAdmRejectCode
	{
		private const string SP_GET_REJECT_LIST = "RejectCode_list";

		private string _rejectCode;

		private string _rejectText;

		public string RejectCode
		{
			get
			{
				if (this._rejectCode == null)
				{
					return string.Empty;
				}
				return this._rejectCode;
			}
			set
			{
				this._rejectCode = value;
			}
		}

		public string RejectText
		{
			get
			{
				if (this._rejectText == null)
				{
					return string.Empty;
				}
				return this._rejectText;
			}
			set
			{
				this._rejectText = value;
			}
		}

		public NPAdmRejectCode()
		{
		}

		public NPAdmRejectCode(string rejectCode, string rejectText)
		{
			this._rejectCode = rejectCode;
			this._rejectText = rejectText;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<NPAdmRejectCode> listRef)
		{
			while (returnData.Read())
			{
				NPAdmRejectCode nPAdmRejectCode = new NPAdmRejectCode(GeneralConverter.ToString(returnData["rejectCode"]), GeneralConverter.ToString(returnData["rejectText"]));
				listRef.Add(nPAdmRejectCode);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<NPAdmRejectCode> GetListRejectCode()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "RejectCode_list");
			List<NPAdmRejectCode> nPAdmRejectCodes = new List<NPAdmRejectCode>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmRejectCode>(sqlCommand, new GenerateListFromReader<NPAdmRejectCode>(NPAdmRejectCode.GenerateListFromReader<NPAdmRejectCode>), ref nPAdmRejectCodes);
			return nPAdmRejectCodes;
		}
	}
}