using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmRangeNumber
	{
		private const string SP_LIST_RANGE_NUMBER_BY_TELP = "RangeNumber_listByTelp";

		private const string SP_GET_RANGE_BY_RANGE_ID = "RangeNumber_getById";

		private const string SP_CHECK_RANGE_NUMBER_OVERLAP = "RangeNumber_checkOverlap";

		private const string SP_CHECK_RANGE_NUMBER_EXIST = "RangeNumber_checkExist";

		private const string SP_LIST_NP_RANGE_UPDATE = "RangeNumber_list";

		private int _rangeId;

		private string _currentNetworkOperator;

		private string _firstNumber;

		private string _lastNumber;

		private string _currentRangeHolder;

		private string _currentServiceOperator;

		private string _spc;

		private string _municipality;

		private string _chargingInfo;

		private string _routingInfo;

		private int _isOwnNumber;

		private string _lastUpdate;

		private string _status;

		public string ChargingInfo
		{
			get
			{
				return this._chargingInfo;
			}
			set
			{
				this._chargingInfo = value;
			}
		}

		public string CurrentNetworkOperator
		{
			get
			{
				return this._currentNetworkOperator;
			}
			set
			{
				this._currentNetworkOperator = value;
			}
		}

		public string CurrentRangeHolder
		{
			get
			{
				return this._currentRangeHolder;
			}
			set
			{
				this._currentRangeHolder = value;
			}
		}

		public string CurrentServiceOperator
		{
			get
			{
				return this._currentServiceOperator;
			}
			set
			{
				this._currentServiceOperator = value;
			}
		}

		public string FirstNumber
		{
			get
			{
				return this._firstNumber;
			}
			set
			{
				this._firstNumber = value;
			}
		}

		public int IsOwnNumber
		{
			get
			{
				return this._isOwnNumber;
			}
			set
			{
				this._isOwnNumber = value;
			}
		}

		public string LastNumber
		{
			get
			{
				return this._lastNumber;
			}
			set
			{
				this._lastNumber = value;
			}
		}

		public string LastUpdate
		{
			get
			{
				return this._lastUpdate;
			}
			set
			{
				this._lastUpdate = value;
			}
		}

		public string Municipality
		{
			get
			{
				return this._municipality;
			}
			set
			{
				this._municipality = value;
			}
		}

		public int RangeId
		{
			get
			{
				return this._rangeId;
			}
		}

		public string RoutingInfo
		{
			get
			{
				return this._routingInfo;
			}
			set
			{
				this._routingInfo = value;
			}
		}

		public string Spc
		{
			get
			{
				return this._spc;
			}
			set
			{
				this._spc = value;
			}
		}

		public string Status
		{
			get
			{
				return this._status;
			}
			set
			{
				this._status = value;
			}
		}

		public NPAdmRangeNumber(int rangeId, string currNetworkOperator, string firstNumber, string lastNumber, string currentRangeHolder, string currentServiceOperator, string spc, string municipality, string chargingInfo, string routingInfo, int isOwnNumber, string lastUpdate, string status)
		{
			this._rangeId = rangeId;
			this._currentNetworkOperator = currNetworkOperator;
			this._firstNumber = firstNumber;
			this._lastNumber = lastNumber;
			this._currentRangeHolder = currentRangeHolder;
			this._currentServiceOperator = currentServiceOperator;
			this._spc = spc;
			this._municipality = municipality;
			this._chargingInfo = chargingInfo;
			this._routingInfo = routingInfo;
			this._isOwnNumber = isOwnNumber;
			this._lastUpdate = lastUpdate;
			this._status = status;
		}

		private static void GenerateRangeNumberListByTelpFromReader<T>(IDataReader returnData, ref List<NPAdmRangeNumber> listNew)
		{
			while (returnData.Read())
			{
				NPAdmRangeNumber nPAdmRangeNumber = new NPAdmRangeNumber(GeneralConverter.ToInt32(returnData["rangeId"]), GeneralConverter.ToString(returnData["currentNetworkOperator"]), GeneralConverter.ToString(returnData["firstNumber"]), GeneralConverter.ToString(returnData["lastNumber"]), GeneralConverter.ToString(returnData["currentRangeHolder"]), GeneralConverter.ToString(returnData["currentServiceOperator"]), GeneralConverter.ToString(returnData["spc"]), GeneralConverter.ToString(returnData["municipality"]), GeneralConverter.ToString(returnData["chargingInfo"]), GeneralConverter.ToString(returnData["routingInfo"]), GeneralConverter.ToInt32(returnData["isOwnNumber"]), GeneralConverter.ToString(returnData["lastUpdate"]), GeneralConverter.ToString(returnData["status"]));
				listNew.Add(nPAdmRangeNumber);
			}
		}

		public static NPAdmRangeNumber GetRangeNumberById(int rangeId)
		{
			if (rangeId < 0)
			{
				throw new ArgumentOutOfRangeException("rangeId");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@rangeId", SqlDbType.Int, 0, ParameterDirection.Input, rangeId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "RangeNumber_getById");
			List<NPAdmRangeNumber> nPAdmRangeNumbers = new List<NPAdmRangeNumber>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmRangeNumber>(sqlCommand, new GenerateListFromReader<NPAdmRangeNumber>(NPAdmRangeNumber.GenerateRangeNumberListByTelpFromReader<NPAdmRangeNumber>), ref nPAdmRangeNumbers);
			if (nPAdmRangeNumbers.Count <= 0)
			{
				return null;
			}
			return nPAdmRangeNumbers[0];
		}

		public static bool IsRangeNumberExisted(string firstNumber, string lastNumber, string spc, string municipality, string routingInfo, string chargingInfo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstNumber", SqlDbType.VarChar, 8, ParameterDirection.Input, firstNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastNumber", SqlDbType.VarChar, 8, ParameterDirection.Input, lastNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@spc", SqlDbType.VarChar, 6, ParameterDirection.Input, spc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@municipality", SqlDbType.Char, 3, ParameterDirection.Input, municipality);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@routingInfo", SqlDbType.VarChar, 8, ParameterDirection.Input, routingInfo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@chargingInfo", SqlDbType.VarChar, 8, ParameterDirection.Input, chargingInfo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "RangeNumber_checkExist");
			DataSet dataSet = sQLDataAccessLayer.ExecuteQuery(sqlCommand);
			if (dataSet != null && dataSet.Tables.Count != 0 && dataSet.Tables[0].Rows.Count != 0)
			{
				return true;
			}
			return false;
		}

		public static bool IsRangeNumberOverlapped(string firstNumber, string lastNumber)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstNumber", SqlDbType.VarChar, 8, ParameterDirection.Input, firstNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastNumber", SqlDbType.VarChar, 8, ParameterDirection.Input, lastNumber);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "RangeNumber_checkOverlap");
			DataSet dataSet = sQLDataAccessLayer.ExecuteQuery(sqlCommand);
			if (dataSet != null && dataSet.Tables.Count != 0 && dataSet.Tables[0].Rows.Count != 0)
			{
				return true;
			}
			return false;
		}

		public static List<NPAdmRangeNumber> ListNPRangeUpdate()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "RangeNumber_list");
			List<NPAdmRangeNumber> nPAdmRangeNumbers = new List<NPAdmRangeNumber>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmRangeNumber>(sqlCommand, new GenerateListFromReader<NPAdmRangeNumber>(NPAdmRangeNumber.GenerateRangeNumberListByTelpFromReader<NPAdmRangeNumber>), ref nPAdmRangeNumbers);
			return nPAdmRangeNumbers;
		}

		public static List<NPAdmRangeNumber> ListRangeNumberByTelpNumber(string telpNumber)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelpNumber", SqlDbType.VarChar, 8, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "RangeNumber_listByTelp");
			List<NPAdmRangeNumber> nPAdmRangeNumbers = new List<NPAdmRangeNumber>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmRangeNumber>(sqlCommand, new GenerateListFromReader<NPAdmRangeNumber>(NPAdmRangeNumber.GenerateRangeNumberListByTelpFromReader<NPAdmRangeNumber>), ref nPAdmRangeNumbers);
			return nPAdmRangeNumbers;
		}
	}
}