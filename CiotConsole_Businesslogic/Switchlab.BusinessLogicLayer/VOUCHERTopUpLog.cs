using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class VOUCHERTopUpLog
	{
		private const string SP_GET_ALL_REPORT = "topup_get_all";

		private const string SP_GET_REPORT_BY_MOBILE_NO = "topup_get_by_mobileno";

		private int _itopup_id;

		private string _smobileno;

		private DateTime _dtcreatedate;

		private int _itopuptypeid;

		private string _stopuptypename;

		private int _itransstatusid;

		private string _stransstatusname;

		private double _damount;

		private double _dprevbal;

		private double _dafterbal;

		private int _ivoucher_id;

		private string _ivoucher_no;

		public double AfterBalance
		{
			get
			{
				return this._dafterbal;
			}
			set
			{
				this._dafterbal = value;
			}
		}

		public double Amount
		{
			get
			{
				return this._damount;
			}
			set
			{
				this._damount = value;
			}
		}

		public DateTime CreateDate
		{
			get
			{
				return this._dtcreatedate;
			}
			set
			{
				this._dtcreatedate = value;
			}
		}

		public string MobileNo
		{
			get
			{
				if (this._smobileno == null)
				{
					return string.Empty;
				}
				return this._smobileno;
			}
			set
			{
				this._smobileno = value;
			}
		}

		public double PreviousBalance
		{
			get
			{
				return this._dprevbal;
			}
			set
			{
				this._dprevbal = value;
			}
		}

		public int TopUpID
		{
			get
			{
				return this._itopup_id;
			}
			set
			{
				this._itopup_id = value;
			}
		}

		public int TopUpTypeID
		{
			get
			{
				return this._itopuptypeid;
			}
			set
			{
				this._itopuptypeid = value;
			}
		}

		public string TopUpTypeName
		{
			get
			{
				if (this._stopuptypename == null)
				{
					return string.Empty;
				}
				return this._stopuptypename;
			}
			set
			{
				this._stopuptypename = value;
			}
		}

		public int TransStatusID
		{
			get
			{
				return this._itransstatusid;
			}
			set
			{
				this._itransstatusid = value;
			}
		}

		public string TransStatusName
		{
			get
			{
				if (this._stransstatusname == null)
				{
					return string.Empty;
				}
				return this._stransstatusname;
			}
			set
			{
				this._stransstatusname = value;
			}
		}

		public int VoucherID
		{
			get
			{
				return this._ivoucher_id;
			}
			set
			{
				this._ivoucher_id = value;
			}
		}

		public string VoucherNo
		{
			get
			{
				if (this._ivoucher_no == null)
				{
					return string.Empty;
				}
				return this._ivoucher_no;
			}
			set
			{
				this._ivoucher_no = value;
			}
		}

		public VOUCHERTopUpLog()
		{
		}

		public VOUCHERTopUpLog(int iTopupID, string sMobileNo, DateTime dtCreateDate, int iTopUpTypeID, string sTopupTypeName, int iTransStatusID, string sTranssSatusName, double dAmount, double dPrevbal, double dAfterbal, int iVoucherID, string iVoucherNo)
		{
			this._itopup_id = iTopupID;
			this._smobileno = sMobileNo;
			this._dtcreatedate = dtCreateDate;
			this._itopuptypeid = iTopUpTypeID;
			this._stopuptypename = sTopupTypeName;
			this._itransstatusid = iTransStatusID;
			this._stransstatusname = sTranssSatusName;
			this._damount = dAmount;
			this._dprevbal = dPrevbal;
			this._dafterbal = dAfterbal;
			this._ivoucher_id = iVoucherID;
			this._ivoucher_no = iVoucherNo;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListVoucherFromReader<T>(IDataReader returnData, ref List<VOUCHERTopUpLog> theList)
		{
			while (returnData.Read())
			{
				VOUCHERTopUpLog vOUCHERTopUpLog = new VOUCHERTopUpLog(GeneralConverter.ToInt32(returnData["topup_id"]), GeneralConverter.ToString(returnData["mobileNo"]), GeneralConverter.ToDateTime(returnData["createDate"]), GeneralConverter.ToInt32(returnData["topupTypeID"]), GeneralConverter.ToString(returnData["topupTypeName"]), GeneralConverter.ToInt32(returnData["transStatusID"]), GeneralConverter.ToString(returnData["transStatusName"]), GeneralConverter.ToDouble(returnData["amount"]), GeneralConverter.ToDouble(returnData["prevbal"]), GeneralConverter.ToDouble(returnData["afterbal"]), GeneralConverter.ToInt32(returnData["voucher_id"]), GeneralConverter.ToString(returnData["voucher_no"]));
				theList.Add(vOUCHERTopUpLog);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<VOUCHERTopUpLog> GetAllReport()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "topup_get_all");
			List<VOUCHERTopUpLog> vOUCHERTopUpLogs = new List<VOUCHERTopUpLog>();
			sQLDataAccessLayer.ExecuteReaderCmd<VOUCHERTopUpLog>(sqlCommand, new GenerateListFromReader<VOUCHERTopUpLog>(VOUCHERTopUpLog.GenerateListVoucherFromReader<VOUCHERTopUpLog>), ref vOUCHERTopUpLogs);
			if (vOUCHERTopUpLogs != null && vOUCHERTopUpLogs.Count > 0)
			{
				return vOUCHERTopUpLogs;
			}
			return null;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<VOUCHERTopUpLog> GetReportByMobileNo(string sMobileNo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetVoucherConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileNo", SqlDbType.VarChar, 16, ParameterDirection.Input, sMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "topup_get_by_mobileno");
			List<VOUCHERTopUpLog> vOUCHERTopUpLogs = new List<VOUCHERTopUpLog>();
			sQLDataAccessLayer.ExecuteReaderCmd<VOUCHERTopUpLog>(sqlCommand, new GenerateListFromReader<VOUCHERTopUpLog>(VOUCHERTopUpLog.GenerateListVoucherFromReader<VOUCHERTopUpLog>), ref vOUCHERTopUpLogs);
			if (vOUCHERTopUpLogs != null && vOUCHERTopUpLogs.Count > 0)
			{
				return vOUCHERTopUpLogs;
			}
			return null;
		}
	}
}