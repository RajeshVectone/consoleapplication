using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class ESPBatch
	{
		private const string SP_CREATEBATCH = "rp1_createbatch";

		private string _telcocode;

		private string _custcode;

		private int _batchcode;

		private int _faceunit;

		private int _billmode;

		private DateTime _startdate;

		private DateTime _expdate;

		private int _playblc;

		private string _currcode;

		private string _danb;

		private int _cardid;

		public ESPBatch(string telcoCode, string custCode, int batchCode, int faceUnit, int billMode, DateTime startDate, DateTime expDate, int playBlc, string currCode, string danB, int cardId)
		{
			if (string.IsNullOrEmpty(telcoCode))
			{
				throw new ArgumentException("telcoCode");
			}
			if (string.IsNullOrEmpty(custCode))
			{
				throw new ArgumentException("custCode");
			}
			if (batchCode < 0 || batchCode > 9999)
			{
				throw new ArgumentOutOfRangeException("batchCode");
			}
			if (cardId < 0)
			{
				throw new ArgumentOutOfRangeException("cardId");
			}
			this._telcocode = telcoCode;
			this._custcode = custCode;
			this._batchcode = batchCode;
			this._faceunit = faceUnit;
			this._billmode = billMode;
			this._startdate = startDate;
			this._expdate = expDate;
			this._playblc = playBlc;
			this._currcode = currCode;
			this._danb = danB;
			this._cardid = cardId;
		}

		public ESPBatch(string telcoCode, string custCode, int batchCode, string currency, int productFamilyId) : this(telcoCode, custCode, batchCode, 1, 2, DateTime.Now, DateTime.Now.AddYears(10), 0, currency, "", productFamilyId)
		{
		}

		public bool CreateBatch(int resellerId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 0, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.Char, 3, ParameterDirection.Input, this._telcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.Char, 8, ParameterDirection.Input, this._custcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, this._batchcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@faceunit", SqlDbType.Int, 0, ParameterDirection.Input, this._faceunit);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@billmode", SqlDbType.Int, 0, ParameterDirection.Input, this._billmode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@startdate", SqlDbType.DateTime, 0, ParameterDirection.Input, this._startdate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@expdate", SqlDbType.DateTime, 0, ParameterDirection.Input, this._expdate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@playblc", SqlDbType.Int, 0, ParameterDirection.Input, this._playblc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, this._currcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@danb", SqlDbType.VarChar, 50, ParameterDirection.Input, this._danb);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardid", SqlDbType.Int, 0, ParameterDirection.Input, this._cardid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_createbatch");
			sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			return (int)sqlCommand.Parameters["@ReturnValue"].Value == 0;
		}
	}
}