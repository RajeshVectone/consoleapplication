using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMPage
	{
		private const string SP_LISTALL = "pages_listall";

		private const string SP_LISTSUBMENU = "pages_listchild";

		private const string SP_UP_PAGE = "up_page";

		private const string SP_DOWN_PAGE = "down_page";

		private const string SP_ListMenu = "pages_listbygroupparent";

		private const string SP_DETAIL = "pages_detail";

		private const string SP_DETAIL_BY_TITLE = "pages_detail_by_title";

		private int _iPageID;

		private string _sTitle;

		private string _sURL;

		private int _iParentMenu;

		private string _sDisplayName;

		private int _iSortOrder;

		public string DisplayName
		{
			get
			{
				if (this._sDisplayName == null)
				{
					return string.Empty;
				}
				return this._sDisplayName;
			}
		}

		public int PageID
		{
			get
			{
				return this._iPageID;
			}
		}

		public int ParentMenu
		{
			get
			{
				return this._iParentMenu;
			}
		}

		public int SortOrder
		{
			get
			{
				return this._iSortOrder;
			}
		}

		public string Title
		{
			get
			{
				if (this._sTitle == null)
				{
					return string.Empty;
				}
				return this._sTitle;
			}
		}

		public string URL
		{
			get
			{
				if (this._sURL == null)
				{
					return string.Empty;
				}
				return this._sURL;
			}
		}

		public CRMPage()
		{
		}

		public CRMPage(int iPageID, int iParentMenu, string sTitle, string sURL, string sDisplayName, int iSortOrder)
		{
			this._iPageID = iPageID;
			this._iParentMenu = iParentMenu;
			this._sTitle = sTitle;
			this._sURL = sURL;
			this._sDisplayName = sDisplayName;
			this._iSortOrder = iSortOrder;
		}

		public static bool DownPage(string iPageId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pageId", SqlDbType.Int, 0, ParameterDirection.Input, int.Parse(iPageId.ToString()));
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "down_page");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		private static void GeneratePageListFromReader<T>(IDataReader returnData, ref List<CRMPage> listNew)
		{
			while (returnData.Read())
			{
				CRMPage cRMPage = new CRMPage(GeneralConverter.ToInt32(returnData["page_id"]), GeneralConverter.ToInt32(returnData["parentmenu"]), Convert.ToString(returnData["title"]), Convert.ToString(returnData["url"]), Convert.ToString(returnData["displayname"]), GeneralConverter.ToInt32(returnData["sortorder"]));
				listNew.Add(cRMPage);
			}
		}

		public static CRMPage GetByID(int PageID)
		{
			if (PageID < 0)
			{
				throw new ArgumentOutOfRangeException("PageID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@page_id", SqlDbType.Int, 0, ParameterDirection.Input, PageID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_detail");
			List<CRMPage> cRMPages = new List<CRMPage>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPage>(sqlCommand, new GenerateListFromReader<CRMPage>(CRMPage.GeneratePageListFromReader<CRMPage>), ref cRMPages);
			if (cRMPages.Count <= 0)
			{
				return null;
			}
			return cRMPages[0];
		}

		public static CRMPage GetByTitle(string title)
		{
			if (title.Trim() == string.Empty)
			{
				throw new ArgumentOutOfRangeException("title");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@page_title", SqlDbType.VarChar, 128, ParameterDirection.Input, title);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_detail_by_title");
			List<CRMPage> cRMPages = new List<CRMPage>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPage>(sqlCommand, new GenerateListFromReader<CRMPage>(CRMPage.GeneratePageListFromReader<CRMPage>), ref cRMPages);
			if (cRMPages.Count <= 0)
			{
				return null;
			}
			return cRMPages[0];
		}

		public static List<CRMPage> ListMenu(int GroupID, int ModuleID, int ParentID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, GroupID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@parentid", SqlDbType.Int, 0, ParameterDirection.Input, ParentID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@moduleid", SqlDbType.Int, 0, ParameterDirection.Input, ModuleID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_listbygroupparent");
			List<CRMPage> cRMPages = new List<CRMPage>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPage>(sqlCommand, new GenerateListFromReader<CRMPage>(CRMPage.GeneratePageListFromReader<CRMPage>), ref cRMPages);
			return cRMPages;
		}

		public static List<CRMPage> ListSubMenu(int iParentID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@parentid", SqlDbType.Int, 0, ParameterDirection.Input, iParentID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "pages_listchild");
			List<CRMPage> cRMPages = new List<CRMPage>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPage>(sqlCommand, new GenerateListFromReader<CRMPage>(CRMPage.GeneratePageListFromReader<CRMPage>), ref cRMPages);
			return cRMPages;
		}

		public static bool UpPage(string iPageId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pageId", SqlDbType.Int, 0, ParameterDirection.Input, int.Parse(iPageId.ToString()));
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "up_page");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}
	}
}