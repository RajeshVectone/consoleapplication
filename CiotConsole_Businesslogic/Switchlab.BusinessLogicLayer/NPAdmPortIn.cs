using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmPortIn
	{
		private const string SP_LIST_PORT_TO_PORT_IN = "PortingIn_listPortToPortIn";

		private const string SP_GET_PORT_BY_PORTING_ID = "PortingIn_getByPortingId";

		private const string SP_GET_LAST_COUNTER_OF_ORIGINATING_NUMBER = "Counter_getNextCounterByCounterId";

		private const string SP_SET_LAST_COUNTER_OF_ORIGINATING_NUMBER = "Counter_updateCounterByCounterId";

		private const string SP_UPDATE_PORTING_IN_STATUS = "PortingIn_updatePortingInStatus";

		private const string SP_LIST_PORT_TO_ACTIVATE = "PortingIn_listPortToActivate";

		private int _portingID;

		private string _telpNumber;

		private string _requestedExecutionDate;

		private string _currentServiceOperator;

		private string _ICCID;

		private int _status;

		private string _statusDesc;

		private string _customerID;

		private string _customerFirstName;

		private string _customerLastName;

		public string CurrentServiceOperator
		{
			get
			{
				return this._currentServiceOperator;
			}
			set
			{
				this._currentServiceOperator = value;
			}
		}

		public string CustomerFirstName
		{
			get
			{
				return this._customerFirstName;
			}
			set
			{
				this._customerFirstName = value;
			}
		}

		public string CustomerID
		{
			get
			{
				return this._customerID;
			}
		}

		public string CustomerLastName
		{
			get
			{
				return this._customerLastName;
			}
			set
			{
				this._customerLastName = value;
			}
		}

		public string ICCID
		{
			get
			{
				return this._ICCID;
			}
			set
			{
				this._ICCID = value;
			}
		}

		public int PortingID
		{
			get
			{
				return this._portingID;
			}
		}

		public string RequestedExecutionDate
		{
			get
			{
				return this._requestedExecutionDate;
			}
			set
			{
				this._requestedExecutionDate = value;
			}
		}

		public int Status
		{
			get
			{
				return this._status;
			}
			set
			{
				this._status = value;
			}
		}

		public string StatusDesc
		{
			get
			{
				return this._statusDesc;
			}
			set
			{
				this._statusDesc = value;
			}
		}

		public string TelpNumber
		{
			get
			{
				return this._telpNumber;
			}
			set
			{
				this._telpNumber = value;
			}
		}

		public NPAdmPortIn(int portingId, string telpNumber, string reqExecutionDate, string currentServiceOperator, string custId, string iccid, int status)
		{
			this._portingID = portingId;
			this._telpNumber = telpNumber;
			this._requestedExecutionDate = reqExecutionDate;
			this._currentServiceOperator = currentServiceOperator;
			this._customerID = custId;
			this._ICCID = iccid;
			this._status = status;
		}

		public NPAdmPortIn(int portingId, string telpNumber, int status, string statusDesc, string custId, string custFirstName, string custLastName)
		{
			this._portingID = portingId;
			this._telpNumber = telpNumber;
			this._status = status;
			this._statusDesc = statusDesc;
			this._customerID = custId;
			this._customerFirstName = custFirstName;
			this._customerLastName = custLastName;
		}

		private static void GeneratePortListByPortingIdFromReader<T>(IDataReader returnData, ref List<NPAdmPortIn> listNew)
		{
			while (returnData.Read())
			{
				NPAdmPortIn nPAdmPortIn = new NPAdmPortIn(GeneralConverter.ToInt32(returnData["portingId"]), GeneralConverter.ToString(returnData["telpNumber"]), GeneralConverter.ToString(returnData["requestedExecutionDate"]), GeneralConverter.ToString(returnData["donorOperator"]), GeneralConverter.ToString(returnData["customerID"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToInt32(returnData["statusId"]));
				listNew.Add(nPAdmPortIn);
			}
		}

		private static void GeneratePortListToActivateFromReader<T>(IDataReader returnData, ref List<NPAdmPortIn> listNew)
		{
			while (returnData.Read())
			{
				NPAdmPortIn nPAdmPortIn = new NPAdmPortIn(GeneralConverter.ToInt32(returnData["portingId"]), GeneralConverter.ToString(returnData["telpNumber"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["customerId"]), GeneralConverter.ToString(returnData["customerFirstName"]), GeneralConverter.ToString(returnData["customerlastName"]));
				listNew.Add(nPAdmPortIn);
			}
		}

		private static void GeneratePortListToPortInFromReader<T>(IDataReader returnData, ref List<NPAdmPortIn> listNew)
		{
			while (returnData.Read())
			{
				NPAdmPortIn nPAdmPortIn = new NPAdmPortIn(GeneralConverter.ToInt32(returnData["portingId"]), GeneralConverter.ToString(returnData["telpNumber"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["customerId"]), GeneralConverter.ToString(returnData["customerFirstName"]), GeneralConverter.ToString(returnData["customerlastName"]));
				listNew.Add(nPAdmPortIn);
			}
		}

		public static int GetLastCounterOfOriginatingNumber()
		{
			int value = -1;
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@counterId", SqlDbType.Int, 0, ParameterDirection.Input, 1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@counter", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Counter_getNextCounterByCounterId");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value && sqlCommand.Parameters["@counter"] != null)
			{
				value = (int)sqlCommand.Parameters["@counter"].Value;
			}
			return value;
		}

		public static NPAdmPortIn GetPortInByPortingId(int portingId)
		{
			if (portingId < 0)
			{
				throw new ArgumentOutOfRangeException("portingId");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingId", SqlDbType.Int, 0, ParameterDirection.Input, portingId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "PortingIn_getByPortingId");
			List<NPAdmPortIn> nPAdmPortIns = new List<NPAdmPortIn>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmPortIn>(sqlCommand, new GenerateListFromReader<NPAdmPortIn>(NPAdmPortIn.GeneratePortListByPortingIdFromReader<NPAdmPortIn>), ref nPAdmPortIns);
			if (nPAdmPortIns.Count <= 0)
			{
				return null;
			}
			return nPAdmPortIns[0];
		}

		public static List<NPAdmPortIn> ListPortToActivate()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "PortingIn_listPortToActivate");
			List<NPAdmPortIn> nPAdmPortIns = new List<NPAdmPortIn>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmPortIn>(sqlCommand, new GenerateListFromReader<NPAdmPortIn>(NPAdmPortIn.GeneratePortListToActivateFromReader<NPAdmContract>), ref nPAdmPortIns);
			return nPAdmPortIns;
		}

		public static List<NPAdmPortIn> ListPortToPortIn()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "PortingIn_listPortToPortIn");
			List<NPAdmPortIn> nPAdmPortIns = new List<NPAdmPortIn>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmPortIn>(sqlCommand, new GenerateListFromReader<NPAdmPortIn>(NPAdmPortIn.GeneratePortListToPortInFromReader<NPAdmContract>), ref nPAdmPortIns);
			return nPAdmPortIns;
		}

		public static void SetLastCounterOfOriginatingNumber()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@counterId", SqlDbType.Int, 0, ParameterDirection.Input, 1);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Counter_updateCounterByCounterId");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
		}

		public static bool UpdatePortingInStatus(int portingId, int status)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingId", SqlDbType.Int, 0, ParameterDirection.Input, portingId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, status);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "PortingIn_updatePortingInStatus");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}
	}
}