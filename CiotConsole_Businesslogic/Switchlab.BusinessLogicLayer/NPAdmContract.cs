using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmContract
	{
		private const string SP_GET_LAST_CUSTOMER_ID = "Customer_getLastCustomerId";

		private const string SP_CREATE_CONTRACT = "Contract_create";

		private const string SP_LIST_CONTRACT_TO_AUTHORIZE = "Contract_listToAuthorize";

		private const string SP_LIST_CONTRACT_BY_PORTING_ID = "Contract_listByPortingId";

		private const string SP_UPDATE_PORTING_STATUS = "Contract_updatePortingStatus";

		private int _portingID;

		private string _telpNumber;

		private string _donorOperator;

		private string _writtenTermination;

		private string _ICCID;

		private string _requestedExecutionDate;

		private int _status;

		private string _statusDesc;

		private int _prepaid;

		private string _customerID;

		private string _customerFirstName;

		private string _customerLastName;

		private string _customerPostalCode;

		private string _customerLocationName;

		private string _customerStreetName;

		private string _customerCity;

		private string _customerFloor;

		private string _customerStreetNumber;

		private string _customerHouseName;

		private string _customerStairCase;

		private string _customerRightLeftDoor;

		public string CustomerCity
		{
			get
			{
				return this._customerCity;
			}
			set
			{
				this._customerCity = value;
			}
		}

		public string CustomerFirstName
		{
			get
			{
				return this._customerFirstName;
			}
			set
			{
				this._customerFirstName = value;
			}
		}

		public string CustomerFloor
		{
			get
			{
				return this._customerFloor;
			}
			set
			{
				this._customerFloor = value;
			}
		}

		public string CustomerHouseName
		{
			get
			{
				return this._customerHouseName;
			}
			set
			{
				this._customerHouseName = value;
			}
		}

		public string CustomerID
		{
			get
			{
				return this._customerID;
			}
		}

		public string CustomerLastName
		{
			get
			{
				return this._customerLastName;
			}
			set
			{
				this._customerLastName = value;
			}
		}

		public string CustomerLocationName
		{
			get
			{
				return this._customerLocationName;
			}
			set
			{
				this._customerLocationName = value;
			}
		}

		public string CustomerPostalCode
		{
			get
			{
				return this._customerPostalCode;
			}
			set
			{
				this._customerPostalCode = value;
			}
		}

		public string CustomerRightLeftDoor
		{
			get
			{
				return this._customerRightLeftDoor;
			}
			set
			{
				this._customerRightLeftDoor = value;
			}
		}

		public string CustomerStairCase
		{
			get
			{
				return this._customerStairCase;
			}
			set
			{
				this._customerStairCase = value;
			}
		}

		public string CustomerStreetName
		{
			get
			{
				return this._customerStreetName;
			}
			set
			{
				this._customerStreetName = value;
			}
		}

		public string CustomerStreetNumber
		{
			get
			{
				return this._customerStreetNumber;
			}
			set
			{
				this._customerStreetNumber = value;
			}
		}

		public string DonorOperator
		{
			get
			{
				return this._donorOperator;
			}
			set
			{
				this._donorOperator = value;
			}
		}

		public string ICCID
		{
			get
			{
				return this._ICCID;
			}
			set
			{
				this._ICCID = value;
			}
		}

		public int PortingID
		{
			get
			{
				return this._portingID;
			}
		}

		public int Prepaid
		{
			get
			{
				return this._prepaid;
			}
			set
			{
				this._prepaid = value;
			}
		}

		public string RequestedExecutionDate
		{
			get
			{
				return this._requestedExecutionDate;
			}
			set
			{
				this._requestedExecutionDate = value;
			}
		}

		public int Status
		{
			get
			{
				return this._status;
			}
			set
			{
				this._status = value;
			}
		}

		public string StatusDesc
		{
			get
			{
				return this._statusDesc;
			}
			set
			{
				this._statusDesc = value;
			}
		}

		public string TelpNumber
		{
			get
			{
				return this._telpNumber;
			}
			set
			{
				this._telpNumber = value;
			}
		}

		public string WrittenTermination
		{
			get
			{
				return this._writtenTermination;
			}
			set
			{
				this._writtenTermination = value;
			}
		}

		public NPAdmContract(int portingId, string telpNumber, string donorOperator, string writtenTermination, string iccid, string reqExecutionDate, int status, string statusDesc, int prepaid, string custId, string custFirstName, string custLastName, string custPostalCode, string custLocationName, string custStreetName, string custCity, string custFloor, string custStreetNumber, string custHouseName, string custStairCase, string custRightLeftDoor)
		{
			this._portingID = portingId;
			this._telpNumber = telpNumber;
			this._donorOperator = donorOperator;
			this._writtenTermination = writtenTermination;
			this._ICCID = iccid;
			this._requestedExecutionDate = reqExecutionDate;
			this._status = status;
			this._statusDesc = statusDesc;
			this._prepaid = prepaid;
			this._customerID = custId;
			this._customerFirstName = custFirstName;
			this._customerLastName = custLastName;
			this._customerPostalCode = custPostalCode;
			this._customerLocationName = custLocationName;
			this._customerStreetName = custStreetName;
			this._customerCity = custCity;
			this._customerFloor = custFloor;
			this._customerStreetNumber = custStreetNumber;
			this._customerHouseName = custHouseName;
			this._customerStairCase = custStairCase;
			this._customerRightLeftDoor = custRightLeftDoor;
		}

		public NPAdmContract(int portingId, string telpNumber, string statusDesc, string custFirstName, string custLastName)
		{
			this._portingID = portingId;
			this._telpNumber = telpNumber;
			this._statusDesc = statusDesc;
			this._customerFirstName = custFirstName;
			this._customerLastName = custLastName;
		}

		public static bool CreateContract(string customerId, string portedInTelpNumber, string tempTelpNumber, int prepaid, string donorOperator, string writtenTermination, string iccid, string requestExecutionDate, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portedInTelpNumber", SqlDbType.VarChar, 17, ParameterDirection.Input, portedInTelpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@tempTelpNumber", SqlDbType.VarChar, 17, ParameterDirection.Input, tempTelpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@prepaid", SqlDbType.Int, 0, ParameterDirection.Input, prepaid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@donorOperator", SqlDbType.VarChar, 32, ParameterDirection.Input, donorOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@writtenTermination", SqlDbType.VarChar, 3000, ParameterDirection.Input, writtenTermination);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, iccid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@reqExecutionDate", SqlDbType.VarChar, 10, ParameterDirection.Input, requestExecutionDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, NPAdmPortingStatus.Status.AuthorizedContract);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custId", SqlDbType.VarChar, 60, ParameterDirection.Input, customerId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Contract_create");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		private static void GenerateContractListByPortingIdFromReader<T>(IDataReader returnData, ref List<NPAdmContract> listNew)
		{
			while (returnData.Read())
			{
				NPAdmContract nPAdmContract = new NPAdmContract(GeneralConverter.ToInt32(returnData["portingId"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToString(returnData["donorOperator"]), GeneralConverter.ToString(returnData["writtenTermination"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToString(returnData["requestedExecutionDate"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToInt32(returnData["prepaid"]), GeneralConverter.ToString(returnData["customerId"]), GeneralConverter.ToString(returnData["customerFirstName"]), GeneralConverter.ToString(returnData["customerLastName"]), GeneralConverter.ToString(returnData["customerPostalCode"]), GeneralConverter.ToString(returnData["customerLocationName"]), GeneralConverter.ToString(returnData["customerStreetName"]), GeneralConverter.ToString(returnData["customerCity"]), GeneralConverter.ToString(returnData["customerFloor"]), GeneralConverter.ToString(returnData["customerStreetNumber"]), GeneralConverter.ToString(returnData["customerHouseName"]), GeneralConverter.ToString(returnData["customerStairCase"]), GeneralConverter.ToString(returnData["customerRightLeftDoor"]));
				listNew.Add(nPAdmContract);
			}
		}

		private static void GenerateContractListToAuthorizeFromReader<T>(IDataReader returnData, ref List<NPAdmContract> listNew)
		{
			while (returnData.Read())
			{
				NPAdmContract nPAdmContract = new NPAdmContract(GeneralConverter.ToInt32(returnData["portingId"]), GeneralConverter.ToString(returnData["telpNumber"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["firstName"]), GeneralConverter.ToString(returnData["lastName"]));
				listNew.Add(nPAdmContract);
			}
		}

		public static NPAdmContract GetContractByPortingId(int portingId)
		{
			if (portingId < 0)
			{
				throw new ArgumentOutOfRangeException("portingId");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingId", SqlDbType.Int, 0, ParameterDirection.Input, portingId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Contract_listByPortingId");
			List<NPAdmContract> nPAdmContracts = new List<NPAdmContract>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmContract>(sqlCommand, new GenerateListFromReader<NPAdmContract>(NPAdmContract.GenerateContractListByPortingIdFromReader<NPAdmContract>), ref nPAdmContracts);
			if (nPAdmContracts.Count <= 0)
			{
				return null;
			}
			return nPAdmContracts[0];
		}

		private static string GetLastCustomerID()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Customer_getLastCustomerId");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == null)
			{
				return string.Empty;
			}
			return obj.ToString();
		}

		public static string GetNewCustomerId()
		{
			string str = "P000000001";
			string lastCustomerID = NPAdmContract.GetLastCustomerID();
			if (lastCustomerID == string.Empty)
			{
				return str;
			}
			string str1 = lastCustomerID.Substring(1);
			int num = int.Parse(str1) + 1;
			str = string.Concat("00000000", num.ToString());
			return string.Concat(lastCustomerID.Substring(0, 1), str.Substring(str.Length - 9));
		}

		public static List<NPAdmContract> ListContractToAuthorize()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Contract_listToAuthorize");
			List<NPAdmContract> nPAdmContracts = new List<NPAdmContract>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmContract>(sqlCommand, new GenerateListFromReader<NPAdmContract>(NPAdmContract.GenerateContractListToAuthorizeFromReader<NPAdmContract>), ref nPAdmContracts);
			return nPAdmContracts;
		}

		public static bool UpdatePortingStatus(int portingId, int status, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingId", SqlDbType.Int, 0, ParameterDirection.Input, portingId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, status);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Contract_updatePortingStatus");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}
	}
}