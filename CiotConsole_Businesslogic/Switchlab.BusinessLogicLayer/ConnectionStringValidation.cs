using System;
using System.Collections;
using System.Configuration;

namespace Switchlab.BusinessLogicLayer
{
	public class ConnectionStringValidation : ConfigurationValidatorBase
	{
		public ConnectionStringValidation()
		{
		}

		public override bool CanValidate(Type type)
		{
			return type == typeof(ConnectionStringSettingsCollection);
		}

		public override void Validate(object value)
		{
			ConnectionStringSettingsCollection connectionStringSettingsCollection = value as ConnectionStringSettingsCollection;
			if (connectionStringSettingsCollection != null)
			{
				foreach (ConnectionStringSettings connectionStringSetting in connectionStringSettingsCollection)
				{
					if (string.IsNullOrEmpty(connectionStringSetting.Name))
					{
						throw new ConfigurationErrorsException("Name was not defined in the connection string.");
					}
					if (!string.IsNullOrEmpty(connectionStringSetting.ConnectionString))
					{
						continue;
					}
					throw new InvalidOperationException("Connectiong String could not be found.");
				}
			}
		}
	}
}