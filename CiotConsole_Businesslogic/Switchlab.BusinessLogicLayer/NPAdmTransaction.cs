using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmTransaction
	{
		private const string SP_GET_SENDER_ID = "Sender_getSenderId";

		private const string SP_GET_TRANS_BY_ID = "Transaction_getById";

		private const string SP_GET_LIST_TRANS_BY_CONF_DATE = "Transaction_getListTransByConfDate";

		private const string SP_GET_LIST_TRANS_BY_STATUS = "Transaction_getListTransByStatus";

		private const string SP_GET_LAST_CONF_TRANS_BY_TELP_NUMBER = "Transaction_getLastConfByTelpNumber";

		private const string SP_GET_LIST_COMPLETE_TRANS_BY_MONTH = "Transaction_getListCompleteTransByMonth";

		private const string SP_GET_LIST_COMPLETE_TRANS = "Transaction_getListCompleteTrans";

		private const string SP_GET_LIST_TRANS_TO_CONFIRM = "Transaction_getListTransToConfirm";

		private const string SP_GET_LIST_TRANS_TO_RETURN = "Transaction_getListTransToReturn";

		private const string SP_GET_LIST_TRANS_PORTING_REQUEST = "Transaction_getListPortingRequest";

		private const string SP_GET_LIST_TRANS_NP_UPDATE = "Transaction_getListNPUpdate";

		private const string SP_GET_LIST_TRANS_CONFIRMED = "Transaction_getListTransConfirmed";

		private const string SP_GET_LIST_TRANS_CONFIRMED_BY_CONF_DATE = "Transaction_getListTransConfirmedByConfDate";

		private const string SP_GET_LIST_COMPLETE_TRANS_PORTING_OUT = "Transaction_getListCompleteTransPortingOut";

		private const string SP_GET_LIST_COMPLETE_TRANS_PORTING_OUT_BY_MONTH = "Transaction_getListCompleteTransPortingOutByMonth";

		private const string SP_SEARCH_TRANS_BY_TELP = "Transaction_searchByTelpNumber";

		private const string SP_SEARCH_TRANS_BY_STATUS = "Transaction_searchByStatus";

		private const string SP_SEARCH_TRANS_BY_TELP_IN_SERIES = "Transaction_searchByTelpNumberInSeries";

		private const string SP_SEARCH_TRANS_BY_OCH_ORDER_NUMBER = "Transaction_searchByOchOrderNumber";

		private const string SP_CREATE_NP = "NP_Create";

		private const string SP_CONFIRM_NP = "NP_Conf";

		private const string SP_COMPLETE_NP = "NP_Completion";

		private const string SP_CANCEL_NP = "NP_Cancel";

		private const string SP_CHANGE_NP = "NP_Change";

		private const string SP_REJECT_NP = "NP_Reject";

		private const string SP_RETURN_NP = "NP_Return";

		private const string SP_RANGE_UPDATE = "NP_Range_Update";

		private const string SP_PORTING_RESPONSE_NP = "NP_Port_Resp";

		public const string NP_P5_PRIORITY = "P5";

		public const string NP_P2_PRIORITY = "P2";

		public const string POINT_OF_CONNECTION = "RECIPIENT";

		public const string NP_CREATE_TRANSACTION_TYPE = "001";

		public const string NP_CONFIRMATION_TRANSACTION_TYPE = "004";

		public const string NP_REJECT_TRANSACTION_TYPE = "006";

		public const string NP_CANCEL_TRANSACTION_TYPE = "007";

		public const string NP_COMPLETION_TRANSACTION_TYPE = "008";

		public const string NP_RETURN_TRANSACTION_TYPE = "012";

		public const string NP_RANGE_UPDATE_TRANSACTION_TYPE = "014";

		public const string NP_CHANGE_TRANSACTION_TYPE = "017";

		public const string NP_PORTING_RESP_TRANSACTION_TYPE = "019";

		private string _agent;

		private string _chargingInfo;

		private int _confirmationStatusId;

		private string _confirmationStatusText;

		private string _confirmedExecutionDate;

		private string _custFirstName;

		private string _custLastName;

		private string _currentNumberType;

		private string _currentRangeHolder;

		private string _currentServiceOperator;

		private string _currentNetworkOperator;

		private string _iccid;

		private string _municipality;

		private string _mode;

		private string _newNumberType;

		private string _numberPorted;

		private string _originatingOrderNumber;

		private string _OCHOrderNumber;

		private string _otherOperator;

		private string _portingCase;

		private string _pointOfConnection;

		private string _priority;

		private int _portingId;

		private string _routingInfo;

		private string _recipientServiceOperator;

		private string _recipientNetworkOperator;

		private int _rangeId;

		private string _rangeUpdateType;

		private string _requestedExecutionDate;

		private string _spc;

		private string _senderId;

		private int _seriesCount;

		private string _statusDesc;

		private int _status;

		private string _sendDateTime;

		private List<NPAdmSeries> _series;

		private string _seriesDisplay;

		private string _sendDate;

		private string _sendTime;

		private string _transactionType;

		private string _telephoneNumber;

		private int _transactionID;

		private int _uniqueID;

		private string _writtenTermination;

		public string Agent
		{
			get
			{
				return this._agent;
			}
			set
			{
				this._agent = value;
			}
		}

		public string ChargingInfo
		{
			get
			{
				return this._chargingInfo;
			}
			set
			{
				this._chargingInfo = value;
			}
		}

		public int ConfirmationStatusId
		{
			get
			{
				return this._confirmationStatusId;
			}
			set
			{
				this._confirmationStatusId = value;
			}
		}

		public string ConfirmationStatusText
		{
			get
			{
				return this._confirmationStatusText;
			}
			set
			{
				this._confirmationStatusText = value;
			}
		}

		public string ConfirmedExecutionDate
		{
			get
			{
				return this._confirmedExecutionDate;
			}
			set
			{
				this._confirmedExecutionDate = value;
			}
		}

		public string CurrentNetworkOperator
		{
			get
			{
				return this._currentNetworkOperator;
			}
			set
			{
				this._currentNetworkOperator = value;
			}
		}

		public string CurrentNumberType
		{
			get
			{
				return this._currentNumberType;
			}
			set
			{
				this._currentNumberType = value;
			}
		}

		public string CurrentRangeHolder
		{
			get
			{
				return this._currentRangeHolder;
			}
			set
			{
				this._currentRangeHolder = value;
			}
		}

		public string CurrentServiceOperator
		{
			get
			{
				return this._currentServiceOperator;
			}
			set
			{
				this._currentServiceOperator = value;
			}
		}

		public string CustFirstName
		{
			get
			{
				return this._custFirstName;
			}
			set
			{
				this._custFirstName = value;
			}
		}

		public string CustLastName
		{
			get
			{
				return this._custLastName;
			}
			set
			{
				this._custLastName = value;
			}
		}

		public string Iccid
		{
			get
			{
				return this._iccid;
			}
			set
			{
				this._iccid = value;
			}
		}

		public string Mode
		{
			get
			{
				return this._mode;
			}
			set
			{
				this._mode = value;
			}
		}

		public string Municipality
		{
			get
			{
				return this._municipality;
			}
			set
			{
				this._municipality = value;
			}
		}

		public string NewNumberType
		{
			get
			{
				return this._newNumberType;
			}
			set
			{
				this._newNumberType = value;
			}
		}

		public string NumberPorted
		{
			get
			{
				return this._numberPorted;
			}
			set
			{
				this._numberPorted = value;
			}
		}

		public string OCHOrderNumber
		{
			get
			{
				return this._OCHOrderNumber;
			}
			set
			{
				this._OCHOrderNumber = value;
			}
		}

		public string OriginatingOrderNumber
		{
			get
			{
				return this._originatingOrderNumber;
			}
			set
			{
				this._originatingOrderNumber = value;
			}
		}

		public string OtherOperator
		{
			get
			{
				return this._otherOperator;
			}
			set
			{
				this._otherOperator = value;
			}
		}

		public string PointOfConnection
		{
			get
			{
				return this._pointOfConnection;
			}
			set
			{
				this._pointOfConnection = value;
			}
		}

		public string PortingCase
		{
			get
			{
				return this._portingCase;
			}
			set
			{
				this._portingCase = value;
			}
		}

		public int PortingId
		{
			get
			{
				return this._portingId;
			}
			set
			{
				this._portingId = value;
			}
		}

		public string Priority
		{
			get
			{
				return this._priority;
			}
			set
			{
				this._priority = value;
			}
		}

		public int RangeId
		{
			get
			{
				return this._rangeId;
			}
			set
			{
				this._rangeId = value;
			}
		}

		public string RangeUpdateType
		{
			get
			{
				return this._rangeUpdateType;
			}
			set
			{
				this._rangeUpdateType = value;
			}
		}

		public string RecipientNetworkOperator
		{
			get
			{
				return this._recipientNetworkOperator;
			}
			set
			{
				this._recipientNetworkOperator = value;
			}
		}

		public string RecipientServiceOperator
		{
			get
			{
				return this._recipientServiceOperator;
			}
			set
			{
				this._recipientServiceOperator = value;
			}
		}

		public string RequestedExecutionDate
		{
			get
			{
				return this._requestedExecutionDate;
			}
			set
			{
				this._requestedExecutionDate = value;
			}
		}

		public string RoutingInfo
		{
			get
			{
				return this._routingInfo;
			}
			set
			{
				this._routingInfo = value;
			}
		}

		public string SendDate
		{
			get
			{
				return this._sendDate;
			}
			set
			{
				this._sendDate = value;
			}
		}

		public string SendDateTime
		{
			get
			{
				return this._sendDateTime;
			}
			set
			{
				this._sendDateTime = value;
			}
		}

		public string SenderID
		{
			get
			{
				return this._senderId;
			}
			set
			{
				this._senderId = value;
			}
		}

		public string SendTime
		{
			get
			{
				return this._sendTime;
			}
			set
			{
				this._sendTime = value;
			}
		}

		public List<NPAdmSeries> Series
		{
			get
			{
				return this._series;
			}
			set
			{
				this._series = value;
			}
		}

		public int SeriesCount
		{
			get
			{
				return this._seriesCount;
			}
			set
			{
				this._seriesCount = value;
			}
		}

		public string SeriesDisplay
		{
			get
			{
				return this._seriesDisplay;
			}
			set
			{
				this._seriesDisplay = value;
			}
		}

		public string Spc
		{
			get
			{
				return this._spc;
			}
			set
			{
				this._spc = value;
			}
		}

		public int Status
		{
			get
			{
				return this._status;
			}
			set
			{
				this._status = value;
			}
		}

		public string StatusDesc
		{
			get
			{
				return this._statusDesc;
			}
			set
			{
				this._statusDesc = value;
			}
		}

		public string TelpNumber
		{
			get
			{
				return this._telephoneNumber;
			}
			set
			{
				this._telephoneNumber = value;
			}
		}

		public int TransactionID
		{
			get
			{
				return this._transactionID;
			}
		}

		public string TransactionType
		{
			get
			{
				return this._transactionType;
			}
			set
			{
				this._transactionType = value;
			}
		}

		public int UniqueID
		{
			get
			{
				return this._uniqueID;
			}
			set
			{
				this._uniqueID = value;
			}
		}

		public string WrittenTermination
		{
			get
			{
				return this._writtenTermination;
			}
			set
			{
				this._writtenTermination = value;
			}
		}

		public NPAdmTransaction(string agent, string chargingInfo, int confirmationStatusId, string confirmationStatusText, string confirmedExecutionDate, string custFirstName, string custLastName, string currentNumberType, string currentRangeHolder, string currentServiceOperator, string currentNetworkOperator, string iccid, string municipality, string mode, string newNumberType, string numberPorted, string originatingOrderNumber, string OCHOrderNumber, string otherOperator, string portingCase, string pointOfConnection, string priority, int portingId, string routingInfo, string recipientServiceOperator, string recipientNetworkOperator, int rangeId, string rangeUpdateType, string requestedExecutionDate, string spc, string senderId, int seriesCount, string statusDesc, int status, string sendDateTime, string sendDate, string sendTime, string transactionType, string telephoneNumber, int transactionID, int uniqueID, string writtenTermination)
		{
			this._agent = agent;
			this._chargingInfo = chargingInfo;
			this._confirmationStatusId = confirmationStatusId;
			this._confirmationStatusText = confirmationStatusText;
			this._confirmedExecutionDate = confirmedExecutionDate;
			this._custFirstName = custFirstName;
			this._custLastName = custLastName;
			this._currentNumberType = currentNumberType;
			this._currentRangeHolder = currentRangeHolder;
			this._currentServiceOperator = currentServiceOperator;
			this._currentNetworkOperator = currentNetworkOperator;
			this._iccid = iccid;
			this._municipality = municipality;
			this._mode = mode;
			this._newNumberType = newNumberType;
			this._numberPorted = numberPorted;
			this._originatingOrderNumber = originatingOrderNumber;
			this._OCHOrderNumber = OCHOrderNumber;
			this._otherOperator = otherOperator;
			this._portingCase = portingCase;
			this._pointOfConnection = pointOfConnection;
			this._priority = priority;
			this._portingId = portingId;
			this._routingInfo = routingInfo;
			this._recipientServiceOperator = recipientServiceOperator;
			this._recipientNetworkOperator = recipientNetworkOperator;
			this._rangeId = rangeId;
			this._rangeUpdateType = rangeUpdateType;
			this._requestedExecutionDate = requestedExecutionDate;
			this._spc = spc;
			this._senderId = senderId;
			this._seriesCount = seriesCount;
			this._statusDesc = statusDesc;
			this._status = status;
			this._sendDateTime = sendDateTime;
			this._series = NPAdmSeries.ListSeriesByTransId(transactionID);
			if (this._series.Count == 0)
			{
				this._seriesDisplay = "-";
			}
			for (int i = 0; i < this._series.Count; i++)
			{
				if (i > 0)
				{
					NPAdmTransaction nPAdmTransaction = this;
					nPAdmTransaction._seriesDisplay = string.Concat(nPAdmTransaction._seriesDisplay, "; ");
				}
				NPAdmTransaction nPAdmTransaction1 = this;
				nPAdmTransaction1._seriesDisplay = string.Concat(nPAdmTransaction1._seriesDisplay, this._series[i].PhoneNumberStart, "-", this._series[i].PhoneNumberEnd);
			}
			this._sendDate = sendDate;
			this._sendTime = sendTime;
			this._transactionType = transactionType;
			this._telephoneNumber = telephoneNumber;
			this._transactionID = transactionID;
			this._uniqueID = uniqueID;
			this._writtenTermination = writtenTermination;
		}

		public NPAdmTransaction(string chargingInfo, int confirmationStatusId, string confirmedExecutionDate, string currentNetworkOperator, string currentNumberType, string currentRangeHolder, string currentServiceOperator, string iccid, string municipality, string newNumberType, string numberPorted, string ochorderNumber, string originatingOrderNumber, string otheroperator, string pointOfConnection, string portingCase, string priority, int rangeId, string rangeUpdateType, string recipientNetworkOperator, string recipientServiceOperator, string requestedExecutionDate, string routingInfo, string sendDate, string sentTime, string senderId, int seriesCount, string spc, string statusDesc, string telephoneNumber, int transacId, string transactionType, int uniqueId) : this(string.Empty, chargingInfo, confirmationStatusId, string.Empty, confirmedExecutionDate, string.Empty, string.Empty, currentNumberType, currentRangeHolder, currentServiceOperator, currentNetworkOperator, iccid, municipality, string.Empty, newNumberType, numberPorted, originatingOrderNumber, ochorderNumber, otheroperator, portingCase, pointOfConnection, priority, 0, routingInfo, recipientServiceOperator, recipientNetworkOperator, rangeId, rangeUpdateType, string.Empty, spc, senderId, seriesCount, statusDesc, 0, string.Empty, sendDate, sentTime, transactionType, telephoneNumber, transacId, uniqueId, string.Empty)
		{
		}

		public NPAdmTransaction(string chargingInfo, string confirmedExecutionDate, string confrimStatusText, string customerFirstName, string customerLastName, string municipality, string newNumberType, string originatingOrderNumber, string ochOrderNumber, int portingId, string portingCase, string recNetworkOperator, string recServiceOperator, string routingInfo, string spc, string sendDateTime, int seriesCount, string statusDesc, string telephoneNumber, int transId, int uniqueId) : this(string.Empty, chargingInfo, 0, confrimStatusText, confirmedExecutionDate, customerFirstName, customerLastName, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, municipality, string.Empty, newNumberType, string.Empty, originatingOrderNumber, ochOrderNumber, string.Empty, portingCase, string.Empty, string.Empty, portingId, routingInfo, recServiceOperator, recNetworkOperator, 0, string.Empty, string.Empty, spc, string.Empty, seriesCount, statusDesc, 0, sendDateTime, string.Empty, string.Empty, string.Empty, telephoneNumber, transId, uniqueId, string.Empty)
		{
		}

		public NPAdmTransaction(string agent, string mode, string originatingOrderNumber, string ochOrderNumber, string sendDateTime, string statusDesc, string telephoneNumber, int transId, string transType, int uniqueId) : this(agent, string.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, mode, string.Empty, string.Empty, originatingOrderNumber, ochOrderNumber, string.Empty, string.Empty, string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, 0, statusDesc, 0, sendDateTime, string.Empty, string.Empty, transType, telephoneNumber, transId, uniqueId, string.Empty)
		{
		}

		public NPAdmTransaction(string originatingOrderNumber, string ochorderNumber, string pointOfConnection, int portingId, string requestedExecutionDate, string recipientServiceOperator, string recipientNetworkOperator, string sendDateTime, string sender, int seriesCount, int status, int transId, string telephoneNumber, int uniqueId, string writtenTermination) : this(string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, originatingOrderNumber, ochorderNumber, string.Empty, string.Empty, pointOfConnection, string.Empty, portingId, string.Empty, recipientServiceOperator, recipientNetworkOperator, 0, string.Empty, requestedExecutionDate, string.Empty, sender, seriesCount, string.Empty, status, sendDateTime, string.Empty, string.Empty, string.Empty, telephoneNumber, transId, uniqueId, writtenTermination)
		{
		}

		public static int CancelNP(string senderId, string sendDate, string sendTime, string telpNumber, string OCHOrderNumber, int uniqueId, string originatingOrderNumber, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P5");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SenderID", SqlDbType.VarChar, 60, ParameterDirection.Input, senderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SendDate", SqlDbType.Char, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.Char, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelephoneNumber", SqlDbType.VarChar, 15, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OCHOrderNumber", SqlDbType.VarChar, 12, ParameterDirection.Input, OCHOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@UniqueID", SqlDbType.Int, 0, ParameterDirection.Input, uniqueId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OriginatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Cancel");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int value = 0;
			if (sqlCommand.Parameters["@transId"] != null)
			{
				value = (int)sqlCommand.Parameters["@transId"].Value;
			}
			return value;
		}

		public static int ChangeNP(string senderId, string sendDate, string sendTime, string telpNumber, string originatingOrderNumber, string currServiceOp, string currNetworkOp, string recServiceOp, string portingCase, string spc, string municipality, string routingInfo, string chargingInfo, string newNumberType, string numberPorted, int uniqueId, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P2");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SenderID", SqlDbType.VarChar, 60, ParameterDirection.Input, senderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SendDate", SqlDbType.Char, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.Char, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelephoneNumber", SqlDbType.VarChar, 15, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OriginatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@UniqueID", SqlDbType.Int, 0, ParameterDirection.Input, uniqueId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Change");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int value = 0;
			if (sqlCommand.Parameters["@transId"] != null)
			{
				value = (int)sqlCommand.Parameters["@transId"].Value;
			}
			return value;
		}

		public static int CompleteNP(string senderId, string sendDate, string sendTime, string chargingInfo, string municipality, string newNumberType, string numberPorted, string OCHOrderNumber, string originatingOrderNumber, string portingCase, string recipientNetworkOperator, string recipientServiceOperator, string routingInfo, int seriesCount, string spc, string telpNumber, int uniqueId, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P2");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SenderID", SqlDbType.VarChar, 60, ParameterDirection.Input, senderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SendDate", SqlDbType.Char, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.Char, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelephoneNumber", SqlDbType.VarChar, 15, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OCHOrderNumber", SqlDbType.VarChar, 12, ParameterDirection.Input, OCHOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@UniqueID", SqlDbType.Int, 0, ParameterDirection.Input, uniqueId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OriginatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@RecipientServiceOperator", SqlDbType.Char, 5, ParameterDirection.Input, recipientServiceOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@RecipientNetworkOperator", SqlDbType.Char, 8, ParameterDirection.Input, recipientNetworkOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingCase", SqlDbType.VarChar, 60, ParameterDirection.Input, portingCase);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@spc", SqlDbType.VarChar, 6, ParameterDirection.Input, spc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@municipality", SqlDbType.VarChar, 3, ParameterDirection.Input, municipality);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@routingInfo", SqlDbType.VarChar, 8, ParameterDirection.Input, routingInfo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@chargingInfo", SqlDbType.VarChar, 8, ParameterDirection.Input, chargingInfo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@newNumberType", SqlDbType.VarChar, 60, ParameterDirection.Input, newNumberType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@numberPorted", SqlDbType.Char, 1, ParameterDirection.Input, numberPorted);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SeriesCount", SqlDbType.Int, 0, ParameterDirection.Input, seriesCount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Completion");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int value = 0;
			if (sqlCommand.Parameters["@transId"] != null)
			{
				value = (int)sqlCommand.Parameters["@transId"].Value;
			}
			return value;
		}

		public static int ConfirmNP(string senderId, string sendDate, string sendTime, string telpNumber, string ochOrderNumber, int uniqueId, string originatingOrderNumber, string confExecutionDate, int seriesCount, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P5");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SenderID", SqlDbType.VarChar, 60, ParameterDirection.Input, senderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SendDate", SqlDbType.Char, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.Char, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelephoneNumber", SqlDbType.VarChar, 15, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OCHOrderNumber", SqlDbType.VarChar, 12, ParameterDirection.Input, ochOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@UniqueID", SqlDbType.Int, 0, ParameterDirection.Input, uniqueId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OriginatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@CurrentServiceOperator", SqlDbType.Char, 5, ParameterDirection.Input, "");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@CurrentNetworkOperator", SqlDbType.Char, 5, ParameterDirection.Input, "");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@CurrentNumberType", SqlDbType.Char, 5, ParameterDirection.Input, "");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ConfirmedExecutionDate", SqlDbType.Char, 8, ParameterDirection.Input, confExecutionDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ConfirmedExecutionTime", SqlDbType.Char, 4, ParameterDirection.Input, "");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ConfirmationStatus", SqlDbType.Int, 0, ParameterDirection.Input, 0);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@DirectoryInfo", SqlDbType.Int, 0, ParameterDirection.Input, 0);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SeriesCount", SqlDbType.Int, 0, ParameterDirection.Input, seriesCount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Conf");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int value = 0;
			if (sqlCommand.Parameters["@transId"] != null)
			{
				value = (int)sqlCommand.Parameters["@transId"].Value;
			}
			return value;
		}

		public static int CreateNP(string senderID, string sendDate, string sendTime, string telpNumber, string OCHOrderNumber, int uniqueID, string originatingOrderNumber, string currentServiceOperator, string recipientServiceOperator, string recipientNetworkOperator, string currentNumberType, string requestedExecutionDate, string requestedExecutionTime, string customerID, string ICC, string seriesCount, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P5");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SenderID", SqlDbType.VarChar, 60, ParameterDirection.Input, senderID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SendDate", SqlDbType.Char, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.Char, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelephoneNumber", SqlDbType.VarChar, 15, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OCHOrderNumber", SqlDbType.VarChar, 12, ParameterDirection.Input, OCHOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@UniqueID", SqlDbType.Int, 0, ParameterDirection.Input, uniqueID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OriginatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@CurrentServiceOperator", SqlDbType.Char, 5, ParameterDirection.Input, currentServiceOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@RecipientServiceOperator", SqlDbType.Char, 5, ParameterDirection.Input, recipientServiceOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@RecipientNetworkOperator", SqlDbType.Char, 8, ParameterDirection.Input, recipientNetworkOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@CurrentNumberType", SqlDbType.VarChar, 60, ParameterDirection.Input, currentNumberType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@RequestedExecutionDate", SqlDbType.Char, 8, ParameterDirection.Input, requestedExecutionDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@RequestedExecutionTime", SqlDbType.Char, 4, ParameterDirection.Input, requestedExecutionTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@CustomerID", SqlDbType.VarChar, 60, ParameterDirection.Input, customerID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ICC", SqlDbType.VarChar, 60, ParameterDirection.Input, ICC);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@PointOfConnection", SqlDbType.VarChar, 60, ParameterDirection.Input, "RECIPIENT");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SeriesCount", SqlDbType.Int, 0, ParameterDirection.Input, seriesCount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Create");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int value = 0;
			if (sqlCommand.Parameters["@transId"] != null)
			{
				value = (int)sqlCommand.Parameters["@transId"].Value;
			}
			return value;
		}

		private static void GenerateCompleteTransListFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["chargingInfo"]), string.Empty, string.Empty, GeneralConverter.ToString(returnData["customerFirstName"]), GeneralConverter.ToString(returnData["customerLastName"]), GeneralConverter.ToString(returnData["municipality"]), GeneralConverter.ToString(returnData["newNumberType"]), GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["ochOrderNumber"]), 0, GeneralConverter.ToString(returnData["portingCase"]), GeneralConverter.ToString(returnData["recNetworkOperator"]), GeneralConverter.ToString(returnData["recServiceOperator"]), GeneralConverter.ToString(returnData["routingInfo"]), GeneralConverter.ToString(returnData["spc"]), GeneralConverter.ToString(returnData["sendDateTime"]), GeneralConverter.ToInt32(returnData["seriesCount"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToInt32(returnData["uniqueId"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateLastConfTransListByTelpNumberFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["OCHOrderNumber"]), string.Empty, 0, string.Empty, GeneralConverter.ToString(returnData["recipientServiceOperator"]), GeneralConverter.ToString(returnData["recipientNetworkOperator"]), string.Empty, GeneralConverter.ToString(returnData["senderId"]), GeneralConverter.ToInt32(returnData["seriesCount"]), GeneralConverter.ToInt32(returnData["portingStatusId"]), GeneralConverter.ToInt32(returnData["transId"]), string.Empty, GeneralConverter.ToInt32(returnData["UniqueId"]), string.Empty);
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransByIdFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["chargingInfo"]), GeneralConverter.ToInt32(returnData["confirmationStatusId"]), GeneralConverter.ToString(returnData["confirmedExecutionDate"]), GeneralConverter.ToString(returnData["currentNetworkOperator"]), GeneralConverter.ToString(returnData["currentNumberType"]), GeneralConverter.ToString(returnData["currentRangeHolder"]), GeneralConverter.ToString(returnData["currentServiceOperator"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToString(returnData["municipality"]), GeneralConverter.ToString(returnData["newnumberType"]), GeneralConverter.ToString(returnData["numberPorted"]), GeneralConverter.ToString(returnData["ochorderNumber"]), GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["otheroperator"]), GeneralConverter.ToString(returnData["pointOfConnection"]), GeneralConverter.ToString(returnData["portingCase"]), GeneralConverter.ToString(returnData["priority"]), GeneralConverter.ToInt32(returnData["rangeId"]), GeneralConverter.ToString(returnData["rangeUpdateType"]), GeneralConverter.ToString(returnData["recipientNetworkOperator"]), GeneralConverter.ToString(returnData["recipientServiceOperator"]), GeneralConverter.ToString(returnData["requestedExecutionDate"]), GeneralConverter.ToString(returnData["routingInfo"]), GeneralConverter.ToString(returnData["sendDate"]), GeneralConverter.ToString(returnData["sentTime"]), GeneralConverter.ToString(returnData["senderId"]), GeneralConverter.ToInt32(returnData["seriesCount"]), GeneralConverter.ToString(returnData["spc"]), string.Empty, GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["transacId"]), GeneralConverter.ToString(returnData["transactionType"]), GeneralConverter.ToInt32(returnData["uniqueId"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListByConfExecDateFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["chargingInfo"]), GeneralConverter.ToString(returnData["confirmedExecutionDate"]), GeneralConverter.ToString(returnData["confrimStatusText"]), GeneralConverter.ToString(returnData["customerFirstName"]), GeneralConverter.ToString(returnData["customerLastName"]), GeneralConverter.ToString(returnData["municipality"]), GeneralConverter.ToString(returnData["newNumberType"]), GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["ochOrderNumber"]), GeneralConverter.ToInt32(returnData["portingId"]), string.Empty, GeneralConverter.ToString(returnData["recNetworkOperator"]), GeneralConverter.ToString(returnData["recServiceOperator"]), GeneralConverter.ToString(returnData["routingInfo"]), GeneralConverter.ToString(returnData["spc"]), GeneralConverter.ToString(returnData["sendDateTime"]), GeneralConverter.ToInt32(returnData["seriesCount"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToInt32(returnData["uniqueId"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListByOchOrderNumberFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["agentId"]), string.Empty, GeneralConverter.ToString(returnData["originatingOrderNumber"]), string.Empty, GeneralConverter.ToString(returnData["sendDateTime"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToString(returnData["transType"]), GeneralConverter.ToInt32(returnData["uniqueId"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListByStatusFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["agentId"]), GeneralConverter.ToString(returnData["portingMode"]), GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["ochOrderNumber"]), GeneralConverter.ToString(returnData["submittedDate"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToString(returnData["transType"]), 0);
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListByTelpNumberFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["agent"]), GeneralConverter.ToString(returnData["mode"]), GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["ochOrderNumber"]), GeneralConverter.ToString(returnData["date"]), GeneralConverter.ToString(returnData["statusDesc"]), string.Empty, GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToString(returnData["transType"]), GeneralConverter.ToInt32(returnData["uniqueId"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListConfirmedFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(string.Empty, GeneralConverter.ToString(returnData["confirmedExecutionDate"]), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["ochOrderNumber"]), GeneralConverter.ToInt32(returnData["portingId"]), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, GeneralConverter.ToString(returnData["sendDateTime"]), GeneralConverter.ToInt32(returnData["seriesCount"]), string.Empty, GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToInt32(returnData["uniqueId"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListNPUpdateFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["chargingInfo"]), 0, string.Empty, GeneralConverter.ToString(returnData["currentNetworkOperator"]), GeneralConverter.ToString(returnData["currentNumberType"]), string.Empty, GeneralConverter.ToString(returnData["currentServiceOperator"]), string.Empty, GeneralConverter.ToString(returnData["municipality"]), string.Empty, GeneralConverter.ToString(returnData["numberPorted"]), GeneralConverter.ToString(returnData["ochOrderNumber"]), GeneralConverter.ToString(returnData["originatingOrderNumber"]), string.Empty, string.Empty, string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, GeneralConverter.ToString(returnData["routingInfo"]), GeneralConverter.ToString(returnData["sendDateTime"]), string.Empty, string.Empty, 0, GeneralConverter.ToString(returnData["spc"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["transId"]), string.Empty, GeneralConverter.ToInt32(returnData["uniqueId"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListPortingRequestFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["ochOrderNumber"]), string.Empty, 0, string.Empty, GeneralConverter.ToString(returnData["recNetworkOperator"]), GeneralConverter.ToString(returnData["recServiceOperator"]), GeneralConverter.ToString(returnData["sendDateTime"]), GeneralConverter.ToString(returnData["sender"]), GeneralConverter.ToInt32(returnData["seriesCount"]), 0, GeneralConverter.ToInt32(returnData["transacId"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["uniqueId"]), string.Empty);
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListToConfirmFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["ochorderNumber"]), GeneralConverter.ToString(returnData["pointOfConnection"]), GeneralConverter.ToInt32(returnData["portingId"]), GeneralConverter.ToString(returnData["requestedExecutionDate"]), GeneralConverter.ToString(returnData["recipientServiceOperator"]), GeneralConverter.ToString(returnData["recipientNetworkOperator"]), GeneralConverter.ToString(returnData["sendDateTime"]), GeneralConverter.ToString(returnData["sender"]), GeneralConverter.ToInt32(returnData["seriesCount"]), 0, GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["uniqueId"]), GeneralConverter.ToString(returnData["writtenTermination"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		private static void GenerateTransListToReturnFromReader<T>(IDataReader returnData, ref List<NPAdmTransaction> listNew)
		{
			while (returnData.Read())
			{
				NPAdmTransaction nPAdmTransaction = new NPAdmTransaction(GeneralConverter.ToString(returnData["chargingInfo"]), string.Empty, string.Empty, GeneralConverter.ToString(returnData["customerFirstName"]), GeneralConverter.ToString(returnData["customerLastName"]), GeneralConverter.ToString(returnData["municipality"]), GeneralConverter.ToString(returnData["newNumberType"]), GeneralConverter.ToString(returnData["originatingOrderNumber"]), GeneralConverter.ToString(returnData["ochOrderNumber"]), GeneralConverter.ToInt32(returnData["portingId"]), GeneralConverter.ToString(returnData["portingCase"]), GeneralConverter.ToString(returnData["recNetworkOperator"]), GeneralConverter.ToString(returnData["recServiceOperator"]), GeneralConverter.ToString(returnData["routingInfo"]), GeneralConverter.ToString(returnData["spc"]), GeneralConverter.ToString(returnData["sendDateTime"]), GeneralConverter.ToInt32(returnData["seriesCount"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["telephoneNumber"]), GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToInt32(returnData["uniqueId"]));
				listNew.Add(nPAdmTransaction);
			}
		}

		public static NPAdmTransaction GetLastConfTransByTelpNumber(string telpNumber)
		{
			if (telpNumber == string.Empty)
			{
				throw new ArgumentOutOfRangeException("telpNumber");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telpNumber", SqlDbType.Int, 0, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getLastConfByTelpNumber");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateLastConfTransListByTelpNumberFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			if (nPAdmTransactions.Count <= 0)
			{
				return null;
			}
			return nPAdmTransactions[0];
		}

		public static string GetSenderId()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Sender_getSenderId");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == null)
			{
				return string.Empty;
			}
			return obj.ToString();
		}

		public static NPAdmTransaction GetTransactionById(int transId)
		{
			if (transId < 0)
			{
				throw new ArgumentOutOfRangeException("transId");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Input, transId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getById");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransByIdFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			if (nPAdmTransactions.Count <= 0)
			{
				return null;
			}
			return nPAdmTransactions[0];
		}

		public static List<NPAdmTransaction> ListCompleteTransByMonth(int month)
		{
			if (month < 0)
			{
				throw new ArgumentOutOfRangeException("month");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			string str = month.ToString();
			if (month != 0)
			{
				if (month < 10)
				{
					str = string.Concat("0", str);
				}
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@month", SqlDbType.Char, 2, ParameterDirection.Input, str);
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListCompleteTransByMonth");
			}
			else
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListCompleteTrans");
			}
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateCompleteTransListFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListCompleteTransPortingOutByMonth(int month)
		{
			if (month < 0)
			{
				throw new ArgumentOutOfRangeException("month");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			string str = month.ToString();
			if (month != 0)
			{
				if (month < 10)
				{
					str = string.Concat("0", str);
				}
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@month", SqlDbType.Char, 2, ParameterDirection.Input, str);
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListCompleteTransPortingOutByMonth");
			}
			else
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListCompleteTransPortingOut");
			}
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateCompleteTransListFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListConfirmedTransaction()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListTransConfirmed");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListConfirmedFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListConfirmedTransactionByConfExecDate(string confExecDate)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListTransConfirmedByConfDate");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@confExecDate", SqlDbType.VarChar, 8, ParameterDirection.Input, confExecDate);
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListConfirmedFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListTransactionByConfirmedExecutionDate(string confDate)
		{
			if (confDate == string.Empty)
			{
				throw new ArgumentOutOfRangeException("confDate");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@confDate", SqlDbType.VarChar, 8, ParameterDirection.Input, confDate);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListTransByConfDate");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListByConfExecDateFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListTransactionByStatus(int status)
		{
			if (status < 0)
			{
				throw new ArgumentOutOfRangeException("status");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, status);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListTransByStatus");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListByConfExecDateFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListTransactionNPUpdate()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListNPUpdate");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListNPUpdateFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListTransactionPortingRequest()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListPortingRequest");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListPortingRequestFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListTransactionToConfirm()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListTransToConfirm");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListToConfirmFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> ListTransactionToReturn()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_getListTransToReturn");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListToReturnFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static int PortingResponseNP(string senderId, string sendDate, string sendTime, string telpNumber, string ochOrderNumber, int uniqueId, string originatingOrderNumber, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P5");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@senderId", SqlDbType.VarChar, 60, ParameterDirection.Input, senderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sendDate", SqlDbType.Char, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.Char, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelephoneNumber", SqlDbType.VarChar, 15, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OchOrderNumber", SqlDbType.VarChar, 12, ParameterDirection.Input, ochOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@uniqueId", SqlDbType.Int, 0, ParameterDirection.Input, uniqueId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@originatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Port_Resp");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int value = 0;
			if (sqlCommand.Parameters["@transId"] != null)
			{
				value = (int)sqlCommand.Parameters["@transId"].Value;
			}
			return value;
		}

		public static int RejectNP(string senderId, string sendDate, string sendTime, string telpNumber, string ochOrderNumber, int uniqueId, string originatingOrderNumber, string otherOperator, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P5");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@senderId", SqlDbType.VarChar, 60, ParameterDirection.Input, senderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sendDate", SqlDbType.VarChar, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.VarChar, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelephoneNumber", SqlDbType.VarChar, 15, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OCHOrderNumber", SqlDbType.VarChar, 12, ParameterDirection.Input, ochOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@uniqueId", SqlDbType.Int, 0, ParameterDirection.Input, uniqueId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@originatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@otherOperator", SqlDbType.Char, 5, ParameterDirection.Input, otherOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Reject");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int value = 0;
			if (sqlCommand.Parameters["@transId"] != null)
			{
				value = (int)sqlCommand.Parameters["@transId"].Value;
			}
			return value;
		}

		public static int ReturnNP(string senderId, string sendDate, string sendTime, string telpNumber, string originatingOrderNumber, int seriesCount, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P2");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@senderId", SqlDbType.VarChar, 60, ParameterDirection.Input, senderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sendDate", SqlDbType.VarChar, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.VarChar, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TelephoneNumber", SqlDbType.VarChar, 15, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@originatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@seriesCount", SqlDbType.Int, 0, ParameterDirection.Input, seriesCount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transId", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Return");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			int value = 0;
			if (sqlCommand.Parameters["@transId"] != null)
			{
				value = (int)sqlCommand.Parameters["@transId"].Value;
			}
			return value;
		}

		public static List<NPAdmTransaction> SearchTransactionByOchOrderNumber(string ochOrderNumber)
		{
			if (ochOrderNumber == string.Empty)
			{
				throw new ArgumentOutOfRangeException("ochOrderNumber");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ochOrderNumber", SqlDbType.VarChar, 12, ParameterDirection.Input, ochOrderNumber);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_searchByOchOrderNumber");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListByOchOrderNumberFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> SearchTransactionByStatus(int status)
		{
			if (status < 0)
			{
				throw new ArgumentOutOfRangeException("status");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, status);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_searchByStatus");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListByStatusFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> SearchTransactionByTelp(string telpNumber)
		{
			if (telpNumber == string.Empty)
			{
				throw new ArgumentOutOfRangeException("telpNumber");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telpNumber", SqlDbType.VarChar, 17, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_searchByTelpNumber");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListByTelpNumberFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static List<NPAdmTransaction> SearchTransactionByTelpInSeries(string telpNumber)
		{
			if (telpNumber == string.Empty)
			{
				throw new ArgumentOutOfRangeException("telpNumber");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telpNumber", SqlDbType.VarChar, 17, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Transaction_searchByTelpNumberInSeries");
			List<NPAdmTransaction> nPAdmTransactions = new List<NPAdmTransaction>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmTransaction>(sqlCommand, new GenerateListFromReader<NPAdmTransaction>(NPAdmTransaction.GenerateTransListByTelpNumberFromReader<NPAdmTransaction>), ref nPAdmTransactions);
			return nPAdmTransactions;
		}

		public static bool UpdateRange(string senderId, string sendDate, string sendTime, string originatingOrderNumber, string rangeUpdateType, string range, string otherOperator, string currentRangeHolder, string currentServiceOperator, string currentNetworkOperator, string portingCase, string spc, string municipality, string routingInfo, string chargingInfo, string newNumberType, int portingStatusId, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@priority", SqlDbType.Char, 2, ParameterDirection.Input, "P2");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@senderId", SqlDbType.VarChar, 60, ParameterDirection.Input, senderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sendDate", SqlDbType.VarChar, 8, ParameterDirection.Input, sendDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SentTime", SqlDbType.VarChar, 4, ParameterDirection.Input, sendTime);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@OCHOrderNumber", SqlDbType.VarChar, 12, ParameterDirection.Input, "");
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@uniqueId", SqlDbType.Int, 0, ParameterDirection.Input, 0);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@originatingOrderNumber", SqlDbType.VarChar, 20, ParameterDirection.Input, originatingOrderNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@rangeUpdateType", SqlDbType.Char, 1, ParameterDirection.Input, rangeUpdateType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@range", SqlDbType.VarChar, 17, ParameterDirection.Input, range);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@otherOperator", SqlDbType.Char, 5, ParameterDirection.Input, otherOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currentRangeHolder", SqlDbType.Char, 5, ParameterDirection.Input, currentRangeHolder);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currentServiceOperator", SqlDbType.Char, 5, ParameterDirection.Input, currentServiceOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currentNetworkOperator", SqlDbType.Char, 5, ParameterDirection.Input, currentNetworkOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingCase", SqlDbType.VarChar, 60, ParameterDirection.Input, portingCase);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@spc", SqlDbType.VarChar, 6, ParameterDirection.Input, spc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@municipality", SqlDbType.Char, 3, ParameterDirection.Input, municipality);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@routingInfo", SqlDbType.VarChar, 8, ParameterDirection.Input, routingInfo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@chargingInfo", SqlDbType.VarChar, 8, ParameterDirection.Input, chargingInfo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@newNumberType", SqlDbType.VarChar, 60, ParameterDirection.Input, newNumberType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingStatusId", SqlDbType.Int, 0, ParameterDirection.Input, portingStatusId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@transID", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Range_Update");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			if (sqlCommand.Parameters["@transId"] != null && sqlCommand.Parameters["@transId"].Value.ToString().Trim() != string.Empty)
			{
				return true;
			}
			return false;
		}
	}
}