using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMDIBSOrder
	{
		private const string SP_GET_ID = "get_order_id";

		private const string SP_CREATE_CHARGE_HISTORY = "create_charge_history";

		private const string SP_UPDATE_CHARGE_ORDER = "update_charge_order";

		private const string SP_DELETE = "DIBS_order_delete";

		private const string SP_GET_REASON = "get_reason_error";

		private const string SP_GET_CURRCODE = "get_ccy_code";

		private const string SP_GET_REASON_CAPTURE = "get_reason_error_capture";

		private const string SP_CREATE_REQ_TRANSACT = "create_req_transact";

		private const string SP_GET_VAT = "get_VAT";

		private int _iorderid;

		private int _ireasonid;

		private string _ireason;

		private string _icurrcode;

		private double _dVAT;

		public string CurrCode
		{
			get
			{
				if (this._icurrcode == null)
				{
					return string.Empty;
				}
				return this._icurrcode;
			}
		}

		public int OrderId
		{
			get
			{
				return this._iorderid;
			}
		}

		public string Reason
		{
			get
			{
				if (this._ireason == null)
				{
					return string.Empty;
				}
				return this._ireason;
			}
		}

		public int ReasonId
		{
			get
			{
				return this._ireasonid;
			}
		}

		public double VAT
		{
			get
			{
				return this._dVAT;
			}
		}

		public CRMDIBSOrder()
		{
		}

		public CRMDIBSOrder(int iOrderId)
		{
			this._iorderid = iOrderId;
		}

		public CRMDIBSOrder(double dVat)
		{
			this._dVAT = dVat;
		}

		public CRMDIBSOrder(string iCurrCode)
		{
			this._icurrcode = iCurrCode;
		}

		public CRMDIBSOrder(int iReasonId, string iReason)
		{
			this._ireasonid = iReasonId;
			this._ireason = iReason;
		}

		public static bool CreateReqTransact(int orderId, string custInfo, int custType, float amount, float vatAmount, string appRequester, string firstName, string lastName, string cardNo, string cardExpdate, string cardCsc, string cardName, string address1, string town, string state, string postCode, string countryCode, string currCode, int companyId, string ipAddress, string typeTP)
		{
			int num = cardName.IndexOf(" ", 0);
			firstName = cardName.Substring(0, num);
			lastName = cardName.Substring(num + 1, cardName.Length - (num + 1));
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, orderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custinfo", SqlDbType.VarChar, 20, ParameterDirection.Input, custInfo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custtype", SqlDbType.Int, 0, ParameterDirection.Input, custType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@amount", SqlDbType.Float, 0, ParameterDirection.Input, amount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@vatamount", SqlDbType.Float, 0, ParameterDirection.Input, vatAmount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@apprequester", SqlDbType.VarChar, 50, ParameterDirection.Input, appRequester);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 32, ParameterDirection.Input, firstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastname", SqlDbType.VarChar, 32, ParameterDirection.Input, lastName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardno", SqlDbType.VarChar, 32, ParameterDirection.Input, cardNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardexpdate", SqlDbType.Char, 4, ParameterDirection.Input, cardExpdate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardcsc", SqlDbType.Char, 4, ParameterDirection.Input, cardCsc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardname", SqlDbType.VarChar, 32, ParameterDirection.Input, cardName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address1", SqlDbType.VarChar, 64, ParameterDirection.Input, address1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@town", SqlDbType.VarChar, 32, ParameterDirection.Input, town);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@state", SqlDbType.VarChar, 32, ParameterDirection.Input, state);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@postcode", SqlDbType.VarChar, 16, ParameterDirection.Input, postCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countrycode", SqlDbType.Char, 2, ParameterDirection.Input, countryCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, currCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardownerid", SqlDbType.Int, 0, ParameterDirection.Input, 0);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@companyid", SqlDbType.Int, 0, ParameterDirection.Input, companyId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ip_address", SqlDbType.VarChar, 15, ParameterDirection.Input, ipAddress);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@type_tp", SqlDbType.VarChar, 20, ParameterDirection.Input, typeTP);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "create_req_transact");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public static bool Delete(int iOrderId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@id", SqlDbType.Int, 0, ParameterDirection.Input, iOrderId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "DIBS_order_delete");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		private static void GenerateCurrCodeFromReader<T>(IDataReader returnData, ref List<CRMDIBSOrder> listNew)
		{
			while (returnData.Read())
			{
				CRMDIBSOrder cRMDIBSOrder = new CRMDIBSOrder(GeneralConverter.ToString(returnData["dibs_code"]));
				listNew.Add(cRMDIBSOrder);
			}
		}

		private static void GenerateOrderIdFromReader<T>(IDataReader returnData, ref List<CRMDIBSOrder> listNew)
		{
			while (returnData.Read())
			{
				CRMDIBSOrder cRMDIBSOrder = new CRMDIBSOrder(GeneralConverter.ToInt32(returnData["orderid"]));
				listNew.Add(cRMDIBSOrder);
			}
		}

		private static void GenerateReasonFromReader<T>(IDataReader returnData, ref List<CRMDIBSOrder> listNew)
		{
			while (returnData.Read())
			{
				CRMDIBSOrder cRMDIBSOrder = new CRMDIBSOrder(GeneralConverter.ToInt32(returnData["id"]), GeneralConverter.ToString(returnData["reason"]));
				listNew.Add(cRMDIBSOrder);
			}
		}

		private static void GenerateVatFromReader<T>(IDataReader returnData, ref List<CRMDIBSOrder> listNew)
		{
			while (returnData.Read())
			{
				CRMDIBSOrder cRMDIBSOrder = new CRMDIBSOrder(GeneralConverter.ToDouble(returnData["vat"]));
				listNew.Add(cRMDIBSOrder);
			}
		}

		public static CRMDIBSOrder GetCurrency(string iCurrCode)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, iCurrCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_ccy_code");
			List<CRMDIBSOrder> cRMDIBSOrders = new List<CRMDIBSOrder>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMDIBSOrder>(sqlCommand, new GenerateListFromReader<CRMDIBSOrder>(CRMDIBSOrder.GenerateCurrCodeFromReader<CRMDIBSOrder>), ref cRMDIBSOrders);
			if (cRMDIBSOrders.Count <= 0)
			{
				return null;
			}
			return cRMDIBSOrders[0];
		}

		public static CRMDIBSOrder GetID()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_order_id");
			List<CRMDIBSOrder> cRMDIBSOrders = new List<CRMDIBSOrder>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMDIBSOrder>(sqlCommand, new GenerateListFromReader<CRMDIBSOrder>(CRMDIBSOrder.GenerateOrderIdFromReader<CRMDIBSOrder>), ref cRMDIBSOrders);
			if (cRMDIBSOrders.Count <= 0)
			{
				return null;
			}
			return cRMDIBSOrders[0];
		}

		public static CRMDIBSOrder GetReason(int iReasonId, int type)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@id", SqlDbType.Int, 0, ParameterDirection.Input, iReasonId);
			if (type != 0)
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_reason_error_capture");
			}
			else
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_reason_error");
			}
			List<CRMDIBSOrder> cRMDIBSOrders = new List<CRMDIBSOrder>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMDIBSOrder>(sqlCommand, new GenerateListFromReader<CRMDIBSOrder>(CRMDIBSOrder.GenerateReasonFromReader<CRMDIBSOrder>), ref cRMDIBSOrders);
			if (cRMDIBSOrders.Count <= 0)
			{
				return null;
			}
			return cRMDIBSOrders[0];
		}

		public static CRMDIBSOrder GetVat(string sCountryCode)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countryCode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCountryCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_VAT");
			List<CRMDIBSOrder> cRMDIBSOrders = new List<CRMDIBSOrder>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMDIBSOrder>(sqlCommand, new GenerateListFromReader<CRMDIBSOrder>(CRMDIBSOrder.GenerateVatFromReader<CRMDIBSOrder>), ref cRMDIBSOrders);
			if (cRMDIBSOrders.Count <= 0)
			{
				return null;
			}
			return cRMDIBSOrders[0];
		}

		public static bool insertChargeHistory(int iOrderId, string iLastUser, int iReasonId, int iCCId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, iOrderId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastuser", SqlDbType.VarChar, 128, ParameterDirection.Input, iLastUser);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@reasonid", SqlDbType.Int, 0, ParameterDirection.Input, iReasonId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ccid", SqlDbType.Int, 0, ParameterDirection.Input, iCCId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "create_charge_history");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		public static bool updateChargeOrder(int iOrderId, string iLastUser, int iLastReason)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastuser", SqlDbType.VarChar, 128, ParameterDirection.Input, iLastUser);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastreason", SqlDbType.Int, 0, ParameterDirection.Input, iLastReason);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, iOrderId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "update_charge_order");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}
	}
}