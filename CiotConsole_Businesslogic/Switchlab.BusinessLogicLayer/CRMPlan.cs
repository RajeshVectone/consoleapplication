using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMPlan
	{
		private const string SP_PLAN_GET_BYID = "plan_view_by_id";

		private const string SP_PLAN_GET_BYLOGIN = "plan_view_by_login";

		private int _iplanid;

		private string _splanname;

		private string _splandesc;

		private int _ivoice_mins;

		private int _imessage_texts;

		private int _ivideo_mins;

		private int _imessage_videos;

		private int _idownloads;

		private int _icontract_months;

		private double _dmonth_price;

		private string _scurrcode;

		private string _slogin;

		public int Contract_Months
		{
			get
			{
				return this._icontract_months;
			}
			set
			{
				this._icontract_months = value;
			}
		}

		public string CurrencyCode
		{
			get
			{
				if (this._scurrcode == null)
				{
					return string.Empty;
				}
				return this._scurrcode;
			}
			set
			{
				this._scurrcode = value;
			}
		}

		public int Downloads
		{
			get
			{
				return this._idownloads;
			}
			set
			{
				this._idownloads = value;
			}
		}

		public string Login
		{
			get
			{
				if (this._slogin == null)
				{
					return string.Empty;
				}
				return this._slogin;
			}
			set
			{
				this._slogin = value;
			}
		}

		public int MessageTexts
		{
			get
			{
				return this._imessage_texts;
			}
			set
			{
				this._imessage_texts = value;
			}
		}

		public int MessageVideo
		{
			get
			{
				return this._imessage_videos;
			}
			set
			{
				this._imessage_videos = value;
			}
		}

		public double Month_Price
		{
			get
			{
				return this._dmonth_price;
			}
			set
			{
				this._dmonth_price = value;
			}
		}

		public string PlanDescription
		{
			get
			{
				if (this._splandesc == null)
				{
					return string.Empty;
				}
				return this._splandesc;
			}
			set
			{
				this._splandesc = value;
			}
		}

		public int PlanID
		{
			get
			{
				return this._iplanid;
			}
			set
			{
				this._iplanid = value;
			}
		}

		public string PlanName
		{
			get
			{
				if (this._splanname == null)
				{
					return string.Empty;
				}
				return this._splanname;
			}
			set
			{
				this._splanname = value;
			}
		}

		public int VideoMins
		{
			get
			{
				return this._ivideo_mins;
			}
			set
			{
				this._ivideo_mins = value;
			}
		}

		public int VoiceMins
		{
			get
			{
				return this._ivoice_mins;
			}
			set
			{
				this._ivoice_mins = value;
			}
		}

		public CRMPlan()
		{
		}

		public CRMPlan(int iPlanID, string sPlanName, string sPlanDesc, int iVoiceMins, int iMessageTexts, int iVideoMins, int iMessageVideos, int iDownloads, int iContractMonths, double dMonthPrice, string sCurrCode)
		{
			this._iplanid = iPlanID;
			this._splanname = sPlanName;
			this._splandesc = sPlanDesc;
			this._ivoice_mins = iVoiceMins;
			this._imessage_texts = iMessageTexts;
			this._ivideo_mins = iVideoMins;
			this._imessage_videos = iMessageVideos;
			this._idownloads = iDownloads;
			this._icontract_months = iContractMonths;
			this._dmonth_price = dMonthPrice;
			this._scurrcode = sCurrCode;
		}

		private static void GeneratePlanListFromReader<T>(IDataReader returnData, ref List<CRMPlan> listNew)
		{
			while (returnData.Read())
			{
				CRMPlan cRMPlan = new CRMPlan(GeneralConverter.ToInt32(returnData["planid"]), GeneralConverter.ToString(returnData["planname"]), GeneralConverter.ToString(returnData["plandesc"]), GeneralConverter.ToInt32(returnData["voice_mins"]), GeneralConverter.ToInt32(returnData["message_texts"]), GeneralConverter.ToInt32(returnData["video_mins"]), GeneralConverter.ToInt32(returnData["message_videos"]), GeneralConverter.ToInt32(returnData["downloads"]), GeneralConverter.ToInt32(returnData["contract_months"]), GeneralConverter.ToDouble(returnData["month_price"]), GeneralConverter.ToString(returnData["currcode"]));
				listNew.Add(cRMPlan);
			}
		}

		public static CRMPlan GetPlanById(int iPlanID)
		{
			if (iPlanID < 0)
			{
				throw new ArgumentOutOfRangeException("iSubscriberID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@planid", SqlDbType.Int, 0, ParameterDirection.Input, iPlanID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "plan_view_by_id");
			List<CRMPlan> cRMPlans = new List<CRMPlan>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPlan>(sqlCommand, new GenerateListFromReader<CRMPlan>(CRMPlan.GeneratePlanListFromReader<CRMPlan>), ref cRMPlans);
			if (cRMPlans.Count != 1)
			{
				return null;
			}
			return cRMPlans[0];
		}

		public static CRMPlan GetPlanByLogin(string sLogin)
		{
			if (string.IsNullOrEmpty(sLogin))
			{
				throw new ArgumentOutOfRangeException("sLogin");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@login", SqlDbType.VarChar, 20, ParameterDirection.Input, sLogin);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "plan_view_by_login");
			List<CRMPlan> cRMPlans = new List<CRMPlan>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPlan>(sqlCommand, new GenerateListFromReader<CRMPlan>(CRMPlan.GeneratePlanListFromReader<CRMPlan>), ref cRMPlans);
			if (cRMPlans.Count != 1)
			{
				return null;
			}
			return cRMPlans[0];
		}
	}
}