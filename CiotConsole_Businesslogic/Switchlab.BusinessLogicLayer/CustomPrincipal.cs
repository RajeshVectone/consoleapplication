using System;
using System.Security.Principal;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CustomPrincipal : IPrincipal
	{
		private int _userID;

		private string _userRole = string.Empty;

		private string _name;

		protected IIdentity _Identity;

		public IIdentity Identity
		{
			get
			{
				return JustDecompileGenerated_get_Identity();
			}
			set
			{
				JustDecompileGenerated_set_Identity(value);
			}
		}

		public IIdentity JustDecompileGenerated_get_Identity()
		{
			return this._Identity;
		}

		public void JustDecompileGenerated_set_Identity(IIdentity value)
		{
			this._Identity = value;
		}

		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public int UserID
		{
			get
			{
				return this._userID;
			}
			set
			{
				this._userID = value;
			}
		}

		public string UserRole
		{
			get
			{
				return this._userRole;
			}
			set
			{
				this._userRole = value;
			}
		}

		public CustomPrincipal()
		{
		}

		public CustomPrincipal(IIdentity identity, string userRole, string name) : this(identity, 0, userRole, name)
		{
		}

		public CustomPrincipal(IIdentity identity, int userID, string userRole, string name)
		{
			this._Identity = identity;
			this._userID = userID;
			this._userRole = userRole;
		}

		public bool IsInRole(string role)
		{
			string[] strArrays = role.Split(new char[] { ',' });
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				if (this._userRole == str)
				{
					return true;
				}
			}
			return false;
		}
	}
}