using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	[Serializable]
	public class CrmSalesOrderItem
	{
		protected const string SP_SELECT = "sales_order_item_select";

		protected const string SP_DETAIL = "sales_order_item_detail";

		protected const string SP_DELETE = "sales_order_item_delete";

		protected const string SP_INSERT = "sales_order_item_insert";

		protected const string SP_UPDATE = "sales_order_item_update";

		protected const string SP_SELECT_SALESITEM = "sales_item_select";

		protected const string SP_SELECT_SALESITEM_ITEM = "sales_item_select_item";

		protected const string SP_DETAIL_SALESITEM = "sales_item_detail";

		protected const string SP_SELECT_SALESSTATUS = "ref_sales_status_select";

		private int _orderid;

		private int _itemid;

		private int _productid;

		private int _quantity;

		private float _price;

		private string _currcode;

		private string _keyid;

		private int _salestatus;

		private DateTime _reservedate;

		private string _reserveby;

		private DateTime _saledate;

		private string _saleby;

		private DateTime _returndate;

		private string _returnby;

		private string _productcode;

		private string _productname;

		private string _productdesc;

		private float _sales_product_price;

		private string _sales_product_currcode;

		private string _keyname;

		private string _statusname;

		private string _statusdesc;

		public string currcode
		{
			get
			{
				if (this._currcode == null)
				{
					return string.Empty;
				}
				return this._currcode;
			}
		}

		public int itemid
		{
			get
			{
				return this._itemid;
			}
		}

		public string keyid
		{
			get
			{
				if (this._keyid == null)
				{
					return string.Empty;
				}
				return this._keyid;
			}
		}

		public string keyname
		{
			get
			{
				if (this._keyname == null)
				{
					return string.Empty;
				}
				return this._keyname;
			}
		}

		public int orderid
		{
			get
			{
				return this._orderid;
			}
		}

		public float price
		{
			get
			{
				return this._price;
			}
		}

		public string productcode
		{
			get
			{
				if (this._productcode == null)
				{
					return string.Empty;
				}
				return this._productcode;
			}
		}

		public string productdesc
		{
			get
			{
				if (this._productdesc == null)
				{
					return string.Empty;
				}
				return this._productdesc;
			}
		}

		public int productid
		{
			get
			{
				return this._productid;
			}
		}

		public string productname
		{
			get
			{
				if (this._productname == null)
				{
					return string.Empty;
				}
				return this._productname;
			}
		}

		public int quantity
		{
			get
			{
				return this._quantity;
			}
		}

		public string reserveby
		{
			get
			{
				if (this._reserveby == null)
				{
					return string.Empty;
				}
				return this._reserveby;
			}
		}

		public DateTime reservedate
		{
			get
			{
				return this._reservedate;
			}
		}

		public string returnby
		{
			get
			{
				if (this._returnby == null)
				{
					return string.Empty;
				}
				return this._returnby;
			}
		}

		public DateTime returndate
		{
			get
			{
				return this._returndate;
			}
		}

		public string saleby
		{
			get
			{
				if (this._saleby == null)
				{
					return string.Empty;
				}
				return this._saleby;
			}
		}

		public DateTime saledate
		{
			get
			{
				return this._saledate;
			}
		}

		public string sales_product_currcode
		{
			get
			{
				if (this._sales_product_currcode == null)
				{
					return string.Empty;
				}
				return this._sales_product_currcode;
			}
		}

		public float sales_product_price
		{
			get
			{
				return this._sales_product_price;
			}
		}

		public int salestatus
		{
			get
			{
				return this._salestatus;
			}
		}

		public string statusdesc
		{
			get
			{
				if (this._statusdesc == null)
				{
					return string.Empty;
				}
				return this._statusdesc;
			}
		}

		public string statusname
		{
			get
			{
				if (this._statusname == null)
				{
					return string.Empty;
				}
				return this._statusname;
			}
		}

		public CrmSalesOrderItem(int __orderid, int __itemid, int __productid, int __quantity, float __price, string __currcode)
		{
			this._orderid = __orderid;
			this._itemid = __itemid;
			this._productid = __productid;
			this._quantity = __quantity;
			this._price = __price;
			this._currcode = __currcode;
		}

		public CrmSalesOrderItem(int __orderid, int __itemid, int __productid, int __quantity, float __price, string __currcode, string __keyid, int __salestatus, DateTime __reservedate, string __reserveby, DateTime __saledate, string __saleby, DateTime __returndate, string __returnby, string __productcode, string __productname, string __productdesc, float __sales_product_price, string __sales_product_currcode, string __keyname, string __statusname, string __statusdesc) : this(__orderid, __itemid, __productid, __quantity, __price, __currcode)
		{
			this._keyid = __keyid;
			this._salestatus = __salestatus;
			this._reservedate = __reservedate;
			this._reserveby = __reserveby;
			this._saledate = __saledate;
			this._saleby = __saleby;
			this._returndate = __returndate;
			this._returnby = __returnby;
			this._productcode = __productcode;
			this._productname = __productname;
			this._productdesc = __productdesc;
			this._sales_product_price = __sales_product_price;
			this._sales_product_currcode = __sales_product_currcode;
			this._keyname = __keyname;
			this._statusname = __statusname;
			this._statusdesc = __statusdesc;
		}

		public CrmSalesOrderItem(int __itemid, int __productid, int __quantity, float __price, string __currcode) : this(0, __itemid, __productid, __quantity, __price, __currcode)
		{
		}

		public CrmSalesOrderItem() : this(0, 0, 0, 0, 0f, null)
		{
		}

		public CrmSalesOrderItem(int __itemid, int __productid, string __keyid, int __salestatus, DateTime __reservedate, string __reserveby, DateTime __saledate, string __saleby, DateTime __returndate, string __returnby, string __productcode, string __productname, string __productdesc, float __sales_product_price, string __sales_product_currcode, string __keyname) : this(0, __itemid, __productid, 0, 0f, null, __keyid, __salestatus, __reservedate, __reserveby, __saledate, __saleby, __returndate, __returnby, __productcode, __productname, __productdesc, __sales_product_price, __sales_product_currcode, __keyname, null, null)
		{
		}

		public CrmSalesOrderItem(int __salestatus, string __statusname, string __statusdesc) : this(0, 0, 0, 0, 0f, null, null, __salestatus, DateTime.MinValue, null, DateTime.MinValue, null, DateTime.MinValue, null, null, null, null, 0f, null, null, __statusname, __statusdesc)
		{
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static IListSource _getAll(int __orderid)
		{
			if (__orderid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_item_select");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(CrmSalesOrderItem.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static IListSource _getById(int __orderid, int __itemid, int __productid)
		{
			if (__orderid < 0 || __itemid < 0 || __productid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid,itemid,productid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemid", SqlDbType.Int, 0, ParameterDirection.Input, __itemid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, __productid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_item_detail");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(CrmSalesOrderItem.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Delete, true)]
		public static bool Delete(int __orderid, int __itemid, int __productid)
		{
			if (__orderid < 0 || __itemid < 0 || __productid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid,itemid,productid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemid", SqlDbType.Int, 0, ParameterDirection.Input, __itemid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, __productid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_item_delete");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CrmSalesOrderItem> theList)
		{
			while (returnData.Read())
			{
				CrmSalesOrderItem crmSalesOrderItem = new CrmSalesOrderItem((int)returnData["orderid"], (int)returnData["itemid"], (int)returnData["productid"], (int)returnData["quantity"], (float)((double)returnData["price"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToString(returnData["keyid"]), (int)returnData["salestatus"], Convert.ToDateTime(returnData["reservedate"]), GeneralConverter.ToString(returnData["reserveby"]), Convert.ToDateTime(returnData["saledate"]), GeneralConverter.ToString(returnData["saleby"]), Convert.ToDateTime(returnData["returndate"]), GeneralConverter.ToString(returnData["returnby"]), GeneralConverter.ToString(returnData["productcode"]), GeneralConverter.ToString(returnData["productname"]), GeneralConverter.ToString(returnData["productdesc"]), (float)((double)returnData["sales_product_price"]), GeneralConverter.ToString(returnData["sales_product_currcode"]), GeneralConverter.ToString(returnData["keyname"]), GeneralConverter.ToString(returnData["statusname"]), GeneralConverter.ToString(returnData["statusdesc"]));
				theList.Add(crmSalesOrderItem);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<IListSource> theList)
		{
			DataTable dataTable = new DataTable();
			dataTable.Load(returnData);
			theList.Add(dataTable);
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReaderSalesItem<T>(IDataReader returnData, ref List<CrmSalesOrderItem> theList)
		{
			while (returnData.Read())
			{
				CrmSalesOrderItem crmSalesOrderItem = new CrmSalesOrderItem((int)returnData["itemid"], (int)returnData["productid"], GeneralConverter.ToString(returnData["keyid"]), (int)returnData["salestatus"], Convert.ToDateTime(returnData["reservedate"]), GeneralConverter.ToString(returnData["reserveby"]), Convert.ToDateTime(returnData["saledate"]), GeneralConverter.ToString(returnData["saleby"]), Convert.ToDateTime(returnData["returndate"]), GeneralConverter.ToString(returnData["returnby"]), GeneralConverter.ToString(returnData["productcode"]), GeneralConverter.ToString(returnData["productname"]), GeneralConverter.ToString(returnData["productdesc"]), (float)((double)returnData["sales_product_price"]), GeneralConverter.ToString(returnData["sales_product_currcode"]), GeneralConverter.ToString(returnData["keyname"]));
				theList.Add(crmSalesOrderItem);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReaderSalesStatus<T>(IDataReader returnData, ref List<CrmSalesOrderItem> theList)
		{
			while (returnData.Read())
			{
				CrmSalesOrderItem crmSalesOrderItem = new CrmSalesOrderItem((int)returnData["salestatus"], GeneralConverter.ToString(returnData["statusname"]), GeneralConverter.ToString(returnData["statusdesc"]));
				theList.Add(crmSalesOrderItem);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmSalesOrderItem> GetAll(int __orderid)
		{
			if (__orderid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_item_select");
			List<CrmSalesOrderItem> crmSalesOrderItems = new List<CrmSalesOrderItem>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrderItem>(sqlCommand, new GenerateListFromReader<CrmSalesOrderItem>(CrmSalesOrderItem.GenerateListFromReader<CrmSalesOrderItem>), ref crmSalesOrderItems);
			return crmSalesOrderItems;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmSalesOrderItem> GetAllSalesItem()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_item_select");
			List<CrmSalesOrderItem> crmSalesOrderItems = new List<CrmSalesOrderItem>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrderItem>(sqlCommand, new GenerateListFromReader<CrmSalesOrderItem>(CrmSalesOrderItem.GenerateListFromReaderSalesItem<CrmSalesOrderItem>), ref crmSalesOrderItems);
			return crmSalesOrderItems;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmSalesOrderItem> GetAllSalesStatus()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "ref_sales_status_select");
			List<CrmSalesOrderItem> crmSalesOrderItems = new List<CrmSalesOrderItem>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrderItem>(sqlCommand, new GenerateListFromReader<CrmSalesOrderItem>(CrmSalesOrderItem.GenerateListFromReaderSalesStatus<CrmSalesOrderItem>), ref crmSalesOrderItems);
			return crmSalesOrderItems;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static CrmSalesOrderItem GetById(int __orderid, int __itemid, int __productid)
		{
			if (__orderid < 0 || __itemid < 0 || __productid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid,itemid,productid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemid", SqlDbType.Int, 0, ParameterDirection.Input, __itemid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, __productid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_item_detail");
			List<CrmSalesOrderItem> crmSalesOrderItems = new List<CrmSalesOrderItem>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrderItem>(sqlCommand, new GenerateListFromReader<CrmSalesOrderItem>(CrmSalesOrderItem.GenerateListFromReader<CrmSalesOrderItem>), ref crmSalesOrderItems);
			if (crmSalesOrderItems.Count <= 0)
			{
				return null;
			}
			return crmSalesOrderItems[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static CrmSalesOrderItem GetSalesItemById(int __itemid, int __productid)
		{
			if (__itemid < 0 || __productid < 0)
			{
				throw new ArgumentOutOfRangeException("itemid,productid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemid", SqlDbType.Int, 0, ParameterDirection.Input, __itemid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, __productid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_item_detail");
			List<CrmSalesOrderItem> crmSalesOrderItems = new List<CrmSalesOrderItem>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrderItem>(sqlCommand, new GenerateListFromReader<CrmSalesOrderItem>(CrmSalesOrderItem.GenerateListFromReaderSalesItem<CrmSalesOrderItem>), ref crmSalesOrderItems);
			if (crmSalesOrderItems.Count <= 0)
			{
				return null;
			}
			return crmSalesOrderItems[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static List<CrmSalesOrderItem> GetSalesItemByItem(int __itemid)
		{
			if (__itemid < 0)
			{
				throw new ArgumentOutOfRangeException("itemid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemid", SqlDbType.Int, 0, ParameterDirection.Input, __itemid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_item_select_item");
			List<CrmSalesOrderItem> crmSalesOrderItems = new List<CrmSalesOrderItem>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmSalesOrderItem>(sqlCommand, new GenerateListFromReader<CrmSalesOrderItem>(CrmSalesOrderItem.GenerateListFromReaderSalesItem<CrmSalesOrderItem>), ref crmSalesOrderItems);
			return crmSalesOrderItems;
		}

		[DataObjectMethod(DataObjectMethodType.Insert, true)]
		public static bool Insert(int iOrderID, int iItemID, int iProductID, int iQuantity, double dPrice, string sCurrCode, string sSaleBy)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, iOrderID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemid", SqlDbType.Int, 0, ParameterDirection.Input, iItemID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, iProductID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@quantity", SqlDbType.Int, 0, ParameterDirection.Input, iQuantity);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@price", SqlDbType.Float, 0, ParameterDirection.Input, dPrice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@saleby", SqlDbType.VarChar, 16, ParameterDirection.Input, sSaleBy);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_item_insert");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Update, true)]
		public static bool Update(int __orderid, int __itemid, int __productid, int __quantity, float __price, string __currcode)
		{
			if (__orderid < 0 || __itemid < 0 || __productid < 0)
			{
				throw new ArgumentOutOfRangeException("orderid,itemid,productid");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@orderid", SqlDbType.Int, 0, ParameterDirection.Input, __orderid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemid", SqlDbType.Int, 0, ParameterDirection.Input, __itemid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, __productid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@quantity", SqlDbType.Int, 0, ParameterDirection.Input, __quantity);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@price", SqlDbType.Float, 0, ParameterDirection.Input, __price);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, __currcode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sales_order_item_update");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}
	}
}