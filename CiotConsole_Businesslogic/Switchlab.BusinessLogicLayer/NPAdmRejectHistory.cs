using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmRejectHistory
	{
		private const string SP_INSERT_REJECT_HISTORY = "NP_Insert_Reject";

		private const string SP_LIST_REJECT_HISTORY_BY_TRANS_ID = "RejectHistory_listByTransId";

		private int _transacId;

		private int _rejectNo;

		private string _rejectCode;

		private string _rejectText;

		public string RejectCode
		{
			get
			{
				return this._rejectCode;
			}
			set
			{
				this._rejectCode = value;
			}
		}

		public int RejectNo
		{
			get
			{
				return this._rejectNo;
			}
			set
			{
				this._rejectNo = value;
			}
		}

		public string RejectText
		{
			get
			{
				return this._rejectText;
			}
			set
			{
				this._rejectText = value;
			}
		}

		public int TransacId
		{
			get
			{
				return this._transacId;
			}
			set
			{
				this._transacId = value;
			}
		}

		public NPAdmRejectHistory()
		{
		}

		public NPAdmRejectHistory(int transId, int rejectNo, string rejectCode, string rejectText)
		{
			this._transacId = transId;
			this._rejectNo = rejectNo;
			this._rejectCode = rejectCode;
			this._rejectText = rejectText;
		}

		private static void GenerateRejectHistoryListByTransIdFromReader<T>(IDataReader returnData, ref List<NPAdmRejectHistory> listNew)
		{
			while (returnData.Read())
			{
				NPAdmRejectHistory nPAdmRejectHistory = new NPAdmRejectHistory(GeneralConverter.ToInt32(returnData["transacId"]), GeneralConverter.ToInt32(returnData["rejectNo"]), GeneralConverter.ToString(returnData["rejectCode"]), GeneralConverter.ToString(returnData["rejectText"]));
				listNew.Add(nPAdmRejectHistory);
			}
		}

		public static bool InsertRejectHistory(int transId, int rejectNo, string rejectCode)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TransID", SqlDbType.Int, 0, ParameterDirection.Input, transId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@rejectNo", SqlDbType.Int, 0, ParameterDirection.Input, rejectNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@rejectCode", SqlDbType.Char, 3, ParameterDirection.Input, rejectCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Insert_Reject");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		public static List<NPAdmRejectHistory> ListRejectHistoryByTransId(int transId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TransID", SqlDbType.Int, 0, ParameterDirection.Input, transId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "RejectHistory_listByTransId");
			List<NPAdmRejectHistory> nPAdmRejectHistories = new List<NPAdmRejectHistory>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmRejectHistory>(sqlCommand, new GenerateListFromReader<NPAdmRejectHistory>(NPAdmRejectHistory.GenerateRejectHistoryListByTransIdFromReader<NPAdmRejectHistory>), ref nPAdmRejectHistories);
			return nPAdmRejectHistories;
		}
	}
}