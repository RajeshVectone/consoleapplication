using System;

namespace Switchlab.BusinessLogicLayer
{
	public class CRMMesages
	{
		public static string E101
		{
			get
			{
				return "Login or password is not match.";
			}
		}

		public static string E102
		{
			get
			{
				return "Your session hase expired, please login again";
			}
		}

		public static string I102
		{
			get
			{
				return "You completely log out";
			}
		}

		public CRMMesages()
		{
		}
	}
}