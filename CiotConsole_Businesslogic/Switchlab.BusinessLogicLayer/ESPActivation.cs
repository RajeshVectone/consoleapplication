using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class ESPActivation
	{
		private const string SP_CREATEACTIVATION = "rp1_createactivation";

		private int _activateid;

		private string _sitecode;

		private string _telcocode;

		private string _custcode;

		private int _batchcode;

		private int _serialcode_fr;

		private int _serialcode_to;

		private int _sellerid;

		private string _actby;

		private string _authby;

		private int _actstatus;

		private DateTime _activdate;

		private DateTime _expdate;

		private float _fixprice;

		private float _sellprice;

		private float _vat;

		private float _discount;

		private string _currcode;

		private string _trffclass;

		public ESPActivation(int activateId, string siteCode, string telcoCode, string custCode, int batchCode, int serialCodeFrom, int serialCodeTo, int customerId, string actBy, string authBy, int actStatus, DateTime activeDate, DateTime expiryDate, float fixPrice, float sellPrice, float vat, float discount, string currCode, string tariffClass)
		{
			if (string.IsNullOrEmpty(telcoCode))
			{
				throw new ArgumentException("telcoCode");
			}
			if (string.IsNullOrEmpty(custCode))
			{
				throw new ArgumentException("custCode");
			}
			if (batchCode < 0 || batchCode > 9999)
			{
				throw new ArgumentOutOfRangeException("batchCode");
			}
			if (serialCodeFrom < 0 || serialCodeFrom > 9999)
			{
				throw new ArgumentOutOfRangeException("serialCodeFrom");
			}
			if (serialCodeTo < 0 || serialCodeTo > 9999)
			{
				throw new ArgumentOutOfRangeException("serialCodeTo");
			}
			this._activateid = activateId;
			this._sitecode = siteCode;
			this._telcocode = telcoCode;
			this._custcode = custCode;
			this._batchcode = batchCode;
			this._serialcode_fr = serialCodeFrom;
			this._serialcode_to = serialCodeTo;
			this._sellerid = customerId;
			this._actby = actBy;
			this._authby = authBy;
			this._actstatus = actStatus;
			this._activdate = activeDate;
			this._expdate = expiryDate;
			this._fixprice = fixPrice;
			this._sellprice = sellPrice;
			this._vat = vat;
			this._discount = discount;
			this._currcode = currCode;
			this._trffclass = tariffClass;
		}

		public ESPActivation(string siteCode, string telcoCode, string custCode, int batchCode, int serialCodeFrom, int serialCodeTo, int customerId, string currCode, string tariffClass) : this(0, siteCode, telcoCode, custCode, batchCode, serialCodeFrom, serialCodeTo, customerId, "", "", 1, DateTime.Now, DateTime.Now.AddYears(10), 0f, 0f, 0f, 0f, currCode, tariffClass)
		{
		}

		public bool CreateActivation(int resellerId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 0, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 32, ParameterDirection.Input, this._sitecode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 4, ParameterDirection.Input, this._telcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, this._custcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, this._batchcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode_fr", SqlDbType.Int, 0, ParameterDirection.Input, this._serialcode_fr);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode_to", SqlDbType.Int, 0, ParameterDirection.Input, this._serialcode_to);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sellerid", SqlDbType.Int, 0, ParameterDirection.Input, this._sellerid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@actby", SqlDbType.VarChar, 50, ParameterDirection.Input, this._actby);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@authby", SqlDbType.VarChar, 50, ParameterDirection.Input, this._authby);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@actstatus", SqlDbType.Int, 0, ParameterDirection.Input, this._actstatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@activdate", SqlDbType.DateTime, 0, ParameterDirection.Input, this._activdate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@expdate", SqlDbType.DateTime, 0, ParameterDirection.Input, this._expdate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fixprice", SqlDbType.Float, 0, ParameterDirection.Input, this._fixprice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sellprice", SqlDbType.Float, 0, ParameterDirection.Input, this._sellprice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@vat", SqlDbType.Float, 0, ParameterDirection.Input, this._vat);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@discount", SqlDbType.Float, 0, ParameterDirection.Input, this._discount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, this._currcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@trffclass", SqlDbType.VarChar, 4, ParameterDirection.Input, this._trffclass);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_createactivation");
			sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			return (int)sqlCommand.Parameters["@ReturnValue"].Value == 0;
		}
	}
}