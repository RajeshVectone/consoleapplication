using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	public class ESPAccountActivation
	{
		protected const string MANAGE_SELECT_SP = "rp1_loadedacc_list";

		protected const string MANAGE_DETAIL_SP = "rp1_loadedacc_getdetail";

		protected const string MANAGE_DELETE_SP = "rp1_loadedacc_manage_delete";

		protected const string MANAGE_INSERT_SP = "rp1_loadedacc_manage_insert";

		protected const string MANAGE_UPDATE_SP = "rp1_loadedacc_manage_update";

		protected const string MANAGE_GENERATE_ACCOUNT_SP = "rp1_account_generate";

		protected const string MANAGE_ACTIVATE_ACCOUNT_SP = "rp1_account_activation";

		protected const string MANAGE_ACTIVATE_LINE_SP = "line_activation";

		protected int _resellerid;

		private string _telcocode;

		private string _sitecode;

		private string _custcode;

		private int? _cardid;

		private float? _value;

		private int? _daysvalid;

		private string _currcode;

		private string _trffclass;

		private DateTime? _startdate;

		private DateTime? _enddate;

		private string _login;

		private string _langcode;

		private int? _batchcode;

		private int? _sfrom;

		private int? _sto;

		private int? _sellerid;

		private float? _fixprice;

		private float? _sellprice;

		private int? _newfrom;

		private int? _newto;

		private DateTime? _loadingdate;

		private string _cardname;

		private DateTime? _expdate;

		private string _custcode_batchcode;

		private int? _customerid;

		public int? batchcode
		{
			get
			{
				return this._batchcode;
			}
		}

		public int? cardid
		{
			get
			{
				return this._cardid;
			}
		}

		public string cardname
		{
			get
			{
				if (this._cardname == null)
				{
					return string.Empty;
				}
				return this._cardname;
			}
		}

		public string currcode
		{
			get
			{
				if (this._currcode == null)
				{
					return string.Empty;
				}
				return this._currcode;
			}
		}

		public string custcode
		{
			get
			{
				if (this._custcode == null)
				{
					return string.Empty;
				}
				return this._custcode;
			}
		}

		public string custcode_batchcode
		{
			get
			{
				if (this._custcode_batchcode == null)
				{
					return string.Empty;
				}
				return this._custcode_batchcode;
			}
		}

		public int? daysvalid
		{
			get
			{
				return this._daysvalid;
			}
		}

		public DateTime? expdate
		{
			get
			{
				return this._expdate;
			}
		}

		public float? fixprice
		{
			get
			{
				return this._fixprice;
			}
		}

		public string fixprice_currcode
		{
			get
			{
				return string.Format("{0} {1}", this.fixprice, this.currcode);
			}
		}

		public DateTime? loadingdate
		{
			get
			{
				return this._loadingdate;
			}
		}

		public int? sfrom
		{
			get
			{
				return this._sfrom;
			}
		}

		public string sitecode
		{
			get
			{
				if (this._sitecode == null)
				{
					return string.Empty;
				}
				return this._sitecode;
			}
		}

		public int? sto
		{
			get
			{
				return this._sto;
			}
		}

		public string telcocode
		{
			get
			{
				if (this._telcocode == null)
				{
					return string.Empty;
				}
				return this._telcocode;
			}
		}

		public int? total
		{
			get
			{
				int? nullable;
				int? nullable1 = this.sto;
				int? nullable2 = this.sfrom;
				if (nullable1.HasValue & nullable2.HasValue)
				{
					nullable = new int?(nullable1.GetValueOrDefault() - nullable2.GetValueOrDefault());
				}
				else
				{
					nullable = null;
				}
				int? nullable3 = nullable;
				if (!nullable3.HasValue)
				{
					return null;
				}
				return new int?(nullable3.GetValueOrDefault() + 1);
			}
		}

		public string trffclass
		{
			get
			{
				if (this._trffclass == null)
				{
					return string.Empty;
				}
				return this._trffclass;
			}
		}

		public ESPAccountActivation(int __resellerid, string __telcocode, string __sitecode, string __custcode, int? __cardid, float? __value, int? __daysvalid, string __currcode, string __trffclass, DateTime? __startdate, DateTime? __enddate, string __login, string __langcode)
		{
			this._resellerid = __resellerid;
			this._telcocode = __telcocode;
			this._sitecode = __sitecode;
			this._custcode = __custcode;
			this._cardid = __cardid;
			this._value = __value;
			this._daysvalid = __daysvalid;
			this._currcode = __currcode;
			this._trffclass = __trffclass;
			this._startdate = __startdate;
			this._enddate = __enddate;
			this._login = __login;
			this._langcode = __langcode;
		}

		public ESPAccountActivation(int __resellerid, string __telcocode, string __sitecode, string __custcode, int? __batchcode, int? __sfrom, int? __sto, int? __sellerid, float? __fixprice, float? __sellprice, string __currcode, string __trffclass, int? __newfrom, int? __newto) : this(__resellerid, __telcocode, __sitecode, __custcode, null, null, null, __currcode, __trffclass, null, null, null, null)
		{
			this._batchcode = __batchcode;
			this._sfrom = __sfrom;
			this._sto = __sto;
			this._sellerid = __sellerid;
			this._fixprice = __fixprice;
			this._sellprice = __sellprice;
			this._newfrom = __newfrom;
			this._newto = __newto;
		}

		public ESPAccountActivation(string __sitecode, string __telcocode, string __custcode, int? __batchcode, int? __sfrom, int? __sto, DateTime? __expdate, float? __fixprice, string __currcode, string __trffclass, int? __daysvalid, DateTime? __loadingdate, int? __cardid, string __cardname) : this(0, __telcocode, __sitecode, __custcode, __batchcode, __sfrom, __sto, null, __fixprice, null, __currcode, __trffclass, null, null)
		{
			this._expdate = __expdate;
			this._daysvalid = __daysvalid;
			this._loadingdate = __loadingdate;
			this._cardid = __cardid;
			this._cardname = __cardname;
		}

		public ESPAccountActivation(string __custcode, int? __batchcode, int? __sfrom, int? __sto, DateTime? __expdate, float? __fixprice, string __currcode, string __trffclass, int? __daysvalid, DateTime? __loadingdate, string __cardname) : this(0, null, null, __custcode, __batchcode, __sfrom, __sto, null, __fixprice, null, __currcode, __trffclass, null, null)
		{
			this._expdate = __expdate;
			this._daysvalid = __daysvalid;
			this._loadingdate = __loadingdate;
			this._cardid = null;
			this._cardname = __cardname;
		}

		public ESPAccountActivation(int __resellerid, int? __customerid, string __telcocode, string __custcode, int? __batchcode, int? __sfrom, int? __sto) : this(__resellerid, __telcocode, null, __custcode, __batchcode, __sfrom, __sto, null, null, null, null, null, null, null)
		{
			this._customerid = __customerid;
		}

		public ESPAccountActivation(string __custcode_batchcode, int? __batchcode)
		{
			this._custcode_batchcode = __custcode_batchcode;
			this._batchcode = __batchcode;
		}

		public ESPAccountActivation()
		{
		}

		public ESPAccountActivation(int __resellerid)
		{
			this._resellerid = __resellerid;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static IListSource _getAll(int __resellerid, int __cardid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardid", SqlDbType.Int, 0, ParameterDirection.Input, __cardid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_loadedacc_list");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(ESPAccountActivation.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Insert, false)]
		public bool ActivateAccount(int __resellerid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, this._telcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, this._sitecode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, this._custcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, this._batchcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sfrom", SqlDbType.Int, 0, ParameterDirection.Input, this._sfrom);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sto", SqlDbType.Int, 0, ParameterDirection.Input, this._sto);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sellerid", SqlDbType.Int, 0, ParameterDirection.Input, this._sellerid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fixprice", SqlDbType.Float, 0, ParameterDirection.Input, this._fixprice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sellprice", SqlDbType.Float, 0, ParameterDirection.Input, this._sellprice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 3, ParameterDirection.Input, this._currcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@trffclass", SqlDbType.VarChar, 4, ParameterDirection.Input, this._trffclass);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@newfrom", SqlDbType.Int, 0, ParameterDirection.Input, this._newfrom);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@newto", SqlDbType.Int, 0, ParameterDirection.Input, this._newto);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_account_activation");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Insert, false)]
		public bool ActivateLine(int __resellerid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetRPCPConnectionString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@customerid", SqlDbType.Int, 0, ParameterDirection.Input, this._customerid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@resellerid", SqlDbType.Int, 0, ParameterDirection.Input, this._resellerid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, this._telcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, this._custcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, this._batchcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sfrom", SqlDbType.Int, 0, ParameterDirection.Input, this._sfrom);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sto", SqlDbType.Int, 0, ParameterDirection.Input, this._sto);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "line_activation");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Insert, false)]
		public bool GenerateAccount(int __resellerid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, this._telcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, this._sitecode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, this._custcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardid", SqlDbType.Int, 0, ParameterDirection.Input, this._cardid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@value", SqlDbType.Float, 0, ParameterDirection.Input, this._value);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@daysvalid", SqlDbType.Int, 0, ParameterDirection.Input, this._daysvalid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 3, ParameterDirection.Input, this._currcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@trffclass", SqlDbType.VarChar, 4, ParameterDirection.Input, this._trffclass);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@startdate", SqlDbType.DateTime, 0, ParameterDirection.Input, this._startdate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@enddate", SqlDbType.DateTime, 0, ParameterDirection.Input, this._enddate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@login", SqlDbType.VarChar, 25, ParameterDirection.Input, this._login);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@langcode", SqlDbType.VarChar, 8, ParameterDirection.Input, this._langcode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_account_generate");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<IListSource> theList)
		{
			DataTable dataTable = new DataTable();
			dataTable.Load(returnData);
			theList.Add(dataTable);
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<ESPAccountActivation> theList)
		{
			while (returnData.Read())
			{
				ESPAccountActivation eSPAccountActivation = new ESPAccountActivation(Convert.ToString(returnData["custcode"]), new int?((int)returnData["batchcode"]), new int?((int)returnData["sfrom"]), new int?((int)returnData["sto"]), new DateTime?(Convert.ToDateTime(returnData["expdate"])), new float?((float)((double)returnData["fixprice"])), Convert.ToString(returnData["currcode"]), Convert.ToString(returnData["trffclass"]), new int?((int)returnData["daysvalid"]), new DateTime?(Convert.ToDateTime(returnData["loadingdate"])), Convert.ToString(returnData["cardname"]));
				theList.Add(eSPAccountActivation);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<ESPAccountActivation> GetAll(int __resellerid, int __cardid)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardid", SqlDbType.Int, 0, ParameterDirection.Input, __cardid);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_loadedacc_list");
			List<ESPAccountActivation> eSPAccountActivations = new List<ESPAccountActivation>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPAccountActivation>(sqlCommand, new GenerateListFromReader<ESPAccountActivation>(ESPAccountActivation.GenerateListFromReader<ESPAccountActivation>), ref eSPAccountActivations);
			return eSPAccountActivations;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static ESPAccountActivation GetById(int __resellerid, string __custcode, int? __batchcode, int? __sfrom, int? __sto, int? __cardid)
		{
			if (__resellerid < 0)
			{
				throw new ArgumentOutOfRangeException("resellerid");
			}
			if (string.IsNullOrEmpty(__custcode))
			{
				throw new ArgumentOutOfRangeException("custcode");
			}
			if (__batchcode.HasValue)
			{
				int? _Batchcode = __batchcode;
				if ((_Batchcode.GetValueOrDefault() >= 0 ? true : !_Batchcode.HasValue))
				{
					if (__sfrom.HasValue)
					{
						int? _Sfrom = __sfrom;
						if ((_Sfrom.GetValueOrDefault() >= 0 ? true : !_Sfrom.HasValue))
						{
							if (__sto.HasValue)
							{
								int? _Sto = __sto;
								if ((_Sto.GetValueOrDefault() >= 0 ? true : !_Sto.HasValue))
								{
									if (__cardid.HasValue)
									{
										int? _Cardid = __cardid;
										if ((_Cardid.GetValueOrDefault() >= 0 ? true : !_Cardid.HasValue))
										{
											SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
											SqlCommand sqlCommand = new SqlCommand();
											sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, __custcode);
											sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, __batchcode);
											sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sfrom", SqlDbType.Int, 0, ParameterDirection.Input, __sfrom);
											sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sto", SqlDbType.Int, 0, ParameterDirection.Input, __sto);
											sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardid", SqlDbType.Int, 0, ParameterDirection.Input, __cardid);
											sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_loadedacc_getdetail");
											List<ESPAccountActivation> eSPAccountActivations = new List<ESPAccountActivation>();
											sQLDataAccessLayer.ExecuteReaderCmd<ESPAccountActivation>(sqlCommand, new GenerateListFromReader<ESPAccountActivation>(ESPAccountActivation.GenerateListFromReader<ESPAccountActivation>), ref eSPAccountActivations);
											if (eSPAccountActivations.Count <= 0)
											{
												return null;
											}
											return eSPAccountActivations[0];
										}
									}
									throw new ArgumentOutOfRangeException("cardid");
								}
							}
							throw new ArgumentOutOfRangeException("sto");
						}
					}
					throw new ArgumentOutOfRangeException("sfrom");
				}
			}
			throw new ArgumentOutOfRangeException("batchcode");
		}
	}
}