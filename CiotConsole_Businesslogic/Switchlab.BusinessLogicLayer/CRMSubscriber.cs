using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMSubscriber
	{
		private const string SP_SUBSCRIBER_GET_BYID = "subs_view_byID";

		private const string SP_SUBSCRIBER_GET_BYLOGIN = "subs_view_bylogin";

		private const string SP_LIST_BY_LOGIN = "subs_search_bylogin";

		private const string SP_LIST_BY_NAME = "subs_search_byname";

		private const string SP_LIST_BY_CODE = "subs_search_bycode";

		private const string SP_CREATE = "subs_create1";

		private const string SP_CREATE_LINE = "subs_create_line";

		private const string SP_UPDATE = "subs_update";

		private const string SP_UPDATE_STATUS = "subs_update_status";

		private const string SP_LIST_BY_NAMESOURCE = "subs_search_bynamesource";

		private const string SP_LIST_BY_SOURCE = "subs_search_bysource";

		private const string SP_LIST_ALL_SUBSCRIBER_REG = "subs_reg_listall";

		private const string SP_LIST_SUBSCRIBER_REG_BY_DATERANGE = "subs_reg_listby_daterange";

		private const string SP_LIST_SUBSCRIBER_REG_BY_STATUS = "subs_reg_listby_status";

		private const string SP_UPDATE_REGISTRATION_STATUS = "subs_reg_update_status";

		private const string SP_CHECK_SSN = "check_SSN";

		private const string SP_GET_SSN = "subs_reg_view";

		private const string SP_VIEW_SSN = "Get_SSN";

		private const string SP_GET_IMSI = "Get_IMSI";

		private const string SP_CHECK_REG = "sp_CheckIsRegistered";

		private const string SP_UPDATENORWAY = "subs_updateNorway";

		private string _smobileno;

		private string _siccid;

		private int _isubscriberid;

		private int _isubscribertype;

		private string _slogin;

		private string _sconfirmcode;

		private int _iplanid;

		private string _stelcocode;

		private string _scustcode;

		private int _ibatchcode;

		private int _iserialcode;

		private string _stitle;

		private string _sfirstname;

		private string _slastname;

		private string _shouseno;

		private string _saddress1;

		private string _saddress2;

		private string _scity;

		private string _spostcode;

		private string _sstate;

		private string _scountrycode;

		private string _stelephone;

		private string _smobilephone;

		private string _sfax;

		private string _semail;

		private DateTime _dtbirthdate;

		private string _ssecurityquestion;

		private string _ssecurityanswer;

		private string _ssubscribertypename;

		private int _iregistrationid;

		private int _iregistrationstatus;

		private string _sregistrationstatusname;

		private string _ssourcereg;

		private DateTime _dtrequestdate;

		private DateTime _dtauthdate;

		private string _simsi;

		private string _scontent_tag;

		private string _sauth_reg_by;

		private string _socialSecurityNo;

		public string Address1
		{
			get
			{
				if (this._saddress1 == null)
				{
					return string.Empty;
				}
				return this._saddress1;
			}
			set
			{
				this._saddress1 = value;
			}
		}

		public string Address2
		{
			get
			{
				if (this._saddress2 == null)
				{
					return string.Empty;
				}
				return this._saddress2;
			}
			set
			{
				this._saddress2 = value;
			}
		}

		public string AuthenticateRegistrationBy
		{
			get
			{
				if (this._sauth_reg_by == null)
				{
					return string.Empty;
				}
				return this._sauth_reg_by;
			}
			set
			{
				this._sauth_reg_by = value;
			}
		}

		public DateTime AuthenticationDate
		{
			get
			{
				return this._dtauthdate;
			}
			set
			{
				this._dtauthdate = value;
			}
		}

		public int BatchCode
		{
			get
			{
				return this._ibatchcode;
			}
			set
			{
				this._ibatchcode = value;
			}
		}

		public DateTime BirthDate
		{
			get
			{
				return this._dtbirthdate;
			}
			set
			{
				this._dtbirthdate = value;
			}
		}

		public string City
		{
			get
			{
				if (this._scity == null)
				{
					return string.Empty;
				}
				return this._scity;
			}
			set
			{
				this._scity = value;
			}
		}

		public string ConfirmCode
		{
			get
			{
				if (this._sconfirmcode == null)
				{
					return string.Empty;
				}
				return this._sconfirmcode;
			}
			set
			{
				this._sconfirmcode = value;
			}
		}

		public string ContentTag
		{
			get
			{
				if (this._scontent_tag == null)
				{
					return string.Empty;
				}
				return this._scontent_tag;
			}
			set
			{
				this._scontent_tag = value;
			}
		}

		public string CountryCode
		{
			get
			{
				if (this._scountrycode == null)
				{
					return string.Empty;
				}
				return this._scountrycode;
			}
			set
			{
				this._scountrycode = value;
			}
		}

		public string CustomerCode
		{
			get
			{
				if (this._scustcode == null)
				{
					return string.Empty;
				}
				return this._scustcode;
			}
			set
			{
				this._scustcode = value;
			}
		}

		public string Email
		{
			get
			{
				if (this._semail == null)
				{
					return string.Empty;
				}
				return this._semail;
			}
			set
			{
				this._semail = value;
			}
		}

		public string Fax
		{
			get
			{
				if (this._sfax == null)
				{
					return string.Empty;
				}
				return this._sfax;
			}
			set
			{
				this._sfax = value;
			}
		}

		public string FirstName
		{
			get
			{
				if (this._sfirstname == null)
				{
					return string.Empty;
				}
				return this._sfirstname;
			}
			set
			{
				this._sfirstname = value;
			}
		}

		public string FullName
		{
			get
			{
				if (string.Concat(this._sfirstname, " ", this._slastname) == null)
				{
					return string.Empty;
				}
				return string.Concat(this._sfirstname, " ", this._slastname);
			}
			set
			{
				this.FullName = value;
			}
		}

		public string HouseNumber
		{
			get
			{
				if (this._shouseno == null)
				{
					return string.Empty;
				}
				return this._shouseno;
			}
			set
			{
				this._shouseno = value;
			}
		}

		public string ICCID
		{
			get
			{
				if (this._siccid == null)
				{
					return string.Empty;
				}
				return this._siccid;
			}
			set
			{
				this._siccid = value;
			}
		}

		public string IMSI
		{
			get
			{
				if (this._simsi == null)
				{
					return string.Empty;
				}
				return this._simsi;
			}
			set
			{
				this._simsi = value;
			}
		}

		public string LastName
		{
			get
			{
				if (this._slastname == null)
				{
					return string.Empty;
				}
				return this._slastname;
			}
			set
			{
				this._slastname = value;
			}
		}

		public string Login
		{
			get
			{
				if (this._slogin == null)
				{
					return string.Empty;
				}
				return this._slogin;
			}
			set
			{
				this._slogin = value;
			}
		}

		public string MobileNo
		{
			get
			{
				if (this._smobileno == null)
				{
					return string.Empty;
				}
				return this._smobileno;
			}
			set
			{
				this._smobileno = value;
			}
		}

		public string MobilePhone
		{
			get
			{
				if (this._smobilephone == null)
				{
					return string.Empty;
				}
				return this._smobilephone;
			}
			set
			{
				this._smobilephone = value;
			}
		}

		public string PostCode
		{
			get
			{
				if (this._spostcode == null)
				{
					return string.Empty;
				}
				return this._spostcode;
			}
			set
			{
				this._spostcode = value;
			}
		}

		public int RegistrationID
		{
			get
			{
				return this._iregistrationid;
			}
			set
			{
				this._iregistrationid = value;
			}
		}

		public int RegistrationStatus
		{
			get
			{
				return this._iregistrationstatus;
			}
			set
			{
				this._iregistrationstatus = value;
			}
		}

		public string RegistrationStatusName
		{
			get
			{
				if (this._sregistrationstatusname == null)
				{
					return string.Empty;
				}
				return this._sregistrationstatusname;
			}
			set
			{
				this._sregistrationstatusname = value;
			}
		}

		public DateTime RequestDate
		{
			get
			{
				return this._dtrequestdate;
			}
			set
			{
				this._dtrequestdate = value;
			}
		}

		public string SecurityAnswer
		{
			get
			{
				if (this._ssecurityanswer == null)
				{
					return string.Empty;
				}
				return this._ssecurityanswer;
			}
			set
			{
				this._ssecurityanswer = value;
			}
		}

		public string SecurityQuestion
		{
			get
			{
				if (this._ssecurityquestion == null)
				{
					return string.Empty;
				}
				return this._ssecurityquestion;
			}
			set
			{
				this._ssecurityquestion = value;
			}
		}

		public int SerialCode
		{
			get
			{
				return this._iserialcode;
			}
			set
			{
				this._iserialcode = value;
			}
		}

		public string Social_SecurityNo
		{
			get
			{
				if (this._socialSecurityNo == null)
				{
					return string.Empty;
				}
				return this._socialSecurityNo;
			}
			set
			{
				this._socialSecurityNo = value;
			}
		}

		public string SourceRegistration
		{
			get
			{
				if (this._ssourcereg == null)
				{
					return string.Empty;
				}
				return this._ssourcereg;
			}
			set
			{
				this._ssourcereg = value;
			}
		}

		public string State
		{
			get
			{
				if (this._sstate == null)
				{
					return string.Empty;
				}
				return this._sstate;
			}
			set
			{
				this._sstate = value;
			}
		}

		public int SubscriberID
		{
			get
			{
				return this._isubscriberid;
			}
			set
			{
				this._isubscriberid = value;
			}
		}

		public int SubscriberType
		{
			get
			{
				return this._isubscribertype;
			}
			set
			{
				this._isubscribertype = value;
			}
		}

		public string SubscriberTypeName
		{
			get
			{
				if (this._ssubscribertypename == null)
				{
					return string.Empty;
				}
				return this._ssubscribertypename;
			}
			set
			{
				this._ssubscribertypename = value;
			}
		}

		public string TelcoCode
		{
			get
			{
				if (this._stelcocode == null)
				{
					return string.Empty;
				}
				return this._stelcocode;
			}
			set
			{
				this._stelcocode = value;
			}
		}

		public string Telephone
		{
			get
			{
				if (this._stelephone == null)
				{
					return string.Empty;
				}
				return this._stelephone;
			}
			set
			{
				this._stelephone = value;
			}
		}

		public string Title
		{
			get
			{
				if (this._stitle == null)
				{
					return string.Empty;
				}
				return this._stitle;
			}
			set
			{
				this._stitle = value;
			}
		}

		public CRMSubscriber()
		{
		}

		public CRMSubscriber(int iSubscriberID, string sLogin, string sConfirmCode, string sTitle, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sPostCode, string sState, string sCountryCode, string sTelephone, string sMobilePhone, string sFax, string sEmail, DateTime dtBirthDate, string sSecurityQuestion, string sSecurityAnswer, int iSubscriberType, string sSubscriberTypeName, int iPlanID, string sTelcoCode, string sCustCode, int iBatchCode, int iSerialCode)
		{
			this._isubscriberid = iSubscriberID;
			this._slogin = sLogin;
			this._sconfirmcode = sConfirmCode;
			this._stitle = sTitle;
			this._sfirstname = sFirstName;
			this._slastname = sLastName;
			this._shouseno = sHouseNo;
			this._saddress1 = sAddress1;
			this._saddress2 = sAddress2;
			this._scity = sCity;
			this._spostcode = sPostCode;
			this._sstate = sState;
			this._scountrycode = sCountryCode;
			this._stelephone = sTelephone;
			this._smobilephone = sMobilePhone;
			this._sfax = sFax;
			this._semail = sEmail;
			this._dtbirthdate = dtBirthDate;
			this._ssecurityquestion = sSecurityQuestion;
			this._ssecurityanswer = sSecurityAnswer;
			this._isubscribertype = iSubscriberType;
			this._ssubscribertypename = sSubscriberTypeName;
			this._iplanid = iPlanID;
			this._stelcocode = sTelcoCode;
			this._scustcode = sCustCode;
			this._ibatchcode = iBatchCode;
			this._iserialcode = iSerialCode;
		}

		public CRMSubscriber(int iSubscriberID, string sLogin, string sConfirmCode, string sTitle, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sPostCode, string sState, string sCountryCode, string sTelephone, string sMobilePhone, string sFax, string sEmail, DateTime dtBirthDate, string sSecurityQuestion, string sSecurityAnswer, int iSubscriberType, string sSubscriberTypeName, int iPlanID, string sTelcoCode, string sCustCode, int iBatchCode, int iSerialCode, string sMobileNo, string sICCID) : this(iSubscriberID, string.Empty, string.Empty, sTitle, sFirstName, sLastName, sHouseNo, sAddress1, sAddress2, sCity, sPostCode, sState, sCountryCode, sTelephone, sMobilePhone, sFax, sEmail, dtBirthDate, sSecurityQuestion, sSecurityAnswer, iSubscriberType, sSubscriberTypeName, iPlanID, sTelcoCode, sCustCode, iBatchCode, iSerialCode)
		{
			this._smobileno = sMobileNo;
			this._siccid = sICCID;
		}

		public CRMSubscriber(int iSubscriberID, string sTitle, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sPostCode, string sState, string sCountryCode, string sTelephone, string sMobilePhone, string sFax, string sEmail, DateTime dtBirthDate, string sSecurityQuestion, string sSecurityAnswer, int iSubscriberType, string sSubscriberTypeName) : this(iSubscriberID, string.Empty, string.Empty, sTitle, sFirstName, sLastName, sHouseNo, sAddress1, sAddress2, sCity, sPostCode, sState, sCountryCode, sTelephone, sMobilePhone, sFax, sEmail, dtBirthDate, sSecurityQuestion, sSecurityAnswer, iSubscriberType, sSubscriberTypeName, 0, string.Empty, string.Empty, 0, 0)
		{
		}

		public CRMSubscriber(int iSubscriberID, string sTitle, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sPostCode, string sState, string sCountryCode, string sTelephone, string sMobilePhone, string sFax, string sEmail, DateTime dtBirthDate, string sSecurityQuestion, string sSecurityAnswer, int iSubscriberType, string sSubscriberTypeName, string sMobileNo, string sICCID) : this(iSubscriberID, sTitle, sFirstName, sLastName, sHouseNo, sAddress1, sAddress2, sCity, sPostCode, sState, sCountryCode, sTelephone, sMobilePhone, sFax, sEmail, dtBirthDate, sSecurityQuestion, sSecurityAnswer, iSubscriberType, sSubscriberTypeName)
		{
			this._smobileno = sMobileNo;
			this._siccid = sICCID;
		}

		public CRMSubscriber(int iRegistrationID, string sIMSI, string sMobileNo, int iRegistrationStatus, string sRegistrationStatusName, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sCity, string sPostCode, string sCountryCode, string sSourceReg, DateTime dtRequestDate, DateTime dtAuthDate, string sContentTag, string sAuthenticateRegistrationBy, string sCountryName) : this(0, string.Empty, sFirstName, sLastName, sHouseNo, sAddress1, string.Empty, sCity, sPostCode, string.Empty, sCountryCode, string.Empty, string.Empty, string.Empty, string.Empty, DateTime.MinValue, string.Empty, string.Empty, 0, string.Empty, sMobileNo, string.Empty)
		{
			this._simsi = sIMSI;
			this._iregistrationid = iRegistrationID;
			this._iregistrationstatus = iRegistrationStatus;
			this._sregistrationstatusname = sRegistrationStatusName;
			this._ssourcereg = sSourceReg;
			this._dtrequestdate = dtRequestDate;
			this._dtauthdate = dtAuthDate;
			this._scontent_tag = sContentTag;
			this._sauth_reg_by = sAuthenticateRegistrationBy;
		}

		public bool CheckSSN(string strAddr, string strSSN)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@Address1", SqlDbType.VarChar, 50, ParameterDirection.Input, strAddr);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SSN", SqlDbType.VarChar, 16, ParameterDirection.Input, strSSN);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "check_SSN");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return false;
			}
			return GeneralConverter.ToBoolean(obj);
		}

		public static int CreateSubscriber(string sTitle, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sPostCode, string sState, string sCountryCode, string sTelephone, string sMobilePhone, string sFax, string sEmail, string sBirthDate, string sSecurityQuestion, string sSecurityAnswer, int iSubscriberType, string sSsn)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 16, ParameterDirection.Input, sTitle);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 30, ParameterDirection.Input, sFirstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastname", SqlDbType.VarChar, 30, ParameterDirection.Input, sLastName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@houseno", SqlDbType.VarChar, 50, ParameterDirection.Input, sHouseNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address1", SqlDbType.VarChar, 50, ParameterDirection.Input, sAddress1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address2", SqlDbType.VarChar, 50, ParameterDirection.Input, sAddress2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@city", SqlDbType.VarChar, 20, ParameterDirection.Input, sCity);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@postcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sPostCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@state", SqlDbType.VarChar, 20, ParameterDirection.Input, sState);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countrycode", SqlDbType.Char, 2, ParameterDirection.Input, sCountryCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telephone", SqlDbType.VarChar, 15, ParameterDirection.Input, sTelephone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobilephone", SqlDbType.VarChar, 15, ParameterDirection.Input, sMobilePhone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fax", SqlDbType.VarChar, 15, ParameterDirection.Input, sFax);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@email", SqlDbType.VarChar, 48, ParameterDirection.Input, sEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@birthdate", SqlDbType.VarChar, 25, ParameterDirection.Input, sBirthDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@question", SqlDbType.VarChar, 100, ParameterDirection.Input, sSecurityQuestion);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@answer", SqlDbType.VarChar, 50, ParameterDirection.Input, sSecurityAnswer);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscribertype", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ssn", SqlDbType.VarChar, 16, ParameterDirection.Input, sSsn);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_create1");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return -1;
			}
			return GeneralConverter.ToInt32(obj);
		}

		public static int CreateSubscriberAndLine(string sTitle, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sPostCode, string sState, string sCountryCode, string sTelephone, string sMobilePhone, string sFax, string sEmail, string sBirthDate, string sSecurityQuestion, string sSecurityAnswer, int iSubscriberType, string sMobileNo, string sICCID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 16, ParameterDirection.Input, sTitle);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 30, ParameterDirection.Input, sFirstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastname", SqlDbType.VarChar, 30, ParameterDirection.Input, sLastName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@houseno", SqlDbType.VarChar, 50, ParameterDirection.Input, sHouseNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address1", SqlDbType.VarChar, 50, ParameterDirection.Input, sAddress1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address2", SqlDbType.VarChar, 50, ParameterDirection.Input, sAddress2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@city", SqlDbType.VarChar, 20, ParameterDirection.Input, sCity);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@postcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sPostCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@state", SqlDbType.VarChar, 20, ParameterDirection.Input, sState);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countrycode", SqlDbType.Char, 2, ParameterDirection.Input, sCountryCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telephone", SqlDbType.VarChar, 15, ParameterDirection.Input, sTelephone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobilephone", SqlDbType.VarChar, 15, ParameterDirection.Input, sMobilePhone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fax", SqlDbType.VarChar, 15, ParameterDirection.Input, sFax);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@email", SqlDbType.VarChar, 48, ParameterDirection.Input, sEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@birthdate", SqlDbType.VarChar, 25, ParameterDirection.Input, sBirthDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@question", SqlDbType.VarChar, 100, ParameterDirection.Input, sSecurityQuestion);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@answer", SqlDbType.VarChar, 50, ParameterDirection.Input, sSecurityAnswer);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscribertype", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileno", SqlDbType.VarChar, 16, ParameterDirection.Input, sMobileNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, sICCID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_create_line");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return -1;
			}
			return GeneralConverter.ToInt32(obj);
		}

		private static void GenerateSSN<T>(IDataReader Reader, ref List<CRMSubscriber> arl)
		{
			while (Reader.Read())
			{
				CRMSubscriber cRMSubscriber = new CRMSubscriber()
				{
					FirstName = GeneralConverter.ToString(Reader["firstname"]),
					LastName = GeneralConverter.ToString(Reader["lastname"]),
					HouseNumber = GeneralConverter.ToString(Reader["houseno"]),
					Address1 = GeneralConverter.ToString(Reader["address1"]),
					City = GeneralConverter.ToString(Reader["city"]),
					PostCode = GeneralConverter.ToString(Reader["postcode"]),
					CountryCode = GeneralConverter.ToString(Reader["countrycode"]),
					MobileNo = GeneralConverter.ToString(Reader["msisdn"]),
					Social_SecurityNo = GeneralConverter.ToString(Reader["social_securityno"])
				};
				arl.Add(cRMSubscriber);
			}
		}

		private static void GenerateSubscriberFromReader<T>(IDataReader returnData, ref List<CRMSubscriber> listNew)
		{
			while (returnData.Read())
			{
				CRMSubscriber cRMSubscriber = new CRMSubscriber()
				{
					SubscriberID = GeneralConverter.ToInt32(returnData["subscriberid"]),
					Title = GeneralConverter.ToString(returnData["title"]),
					FirstName = GeneralConverter.ToString(returnData["firstname"]),
					LastName = GeneralConverter.ToString(returnData["lastname"]),
					HouseNumber = GeneralConverter.ToString(returnData["houseno"]),
					Address1 = GeneralConverter.ToString(returnData["address1"]),
					Address2 = GeneralConverter.ToString(returnData["address2"]),
					City = GeneralConverter.ToString(returnData["city"]),
					PostCode = GeneralConverter.ToString(returnData["postcode"]),
					State = GeneralConverter.ToString(returnData["state"]),
					CountryCode = GeneralConverter.ToString(returnData["countrycode"]),
					Telephone = GeneralConverter.ToString(returnData["telephone"]),
					MobilePhone = GeneralConverter.ToString(returnData["mobilephone"]),
					Fax = GeneralConverter.ToString(returnData["fax"]),
					Email = GeneralConverter.ToString(returnData["email"]),
					BirthDate = GeneralConverter.ToDateTime(returnData["birthdate"]),
					SecurityQuestion = GeneralConverter.ToString(returnData["securityquestion"]),
					SecurityAnswer = GeneralConverter.ToString(returnData["securityanswer"]),
					SubscriberType = GeneralConverter.ToInt32(returnData["subscribertype"]),
					SubscriberTypeName = GeneralConverter.ToString(returnData["subscribertypename"]),
					Social_SecurityNo = GeneralConverter.ToString(returnData["social_securityno"])
				};
				listNew.Add(cRMSubscriber);
			}
		}

		private static void GenerateSubscriberListFromReader<T>(IDataReader returnData, ref List<CRMSubscriber> listNew)
		{
			while (returnData.Read())
			{
				CRMSubscriber cRMSubscriber = new CRMSubscriber(GeneralConverter.ToInt32(returnData["subscriberid"]), GeneralConverter.ToString(returnData["login"]), GeneralConverter.ToString(returnData["confirmcode"]), GeneralConverter.ToString(returnData["title"]), GeneralConverter.ToString(returnData["firstname"]), GeneralConverter.ToString(returnData["lastname"]), GeneralConverter.ToString(returnData["houseno"]), GeneralConverter.ToString(returnData["address1"]), GeneralConverter.ToString(returnData["address2"]), GeneralConverter.ToString(returnData["city"]), GeneralConverter.ToString(returnData["postcode"]), GeneralConverter.ToString(returnData["state"]), GeneralConverter.ToString(returnData["countrycode"]), GeneralConverter.ToString(returnData["telephone"]), GeneralConverter.ToString(returnData["mobilephone"]), GeneralConverter.ToString(returnData["fax"]), GeneralConverter.ToString(returnData["email"]), GeneralConverter.ToDateTime(returnData["birthdate"]), GeneralConverter.ToString(returnData["securityquestion"]), GeneralConverter.ToString(returnData["securityanswer"]), GeneralConverter.ToInt32(returnData["subscribertype"]), GeneralConverter.ToString(returnData["subscribertypename"]), GeneralConverter.ToInt32(returnData["planid"]), GeneralConverter.ToString(returnData["telcocode"]), GeneralConverter.ToString(returnData["custcode"]), GeneralConverter.ToInt32(returnData["batchcode"]), GeneralConverter.ToInt32(returnData["serialcode"]));
				listNew.Add(cRMSubscriber);
			}
		}

		private static void GenerateSubscriberRegistrationListFromReader<T>(IDataReader returnData, ref List<CRMSubscriber> listNew)
		{
			while (returnData.Read())
			{
				CRMSubscriber cRMSubscriber = new CRMSubscriber(GeneralConverter.ToInt32(returnData["registrationid"]), GeneralConverter.ToString(returnData["imsi"]), GeneralConverter.ToString(returnData["msisdn"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToString(returnData["statusname"]), GeneralConverter.ToString(returnData["firstname"]), GeneralConverter.ToString(returnData["lastname"]), GeneralConverter.ToString(returnData["houseno"]), GeneralConverter.ToString(returnData["address1"]), GeneralConverter.ToString(returnData["city"]), GeneralConverter.ToString(returnData["postcode"]), GeneralConverter.ToString(returnData["countrycode"]), GeneralConverter.ToString(returnData["sourcereg"]), GeneralConverter.ToDateTime(returnData["requestdate"]), GeneralConverter.ToDateTime(returnData["authdate"]), GeneralConverter.ToString(returnData["content_tag"]), GeneralConverter.ToString(returnData["authby"]), GeneralConverter.ToString(returnData["countryname"]));
				listNew.Add(cRMSubscriber);
			}
		}

		public static List<CRMSubscriber> getDetailFromSSN(string strSSN)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@SSN", SqlDbType.VarChar, 16, ParameterDirection.Input, strSSN);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_reg_view");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSSN<CRMSubscriber>), ref cRMSubscribers);
			return cRMSubscribers;
		}

		public string GETIMSI(string strMobile)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msisdn", SqlDbType.VarChar, 15, ParameterDirection.Input, strMobile);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Get_IMSI");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return string.Empty;
			}
			return GeneralConverter.ToString(obj);
		}

		public static CRMSubscriber GetSubscriberByICCID(string sICCID, string sTelcoCode)
		{
			ESPMVNOAccount mVNOAccountByICCID = ESPMVNOAccount.GetMVNOAccountByICCID(sICCID);
			if (mVNOAccountByICCID == null)
			{
				return null;
			}
			CRMSubscriber mobileNo = CRMSubscriber.SearchByCode(sTelcoCode, mVNOAccountByICCID.CustomerCode, mVNOAccountByICCID.BatchCode, mVNOAccountByICCID.SerialCode);
			if (mobileNo != null)
			{
				mobileNo.MobileNo = mVNOAccountByICCID.MobileNo;
				mobileNo.ICCID = mVNOAccountByICCID.ICCID;
				return mobileNo;
			}
			string str = "";
			if (sICCID.StartsWith("8946"))
			{
				str = "SE";
			}
			else if (sICCID.StartsWith("8947"))
			{
				str = "NO";
			}
			else if (sICCID.StartsWith("8945"))
			{
				str = "DK";
			}
			else if (sICCID.StartsWith("8931"))
			{
				str = "NL";
			}
			else if (sICCID.StartsWith("8943"))
			{
				str = "AU";
			}
			int num = CRMSubscriber.CreateSubscriberAndLine("", mVNOAccountByICCID.MobileNo, "", "", "", "", "", "", "", str, "", mVNOAccountByICCID.MobileNo, "", "", "", "", "", 1, mVNOAccountByICCID.MobileNo, mVNOAccountByICCID.ICCID);
			if (num <= 0)
			{
				return null;
			}
			mobileNo = CRMSubscriber.GetSubscriberById(num);
			mobileNo.MobileNo = mVNOAccountByICCID.MobileNo;
			mobileNo.ICCID = mVNOAccountByICCID.ICCID;
			object[] objArray = new object[] { sTelcoCode, mVNOAccountByICCID.CustomerCode.Substring(4, 4), null, null };
			int batchCode = mVNOAccountByICCID.BatchCode;
			objArray[2] = batchCode.ToString().PadLeft(2, '0');
			int serialCode = mVNOAccountByICCID.SerialCode;
			objArray[3] = serialCode.ToString().PadLeft(4, '0');
			mobileNo.Login = string.Format("{0}{1}{2}{3}", objArray);
			return mobileNo;
		}

		public static CRMSubscriber GetSubscriberById(int iSubscriberID)
		{
			if (iSubscriberID < 0)
			{
				throw new ArgumentOutOfRangeException("iSubscriberID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscriberid", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_view_byID");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberFromReader<CRMSubscriber>), ref cRMSubscribers);
			if (cRMSubscribers.Count != 1)
			{
				return null;
			}
			return cRMSubscribers[0];
		}

		public static CRMSubscriber GetSubscriberByLogin(string sLogin)
		{
			if (string.IsNullOrEmpty(sLogin))
			{
				throw new ArgumentOutOfRangeException("sLogin");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@login", SqlDbType.VarChar, 20, ParameterDirection.Input, sLogin);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_view_bylogin");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberListFromReader<CRMSubscriber>), ref cRMSubscribers);
			if (cRMSubscribers.Count != 1)
			{
				return null;
			}
			return cRMSubscribers[0];
		}

		public static CRMSubscriber GetSubscriberByMobileNo(string sMobileNo, string sTelcoCode)
		{
			ESPMVNOAccount mVNOAccountByMobileNo = ESPMVNOAccount.GetMVNOAccountByMobileNo(sMobileNo);
			if (mVNOAccountByMobileNo == null)
			{
				return null;
			}
			CRMSubscriber mobileNo = CRMSubscriber.SearchByCode(sTelcoCode, mVNOAccountByMobileNo.CustomerCode, mVNOAccountByMobileNo.BatchCode, mVNOAccountByMobileNo.SerialCode);
			if (mobileNo != null)
			{
				mobileNo.MobileNo = mVNOAccountByMobileNo.MobileNo;
				mobileNo.ICCID = mVNOAccountByMobileNo.ICCID;
				return mobileNo;
			}
			string str = "";
			if (sMobileNo.StartsWith("46"))
			{
				str = "SE";
			}
			else if (sMobileNo.StartsWith("47"))
			{
				str = "NO";
			}
			else if (sMobileNo.StartsWith("45"))
			{
				str = "DK";
			}
			else if (sMobileNo.StartsWith("31"))
			{
				str = "NL";
			}
			else if (sMobileNo.StartsWith("43"))
			{
				str = "AU";
			}
			int num = CRMSubscriber.CreateSubscriberAndLine("", mVNOAccountByMobileNo.MobileNo, "", "", "", "", "", "", "", str, "", mVNOAccountByMobileNo.MobileNo, "", "", "", "", "", 1, mVNOAccountByMobileNo.MobileNo, mVNOAccountByMobileNo.ICCID);
			if (num <= 0)
			{
				return null;
			}
			mobileNo = CRMSubscriber.GetSubscriberById(num);
			mobileNo.MobileNo = mVNOAccountByMobileNo.MobileNo;
			mobileNo.ICCID = mVNOAccountByMobileNo.ICCID;
			object[] objArray = new object[] { sTelcoCode, mVNOAccountByMobileNo.CustomerCode.Substring(4, 4), null, null };
			int batchCode = mVNOAccountByMobileNo.BatchCode;
			objArray[2] = batchCode.ToString().PadLeft(2, '0');
			int serialCode = mVNOAccountByMobileNo.SerialCode;
			objArray[3] = serialCode.ToString().PadLeft(4, '0');
			mobileNo.Login = string.Format("{0}{1}{2}{3}", objArray);
			return mobileNo;
		}

		public bool IsCheckSubscriber(string strMobile)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileNo", SqlDbType.VarChar, 15, ParameterDirection.Input, strMobile);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sp_CheckIsRegistered");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return false;
			}
			return GeneralConverter.ToBoolean(obj);
		}

		public static List<CRMSubscriber> ListAllSubscriberReg()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_reg_listall");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberRegistrationListFromReader<CRMSubscriber>), ref cRMSubscribers);
			return cRMSubscribers;
		}

		public static List<CRMSubscriber> ListSubscriberRegByDateRange(DateTime dtStartDate, DateTime dtEndDate)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@startdate", SqlDbType.DateTime, 0, ParameterDirection.Input, dtStartDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@enddate", SqlDbType.DateTime, 0, ParameterDirection.Input, dtEndDate);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_reg_listby_daterange");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberRegistrationListFromReader<CRMSubscriber>), ref cRMSubscribers);
			return cRMSubscribers;
		}

		public static List<CRMSubscriber> ListSubscriberRegByRegStatus(int iRegistrationStatus)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, iRegistrationStatus);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_reg_listby_status");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberRegistrationListFromReader<CRMSubscriber>), ref cRMSubscribers);
			return cRMSubscribers;
		}

		public static CRMSubscriber SearchByCode(string sTelcocode, string sCustomerCode, int iBatchCode, int iSerialCode)
		{
			if (sTelcocode == string.Empty)
			{
				throw new ArgumentOutOfRangeException("TelcoCode");
			}
			if (sCustomerCode == string.Empty)
			{
				throw new ArgumentOutOfRangeException("CustomerCode");
			}
			if (iBatchCode < 0)
			{
				throw new ArgumentOutOfRangeException("BatchCode");
			}
			if (iSerialCode < 0)
			{
				throw new ArgumentOutOfRangeException("SerialCode");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, sTelcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sCustomerCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, iBatchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, iSerialCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_search_bycode");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberListFromReader<CRMSubscriber>), ref cRMSubscribers);
			if (cRMSubscribers.Count != 1)
			{
				return null;
			}
			return cRMSubscribers[0];
		}

		public static List<CRMSubscriber> SearchByLogin(string sLogin)
		{
			if (sLogin == string.Empty)
			{
				sLogin = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@login", SqlDbType.VarChar, 20, ParameterDirection.Input, sLogin);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_search_bylogin");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberListFromReader<CRMSubscriber>), ref cRMSubscribers);
			return cRMSubscribers;
		}

		public static List<CRMSubscriber> SearchByName(string sName)
		{
			if (sName == string.Empty)
			{
				sName = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 61, ParameterDirection.Input, sName);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_search_byname");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberListFromReader<CRMSubscriber>), ref cRMSubscribers);
			return cRMSubscribers;
		}

		public static List<CRMSubscriber> SearchByName(string sName, string Source)
		{
			if (sName == string.Empty)
			{
				sName = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 61, ParameterDirection.Input, sName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sourcereg", SqlDbType.VarChar, 32, ParameterDirection.Input, Source);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_search_bynamesource");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberListFromReader<CRMSubscriber>), ref cRMSubscribers);
			return cRMSubscribers;
		}

		public static List<CRMSubscriber> SearchBySourceRegistered(string Source)
		{
			if (Source == string.Empty || Source == "-")
			{
				Source = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sourcereg", SqlDbType.VarChar, 32, ParameterDirection.Input, Source);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_search_bysource");
			List<CRMSubscriber> cRMSubscribers = new List<CRMSubscriber>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMSubscriber>(sqlCommand, new GenerateListFromReader<CRMSubscriber>(CRMSubscriber.GenerateSubscriberListFromReader<CRMSubscriber>), ref cRMSubscribers);
			return cRMSubscribers;
		}

		public static CRMSubscriber SearchMvnoAndSubscriberByLogin(string sLogin)
		{
			if (string.IsNullOrEmpty(sLogin))
			{
				throw new ArgumentOutOfRangeException("sLogin");
			}
			CRMSubscriber subscriberByLogin = CRMSubscriber.GetSubscriberByLogin(sLogin);
			if (subscriberByLogin == null)
			{
				return null;
			}
			ESPMVNOAccount mVNOAccountByCode = ESPMVNOAccount.GetMVNOAccountByCode(subscriberByLogin.CustomerCode, subscriberByLogin.BatchCode, subscriberByLogin.SerialCode);
			if (mVNOAccountByCode != null)
			{
				subscriberByLogin.MobileNo = mVNOAccountByCode.MobileNo;
				subscriberByLogin.ICCID = mVNOAccountByCode.ICCID;
			}
			return subscriberByLogin;
		}

		public static List<CRMSubscriber> SearchMvnoAndSubscriberByName(string sName)
		{
			if (sName == string.Empty)
			{
				sName = "%";
			}
			List<CRMSubscriber> cRMSubscribers = CRMSubscriber.SearchByName(sName);
			if (cRMSubscribers == null)
			{
				return null;
			}
			List<CRMSubscriber> cRMSubscribers1 = new List<CRMSubscriber>();
			foreach (CRMSubscriber mobileNo in cRMSubscribers)
			{
				ESPMVNOAccount mVNOAccountByCode = ESPMVNOAccount.GetMVNOAccountByCode(mobileNo.CustomerCode, mobileNo.BatchCode, mobileNo.SerialCode);
				if (mVNOAccountByCode != null)
				{
					mobileNo.MobileNo = mVNOAccountByCode.MobileNo;
					mobileNo.ICCID = mVNOAccountByCode.ICCID;
				}
				cRMSubscribers1.Add(mobileNo);
			}
			return cRMSubscribers1;
		}

		public static List<CRMSubscriber> SearchMvnoAndSubscriberByName(string sName, string Source)
		{
			if (sName == string.Empty)
			{
				sName = "%";
			}
			List<CRMSubscriber> cRMSubscribers = CRMSubscriber.SearchByName(sName, Source);
			if (cRMSubscribers == null)
			{
				return null;
			}
			List<CRMSubscriber> cRMSubscribers1 = new List<CRMSubscriber>();
			foreach (CRMSubscriber mobileNo in cRMSubscribers)
			{
				ESPMVNOAccount mVNOAccountByCode = ESPMVNOAccount.GetMVNOAccountByCode(mobileNo.CustomerCode, mobileNo.BatchCode, mobileNo.SerialCode);
				if (mVNOAccountByCode != null)
				{
					mobileNo.MobileNo = mVNOAccountByCode.MobileNo;
					mobileNo.ICCID = mVNOAccountByCode.ICCID;
				}
				cRMSubscribers1.Add(mobileNo);
			}
			return cRMSubscribers1;
		}

		public static List<CRMSubscriber> SearchMvnoAndSubscriberBySourceRegistered(string Source)
		{
			if (Source == string.Empty || Source == "-")
			{
				Source = "%";
			}
			List<CRMSubscriber> cRMSubscribers = CRMSubscriber.SearchBySourceRegistered(Source);
			if (cRMSubscribers == null)
			{
				return null;
			}
			List<CRMSubscriber> cRMSubscribers1 = new List<CRMSubscriber>();
			foreach (CRMSubscriber mobileNo in cRMSubscribers)
			{
				ESPMVNOAccount mVNOAccountByCode = ESPMVNOAccount.GetMVNOAccountByCode(mobileNo.CustomerCode, mobileNo.BatchCode, mobileNo.SerialCode);
				if (mVNOAccountByCode != null)
				{
					mobileNo.MobileNo = mVNOAccountByCode.MobileNo;
					mobileNo.ICCID = mVNOAccountByCode.ICCID;
				}
				cRMSubscribers1.Add(mobileNo);
			}
			return cRMSubscribers1;
		}

		public static bool UpdateStatus(int iSubscriberId, int iSubscriberType)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscriberId", SqlDbType.VarChar, 16, ParameterDirection.Input, iSubscriberId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscribertype", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberType);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_update_status");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public static bool UpdateStatusSubscriberRegistration(int iRegistrationID, int iRegistrationStatus, string sAuthenticateRegBy)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@registrationid", SqlDbType.Int, 0, ParameterDirection.Input, iRegistrationID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@registrationstatus", SqlDbType.Int, 0, ParameterDirection.Input, iRegistrationStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@authby", SqlDbType.VarChar, 16, ParameterDirection.Input, sAuthenticateRegBy);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_reg_update_status");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public bool UpdateSubscriber(int iSubscriberID, string sTitle, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sPostCode, string sState, string sCountryCode, string sTelephone, string sMobilePhone, string sFax, string sEmail, string sBirthDate, string sSecurityQuestion, string sSecurityAnswer, int iSubscriberType, string sSocialSecurityNo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscriberid", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 16, ParameterDirection.Input, sTitle);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 30, ParameterDirection.Input, sFirstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastname", SqlDbType.VarChar, 30, ParameterDirection.Input, sLastName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@houseno", SqlDbType.VarChar, 50, ParameterDirection.Input, sHouseNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address1", SqlDbType.VarChar, 50, ParameterDirection.Input, sAddress1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address2", SqlDbType.VarChar, 50, ParameterDirection.Input, sAddress2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@city", SqlDbType.VarChar, 20, ParameterDirection.Input, sCity);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@postcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sPostCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@state", SqlDbType.VarChar, 20, ParameterDirection.Input, sState);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countrycode", SqlDbType.Char, 2, ParameterDirection.Input, sCountryCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telephone", SqlDbType.VarChar, 15, ParameterDirection.Input, sTelephone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobilephone", SqlDbType.VarChar, 15, ParameterDirection.Input, sMobilePhone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fax", SqlDbType.VarChar, 15, ParameterDirection.Input, sFax);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@email", SqlDbType.VarChar, 48, ParameterDirection.Input, sEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@birthdate", SqlDbType.VarChar, 25, ParameterDirection.Input, sBirthDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@question", SqlDbType.VarChar, 100, ParameterDirection.Input, sSecurityQuestion);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@answer", SqlDbType.VarChar, 50, ParameterDirection.Input, sSecurityAnswer);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscribertype", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberType);
			if (sCountryCode.Trim() != "NO")
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_update");
			}
			else
			{
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@socialsecurityno", SqlDbType.VarChar, 16, ParameterDirection.Input, sSocialSecurityNo);
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_updateNorway");
			}
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public bool UpdateSubscriber(int iSubscriberID, string sTitle, string sFirstName, string sLastName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sPostCode, string sState, string sCountryCode, string sTelephone, string sMobilePhone, string sFax, string sEmail, string sBirthDate, string sSecurityQuestion, string sSecurityAnswer, int iSubscriberType)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscriberid", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 16, ParameterDirection.Input, sTitle);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 30, ParameterDirection.Input, sFirstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastname", SqlDbType.VarChar, 30, ParameterDirection.Input, sLastName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@houseno", SqlDbType.VarChar, 50, ParameterDirection.Input, sHouseNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address1", SqlDbType.VarChar, 50, ParameterDirection.Input, sAddress1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address2", SqlDbType.VarChar, 50, ParameterDirection.Input, sAddress2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@city", SqlDbType.VarChar, 20, ParameterDirection.Input, sCity);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@postcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sPostCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@state", SqlDbType.VarChar, 20, ParameterDirection.Input, sState);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countrycode", SqlDbType.Char, 2, ParameterDirection.Input, sCountryCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telephone", SqlDbType.VarChar, 15, ParameterDirection.Input, sTelephone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobilephone", SqlDbType.VarChar, 15, ParameterDirection.Input, sMobilePhone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fax", SqlDbType.VarChar, 15, ParameterDirection.Input, sFax);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@email", SqlDbType.VarChar, 48, ParameterDirection.Input, sEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@birthdate", SqlDbType.VarChar, 25, ParameterDirection.Input, sBirthDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@question", SqlDbType.VarChar, 100, ParameterDirection.Input, sSecurityQuestion);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@answer", SqlDbType.VarChar, 50, ParameterDirection.Input, sSecurityAnswer);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscribertype", SqlDbType.Int, 0, ParameterDirection.Input, iSubscriberType);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "subs_update");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public string ViewSSN(string strMobile)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msisdn", SqlDbType.VarChar, 15, ParameterDirection.Input, strMobile);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Get_SSN");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return string.Empty;
			}
			return GeneralConverter.ToString(obj);
		}
	}
}