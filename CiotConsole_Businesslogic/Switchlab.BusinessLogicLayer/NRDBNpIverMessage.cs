using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class NRDBNpIverMessage : NRDBNpMessage
	{
		public NRDBNpIverMessage()
		{
		}

		public override bool Save(NRDBMessage message)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNRDBConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msgtype_id", SqlDbType.Int, 0, ParameterDirection.Input, message.TypeId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@caseId", SqlDbType.BigInt, 0, ParameterDirection.Input, message.CaseId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sequence", SqlDbType.Int, 0, ParameterDirection.Input, message.Sequence);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "send_NP_Message");
			return sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0;
		}
	}
}