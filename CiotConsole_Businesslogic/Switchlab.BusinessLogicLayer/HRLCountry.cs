using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class HRLCountry
	{
		private const string SP_COUNTRY = "hlr_getcountry";

		private string _sCountryCode;

		private string _sCountryName;

		public string sCountryCode
		{
			get
			{
				return this._sCountryCode;
			}
			set
			{
				this._sCountryCode = value;
			}
		}

		public string sCountryName
		{
			get
			{
				return this._sCountryName;
			}
			set
			{
				this._sCountryName = value;
			}
		}

		public HRLCountry()
		{
		}

		private static void GenerateCountryListFromReader<T>(IDataReader returnData, ref List<HRLCountry> lstCountry)
		{
			while (returnData.Read())
			{
				HRLCountry hRLCountry = new HRLCountry()
				{
					sCountryCode = GeneralConverter.ToString(returnData["countryCode"]).Trim(),
					sCountryName = GeneralConverter.ToString(returnData["name"]).Trim()
				};
				lstCountry.Add(hRLCountry);
			}
		}

		public static List<HRLCountry> GetLocationCode()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetHLRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "hlr_getcountry");
			List<HRLCountry> hRLCountries = new List<HRLCountry>();
			sQLDataAccessLayer.ExecuteReaderCmd<HRLCountry>(sqlCommand, new GenerateListFromReader<HRLCountry>(HRLCountry.GenerateCountryListFromReader<HRLCountry>), ref hRLCountries);
			return hRLCountries;
		}
	}
}