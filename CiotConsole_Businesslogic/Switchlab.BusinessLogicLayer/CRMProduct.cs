using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMProduct
	{
		private const string SP_LIST_BY_CODE = "product_search_bycode";

		private const string SP_LIST_BY_TYPE = "product_search_bytype";

		private const string SP_LIST_BY_NAME = "product_search_byname";

		private const string SP_LIST_BY_ID = "product_search_byid";

		private const string SP_LIST_ITEM = "product_list_item";

		private const string SP_UNSELECT_ITEM = "unselect_item";

		private const string SP_LIST_NUMBER = "select_number_available";

		private const string SP_SELECT_NUMBER = "select_number";

		private const string SP_CREATE = "product_create";

		private const string SP_UPDATE = "product_update";

		private const string SP_CREATE_ITEM = "item_create";

		private int _productid;

		private string _productcode;

		private string _productname;

		private string _productdesc;

		private double _price;

		private string _currcode;

		private string _keyname1;

		private string _keyname2;

		private string _keyitem1;

		private int _producttype;

		private int _stock;

		private int _itemid;

		private string _smallimageUrl;

		private string _fullimageUrl;

		public string CurrCode
		{
			get
			{
				if (this._currcode == null)
				{
					return string.Empty;
				}
				return this._currcode;
			}
		}

		public string FullImageUrl
		{
			get
			{
				if (this._fullimageUrl == null)
				{
					return string.Empty;
				}
				return this._fullimageUrl;
			}
		}

		public int ItemId
		{
			get
			{
				return this._itemid;
			}
		}

		public string KeyItem1
		{
			get
			{
				if (this._keyitem1 == null)
				{
					return string.Empty;
				}
				return this._keyitem1;
			}
		}

		public string KeyName1
		{
			get
			{
				if (this._keyname1 == null)
				{
					return string.Empty;
				}
				return this._keyname1;
			}
		}

		public string KeyName2
		{
			get
			{
				if (this._keyname2 == null)
				{
					return string.Empty;
				}
				return this._keyname2;
			}
		}

		public double Price
		{
			get
			{
				return this._price;
			}
		}

		public string ProductCode
		{
			get
			{
				if (this._productcode == null)
				{
					return string.Empty;
				}
				return this._productcode;
			}
		}

		public string ProductDesc
		{
			get
			{
				if (this._productdesc == null)
				{
					return string.Empty;
				}
				return this._productdesc;
			}
		}

		public int ProductID
		{
			get
			{
				return this._productid;
			}
		}

		public string ProductName
		{
			get
			{
				if (this._productname == null)
				{
					return string.Empty;
				}
				return this._productname;
			}
		}

		public int ProductType
		{
			get
			{
				return this._producttype;
			}
		}

		public string SmallImageUrl
		{
			get
			{
				if (this._smallimageUrl == null)
				{
					return string.Empty;
				}
				return this._smallimageUrl;
			}
		}

		public int Stock
		{
			get
			{
				return this._stock;
			}
		}

		public CRMProduct()
		{
		}

		public CRMProduct(int productId, int itemId, string productName, double price, string currCode, string KeyItem1)
		{
			this._productid = productId;
			this._itemid = itemId;
			this._productname = productName;
			this._price = price;
			this._currcode = currCode;
			this._keyitem1 = KeyItem1;
		}

		public CRMProduct(int productId, string productCode, string productName, string productDesc, double price, string currCode, string keyName1, string keyName2, int productType, string smallImageUrl, string fullImageUrl)
		{
			this._productid = productId;
			this._productcode = productCode;
			this._productname = productName;
			this._productdesc = productDesc;
			this._price = price;
			this._currcode = currCode;
			this._keyname1 = keyName1;
			this._keyname2 = keyName2;
			this._producttype = productType;
			this._smallimageUrl = smallImageUrl;
			this._fullimageUrl = fullImageUrl;
		}

		public CRMProduct(int productId, string productCode, string productName, double price, string currCode, int productType, int stock)
		{
			this._productid = productId;
			this._productcode = productCode;
			this._productname = productName;
			this._price = price;
			this._currcode = currCode;
			this._producttype = productType;
			this._stock = stock;
		}

		public static bool CreateItem(int iProductId, string iKeyID1, string iKeyID2)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, iProductId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@keyid1", SqlDbType.VarChar, 64, ParameterDirection.Input, iKeyID1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@keyid2", SqlDbType.VarChar, 64, ParameterDirection.Input, iKeyID2);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "item_create");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public static bool CreateProduct(string iProductCode, string iProductName, string iProductDesc, double iPrice, string iCurrCode, string iKeyName1, string iKeyName2, int iProductType, string iSmallImageUrl, string iFullImageUrl)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productcode", SqlDbType.VarChar, 8, ParameterDirection.Input, iProductCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productname", SqlDbType.VarChar, 64, ParameterDirection.Input, iProductName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productdesc", SqlDbType.VarChar, 128, ParameterDirection.Input, iProductDesc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@price", SqlDbType.Float, 0, ParameterDirection.Input, iPrice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, iCurrCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@keyname1", SqlDbType.VarChar, 32, ParameterDirection.Input, iKeyName1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@keyname2", SqlDbType.VarChar, 32, ParameterDirection.Input, iKeyName2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@producttype", SqlDbType.Int, 0, ParameterDirection.Input, iProductType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@smallimageurl", SqlDbType.VarChar, 256, ParameterDirection.Input, iSmallImageUrl);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fullimageurl", SqlDbType.VarChar, 256, ParameterDirection.Input, iFullImageUrl);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "product_create");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		private static void GenerateItemListFromReader<T>(IDataReader returnData, ref List<CRMProduct> listNew)
		{
			while (returnData.Read())
			{
				CRMProduct cRMProduct = new CRMProduct(GeneralConverter.ToInt32(returnData["productid"]), GeneralConverter.ToInt32(returnData["itemid"]), GeneralConverter.ToString(returnData["productname"]), GeneralConverter.ToDouble(returnData["price"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToString(returnData["keyid1"]));
				listNew.Add(cRMProduct);
			}
		}

		private static void GenerateProductListFromReader<T>(IDataReader returnData, ref List<CRMProduct> listNew)
		{
			while (returnData.Read())
			{
				if (returnData.IsDBNull(0))
				{
					continue;
				}
				CRMProduct cRMProduct = new CRMProduct(GeneralConverter.ToInt32(returnData["productid"]), GeneralConverter.ToString(returnData["productcode"]), GeneralConverter.ToString(returnData["productname"]), GeneralConverter.ToString(returnData["productdesc"]), GeneralConverter.ToDouble(returnData["price"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToString(returnData["keyname1"]), GeneralConverter.ToString(returnData["keyname2"]), GeneralConverter.ToInt32(returnData["producttype"]), GeneralConverter.ToString(returnData["smallimageUrl"]), GeneralConverter.ToString(returnData["fullimageUrl"]));
				listNew.Add(cRMProduct);
			}
		}

		private static void GenerateProductSearchListFromReader<T>(IDataReader returnData, ref List<CRMProduct> listNew)
		{
			while (returnData.Read())
			{
				CRMProduct cRMProduct = new CRMProduct(GeneralConverter.ToInt32(returnData["productid"]), GeneralConverter.ToString(returnData["productcode"]), GeneralConverter.ToString(returnData["productname"]), GeneralConverter.ToDouble(returnData["price"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToInt32(returnData["producttype"]), GeneralConverter.ToInt32(returnData["stock"]));
				listNew.Add(cRMProduct);
			}
		}

		public static List<CRMProduct> ListByCode(string iProductCode)
		{
			if (iProductCode == string.Empty)
			{
				iProductCode = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productcode", SqlDbType.VarChar, 8, ParameterDirection.Input, iProductCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "product_search_bycode");
			List<CRMProduct> cRMProducts = new List<CRMProduct>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMProduct>(sqlCommand, new GenerateListFromReader<CRMProduct>(CRMProduct.GenerateProductSearchListFromReader<CRMProduct>), ref cRMProducts);
			return cRMProducts;
		}

		public static CRMProduct ListByID(int iProductID)
		{
			if (iProductID < 0)
			{
				iProductID = 0;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, iProductID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "product_search_byid");
			List<CRMProduct> cRMProducts = new List<CRMProduct>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMProduct>(sqlCommand, new GenerateListFromReader<CRMProduct>(CRMProduct.GenerateProductListFromReader<CRMProduct>), ref cRMProducts);
			if (cRMProducts.Count <= 0)
			{
				return null;
			}
			return cRMProducts[0];
		}

		public static List<CRMProduct> ListByName(string iProductName)
		{
			if (iProductName == string.Empty)
			{
				iProductName = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productname", SqlDbType.VarChar, 64, ParameterDirection.Input, iProductName);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "product_search_byname");
			List<CRMProduct> cRMProducts = new List<CRMProduct>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMProduct>(sqlCommand, new GenerateListFromReader<CRMProduct>(CRMProduct.GenerateProductSearchListFromReader<CRMProduct>), ref cRMProducts);
			return cRMProducts;
		}

		public static List<CRMProduct> ListByType(int iProductType)
		{
			if (iProductType < 0)
			{
				iProductType = 0;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@producttype", SqlDbType.Int, 0, ParameterDirection.Input, iProductType);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "product_search_bytype");
			List<CRMProduct> cRMProducts = new List<CRMProduct>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMProduct>(sqlCommand, new GenerateListFromReader<CRMProduct>(CRMProduct.GenerateProductSearchListFromReader<CRMProduct>), ref cRMProducts);
			return cRMProducts;
		}

		public static List<CRMProduct> ListItemSelected(int iProductID, int iquantity, string iReserveBy)
		{
			if (iProductID < 0)
			{
				iProductID = 0;
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productId", SqlDbType.VarChar, 10, ParameterDirection.Input, iProductID.ToString());
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@quantity", SqlDbType.VarChar, 10, ParameterDirection.Input, iquantity.ToString());
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@reserveBy", SqlDbType.VarChar, 16, ParameterDirection.Input, iReserveBy.ToString());
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "product_list_item");
			List<CRMProduct> cRMProducts = new List<CRMProduct>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMProduct>(sqlCommand, new GenerateListFromReader<CRMProduct>(CRMProduct.GenerateItemListFromReader<CRMProduct>), ref cRMProducts);
			return cRMProducts;
		}

		public static List<CRMProduct> ListNumberAvailable()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "select_number_available");
			List<CRMProduct> cRMProducts = new List<CRMProduct>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMProduct>(sqlCommand, new GenerateListFromReader<CRMProduct>(CRMProduct.GenerateItemListFromReader<CRMProduct>), ref cRMProducts);
			return cRMProducts;
		}

		public static bool SelectNumber(int iItemId, string iReserveBy)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemId", SqlDbType.Int, 0, ParameterDirection.Input, iItemId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@reserveBy", SqlDbType.VarChar, 16, ParameterDirection.Input, iReserveBy.ToString());
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "select_number");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public static bool unSelectItem(int iItemId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@itemId", SqlDbType.Int, 0, ParameterDirection.Input, iItemId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "unselect_item");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public static bool UpdateProduct(int iProductID, string iProductCode, string iProductName, string iProductDesc, double iPrice, string iCurrCode, string iKeyName1, string iKeyName2, int iProductType, string iSmallImgUrl, string iFullImgUrl)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productid", SqlDbType.Int, 0, ParameterDirection.Input, iProductID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productcode", SqlDbType.VarChar, 8, ParameterDirection.Input, iProductCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productname", SqlDbType.VarChar, 64, ParameterDirection.Input, iProductName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@productdesc", SqlDbType.VarChar, 128, ParameterDirection.Input, iProductDesc);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@price", SqlDbType.Float, 0, ParameterDirection.Input, iPrice);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, iCurrCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@keyname1", SqlDbType.VarChar, 32, ParameterDirection.Input, iKeyName1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@keyname2", SqlDbType.VarChar, 32, ParameterDirection.Input, iKeyName2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@producttype", SqlDbType.Int, 0, ParameterDirection.Input, iProductType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@smallimageurl", SqlDbType.VarChar, 256, ParameterDirection.Input, iSmallImgUrl);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@fullimageurl", SqlDbType.VarChar, 256, ParameterDirection.Input, iFullImgUrl);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "product_update");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}
	}
}