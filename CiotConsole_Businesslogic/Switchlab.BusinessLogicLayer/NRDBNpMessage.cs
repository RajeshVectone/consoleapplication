using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public abstract class NRDBNpMessage
	{
		private const string SP_SEARCH_CASES = "search_cases";

		private const string SP_GET_CASES_BY_CASE_ID = "get_case_by_case_id";

		protected const string SP_SEND_NP_MESSAGE = "send_NP_Message";

		protected NRDBNpMessage()
		{
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<NRDBMessage> listRef)
		{
			while (returnData.Read())
			{
				NRDBMessage nRDBMessage = new NRDBMessage(GeneralConverter.ToInt32(returnData["msg_out_id"]), GeneralConverter.ToString(returnData["op_msg_sender"]), GeneralConverter.ToLong(returnData["op_nrdbcase_id"]), GeneralConverter.ToString(returnData["op_auth_ref"]), GeneralConverter.ToString(returnData["subscr_type"]), GeneralConverter.ToString(returnData["op_telephonenumber_main"]), GeneralConverter.ToDateTime(returnData["op_port_datetime"]), GeneralConverter.ToString(returnData["status"]), GeneralConverter.ToString(returnData["msg_type"]));
				listRef.Add(nRDBMessage);
			}
		}

		private static void GenerateListFromReaderByCaseId<T>(IDataReader returnData, ref List<NRDBMessage> listRef)
		{
			while (returnData.Read())
			{
				NRDBMessage nRDBMessage = new NRDBMessage(GeneralConverter.ToInt32(returnData["msg_out_id"]), GeneralConverter.ToString(returnData["msg_type"]), GeneralConverter.ToInt32(returnData["op_sequence"]), GeneralConverter.ToString(returnData["op_msg_sent_datetime"]), GeneralConverter.ToString(returnData["subscr_type"]), GeneralConverter.ToString(returnData["op_telephonenumber_main"]), GeneralConverter.ToString(returnData["op_auth_ref"]));
				listRef.Add(nRDBMessage);
			}
		}

		public static List<NRDBMessage> GetByCaseId(long caseId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNRDBConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@caseNumber", SqlDbType.BigInt, 0, ParameterDirection.Input, caseId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_case_by_case_id");
			List<NRDBMessage> nRDBMessages = new List<NRDBMessage>();
			sQLDataAccessLayer.ExecuteReaderCmd<NRDBMessage>(sqlCommand, new GenerateListFromReader<NRDBMessage>(NRDBNpMessage.GenerateListFromReaderByCaseId<NRDBMessage>), ref nRDBMessages);
			return nRDBMessages;
		}

		public abstract bool Save(NRDBMessage message);

		public static List<NRDBMessage> SearchCases(long caseId, string telpNumber, DateTime? portingDateFrom, DateTime? portingDateTo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNRDBConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@caseNumber", SqlDbType.BigInt, 0, ParameterDirection.Input, caseId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telpNumber", SqlDbType.VarChar, 254, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@porting_date_from", SqlDbType.DateTime, 0, ParameterDirection.Input, portingDateFrom);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@porting_date_to", SqlDbType.DateTime, 0, ParameterDirection.Input, portingDateTo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "search_cases");
			List<NRDBMessage> nRDBMessages = new List<NRDBMessage>();
			sQLDataAccessLayer.ExecuteReaderCmd<NRDBMessage>(sqlCommand, new GenerateListFromReader<NRDBMessage>(NRDBNpMessage.GenerateListFromReader<NRDBMessage>), ref nRDBMessages);
			return nRDBMessages;
		}
	}
}