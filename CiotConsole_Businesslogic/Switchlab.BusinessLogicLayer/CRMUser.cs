using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMUser
	{
		private const string SP_USER_LOADBYLOGIN = "user_loadbylogin";

		private const string SP_USER_LOADBYID = "user_loadbyid";

		private const string SP_USER_LISTBYGROUP = "user_listbygroup";

		private const string SP_USER_LIST = "user_listall";

		private const string SP_USER_LISTBYMANAGER = "user_listbymanager";

		private const string SP_USER_CREATE = "user_create";

		private const string SP_USER_UPDATE = "user_update";

		private const string SP_USER_UPDATENOPASS = "user_updatenopass";

		private const string SP_USER_DELETE = "user_delete";

		private const string SP_USER_UPDATEOWNDETAIL = "user_updateowndetail";

		private const string SP_USER_AUTHENTICATE_AGENT = "user_authenticate_agent";

		private string _login;

		private int _groupid;

		private int _status;

		private string _firstname;

		private string _surname;

		private string _email;

		private DateTime _expiry;

		private int _userid;

		private int _authtype;

		private string _StatusStr;

		private string _GroupName;

		public int AuthenticationType
		{
			get
			{
				return this._authtype;
			}
		}

		public string Email
		{
			get
			{
				if (this._email == null)
				{
					return string.Empty;
				}
				return this._email;
			}
		}

		public DateTime Expiry
		{
			get
			{
				return this._expiry;
			}
		}

		public string FirstName
		{
			get
			{
				if (this._firstname == null)
				{
					return string.Empty;
				}
				return this._firstname;
			}
		}

		public int GroupID
		{
			get
			{
				return this._groupid;
			}
		}

		public string GroupName
		{
			get
			{
				if (this._GroupName == null)
				{
					return string.Empty;
				}
				return this._GroupName;
			}
		}

		public string Login
		{
			get
			{
				return this._login;
			}
		}

		public int Status
		{
			get
			{
				return this._status;
			}
		}

		public string StatusStr
		{
			get
			{
				if (this._StatusStr == null)
				{
					return string.Empty;
				}
				return this._StatusStr;
			}
		}

		public string SurName
		{
			get
			{
				if (this._surname == null)
				{
					return string.Empty;
				}
				return this._surname;
			}
		}

		public int UserID
		{
			get
			{
				return this._userid;
			}
		}

		public CRMUser()
		{
		}

		public CRMUser(string login)
		{
			if (string.IsNullOrEmpty(login))
			{
				throw new ArgumentException("login");
			}
			this._login = login;
		}

		public CRMUser(int iUserID, string sLogin, int iGroupID, int iStatus, string sStatusStr, string sFirstName, string sSurName, string sEmail, DateTime dExpiry, int iAuthType, string sGroupName) : this(sLogin)
		{
			this._groupid = iGroupID;
			this._status = iStatus;
			this._firstname = sFirstName;
			this._surname = sSurName;
			this._email = sEmail;
			this._expiry = dExpiry;
			this._userid = iUserID;
			this._authtype = iAuthType;
			this._StatusStr = sStatusStr;
			this._GroupName = sGroupName;
		}

		public static int AuthenticateAgent(string username, string password)
		{
			if (string.IsNullOrEmpty(username))
			{
				throw new ArgumentException("username");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 0, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@login", SqlDbType.VarChar, 16, ParameterDirection.Input, username);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@passwd", SqlDbType.VarChar, 32, ParameterDirection.Input, password);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_authenticate_agent");
			sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			return (int)sqlCommand.Parameters["@ReturnValue"].Value;
		}

		public static CRMUser Create(string sLogin, string sPassword, object iGroupID, int iStatus, string sFirstName, string sSurName, string sEmail, DateTime dExpiry, int iAuthenticationType)
		{
			if (string.IsNullOrEmpty(sLogin))
			{
				throw new ArgumentException("login");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@login", SqlDbType.VarChar, 16, ParameterDirection.Input, sLogin);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@password", SqlDbType.VarChar, 32, ParameterDirection.Input, sPassword);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@group_id", SqlDbType.Int, 0, ParameterDirection.Input, iGroupID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, iStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 32, ParameterDirection.Input, sFirstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@surname", SqlDbType.VarChar, 64, ParameterDirection.Input, sSurName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@email", SqlDbType.VarChar, 64, ParameterDirection.Input, sEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@expirydate", SqlDbType.DateTime, 0, ParameterDirection.Input, dExpiry);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@authtype", SqlDbType.Int, 0, ParameterDirection.Input, iAuthenticationType);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_create");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return null;
			}
			return CRMUser.GetByID(GeneralConverter.ToInt32(obj));
		}

		public static CRMUser CreateUser(string sLogin, string sPassword, int iGroupID, int iStatus, string sFirstName, string sSurName, string sEmail, DateTime dExpiry)
		{
			return CRMUser.Create(sLogin, sPassword, iGroupID, iStatus, sFirstName, sSurName, sEmail, dExpiry, 1);
		}

		public static bool Delete(int iUserID)
		{
			if (iUserID < 0)
			{
				throw new ArgumentOutOfRangeException("iUserID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@userid", SqlDbType.Int, 0, ParameterDirection.Input, iUserID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_delete");
			return sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CRMUser> listItem)
		{
			while (returnData.Read())
			{
				CRMUser cRMUser = new CRMUser(GeneralConverter.ToInt32(returnData["user_id"]), Convert.ToString(returnData["login"]), GeneralConverter.ToInt32(returnData["group_id"]), GeneralConverter.ToInt32(returnData["status"]), Convert.ToString(returnData["statusstr"]), Convert.ToString(returnData["firstname"]), Convert.ToString(returnData["surname"]), Convert.ToString(returnData["email"]), GeneralConverter.ToDateTime(returnData["expirydate"]), GeneralConverter.ToInt32(returnData["authtype"]), Convert.ToString(returnData["groupname"]));
				listItem.Add(cRMUser);
			}
		}

		public static CRMUser GetByID(int UserID)
		{
			if (UserID < 0)
			{
				throw new ArgumentOutOfRangeException("UserID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@userid", SqlDbType.Int, 0, ParameterDirection.Input, UserID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_loadbyid");
			List<CRMUser> cRMUsers = new List<CRMUser>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMUser>(sqlCommand, new GenerateListFromReader<CRMUser>(CRMUser.GenerateListFromReader<CRMUser>), ref cRMUsers);
			if (cRMUsers.Count <= 0)
			{
				return null;
			}
			return cRMUsers[0];
		}

		public static CRMUser GetByLogin(string sLogin, int iAuthType)
		{
			if (string.IsNullOrEmpty(sLogin))
			{
				throw new ArgumentException("login");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@login", SqlDbType.VarChar, 16, ParameterDirection.Input, sLogin);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@authtype", SqlDbType.Int, 0, ParameterDirection.Input, iAuthType);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_loadbylogin");
			List<CRMUser> cRMUsers = new List<CRMUser>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMUser>(sqlCommand, new GenerateListFromReader<CRMUser>(CRMUser.GenerateListFromReader<CRMUser>), ref cRMUsers);
			if (cRMUsers.Count <= 0)
			{
				return null;
			}
			return cRMUsers[0];
		}

		public static List<CRMUser> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_listall");
			List<CRMUser> cRMUsers = new List<CRMUser>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMUser>(sqlCommand, new GenerateListFromReader<CRMUser>(CRMUser.GenerateListFromReader<CRMUser>), ref cRMUsers);
			return cRMUsers;
		}

		public static List<CRMUser> ListByGroup(int GroupID)
		{
			if (GroupID < 0)
			{
				throw new ArgumentOutOfRangeException("GroupID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, GroupID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_listbygroup");
			List<CRMUser> cRMUsers = new List<CRMUser>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMUser>(sqlCommand, new GenerateListFromReader<CRMUser>(CRMUser.GenerateListFromReader<CRMUser>), ref cRMUsers);
			return cRMUsers;
		}

		public static List<CRMUser> ListByManager(int ManagerID)
		{
			if (ManagerID < 0)
			{
				throw new ArgumentOutOfRangeException("ManagerID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@managerid", SqlDbType.Int, 0, ParameterDirection.Input, ManagerID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_listbymanager");
			List<CRMUser> cRMUsers = new List<CRMUser>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMUser>(sqlCommand, new GenerateListFromReader<CRMUser>(CRMUser.GenerateListFromReader<CRMUser>), ref cRMUsers);
			return cRMUsers;
		}

		public bool UpdateOwnDetail(string sOldPassword, string sNewPassword, object iGroupID, int iStatus, string sFirstName, string sSurName, string sEmail, DateTime dExpiry)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@userid", SqlDbType.Int, 0, ParameterDirection.Input, this.UserID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@group_id", SqlDbType.Int, 0, ParameterDirection.Input, iGroupID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, iStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 32, ParameterDirection.Input, sFirstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@surname", SqlDbType.VarChar, 64, ParameterDirection.Input, sSurName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@email", SqlDbType.VarChar, 64, ParameterDirection.Input, sEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@expirydate", SqlDbType.DateTime, 0, ParameterDirection.Input, dExpiry);
			if (sOldPassword == string.Empty || sNewPassword == string.Empty)
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_updatenopass");
			}
			else
			{
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@oldPassword", SqlDbType.VarChar, 32, ParameterDirection.Input, sOldPassword);
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@newPassword", SqlDbType.VarChar, 32, ParameterDirection.Input, sNewPassword);
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_updateowndetail");
			}
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) <= 0)
			{
				return false;
			}
			this._groupid = GeneralConverter.ToInt32(iGroupID);
			this._status = iStatus;
			this._firstname = sFirstName;
			this._surname = sSurName;
			this._email = sEmail;
			this._expiry = dExpiry;
			return true;
		}

		public bool UpdateUser(string sPassword, object iGroupID, int iStatus, string sFirstName, string sSurName, string sEmail, DateTime dExpiry)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@userid", SqlDbType.Int, 0, ParameterDirection.Input, this.UserID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@group_id", SqlDbType.Int, 0, ParameterDirection.Input, iGroupID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, iStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 32, ParameterDirection.Input, sFirstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@surname", SqlDbType.VarChar, 64, ParameterDirection.Input, sSurName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@email", SqlDbType.VarChar, 64, ParameterDirection.Input, sEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@expirydate", SqlDbType.DateTime, 0, ParameterDirection.Input, dExpiry);
			if (sPassword != string.Empty)
			{
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@password", SqlDbType.VarChar, 32, ParameterDirection.Input, sPassword);
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_update");
			}
			else
			{
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "user_updatenopass");
			}
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) <= 0)
			{
				return false;
			}
			this._groupid = GeneralConverter.ToInt32(iGroupID);
			this._status = iStatus;
			this._firstname = sFirstName;
			this._surname = sSurName;
			this._email = sEmail;
			this._expiry = dExpiry;
			return true;
		}
	}
}