using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class NRDBNpGodkMessage : NRDBNpMessage
	{
		public NRDBNpGodkMessage()
		{
		}

		public override bool Save(NRDBMessage message)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNRDBConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msgtype_id", SqlDbType.Int, 0, ParameterDirection.Input, message.TypeId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@caseId", SqlDbType.BigInt, 0, ParameterDirection.Input, message.CaseId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sequence", SqlDbType.Int, 0, ParameterDirection.Input, message.Sequence);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@contact_name", SqlDbType.VarChar, 254, ParameterDirection.Input, message.ContactName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@contact_telp", SqlDbType.VarChar, 254, ParameterDirection.Input, message.ContactTelephone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@contact_email", SqlDbType.VarChar, 254, ParameterDirection.Input, message.ContactEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@coordinate_porting", SqlDbType.Int, 0, ParameterDirection.Input, message.CoordinatePorting);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "send_NP_Message");
			return sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0;
		}
	}
}