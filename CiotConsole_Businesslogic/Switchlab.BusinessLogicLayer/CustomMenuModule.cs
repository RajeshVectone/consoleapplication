using System;
using System.Collections.Generic;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CustomMenuModule
	{
		private const string SP_LISTALL = "sample_listall";

		private int _SelectedModuleID;

		private string _SelectedModule;

		private int _GroupID;

		private string _bufferPage;

		public int GroupID
		{
			get
			{
				return this._GroupID;
			}
		}

		public string SelectedModule
		{
			get
			{
				if (this._SelectedModule == null)
				{
					return string.Empty;
				}
				return this._SelectedModule;
			}
		}

		public int SelectedModuleID
		{
			get
			{
				return this._SelectedModuleID;
			}
		}

		public CustomMenuModule()
		{
		}

		public CustomMenuModule(string bufferPage, int GroupID)
		{
			this._GroupID = GroupID;
			this._bufferPage = bufferPage;
		}

		public string FillMenu(int SelectedMenuID)
		{
			return this.FillMenu(this.SelectedModuleID, SelectedMenuID);
		}

		public string FillMenu(int SelectedModuleID, int SelectedMenuID)
		{
			string empty = string.Empty;
			this._SelectedModuleID = SelectedModuleID;
			foreach (CRMPage cRMPage in CRMPage.ListMenu(this.GroupID, SelectedModuleID, 0))
			{
				string str = string.Empty;
				bool flag = this.GenerateSubMenu(ref str, cRMPage.PageID, SelectedMenuID);
				if (str != string.Empty)
				{
					empty = (!flag ? string.Concat(empty, string.Format("<li class=\"treenode\"><a href=\"#\">&raquo; {0}</a><ul class=\"nodechild\">{1}</ul></li>", cRMPage.DisplayName, str)) : string.Concat(empty, string.Format("<li class=\"treenodeopen\"><a href=\"#\">&raquo; {0}</a><ul class=\"nodechild\">{1}</ul></li>", cRMPage.DisplayName, str)));
				}
				else
				{
					empty = (cRMPage.PageID != SelectedMenuID ? string.Concat(empty, string.Format("<li class=\"nodesubchild\"><a href=\"{2}?MenuUrl={0}\">&bull; {1}</a></li>", cRMPage.PageID, cRMPage.DisplayName, this._bufferPage)) : string.Concat(empty, string.Format("<li class=\"nodesubchild\"><a href=\"#\">&bull; <u>{0}</u></a></li>", cRMPage.DisplayName)));
				}
			}
			return empty;
		}

		public string FillModule(string SelectedModule)
		{
			string empty = string.Empty;
			this._SelectedModule = SelectedModule;
			foreach (CrmModule crmModule in CrmModule.ListModule(this.GroupID))
			{
				if (crmModule.title != SelectedModule)
				{
					empty = string.Concat(empty, string.Format("<li><a href=\"{2}?ModuleUrl={0}\">{1}</a></li>", crmModule.title, crmModule.displayname, this._bufferPage));
				}
				else
				{
					empty = string.Concat(empty, string.Format("<li id=\"current\"><a href=\"#\">{0}</a></li>", crmModule.displayname));
					this._SelectedModuleID = crmModule.module_id;
				}
			}
			return string.Format("<ul>{0}</ul>", empty);
		}

		protected bool GenerateSubMenu(ref string OutMenu, int ParentID, int SelectedMenuID)
		{
			bool flag = false;
			OutMenu = string.Empty;
			foreach (CRMPage cRMPage in CRMPage.ListMenu(this.GroupID, this.SelectedModuleID, ParentID))
			{
				string empty = string.Empty;
				bool flag1 = this.GenerateSubMenu(ref empty, cRMPage.PageID, SelectedMenuID);
				if (empty != string.Empty)
				{
					if (!flag1)
					{
						OutMenu = string.Concat(OutMenu, string.Format("<li class=\"treenode\"><a href=\"#\">&raquo; {0}</a><ul class=\"nodechild\">{1}</ul></li>", cRMPage.DisplayName, empty));
					}
					else
					{
						OutMenu = string.Concat(OutMenu, string.Format("<li class=\"treenodeopen\"><a href=\"#\">&raquo; {0}</a><ul class=\"nodechild\">{1}</ul></li>", cRMPage.DisplayName, empty));
					}
					flag |= flag1;
				}
				else if (cRMPage.PageID != SelectedMenuID)
				{
					OutMenu = string.Concat(OutMenu, string.Format("<li><a href=\"{2}?MenuUrl={0}\">&bull; {1}</a></li>", cRMPage.PageID, cRMPage.DisplayName, this._bufferPage));
				}
				else
				{
					OutMenu = string.Concat(OutMenu, string.Format("<li><a href=\"#\">&bull; <u>{0}</u></a></li>", cRMPage.DisplayName));
					flag = true;
				}
			}
			return flag;
		}
	}
}