using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class CRMRecredit
	{
		private const string SP_REQUEST_RECREDIT = "recredit_new_request";

		private const string SP_REQUEST_UPDATESTATUS = "recredit_updstatus_request";

		private const string SP_LIST_BYSTATUSMOBILENO = "recredit_list_bystatno";

		private const string SP_RECREDIT_ACCOUNT = "account_recredit";

		private int _recreditid;

		private string _phonenb;

		private string _iccid;

		private string _telcocode;

		private string _custcode;

		private int _batchcode;

		private int _serialcode;

		private float _amount;

		private float _prevbalance;

		private float _aftbalance;

		private DateTime _requesttime;

		private DateTime _approvetime;

		private int _recreditstatus;

		private int _requestuserid;

		private int _approveuserid;

		private string _requestreason;

		private string _approvereason;

		private string _requestuserstr;

		private string _approveuserstr;

		public float AftBalance
		{
			get
			{
				return this._aftbalance;
			}
		}

		public float AftBalanceMoney
		{
			get
			{
				if (this._aftbalance == 0f)
				{
					return 0f;
				}
				return this._aftbalance / 100f;
			}
		}

		public float Amount
		{
			get
			{
				return this._amount;
			}
		}

		public float AmountMoney
		{
			get
			{
				if (this._amount == 0f)
				{
					return 0f;
				}
				return this._amount / 100f;
			}
		}

		public string ApproveReason
		{
			get
			{
				if (this._approvereason == null)
				{
					return string.Empty;
				}
				return this._approvereason;
			}
		}

		public DateTime ApproveTime
		{
			get
			{
				return this._approvetime;
			}
		}

		public int ApproveUserID
		{
			get
			{
				return this._approveuserid;
			}
		}

		public string ApproveUserStr
		{
			get
			{
				if (this._approveuserstr == null)
				{
					return string.Empty;
				}
				return this._approveuserstr;
			}
		}

		public int BatchCode
		{
			get
			{
				return this._batchcode;
			}
		}

		public string BatchNo
		{
			get
			{
				string str = this.CustCode.Substring(4);
				int batchCode = this.BatchCode;
				string str1 = batchCode.ToString().PadLeft(4, '0');
				int serialCode = this.SerialCode;
				return string.Format("{0}{1}{2}", str, str1, serialCode.ToString().PadLeft(4, '0'));
			}
		}

		public string CustCode
		{
			get
			{
				if (this._custcode == null)
				{
					return string.Empty;
				}
				return this._custcode;
			}
		}

		public string ICCID
		{
			get
			{
				if (this._iccid == null)
				{
					return string.Empty;
				}
				return this._iccid;
			}
		}

		public string PhoneNB
		{
			get
			{
				if (this._phonenb == null)
				{
					return string.Empty;
				}
				return this._phonenb;
			}
		}

		public float PrevBalance
		{
			get
			{
				return this._prevbalance;
			}
		}

		public float PrevBalanceMoney
		{
			get
			{
				if (this._prevbalance == 0f)
				{
					return 0f;
				}
				return this._prevbalance / 100f;
			}
		}

		public int RecreditID
		{
			get
			{
				return this._recreditid;
			}
		}

		public int RecreditStatus
		{
			get
			{
				return this._recreditstatus;
			}
		}

		public string RequestReason
		{
			get
			{
				if (this._requestreason == null)
				{
					return string.Empty;
				}
				return this._requestreason;
			}
		}

		public DateTime RequestTime
		{
			get
			{
				return this._requesttime;
			}
		}

		public int RequestUserID
		{
			get
			{
				return this._requestuserid;
			}
		}

		public string RequestUserStr
		{
			get
			{
				if (this._requestuserstr == null)
				{
					return string.Empty;
				}
				return this._requestuserstr;
			}
		}

		public int SerialCode
		{
			get
			{
				return this._serialcode;
			}
		}

		public string TelcoCode
		{
			get
			{
				if (this._telcocode == null)
				{
					return string.Empty;
				}
				return this._telcocode;
			}
		}

		public CRMRecredit()
		{
		}

		public CRMRecredit(int RecreditID, string PhoneNB, string ICCID, string TelcoCode, string CustCode, int BatchCode, int SerialCode, float Amount, float PrevBalance, float AftBalance, DateTime RequestTime, DateTime ApproveTime, int RecreditStatus, int RequestUserID, int ApproveUserID, string RequestReason, string ApproveReason, string RequestUserStr, string ApproveUserStr)
		{
			this._recreditid = RecreditID;
			this._phonenb = PhoneNB;
			this._iccid = ICCID;
			this._telcocode = TelcoCode;
			this._custcode = TelcoCode;
			this._batchcode = BatchCode;
			this._serialcode = SerialCode;
			this._amount = Amount;
			this._prevbalance = PrevBalance;
			this._aftbalance = AftBalance;
			this._requesttime = RequestTime;
			this._approvetime = ApproveTime;
			this._recreditstatus = RecreditStatus;
			this._requestuserid = RequestUserID;
			this._approveuserid = ApproveUserID;
			this._requestreason = RequestReason;
			this._approvereason = ApproveReason;
			this._requestuserstr = RequestUserStr;
			this._approveuserstr = ApproveUserStr;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CRMRecredit> listNew)
		{
			while (returnData.Read())
			{
				CRMRecredit cRMRecredit = null;
				cRMRecredit = (returnData.FieldCount != 17 ? new CRMRecredit(GeneralConverter.ToInt32(returnData["recreditid"]), GeneralConverter.ToString(returnData["phonenb"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToString(returnData["telcocode"]), GeneralConverter.ToString(returnData["custcode"]), GeneralConverter.ToInt32(returnData["batchcode"]), GeneralConverter.ToInt32(returnData["serialcode"]), GeneralConverter.ToSingle(returnData["amount"]), GeneralConverter.ToSingle(returnData["prevbalance"]), GeneralConverter.ToSingle(returnData["aftbalance"]), GeneralConverter.ToDateTime(returnData["requesttime"]), GeneralConverter.ToDateTime(returnData["approvetime"]), GeneralConverter.ToInt32(returnData["recredit_status"]), GeneralConverter.ToInt32(returnData["requestuserid"]), GeneralConverter.ToInt32(returnData["approveuserid"]), GeneralConverter.ToString(returnData["reqreason"]), GeneralConverter.ToString(returnData["approvereason"]), GeneralConverter.ToString(returnData["requestlogin"]), GeneralConverter.ToString(returnData["approvelogin"])) : new CRMRecredit(GeneralConverter.ToInt32(returnData["recreditid"]), GeneralConverter.ToString(returnData["phonenb"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToString(returnData["telcocode"]), GeneralConverter.ToString(returnData["custcode"]), GeneralConverter.ToInt32(returnData["batchcode"]), GeneralConverter.ToInt32(returnData["serialcode"]), GeneralConverter.ToSingle(returnData["amount"]), GeneralConverter.ToSingle(returnData["prevbalance"]), GeneralConverter.ToSingle(returnData["aftbalance"]), GeneralConverter.ToDateTime(returnData["requesttime"]), GeneralConverter.ToDateTime(returnData["approvetime"]), GeneralConverter.ToInt32(returnData["recredit_status"]), GeneralConverter.ToInt32(returnData["requestuserid"]), GeneralConverter.ToInt32(returnData["approveuserid"]), GeneralConverter.ToString(returnData["reqreason"]), GeneralConverter.ToString(returnData["approvereason"]), "", ""));
				listNew.Add(cRMRecredit);
			}
		}

		public static List<CRMRecredit> ListApproved(string PhoneNB)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@recreditstatus", SqlDbType.BigInt, 0, ParameterDirection.Input, 2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@phonenb", SqlDbType.VarChar, 30, ParameterDirection.Input, PhoneNB);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "recredit_list_bystatno");
			List<CRMRecredit> cRMRecredits = new List<CRMRecredit>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRecredit>(sqlCommand, new GenerateListFromReader<CRMRecredit>(CRMRecredit.GenerateListFromReader<CRMRecredit>), ref cRMRecredits);
			return cRMRecredits;
		}

		public static List<CRMRecredit> ListRejected(string PhoneNB)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@recreditstatus", SqlDbType.BigInt, 0, ParameterDirection.Input, 3);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@phonenb", SqlDbType.VarChar, 30, ParameterDirection.Input, PhoneNB);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "recredit_list_bystatno");
			List<CRMRecredit> cRMRecredits = new List<CRMRecredit>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRecredit>(sqlCommand, new GenerateListFromReader<CRMRecredit>(CRMRecredit.GenerateListFromReader<CRMRecredit>), ref cRMRecredits);
			return cRMRecredits;
		}

		public static List<CRMRecredit> ListRequest(string PhoneNB)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@recreditstatus", SqlDbType.BigInt, 0, ParameterDirection.Input, 1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@phonenb", SqlDbType.VarChar, 30, ParameterDirection.Input, PhoneNB);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "recredit_list_bystatno");
			List<CRMRecredit> cRMRecredits = new List<CRMRecredit>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRecredit>(sqlCommand, new GenerateListFromReader<CRMRecredit>(CRMRecredit.GenerateListFromReader<CRMRecredit>), ref cRMRecredits);
			return cRMRecredits;
		}

		public static bool RequestApprove(int RecreditID, int ApproveUserID, string ApproveReason)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@recreditid", SqlDbType.Int, 0, ParameterDirection.Input, RecreditID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@approveuserid", SqlDbType.Int, 0, ParameterDirection.Input, ApproveUserID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@approvereason", SqlDbType.VarChar, 255, ParameterDirection.Input, ApproveReason);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "account_recredit");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj != null && GeneralConverter.ToInt32(obj) > 0)
			{
				return true;
			}
			return false;
		}

		public static bool RequestRecredit(string PhoneNB, string ICCID, string TelcoCode, string CustCode, int BatchCode, int SerialCode, float Amount, float PrevBalance, int RequestUserID, string RequestReason)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@phonenb", SqlDbType.Char, 30, ParameterDirection.Input, PhoneNB);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.Char, 20, ParameterDirection.Input, ICCID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, TelcoCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.Char, 8, ParameterDirection.Input, CustCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, BatchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, SerialCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@amount", SqlDbType.Float, 0, ParameterDirection.Input, Amount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@prevbalance", SqlDbType.Float, 0, ParameterDirection.Input, PrevBalance);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@requestuserid", SqlDbType.Int, 0, ParameterDirection.Input, RequestUserID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@reqreason", SqlDbType.VarChar, 255, ParameterDirection.Input, RequestReason);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "recredit_new_request");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != null)
			{
				return true;
			}
			return false;
		}

		public static bool RequestReject(int RecreditID, int RejectUserID, string RejectReason)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@recreditid", SqlDbType.Int, 0, ParameterDirection.Input, RecreditID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@approveuserid", SqlDbType.Int, 0, ParameterDirection.Input, RejectUserID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@recreditstatus", SqlDbType.Int, 0, ParameterDirection.Input, 3);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@approvereason", SqlDbType.VarChar, 255, ParameterDirection.Input, RejectReason);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "recredit_updstatus_request");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj != null && GeneralConverter.ToInt32(obj) > 0)
			{
				return true;
			}
			return false;
		}
	}
}