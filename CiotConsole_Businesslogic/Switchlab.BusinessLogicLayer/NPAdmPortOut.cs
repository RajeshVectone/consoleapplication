using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmPortOut
	{
		private const string SP_UPDATE_PORTING_OUT_STATUS = "PortingOut_updatePortingOutStatus";

		private const string SP_CREATE_PORTING_OUT = "PortingOut_insert";

		private const string SP_LIST_NEW_WT = "PortingOut_getListNewWrittenTermination";

		private int _portingID;

		private string _telpNumber;

		private string _customerID;

		private string _recipientOperator;

		private int _status;

		private string _statusDesc;

		private string _customerFirstName;

		private string _customerLastName;

		public string CustomerFirstName
		{
			get
			{
				return this._customerFirstName;
			}
			set
			{
				this._customerFirstName = value;
			}
		}

		public string CustomerID
		{
			get
			{
				return this._customerID;
			}
			set
			{
				this._customerID = value;
			}
		}

		public string CustomerLastName
		{
			get
			{
				return this._customerLastName;
			}
			set
			{
				this._customerLastName = value;
			}
		}

		public int PortingID
		{
			get
			{
				return this._portingID;
			}
		}

		public string RecipientOperator
		{
			get
			{
				return this._recipientOperator;
			}
			set
			{
				this._recipientOperator = value;
			}
		}

		public int Status
		{
			get
			{
				return this._status;
			}
			set
			{
				this._status = value;
			}
		}

		public string StatusDesc
		{
			get
			{
				return this._statusDesc;
			}
			set
			{
				this._statusDesc = value;
			}
		}

		public string TelpNumber
		{
			get
			{
				return this._telpNumber;
			}
			set
			{
				this._telpNumber = value;
			}
		}

		public NPAdmPortOut(int portingId, string telpNumber, string custId, string recipientOperator, int status, string statusDesc, string customerFirstName, string customerLastName)
		{
			this._portingID = portingId;
			this._telpNumber = telpNumber;
			this._customerID = custId;
			this._recipientOperator = recipientOperator;
			this._status = status;
			this._statusDesc = statusDesc;
			this._customerFirstName = customerFirstName;
			this._customerLastName = customerLastName;
		}

		public static bool Create(string telpNumber, string recipientOperator, string customerId, int status, string agent)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telpNumber", SqlDbType.VarChar, 17, ParameterDirection.Input, telpNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@recipientOperator", SqlDbType.VarChar, 50, ParameterDirection.Input, recipientOperator);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custId", SqlDbType.VarChar, 60, ParameterDirection.Input, customerId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, status);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@agent", SqlDbType.VarChar, 32, ParameterDirection.Input, agent);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@returnvalue", SqlDbType.VarChar, 32, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "PortingOut_insert");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
			if (Convert.ToInt32(sqlCommand.Parameters["@returnvalue"].Value) == 0)
			{
				return true;
			}
			return false;
		}

		private static void GenerateNewWrittenTerminationFromReader<T>(IDataReader returnData, ref List<NPAdmPortOut> listNew)
		{
			while (returnData.Read())
			{
				NPAdmPortOut nPAdmPortOut = new NPAdmPortOut(GeneralConverter.ToInt32(returnData["portingId"]), GeneralConverter.ToString(returnData["telpNumber"]), GeneralConverter.ToString(returnData["customerId"]), GeneralConverter.ToString(returnData["recipientOperator"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToString(returnData["statusDesc"]), GeneralConverter.ToString(returnData["customerFirstName"]), GeneralConverter.ToString(returnData["customerlastName"]));
				listNew.Add(nPAdmPortOut);
			}
		}

		public static List<NPAdmPortOut> ListNewWrittenTermination()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "PortingOut_getListNewWrittenTermination");
			List<NPAdmPortOut> nPAdmPortOuts = new List<NPAdmPortOut>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmPortOut>(sqlCommand, new GenerateListFromReader<NPAdmPortOut>(NPAdmPortOut.GenerateNewWrittenTerminationFromReader<NPAdmPortOut>), ref nPAdmPortOuts);
			return nPAdmPortOuts;
		}

		public static bool UpdatePortingOutStatus(int portingId, int status)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@portingId", SqlDbType.Int, 0, ParameterDirection.Input, portingId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, status);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "PortingOut_updatePortingOutStatus");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}
	}
}