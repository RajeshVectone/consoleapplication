using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class CrmRefProblemStatus
	{
		private const string SP_LIST = "r_problem_status";

		private int _problemstatus;

		private string _pstatusname;

		private string _pstatusdesc;

		public int ProblemStatus
		{
			get
			{
				return this._problemstatus;
			}
			set
			{
				this._problemstatus = value;
			}
		}

		public string ProblemStatusDesc
		{
			get
			{
				if (this._pstatusdesc == null)
				{
					return string.Empty;
				}
				return this._pstatusdesc;
			}
			set
			{
				this._pstatusdesc = value;
			}
		}

		public string ProblemStatusName
		{
			get
			{
				if (this._pstatusname == null)
				{
					return string.Empty;
				}
				return this._pstatusname;
			}
			set
			{
				this._pstatusname = value;
			}
		}

		public CrmRefProblemStatus()
		{
		}

		public CrmRefProblemStatus(int ProblemStatus, string ProblemStatusName, string ProblemStatusDesc)
		{
			this._problemstatus = ProblemStatus;
			this._pstatusname = ProblemStatusName;
			this._pstatusdesc = ProblemStatusDesc;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CrmRefProblemStatus> listRef)
		{
			while (returnData.Read())
			{
				CrmRefProblemStatus crmRefProblemStatu = new CrmRefProblemStatus(GeneralConverter.ToInt32(returnData["ProblemStatus"]), Convert.ToString(returnData["StatusName"]), Convert.ToString(returnData["StatusDesc"]));
				listRef.Add(crmRefProblemStatu);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmRefProblemStatus> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_problem_status");
			List<CrmRefProblemStatus> crmRefProblemStatuses = new List<CrmRefProblemStatus>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmRefProblemStatus>(sqlCommand, new GenerateListFromReader<CrmRefProblemStatus>(CrmRefProblemStatus.GenerateListFromReader<CrmRefProblemStatus>), ref crmRefProblemStatuses);
			return crmRefProblemStatuses;
		}
	}
}