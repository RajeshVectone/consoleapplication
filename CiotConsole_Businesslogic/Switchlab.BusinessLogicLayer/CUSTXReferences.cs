using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	[Serializable]
	public class CUSTXReferences
	{
		protected const string SP_REF_AGENT_PHONE_STATUS = "r_agent_phone_status";

		private int _iID;

		private string _sID;

		private string _sName;

		private string _sDescription;

		public int NumberID
		{
			get
			{
				return this._iID;
			}
			set
			{
				this._iID = value;
			}
		}

		public string StringDescription
		{
			get
			{
				if (this._sDescription == null)
				{
					return string.Empty;
				}
				return this._sDescription;
			}
			set
			{
				this._sDescription = value;
			}
		}

		public string StringID
		{
			get
			{
				if (this._sID == null)
				{
					return string.Empty;
				}
				return this._sID;
			}
			set
			{
				this._sID = value;
			}
		}

		public string StringName
		{
			get
			{
				if (this._sName == null)
				{
					return string.Empty;
				}
				return this._sName;
			}
			set
			{
				this._sName = value;
			}
		}

		public CUSTXReferences()
		{
		}

		public CUSTXReferences(string sID, string sName, string sDescription)
		{
			this._sID = sID;
			this._sName = sName;
			this._sDescription = sDescription;
		}

		public CUSTXReferences(int iID, string sName, string sDescription)
		{
			this._iID = iID;
			this._sName = sName;
			this._sDescription = sDescription;
		}

		public CUSTXReferences(string sID, string sName) : this(sID, sName, string.Empty)
		{
		}

		public CUSTXReferences(int iID, string sName) : this(iID, sName, string.Empty)
		{
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader1<T>(IDataReader returnData, ref List<CUSTXReferences> theList)
		{
			while (returnData.Read())
			{
				CUSTXReferences cUSTXReference = new CUSTXReferences((int)returnData["id"], GeneralConverter.ToString(returnData["name"]));
				theList.Add(cUSTXReference);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CUSTXReferences> GetAllAgentPhoneStatus()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCustXConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_agent_phone_status");
			List<CUSTXReferences> cUSTXReferences = new List<CUSTXReferences>();
			sQLDataAccessLayer.ExecuteReaderCmd<CUSTXReferences>(sqlCommand, new GenerateListFromReader<CUSTXReferences>(CUSTXReferences.GenerateListFromReader1<CUSTXReferences>), ref cUSTXReferences);
			return cUSTXReferences;
		}
	}
}