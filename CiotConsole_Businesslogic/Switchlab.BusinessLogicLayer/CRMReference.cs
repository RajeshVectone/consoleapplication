using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	[Serializable]
	public class CRMReference
	{
		protected const string SP_REF_ORDER_STATUS = "r_order_status";

		protected const string SP_REF_PROBLEM_STATUS = "r_problem_status";

		protected const string SP_REF_PROBLEM_TYPE = "r_problem_type";

		protected const string SP_REF_PRODUCT_TYPE = "r_product_type";

		protected const string SP_REF_COURRIER = "r_courrier_select";

		private int _iID;

		private string _sID;

		private string _sName;

		private string _sDescription;

		public int NumberID
		{
			get
			{
				return this._iID;
			}
			set
			{
				this._iID = value;
			}
		}

		public string StringDescription
		{
			get
			{
				if (this._sDescription == null)
				{
					return string.Empty;
				}
				return this._sDescription;
			}
			set
			{
				this._sDescription = value;
			}
		}

		public string StringID
		{
			get
			{
				if (this._sID == null)
				{
					return string.Empty;
				}
				return this._sID;
			}
			set
			{
				this._sID = value;
			}
		}

		public string StringName
		{
			get
			{
				if (this._sName == null)
				{
					return string.Empty;
				}
				return this._sName;
			}
			set
			{
				this._sName = value;
			}
		}

		public CRMReference()
		{
		}

		public CRMReference(string sID, string sName, string sDescription)
		{
			this._sID = sID;
			this._sName = sName;
			this._sDescription = sDescription;
		}

		public CRMReference(int iID, string sName, string sDescription)
		{
			this._iID = iID;
			this._sName = sName;
			this._sDescription = sDescription;
		}

		public CRMReference(string sID, string sName) : this(sID, sName, string.Empty)
		{
		}

		public CRMReference(int iID, string sName) : this(iID, sName, string.Empty)
		{
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader1<T>(IDataReader returnData, ref List<CRMReference> theList)
		{
			while (returnData.Read())
			{
				CRMReference cRMReference = new CRMReference((int)returnData["id"], GeneralConverter.ToString(returnData["name"]), GeneralConverter.ToString(returnData["desc"]));
				theList.Add(cRMReference);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMReference> GetAllCourrier()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_courrier_select");
			List<CRMReference> cRMReferences = new List<CRMReference>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMReference>(sqlCommand, new GenerateListFromReader<CRMReference>(CRMReference.GenerateListFromReader1<CRMReference>), ref cRMReferences);
			return cRMReferences;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMReference> GetAllOrderStatus()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_order_status");
			List<CRMReference> cRMReferences = new List<CRMReference>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMReference>(sqlCommand, new GenerateListFromReader<CRMReference>(CRMReference.GenerateListFromReader1<CRMReference>), ref cRMReferences);
			return cRMReferences;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMReference> GetAllProblemStatus()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_problem_status");
			List<CRMReference> cRMReferences = new List<CRMReference>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMReference>(sqlCommand, new GenerateListFromReader<CRMReference>(CRMReference.GenerateListFromReader1<CRMReference>), ref cRMReferences);
			return cRMReferences;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMReference> GetAllProblemType()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_problem_type");
			List<CRMReference> cRMReferences = new List<CRMReference>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMReference>(sqlCommand, new GenerateListFromReader<CRMReference>(CRMReference.GenerateListFromReader1<CRMReference>), ref cRMReferences);
			return cRMReferences;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMReference> GetAllProductType()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_product_type");
			List<CRMReference> cRMReferences = new List<CRMReference>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMReference>(sqlCommand, new GenerateListFromReader<CRMReference>(CRMReference.GenerateListFromReader1<CRMReference>), ref cRMReferences);
			return cRMReferences;
		}
	}
}