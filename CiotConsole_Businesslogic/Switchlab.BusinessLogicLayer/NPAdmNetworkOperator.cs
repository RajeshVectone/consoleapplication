using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class NPAdmNetworkOperator
	{
		private const string SP_GET_NETWORK_OPERATOR_LIST = "networkOperator_listNetworkOperator";

		private const string SP_GET_SERVICE_OPERATOR_LIST = "networkOperator_listServiceOperator";

		private const string SP_GET_ALL_OPERATOR_LIST = "NetworkOperator_listAll";

		private string _networkID;

		private string _networkName;

		public string NetworkID
		{
			get
			{
				if (this._networkID == null)
				{
					return string.Empty;
				}
				return this._networkID;
			}
			set
			{
				this._networkID = value;
			}
		}

		public string NetworkName
		{
			get
			{
				if (this._networkName == null)
				{
					return string.Empty;
				}
				return this._networkName;
			}
			set
			{
				this._networkName = value;
			}
		}

		public NPAdmNetworkOperator()
		{
		}

		public NPAdmNetworkOperator(string networkID, string networkName)
		{
			this._networkID = networkID;
			this._networkName = networkName;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<NPAdmNetworkOperator> listRef)
		{
			while (returnData.Read())
			{
				NPAdmNetworkOperator nPAdmNetworkOperator = new NPAdmNetworkOperator(GeneralConverter.ToString(returnData["networkID"]), GeneralConverter.ToString(returnData["networkName"]));
				listRef.Add(nPAdmNetworkOperator);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<NPAdmNetworkOperator> GetListAllOperators()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NetworkOperator_listAll");
			List<NPAdmNetworkOperator> nPAdmNetworkOperators = new List<NPAdmNetworkOperator>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmNetworkOperator>(sqlCommand, new GenerateListFromReader<NPAdmNetworkOperator>(NPAdmNetworkOperator.GenerateListFromReader<NPAdmNetworkOperator>), ref nPAdmNetworkOperators);
			return nPAdmNetworkOperators;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<NPAdmNetworkOperator> GetListNetworkOperator()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "networkOperator_listNetworkOperator");
			List<NPAdmNetworkOperator> nPAdmNetworkOperators = new List<NPAdmNetworkOperator>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmNetworkOperator>(sqlCommand, new GenerateListFromReader<NPAdmNetworkOperator>(NPAdmNetworkOperator.GenerateListFromReader<NPAdmNetworkOperator>), ref nPAdmNetworkOperators);
			return nPAdmNetworkOperators;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<NPAdmNetworkOperator> GetListServiceOperator()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "networkOperator_listServiceOperator");
			List<NPAdmNetworkOperator> nPAdmNetworkOperators = new List<NPAdmNetworkOperator>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmNetworkOperator>(sqlCommand, new GenerateListFromReader<NPAdmNetworkOperator>(NPAdmNetworkOperator.GenerateListFromReader<NPAdmNetworkOperator>), ref nPAdmNetworkOperators);
			return nPAdmNetworkOperators;
		}
	}
}