using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class CallDetailRecord
	{
		private const string SP_CALL_GETCALLRECORDBYDATERANGE = "rp1_getcallrecordbydaterange";

		private const string SP_CALL_COUNTCALLRECORDBYDATERANGE = "rp1_countcallrecordbydaterange";

		private DateTime _cnxdate;

		private string _cli;

		private string _dialednum;

		private string _destcode;

		private double _talkcharge;

		private double _balance;

		private int _talktime;

		private string _currcode;

		public double Balance
		{
			get
			{
				return this._balance;
			}
			set
			{
				this._balance = value;
			}
		}

		public string CalledFrom
		{
			get
			{
				if (this._cli == null)
				{
					return string.Empty;
				}
				return this._cli;
			}
			set
			{
				this._cli = value;
			}
		}

		public DateTime ConnectionDate
		{
			get
			{
				return this._cnxdate;
			}
			set
			{
				this._cnxdate = value;
			}
		}

		public string CurrencyCode
		{
			get
			{
				if (this._currcode == null)
				{
					return string.Empty;
				}
				return this._currcode;
			}
			set
			{
				this._currcode = value;
			}
		}

		public string DestinationCode
		{
			get
			{
				if (this._destcode == null)
				{
					return string.Empty;
				}
				return this._destcode;
			}
			set
			{
				this._destcode = value;
			}
		}

		public string DialedNumber
		{
			get
			{
				if (this._dialednum == null)
				{
					return string.Empty;
				}
				return this._dialednum;
			}
			set
			{
				this._dialednum = value;
			}
		}

		public double TalkCharge
		{
			get
			{
				return this._talkcharge;
			}
			set
			{
				this._talkcharge = value;
			}
		}

		public int TalkTime
		{
			get
			{
				return this._talktime;
			}
			set
			{
				this._talktime = value;
			}
		}

		public CallDetailRecord()
		{
		}

		public CallDetailRecord(DateTime cnxDate, string calledFrom, string dialedNum, string destCode, double talkCharge, double balance, int talkTime, string currCode)
		{
			if (talkCharge < 0)
			{
				throw new ArgumentOutOfRangeException("talkCharge");
			}
			if (balance < 0)
			{
				throw new ArgumentOutOfRangeException("balance");
			}
			if (talkTime < 0)
			{
				throw new ArgumentOutOfRangeException("talkTime");
			}
			this._cnxdate = cnxDate;
			this._cli = calledFrom;
			this._dialednum = dialedNum;
			this._destcode = destCode;
			this._talkcharge = talkCharge;
			this._balance = balance;
			this._talktime = talkTime;
			this._currcode = currCode;
		}

		public static int CountCallDetailRecordByDateRange(string telcoCode, string custCode, int batchCode, int serialCode, DateTime startDate, DateTime endDate)
		{
			if (string.IsNullOrEmpty(telcoCode))
			{
				throw new ArgumentException("telcoCode");
			}
			if (string.IsNullOrEmpty(custCode))
			{
				throw new ArgumentException("custCode");
			}
			if (batchCode < 0)
			{
				throw new ArgumentOutOfRangeException("batchCode");
			}
			if (serialCode < 0)
			{
				throw new ArgumentOutOfRangeException("serialCode");
			}
			if (startDate > endDate)
			{
				throw new ArgumentException("daterange");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCDRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 0, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, telcoCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, custCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, batchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, serialCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@startdate", SqlDbType.DateTime, 0, ParameterDirection.Input, startDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@enddate", SqlDbType.DateTime, 0, ParameterDirection.Input, endDate);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_countcallrecordbydaterange");
			sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			return (int)sqlCommand.Parameters["@ReturnValue"].Value;
		}

		private static void GenerateCallDetailRecordListFromReader<T>(IDataReader returnData, ref List<CallDetailRecord> recordList)
		{
			while (returnData.Read())
			{
				CallDetailRecord callDetailRecord = new CallDetailRecord(Convert.ToDateTime(returnData["cnxdate"]), Convert.ToString(returnData["cli"]), Convert.ToString(returnData["dialednum"]), Convert.ToString(returnData["destcode"]), (returnData["talkcharge"] == DBNull.Value ? 0 : Convert.ToDouble(returnData["talkcharge"])), (returnData["balance"] == DBNull.Value ? 0 : Convert.ToDouble(returnData["balance"])), (returnData["talktime"] == DBNull.Value ? 0 : Convert.ToInt32(returnData["talktime"])), Convert.ToString(returnData["currcode"]));
				recordList.Add(callDetailRecord);
			}
		}

		public static List<CallDetailRecord> GetCallDetailRecordByDateRange(string telcoCode, string custCode, int batchCode, int serialCode, DateTime startDate, DateTime endDate, int rowsPerPage, int pageNumber)
		{
			if (string.IsNullOrEmpty(telcoCode))
			{
				throw new ArgumentException("telcoCode");
			}
			if (string.IsNullOrEmpty(custCode))
			{
				throw new ArgumentException("custCode");
			}
			if (batchCode < 0)
			{
				throw new ArgumentOutOfRangeException("batchCode");
			}
			if (serialCode < 0)
			{
				throw new ArgumentOutOfRangeException("serialCode");
			}
			if (startDate > endDate)
			{
				throw new ArgumentException("daterange");
			}
			if (rowsPerPage < 0)
			{
				throw new ArgumentOutOfRangeException("rowsPerPage");
			}
			if (pageNumber <= 0)
			{
				throw new ArgumentOutOfRangeException("pageNumber");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCDRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, telcoCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, custCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, batchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, serialCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@startdate", SqlDbType.DateTime, 0, ParameterDirection.Input, startDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@enddate", SqlDbType.DateTime, 0, ParameterDirection.Input, endDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@rowsperpage", SqlDbType.Int, 0, ParameterDirection.Input, rowsPerPage);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pagenumber", SqlDbType.Int, 0, ParameterDirection.Input, pageNumber);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_getcallrecordbydaterange");
			List<CallDetailRecord> callDetailRecords = new List<CallDetailRecord>();
			sQLDataAccessLayer.ExecuteReaderCmd<CallDetailRecord>(sqlCommand, new GenerateListFromReader<CallDetailRecord>(CallDetailRecord.GenerateCallDetailRecordListFromReader<CallDetailRecord>), ref callDetailRecords);
			return callDetailRecords;
		}
	}
}