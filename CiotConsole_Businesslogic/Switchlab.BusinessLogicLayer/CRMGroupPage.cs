using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMGroupPage
	{
		private const string SP_LIST = "grouppage_list";

		private const string SP_ADD = "grouppage_add";

		private int _igroupid;

		private int _ipageid;

		private int _imoduleid;

		public int GroupID
		{
			get
			{
				return this._igroupid;
			}
		}

		public int ModuleID
		{
			get
			{
				return this._imoduleid;
			}
		}

		public int PageID
		{
			get
			{
				return this._ipageid;
			}
		}

		public CRMGroupPage()
		{
		}

		public CRMGroupPage(int iGroupID, int iModuleID, int iPageID)
		{
			this._igroupid = iGroupID;
			this._imoduleid = iModuleID;
			this._ipageid = iPageID;
		}

		public static CRMGroupPage Create(int GroupID, int PageID, int moduleID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, GroupID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pageid", SqlDbType.Int, 0, ParameterDirection.Input, PageID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@moduleid", SqlDbType.Int, 0, ParameterDirection.Input, moduleID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "grouppage_add");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) <= 0)
			{
				return null;
			}
			return new CRMGroupPage(GroupID, moduleID, PageID);
		}

		private static void GenerateGroupPageListFromReader<T>(IDataReader returnData, ref List<CRMGroupPage> listNew)
		{
			while (returnData.Read())
			{
				CRMGroupPage cRMGroupPage = new CRMGroupPage(GeneralConverter.ToInt32(returnData["group_id"]), GeneralConverter.ToInt32(returnData["module_id"]), GeneralConverter.ToInt32(returnData["page_id"]));
				listNew.Add(cRMGroupPage);
			}
		}

		public static List<CRMGroupPage> GetList(int GroupID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, GroupID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "grouppage_list");
			List<CRMGroupPage> cRMGroupPages = new List<CRMGroupPage>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMGroupPage>(sqlCommand, new GenerateListFromReader<CRMGroupPage>(CRMGroupPage.GenerateGroupPageListFromReader<CRMGroupPage>), ref cRMGroupPages);
			return cRMGroupPages;
		}
	}
}