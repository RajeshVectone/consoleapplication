using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMGroup
	{
		private const string SP_LOADBYID = "group_loadbyid";

		private const string SP_LOADBYNAME = "group_loadbyname";

		private const string SP_LIST = "group_list";

		private const string SP_LISTALL = "group_listall";

		private const string SP_CREATE = "group_create";

		private const string SP_DELETE = "group_delete";

		private const string SP_UPDATE = "group_update";

		private const string SP_CLEARPAGELINK = "grouppage_clear";

		private int _igroupid;

		private string _sname;

		private string _sdesc;

		private string _scssfile;

		private int _itimeout;

		private int _imanager;

		private string _smanager;

		public string CSSFile
		{
			get
			{
				if (this._scssfile == null)
				{
					return string.Empty;
				}
				return this._scssfile;
			}
		}

		public string Description
		{
			get
			{
				if (this._sdesc == null)
				{
					return string.Empty;
				}
				return this._sdesc;
			}
		}

		public int GroupID
		{
			get
			{
				return this._igroupid;
			}
		}

		public int Manager
		{
			get
			{
				return this._imanager;
			}
		}

		public string ManagerStr
		{
			get
			{
				return this._smanager;
			}
		}

		public string Name
		{
			get
			{
				if (this._sname == null)
				{
					return string.Empty;
				}
				return this._sname;
			}
		}

		public int SessionTimeOut
		{
			get
			{
				return this._itimeout;
			}
		}

		public CRMGroup()
		{
		}

		public CRMGroup(int iGroupID, string sName, string sDescription, string sCSSFile, int iSessionTimeOut, int iManager, string sManager)
		{
			this._igroupid = iGroupID;
			this._sname = sName;
			this._sdesc = sDescription;
			this._scssfile = sCSSFile;
			this._itimeout = iSessionTimeOut;
			this._imanager = iManager;
			this._smanager = sManager;
		}

		public void ClearPageLink()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, this.GroupID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "grouppage_clear");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
		}

		public static CRMGroup Create(string sName, string sDescription, string sCSSFile, int iSessionTimeOut, int iManager)
		{
			if (sName == string.Empty)
			{
				throw new ArgumentNullException("sName");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 32, ParameterDirection.Input, sName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@desc", SqlDbType.VarChar, 128, ParameterDirection.Input, sDescription);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cssfile", SqlDbType.VarChar, 128, ParameterDirection.Input, sCSSFile);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@session_tmout", SqlDbType.Int, 0, ParameterDirection.Input, iSessionTimeOut);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@manager", SqlDbType.Int, 0, ParameterDirection.Input, iManager);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "group_create");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj == DBNull.Value)
			{
				return null;
			}
			return CRMGroup.GetByID(GeneralConverter.ToInt32(obj));
		}

		public static bool Delete(int iGroupID)
		{
			if (iGroupID < 0)
			{
				throw new ArgumentOutOfRangeException("iGroupID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, iGroupID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "group_delete");
			return sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0;
		}

		private static void GenerateGroupListFromReader<T>(IDataReader returnData, ref List<CRMGroup> listNew)
		{
			while (returnData.Read())
			{
				CRMGroup cRMGroup = new CRMGroup(GeneralConverter.ToInt32(returnData["group_id"]), Convert.ToString(returnData["name"]), Convert.ToString(returnData["desc"]), Convert.ToString(returnData["cssfile"]), GeneralConverter.ToInt32(returnData["session_tmout"]), GeneralConverter.ToInt32(returnData["manager"]), Convert.ToString(returnData["managerstr"]));
				listNew.Add(cRMGroup);
			}
		}

		public static CRMGroup GetByID(int iGroupID)
		{
			if (iGroupID < 0)
			{
				throw new ArgumentOutOfRangeException("iGroupID");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, iGroupID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "group_loadbyid");
			List<CRMGroup> cRMGroups = new List<CRMGroup>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMGroup>(sqlCommand, new GenerateListFromReader<CRMGroup>(CRMGroup.GenerateGroupListFromReader<CRMGroup>), ref cRMGroups);
			if (cRMGroups.Count <= 0)
			{
				return null;
			}
			return cRMGroups[0];
		}

		public static CRMGroup GetByName(string sName)
		{
			if (sName == null)
			{
				throw new ArgumentOutOfRangeException("sName");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 32, ParameterDirection.Input, sName);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "group_loadbyname");
			List<CRMGroup> cRMGroups = new List<CRMGroup>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMGroup>(sqlCommand, new GenerateListFromReader<CRMGroup>(CRMGroup.GenerateGroupListFromReader<CRMGroup>), ref cRMGroups);
			if (cRMGroups.Count <= 0)
			{
				return null;
			}
			return cRMGroups[0];
		}

		public static List<CRMGroup> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "group_listall");
			List<CRMGroup> cRMGroups = new List<CRMGroup>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMGroup>(sqlCommand, new GenerateListFromReader<CRMGroup>(CRMGroup.GenerateGroupListFromReader<CRMGroup>), ref cRMGroups);
			return cRMGroups;
		}

		public static List<CRMGroup> ListByManager(int iManager)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@manager", SqlDbType.Int, 0, ParameterDirection.Input, iManager);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "group_list");
			List<CRMGroup> cRMGroups = new List<CRMGroup>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMGroup>(sqlCommand, new GenerateListFromReader<CRMGroup>(CRMGroup.GenerateGroupListFromReader<CRMGroup>), ref cRMGroups);
			return cRMGroups;
		}

		public bool Update(string sName, string sDescription, string sCSSFile, int iSessionTimeOut, int iManager)
		{
			if (sName != this.Name && CRMGroup.GetByName(sName) != null)
			{
				throw new ArgumentException("New Group Name Already Exists");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, this.GroupID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 32, ParameterDirection.Input, sName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@desc", SqlDbType.VarChar, 128, ParameterDirection.Input, sDescription);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cssfile", SqlDbType.VarChar, 128, ParameterDirection.Input, sCSSFile);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@session_tmout", SqlDbType.Int, 0, ParameterDirection.Input, iSessionTimeOut);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@manager", SqlDbType.Int, 0, ParameterDirection.Input, iManager);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "group_update");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) <= 0)
			{
				return false;
			}
			this._sname = sName;
			this._sdesc = sDescription;
			this._scssfile = sCSSFile;
			this._itimeout = iSessionTimeOut;
			this._imanager = iManager;
			return true;
		}
	}
}