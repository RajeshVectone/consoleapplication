using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class jobs
	{
		private const string SP_UPDATE_SALES_ITEM = "update_jobs";

		private int _interval;

		private int _startTime;

		public int Interval
		{
			get
			{
				return this._interval;
			}
		}

		public int StartTime
		{
			get
			{
				return this._startTime;
			}
		}

		public jobs()
		{
		}

		public jobs(int interval, int startTime)
		{
			this._interval = interval;
			this._startTime = startTime;
		}

		public static bool UpdateProduct(int iInterval, int iStartTime)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetMSDBConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@interval", SqlDbType.Int, 0, ParameterDirection.Input, iInterval);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@startTime", SqlDbType.Int, 0, ParameterDirection.Input, iStartTime);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "update_jobs");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}
	}
}