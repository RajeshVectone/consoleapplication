using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmComment
	{
		private const string SP_INSERT_COMMENT = "NP_Insert_Comment";

		private const string SP_LIST_COMMENT_BY_TRANS_ID = "Comment_listByTransId";

		private int _transactionId;

		private int _commentNo;

		private string _comment;

		public string Comment
		{
			get
			{
				return this._comment;
			}
			set
			{
				this._comment = value;
			}
		}

		public int CommentNo
		{
			get
			{
				return this._commentNo;
			}
			set
			{
				this._commentNo = value;
			}
		}

		public int TransactionID
		{
			get
			{
				return this._transactionId;
			}
			set
			{
				this._transactionId = value;
			}
		}

		public NPAdmComment(int transId, int commentNo, string comment)
		{
			this._transactionId = transId;
			this._commentNo = commentNo;
			this._comment = comment;
		}

		private static void GenerateCommentListByTransIdFromReader<T>(IDataReader returnData, ref List<NPAdmComment> listNew)
		{
			while (returnData.Read())
			{
				NPAdmComment nPAdmComment = new NPAdmComment(GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToInt32(returnData["commentNo"]), GeneralConverter.ToString(returnData["comment"]));
				listNew.Add(nPAdmComment);
			}
		}

		public static bool InsertComment(int transId, int commentNo, string commentDesc)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TransID", SqlDbType.Int, 0, ParameterDirection.Input, transId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@CommentNo", SqlDbType.Int, 0, ParameterDirection.Input, commentNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@Comment", SqlDbType.VarChar, 255, ParameterDirection.Input, commentDesc);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Insert_Comment");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		public static List<NPAdmComment> ListCommentByTransId(int transId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TransID", SqlDbType.Int, 0, ParameterDirection.Input, transId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Comment_listByTransId");
			List<NPAdmComment> nPAdmComments = new List<NPAdmComment>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmComment>(sqlCommand, new GenerateListFromReader<NPAdmComment>(NPAdmComment.GenerateCommentListByTransIdFromReader<NPAdmComment>), ref nPAdmComments);
			return nPAdmComments;
		}
	}
}