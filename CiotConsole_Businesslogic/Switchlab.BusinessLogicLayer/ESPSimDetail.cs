using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class ESPSimDetail
	{
		private const string SP_VIEW_BY_ICCID = "sim_detail_view_by_iccid";

		private int _iindex;

		private string _smsisdn;

		private string _siccid;

		private string _simsi;

		private string _spin1;

		private string _spuk1;

		private string _spin2;

		private string _spuk2;

		public string ICCID
		{
			get
			{
				if (this._siccid == null)
				{
					return string.Empty;
				}
				return this._siccid;
			}
			set
			{
				this._siccid = value;
			}
		}

		public string IMSI
		{
			get
			{
				if (this._simsi == null)
				{
					return string.Empty;
				}
				return this._simsi;
			}
			set
			{
				this._simsi = value;
			}
		}

		public int Index
		{
			get
			{
				return this._iindex;
			}
			set
			{
				this._iindex = value;
			}
		}

		public string MSISDN
		{
			get
			{
				if (this._smsisdn == null)
				{
					return string.Empty;
				}
				return this._smsisdn;
			}
			set
			{
				this._smsisdn = value;
			}
		}

		public string Pin1
		{
			get
			{
				if (this._spin1 == null)
				{
					return string.Empty;
				}
				return this._spin1;
			}
			set
			{
				this._spin1 = value;
			}
		}

		public string Pin2
		{
			get
			{
				if (this._spin2 == null)
				{
					return string.Empty;
				}
				return this._spin2;
			}
			set
			{
				this._spin2 = value;
			}
		}

		public string PUK1
		{
			get
			{
				if (this._spuk1 == null)
				{
					return string.Empty;
				}
				return this._spuk1;
			}
			set
			{
				this._spuk1 = value;
			}
		}

		public string PUK2
		{
			get
			{
				if (this._spuk2 == null)
				{
					return string.Empty;
				}
				return this._spuk2;
			}
			set
			{
				this._spuk2 = value;
			}
		}

		public ESPSimDetail()
		{
		}

		public ESPSimDetail(int iIndex, string sMSISDN, string sICCID, string sIMSI, string sPin1, string sPUK1, string sPin2, string sPUK2)
		{
			this._iindex = iIndex;
			this._smsisdn = sMSISDN;
			this._siccid = sICCID;
			this._simsi = sIMSI;
			this._spin1 = sPin1;
			this._spuk1 = sPUK1;
			this._spin2 = sPin2;
			this._spuk2 = sPUK2;
		}

		private static void GenerateSimDetailFromReader<T>(IDataReader returnData, ref List<ESPSimDetail> listNew)
		{
			while (returnData.Read())
			{
				ESPSimDetail eSPSimDetail = new ESPSimDetail(GeneralConverter.ToInt32(returnData["idx"]), GeneralConverter.ToString(returnData["msisdn"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToString(returnData["imsi"]), GeneralConverter.ToString(returnData["pin1"]), GeneralConverter.ToString(returnData["puk1"]), GeneralConverter.ToString(returnData["pin2"]), GeneralConverter.ToString(returnData["puk2"]));
				listNew.Add(eSPSimDetail);
			}
		}

		public static ESPSimDetail ViewByICCID(string sICCID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, sICCID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sim_detail_view_by_iccid");
			List<ESPSimDetail> eSPSimDetails = new List<ESPSimDetail>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPSimDetail>(sqlCommand, new GenerateListFromReader<ESPSimDetail>(ESPSimDetail.GenerateSimDetailFromReader<ESPSimDetail>), ref eSPSimDetails);
			if (eSPSimDetails == null || eSPSimDetails.Count <= 0)
			{
				return null;
			}
			return eSPSimDetails[0];
		}
	}
}