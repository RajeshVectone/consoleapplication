using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class NPNLUpdatingNumberRange
	{
		private const string SP_UPDATING_NUMBER_RANGE = "add_nbrange";

		private string _firstNumber;

		private string _lastNumber;

		private string _rcid;

		private string _todaydate;

		public string FirstNumber
		{
			get
			{
				return this._firstNumber;
			}
			set
			{
				this._firstNumber = value;
			}
		}

		public string LastNumber
		{
			get
			{
				return this._lastNumber;
			}
			set
			{
				this._lastNumber = value;
			}
		}

		public string RCID
		{
			get
			{
				return this._rcid;
			}
			set
			{
				this._rcid = value;
			}
		}

		public string Todaydate
		{
			get
			{
				return this._todaydate;
			}
			set
			{
				this._todaydate = value;
			}
		}

		public NPNLUpdatingNumberRange()
		{
		}

		public NPNLUpdatingNumberRange(string firstnumber, string lastnumber, string rcid, string todaydate)
		{
			this._firstNumber = firstnumber;
			this._lastNumber = lastnumber;
			this._rcid = rcid;
			this._todaydate = todaydate;
		}

		public void doUpdateNumberRange()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNLTempConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstnb", SqlDbType.VarChar, 0, ParameterDirection.Input, this._firstNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastnb", SqlDbType.VarChar, 0, ParameterDirection.Input, this._lastNumber);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@rcid", SqlDbType.VarChar, 0, ParameterDirection.Input, this._rcid);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@date", SqlDbType.VarChar, 0, ParameterDirection.Input, this._todaydate);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "add_nbrange");
			sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
		}
	}
}