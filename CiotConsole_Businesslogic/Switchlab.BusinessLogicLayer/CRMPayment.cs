using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMPayment
	{
		private const string SP_CREATE = "payment_new";

		private const string SP_LIST_BY_NO = "cc_search_by_no";

		private string _refno;

		private double _paidamount;

		private int _paymenttype;

		private int _paymentstatus;

		private string _submitby;

		private int _creditcardid;

		public int CreditCardId
		{
			get
			{
				return this._creditcardid;
			}
			set
			{
				this._creditcardid = value;
			}
		}

		public double PaidAmount
		{
			get
			{
				return this._paidamount;
			}
			set
			{
				this._paidamount = value;
			}
		}

		public int PaymentStatus
		{
			get
			{
				return this._paymentstatus;
			}
			set
			{
				this._paymentstatus = value;
			}
		}

		public int PaymentType
		{
			get
			{
				return this._paymenttype;
			}
			set
			{
				this._paymenttype = value;
			}
		}

		public string RefNo
		{
			get
			{
				if (this._refno == null)
				{
					return string.Empty;
				}
				return this._refno;
			}
			set
			{
				this._refno = value;
			}
		}

		public string SubmitBy
		{
			get
			{
				if (this._submitby == null)
				{
					return string.Empty;
				}
				return this._submitby;
			}
			set
			{
				this._submitby = value;
			}
		}

		public CRMPayment()
		{
		}

		public CRMPayment(string refNo, double paidAmount, int paymentType, int paymentStatus, string submitBy, int creditCard)
		{
			this._refno = refNo;
			this._paidamount = paidAmount;
			this._paymenttype = paymentType;
			this._paymentstatus = paymentStatus;
			this._submitby = submitBy;
			this._creditcardid = creditCard;
		}

		public static bool AddNewPayment(string iRefNo, double iPaidAmount, int iPaymentType, int iPaymentStatus, string iSubmitBy, int iCreditCard, ref int iPaymentID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ref_no", SqlDbType.VarChar, 64, ParameterDirection.Input, iRefNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@paidamount", SqlDbType.Float, 0, ParameterDirection.Input, iPaidAmount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@paymenttype", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@paymentstatus", SqlDbType.Int, 0, ParameterDirection.Input, iPaymentStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@submitby", SqlDbType.VarChar, 16, ParameterDirection.Input, iSubmitBy);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@creditcardid", SqlDbType.Int, 0, ParameterDirection.Input, iCreditCard);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@paymentid", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "payment_new");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) == DBNull.Value || sqlCommand.Parameters["@paymentid"] == null || (int)sqlCommand.Parameters["@paymentid"].Value == -1)
			{
				return false;
			}
			iPaymentID = (int)sqlCommand.Parameters["@paymentid"].Value;
			return true;
		}
	}
}