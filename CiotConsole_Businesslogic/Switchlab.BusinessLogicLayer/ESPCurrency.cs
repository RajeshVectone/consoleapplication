using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	[Serializable]
	public class ESPCurrency
	{
		protected const string SP_GET_CURRENCY_DETAIL_BY_ID = "curr_get_by_id";

		protected const string SP_GET_LIST_CURRENCY = "curr_get_all";

		protected const string SP_CURRENCY_NEW = "curr_new";

		protected const string SP_CURRENCY_EDIT = "curr_edit";

		protected const string SP_CURRENCY_DELETE = "curr_delete";

		protected const string MANAGE_SELECT_SP = "rp1_currency_manage_select";

		protected const string MANAGE_DETAIL_SP = "rp1_currency_manage_detail";

		protected const string MANAGE_DELETE_SP = "rp1_currency_manage_delete";

		protected const string MANAGE_INSERT_SP = "rp1_currency_manage_insert";

		protected const string MANAGE_UPDATE_SP = "rp1_currency_manage_update";

		private string _ssitecode;

		private string _scurrcode;

		private double _dunitexcg;

		private string _sname;

		private string _spr_sing;

		private string _spr_plur;

		private string _spr_fsing;

		private string _spr_fplur;

		protected int _resellerid;

		public string CurrencyCode
		{
			get
			{
				if (this._scurrcode == null)
				{
					return string.Empty;
				}
				return this._scurrcode;
			}
			set
			{
				this._scurrcode = value;
			}
		}

		public string CurrencyName
		{
			get
			{
				if (this._sname == null)
				{
					return string.Empty;
				}
				return this._sname;
			}
			set
			{
				this._sname = value;
			}
		}

		public string Pr_FPlur
		{
			get
			{
				if (this._spr_fplur == null)
				{
					return string.Empty;
				}
				return this._spr_fplur;
			}
			set
			{
				this._spr_fplur = value;
			}
		}

		public string Pr_FSing
		{
			get
			{
				if (this._spr_fsing == null)
				{
					return string.Empty;
				}
				return this._spr_fsing;
			}
			set
			{
				this._spr_fsing = value;
			}
		}

		public string Pr_Plur
		{
			get
			{
				if (this._spr_plur == null)
				{
					return string.Empty;
				}
				return this._spr_plur;
			}
			set
			{
				this._spr_plur = value;
			}
		}

		public string Pr_Sing
		{
			get
			{
				if (this._spr_sing == null)
				{
					return string.Empty;
				}
				return this._spr_sing;
			}
			set
			{
				this._spr_sing = value;
			}
		}

		public string SiteCode
		{
			get
			{
				if (this._ssitecode == null)
				{
					return string.Empty;
				}
				return this._ssitecode;
			}
			set
			{
				this._ssitecode = value;
			}
		}

		public double UniteXcg
		{
			get
			{
				return this._dunitexcg;
			}
			set
			{
				this._dunitexcg = value;
			}
		}

		public ESPCurrency()
		{
		}

		public ESPCurrency(string sSiteCode, string sCurrCode, double dUniteXcg, string sName, string sPr_sing, string sPr_plur, string sPr_fsing, string sPr_fplur)
		{
			this._ssitecode = sSiteCode;
			this._scurrcode = sCurrCode;
			this._dunitexcg = dUniteXcg;
			this._sname = sName;
			this._spr_sing = sPr_sing;
			this._spr_plur = sPr_plur;
			this._spr_fsing = sPr_fsing;
			this._spr_fplur = sPr_fplur;
		}

		public static bool DeleteCurrency(string sSiteCode, string sCurrencyCode)
		{
			if (string.IsNullOrEmpty(sSiteCode))
			{
				throw new ArgumentOutOfRangeException("Site Code");
			}
			if (string.IsNullOrEmpty(sCurrencyCode))
			{
				throw new ArgumentOutOfRangeException("Currency Code");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, sSiteCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrencyCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "curr_delete");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		public static bool EditCurrency(string sSiteCode, string sCurrencyCode, double dUniteXcg, string sCurrencyName, string sPr_sing, string sPr_plur, string sPr_fsing, string sPr_fplur)
		{
			if (string.IsNullOrEmpty(sSiteCode))
			{
				throw new ArgumentOutOfRangeException("Site Code");
			}
			if (string.IsNullOrEmpty(sCurrencyCode))
			{
				throw new ArgumentOutOfRangeException("Currency Code");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, sSiteCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrencyCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@unitexcg", SqlDbType.Float, 0, ParameterDirection.Input, dUniteXcg);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 20, ParameterDirection.Input, sCurrencyName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pr_sing", SqlDbType.VarChar, 8, ParameterDirection.Input, sPr_sing);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pr_plur", SqlDbType.VarChar, 8, ParameterDirection.Input, sPr_plur);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pr_fsing", SqlDbType.VarChar, 8, ParameterDirection.Input, sPr_fsing);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pr_fplur", SqlDbType.VarChar, 8, ParameterDirection.Input, sPr_fplur);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "curr_edit");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListCurrencyFromReader<T>(IDataReader returnData, ref List<ESPCurrency> theList)
		{
			while (returnData.Read())
			{
				ESPCurrency eSPCurrency = new ESPCurrency(GeneralConverter.ToString(returnData["sitecode"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToDouble(returnData["unitexcg"]), GeneralConverter.ToString(returnData["name"]), GeneralConverter.ToString(returnData["pr_sing"]), GeneralConverter.ToString(returnData["pr_plur"]), GeneralConverter.ToString(returnData["pr_fsing"]), GeneralConverter.ToString(returnData["pr_fplur"]));
				theList.Add(eSPCurrency);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<ESPCurrency> theList)
		{
			while (returnData.Read())
			{
				ESPCurrency eSPCurrency = new ESPCurrency(Convert.ToString(returnData["sitecode"]), Convert.ToString(returnData["currcode"]), (double)returnData["unitexcg"], Convert.ToString(returnData["name"]), Convert.ToString(returnData["pr_sing"]), Convert.ToString(returnData["pr_plur"]), Convert.ToString(returnData["pr_fsing"]), Convert.ToString(returnData["pr_fplur"]));
				theList.Add(eSPCurrency);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static ESPCurrency GetByCurrId(string sSiteCode, string sCurrCode)
		{
			if (string.IsNullOrEmpty(sCurrCode))
			{
				throw new ArgumentOutOfRangeException("Currency Code");
			}
			if (string.IsNullOrEmpty(sSiteCode))
			{
				throw new ArgumentOutOfRangeException("SiteCode");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, sSiteCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "curr_get_by_id");
			List<ESPCurrency> eSPCurrencies = new List<ESPCurrency>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPCurrency>(sqlCommand, new GenerateListFromReader<ESPCurrency>(ESPCurrency.GenerateListCurrencyFromReader<ESPCurrency>), ref eSPCurrencies);
			if (eSPCurrencies.Count <= 0)
			{
				return null;
			}
			return eSPCurrencies[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static ESPCurrency GetById(int __resellerid, string __sitecode, string __currcode)
		{
			if (string.IsNullOrEmpty(__sitecode) || string.IsNullOrEmpty(__currcode))
			{
				throw new ArgumentOutOfRangeException("sitecode;currcode");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, __sitecode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, __currcode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_currency_manage_detail");
			List<ESPCurrency> eSPCurrencies = new List<ESPCurrency>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPCurrency>(sqlCommand, new GenerateListFromReader<ESPCurrency>(ESPCurrency.GenerateListFromReader<ESPCurrency>), ref eSPCurrencies);
			if (eSPCurrencies.Count <= 0)
			{
				return null;
			}
			return eSPCurrencies[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static List<ESPCurrency> GetListCurrency(string sSiteCode)
		{
			if (string.IsNullOrEmpty(sSiteCode))
			{
				throw new ArgumentOutOfRangeException("SiteCode");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, sSiteCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "curr_get_all");
			List<ESPCurrency> eSPCurrencies = new List<ESPCurrency>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPCurrency>(sqlCommand, new GenerateListFromReader<ESPCurrency>(ESPCurrency.GenerateListCurrencyFromReader<ESPCurrency>), ref eSPCurrencies);
			if (eSPCurrencies.Count > 0)
			{
				return eSPCurrencies;
			}
			return null;
		}

		public static bool InsertCurrency(string sSiteCode, string sCurrencyCode, double dUniteXcg, string sCurrencyName, string sPr_sing, string sPr_plur, string sPr_fsing, string sPr_fplur)
		{
			if (string.IsNullOrEmpty(sSiteCode))
			{
				throw new ArgumentOutOfRangeException("Site Code");
			}
			if (string.IsNullOrEmpty(sCurrencyCode))
			{
				throw new ArgumentOutOfRangeException("Currency Code");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sitecode", SqlDbType.VarChar, 3, ParameterDirection.Input, sSiteCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrencyCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@unitexcg", SqlDbType.Float, 0, ParameterDirection.Input, dUniteXcg);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@name", SqlDbType.VarChar, 20, ParameterDirection.Input, sCurrencyName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pr_sing", SqlDbType.VarChar, 8, ParameterDirection.Input, sPr_sing);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pr_plur", SqlDbType.VarChar, 8, ParameterDirection.Input, sPr_plur);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pr_fsing", SqlDbType.VarChar, 8, ParameterDirection.Input, sPr_fsing);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@pr_fplur", SqlDbType.VarChar, 8, ParameterDirection.Input, sPr_fplur);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "curr_new");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}
	}
}