using Switchlab.DataAccessLayer;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class NRDBNpOppsMessage : NRDBNpMessage
	{
		public NRDBNpOppsMessage()
		{
		}

		public override bool Save(NRDBMessage message)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNRDBConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@msgtype_id", SqlDbType.Int, 0, ParameterDirection.Input, message.TypeId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@caseId", SqlDbType.BigInt, 0, ParameterDirection.Input, message.CaseId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sequence", SqlDbType.Int, 0, ParameterDirection.Input, message.Sequence);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@auth_ref", SqlDbType.VarChar, 254, ParameterDirection.Input, message.AuthRef);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cust_id", SqlDbType.VarChar, 254, ParameterDirection.Input, message.CustId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cust_name", SqlDbType.VarChar, 254, ParameterDirection.Input, message.CustName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@subscr_type", SqlDbType.Char, 1, ParameterDirection.Input, message.SubscribeTypeCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telp_number_main", SqlDbType.VarChar, 254, ParameterDirection.Input, message.TelephoneNumberMain);
			string[] telephoneNumberAdd = message.TelephoneNumberAdd;
			if (telephoneNumberAdd != null)
			{
				for (int i = 0; i < (int)telephoneNumberAdd.Length; i++)
				{
					sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, string.Concat("@telp_number_add_", i + 1), SqlDbType.VarChar, 254, ParameterDirection.Input, telephoneNumberAdd[i]);
				}
			}
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@port_datetime", SqlDbType.DateTime, 0, ParameterDirection.Input, message.PortingDate);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "send_NP_Message");
			return sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0;
		}
	}
}