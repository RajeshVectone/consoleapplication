using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMPaymentHistory
	{
		private const string SP_GET_BY_MOBILE_NO = "get_history_payment_by_mobileno";

		private const string SP_GET_ALL = "get_history_payment";

		private int _iorderid;

		private string _sfullname;

		private string _sfullamount;

		private DateTime _dtrequestdate;

		private DateTime _dttransdate;

		private string _sprocessby;

		private string _sresult;

		private int _ireasonfail;

		public string FullAmount
		{
			get
			{
				if (this._sfullamount == null)
				{
					return string.Empty;
				}
				return this._sfullamount;
			}
			set
			{
				this._sfullamount = value;
			}
		}

		public string FullName
		{
			get
			{
				if (this._sfullname == null)
				{
					return string.Empty;
				}
				return this._sfullname;
			}
			set
			{
				this._sfullname = value;
			}
		}

		public int OrderID
		{
			get
			{
				return this._iorderid;
			}
			set
			{
				this._iorderid = value;
			}
		}

		public string ProcessBy
		{
			get
			{
				if (this._sprocessby == null)
				{
					return string.Empty;
				}
				return this._sprocessby;
			}
			set
			{
				this._sprocessby = value;
			}
		}

		public int ReasonFail
		{
			get
			{
				return this._ireasonfail;
			}
			set
			{
				this._ireasonfail = value;
			}
		}

		public DateTime RequestDate
		{
			get
			{
				return this._dtrequestdate;
			}
			set
			{
				this._dtrequestdate = value;
			}
		}

		public string Result
		{
			get
			{
				if (this._sresult == null)
				{
					return string.Empty;
				}
				return this._sresult;
			}
			set
			{
				this._sresult = value;
			}
		}

		public DateTime TransDate
		{
			get
			{
				return this._dttransdate;
			}
			set
			{
				this._dttransdate = value;
			}
		}

		public CRMPaymentHistory()
		{
		}

		public CRMPaymentHistory(int iOrderID, string sFullName, string sFullAmount, DateTime dtRequestDate, DateTime dtTransDate, string sProcessBy, string sResult, int iReasonFail)
		{
			this._iorderid = iOrderID;
			this._sfullname = sFullName;
			this._sfullamount = sFullAmount;
			this._dtrequestdate = dtRequestDate;
			this._dttransdate = dtTransDate;
			this._sprocessby = sProcessBy;
			this._sresult = sResult;
			this._ireasonfail = iReasonFail;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CRMPaymentHistory> theList)
		{
			while (returnData.Read())
			{
				CRMPaymentHistory cRMPaymentHistory = new CRMPaymentHistory(GeneralConverter.ToInt32(returnData["orderid"]), GeneralConverter.ToString(returnData["fullname"]), GeneralConverter.ToString(returnData["fullamount"]), GeneralConverter.ToDateTime(returnData["requestdate"]), GeneralConverter.ToDateTime(returnData["transdate"]), GeneralConverter.ToString(returnData["processby"]), GeneralConverter.ToString(returnData["result"]), GeneralConverter.ToInt32(returnData["reasonfail"]));
				theList.Add(cRMPaymentHistory);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMPaymentHistory> GetAllHistory()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_history_payment");
			List<CRMPaymentHistory> cRMPaymentHistories = new List<CRMPaymentHistory>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPaymentHistory>(sqlCommand, new GenerateListFromReader<CRMPaymentHistory>(CRMPaymentHistory.GenerateListFromReader<CRMPaymentHistory>), ref cRMPaymentHistories);
			if (cRMPaymentHistories != null && cRMPaymentHistories.Count > 0)
			{
				return cRMPaymentHistories;
			}
			return null;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMPaymentHistory> GetHistoryByMobileNo(string sMobileNo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileno", SqlDbType.VarChar, 20, ParameterDirection.Input, sMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_history_payment_by_mobileno");
			List<CRMPaymentHistory> cRMPaymentHistories = new List<CRMPaymentHistory>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMPaymentHistory>(sqlCommand, new GenerateListFromReader<CRMPaymentHistory>(CRMPaymentHistory.GenerateListFromReader<CRMPaymentHistory>), ref cRMPaymentHistories);
			if (cRMPaymentHistories != null && cRMPaymentHistories.Count > 0)
			{
				return cRMPaymentHistories;
			}
			return null;
		}
	}
}