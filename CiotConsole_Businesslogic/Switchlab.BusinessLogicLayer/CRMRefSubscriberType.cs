using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class CRMRefSubscriberType
	{
		private const string SP_LIST = "r_subscriber_type";

		private int _isubscribertype;

		private string _stypename;

		private string _stypedesc;

		public string SubscriberStatusDesc
		{
			get
			{
				if (this._stypedesc == null)
				{
					return string.Empty;
				}
				return this._stypedesc;
			}
			set
			{
				this._stypedesc = value;
			}
		}

		public int SubscriberType
		{
			get
			{
				return this._isubscribertype;
			}
			set
			{
				this._isubscribertype = value;
			}
		}

		public string SubscriberTypeName
		{
			get
			{
				if (this._stypename == null)
				{
					return string.Empty;
				}
				return this._stypename;
			}
			set
			{
				this._stypename = value;
			}
		}

		public CRMRefSubscriberType()
		{
		}

		public CRMRefSubscriberType(int iSubscriberType, string sSubscriberTypeName, string sSubscriberStatusDesc)
		{
			this._isubscribertype = iSubscriberType;
			this._stypename = sSubscriberTypeName;
			this._stypedesc = sSubscriberStatusDesc;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CRMRefSubscriberType> listRef)
		{
			while (returnData.Read())
			{
				CRMRefSubscriberType cRMRefSubscriberType = new CRMRefSubscriberType(GeneralConverter.ToInt32(returnData["subscribertype"]), Convert.ToString(returnData["typename"]), Convert.ToString(returnData["typedesc"]));
				listRef.Add(cRMRefSubscriberType);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMRefSubscriberType> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_subscriber_type");
			List<CRMRefSubscriberType> cRMRefSubscriberTypes = new List<CRMRefSubscriberType>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRefSubscriberType>(sqlCommand, new GenerateListFromReader<CRMRefSubscriberType>(CRMRefSubscriberType.GenerateListFromReader<CRMRefSubscriberType>), ref cRMRefSubscriberTypes);
			return cRMRefSubscriberTypes;
		}
	}
}