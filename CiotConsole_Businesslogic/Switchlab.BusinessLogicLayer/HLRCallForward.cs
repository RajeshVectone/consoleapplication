using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class HLRCallForward
	{
		private const string SP_DEACTIVATE_CALL_FORWARDED_BY_IMSI = "crm_callforward_deactivate";

		private const string SP_ACTIVATE_CALL_FORWARDED_BY_IMSI = "crm_callforward_activate";

		private const string SP_UPDATE_CALL_FORWARDED_BY_IMSI = "crm_callforward_update";

		private const string SP_CHECK_CALLFORWARDED_BY_IMSI = "crm_callforward_check";

		private string _sIMSI;

		private int _icfwstatus;

		private string _scfwstatusname;

		private int _icfwtype;

		private string _scfwtypename;

		private int _iactivationstate;

		private int _iregistrationstate;

		private int _ihlrinductionstate;

		private string _sforwardtonb;

		private string _ssubaddress;

		private int _inoreplytimer;

		private int _iservicecode;

		private int _iservicetype;

		private string _sservicetypename;

		public int ActivationState
		{
			get
			{
				return this._iactivationstate;
			}
			set
			{
				this._iactivationstate = value;
			}
		}

		public int CallForwardStatus
		{
			get
			{
				return this._icfwstatus;
			}
			set
			{
				this._icfwstatus = value;
			}
		}

		public string CallForwardStatusName
		{
			get
			{
				if (this._scfwstatusname == null)
				{
					return string.Empty;
				}
				return this._scfwstatusname;
			}
			set
			{
				this._scfwstatusname = value;
			}
		}

		public int CallForwardType
		{
			get
			{
				return this._icfwtype;
			}
			set
			{
				this._icfwtype = value;
			}
		}

		public string CallForwardTypeDefinition
		{
			get
			{
				switch (this._icfwtype)
				{
					case 41:
					{
						return "Call Forward Busy";
					}
					case 42:
					{
						return "Call Forward No Reply";
					}
					case 43:
					{
						return "Call Forward Unreachable";
					}
				}
				return string.Empty;
			}
		}

		public string CallForwardTypeName
		{
			get
			{
				if (this._scfwtypename == null)
				{
					return string.Empty;
				}
				return this._scfwtypename;
			}
			set
			{
				this._scfwtypename = value;
			}
		}

		public string ForwardToNumber
		{
			get
			{
				if (this._sforwardtonb == null)
				{
					return string.Empty;
				}
				return this._sforwardtonb;
			}
			set
			{
				this._sforwardtonb = value;
			}
		}

		public int HLRInductionState
		{
			get
			{
				return this._ihlrinductionstate;
			}
			set
			{
				this._ihlrinductionstate = value;
			}
		}

		public string IMSI
		{
			get
			{
				if (this._sIMSI == null)
				{
					return string.Empty;
				}
				return this._sIMSI;
			}
			set
			{
				this._sIMSI = value;
			}
		}

		public int NoReplyTimer
		{
			get
			{
				return this._inoreplytimer;
			}
			set
			{
				this._inoreplytimer = value;
			}
		}

		public int RegistrationState
		{
			get
			{
				return this._iregistrationstate;
			}
			set
			{
				this._iregistrationstate = value;
			}
		}

		public int ServiceCode
		{
			get
			{
				return this._iservicecode;
			}
			set
			{
				this._iservicecode = value;
			}
		}

		public int ServiceType
		{
			get
			{
				return this._iservicetype;
			}
			set
			{
				this._iservicetype = value;
			}
		}

		public string ServiceTypeName
		{
			get
			{
				if (this._sservicetypename == null)
				{
					return string.Empty;
				}
				return this._sservicetypename;
			}
			set
			{
				this._sservicetypename = value;
			}
		}

		public string SubAddress
		{
			get
			{
				if (this._ssubaddress == null)
				{
					return string.Empty;
				}
				return this._ssubaddress;
			}
			set
			{
				this._ssubaddress = value;
			}
		}

		public HLRCallForward()
		{
		}

		public HLRCallForward(string sIMSI, int iCallForwardType, string sCallForwardTypeName, int iActivationState, int iRegistrationState, int iHLRInductionState, string sForwardToNumber, string sSubAddress, int iNoReplyTimer, int iServiceCode, int iServiceType, string sServiceTypeName)
		{
			this._sIMSI = sIMSI;
			this._icfwtype = iCallForwardType;
			this._scfwtypename = sCallForwardTypeName;
			this._iactivationstate = iActivationState;
			this._iregistrationstate = iRegistrationState;
			this._ihlrinductionstate = iHLRInductionState;
			this._sforwardtonb = sForwardToNumber;
			this._ssubaddress = sSubAddress;
			this._inoreplytimer = iNoReplyTimer;
			this._iservicecode = iServiceCode;
			this._iservicetype = iServiceType;
			this._sservicetypename = sServiceTypeName;
		}

		public HLRCallForward(int iCallForwardType, string sCallForwardTypeName, string sForwardToNumber, int iNoReplyTimer, int iCallForwardStatus, string sCallForwardStatusName)
		{
			this._icfwtype = iCallForwardType;
			this._scfwtypename = sCallForwardTypeName;
			this._sforwardtonb = sForwardToNumber;
			this._inoreplytimer = iNoReplyTimer;
			this._icfwstatus = iCallForwardStatus;
			this._scfwstatusname = sCallForwardStatusName;
		}

		public static int ActivateForwarded(string sICCID, int iCFWType, string sForwardToNb, ref string resultMessage)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetHLRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, sICCID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cfwtype", SqlDbType.Int, 0, ParameterDirection.Input, iCFWType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@forwardToNb", SqlDbType.VarChar, 20, ParameterDirection.Input, sForwardToNb);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "crm_callforward_activate");
			DataSet dataSet = sQLDataAccessLayer.ExecuteQuery(sqlCommand);
			int num = GeneralConverter.ToInt32(dataSet.Tables[0].Rows[0]["err_code"]);
			resultMessage = dataSet.Tables[0].Rows[0]["err_message"].ToString();
			return num;
		}

		public static List<HLRCallForward> CheckForwardedService(string sICCID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetHLRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, sICCID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "crm_callforward_check");
			List<HLRCallForward> hLRCallForwards = new List<HLRCallForward>();
			sQLDataAccessLayer.ExecuteReaderCmd<HLRCallForward>(sqlCommand, new GenerateListFromReader<HLRCallForward>(HLRCallForward.GenerateListVoiceFromReader<HLRCallForward>), ref hLRCallForwards);
			if (hLRCallForwards != null && hLRCallForwards.Count > 0)
			{
				return hLRCallForwards;
			}
			return null;
		}

		public static int DeactivateForwarder(string sICCID, int iCFWType, ref string resultMessage)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetHLRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, sICCID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cfwtype", SqlDbType.Int, 0, ParameterDirection.Input, iCFWType);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "crm_callforward_deactivate");
			DataSet dataSet = sQLDataAccessLayer.ExecuteQuery(sqlCommand);
			int num = GeneralConverter.ToInt32(dataSet.Tables[0].Rows[0]["err_code"]);
			resultMessage = dataSet.Tables[0].Rows[0]["err_message"].ToString();
			return num;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<HLRCallForward> listNew)
		{
			while (returnData.Read())
			{
				HLRCallForward hLRCallForward = new HLRCallForward(GeneralConverter.ToString(returnData["IMSI"]), GeneralConverter.ToInt32(returnData["CFWType"]), GeneralConverter.ToString(returnData["CFWTypeName"]), GeneralConverter.ToInt32(returnData["ActivationState"]), GeneralConverter.ToInt32(returnData["RegistrationState"]), GeneralConverter.ToInt32(returnData["HLRInductionState"]), GeneralConverter.ToString(returnData["ForwardToNb"]), GeneralConverter.ToString(returnData["Subaddress"]), GeneralConverter.ToInt32(returnData["NoReplyTimer"]), GeneralConverter.ToInt32(returnData["ServiceCode"]), GeneralConverter.ToInt32(returnData["ServiceType"]), GeneralConverter.ToString(returnData["ServiceTypeName"]));
				listNew.Add(hLRCallForward);
			}
		}

		private static void GenerateListVoiceFromReader<T>(IDataReader returnData, ref List<HLRCallForward> listNew)
		{
			while (returnData.Read())
			{
				HLRCallForward hLRCallForward = new HLRCallForward(GeneralConverter.ToInt32(returnData["CFWType"]), GeneralConverter.ToString(returnData["CFWTypeName"]), GeneralConverter.ToString(returnData["ForwardToNb"]), GeneralConverter.ToInt32(returnData["NoReplyTimer"]), GeneralConverter.ToInt32(returnData["CFWStatus"]), GeneralConverter.ToString(returnData["CFWStatusName"]));
				listNew.Add(hLRCallForward);
			}
		}

		public static int UpdateForwarded(string sICCID, int iCFWType1, string sForwardToNb1, int iCFWType2, string sForwardToNb2, int iCFWType3, string sForwardToNb3, int? noreplytime, ref string resultMessage)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetHLRConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, sICCID);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cfwtype1", SqlDbType.Int, 0, ParameterDirection.Input, iCFWType1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@forwardToNb1", SqlDbType.VarChar, 20, ParameterDirection.Input, sForwardToNb1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cfwtype2", SqlDbType.Int, 0, ParameterDirection.Input, iCFWType2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@forwardToNb2", SqlDbType.VarChar, 20, ParameterDirection.Input, sForwardToNb2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cfwtype3", SqlDbType.Int, 0, ParameterDirection.Input, iCFWType3);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@forwardToNb3", SqlDbType.VarChar, 20, ParameterDirection.Input, sForwardToNb3);
			if (noreplytime.HasValue)
			{
				sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@NoReplyTimer", SqlDbType.Int, 0, ParameterDirection.Input, noreplytime);
			}
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "crm_callforward_update");
			DataSet dataSet = sQLDataAccessLayer.ExecuteQuery(sqlCommand);
			int num = GeneralConverter.ToInt32(dataSet.Tables[0].Rows[0]["err_code"]);
			resultMessage = dataSet.Tables[0].Rows[0]["err_message"].ToString();
			return num;
		}
	}
}