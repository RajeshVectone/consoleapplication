using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class ESPAccount
	{
		private const string SP_ACCOUNT_GET_BY_TCBS = "account_get_by_tcbs";

		private const string SP_ACCOUNT_EDIT_BALANCE_BY_TCBS = "account_update_balance_by_tcbs";

		private const string SP_CREATEACCOUNT = "rp1_createAccount";

		private const string SP_MASS_UPDATE_STATUS_ACCOUNT = "rp1_account_massUpdateStatus";

		private const string SP_VIEW_ACCOUNT_DETAIL_BY_CODE = "rp1_view_account_detail_bycode";

		private const string SP_LIST_BY_MOBILENO = "account_search_by_mobileno";

		private const string SP_ACCOUNT_UPDATETARIFFCLASS = "rp1_account_updatetariffclass";

		private string _stelcocode;

		private string _scustcode;

		private int _ibatchcode;

		private int _iserialcode;

		private int _istatus;

		private string _spincode;

		private string _slangcode;

		private int _iacctype;

		private string _sphonenb;

		private DateTime _dtfirstusg;

		private string _sspdial;

		private string _scug;

		private string _sdid;

		private string _sani;

		private double _dbalance;

		private int _inbaccess;

		private int _imaxaccess;

		private string _strffclass;

		private int _isecurity;

		private double _dtotalcons;

		private double _dmonthlim;

		private DateTime _dtstartdate;

		private DateTime _dtenddate;

		private string _slang;

		private int _icnxcount;

		private string _scurrcode;

		private string _stoken;

		private int _ifree_cnxcharge;

		private double _dfree_balance;

		private int _ifirstusgvox;

		private int _ifirstusgdat;

		private double _dtotalcost;

		private float _fBalance;

		private double _dBalance;

		private string _strTrffClass;

		private string _strCurrCode;

		private string _iccid;

		private string _sbatchno;

		private double _dbalancemoney;

		public int AccType
		{
			get
			{
				return this._iacctype;
			}
			set
			{
				this._iacctype = value;
			}
		}

		public double Balance
		{
			get
			{
				return this._dbalance;
			}
			set
			{
				this._dbalance = value;
			}
		}

		public double BalanceMoney
		{
			get
			{
				if (this.Balance == 0)
				{
					return 0;
				}
				return this.Balance / 100;
			}
			set
			{
				this._dbalancemoney = value;
			}
		}

		public int BatchCode
		{
			get
			{
				return this._ibatchcode;
			}
			set
			{
				this._ibatchcode = value;
			}
		}

		public string BatchNo
		{
			get
			{
				string str = this.CustomerCode.Substring(4);
				int batchCode = this.BatchCode;
				string str1 = batchCode.ToString().PadLeft(4, '0');
				int serialCode = this.SerialCode;
				return string.Format("{0}{1}{2}", str, str1, serialCode.ToString().PadLeft(4, '0'));
			}
			set
			{
				this._sbatchno = value;
			}
		}

		public int CnxCount
		{
			get
			{
				return this._icnxcount;
			}
			set
			{
				this._icnxcount = value;
			}
		}

		public string CurrCode
		{
			get
			{
				if (this._strCurrCode == null)
				{
					return string.Empty;
				}
				return this._strCurrCode;
			}
			set
			{
				this._strCurrCode = value;
			}
		}

		public string CurrencyCode
		{
			get
			{
				if (this._scurrcode == null)
				{
					return string.Empty;
				}
				return this._scurrcode;
			}
			set
			{
				this._scurrcode = value;
			}
		}

		public string CustomerCode
		{
			get
			{
				if (this._scustcode == null)
				{
					return string.Empty;
				}
				return this._scustcode;
			}
			set
			{
				this._scustcode = value;
			}
		}

		public double DBalance
		{
			get
			{
				return this._dBalance;
			}
			set
			{
				this._dBalance = value;
			}
		}

		public string DID
		{
			get
			{
				if (this._sdid == null)
				{
					return string.Empty;
				}
				return this._sdid;
			}
			set
			{
				this._sdid = value;
			}
		}

		public DateTime EndDate
		{
			get
			{
				return this._dtenddate;
			}
			set
			{
				this._dtenddate = value;
			}
		}

		public DateTime FirstUsg
		{
			get
			{
				return this._dtfirstusg;
			}
			set
			{
				this._dtfirstusg = value;
			}
		}

		public string ICCID
		{
			get
			{
				if (this._iccid == null)
				{
					return string.Empty;
				}
				return this._iccid;
			}
			set
			{
				this._iccid = value;
			}
		}

		public string LangCode
		{
			get
			{
				if (this._slangcode == null)
				{
					return string.Empty;
				}
				return this._slangcode;
			}
			set
			{
				this._slangcode = value;
			}
		}

		public int MaxAccess
		{
			get
			{
				return this._imaxaccess;
			}
			set
			{
				this._imaxaccess = value;
			}
		}

		public string PhoneNB
		{
			get
			{
				if (this._sphonenb == null)
				{
					return string.Empty;
				}
				return this._sphonenb;
			}
			set
			{
				this._sphonenb = value;
			}
		}

		public string PinCode
		{
			get
			{
				if (this._spincode == null)
				{
					return string.Empty;
				}
				return this._spincode;
			}
			set
			{
				this._spincode = value;
			}
		}

		public int SerialCode
		{
			get
			{
				return this._iserialcode;
			}
			set
			{
				this._iserialcode = value;
			}
		}

		public DateTime StartDate
		{
			get
			{
				return this._dtstartdate;
			}
			set
			{
				this._dtstartdate = value;
			}
		}

		public int Status
		{
			get
			{
				return this._istatus;
			}
			set
			{
				this._istatus = value;
			}
		}

		public string TariffClass
		{
			get
			{
				if (this._strTrffClass == null)
				{
					return string.Empty;
				}
				return this._strTrffClass;
			}
			set
			{
				this._strTrffClass = value;
			}
		}

		public string TelcoCode
		{
			get
			{
				if (this._stelcocode == null)
				{
					return string.Empty;
				}
				return this._stelcocode;
			}
			set
			{
				this._stelcocode = value;
			}
		}

		public double TotalCons
		{
			get
			{
				return this._dtotalcons;
			}
			set
			{
				this._dtotalcons = value;
			}
		}

		public ESPAccount()
		{
		}

		public ESPAccount(string sTelcoCode, string sCustCode, int iBatchCode, int iSerialCode, int iStatus, string sPinCode, string sLangCode, int iAccType, string sPhonenb, DateTime dtFirstUsg, string sSpDial, string sCug, string sDid, string sAni, double dBalance, int iNbAccess, int iMaxaccess, string sTrffClass, int iSecurity, double dTotalCons, double dMonthLim, DateTime dtStartDate, DateTime dtEndDate, string sLang, int iCnxCount, string sCurrCode, string sToken, int iFreeCnxCharge, double dFreeBalance, int iFirstUsgVox, int iFirstUsgDat, double dTotalCost, string sICCID)
		{
			this._stelcocode = sTelcoCode;
			this._scustcode = sCustCode;
			this._ibatchcode = iBatchCode;
			this._iserialcode = iSerialCode;
			this._istatus = iStatus;
			this._spincode = sPinCode;
			this._slangcode = sLangCode;
			this._iacctype = iAccType;
			this._sphonenb = sPhonenb;
			this._dtfirstusg = dtFirstUsg;
			this._sspdial = sSpDial;
			this._scug = sCug;
			this._sdid = sDid;
			this._sani = sAni;
			this._dbalance = dBalance;
			this._inbaccess = iNbAccess;
			this._imaxaccess = iMaxaccess;
			this._strffclass = sTrffClass;
			this._isecurity = iSecurity;
			this._dtotalcons = dTotalCons;
			this._dmonthlim = dMonthLim;
			this._dtstartdate = dtStartDate;
			this._dtenddate = dtEndDate;
			this._slang = sLang;
			this._icnxcount = iCnxCount;
			this._scurrcode = sCurrCode;
			this._stoken = sToken;
			this._ifree_cnxcharge = iFreeCnxCharge;
			this._dfree_balance = dFreeBalance;
			this._ifirstusgvox = iFirstUsgVox;
			this._ifirstusgdat = iFirstUsgDat;
			this._dtotalcost = dTotalCost;
			this._iccid = sICCID;
		}

		public ESPAccount(string sTelcoCode, string sCustCode, int iBatchCode, int iSerialCode, int iStatus, string sPinCode, string sLangCode, int iAccType, string sPhonenb, DateTime dtFirstUsg, string sSpDial, string sCug, string sDid, string sAni, double dBalance, int iNbAccess, int iMaxaccess, string sTrffClass, int iSecurity, double dTotalCons, double dMonthLim, DateTime dtStartDate, DateTime dtEndDate, string sLang, int iCnxCount, string sCurrCode, string sToken, int iFreeCnxCharge, double dFreeBalance, int iFirstUsgVox, int iFirstUsgDat, double dTotalCost, string sICCID, string sBatchNo, double dBalanceMoney)
		{
			this._stelcocode = sTelcoCode;
			this._scustcode = sCustCode;
			this._ibatchcode = iBatchCode;
			this._iserialcode = iSerialCode;
			this._istatus = iStatus;
			this._spincode = sPinCode;
			this._slangcode = sLangCode;
			this._iacctype = iAccType;
			this._sphonenb = sPhonenb;
			this._dtfirstusg = dtFirstUsg;
			this._sspdial = sSpDial;
			this._scug = sCug;
			this._sdid = sDid;
			this._sani = sAni;
			this._dbalance = dBalance;
			this._inbaccess = iNbAccess;
			this._imaxaccess = iMaxaccess;
			this._strffclass = sTrffClass;
			this._isecurity = iSecurity;
			this._dtotalcons = dTotalCons;
			this._dmonthlim = dMonthLim;
			this._dtstartdate = dtStartDate;
			this._dtenddate = dtEndDate;
			this._slang = sLang;
			this._icnxcount = iCnxCount;
			this._scurrcode = sCurrCode;
			this._stoken = sToken;
			this._ifree_cnxcharge = iFreeCnxCharge;
			this._dfree_balance = dFreeBalance;
			this._ifirstusgvox = iFirstUsgVox;
			this._ifirstusgdat = iFirstUsgDat;
			this._dtotalcost = dTotalCost;
			this._iccid = sICCID;
			this._sbatchno = sBatchNo;
			this._dbalancemoney = dBalanceMoney;
		}

		public ESPAccount(string strTelcoCode, string strCustCode, int iBatchCode, int iSerialCode, int iStatus, int iAccType, float fBalance, string strTariffClass)
		{
			if (string.IsNullOrEmpty(strTelcoCode))
			{
				throw new ArgumentException("TelcoCode");
			}
			if (iBatchCode < 0 || iBatchCode > 9999)
			{
				throw new ArgumentOutOfRangeException("BatchCode");
			}
			if (iSerialCode < 0)
			{
				throw new ArgumentOutOfRangeException("SerialCode");
			}
			if (fBalance < 0f)
			{
				throw new ArgumentOutOfRangeException("Balance");
			}
			this._stelcocode = strTelcoCode;
			this._scustcode = strCustCode;
			this._ibatchcode = iBatchCode;
			this._iserialcode = iSerialCode;
			this._istatus = iStatus;
			this._iacctype = iAccType;
			this._fBalance = fBalance;
			this._strTrffClass = strTariffClass;
		}

		public ESPAccount(string strTelcoCode, string strCustCode, int iBatchCode, int iSerialCode, int iStatus, int iAccType, double dBalance, string strTariffClass, DateTime dtStartDate, DateTime dtEndDate, string sCurrCode) : this(strTelcoCode, strCustCode, iBatchCode, iSerialCode, iStatus, iAccType, 0f, strTariffClass)
		{
			this._dBalance = dBalance;
			this._dtstartdate = dtStartDate;
			this._dtenddate = dtEndDate;
			this._strCurrCode = sCurrCode;
		}

		public bool CreateAccount(int iResellerID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 0, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.Char, 3, ParameterDirection.Input, this._stelcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.Char, 8, ParameterDirection.Input, this._scustcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, this._ibatchcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, this._iserialcode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, this._istatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@AccType", SqlDbType.Int, 0, ParameterDirection.Input, this._iacctype);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@Balance", SqlDbType.Float, 0, ParameterDirection.Input, this._fBalance);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TrffClass", SqlDbType.VarChar, 0, ParameterDirection.Input, this._strTrffClass);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_createAccount");
			sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			return (int)sqlCommand.Parameters["@ReturnValue"].Value == 0;
		}

		public static bool EditAccountBalanceByTCBS(string sTelcoCode, string sCustCode, int iBatchCode, int iSerialCode, double dAmount, string sMobileNo)
		{
			if (sTelcoCode == string.Empty)
			{
				throw new ArgumentOutOfRangeException("TelcoCode");
			}
			if (sCustCode == string.Empty)
			{
				throw new ArgumentOutOfRangeException("CustomerCode");
			}
			if (iBatchCode < 0)
			{
				throw new ArgumentOutOfRangeException("BatchCode");
			}
			if (iSerialCode < 0)
			{
				throw new ArgumentOutOfRangeException("SerialCode");
			}
			if (dAmount < 0)
			{
				throw new ArgumentOutOfRangeException("Amount");
			}
			if (sMobileNo == string.Empty)
			{
				throw new ArgumentOutOfRangeException("Mobile Number");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, sTelcoCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sCustCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, iBatchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, iSerialCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@amount", SqlDbType.Float, 0, ParameterDirection.Input, dAmount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileno", SqlDbType.VarChar, 16, ParameterDirection.Input, sMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "account_update_balance_by_tcbs");
			object obj = sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (obj != null && Convert.ToInt32(obj) == 1)
			{
				return true;
			}
			return false;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<ESPAccount> theList)
		{
			while (returnData.Read())
			{
				ESPAccount eSPAccount = new ESPAccount(GeneralConverter.ToString(returnData["telcocode"]), GeneralConverter.ToString(returnData["custcode"]), GeneralConverter.ToInt32(returnData["batchcode"]), GeneralConverter.ToInt32(returnData["serialcode"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToInt32(returnData["acctype"]), GeneralConverter.ToDouble(returnData["balance"]), GeneralConverter.ToString(returnData["trffclass"]), GeneralConverter.ToDateTime(returnData["startdate"]), GeneralConverter.ToDateTime(returnData["enddate"]), GeneralConverter.ToString(returnData["currcode"]));
				theList.Add(eSPAccount);
			}
		}

		private static void GenerateSubscriberListFromReader<T>(IDataReader returnData, ref List<ESPAccount> listNew)
		{
			string str;
			while (returnData.Read())
			{
				str = (returnData.FieldCount >= 33 ? GeneralConverter.ToString(returnData["iccid"]) : "");
				ESPAccount eSPAccount = new ESPAccount(GeneralConverter.ToString(returnData["telcocode"]), GeneralConverter.ToString(returnData["custcode"]), GeneralConverter.ToInt32(returnData["batchcode"]), GeneralConverter.ToInt32(returnData["serialcode"]), GeneralConverter.ToInt32(returnData["status"]), GeneralConverter.ToString(returnData["pincode"]), GeneralConverter.ToString(returnData["langcode"]), GeneralConverter.ToInt32(returnData["acctype"]), GeneralConverter.ToString(returnData["phonenb"]), GeneralConverter.ToDateTime(returnData["firstusg"]), GeneralConverter.ToString(returnData["spdial"]), GeneralConverter.ToString(returnData["cug"]), GeneralConverter.ToString(returnData["did"]), GeneralConverter.ToString(returnData["ani"]), GeneralConverter.ToDouble(returnData["balance"]), GeneralConverter.ToInt32(returnData["nbaccess"]), GeneralConverter.ToInt32(returnData["maxaccess"]), GeneralConverter.ToString(returnData["trffclass"]), GeneralConverter.ToInt32(returnData["security"]), GeneralConverter.ToDouble(returnData["totalcons"]), GeneralConverter.ToDouble(returnData["monthlim"]), GeneralConverter.ToDateTime(returnData["startdate"]), GeneralConverter.ToDateTime(returnData["enddate"]), GeneralConverter.ToString(returnData["lang"]), GeneralConverter.ToInt32(returnData["cnxcount"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToString(returnData["token"]), GeneralConverter.ToInt32(returnData["free_cnxcharge"]), GeneralConverter.ToDouble(returnData["free_balance"]), GeneralConverter.ToInt32(returnData["firstusgvox"]), GeneralConverter.ToInt32(returnData["firstusgdat"]), GeneralConverter.ToDouble(returnData["totalcost"]), str);
				listNew.Add(eSPAccount);
			}
		}

		public static ESPAccount GetAccountDetailByCode(string sTelcocode, string sCustomerCode, int iBatchCode, int iSerialCode)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, sTelcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sCustomerCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, iBatchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, iSerialCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_view_account_detail_bycode");
			List<ESPAccount> eSPAccounts = new List<ESPAccount>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPAccount>(sqlCommand, new GenerateListFromReader<ESPAccount>(ESPAccount.GenerateListFromReader<ESPAccount>), ref eSPAccounts);
			if (eSPAccounts.Count != 1)
			{
				return null;
			}
			return eSPAccounts[0];
		}

		public static ESPAccount GetAccountDetailByLogin(string sLogin)
		{
			CRMSubscriber subscriberByLogin = CRMSubscriber.GetSubscriberByLogin(sLogin);
			if (subscriberByLogin == null)
			{
				return null;
			}
			ESPAccount accountDetailByCode = ESPAccount.GetAccountDetailByCode(subscriberByLogin.TelcoCode, subscriberByLogin.CustomerCode, subscriberByLogin.BatchCode, subscriberByLogin.SerialCode);
			if (accountDetailByCode != null)
			{
				return accountDetailByCode;
			}
			return null;
		}

		public static List<ESPAccount> ListByMobileNo(string MobileNo)
		{
			if (MobileNo == string.Empty)
			{
				throw new ArgumentOutOfRangeException("MobileNo");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileno", SqlDbType.VarChar, 16, ParameterDirection.Input, MobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "account_search_by_mobileno");
			List<ESPAccount> eSPAccounts = new List<ESPAccount>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPAccount>(sqlCommand, new GenerateListFromReader<ESPAccount>(ESPAccount.GenerateSubscriberListFromReader<ESPAccount>), ref eSPAccounts);
			if (eSPAccounts.Count > 0)
			{
				return eSPAccounts;
			}
			return null;
		}

		public static bool MassUpdateStatusAccount(int iResellerID, string sTelcocode, string sCustomerCode, int iBatchCode, int iSerialCodeFrom, int iSerialCodeTo, int iStatus)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.Char, 3, ParameterDirection.Input, sTelcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.Char, 8, ParameterDirection.Input, sCustomerCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, iBatchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcodefr", SqlDbType.Int, 0, ParameterDirection.Input, iSerialCodeFrom);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcodeto", SqlDbType.Int, 0, ParameterDirection.Input, iSerialCodeTo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, iStatus);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_account_massUpdateStatus");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		public static ESPAccount SearchByTCBS(string sTelcocode, string sCustomerCode, int iBatchCode, int iSerialCode)
		{
			if (sTelcocode == string.Empty)
			{
				throw new ArgumentOutOfRangeException("TelcoCode");
			}
			if (sCustomerCode == string.Empty)
			{
				throw new ArgumentOutOfRangeException("CustomerCode");
			}
			if (iBatchCode < 0)
			{
				throw new ArgumentOutOfRangeException("BatchCode");
			}
			if (iSerialCode < 0)
			{
				throw new ArgumentOutOfRangeException("SerialCode");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telcocode", SqlDbType.VarChar, 3, ParameterDirection.Input, sTelcocode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sCustomerCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, iBatchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, iSerialCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "account_get_by_tcbs");
			List<ESPAccount> eSPAccounts = new List<ESPAccount>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPAccount>(sqlCommand, new GenerateListFromReader<ESPAccount>(ESPAccount.GenerateSubscriberListFromReader<ESPAccount>), ref eSPAccounts);
			if (eSPAccounts == null || eSPAccounts.Count != 1)
			{
				return null;
			}
			return eSPAccounts[0];
		}

		public static int UpdateTariffClass(string mobileNo, string ICCID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ReturnValue", SqlDbType.Int, 0, ParameterDirection.ReturnValue, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileno", SqlDbType.Char, 16, ParameterDirection.Input, mobileNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, ICCID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_account_updatetariffclass");
			sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			return (int)sqlCommand.Parameters["@ReturnValue"].Value;
		}
	}
}