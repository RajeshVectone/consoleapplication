using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class CRMCreditCard
	{
		private const string SP_CREATE = "cc_create";

		private const string SP_LIST_BY_NO = "cc_search_by_no";

		private const string SP_CHECKAUTOTOPUP = "Check_AutoTopUP";

		private const string SP_GETAUTOTOPUPSETTING = "get_autotopupsetting";

		private const string SP_SETAUTOTOPUP = "set_autotopup";

		private int _icreditcardid;

		private string _sfirstname;

		private string _slastname;

		private DateTime _dtbirthdate;

		private string _stelephone;

		private string _semail;

		private string _icardtype;

		private string _scardno;

		private string _scardexpdate;

		private string _scardstartdate;

		private string _scardcsc;

		private string _scardissueno;

		private string _scardname;

		private string _shouseno;

		private string _saddress1;

		private string _saddress2;

		private string _scity;

		private string _sstate;

		private string _spostcode;

		private string _scountrycode;

		private string _scurrcode;

		private int _icardstatus;

		public string Address1
		{
			get
			{
				if (this._saddress1 == null)
				{
					return string.Empty;
				}
				return this._saddress1;
			}
			set
			{
				this._saddress1 = value;
			}
		}

		public string Address2
		{
			get
			{
				if (this._saddress2 == null)
				{
					return string.Empty;
				}
				return this._saddress2;
			}
			set
			{
				this._saddress2 = value;
			}
		}

		public DateTime BirthDate
		{
			get
			{
				return this._dtbirthdate;
			}
			set
			{
				this._dtbirthdate = value;
			}
		}

		public string CardCSC
		{
			get
			{
				if (this._scardcsc == null)
				{
					return string.Empty;
				}
				return this._scardcsc;
			}
			set
			{
				this._scardcsc = value;
			}
		}

		public string CardExpiredDate
		{
			get
			{
				if (this._scardexpdate == null)
				{
					return string.Empty;
				}
				return this._scardexpdate;
			}
			set
			{
				this._scardexpdate = value;
			}
		}

		public string CardIssueNo
		{
			get
			{
				if (this._scardissueno == null)
				{
					return string.Empty;
				}
				return this._scardissueno;
			}
			set
			{
				this._scardissueno = value;
			}
		}

		public string CardName
		{
			get
			{
				if (this._scardname == null)
				{
					return string.Empty;
				}
				return this._scardname;
			}
			set
			{
				this._scardname = value;
			}
		}

		public string CardStartDate
		{
			get
			{
				if (this._scardstartdate == null)
				{
					return string.Empty;
				}
				return this._scardstartdate;
			}
			set
			{
				this._scardstartdate = value;
			}
		}

		public string City
		{
			get
			{
				if (this._scity == null)
				{
					return string.Empty;
				}
				return this._scity;
			}
			set
			{
				this._scity = value;
			}
		}

		public string CountryCode
		{
			get
			{
				if (this._scountrycode == null)
				{
					return string.Empty;
				}
				return this._scountrycode;
			}
			set
			{
				this._scountrycode = value;
			}
		}

		public int CreditCardID
		{
			get
			{
				return this._icreditcardid;
			}
			set
			{
				this._icreditcardid = value;
			}
		}

		public string CreditCardNo
		{
			get
			{
				if (this._scardno == null)
				{
					return string.Empty;
				}
				return this._scardno;
			}
			set
			{
				this._scardno = value;
			}
		}

		public int CreditCardStatus
		{
			get
			{
				return this._icardstatus;
			}
			set
			{
				this._icardstatus = value;
			}
		}

		public string CreditCardType
		{
			get
			{
				if (this._icardtype == null)
				{
					return string.Empty;
				}
				return this._icardtype;
			}
			set
			{
				this._icardtype = value;
			}
		}

		public string CurrencyCode
		{
			get
			{
				if (this._scurrcode == null)
				{
					return string.Empty;
				}
				return this._scurrcode;
			}
			set
			{
				this._scurrcode = value;
			}
		}

		public string Email
		{
			get
			{
				if (this._semail == null)
				{
					return string.Empty;
				}
				return this._semail;
			}
			set
			{
				this._semail = value;
			}
		}

		public string FirstName
		{
			get
			{
				if (this._sfirstname == null)
				{
					return string.Empty;
				}
				return this._sfirstname;
			}
			set
			{
				this._sfirstname = value;
			}
		}

		public string FullName
		{
			get
			{
				if (string.Concat(this._sfirstname, " ", this._slastname) == null)
				{
					return string.Empty;
				}
				return string.Concat(this._sfirstname, " ", this._slastname);
			}
			set
			{
				this.FullName = value;
			}
		}

		public string HouseNumber
		{
			get
			{
				if (this._shouseno == null)
				{
					return string.Empty;
				}
				return this._shouseno;
			}
			set
			{
				this._shouseno = value;
			}
		}

		public string LastName
		{
			get
			{
				if (this._slastname == null)
				{
					return string.Empty;
				}
				return this._slastname;
			}
			set
			{
				this._slastname = value;
			}
		}

		public string PostCode
		{
			get
			{
				if (this._spostcode == null)
				{
					return string.Empty;
				}
				return this._spostcode;
			}
			set
			{
				this._spostcode = value;
			}
		}

		public string State
		{
			get
			{
				if (this._sstate == null)
				{
					return string.Empty;
				}
				return this._sstate;
			}
			set
			{
				this._sstate = value;
			}
		}

		public string Telephone
		{
			get
			{
				if (this._stelephone == null)
				{
					return string.Empty;
				}
				return this._stelephone;
			}
			set
			{
				this._stelephone = value;
			}
		}

		public CRMCreditCard()
		{
		}

		public CRMCreditCard(int iCreditCardID, string sFirstName, string sLastName, DateTime dtBirthDate, string sTelephone, string sEmail, string iCardType, string sCardNo, string sCardExpDate, string sCardStartDate, string sCardCSC, string sCardIssueNo, string sCardName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sState, string sPostCode, string sCountryCode, string sCurrCode, int iCardStatus)
		{
			this._icreditcardid = iCreditCardID;
			this._sfirstname = sFirstName;
			this._slastname = sLastName;
			this._dtbirthdate = dtBirthDate;
			this._stelephone = sTelephone;
			this._semail = sEmail;
			this._icardtype = iCardType;
			this._scardno = sCardNo;
			this._scardexpdate = sCardExpDate;
			this._scardstartdate = sCardStartDate;
			this._scardcsc = sCardCSC;
			this._scardissueno = sCardIssueNo;
			this._scardname = sCardName;
			this._shouseno = sHouseNo;
			this._saddress1 = sAddress1;
			this._saddress2 = sAddress2;
			this._scity = sCity;
			this._sstate = sState;
			this._spostcode = sPostCode;
			this._scountrycode = sCountryCode;
			this._scurrcode = sCurrCode;
			this._icardstatus = iCardStatus;
		}

		public static bool AddNewCreditCard(string sFirstName, string sLastName, string sBirthDate, string sTelephone, string sEmail, string iCardType, string sCardNo, string sCardExpDate, string sCardStartDate, string sCardCSC, string sCardIssueNo, string sCardName, string sHouseNo, string sAddress1, string sAddress2, string sCity, string sState, string sPostCode, string sCountryCode, string sCurrCode, int iCardStatus, ref int iCardID, ref string sRefNo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@firstname", SqlDbType.VarChar, 32, ParameterDirection.Input, sFirstName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastname", SqlDbType.VarChar, 32, ParameterDirection.Input, sLastName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@birthdate", SqlDbType.VarChar, 25, ParameterDirection.Input, sBirthDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@telephone", SqlDbType.VarChar, 16, ParameterDirection.Input, sTelephone);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@email", SqlDbType.VarChar, 64, ParameterDirection.Input, sEmail);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardtype", SqlDbType.VarChar, 10, ParameterDirection.Input, iCardType);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardno", SqlDbType.VarChar, 32, ParameterDirection.Input, sCardNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardexpdate", SqlDbType.VarChar, 4, ParameterDirection.Input, sCardExpDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardstartdate", SqlDbType.VarChar, 4, ParameterDirection.Input, sCardStartDate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardcsc", SqlDbType.VarChar, 3, ParameterDirection.Input, sCardCSC);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardissueno", SqlDbType.VarChar, 2, ParameterDirection.Input, sCardIssueNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardname", SqlDbType.VarChar, 32, ParameterDirection.Input, sCardName);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@houseno", SqlDbType.VarChar, 32, ParameterDirection.Input, sHouseNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address1", SqlDbType.VarChar, 64, ParameterDirection.Input, sAddress1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@address2", SqlDbType.VarChar, 64, ParameterDirection.Input, sAddress2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@town", SqlDbType.VarChar, 32, ParameterDirection.Input, sCity);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@state", SqlDbType.VarChar, 32, ParameterDirection.Input, sState);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@postcode", SqlDbType.VarChar, 16, ParameterDirection.Input, sPostCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@countrycode", SqlDbType.Char, 2, ParameterDirection.Input, sCountryCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@cardstatus", SqlDbType.Int, 0, ParameterDirection.Input, iCardStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@CCid", SqlDbType.Int, 0, ParameterDirection.Output, null);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ref_no", SqlDbType.VarChar, 64, ParameterDirection.Output, null);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cc_create");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) == DBNull.Value)
			{
				return false;
			}
			if (sqlCommand.Parameters["@CCid"] == null || (int)sqlCommand.Parameters["@CCid"].Value == -1)
			{
				return false;
			}
			iCardID = (int)sqlCommand.Parameters["@CCid"].Value;
			sRefNo = sqlCommand.Parameters["@ref_no"].Value.ToString();
			return true;
		}

		private static void GenerateCCListFromReader<T>(IDataReader returnData, ref List<CRMCreditCard> listNew)
		{
			while (returnData.Read())
			{
				if (returnData.IsDBNull(0))
				{
					continue;
				}
				CRMCreditCard cRMCreditCard = new CRMCreditCard(GeneralConverter.ToInt32(returnData["creditcardid"]), GeneralConverter.ToString(returnData["firstname"]), GeneralConverter.ToString(returnData["lastname"]), GeneralConverter.ToDateTime(returnData["birthdate"]), GeneralConverter.ToString(returnData["telephone"]), GeneralConverter.ToString(returnData["email"]), GeneralConverter.ToString(returnData["cardtype"]), GeneralConverter.ToString(returnData["cardno"]), GeneralConverter.ToString(returnData["cardexpdate"]), GeneralConverter.ToString(returnData["cardstartdate"]), GeneralConverter.ToString(returnData["cardcsc"]), GeneralConverter.ToString(returnData["cardissueno"]), GeneralConverter.ToString(returnData["cardname"]), GeneralConverter.ToString(returnData["houseno"]), GeneralConverter.ToString(returnData["address1"]), GeneralConverter.ToString(returnData["address2"]), GeneralConverter.ToString(returnData["town"]), GeneralConverter.ToString(returnData["state"]), GeneralConverter.ToString(returnData["postcode"]), GeneralConverter.ToString(returnData["countrycode"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToInt32(returnData["cardstatus"]));
				listNew.Add(cRMCreditCard);
			}
		}

		public static bool IsEnableAutoTopUp(string mobileNo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobno", SqlDbType.VarChar, 20, ParameterDirection.Input, mobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Check_AutoTopUP");
			return (int)sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) == 1;
		}

		public static bool IsEnableAutoTopUp(string mobileNo, out float amount, out float threshold)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileno", SqlDbType.VarChar, 20, ParameterDirection.Input, mobileNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Output, 0);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@amount", SqlDbType.Float, 0, ParameterDirection.Output, 0);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@threshold", SqlDbType.Float, 0, ParameterDirection.Output, 0);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "get_autotopupsetting");
			sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand);
			if (GeneralConverter.ToInt32(sqlCommand.Parameters["@status"].Value) == 0)
			{
				amount = 0f;
				threshold = 0f;
				return false;
			}
			amount = Convert.ToSingle(sqlCommand.Parameters["@amount"].Value);
			threshold = Convert.ToSingle(sqlCommand.Parameters["@threshold"].Value);
			return true;
		}

		public static CRMCreditCard ListByNo(string iCCNo)
		{
			if (iCCNo == string.Empty)
			{
				iCCNo = "%";
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@ccNo", SqlDbType.VarChar, 32, ParameterDirection.Input, iCCNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "cc_search_by_no");
			List<CRMCreditCard> cRMCreditCards = new List<CRMCreditCard>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMCreditCard>(sqlCommand, new GenerateListFromReader<CRMCreditCard>(CRMCreditCard.GenerateCCListFromReader<CRMCreditCard>), ref cRMCreditCards);
			if (cRMCreditCards.Count <= 0)
			{
				return null;
			}
			return cRMCreditCards[0];
		}

		public static int SetAutoTopUpSetting(string mobileNo, int autoTopUpStatus, float amount, float threshold)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileno", SqlDbType.VarChar, 20, ParameterDirection.Input, mobileNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@status", SqlDbType.Int, 0, ParameterDirection.Input, autoTopUpStatus);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@amount", SqlDbType.Float, 0, ParameterDirection.Input, amount);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@threshold", SqlDbType.Float, 0, ParameterDirection.Input, threshold);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "set_autotopup");
			return sQLDataAccessLayer.ExecuteNonQuery(sqlCommand);
		}
	}
}