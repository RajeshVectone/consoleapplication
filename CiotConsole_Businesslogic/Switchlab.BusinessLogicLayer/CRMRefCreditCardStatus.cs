using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class CRMRefCreditCardStatus
	{
		private const string SP_LIST = "r_creditcard_status";

		private int _icardstatus;

		private string _sstatusname;

		private string _sstatusdesc;

		public int CreditCardStatus
		{
			get
			{
				return this._icardstatus;
			}
			set
			{
				this._icardstatus = value;
			}
		}

		public string CreditCardStatusDescription
		{
			get
			{
				if (this._sstatusdesc == null)
				{
					return string.Empty;
				}
				return this._sstatusdesc;
			}
			set
			{
				this._sstatusdesc = value;
			}
		}

		public string CreditCardStatusName
		{
			get
			{
				if (this._sstatusname == null)
				{
					return string.Empty;
				}
				return this._sstatusname;
			}
			set
			{
				this._sstatusname = value;
			}
		}

		public CRMRefCreditCardStatus()
		{
		}

		public CRMRefCreditCardStatus(int iCreditCardStatus, string sCreditCardStatusName, string sCreditCardStatusDescription)
		{
			this._icardstatus = iCreditCardStatus;
			this._sstatusname = sCreditCardStatusName;
			this._sstatusdesc = sCreditCardStatusDescription;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CRMRefCreditCardStatus> listRef)
		{
			while (returnData.Read())
			{
				CRMRefCreditCardStatus cRMRefCreditCardStatu = new CRMRefCreditCardStatus(GeneralConverter.ToInt32(returnData["cardstatus"]), GeneralConverter.ToString(returnData["statusname"]), GeneralConverter.ToString(returnData["statusdesc"]));
				listRef.Add(cRMRefCreditCardStatu);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMRefCreditCardStatus> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_creditcard_status");
			List<CRMRefCreditCardStatus> cRMRefCreditCardStatuses = new List<CRMRefCreditCardStatus>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRefCreditCardStatus>(sqlCommand, new GenerateListFromReader<CRMRefCreditCardStatus>(CRMRefCreditCardStatus.GenerateListFromReader<CRMRefCreditCardStatus>), ref cRMRefCreditCardStatuses);
			return cRMRefCreditCardStatuses;
		}
	}
}