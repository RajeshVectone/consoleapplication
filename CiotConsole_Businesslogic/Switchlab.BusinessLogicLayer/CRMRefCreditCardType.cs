using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class CRMRefCreditCardType
	{
		private const string SP_LIST = "r_creditcard_type";

		private string _icardtype;

		private string _scardtypename;

		public string CreditCardType
		{
			get
			{
				if (this._icardtype == null)
				{
					return string.Empty;
				}
				return this._icardtype;
			}
			set
			{
				this._icardtype = value;
			}
		}

		public string CreditCardTypeName
		{
			get
			{
				if (this._scardtypename == null)
				{
					return string.Empty;
				}
				return this._scardtypename;
			}
			set
			{
				this._scardtypename = value;
			}
		}

		public CRMRefCreditCardType()
		{
		}

		public CRMRefCreditCardType(string iCreditCardType, string sCreditCardTypeName)
		{
			this._icardtype = iCreditCardType;
			this._scardtypename = sCreditCardTypeName;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CRMRefCreditCardType> listRef)
		{
			while (returnData.Read())
			{
				CRMRefCreditCardType cRMRefCreditCardType = new CRMRefCreditCardType(GeneralConverter.ToString(returnData["cardtype"]), GeneralConverter.ToString(returnData["typename"]));
				listRef.Add(cRMRefCreditCardType);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CRMRefCreditCardType> ListAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetDIBSConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "r_creditcard_type");
			List<CRMRefCreditCardType> cRMRefCreditCardTypes = new List<CRMRefCreditCardType>();
			sQLDataAccessLayer.ExecuteReaderCmd<CRMRefCreditCardType>(sqlCommand, new GenerateListFromReader<CRMRefCreditCardType>(CRMRefCreditCardType.GenerateListFromReader<CRMRefCreditCardType>), ref cRMRefCreditCardTypes);
			return cRMRefCreditCardTypes;
		}
	}
}