using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	[Serializable]
	public class ESPCurrencyExchange
	{
		protected const string SP_INSERT_CURR_EXCHANGE = "currex_insert";

		protected const string SP_UPDATE_CURR_EXCHANGE = "currex_update";

		protected const string SP_DELETE_CURR_EXCHANGE = "currex_delete";

		protected const string SP_GET_ONE_RATE = "currex_get_one_rate";

		protected const string SP_GET_ONE_CURRENCY = "currex_get_one_currency";

		protected const string SP_GET_ALL_RATE = "currex_get_all_rate";

		private string _scurrcode1;

		private string _scurrcode2;

		private double _drate;

		private DateTime _dtlastupdate;

		private string _slastuser;

		public string CurrencyCode1
		{
			get
			{
				if (this._scurrcode1 == null)
				{
					return string.Empty;
				}
				return this._scurrcode1;
			}
			set
			{
				this._scurrcode1 = value;
			}
		}

		public string CurrencyCode2
		{
			get
			{
				if (this._scurrcode2 == null)
				{
					return string.Empty;
				}
				return this._scurrcode2;
			}
			set
			{
				this._scurrcode2 = value;
			}
		}

		public DateTime LastUpdate
		{
			get
			{
				return this._dtlastupdate;
			}
			set
			{
				this._dtlastupdate = value;
			}
		}

		public string LastUser
		{
			get
			{
				if (this._slastuser == null)
				{
					return string.Empty;
				}
				return this._slastuser;
			}
			set
			{
				this._slastuser = value;
			}
		}

		public double Rate
		{
			get
			{
				return this._drate;
			}
			set
			{
				this._drate = value;
			}
		}

		public ESPCurrencyExchange()
		{
		}

		public ESPCurrencyExchange(string sCurrencyCode1, string sCurrencyCode2, double dRate, DateTime dtLastUpdate, string sLastUser)
		{
			this._scurrcode1 = sCurrencyCode1;
			this._scurrcode2 = sCurrencyCode2;
			this._drate = dRate;
			this._dtlastupdate = dtLastUpdate;
			this._slastuser = sLastUser;
		}

		public static bool DeleteExchangeRate(string sCurrency1, string sCurrency2)
		{
			if (string.IsNullOrEmpty(sCurrency1))
			{
				throw new ArgumentOutOfRangeException("Currency 1");
			}
			if (string.IsNullOrEmpty(sCurrency2))
			{
				throw new ArgumentOutOfRangeException("Currency 2");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode1", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrency1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode2", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrency2);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "currex_delete");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListCurrencyFromReader<T>(IDataReader returnData, ref List<ESPCurrencyExchange> theList)
		{
			while (returnData.Read())
			{
				ESPCurrencyExchange eSPCurrencyExchange = new ESPCurrencyExchange(GeneralConverter.ToString(returnData["currcode1"]), GeneralConverter.ToString(returnData["currcode2"]), GeneralConverter.ToDouble(returnData["factor"]), GeneralConverter.ToDateTime(returnData["lastupdate"]), GeneralConverter.ToString(returnData["lastuser"]));
				theList.Add(eSPCurrencyExchange);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static List<ESPCurrencyExchange> get_all_rate()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "currex_get_all_rate");
			List<ESPCurrencyExchange> eSPCurrencyExchanges = new List<ESPCurrencyExchange>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPCurrencyExchange>(sqlCommand, new GenerateListFromReader<ESPCurrencyExchange>(ESPCurrencyExchange.GenerateListCurrencyFromReader<ESPCurrencyExchange>), ref eSPCurrencyExchanges);
			if (eSPCurrencyExchanges != null && eSPCurrencyExchanges.Count > 0)
			{
				return eSPCurrencyExchanges;
			}
			return null;
		}

		public static ESPCurrencyExchange get_specific_rate(string sCurrency1, string sCurrency2)
		{
			if (string.IsNullOrEmpty(sCurrency1))
			{
				throw new ArgumentOutOfRangeException("Currency 1");
			}
			if (string.IsNullOrEmpty(sCurrency2))
			{
				throw new ArgumentOutOfRangeException("Currency 2");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode1", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrency1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode2", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrency2);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "currex_get_one_rate");
			List<ESPCurrencyExchange> eSPCurrencyExchanges = new List<ESPCurrencyExchange>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPCurrencyExchange>(sqlCommand, new GenerateListFromReader<ESPCurrencyExchange>(ESPCurrencyExchange.GenerateListCurrencyFromReader<ESPCurrencyExchange>), ref eSPCurrencyExchanges);
			if (eSPCurrencyExchanges.Count <= 0)
			{
				return null;
			}
			return eSPCurrencyExchanges[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static List<ESPCurrencyExchange> get_specified_currency(string sCurrCode)
		{
			if (string.IsNullOrEmpty(sCurrCode))
			{
				throw new ArgumentOutOfRangeException("Currency");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "currex_get_one_currency");
			List<ESPCurrencyExchange> eSPCurrencyExchanges = new List<ESPCurrencyExchange>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPCurrencyExchange>(sqlCommand, new GenerateListFromReader<ESPCurrencyExchange>(ESPCurrencyExchange.GenerateListCurrencyFromReader<ESPCurrencyExchange>), ref eSPCurrencyExchanges);
			if (eSPCurrencyExchanges != null && eSPCurrencyExchanges.Count > 0)
			{
				return eSPCurrencyExchanges;
			}
			return null;
		}

		public static int InsertExchangeRate(string sSiteCode, string sCurrency1, string sCurrency2, double dRate, string sUser)
		{
			if (string.IsNullOrEmpty(sCurrency1))
			{
				throw new ArgumentOutOfRangeException("Currency 1");
			}
			if (string.IsNullOrEmpty(sCurrency2))
			{
				throw new ArgumentOutOfRangeException("Currency 2");
			}
			if (dRate < 0)
			{
				throw new ArgumentOutOfRangeException("Rate");
			}
			if (string.IsNullOrEmpty(sSiteCode))
			{
				throw new ArgumentOutOfRangeException("Site Code");
			}
			if (string.IsNullOrEmpty(sUser))
			{
				throw new ArgumentOutOfRangeException("User");
			}
			if (ESPCurrency.GetByCurrId(sSiteCode, sCurrency1) == null)
			{
				return -1;
			}
			if (ESPCurrency.GetByCurrId(sSiteCode, sCurrency2) == null)
			{
				return -2;
			}
			if (ESPCurrencyExchange.get_specific_rate(sCurrency1, sCurrency2) == null)
			{
				return ESPCurrencyExchange.InsertExchangeRate(sCurrency1, sCurrency2, dRate, sUser);
			}
			return ESPCurrencyExchange.UpdateExchangeRate(sCurrency1, sCurrency2, dRate, sUser);
		}

		public static int InsertExchangeRate(string sCurrency1, string sCurrency2, double dRate, string sUser)
		{
			if (string.IsNullOrEmpty(sCurrency1))
			{
				throw new ArgumentOutOfRangeException("Currency 1");
			}
			if (string.IsNullOrEmpty(sCurrency2))
			{
				throw new ArgumentOutOfRangeException("Currency 2");
			}
			if (dRate < 0)
			{
				throw new ArgumentOutOfRangeException("Rate");
			}
			if (string.IsNullOrEmpty(sUser))
			{
				throw new ArgumentOutOfRangeException("User");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode1", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrency1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode2", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrency2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@factor", SqlDbType.Float, 0, ParameterDirection.Input, dRate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastuser", SqlDbType.VarChar, 16, ParameterDirection.Input, sUser);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "currex_insert");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return 1;
			}
			return -3;
		}

		public static int UpdateExchangeRate(string sSiteCode, string sCurrency1, string sCurrency2, double dRate, string sUser)
		{
			if (string.IsNullOrEmpty(sCurrency1))
			{
				throw new ArgumentOutOfRangeException("Currency 1");
			}
			if (string.IsNullOrEmpty(sCurrency2))
			{
				throw new ArgumentOutOfRangeException("Currency 2");
			}
			if (dRate < 0)
			{
				throw new ArgumentOutOfRangeException("Rate");
			}
			if (string.IsNullOrEmpty(sSiteCode))
			{
				throw new ArgumentOutOfRangeException("Site Code");
			}
			if (string.IsNullOrEmpty(sUser))
			{
				throw new ArgumentOutOfRangeException("User");
			}
			if (ESPCurrency.GetByCurrId(sSiteCode, sCurrency1) == null)
			{
				return -1;
			}
			if (ESPCurrency.GetByCurrId(sSiteCode, sCurrency2) == null)
			{
				return -2;
			}
			if (ESPCurrencyExchange.get_specific_rate(sCurrency1, sCurrency2) != null)
			{
				return ESPCurrencyExchange.UpdateExchangeRate(sCurrency1, sCurrency2, dRate, sUser);
			}
			return ESPCurrencyExchange.InsertExchangeRate(sCurrency1, sCurrency2, dRate, sUser);
		}

		public static int UpdateExchangeRate(string sCurrency1, string sCurrency2, double dRate, string sUser)
		{
			if (string.IsNullOrEmpty(sCurrency1))
			{
				throw new ArgumentOutOfRangeException("Currency 1");
			}
			if (string.IsNullOrEmpty(sCurrency2))
			{
				throw new ArgumentOutOfRangeException("Currency 2");
			}
			if (dRate < 0)
			{
				throw new ArgumentOutOfRangeException("Rate");
			}
			if (string.IsNullOrEmpty(sUser))
			{
				throw new ArgumentOutOfRangeException("User");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode1", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrency1);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@currcode2", SqlDbType.VarChar, 5, ParameterDirection.Input, sCurrency2);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@factor", SqlDbType.Float, 0, ParameterDirection.Input, dRate);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@lastuser", SqlDbType.VarChar, 16, ParameterDirection.Input, sUser);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "currex_update");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return 1;
			}
			return -3;
		}
	}
}