using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	public class ESPMVNOAccount
	{
		private const string SP_LIST_BY_MOBILENO = "esp_mvno_account_list_by_mobileNo";

		private const string SP_LIST_BY_CODE = "esp_mvno_account_list_by_code";

		private const string SP_LIST_BY_ICCID = "esp_mvno_account_list_by_iccid";

		private string _smobileno;

		private string _scustcode;

		private int _ibatchcode;

		private int _iserialcode;

		private string _siccid;

		private string _simei;

		private string _stariffclass;

		private int _icardid;

		private string _scardname;

		private DateTime _dtexpdate;

		public int BatchCode
		{
			get
			{
				return this._ibatchcode;
			}
			set
			{
				this._ibatchcode = value;
			}
		}

		public int CardID
		{
			get
			{
				return this._icardid;
			}
			set
			{
				this._icardid = value;
			}
		}

		public string CardName
		{
			get
			{
				if (this._scardname == null)
				{
					return string.Empty;
				}
				return this._scardname;
			}
			set
			{
				this._scardname = value;
			}
		}

		public string CustomerCode
		{
			get
			{
				if (this._scustcode == null)
				{
					return string.Empty;
				}
				return this._scustcode;
			}
			set
			{
				this._scustcode = value;
			}
		}

		public DateTime ExpDate
		{
			get
			{
				return this._dtexpdate;
			}
			set
			{
				this._dtexpdate = value;
			}
		}

		public string ICCID
		{
			get
			{
				if (this._siccid == null)
				{
					return string.Empty;
				}
				return this._siccid;
			}
			set
			{
				this._siccid = value;
			}
		}

		public string IMEI
		{
			get
			{
				if (this._simei == null)
				{
					return string.Empty;
				}
				return this._simei;
			}
			set
			{
				this._simei = value;
			}
		}

		public string MobileNo
		{
			get
			{
				if (this._smobileno == null)
				{
					return string.Empty;
				}
				return this._smobileno;
			}
			set
			{
				this._smobileno = value;
			}
		}

		public int SerialCode
		{
			get
			{
				return this._iserialcode;
			}
			set
			{
				this._iserialcode = value;
			}
		}

		public string TariffClass
		{
			get
			{
				if (this._stariffclass == null)
				{
					return string.Empty;
				}
				return this._stariffclass;
			}
			set
			{
				this._stariffclass = value;
			}
		}

		public ESPMVNOAccount()
		{
		}

		public ESPMVNOAccount(string sMobileNo, string sCustCode, int iBatchCode, int iSerialCode, string sICCID, string sIMEI, string sTariffClass, int iCardID, string sCardName, DateTime dtExpDate)
		{
			this._smobileno = sMobileNo;
			this._scustcode = sCustCode;
			this._ibatchcode = iBatchCode;
			this._iserialcode = iSerialCode;
			this._siccid = sICCID;
			this._simei = sIMEI;
			this._stariffclass = sTariffClass;
			this._icardid = iCardID;
			this._scardname = sCardName;
			this._dtexpdate = dtExpDate;
		}

		private static void GenerateRefListFromReader<T>(IDataReader returnData, ref List<ESPMVNOAccount> listRef)
		{
			while (returnData.Read())
			{
				ESPMVNOAccount eSPMVNOAccount = new ESPMVNOAccount(GeneralConverter.ToString(returnData["mobileNo"]), GeneralConverter.ToString(returnData["custcode"]), GeneralConverter.ToInt32(returnData["batchcode"]), GeneralConverter.ToInt32(returnData["serialcode"]), GeneralConverter.ToString(returnData["iccid"]), GeneralConverter.ToString(returnData["imei"]), GeneralConverter.ToString(returnData["tariffclass"]), GeneralConverter.ToInt32(returnData["card_id"]), GeneralConverter.ToString(returnData["cardname"]), GeneralConverter.ToDateTime(returnData["expdate"]));
				listRef.Add(eSPMVNOAccount);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static ESPMVNOAccount GetMVNOAccountByCode(string sCustCode, int iBatchCode, int iSerialCode)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sCustCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, iBatchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, iSerialCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "esp_mvno_account_list_by_code");
			List<ESPMVNOAccount> eSPMVNOAccounts = new List<ESPMVNOAccount>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPMVNOAccount>(sqlCommand, new GenerateListFromReader<ESPMVNOAccount>(ESPMVNOAccount.GenerateRefListFromReader<ESPMVNOAccount>), ref eSPMVNOAccounts);
			if (eSPMVNOAccounts.Count != 1)
			{
				return null;
			}
			return eSPMVNOAccounts[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static ESPMVNOAccount GetMVNOAccountByICCID(string sICCID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@iccid", SqlDbType.VarChar, 20, ParameterDirection.Input, sICCID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "esp_mvno_account_list_by_iccid");
			List<ESPMVNOAccount> eSPMVNOAccounts = new List<ESPMVNOAccount>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPMVNOAccount>(sqlCommand, new GenerateListFromReader<ESPMVNOAccount>(ESPMVNOAccount.GenerateRefListFromReader<ESPMVNOAccount>), ref eSPMVNOAccounts);
			if (eSPMVNOAccounts.Count != 1)
			{
				return null;
			}
			return eSPMVNOAccounts[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static ESPMVNOAccount GetMVNOAccountByMobileNo(string sMobileNo)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@mobileno", SqlDbType.VarChar, 16, ParameterDirection.Input, sMobileNo);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "esp_mvno_account_list_by_mobileNo");
			List<ESPMVNOAccount> eSPMVNOAccounts = new List<ESPMVNOAccount>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPMVNOAccount>(sqlCommand, new GenerateListFromReader<ESPMVNOAccount>(ESPMVNOAccount.GenerateRefListFromReader<ESPMVNOAccount>), ref eSPMVNOAccounts);
			if (eSPMVNOAccounts.Count != 1)
			{
				return null;
			}
			return eSPMVNOAccounts[0];
		}
	}
}