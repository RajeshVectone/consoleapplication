using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject(true)]
	[Serializable]
	public class NPAdmPortingStatus
	{
		private const string SP_GET_PORTING_STATUS_LIST = "portingStatus_list";

		private string _portingStatusID;

		private string _portingStatusName;

		public string PortingStatusID
		{
			get
			{
				if (this._portingStatusID == null)
				{
					return string.Empty;
				}
				return this._portingStatusID;
			}
			set
			{
				this._portingStatusID = value;
			}
		}

		public string PortingStatusName
		{
			get
			{
				if (this._portingStatusName == null)
				{
					return string.Empty;
				}
				return this._portingStatusName;
			}
			set
			{
				this._portingStatusName = value;
			}
		}

		public NPAdmPortingStatus()
		{
		}

		public NPAdmPortingStatus(string portingStatusID, string portingStatusName)
		{
			this._portingStatusID = portingStatusID;
			this._portingStatusName = portingStatusName;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<NPAdmPortingStatus> listRef)
		{
			while (returnData.Read())
			{
				NPAdmPortingStatus nPAdmPortingStatu = new NPAdmPortingStatus(GeneralConverter.ToString(returnData["portingStatusID"]), GeneralConverter.ToString(returnData["portingStatusName"]));
				listRef.Add(nPAdmPortingStatu);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<NPAdmPortingStatus> GetListPortingStatus()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "portingStatus_list");
			List<NPAdmPortingStatus> nPAdmPortingStatuses = new List<NPAdmPortingStatus>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmPortingStatus>(sqlCommand, new GenerateListFromReader<NPAdmPortingStatus>(NPAdmPortingStatus.GenerateListFromReader<NPAdmPortingStatus>), ref nPAdmPortingStatuses);
			return nPAdmPortingStatuses;
		}

		public enum Status
		{
			NewContract = 1,
			AuthorizedContract = 2,
			RejectedContract = 3,
			WaitingConfirmation = 4,
			WaitingActivation = 5,
			Rejected = 6,
			Cancelled = 7,
			WaitingCompleted = 8,
			Completed = 9,
			NewWT = 10,
			ConfirmedPortingOutRequest = 12,
			RejectedPortingOut = 15,
			ChangeRequestSent = 17,
			ReturnRequestSent = 19,
			RangeUpdateSent = 21
		}
	}
}