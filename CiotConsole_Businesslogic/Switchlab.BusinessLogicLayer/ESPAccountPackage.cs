using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	public class ESPAccountPackage
	{
		private const string SP_VIEW_ACCOUNT_PACKAGE_BY_CODE = "rp1_view_account_package_bycode";

		private int _ipackage_id;

		private string _scustcode;

		private int _ibatchcode;

		private int _iserialcode;

		private string _smobileno;

		private double _dbalance;

		private string _scurrcode;

		private DateTime _dtstartdate;

		private DateTime _dtexpdate;

		private int _ipackagestatusid;

		private string _sm_custcode;

		private int _im_batchcode;

		private int _im_serialcode;

		private string _stariffclass;

		private int _icardid;

		private string _spackagename;

		private double _dm_balance;

		private string _sm_currcode;

		public double Balance
		{
			get
			{
				return this._dbalance;
			}
			set
			{
				this._dbalance = value;
			}
		}

		public int BatchCode
		{
			get
			{
				return this._ibatchcode;
			}
			set
			{
				this._ibatchcode = value;
			}
		}

		public int Card_ID
		{
			get
			{
				return this._icardid;
			}
			set
			{
				this._icardid = value;
			}
		}

		public string CurrencyCode
		{
			get
			{
				if (this._scurrcode == null)
				{
					return string.Empty;
				}
				return this._scurrcode;
			}
			set
			{
				this._scurrcode = value;
			}
		}

		public string CustomerCode
		{
			get
			{
				if (this._scustcode == null)
				{
					return string.Empty;
				}
				return this._scustcode;
			}
			set
			{
				this._scustcode = value;
			}
		}

		public DateTime ExpiredDate
		{
			get
			{
				return this._dtexpdate;
			}
			set
			{
				this._dtexpdate = value;
			}
		}

		public double M_Balance
		{
			get
			{
				return this._dm_balance;
			}
			set
			{
				this._dm_balance = value;
			}
		}

		public int M_BatchCode
		{
			get
			{
				return this._im_batchcode;
			}
			set
			{
				this._im_batchcode = value;
			}
		}

		public string M_CurrencyCode
		{
			get
			{
				if (this._sm_currcode == null)
				{
					return string.Empty;
				}
				return this._sm_currcode;
			}
			set
			{
				this._sm_currcode = value;
			}
		}

		public string M_CustomerCode
		{
			get
			{
				if (this._sm_custcode == null)
				{
					return string.Empty;
				}
				return this._sm_custcode;
			}
			set
			{
				this._sm_custcode = value;
			}
		}

		public int M_SerialCode
		{
			get
			{
				return this._im_serialcode;
			}
			set
			{
				this._im_serialcode = value;
			}
		}

		public string MobileNo
		{
			get
			{
				if (this._smobileno == null)
				{
					return string.Empty;
				}
				return this._smobileno;
			}
			set
			{
				this._smobileno = value;
			}
		}

		public int Package_ID
		{
			get
			{
				return this._ipackage_id;
			}
			set
			{
				this._ipackage_id = value;
			}
		}

		public string PackageName
		{
			get
			{
				if (this._spackagename == null)
				{
					return string.Empty;
				}
				return this._spackagename;
			}
			set
			{
				this._spackagename = value;
			}
		}

		public int PackageStatus_ID
		{
			get
			{
				return this._ipackagestatusid;
			}
			set
			{
				this._ipackagestatusid = value;
			}
		}

		public int SerialCode
		{
			get
			{
				return this._iserialcode;
			}
			set
			{
				this._iserialcode = value;
			}
		}

		public DateTime StartDate
		{
			get
			{
				return this._dtstartdate;
			}
			set
			{
				this._dtstartdate = value;
			}
		}

		public string TariffClass
		{
			get
			{
				if (this._stariffclass == null)
				{
					return string.Empty;
				}
				return this._stariffclass;
			}
			set
			{
				this._stariffclass = value;
			}
		}

		public ESPAccountPackage()
		{
		}

		public ESPAccountPackage(int iPackage_ID, string sCustCode, int iBatchCode, int iSerialCode, string sMobileNo, double dBalance, string sCurrcode, DateTime dtStartDate, DateTime dtExpDate, int iPackageStatusID, string sM_CustCode, int iM_BatchCode, int iM_SerialCode, string sTariffClass, int iCardID)
		{
			this._ipackage_id = iPackage_ID;
			this._scustcode = sCustCode;
			this._ibatchcode = iBatchCode;
			this._iserialcode = iSerialCode;
			this._smobileno = sMobileNo;
			this._dbalance = dBalance;
			this._scurrcode = sCurrcode;
			this._dtstartdate = dtStartDate;
			this._dtexpdate = dtExpDate;
			this._ipackagestatusid = iPackageStatusID;
			this._sm_custcode = sM_CustCode;
			this._im_batchcode = iM_BatchCode;
			this._im_serialcode = iM_SerialCode;
			this._stariffclass = sTariffClass;
			this._icardid = iCardID;
		}

		public ESPAccountPackage(int iPackage_ID, string sCustCode, int iBatchCode, int iSerialCode, string sMobileNo, double dBalance, string sCurrcode, DateTime dtStartDate, DateTime dtExpDate, int iPackageStatusID, string sM_CustCode, int iM_BatchCode, int iM_SerialCode, string sTariffClass, int iCardID, string sPackageName) : this(iPackage_ID, sCustCode, iBatchCode, iSerialCode, sMobileNo, dBalance, sCurrcode, dtStartDate, dtExpDate, iPackageStatusID, sM_CustCode, iM_BatchCode, iM_SerialCode, sTariffClass, iCardID)
		{
			this._spackagename = sPackageName;
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<ESPAccountPackage> theList)
		{
			while (returnData.Read())
			{
				ESPAccountPackage eSPAccountPackage = new ESPAccountPackage(GeneralConverter.ToInt32(returnData["package_id"]), GeneralConverter.ToString(returnData["custcode"]), GeneralConverter.ToInt32(returnData["batchcode"]), GeneralConverter.ToInt32(returnData["serialcode"]), GeneralConverter.ToString(returnData["mobileNo"]), GeneralConverter.ToDouble(returnData["balance"]), GeneralConverter.ToString(returnData["currcode"]), GeneralConverter.ToDateTime(returnData["startDate"]), GeneralConverter.ToDateTime(returnData["expDate"]), GeneralConverter.ToInt32(returnData["packageStatusID"]), GeneralConverter.ToString(returnData["m_custcode"]), GeneralConverter.ToInt32(returnData["m_batchcode"]), GeneralConverter.ToInt32(returnData["m_serialcode"]), GeneralConverter.ToString(returnData["tariffClass"]), GeneralConverter.ToInt32(returnData["card_id"]));
				theList.Add(eSPAccountPackage);
			}
		}

		public static List<ESPAccountPackage> getAccountPackageByCode(string sCustomerCode, int iBatchCode, int iSerialCode)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetESPConnectionString().ToString());
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@custcode", SqlDbType.VarChar, 8, ParameterDirection.Input, sCustomerCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@batchcode", SqlDbType.Int, 0, ParameterDirection.Input, iBatchCode);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@serialcode", SqlDbType.Int, 0, ParameterDirection.Input, iSerialCode);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "rp1_view_account_package_bycode");
			List<ESPAccountPackage> eSPAccountPackages = new List<ESPAccountPackage>();
			sQLDataAccessLayer.ExecuteReaderCmd<ESPAccountPackage>(sqlCommand, new GenerateListFromReader<ESPAccountPackage>(ESPAccountPackage.GenerateListFromReader<ESPAccountPackage>), ref eSPAccountPackages);
			if (eSPAccountPackages.Count > 0)
			{
				return eSPAccountPackages;
			}
			return null;
		}

		public static List<ESPAccountPackage> GetListAccountPackageWithDetailByCode(string sCustomerCode, int iBatchCode, int iSerialCode, string sTelcoCode)
		{
			List<ESPAccountPackage> accountPackageByCode = ESPAccountPackage.getAccountPackageByCode(sCustomerCode, iBatchCode, iSerialCode);
			List<ESPAccountPackage> eSPAccountPackages = new List<ESPAccountPackage>();
			foreach (ESPAccountPackage packageName in accountPackageByCode)
			{
				ESPPackage packageByPackageID = ESPPackage.getPackageByPackageID(packageName.Package_ID);
				ESPAccount accountDetailByCode = ESPAccount.GetAccountDetailByCode(sTelcoCode, sCustomerCode, iBatchCode, iSerialCode);
				packageName.PackageName = packageByPackageID.PackageName;
				packageName.M_Balance = accountDetailByCode.DBalance;
				packageName.M_CurrencyCode = accountDetailByCode.CurrCode;
			}
			if (eSPAccountPackages.Count < 0)
			{
				return eSPAccountPackages;
			}
			return null;
		}

		public static List<ESPAccountPackage> GetListPackageByLogin(string sLogin)
		{
			CRMSubscriber subscriberByLogin = CRMSubscriber.GetSubscriberByLogin(sLogin);
			if (subscriberByLogin == null)
			{
				return null;
			}
			if (ESPAccount.GetAccountDetailByCode(subscriberByLogin.TelcoCode, subscriberByLogin.CustomerCode, subscriberByLogin.BatchCode, subscriberByLogin.SerialCode) == null)
			{
				return null;
			}
			List<ESPAccountPackage> eSPAccountPackages = new List<ESPAccountPackage>();
			eSPAccountPackages.AddRange(ESPAccountPackage.GetListAccountPackageWithDetailByCode(subscriberByLogin.CustomerCode, subscriberByLogin.BatchCode, subscriberByLogin.SerialCode, subscriberByLogin.TelcoCode));
			if (eSPAccountPackages != null)
			{
				return eSPAccountPackages;
			}
			return null;
		}
	}
}