using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[Serializable]
	public class NPAdmSeries
	{
		private const string SP_INSERT_SERIES = "NP_Insert_Series";

		private const string SP_LIST_SERIES_BY_TRANS_ID = "Series_listByTransId";

		private int _transactionId;

		private int _seriesNo;

		private string _phoneNumberStart;

		private string _phoneNumberEnd;

		public string PhoneNumberEnd
		{
			get
			{
				return this._phoneNumberEnd;
			}
			set
			{
				this._phoneNumberEnd = value;
			}
		}

		public string PhoneNumberStart
		{
			get
			{
				return this._phoneNumberStart;
			}
			set
			{
				this._phoneNumberStart = value;
			}
		}

		public int SeriesNo
		{
			get
			{
				return this._seriesNo;
			}
			set
			{
				this._seriesNo = value;
			}
		}

		public int TransactionId
		{
			get
			{
				return this._transactionId;
			}
			set
			{
				this._transactionId = value;
			}
		}

		public NPAdmSeries(int transId, int seriesNo, string phoneStart, string phoneEnd)
		{
			this._transactionId = transId;
			this._seriesNo = seriesNo;
			this._phoneNumberStart = phoneStart;
			this._phoneNumberEnd = phoneEnd;
		}

		private static void GenerateSeriesListByTransIdFromReader<T>(IDataReader returnData, ref List<NPAdmSeries> listNew)
		{
			while (returnData.Read())
			{
				NPAdmSeries nPAdmSeries = new NPAdmSeries(GeneralConverter.ToInt32(returnData["transId"]), GeneralConverter.ToInt32(returnData["seriesNo"]), GeneralConverter.ToString(returnData["phoneNumberStart"]), GeneralConverter.ToString(returnData["phoneNumberEnd"]));
				listNew.Add(nPAdmSeries);
			}
		}

		public static bool InsertSeries(int transId, int seriesNo, string phoneStart, string phoneEnd)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TransID", SqlDbType.Int, 0, ParameterDirection.Input, transId);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@seriesNo", SqlDbType.Int, 0, ParameterDirection.Input, seriesNo);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@phoneStart", SqlDbType.VarChar, 15, ParameterDirection.Input, phoneStart);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@phoneEnd", SqlDbType.VarChar, 15, ParameterDirection.Input, phoneEnd);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "NP_Insert_Series");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		public static List<NPAdmSeries> ListSeriesByTransId(int transId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetNPAdmConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@TransID", SqlDbType.Int, 0, ParameterDirection.Input, transId);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "Series_listByTransId");
			List<NPAdmSeries> nPAdmSeries = new List<NPAdmSeries>();
			sQLDataAccessLayer.ExecuteReaderCmd<NPAdmSeries>(sqlCommand, new GenerateListFromReader<NPAdmSeries>(NPAdmSeries.GenerateSeriesListByTransIdFromReader<NPAdmSeries>), ref nPAdmSeries);
			return nPAdmSeries;
		}
	}
}