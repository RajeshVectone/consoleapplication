using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.BusinessLogicLayer
{
	[DataObject]
	[Serializable]
	public class CrmModule
	{
		protected const string SP_SELECT = "modules_select";

		protected const string SP_DETAIL = "modules_detail";

		protected const string SP_DELETE = "modules_delete";

		protected const string SP_INSERT = "modules_insert";

		protected const string SP_UPDATE = "modules_update";

		private const string SP_UP_PAGE = "modules_order_up";

		private const string SP_DOWN_PAGE = "modules_order_down";

		private const string SP_ListModule = "modules_listbygroup";

		private const string SP_GetByTitle = "modules_getbytitle";

		private int _module_id;

		private string _title;

		private string _url;

		private string _displayname;

		private int _sortorder;

		public string displayname
		{
			get
			{
				if (this._displayname == null)
				{
					return string.Empty;
				}
				return this._displayname;
			}
		}

		public int module_id
		{
			get
			{
				return this._module_id;
			}
		}

		public int sortorder
		{
			get
			{
				return this._sortorder;
			}
		}

		public string title
		{
			get
			{
				if (this._title == null)
				{
					return string.Empty;
				}
				return this._title;
			}
		}

		public string url
		{
			get
			{
				if (this._url == null)
				{
					return string.Empty;
				}
				return this._url;
			}
		}

		public CrmModule(int __module_id, string __title, string __url, string __displayname, int __sortorder)
		{
			this._module_id = __module_id;
			this._title = __title;
			this._url = __url;
			this._displayname = __displayname;
			this._sortorder = __sortorder;
		}

		public CrmModule(string __title, string __url, string __displayname, int __sortorder) : this(0, __title, __url, __displayname, __sortorder)
		{
		}

		public CrmModule() : this(null, null, null, 0)
		{
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static IListSource _getAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_select");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(CrmModule.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static IListSource _getById(int __module_id)
		{
			if (__module_id < 0)
			{
				throw new ArgumentOutOfRangeException("module_id");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@module_id", SqlDbType.Int, 0, ParameterDirection.Input, __module_id);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_detail");
			List<IListSource> listSources = new List<IListSource>();
			sQLDataAccessLayer.ExecuteReaderCmd<IListSource>(sqlCommand, new GenerateListFromReader<IListSource>(CrmModule.GenerateListFromReader<IListSource>), ref listSources);
			if (listSources.Count <= 0)
			{
				return null;
			}
			return listSources[0];
		}

		[DataObjectMethod(DataObjectMethodType.Delete, true)]
		public static bool Delete(int __module_id)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@module_id", SqlDbType.Int, 0, ParameterDirection.Input, __module_id);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_delete");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		public static bool DownPage(string iPageId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@moduleid", SqlDbType.Int, 0, ParameterDirection.Input, int.Parse(iPageId.ToString()));
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_order_down");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CrmModule> theList)
		{
			while (returnData.Read())
			{
				CrmModule crmModule = new CrmModule((int)returnData["module_id"], Convert.ToString(returnData["title"]), Convert.ToString(returnData["url"]), GeneralConverter.ToString(returnData["displayname"]), (int)returnData["sortorder"]);
				theList.Add(crmModule);
			}
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<IListSource> theList)
		{
			DataTable dataTable = new DataTable();
			dataTable.Load(returnData);
			theList.Add(dataTable);
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public static List<CrmModule> GetAll()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_select");
			List<CrmModule> crmModules = new List<CrmModule>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmModule>(sqlCommand, new GenerateListFromReader<CrmModule>(CrmModule.GenerateListFromReader<CrmModule>), ref crmModules);
			return crmModules;
		}

		[DataObjectMethod(DataObjectMethodType.Select, false)]
		public static CrmModule GetById(int __module_id)
		{
			if (__module_id < 0)
			{
				throw new ArgumentOutOfRangeException("module_id");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@module_id", SqlDbType.Int, 0, ParameterDirection.Input, __module_id);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_detail");
			List<CrmModule> crmModules = new List<CrmModule>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmModule>(sqlCommand, new GenerateListFromReader<CrmModule>(CrmModule.GenerateListFromReader<CrmModule>), ref crmModules);
			if (crmModules.Count <= 0)
			{
				return null;
			}
			return crmModules[0];
		}

		public static CrmModule GetByTitle(string Title)
		{
			if (Title == string.Empty)
			{
				throw new ArgumentOutOfRangeException("Module Title");
			}
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 128, ParameterDirection.Input, Title);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_getbytitle");
			List<CrmModule> crmModules = new List<CrmModule>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmModule>(sqlCommand, new GenerateListFromReader<CrmModule>(CrmModule.GenerateListFromReader<CrmModule>), ref crmModules);
			if (crmModules.Count <= 0)
			{
				return null;
			}
			return crmModules[0];
		}

		[DataObjectMethod(DataObjectMethodType.Insert, true)]
		public bool Insert()
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 128, ParameterDirection.Input, this._title);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@url", SqlDbType.VarChar, 256, ParameterDirection.Input, this._url);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@displayname", SqlDbType.VarChar, 32, ParameterDirection.Input, this._displayname);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sortorder", SqlDbType.Int, 0, ParameterDirection.Input, this._sortorder);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_insert");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		public static List<CrmModule> ListModule(int GroupID)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@groupid", SqlDbType.Int, 0, ParameterDirection.Input, GroupID);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_listbygroup");
			List<CrmModule> crmModules = new List<CrmModule>();
			sQLDataAccessLayer.ExecuteReaderCmd<CrmModule>(sqlCommand, new GenerateListFromReader<CrmModule>(CrmModule.GenerateListFromReader<CrmModule>), ref crmModules);
			return crmModules;
		}

		[DataObjectMethod(DataObjectMethodType.Update, true)]
		public static bool Update(int __module_id, string __title, string __url, string __displayname, int __sortorder)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@module_id", SqlDbType.Int, 0, ParameterDirection.Input, __module_id);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@title", SqlDbType.VarChar, 128, ParameterDirection.Input, __title);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@url", SqlDbType.VarChar, 256, ParameterDirection.Input, __url);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@displayname", SqlDbType.VarChar, 32, ParameterDirection.Input, __displayname);
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@sortorder", SqlDbType.Int, 0, ParameterDirection.Input, __sortorder);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_update");
			if (sQLDataAccessLayer.ExecuteNonQuery(sqlCommand) > 0)
			{
				return true;
			}
			return false;
		}

		public static bool UpPage(string iPageId)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCRMConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@moduleid", SqlDbType.Int, 0, ParameterDirection.Input, int.Parse(iPageId.ToString()));
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "modules_order_up");
			if (sQLDataAccessLayer.ExecuteScalarCmd(sqlCommand) != DBNull.Value)
			{
				return true;
			}
			return false;
		}
	}
}