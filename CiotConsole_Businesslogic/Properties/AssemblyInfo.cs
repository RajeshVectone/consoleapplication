﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Universal Data Teknologi")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © Universal Data Teknologi 2006")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("BusinessLogic")]
[assembly: AssemblyTitle("BusinessLogic")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: Guid("bc0c0de7-284a-457a-ae1b-2456d989f716")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
