using Switchlab.BusinessLogicLayer;
using Switchlab.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Switchlab.Product.BusinessLogicLayer
{
	public class CIOTDetailData
	{
		private const string SP_GET_DETAIL_CIOT = "sp_get_ciot_manual";

		private const string SP_GET_DETAIL_CIOT_AUTO = "sp_get_ALLDetailCIOT";

		private const string SP_GET_DETAIL_TEST = "test_xml";

		private string _sMobileNo = string.Empty;

		private DateTime _dFirstUpdate = new DateTime();

		private DateTime _dSubmitDate = new DateTime();

		private string _sLastName = string.Empty;

		private string _sPrefix = string.Empty;

		private string _sInitial = string.Empty;

		private string _sStreet = string.Empty;

		private string _sStreetNumber = "";

		private string _sStrtPrefix = string.Empty;

		private string _sPostCode = "";

		private string _sFPostCode = string.Empty;

		private string _sCity = string.Empty;

		private string _sLocation = string.Empty;

		private string _sCountry = string.Empty;

		private string _sAddrType = string.Empty;

		private string _sConnType = string.Empty;

		private string _sSrvType = string.Empty;

		private string _sSrvProvider = string.Empty;

		private string _sNetProvider = string.Empty;

		private DateTime _dGenDate = new DateTime();

		private DateTime _dValDateFrom = new DateTime();

		private DateTime _dValDateTo = new DateTime();

		public string AddressType
		{
			get
			{
				return this._sAddrType;
			}
			set
			{
				this._sAddrType = value;
			}
		}

		public string City
		{
			get
			{
				return this._sCity;
			}
			set
			{
				this._sCity = value;
			}
		}

		public string ConnType
		{
			get
			{
				return this._sConnType;
			}
			set
			{
				this._sConnType = value;
			}
		}

		public string Country
		{
			get
			{
				return this._sCountry;
			}
			set
			{
				this._sCountry = value;
			}
		}

		public DateTime FirstUpdate
		{
			get
			{
				return this._dFirstUpdate;
			}
			set
			{
				this._dFirstUpdate = value;
			}
		}

		public string ForeignPostCode
		{
			get
			{
				return this._sFPostCode;
			}
			set
			{
				this._sFPostCode = value;
			}
		}

		public DateTime GenerateDateTime
		{
			get
			{
				return this._dGenDate;
			}
			set
			{
				this._dGenDate = value;
			}
		}

		public string Initial
		{
			get
			{
				return this._sInitial;
			}
			set
			{
				this._sInitial = value;
			}
		}

		public string LastName
		{
			get
			{
				return this._sLastName;
			}
			set
			{
				this._sLastName = value;
			}
		}

		public string Location
		{
			get
			{
				return this._sLocation;
			}
			set
			{
				this._sLocation = value;
			}
		}

		public string MobileNo
		{
			get
			{
				return this._sMobileNo;
			}
			set
			{
				this._sMobileNo = value;
			}
		}

		public string NetworkProvider
		{
			get
			{
				return this._sNetProvider;
			}
			set
			{
				this._sNetProvider = value;
			}
		}

		public string PostCode
		{
			get
			{
				return this._sPostCode;
			}
			set
			{
				this._sPostCode = value;
			}
		}

		public string Prefix
		{
			get
			{
				return this._sPrefix;
			}
			set
			{
				this._sPrefix = value;
			}
		}

		public string ServiceProvider
		{
			get
			{
				return this._sSrvProvider;
			}
			set
			{
				this._sSrvProvider = value;
			}
		}

		public string ServiceType
		{
			get
			{
				return this._sSrvType;
			}
			set
			{
				this._sSrvType = value;
			}
		}

		public string Street
		{
			get
			{
				return this._sStreet;
			}
			set
			{
				this._sStreet = value;
			}
		}

		public string StreetNumber
		{
			get
			{
				return this._sStreetNumber;
			}
			set
			{
				this._sStreetNumber = value;
			}
		}

		public string StreetPrefix
		{
			get
			{
				return this._sStrtPrefix;
			}
			set
			{
				this._sStrtPrefix = value;
			}
		}

		public DateTime SubmitDate
		{
			get
			{
				return this._dSubmitDate;
			}
			set
			{
				this._dSubmitDate = value;
			}
		}

		public DateTime ValDateFrom
		{
			get
			{
				return this._dValDateFrom;
			}
			set
			{
				this._dValDateFrom = value;
			}
		}

		public DateTime ValDateTo
		{
			get
			{
				return this._dValDateTo;
			}
			set
			{
				this._dValDateTo = value;
			}
		}

		public CIOTDetailData()
		{
		}

		private static void GenerateListFromReader<T>(IDataReader returnData, ref List<CIOTDetailData> lstDetail)
		{
			while (returnData.Read())
			{
				CIOTDetailData cIOTDetailDatum = new CIOTDetailData()
				{
					LastName = GeneralConverter.ToString(returnData["LastName"]),
					Prefix = GeneralConverter.ToString(returnData["Prefix"]),
					Initial = GeneralConverter.ToString(returnData["Initial"]),
					Street = GeneralConverter.ToString(returnData["Street"]),
					StreetNumber = GeneralConverter.ToString(returnData["StreetNumber"]),
					StreetPrefix = GeneralConverter.ToString(returnData["StreetNumberPrefix"]),
					PostCode = GeneralConverter.ToString(returnData["PostCode"]),
					ForeignPostCode = GeneralConverter.ToString(returnData["ForeignPostCode"]),
					City = GeneralConverter.ToString(returnData["City"]),
					Location = GeneralConverter.ToString(returnData["Location"]),
					Country = GeneralConverter.ToString(returnData["Country"]),
					AddressType = GeneralConverter.ToString(returnData["AddressType"]),
					ConnType = GeneralConverter.ToString(returnData["ConnectionType"]),
					ServiceType = GeneralConverter.ToString(returnData["ServiceType"]),
					ServiceProvider = GeneralConverter.ToString(returnData["ServiceProvider"])
				};
				DateTime now = DateTime.Now;
				cIOTDetailDatum.GenerateDateTime = GeneralConverter.ToDateTime(now.AddHours(1));
				cIOTDetailDatum.FirstUpdate = GeneralConverter.ToDateTime(returnData["FirstUpdate"]);
				DateTime generateDateTime = cIOTDetailDatum.GenerateDateTime;
				cIOTDetailDatum.ValDateFrom = GeneralConverter.ToDateTime(generateDateTime.AddDays(-1));
				DateTime dateTime = cIOTDetailDatum.GenerateDateTime;
				cIOTDetailDatum.ValDateTo = GeneralConverter.ToDateTime(dateTime.AddDays(1));
				cIOTDetailDatum.MobileNo = GeneralConverter.ToString(returnData["MobileNo"]);
				lstDetail.Add(cIOTDetailDatum);
			}
		}

		public List<CIOTDetailData> GetDetailData(DateTime dStartDate, DateTime dEndDate)
		{
			SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(BusinessFacade.GetCIOTConnectionString().ConnectionString);
			SqlCommand sqlCommand = new SqlCommand();
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@StartDate", SqlDbType.DateTime, 0, ParameterDirection.Input, dStartDate.ToShortDateString());
			sQLDataAccessLayer.AddParamToSQLCmd(sqlCommand, "@EndDate", SqlDbType.DateTime, 0, ParameterDirection.Input, dEndDate);
			sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "sp_get_ciot_manual");
			sqlCommand.CommandTimeout = 0;
			List<CIOTDetailData> cIOTDetailDatas = new List<CIOTDetailData>();
			sQLDataAccessLayer.ExecuteReaderCmd<CIOTDetailData>(sqlCommand, new GenerateListFromReader<CIOTDetailData>(CIOTDetailData.GenerateListFromReader<CIOTDetailData>), ref cIOTDetailDatas);
			return cIOTDetailDatas;
		}

		public List<CIOTDetailData> GetDetailData()
		{
			List<CIOTDetailData> cIOTDetailDatas = new List<CIOTDetailData>();
			try
			{
				SQLDataAccessLayer sQLDataAccessLayer = new SQLDataAccessLayer(ConfigurationManager.AppSettings["strConn"]);
				SqlCommand sqlCommand = new SqlCommand();
				sQLDataAccessLayer.SetCommandType(sqlCommand, CommandType.StoredProcedure, "test_xml");
				sqlCommand.CommandTimeout = 0;
				sQLDataAccessLayer.ExecuteReaderCmd<CIOTDetailData>(sqlCommand, new GenerateListFromReader<CIOTDetailData>(CIOTDetailData.GenerateListFromReader<CIOTDetailData>), ref cIOTDetailDatas);
			}
			catch (Exception exception)
			{
				string message = exception.Message;
			}
			return cIOTDetailDatas;
		}
	}
}