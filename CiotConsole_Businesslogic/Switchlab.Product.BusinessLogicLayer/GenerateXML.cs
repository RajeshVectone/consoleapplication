using Abonee.NL;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;

namespace Switchlab.Product.BusinessLogicLayer
{
	public class GenerateXML : XmlGeneratorBase
	{
		private string DIENSTAANBIEDER = ConfigurationManager.AppSettings["DienstAanbieder"].ToString();

		private string DIENSTSOORT = ConfigurationManager.AppSettings["DienstSoort"].ToString();

		private string NETWERKAANBIEDER = ConfigurationManager.AppSettings["NetwerkAanbieder"].ToString();

		private string SOORTVERBINDING = ConfigurationManager.AppSettings["soortVerbinding"].ToString();

		public GenerateXML()
		{
		}

		public void AddAbonne(List<CIOTDetailData> lstDetail)
		{
			foreach (CIOTDetailData cIOTDetailDatum in lstDetail)
			{
				AboneeType prefix = base.NewAbonee();
				prefix.Achternaam.Append().Value = cIOTDetailDatum.LastName.Trim();
				prefix.Voorvoegsels.Append().Value = cIOTDetailDatum.Prefix;
				prefix.Voorletters.Append().Value = cIOTDetailDatum.Initial.Trim();
				if (cIOTDetailDatum.Street == "-")
				{
					cIOTDetailDatum.Street = "";
				}
				if (cIOTDetailDatum.StreetNumber == "-")
				{
					cIOTDetailDatum.StreetNumber = "";
				}
				if (cIOTDetailDatum.PostCode == "-")
				{
					cIOTDetailDatum.PostCode = "";
				}
				prefix.Straatnaam.Append().Value = cIOTDetailDatum.Street;
				prefix.Huisnummer.Append().Value = cIOTDetailDatum.StreetNumber.Trim().ToString();
				prefix.Huisnummertoevoeging.Append().Value = cIOTDetailDatum.StreetPrefix;
				prefix.Postcode.Append().Value = cIOTDetailDatum.PostCode.Trim().Replace(" ", "").ToString();
				prefix.PostcodeBL.Append().Value = cIOTDetailDatum.ForeignPostCode;
				prefix.Woonplaats.Append().Value = cIOTDetailDatum.City.Trim();
				prefix.LocatieOms.Append().Value = cIOTDetailDatum.Location.Trim();
				prefix.Land.Append().Value = cIOTDetailDatum.Country.Trim();
				if (cIOTDetailDatum.AddressType == "" || cIOTDetailDatum.PostCode == "")
				{
					prefix.soortNAW.Append().Value = "N";
				}
				else
				{
					prefix.soortNAW.Append().Value = cIOTDetailDatum.AddressType;
				}
				prefix.soortVerbinding.Append().Value = this.SOORTVERBINDING;
				prefix.DienstSoort.Append().Value = this.DIENSTSOORT;
				prefix.DienstAanbieder.Append().Value = this.DIENSTAANBIEDER;
				prefix.NetwerkAanbieder.Append().Value = this.NETWERKAANBIEDER;
				prefix.Generatiedatum.Append().Value = cIOTDetailDatum.GenerateDateTime.ToString("yyyy-MM-ddThh:mm:ss.0Z");
				prefix.Ingangsdatumtijd.Append().Value = cIOTDetailDatum.ValDateFrom.ToString("yyyy-MM-ddThh:mm:ss.0Z");
				prefix.Einddatumtijd.Append().Value = cIOTDetailDatum.ValDateTo.ToString("yyyy-MM-ddThh:mm:ss.0Z");
				prefix.AbonneeNummer.Append().Telefoonnummer.Append().Value = cIOTDetailDatum.MobileNo.Trim().ToString();
			}
		}

		public void GenerateSoapXML()
		{
			string empty = string.Empty;
			empty = string.Concat("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>", "<env:Envelope ");
			empty = string.Concat(empty, "xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">");
			empty = string.Concat(empty, "<soap:Body>");
			empty = string.Concat(empty, "<Header>");
			empty = string.Concat(empty, "<RequestID>121378079376006</RequestID>");
			empty = string.Concat(empty, "</Header>");
		}

		public string GetCounter()
		{
			string end;
			using (StreamReader streamReader = new StreamReader(ConfigurationManager.AppSettings["counterFile"]))
			{
				end = streamReader.ReadToEnd();
				char[] chrArray = new char[] { '\n', '\r' };
				end.Split(chrArray);
				try
				{
					end = (DateTime.Now.DayOfYear != 1 ? Convert.ToString(DateTime.Now.DayOfYear) : Convert.ToString(0));
				}
				catch (Exception exception)
				{
					throw exception;
				}
			}
			using (StreamWriter streamWriter = new StreamWriter(ConfigurationManager.AppSettings["counterFile"]))
			{
				streamWriter.Write(end);
			}
			if (Convert.ToInt32(end) < 10)
			{
				return string.Concat("00", end);
			}
			if (Convert.ToInt32(end) <= 9 || Convert.ToInt32(end) > 99)
			{
				return end;
			}
			return string.Concat("0", end);
		}

		public new void Save(string fileName)
		{
			base.Save(fileName);
		}

		public new string SaveToString()
		{
			return base.SaveToString();
		}
	}
}