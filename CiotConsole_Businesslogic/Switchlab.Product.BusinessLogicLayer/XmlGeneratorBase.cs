using Abonee.NL;
using System;
using System.IO;
using System.Text;

namespace Switchlab.Product.BusinessLogicLayer
{
	public class XmlGeneratorBase
	{
		private Abonees _doc;

		private AboneesType _root;

		private AboneeType _root1;

		private AboneeType _doc1;

		public XmlGeneratorBase()
		{
			this._doc = Abonees.CreateDocument();
			this._root = this._doc.Abonnees.Append();
			this._doc.SetSchemaLocation("xsd-Telco-CIOT-v1.6.xsd");
		}

		protected AboneeType NewAbonee()
		{
			return this._root.Abonnee.Append();
		}

		protected AboneesType NewAbonees()
		{
			return this._root.Abonnees.Append();
		}

		protected AbonneeNummerType NewAbonneeNummerType()
		{
			return this._doc1.AbonneeNummer.Append();
		}

		protected void Save(string fileName)
		{
			this._doc.SaveToFile(fileName, true);
			StringBuilder stringBuilder = new StringBuilder();
			using (StreamReader streamReader = new StreamReader(fileName))
			{
				stringBuilder.Append(streamReader.ReadToEnd());
			}
			stringBuilder.Replace("<UnknownParty>", "<UnknownParty/>");
			stringBuilder.Replace("</UnknownParty>", "");
			StreamWriter streamWriter = new StreamWriter(fileName);
			streamWriter.Write(stringBuilder.ToString());
			streamWriter.Close();
		}

		protected string SaveToString()
		{
			return this._doc.SaveToString(true);
		}
	}
}