﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APN
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Debug("===================APN Start======================");
            string response = string.Empty;
            int _SMS_Success_Count = 0;
            try
            {
                List<APN.DBHelp.smsomasettingexegetcustomerinfoOutput> result = new List<APN.DBHelp.smsomasettingexegetcustomerinfoOutput>();
                //DB hit
                result = DBHelp.smsomasettingexegetcustomerinfo();

                if (result != null && result.Count() > 0)
                {
                    for (int iCount = 0; iCount < result.Count(); iCount++)
                    {
                        try
                        {
                            //SMS hit
                            response = SMSHelp.SendAPN(result[iCount].sitecode, "URL", result[iCount].mobileno, result[iCount].ota_command);
                            ++_SMS_Success_Count;

                            //DB hit
                            DBHelp.smsomaexeupdateotastatus(result[iCount].ID, result[iCount].sitecode, Convert.ToInt32(response.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries)[0]));
                        }
                        catch (Exception ex)
                        {
                            Log.Debug("Loop Exception : " + ex.Message);
                        }
                    }
                }
                else
                {
                    Log.Debug("No records to send APN Settings");
                }
            }
            catch (Exception ex)
            {
                Log.Debug("Main : " + ex.Message);
            }
            Log.Debug("SMS_Success_Count : " + _SMS_Success_Count.ToString());
            Log.Debug("====================APN End=======================");
        }
    }
}
