﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace APN
{
    class SMSHelp
    {
        public static string SendAPN(string countrycode, string SmsServer, string msisdn, string setting_sms)
        {
            Log.Debug("SendAPN");
            try
            {
                string resulXmlFromWebService = string.Empty;

                //string setting_sms = ConfigurationSettings.AppSettings["VM" + countrycode].ToString();
                //Log.Debug(setting_sms);

                string sServerApiUrl = SmsServer;
                Log.Debug("URL : " + sServerApiUrl);

                string msg = setting_sms;

                int lenStr = msg.Length;

                int smsCount = 0;
                if (lenStr % 280 != 0)
                    smsCount = (lenStr / 280) + 1;
                else
                    smsCount = lenStr / 280;

                for (int j = 0; j < smsCount; j++)
                {
                    string smsMsg = "";
                    if (j == smsCount - 1)
                        smsMsg = msg.Substring(j * 280, lenStr - (280 * j));
                    else
                        smsMsg = msg.Substring(j * 280, 280);

                    string postData = "orig-addr=111" +
                    "&orig-noa=0" +
                    "&support-long=1" +
                    "&dest-addr=" + msisdn +
                    "&dest-noa=1" +
                    "&tp-dcs=21" +
                    "&tp-pid=245" +
                    "&tp-udhi=1" +
                    "&tp-ud=" + smsMsg +
                    "&payload-type=text";



                    Log.Debug("postData : " + postData);
                    HttpWebRequest Webrequest = (HttpWebRequest)WebRequest.Create(sServerApiUrl);
                    Webrequest.Method = "POST";
                    Webrequest.ContentType = "application/x-www-form-urlencoded";

                    ASCIIEncoding enc = new ASCIIEncoding();
                    byte[] data = enc.GetBytes(postData);
                    Webrequest.ContentLength = data.Length;

                    Stream newStream = Webrequest.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();

                    HttpWebResponse wr = (HttpWebResponse)Webrequest.GetResponse();
                    StreamReader srd = new StreamReader(wr.GetResponseStream());
                    resulXmlFromWebService = srd.ReadToEnd();
                    Log.Debug("resulXmlFromWebService : " + resulXmlFromWebService);
                }
                return resulXmlFromWebService;
            }
            catch (Exception ex)
            {
                Log.Debug("Exception while sending SMS : " + ex.Message);
                throw new Exception(ex.Message);
            }
        }


    }
}
