﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;

namespace APN
{
    class DBHelp
    {
        public class smsomasettingexegetcustomerinfoOutput
        {
            public int? ID { get; set; }
            public string mobileno { get; set; }
            public string sitecode { get; set; }
            public string iccid { get; set; }
            public string ota_command { get; set; }
            public string ota_length { get; set; }
            public DateTime? LastUpdate { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public static List<smsomasettingexegetcustomerinfoOutput> smsomasettingexegetcustomerinfo()
        {
            Log.Debug("smsomasettingexegetcustomerinfo");
            Log.Debug("Input : Get Method");
            string Responce = string.Empty;
            List<smsomasettingexegetcustomerinfoOutput> OutputList = new List<smsomasettingexegetcustomerinfoOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["APN"].ToString()))
                {
                    conn.Open();
                    var sp = "smsoma_setting_exe_get_customer_info";

                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                //@campid = CampID,
                                //@mobileno = msisdn.Remove(msisdn.Length - 1)
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new smsomasettingexegetcustomerinfoOutput()
                        {
                            ID = r.ID,
                            mobileno = r.mobileno,
                            sitecode = r.sitecode,
                            iccid = r.iccid,
                            ota_command = r.ota_command,
                            ota_length = r.ota_length,
                            LastUpdate = r.LastUpdate,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    //else
                    //{
                    //    smsomasettingexegetcustomerinfoOutput outputobj = new smsomasettingexegetcustomerinfoOutput();
                    //    outputobj.errcode = -1;
                    //    outputobj.errmsg = "No Rec found";
                    //    OutputList.Add(outputobj);
                    //}
                }
            }
            catch (Exception ex)
            {
                //smsomasettingexegetcustomerinfoOutput outputobj = new smsomasettingexegetcustomerinfoOutput();
                //outputobj.errcode = -1;
                //outputobj.errmsg = ex.Message;
                //OutputList.Add(outputobj);
                Log.Debug("Catch in smsoma_setting_exe_get_customer_info: " + ex.Message);
            }
            return OutputList;
        }

        public static string smsomaexeupdateotastatus(int? id, string sitecode, int sms_id)
        {
            Log.Debug("smsomaexeupdateotastatus");
            Log.Debug("Input : id = " + id + ", sitecode = " + sitecode + ", sms_id = " + sms_id);
            Log.Debug("Result wont generate in particular SP");
            string Responce = string.Empty;
            try
            {
                using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["APN"].ToString()))
                {
                    conn.Open();
                    var sp = "smsoma_exe_update_ota_status";

                    //var result = conn.Query<dynamic>(
                    conn.Query<dynamic>(
                            sp, new
                            {
                                @id = id,
                                @sitecode = sitecode,
                                @sms_id = sms_id
                            },
                            commandType: CommandType.StoredProcedure);
                    //Log.Debug("result : " + JsonConvert.SerializeObject(result));
                    //if (result != null && result.Count() > 0)
                    //{
                    //    Responce = result.ElementAt(0).errmsg;
                    //}
                    //else
                    //{
                    //    Responce = "No Rec found";
                    //}
                }
            }
            catch (Exception ex)
            {
                Responce = ex.Message;
                Log.Debug("Catch in smsoma_exe_update_ota_status: " + ex.Message);
            }
            return Responce;
        }


    }
}
