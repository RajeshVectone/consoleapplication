﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using NLog;
#endregion


namespace PHPMySQLConsole
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region InsertRecord
        public static int InsertRecord(InsertRecordInput input)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "website_central_insert_payment_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sitecode = input.sitecode,
                                @mobileno = input.mobileno,
                                @unique_code = input.unique_code,
                                @pay_ref = input.pay_ref,
                                @step = input.step,
                                @reason_code = input.reason_code,
                                @comments = input.comments,
                                @cc_no = input.cc_no,
                                @ip_client = input.ip_client,
                                @now = input.now
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode == 0)
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertRecord() ex : " + ex.Message);
            }
            return -1;
        }
        #endregion
    }
}
