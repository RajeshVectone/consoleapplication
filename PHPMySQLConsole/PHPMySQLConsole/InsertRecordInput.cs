﻿namespace PHPMySQLConsole
{
    public class InsertRecordInput
    {
        public string now { get; set; }
        public string sitecode { get; set; }
        public string mobileno { get; set; }
        public string unique_code { get; set; }
        public string pay_ref { get; set; }
        public string step { get; set; }
        public string reason_code { get; set; }
        public string comments { get; set; }
        public string cc_no { get; set; }
        public string ip_client { get; set; }
    }
}