﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace PHPMySQLConsole
{
    class Program
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        static void Main(string[] args)
        {
            //while (true)
            //{
            try
            {
                Console.WriteLine("PHPMySQLConsole : Process Started");
                log.Debug("Process Started");
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["PHPURL"]);

                string responseData = string.Empty;
                HttpWebResponse httpResponse = (HttpWebResponse)webRequest.GetResponse();
                using (StreamReader responseReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    responseData = responseReader.ReadToEnd();
                }
                log.Debug("responseData : " + responseData);
                string[] result = responseData.Split(new[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                log.Debug("result count : " + result.Length);
                foreach (string s in result)
                {
                    try
                    {
                        if (s.Length > 0)
                        {
                            //log.Debug("line : " + String.Join(",", result));
                            string[] value = s.Split(',');

                            InsertRecordInput input = new InsertRecordInput() { now = value[0], sitecode = value[1], mobileno = value[2], unique_code = value[3], pay_ref = value[4], step = value[5], reason_code = value[6], comments = value[7], cc_no = value[8], ip_client = value[9] };
                            DataAccess.InsertRecord(input);
                        }
                    }
                    catch (Exception iex)
                    {
                        log.Error("iex : " + iex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
            finally
            {
                log.Debug("Process Ended");
            }
            //    System.Threading.Thread.Sleep(60000);
            //}
        }
    }
}
