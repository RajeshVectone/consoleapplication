using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
namespace paypalpaymentapi
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
           "ClientAccessToken",
           routeTemplate: "v1/clientaccesstoken",
           defaults: new { controller = "clientaccesstoken" });

            config.Routes.MapHttpRoute(
		   "AuthToken",
		   routeTemplate: "v1/authtoken",
		   defaults: new { controller = "AuthToken" });
		
			config.Routes.MapHttpRoute(
		   "ClientToken",
		   routeTemplate: "v1/clienttoken",
		   defaults: new { controller = "ClientToken" });
			
			config.Routes.MapHttpRoute(
		   "ExecutePayment",
		   routeTemplate: "v1/executepayment",
		   defaults: new { controller = "ExecutePayment" });

            config.Routes.MapHttpRoute(
           "AutoRenew",
           routeTemplate: "v1/autorenew",
           defaults: new { controller = "AutoRenew" });

            config.Routes.MapHttpRoute(
           "ExecuteIOSPayment",
           routeTemplate: "v1/executeiospayment",
           defaults: new { controller = "ExecuteIOSPayment" }); 
        }
	}
}





















































































































































