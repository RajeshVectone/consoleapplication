﻿using System;

namespace paypalpaymentapi
{
    public class ExecutePaymentInput
    {
        public Decimal amount { get; set; }
        public string  nonce { get; set; }
        public string mobileno { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string account_type { get; set; }
        public string email { get; set; }
        public string ip_payment_agent { get; set; }
        public string ip_payment_client { get; set; }
        public string last6digitccno { get; set; }
        public string csreasoncode { get; set; }
        public string productcode { get; set; }
        //07-Jul-2017 : Moorthy : Added for Auto Topup
        public bool? autotopup { get; set; }
        public string devicetype { get; set; }
        //13-Feb-2019: Moorthy : Added for 'Some phone numbers are not captured in full' issue
        public string AliasName { get; set; }
        //20-Jun-2019 : Moorthy : Added for adding Bundle service
        public string device_type { get; set; }
        public string browser { get; set; }
        public string os_version { get; set; }
        public string topup_url { get; set; }
        public int bundle_id { get; set; }
    }

    public class ExecutePaymentOutput : CommonOutput
    {
        public string transaction_id { get; set; }
        public string order_id { get; set; }
    }
} 