﻿using System;
namespace paypalpaymentapi
{
    public class PayPalTransaction : CommonOutput
    {
        private string _amount;

        public string Amount
        {
            get
            {
                return this._amount;
            }
            set
            {
                this._amount = value;
            }
        }

        public string changeIssueAmount
        {
            get;
            set;
        }

        public string changeIssueExpiryDate
        {
            get;
            set;
        }

        public string changeIssueVoucherCurr
        {
            get;
            set;
        }

        public string changeIssueVoucherNumber
        {
            get;
            set;
        }

        public string currencyConversion
        {
            get;
            set;
        }

        public int errCode
        {
            get;
            set;
        }

        public string errMsg
        {
            get;
            set;
        }

        public string MtID
        {
            get;
            set;
        }

        public string orderNumber
        {
            get;
            set;
        }

        public string paysafeTransactionId
        {
            get;
            set;
        }

        public string resultCode
        {
            get;
            set;
        }

        public string ReturnCustomerURL
        {
            get;
            set;
        }

        public string settleAmount
        {
            get;
            set;
        }

        public string transactionId
        {
            get;
            set;
        }

        public string txCode
        {
            get;
            set;
        }

        public string txDescription
        {
            get;
            set;
        }
    }
} 