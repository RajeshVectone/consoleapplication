﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace paypalpaymentapi
{
    public class PayPalRequest
    {
        private string _mtId;

        private string _amount;

        private string _currency;

        private string _okUrl;

        private string _nokUrl;

        private string _orderNumber;

        private string _vat;

        private string _pnurl;

        private bool _saverflag;

        private string _vatPercentage;

        private string _total;

        private string _sitecode;

        private string _applicationcode;

        private string _productcode;

        private string _paymentagent;

        private string _servicetype;

        private string _paymentmode;

        private int _paymentstep;

        private int _paymentstatus;

        private string _accountid;

        private string _totalamount;

        private string _csreasoncode;

        private string _email;

        private string _IPpaymentagent;

        private string _IPClient;

        private string _webSite;

        private string _description;

        private string _mobile;

        private string _name;

        private string _street;

        private string _city;

        private string _postCode;

        private string _country;

        public string Accountid
        {
            get
            {
                return this._accountid;
            }
            set
            {
                this._accountid = value;
            }
        }

        public string Amount
        {
            get
            {
                return this._amount;
            }
            set
            {
                this._amount = value;
            }
        }

        public string Applicationcode
        {
            get
            {
                return this._applicationcode;
            }
            set
            {
                this._applicationcode = value;
            }
        }

        public string City
        {
            get
            {
                return this._city;
            }
            set
            {
                this._city = value;
            }
        }

        public string Country
        {
            get
            {
                return this._country;
            }
            set
            {
                this._country = value;
            }
        }

        public string Csreasoncode
        {
            get
            {
                return this._csreasoncode;
            }
            set
            {
                this._csreasoncode = value;
            }
        }

        public string Currency
        {
            get
            {
                return this._currency;
            }
            set
            {
                this._currency = value;
            }
        }

        public string Description
        {
            get
            {
                return this._description;
            }
            set
            {
                this._description = value;
            }
        }

        public string Email
        {
            get
            {
                return this._email;
            }
            set
            {
                this._email = value;
            }
        }

        public string IPClient
        {
            get
            {
                return this._IPClient;
            }
            set
            {
                this._IPClient = value;
            }
        }

        public string IPpaymentagent
        {
            get
            {
                return this._IPpaymentagent;
            }
            set
            {
                this._IPpaymentagent = value;
            }
        }

        public string Mobile
        {
            get
            {
                return this._mobile;
            }
            set
            {
                this._mobile = value;
            }
        }

        public string MtId
        {
            get
            {
                return this._mtId;
            }
            set
            {
                this._mtId = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public string NokUrl
        {
            get
            {
                return this._nokUrl;
            }
            set
            {
                this._nokUrl = value;
            }
        }

        public string OkUrl
        {
            get
            {
                return this._okUrl;
            }
            set
            {
                this._okUrl = value;
            }
        }

        public string OrderNumber
        {
            get
            {
                return this._orderNumber;
            }
            set
            {
                this._orderNumber = value;
            }
        }

        public string Paymentagent
        {
            get
            {
                return this._paymentagent;
            }
            set
            {
                this._paymentagent = value;
            }
        }

        public string Paymentmode
        {
            get
            {
                return this._paymentmode;
            }
            set
            {
                this._paymentmode = value;
            }
        }

        public int Paymentstatus
        {
            get
            {
                return this._paymentstatus;
            }
            set
            {
                this._paymentstatus = value;
            }
        }

        public int Paymentstep
        {
            get
            {
                return this._paymentstep;
            }
            set
            {
                this._paymentstep = value;
            }
        }

        public string Pnurl
        {
            get
            {
                return this._pnurl;
            }
            set
            {
                this._pnurl = value;
            }
        }

        public string PostCode
        {
            get
            {
                return this._postCode;
            }
            set
            {
                this._postCode = value;
            }
        }

        public string Productcode
        {
            get
            {
                return this._productcode;
            }
            set
            {
                this._productcode = value;
            }
        }

        public bool Saverflag
        {
            get
            {
                return this._saverflag;
            }
            set
            {
                this._saverflag = value;
            }
        }

        public string Servicetype
        {
            get
            {
                return this._servicetype;
            }
            set
            {
                this._servicetype = value;
            }
        }

        public string Sitecode
        {
            get
            {
                return this._sitecode;
            }
            set
            {
                this._sitecode = value;
            }
        }

        public string Street
        {
            get
            {
                return this._street;
            }
            set
            {
                this._street = value;
            }
        }

        public string Total
        {
            get
            {
                return this._total;
            }
            set
            {
                this._total = value;
            }
        }

        public string Totalamount
        {
            get
            {
                return this._totalamount;
            }
            set
            {
                this._totalamount = value;
            }
        }

        public string Vat
        {
            get
            {
                return this._vat;
            }
            set
            {
                this._vat = value;
            }
        }

        public string VatPercentage
        {
            get
            {
                return this._vatPercentage;
            }
            set
            {
                this._vatPercentage = value;
            }
        }

        public string WebSite
        {
            get
            {
                return this._webSite;
            }
            set
            {
                this._webSite = value;
            }
        }
    }
}