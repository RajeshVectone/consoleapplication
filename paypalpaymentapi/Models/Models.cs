﻿namespace paypalpaymentapi
{
    public class CommonOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class ClientTokenOutput : CommonOutput
    {
        public string client_access_token { get; set; }
    }

    public class ClientAccessTokenOutput : CommonOutput
    {
        public string client_access_token { get; set; }
    }
}