2017-12-13 15:43:03.7198 INFO AutoRenew Controller
2017-12-13 15:43:04.3099 INFO Input : {
  "subscription_id": "bnpqsw6",
  "amount": "5",
  "currency": "GBP",
  "country": "UK",
  "referencecode": "VMUK-AUT005-PPL-1898506",
  "accountid": "447451846188",
  "sitecode": "VMUK",
  "product_code": "UKTOPUP",
  "application_code": "VMUK",
  "payment_agent": "VMUK",
  "service_type": "MVNO",
  "payment_mode": "DEF",
  "client_ip": "192.168.2.183",
  "email": "test@gmail.com",
  "account_type": "1",
  "last6digitccno": null
}
2017-12-13 15:43:10.1438 INFO orderId : VMUK-AUT005-PPL-1898506
2017-12-13 15:45:25.4030 INFO result : {
  "CreditCardVerification": null,
  "Transaction": {
    "Id": "9j7hrp9k",
    "AddOns": [],
    "Amount": 5.00,
    "AvsErrorResponseCode": null,
    "AvsPostalCodeResponseCode": "I",
    "AvsStreetAddressResponseCode": "I",
    "BillingAddress": {
      "Id": null,
      "CustomerId": null,
      "FirstName": null,
      "LastName": null,
      "Company": null,
      "StreetAddress": null,
      "ExtendedAddress": null,
      "Locality": null,
      "Region": null,
      "PostalCode": null,
      "CountryCodeAlpha2": null,
      "CountryCodeAlpha3": null,
      "CountryCodeNumeric": null,
      "CountryName": null,
      "CreatedAt": null,
      "UpdatedAt": null
    },
    "Channel": null,
    "CreatedAt": "2017-12-13T10:15:23Z",
    "CreditCard": {
      "Bin": null,
      "CardholderName": null,
      "CardType": {},
      "CreatedAt": null,
      "CustomerId": null,
      "IsDefault": null,
      "IsVenmoSdk": false,
      "IsExpired": null,
      "CustomerLocation": {},
      "LastFour": null,
      "UniqueNumberIdentifier": null,
      "Subscriptions": [],
      "Token": "bnpqsw6",
      "UpdatedAt": null,
      "BillingAddress": {
        "Id": null,
        "CustomerId": null,
        "FirstName": null,
        "LastName": null,
        "Company": null,
        "StreetAddress": null,
        "ExtendedAddress": null,
        "Locality": null,
        "Region": null,
        "PostalCode": null,
        "CountryCodeAlpha2": null,
        "CountryCodeAlpha3": null,
        "CountryCodeNumeric": null,
        "CountryName": null,
        "CreatedAt": null,
        "UpdatedAt": null
      },
      "ExpirationMonth": null,
      "ExpirationYear": null,
      "Prepaid": {},
      "Payroll": {},
      "Debit": {},
      "Commercial": {},
      "Healthcare": {},
      "DurbinRegulated": {},
      "ImageUrl": "https://assets.braintreegateway.com/payment_method_logo/unknown.png?environment=production",
      "Verification": null,
      "CountryOfIssuance": "Unknown",
      "IssuingBank": "Unknown",
      "ProductId": "Unknown",
      "ExpirationDate": "/",
      "MaskedNumber": "******"
    },
    "CurrencyIsoCode": "GBP",
    "Customer": {
      "Id": "1469308703",
      "FirstName": null,
      "LastName": null,
      "Company": null,
      "Email": null,
      "Phone": null,
      "Fax": null,
      "Website": null,
      "CreatedAt": null,
      "UpdatedAt": null,
      "CreditCards": [],
      "PayPalAccounts": [],
      "ApplePayCards": [],
      "AndroidPayCards": [],
      "AmexExpressCheckoutCards": [],
      "CoinbaseAccounts": [],
      "VenmoAccounts": [],
      "UsBankAccounts": [],
      "PaymentMethods": [],
      "Addresses": [],
      "CustomFields": null,
      "DefaultPaymentMethod": null
    },
    "CvvResponseCode": "I",
    "Descriptor": {
      "Name": null,
      "Phone": null,
      "Url": null
    },
    "Discounts": [],
    "Disputes": [],
    "GatewayRejectionReason": {},
    "MerchantAccountId": "GBP",
    "OrderId": "VMUK-AUT005-PPL-1898506",
    "PlanId": null,
    "ProcessorAuthorizationCode": null,
    "ProcessorResponseCode": "2069",
    "ProcessorResponseText": "PayPal Blocking Duplicate Order IDs",
    "ProcessorSettlementResponseCode": null,
    "ProcessorSettlementResponseText": null,
    "AdditionalProcessorResponse": "2069 : ",
    "VoiceReferralNumber": null,
    "PurchaseOrderNumber": null,
    "Recurring": false,
    "RefundedTransactionId": null,
    "RefundId": null,
    "RefundIds": [],
    "PartialSettlementTransactionIds": [],
    "AuthorizedTransactionId": null,
    "SettlementBatchId": null,
    "ShippingAddress": {
      "Id": null,
      "CustomerId": null,
      "FirstName": null,
      "LastName": null,
      "Company": null,
      "StreetAddress": null,
      "ExtendedAddress": null,
      "Locality": null,
      "Region": null,
      "PostalCode": null,
      "CountryCodeAlpha2": null,
      "CountryCodeAlpha3": null,
      "CountryCodeNumeric": null,
      "CountryName": null,
      "CreatedAt": null,
      "UpdatedAt": null
    },
    "EscrowStatus": {},
    "Status": {},
    "StatusHistory": [
      {
        "Amount": 5.00,
        "Status": {},
        "Timestamp": "2017-12-13T10:15:24Z",
        "Source": {},
        "User": null
      }
    ],
    "SubscriptionId": null,
    "Subscription": {
      "Balance": null,
      "AddOns": [],
      "BillingDayOfMonth": null,
      "BillingPeriodEndDate": null,
      "BillingPeriodStartDate": null,
      "CurrentBillingCycle": null,
      "DaysPastDue": null,
      "Descriptor": {
        "Name": null,
        "Phone": null,
        "Url": null
      },
      "Discounts": [],
      "FailureCount": null,
      "FirstBillingDate": null,
      "CreatedAt": null,
      "UpdatedAt": null,
      "HasTrialPeriod": null,
      "Id": null,
      "NeverExpires": null,
      "NextBillAmount": null,
      "NextBillingDate": null,
      "NextBillingPeriodAmount": null,
      "NumberOfBillingCycles": null,
      "PaidThroughDate": null,
      "PaymentMethodToken": null,
      "PlanId": null,
      "Price": null,
      "StatusHistory": [],
      "Status": {},
      "Transactions": [],
      "TrialDuration": null,
      "TrialDurationUnit": null,
      "MerchantAccountId": null
    },
    "TaxAmount": null,
    "TaxExempt": false,
    "Type": {},
    "UpdatedAt": "2017-12-13T10:15:24Z",
    "CustomFields": {},
    "ServiceFeeAmount": null,
    "DisbursementDetails": {
      "SettlementAmount": null,
      "SettlementCurrencyIsoCode": null,
      "SettlementCurrencyExchangeRate": null,
      "FundsHeld": null,
      "Success": null,
      "DisbursementDate": null
    },
    "ApplePayDetails": null,
    "AndroidPayDetails": null,
    "AmexExpressCheckoutDetails": null,
    "PayPalDetails": {
      "PayerEmail": "finance@mundio.com",
      "PaymentId": null,
      "AuthorizationId": null,
      "Token": "bnpqsw6",
      "ImageUrl": "https://assets.braintreegateway.com/payment_method_logo/paypal.png?environment=production",
      "DebugId": "f048fb4e8d9a1, f048fb4e8d9a1",
      "PayeeEmail": null,
      "CustomField": null,
      "PayerId": null,
      "PayerFirstName": null,
      "PayerLastName": null,
      "SellerProtectionStatus": null,
      "CaptureId": null,
      "RefundId": null,
      "TransactionFeeAmount": null,
      "TransactionFeeCurrencyIsoCode": null,
      "Description": null
    },
    "CoinbaseDetails": null,
    "VenmoAccountDetails": null,
    "UsBankAccountDetails": null,
    "PaymentInstrumentType": {},
    "RiskData": null,
    "ThreeDSecureInfo": null
  },
  "Subscription": null,
  "Errors": {
    "Count": 0,
    "DeepCount": 0
  },
  "Parameters": {
    "transaction[amount]": "5.00",
    "transaction[order_id]": "VMUK-AUT005-PPL-1898506",
    "transaction[payment_method_token]": "bnpqsw6",
    "transaction[type]": "sale",
    "transaction[options][submit_for_settlement]": "true"
  },
  "Message": "PayPal Blocking Duplicate Order IDs",
  "Target": null
}
2017-12-13 15:45:25.4075 ERROR Object reference not set to an instance of an object.
