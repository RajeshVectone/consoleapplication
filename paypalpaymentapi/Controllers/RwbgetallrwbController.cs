using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace apnsproviderapi.Controllers
{
    public class RwbgetallrwbController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class RwbgetallrwbInput
        {
			public int type  { get; set; }
			public int Usertype  { get; set; }  	                   	    
        }
        public class RwbgetallrwbOutput
        {
			public int id  { get; set; }
			public string tadig  { get; set; }
			public string networkcode  { get; set; }
			public int countrycode  { get; set; }
			public string nationaldestination_code  { get; set; }
			public string vlr  { get; set; }
			public string country  { get; set; }
			public string range_start  { get; set; }
			public string range_end  { get; set; }
			public string kpn_voice  { get; set; }
			public string kpn_data  { get; set; }
			public string kpn_camel  { get; set; }
			public string kpn_lte4g  { get; set; }
			public string operatorname  { get; set; }
			public string remarks_status  { get; set; }
			public string baoc_reject_fullopen  { get; set; }
			public string requestbyuser  { get; set; }
			public DateTime? updatedate  { get; set; }
			public string approvedbyuser  { get; set; }
			public int errcode  { get; set; }
			public string errmsg  { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, RwbgetallrwbInput req)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Roaming_wb"].ConnectionString))
                {
                    conn.Open();
                    var sp ="Rwb_get_allrwb";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
							@type=req.type,
							@Usertype=req.Usertype 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    List<RwbgetallrwbOutput> OutputList = new List<RwbgetallrwbOutput>();
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new RwbgetallrwbOutput()
                        {
						id=r.id==null?0:r.id,
						tadig=r.tadig,
						networkcode=r.networkcode,
						countrycode=r.countrycode==null?0:r.countrycode,
						nationaldestination_code=r.nationaldestination_code,
						vlr=r.vlr,
						country=r.country,
						range_start=r.range_start,
						range_end=r.range_end,
						kpn_voice=r.kpn_voice,
						kpn_data=r.kpn_data,
						kpn_camel=r.kpn_camel,
						kpn_lte4g=r.kpn_lte4g,
						operatorname=r.operatorname,
						remarks_status=r.remarks_status,
						baoc_reject_fullopen=r.baoc_reject_fullopen,
						requestbyuser=r.requestbyuser,
						updatedate=r.updatedate==null?null:r.updatedate,
						approvedbyuser=r.approvedbyuser,
						errcode=r.errcode==null?0:r.errcode,
						errmsg=r.errmsg==null?"Success":r.errmsg
                        }));
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                    else
                    {
                        RwbgetallrwbOutput outputobj = new RwbgetallrwbOutput();
						outputobj.errcode=-1;
						outputobj.errmsg="No Rec found";                     
                        OutputList.Add(outputobj);
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
}
