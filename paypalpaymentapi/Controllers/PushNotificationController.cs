﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;
using PushSharp.Apple;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using NLog;

namespace apnsproviderapi
{
    public class PushNotificationController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        [BasicAuthenticationFilter]
        public HttpResponseMessage Post(PushNotificationInput input)
        {
            Log.Info("Push Notification Controller");
            PushNotificationOutput output = new PushNotificationOutput();
            if (input == null)
            {
                output.errcode = -1;
                output.errmsg = "input details not found";
            }
            else
            {
                try
                {
                    Log.Info("sender : " + input.sender + " & receiver : " + input.receiver + " & message : " + input.message);
                    string ToNumber = GetMobileNo(input.receiver);
                    //TODO : Temp
                    ToNumber = "919092239025";
                    if (ToNumber != "")
                    {
                        Log.Info("To mobile no : " + ToNumber);
                        //Device Token
                        String deviceToken = GetDeviceToken(ToNumber, input.isvoip);
                        if (deviceToken == "")
                        {
                            output.errcode = -1;
                            output.errmsg = "device not found";
                            Log.Info("device not found");
                        }
                        else
                        {
                            Log.Info("deviceToken : " + deviceToken);
                            String message = input.message.Trim().Length == 0 ? string.Format(Helper.push_message, input.sender) : input.message;
                            Log.Info("message : " + message);

                            string Name = GetName(input.sender);
                            Log.Info("Name : " + Name);
                            string FromNumber = GetMobileNo(input.sender);
                            Log.Info("MobileNo : " + FromNumber);

                            string p12File = "";
                            string p12FilePwd = "";
                            if (input.isvoip)
                            {
                                p12File = Helper.voip_key_file;
                                p12FilePwd = Helper.voip_key_pwd;
                            }
                            else
                            {
                                p12File = Helper.message_key_file;
                                p12FilePwd = Helper.message_key_pwd;
                            }

                            ApnsConfiguration config = null;
                            if (Helper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com")
                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !input.isvoip);
                            else
                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !input.isvoip);

                            // Create a new broker
                            var apnsBroker = new ApnsServiceBroker(config);

                            // Wire up events
                            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                            {
                                aggregateEx.Handle(ex =>
                                {
                                    output.errcode = -1;
                                    // See what kind of exception it was to further diagnose
                                    if (ex is ApnsNotificationException)
                                    {
                                        var notificationException = (ApnsNotificationException)ex;
                                        var apnsNotification = notificationException.Notification;
                                        var statusCode = notificationException.ErrorStatusCode;
                                        output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                        Log.Info(output.errmsg);
                                    }
                                    else
                                    {
                                        output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                        Log.Info(output.errmsg);
                                    }
                                    return true;
                                });
                            };

                            apnsBroker.OnNotificationSucceeded += (notification) =>
                            {
                                output.errcode = 0;
                                output.errmsg = "Notification Sent!";
                                Log.Info(output.errmsg);
                            };

                            apnsBroker.Start();

                            apnsBroker.QueueNotification(new ApnsNotification
                            {
                                DeviceToken = deviceToken,
                                Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + message + "\",\"badge\":\"" + Helper.push_badge + "\",\"name\":\"" + Name + "\",\"mobileno\":\"" + FromNumber + "\",\"sound\":\"" + Helper.push_sound + "\"}}")
                            });
                            apnsBroker.Stop();
                        }
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "mobile no not found";
                        Log.Info(output.errmsg);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        public HttpResponseMessage Get(string origin, string target, bool isvoip, string media)
        {
            Log.Info("Push Notification Controller");
            PushNotificationOutput output = new PushNotificationOutput();
            try
            {
                Log.Info("sender : " + origin + " & receiver : " + target);
                string ToNumber = GetMobileNo(target);
                if (ToNumber != "")
                {
                    Log.Info("To mobile no : " + ToNumber);
                    //Device Token
                    String deviceToken = GetDeviceToken(ToNumber, isvoip);
                    if (deviceToken == "")
                    {
                        output.errcode = -1;
                        output.errmsg = "device not found";
                        Log.Info("device not found");
                    }
                    else
                    {
                        Log.Info("deviceToken : " + deviceToken);

                        string name;
                        if (origin.Length < 10)
                            name = GetName(origin);
                        else
                            name = origin;
                        if (name.Length == 0)
                            name = origin;
                        Log.Info(string.Format("Name : {0}", name));

                        String message = string.Format(Helper.push_message, name);
                        //String message = string.Format(Helper.push_message, origin);

                        Log.Info("message : " + message);

                        string p12File = "";
                        string p12FilePwd = "";
                        if (isvoip)
                        {
                            p12File = Helper.voip_key_file;
                            p12FilePwd = Helper.voip_key_pwd;
                        }
                        else
                        {
                            p12File = Helper.message_key_file;
                            p12FilePwd = Helper.message_key_pwd;
                        }

                        ApnsConfiguration config = null;
                        if (Helper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com")
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !isvoip);
                        else
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !isvoip);

                        // Create a new broker
                        var apnsBroker = new ApnsServiceBroker(config);

                        // Wire up events
                        apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                        {
                            aggregateEx.Handle(ex =>
                            {
                                output.errcode = -1;
                                // See what kind of exception it was to further diagnose
                                if (ex is ApnsNotificationException)
                                {
                                    var notificationException = (ApnsNotificationException)ex;
                                    var apnsNotification = notificationException.Notification;
                                    var statusCode = notificationException.ErrorStatusCode;
                                    output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                    Log.Info(output.errmsg);
                                }
                                else
                                {
                                    output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                    Log.Info(output.errmsg);
                                }
                                return true;
                            });
                        };

                        apnsBroker.OnNotificationSucceeded += (notification) =>
                        {
                            output.errcode = 0;
                            output.errmsg = "Notification Sent!";
                            Log.Info(output.errmsg);
                        };

                        apnsBroker.Start();

                        string jsonString = "{\"aps\":{\"content-available\":\"" + "1" + "\",\"name\":\"" + name + "\",\"media\":\"" + media + "\",\"UUID\":\"" + deviceToken + "\",\"alert\":\"" + message + "\",\"badge\":\"" + Helper.push_badge + "\",\"sound\":\"" + Helper.push_sound + "\"}}";

                        apnsBroker.QueueNotification(new ApnsNotification
                        {
                            DeviceToken = deviceToken,
                            Payload = JObject.Parse(jsonString)
                            //Payload = JObject.Parse("{\"aps\":{\"content-available\":\"" + "1" + "\",\"alert\":\"" + message + "\",\"badge\":\"" + Helper.push_badge + "\",\"sound\":\"" + Helper.push_sound + "\"}}")
                        });
                        apnsBroker.Stop();

                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "mobile no not found";
                    Log.Info(output.errmsg);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        string GetDeviceToken(string deviceId, bool isvoip)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
            {
                conn.Open();
                var sp = "usp_getdevicetoken";
                var result = conn.Query<dynamic>(
                        sp, new
                        {
                            @DeviceID = deviceId
                        },
                        commandType: CommandType.StoredProcedure);
                if (result != null && result.Count() > 0)
                {
                    if (isvoip)
                        return result.ElementAt(0).voip_token;
                    else
                        return result.ElementAt(0).message_token;
                }
            }
            return "";
        }

        private string GetName(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["chillitalk"].ConnectionString))
                {
                    var sp = "ctp_get_account_name_info";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @accountid = accountid
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode != null && result.ElementAt(0).errcode == 0)
                    {
                        custName = result.ElementAt(0).name;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }

        private string GetMobileNo(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["chillitalk"].ConnectionString))
                {
                    var sp = "ctp_get_mobileno";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @accountid = accountid
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        custName = result.ElementAt(0).mobileno;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }
    }
}
