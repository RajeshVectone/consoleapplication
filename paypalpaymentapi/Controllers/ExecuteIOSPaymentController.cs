﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;
using Braintree;
using NLog;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using Newtonsoft.Json;

namespace paypalpaymentapi
{
    public class ExecuteIOSPaymentController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        [BasicAuthenticationFilter]
        public HttpResponseMessage Post(ExecutePaymentInput input)
        {
            Log.Info("Execute IOS Payment Controller");
            ExecutePaymentOutput output = new ExecutePaymentOutput();
            try
            {
                Log.Info("Input : " + JsonConvert.SerializeObject(input, Formatting.Indented));
                if (!String.IsNullOrEmpty(input.email) && input.email.ToUpper().StartsWith("ARJUN.GUPTA"))
                {
                    Log.Debug("Fails Fraud Checks!");
                    return Request.CreateResponse(HttpStatusCode.OK, new ExecutePaymentOutput() { errcode = -1, errmsg = "Fails Fraud Checks!" });
                }
                PaymentConfigInfo paymentConfigInfo = GetPaymentConfigInfo(input.mobileno, input.account_type);
                if (paymentConfigInfo != null && paymentConfigInfo.errcode == 0)
                {
                    Log.Info("PaymentConfigInfo : " + JsonConvert.SerializeObject(paymentConfigInfo, Formatting.Indented));

                    //Log.Info(string.Format("amount : {0}, nonce:{1}, country:{2}, mobileno:{3}, account_type:{4}, email:{5}, currency:{6},autotopup:{7}", input.amount, input.nonce, input.country, input.mobileno, input.account_type, input.email, input.currency,input.autotopup));
                    //string currency = Helper.GetAppSettings(string.Concat("currency_", input.country));
                    //string locale = Helper.GetAppSettings(string.Concat("locale_", input.country));

                    BraintreeGateway gateway = new BraintreeGateway(Helper.CLIENT_ACCESS_TOKEN);

                    //TODO : Need to change accordingly
                    //For Business
                    //string orderId = paymentConfigInfo.brand_type + "-" + input.productcode + "-" + Math.Truncate(input.amount) + "-PPL-" + paymentConfigInfo.orderid;
                    //For App

                    //11-Jul-2019: Moorthy : Modified for Reference
                    string orderId = paymentConfigInfo.brand_type + "-IOS" + input.amount.ToString("000") + "-PPL-" + paymentConfigInfo.orderid;
                    if (String.IsNullOrEmpty(input.devicetype) || input.devicetype.ToUpper().StartsWith("AND"))
                        orderId = paymentConfigInfo.brand_type + "-AND" + input.amount.ToString("000") + "-PPL-" + paymentConfigInfo.orderid;

                    //string orderId = paymentConfigInfo.brand_type + "-IOS-" + Math.Abs(input.amount) + "-PPL-" + paymentConfigInfo.orderid;
                    //if (String.IsNullOrEmpty(input.devicetype) || input.devicetype.ToUpper().StartsWith("AND"))
                    //    orderId = paymentConfigInfo.brand_type + "-AND-" + Math.Abs(input.amount) + "-PPL-" + paymentConfigInfo.orderid;

                    output.order_id = orderId;
                    Log.Info("orderId : " + orderId);

                    var request = new TransactionRequest();

                    string customerId = "";
                    string cardToken = "";

                    if (input.autotopup != null && input.autotopup == true)
                    {
                        var customerRequest = new CustomerRequest
                        {
                            PaymentMethodNonce = input.nonce
                        };

                        Result<Customer> resultCustomer = gateway.Customer.Create(customerRequest);

                        Log.Info("resultCustomer : " + JsonConvert.SerializeObject(resultCustomer, Formatting.Indented));

                        if (resultCustomer.IsSuccess())
                        {
                            Customer customer = resultCustomer.Target;
                            customerId = customer.Id;
                            cardToken = customer.PaymentMethods[0].Token;

                            request = new TransactionRequest
                            {
                                Amount = input.amount,
                                PaymentMethodToken = cardToken,
                                MerchantAccountId = input.currency,
                                OrderId = orderId,
                                Options = new TransactionOptionsRequest
                                {
                                    SubmitForSettlement = true,
                                }
                            };
                            InsertAutoRenew(input.mobileno, cardToken, DateTime.Now.ToString("yyyyMMdd"), orderId, customerId, input.currency);
                        }
                        else
                        {
                            output.errcode = -1;
                            output.errmsg = resultCustomer != null ? resultCustomer.Message : "Failure";
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                    else
                    {
                        request = new TransactionRequest
                        {
                            Amount = input.amount,
                            PaymentMethodNonce = input.nonce,
                            MerchantAccountId = input.currency,
                            OrderId = orderId, //input.country + "-WEB-" + Math.Truncate(input.amount) + "-PAYPAL-",
                            Options = new TransactionOptionsRequest
                            {
                                SubmitForSettlement = true,
                            }
                        };
                    }
                    Log.Info("request : " + JsonConvert.SerializeObject(request, Formatting.Indented));

                    string message = string.Empty;
                    Result<Transaction> result = gateway.Transaction.Sale(request);
                    Log.Info("response : " + JsonConvert.SerializeObject(result, Formatting.Indented));
                    if (result.IsSuccess())
                    {
                        output.errcode = 0;
                        message = output.errmsg = "Transaction Succeeded.";
                        Transaction transaction = result.Target;
                        output.transaction_id = transaction.Id;

                        Log.Info("transaction : " + JsonConvert.SerializeObject(transaction, Formatting.Indented));
                    }
                    else if (result.Transaction != null)
                    {
                        output.errcode = -1;
                        message = output.errmsg = "Transaction Failed.";
                        Transaction transaction = result.Transaction;
                        output.transaction_id = transaction.Id;

                        Log.Info("transaction : " + JsonConvert.SerializeObject(transaction, Formatting.Indented));
                    }
                    else
                    {
                        string errorMessages = "";
                        foreach (ValidationError error in result.Errors.DeepAll())
                        {
                            errorMessages += "Error: " + (int)error.Code + " - " + error.Message + "\n";
                        }
                        output.errcode = -1;
                        output.errmsg = errorMessages;
                        if (errorMessages.Length > 150)
                            message = errorMessages.Substring(0, 149);
                        else
                            message = errorMessages;
                    }
                    InsertPayment(paymentConfigInfo, input, output.transaction_id, orderId, output.errcode == 0 ? 100 : -1, message);
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = string.Format("Payment Config Info not found for  mobileno:{0} and account_type:{1}", input.mobileno, input.account_type);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error :" + ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
                Log.Error(ex.Message);
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(output, Formatting.None));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        #region InsertAutoRenew
        //Used to insert auto renew details
        void InsertAutoRenew(string account_id, string token, string purchas_date, string trans_reference, string customer_id, string currency)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payment_server"].ConnectionString))
                {
                    conn.Open();
                    var sp = "paypal_insert_subscribtion_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @account_id = account_id,
                                @token = token,
                                @purchas_date = @purchas_date,
                                @trans_reference = trans_reference,
                                @customer_id = customer_id,
                                @currency = currency
                            },
                            commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
        #endregion

        #region InsertPayment
        //Used to insert payment log
        void InsertPayment(PaymentConfigInfo paymentConfigInfo, ExecutePaymentInput executePayment, string transaction_Id, string orderId, int status, string message = "")
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payment_server"].ConnectionString))
                {
                    conn.Open();
                    var sp = "paypal_insert_payment_transaction_detail";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sitecode = paymentConfigInfo.sitecode,
                                @productcode = paymentConfigInfo.brand_type + "-" + Math.Truncate(executePayment.amount),
                                @paymentagent = paymentConfigInfo.payment_agent,
                                @ref_id = orderId,//paymentConfigInfo.brand_type + "-WEB-" + Math.Truncate(executePayment.amount) + "-PAYPAL-" + paymentConfigInfo.orderid,
                                @servicetype = executePayment.account_type == "1" ? "MVNO" : "CHILLITALK",
                                @paymentmode = paymentConfigInfo.payment_mode,
                                @paymentstep = 7,
                                @paymentstatus = 3,
                                @accountid = executePayment.mobileno,
                                @last6digitccno = executePayment.last6digitccno,
                                @totalamount = executePayment.amount,
                                @currency = executePayment.currency,
                                @merchantid = transaction_Id,
                                @providercode = "PP",
                                @csreasoncode = status,
                                @generalerrorcode = status,
                                @message = message,
                                @ip_payment_agent = executePayment.ip_payment_agent,
                                @ip_payment_client = executePayment.ip_payment_client,
                                @email = executePayment.email,
                                @paypal_transid = transaction_Id,
                                @paypal_email_id = Helper.BRAINTREE_ACCOUNT
                            },
                            commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
        #endregion

        #region GetPaymentConfigInfo
        //Used to get the Payment Config Info
        PaymentConfigInfo GetPaymentConfigInfo(string mobileno, string account_type)
        {
            PaymentConfigInfo output = new PaymentConfigInfo();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["crm_server"].ConnectionString))
                {
                    conn.Open();
                    var sp = "paypal_get_payment_config_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @mobileno = mobileno,
                                @account_type = account_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        output.errcode = 0;
                        output.errmsg = "Success";
                        output.sitecode = result.ElementAt(0).sitecode;
                        output.payment_agent = result.ElementAt(0).PAYMENT_AGENT;
                        output.payment_mode = result.ElementAt(0).PAYMENT_MODE;
                        output.brand_type = result.ElementAt(0).brand_type;
                        output.brand = result.ElementAt(0).brand;
                        output.currency = result.ElementAt(0).currency;
                        output.orderid = result.ElementAt(0).orderid;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = string.Format("Payment config info not found for {0}   {1}", mobileno, account_type);
                        Log.Error(string.Format("Payment config info not found for {0}   {1}", mobileno, account_type));
                    }
                }
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
                Log.Error(ex.Message);
            }
            return output;
        }
        #endregion
    }
}
