﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;
using Braintree;
using NLog;

namespace paypalpaymentapi
{
    public class ClientTokenController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        [BasicAuthenticationFilter]
        public HttpResponseMessage Get(string country = "GB")
        {
            Log.Info("Client Token Controller");
            ClientTokenOutput output = new ClientTokenOutput();
            try
            {
                BraintreeGateway gateway = new BraintreeGateway(Helper.CLIENT_ACCESS_TOKEN);
                var clientToken = gateway.ClientToken.generate();
                output.errcode = 0;
                output.errmsg = "Success";
                output.client_access_token = clientToken;
                Log.Info(clientToken);
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
                Log.Error(ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
