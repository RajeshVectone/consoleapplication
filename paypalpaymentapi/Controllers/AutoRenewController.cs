﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;
using Braintree;
using NLog;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using Newtonsoft.Json;

namespace paypalpaymentapi.Controllers
{
    public class AutoRenewController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        [BasicAuthenticationFilter]
        public HttpResponseMessage Post(AutoRenewInput input)
        {
            Log.Info("AutoRenew Controller");
            AutoRenewOutput output = new AutoRenewOutput();
            if (input != null)
            {
                try
                {
                    Log.Info("Input : " + JsonConvert.SerializeObject(input, Formatting.Indented));

                    BraintreeGateway gateway = new BraintreeGateway(Helper.CLIENT_ACCESS_TOKEN);

                    //string orderId = paymentConfigInfo.brand_type + "-" + input.productCode + "-" + Math.Truncate(input.amount) + "-PPL-" + paymentConfigInfo.orderid;

                    string orderId = input.referencecode;
                    Log.Info("orderId : " + orderId);

                    var request = new TransactionRequest();
                    if (String.IsNullOrEmpty(input.currency))
                    {
                        request = new TransactionRequest()
                        {
                            Amount = Convert.ToDecimal(input.amount),
                            PaymentMethodToken = input.subscription_id,
                            //MerchantAccountId = input.currency,
                            OrderId = orderId,
                            Options = new TransactionOptionsRequest
                            {
                                SubmitForSettlement = true,
                            }
                        };
                    }
                    else
                    {
                        request = new TransactionRequest()
                        {
                            Amount = Convert.ToDecimal(input.amount),
                            PaymentMethodToken = input.subscription_id,
                            MerchantAccountId = input.currency,
                            OrderId = orderId,
                            Options = new TransactionOptionsRequest
                            {
                                SubmitForSettlement = true,
                            }
                        };
                    }

                    string message = string.Empty;
                    Result<Transaction> result = gateway.Transaction.Sale(request);
                    Log.Info("response : " + JsonConvert.SerializeObject(result, Formatting.Indented));
                    if (result.IsSuccess())
                    {
                        output.errcode = 0;
                        message = output.errmsg = "Transaction Succeeded.";
                        Transaction transaction = result.Target;
                        output.transaction_id = transaction.Id;
                        output.order_id = orderId;

                        Log.Info("transaction : " + JsonConvert.SerializeObject(transaction, Formatting.Indented));
                    }
                    else if (result.Transaction != null)
                    {
                        output.errcode = -1;
                        message = output.errmsg = result.Message == null ? "Failure" : result.Message;
                        Transaction transaction = result.Transaction;
                        output.transaction_id = transaction.Id;

                        Log.Info("transaction : " + JsonConvert.SerializeObject(transaction, Formatting.Indented));
                    }
                    else
                    {
                        Log.Info("result : " + JsonConvert.SerializeObject(result, Formatting.Indented));

                        string errorMessages = "";
                        foreach (ValidationError error in result.Errors.DeepAll())
                        {
                            errorMessages += "Error: " + (int)error.Code + " - " + error.Message + "\n";
                        }
                        output.errcode = -1;
                        output.errmsg = errorMessages;
                        if (errorMessages.Length > 150)
                            message = errorMessages.Substring(0, 149);
                        else
                            message = errorMessages;
                    }
                    InsertPayment(input, output.transaction_id, orderId, output.errcode == 0 ? 100 : -1, message);
                }
                catch (Exception ex)
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                    Log.Error(ex.Message);
                }
            }
            else
            {
                output.errcode = -1;
                output.errmsg = "input details not found";
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(output, Formatting.None));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        #region InsertPayment
        //Used to insert payment log
        void InsertPayment(AutoRenewInput input, string transaction_Id, string orderId, int status, string message = "")
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payment_server"].ConnectionString))
                {
                    conn.Open();
                    var sp = "paypal_insert_payment_transaction_detail";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sitecode = input.sitecode,
                                @productcode = input.product_code,
                                @paymentagent = input.payment_agent,
                                @ref_id = orderId,
                                @servicetype = input.account_type == "1" ? "MVNO" : "CHILLITALK",
                                @paymentmode = input.payment_mode,
                                @paymentstep = 7,
                                @paymentstatus = 3,
                                @accountid = input.accountid,
                                @last6digitccno = input.last6digitccno,
                                @totalamount = input.amount,
                                @currency = input.currency,
                                @merchantid = transaction_Id,
                                @providercode = "PP",
                                @csreasoncode = status,
                                @generalerrorcode = status,
                                @message = message,
                                @ip_payment_agent = "",
                                @ip_payment_client = "",
                                @email = input.email,
                                @paypal_transid = transaction_Id,
                                @paypal_email_id = Helper.BRAINTREE_ACCOUNT
                            },
                            commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
        #endregion
    }
}