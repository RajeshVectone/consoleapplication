﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using System.Data.SqlTypes;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using NLog;

namespace apnsproviderapi.Controllers
{
    public class TokenController : ApiController
    {

        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [Authorize]
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request, TokenInput req)
        {
            TokenOutput output = new TokenOutput();
            string AccessToken = req.app_id + DateTime.Now.ToString("hh/mm") + "Access";
            string RefershToken = req.app_id + DateTime.Now.ToString("HH/mm") + "Refresh";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["auth_server"].ConnectionString))
                {
                    conn.Open();

                    //Getting Project Key from authkey
                    string projectID = string.Empty;
                    projectID = Convert.ToString(req.app_id);

                    //Getting Project Key from authkey
                    int firauthkey = Convert.ToInt16(projectID.Substring(0, 1));
                    if (firauthkey == 1)
                    {
                        projectID = projectID.Substring(4);
                    }
                    else if (firauthkey == 2)
                    {
                        projectID = projectID.Substring(5);
                    }
                    req.app_id = Convert.ToInt32(projectID);
                    req.access_token = aes_Convert(Base64Encode(AccessToken));
                    req.refresh_token = aes_Convert(Base64Encode(RefershToken));
                    req.access_minute = Convert.ToInt32(ConfigurationManager.AppSettings["AccessTokenMinutes"]);
                    req.refresh_minute = Convert.ToInt32(ConfigurationManager.AppSettings["RefreshTokenMinutes"]);

                    var sp = "Usp_API_Insert_Token_Master";

                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Proj_ID = req.app_id,
                                @AToken_ID = req.access_token,
                                @AToken_Min = req.access_minute,
                                @RToken_ID = req.refresh_token,
                                @RToken_Min = req.refresh_minute
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        output.errcode = 0;
                        output.errmsg = "Success";
                        output.access_token = Base64Encode(AccessToken);
                        output.refresh_token = Base64Encode(RefershToken);
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "No records found";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        #region To encrypt from Base64
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        #endregion

        #region aes convertion
        private string aes_Convert(string aes_Convert)
        {
            string original = aes_Convert;

            // Create a new instance of the Aes
            // class.  This generates a new key and initialization 
            // vector (IV).
            //string roundtrip = "";
            string encryptStr = string.Empty;

            using (Aes myAes = Aes.Create())
            {
                byte[] encrypted;

                byte[] bkey = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
                byte[] biv = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

                // Encrypt the string to an array of bytes.
                //encrypted = encryptStringToBytes_AES(original, myAes.Key, myAes.IV);
                encrypted = encryptStringToBytes_AES(original, bkey, biv);

                // Decrypt the bytes to a string.
                //roundtrip = decryptStringFromBytes_AES(encrypted, myAes.Key, myAes.IV);

                StringBuilder print = new StringBuilder();


                for (int i = 0; i < encrypted.Length; i++)
                {
                    print.Append(encrypted[i].ToString());
                }


                //Store encrypted string
                //StringBuilder to string
                encryptStr = print.ToString();
            }

            return encryptStr;
        }
        #endregion

        #region AES Encryption
        static byte[] encryptStringToBytes_AES(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the streams used
            // to encrypt to an in memory
            // array of bytes.
            MemoryStream msEncrypt = null;
            CryptoStream csEncrypt = null;
            StreamWriter swEncrypt = null;

            // Declare the Aes object
            // used to encrypt the data.
            Aes aesAlg = null;

            // Declare the bytes used to hold the
            // encrypted data.
            //byte[] encrypted = null;

            try
            {
                // Create an Aes object
                // with the specified key and IV.
                aesAlg = Aes.Create();
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                msEncrypt = new MemoryStream();
                csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                swEncrypt = new StreamWriter(csEncrypt);

                //Write all data to the stream.
                swEncrypt.Write(plainText);

            }
            finally
            {
                // Clean things up.

                // Close the streams.
                if (swEncrypt != null)
                    swEncrypt.Close();
                //if (csEncrypt != null)
                //    csEncrypt.Close();
                //if (msEncrypt != null)
                //    msEncrypt.Close();

                // Clear the Aes object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return msEncrypt.ToArray();

        }
        #endregion
     
    }
}
