﻿using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Text;
using Braintree;
using NLog;

namespace paypalpaymentapi
{
    public class ClientAccessTokenController : ApiController
    {
        #region Declarations
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        [BasicAuthenticationFilter]
        public HttpResponseMessage Get(string country = "GB")
        {
            Log.Info("Client Access Token Controller");
            ClientAccessTokenOutput output = new ClientAccessTokenOutput();
            try
            {
                output.errcode = 0;
                output.errmsg = "Success";
                output.client_access_token = Helper.CLIENT_ACCESS_TOKEN;
                Log.Info(Helper.CLIENT_ACCESS_TOKEN);
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
                Log.Error(ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
