﻿using System.Security.Principal;

namespace paypalpaymentapi
{
    public interface IProvidePrincipal
    {
        IPrincipal ReturnTokenStatus(string projectName, string tokenId);

        IPrincipal CreatePrincipal(string username, string password, string projectId);
    }
}