﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Globalization;
using System.Collections;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using System.Security.Cryptography;

namespace apnsproviderapi
{
    public static class ConversionExtensions
    {
        public static byte[] ToByteArray(this string value)
        {
            return Encoding.Default.GetBytes(value);
        }
    }
}
