﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace TPGServiceConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("TPGServiceConsole");
            Console.WriteLine("==============================");
            Console.WriteLine("From Number : ");
            String From = Console.ReadLine();
            Console.WriteLine("To Number : ");
            String To = Console.ReadLine();
            var Tonumber = new List<String>();
            Tonumber.Add(To);
            Console.WriteLine("Input Text : ");
            String Text = Console.ReadLine();

            APITKRequest apitkReq = new APITKRequest();
            apitkReq.ClientIp = "82.113.66.66";
            apitkReq.DCS = 5;
            apitkReq.DeliveryReport = TPGServiceType.DeliveryReport.YES;
            apitkReq.ExtReference = "";
            apitkReq.MessageText = Text;
            apitkReq.MsisdnList = Tonumber;
            apitkReq.PID = 5;
            apitkReq.ReplyNumber = From;
            apitkReq.Sent = DateTime.Now;
            apitkReq.UDHI = 5;
            apitkReq.ValidityPeriod = 5;
            


            List<ATKResponse> aTKResponses;
            object obj;
            string steps = "starting the process";
            string logRequest = string.Empty;
            string logResponse = string.Empty;
            List<ATKResponse> result = new List<ATKResponse>();


            //string contentType = (apitkReq.MsisdnList.Count == 1 ? "text/x-sgml" : "multipart/form-data; boundary=F117A");
            string contentType = ("text/x-sgml"); //To number count is 1

            foreach (byte[] tpgRequestByte in apitkReq.TextData(ConfigurationSettings.AppSettings["TPGKey"].ToString(), ref logRequest))
            {
                HttpWebRequest tpgWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationSettings.AppSettings["TPGConnectionURL"].ToString());
                if (ConfigurationSettings.AppSettings["Certificate"].ToString() != "")
                {
                    ServicePointManager.CertificatePolicy = new CertificatePolicy();
                    X509Certificate certificate = new X509Certificate(ConfigurationSettings.AppSettings["TPGConnectionURL"].ToString());
                    tpgWebRequest.ClientCertificates.Add(certificate);
                }

                ASCIIEncoding enc = new ASCIIEncoding();
                byte[] data = enc.GetBytes(Text);
                tpgWebRequest.ContentLength = (long)((int)data.Length);
                //steps = "setting the request method & parameters";
                tpgWebRequest.Method = "POST";
                tpgWebRequest.UserAgent = "TPGService";
                tpgWebRequest.Accept = "text/x-sgml, text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2";
                tpgWebRequest.ContentType = contentType;
                Stream contentStream = tpgWebRequest.GetRequestStream();
                contentStream.Write(data, 0, (int)data.Length);
                contentStream.Close();

                HttpWebResponse tpgWebResponse = (HttpWebResponse)tpgWebRequest.GetResponse();
                Stream resStream = tpgWebResponse.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(resStream, encode);
                string _sresponsestring = readStream.ReadToEnd().Trim();
                Console.WriteLine("Responce from TGP : {0}", _sresponsestring);
                Console.WriteLine("==============================");
                Console.ReadLine();
            }
        }
    }


}
