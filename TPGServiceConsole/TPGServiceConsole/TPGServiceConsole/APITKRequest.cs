﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TPGServiceConsole
{
    public class APITKRequest
    {
        private static int _intCounter;

        public string ClientIp
        {
            get;
            set;
        }

        public int DCS
        {
            get;
            set;
        }

        public TPGServiceConsole.TPGServiceType.DeliveryReport DeliveryReport
        {
            get;
            set;
        }

        public string ExtReference
        {
            get;
            set;
        }

        public string MessageText
        {
            get;
            set;
        }

        public List<string> MsisdnList
        {
            get;
            set;
        }

        public int PID
        {
            get;
            set;
        }

        public string ReplyNumber
        {
            get;
            set;
        }

        public DateTime Sent
        {
            get;
            set;
        }

        public int UDHI
        {
            get;
            set;
        }

        public int ValidityPeriod
        {
            get;
            set;
        }

        static APITKRequest()
        {
        }

        public APITKRequest()
        {
            this.MsisdnList = new List<string>();
        }

        public byte[] BinaryData(ref string log)
        {
            byte[] controlDataValue;
            string simpleTemplate = (new StringBuilder()).AppendLine("SMS-Request={0}").ToString();
            string logTemplate = (new StringBuilder()).AppendLine("SMS-Request=").AppendLine("{0}").ToString();
            FileStream tpgRequest = new FileStream(ConfigurationSettings.AppSettings["APITKRequest_Binary_xml"].ToString(), FileMode.Open);
            StreamReader tpgRequestReader = new StreamReader(tpgRequest);
            string tpgRequestString = tpgRequestReader.ReadToEnd().Trim();
            tpgRequestReader.Close();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(tpgRequestString);
            xmlDoc.SelectSingleNode("descendant::Ext_Reference").InnerText = this.ExtReference;
            try
            {
                string hexString = this.MessageText;
                uint num = uint.Parse(hexString, System.Globalization.NumberStyles.AllowHexSpecifier);
                controlDataValue = BitConverter.GetBytes(num);
                //controlDataValue = Utility.HexStringToByteArray(this.MessageText);
            }
            catch (Exception exception)
            {
                controlDataValue = Encoding.UTF8.GetBytes(this.MessageText);
            }
            if (this.UDHI != 1)
            {
                xmlDoc.SelectSingleNode("descendant::SMS_Message").RemoveChild(xmlDoc.SelectSingleNode("descendant::UDH"));
            }
            else
            {
                List<byte> tempData = new List<byte>(controlDataValue);
                int udhL = tempData[0];
                byte[] udhValue = new byte[udhL];
                tempData.CopyTo(1, udhValue, 0, udhL);
                tempData.RemoveRange(0, udhL + 1);
                controlDataValue = tempData.ToArray();
                xmlDoc.SelectSingleNode("descendant::UDH").InnerText = Convert.ToBase64String(udhValue);
            }
            xmlDoc.SelectSingleNode("descendant::Control_Data").InnerText = Convert.ToBase64String(controlDataValue);
            xmlDoc.SelectSingleNode("descendant::MSISDN").InnerText = this.MsisdnList[0];
            xmlDoc.SelectSingleNode("descendant::Validity_Period").InnerText = this.ValidityPeriod.ToString();
            xmlDoc.SelectSingleNode("descendant::Reply_Number").InnerText = this.ReplyNumber;
            string pidHex = Convert.ToString(this.PID, 16).ToUpper();
            string protocolValue = string.Empty;
            string str = pidHex;
            string str1 = str;
            if (str != null)
            {
                if (str1 == "7D")
                {
                    protocolValue = SMSOption.MEDataDownload;
                }
                else if (str1 == "7E")
                {
                    protocolValue = SMSOption.MEDSME;
                }
                else if (str1 == "7F")
                {
                    protocolValue = SMSOption.SIMDataDownload;
                }
                else if (str1 == "5F")
                {
                    protocolValue = SMSOption.ReturnCall;
                }
            }
            if (protocolValue.Equals(string.Empty))
            {
                xmlDoc.SelectSingleNode("descendant::SMS_Options").RemoveChild(xmlDoc.SelectSingleNode("descendant::Protocol"));
            }
            else
            {
                XmlElement protocolElement = (XmlElement)xmlDoc.SelectSingleNode("descendant::Protocol");
                protocolElement.SetAttribute("Type", protocolValue);
                protocolElement.InnerText = "null";
            }
            string dcsBin = Convert.ToString(this.DCS, 2).PadLeft(8, '0');
            string dcsCompressed = string.Empty;
            string dcsAlphabet = string.Empty;
            string dcsMessageClass = string.Empty;
            string dcsCodingGroup = dcsBin.Substring(0, 4);
            string str2 = dcsCodingGroup;
            string str3 = str2;
            if (str2 != null)
            {
                if (str3 == "0000" || str3 == "0010" || str3 == "0001" || str3 == "0011")
                {
                    dcsCompressed = (dcsBin[2] == '0' ? "N" : "Y");
                    if (dcsBin[3] == '1')
                    {
                        string str4 = dcsBin.Substring(6, 2);
                        string str5 = str4;
                        if (str4 != null)
                        {
                            if (str5 == "00")
                            {
                                dcsMessageClass = SMSOption.Class0;
                            }
                            else if (str5 == "01")
                            {
                                dcsMessageClass = SMSOption.MeSpecific;
                            }
                            else if (str5 == "10")
                            {
                                dcsMessageClass = SMSOption.SIMSpecific;
                            }
                            else if (str5 == "11")
                            {
                                dcsMessageClass = SMSOption.TESpecific;
                            }
                        }
                        string str6 = dcsBin.Substring(4, 2);
                        string str7 = str6;
                        if (str6 != null)
                        {
                            if (str7 == "00")
                            {
                                dcsAlphabet = SMSOption.DEF;
                            }
                            else if (str7 == "01")
                            {
                                dcsAlphabet = SMSOption.bit;
                            }
                            else if (str7 == "10")
                            {
                                dcsAlphabet = SMSOption.UCS2;
                            }
                        }
                    }
                }
                else
                {
                    if (str3 != "1111")
                    {
                        throw new Exception("Coding group not supported!");
                    }
                    dcsAlphabet = (dcsBin[5] == '0' ? SMSOption.DEF : SMSOption.bit);
                    string str8 = dcsBin.Substring(6, 2);
                    string str9 = str8;
                    if (str8 != null)
                    {
                        if (str9 == "00")
                        {
                            dcsMessageClass = SMSOption.Class0;
                        }
                        else if (str9 == "01")
                        {
                            dcsMessageClass = SMSOption.MeSpecific;
                        }
                        else if (str9 == "10")
                        {
                            dcsMessageClass = SMSOption.SIMSpecific;
                        }
                        else if (str9 == "11")
                        {
                            dcsMessageClass = SMSOption.TESpecific;
                        }
                    }
                }
                if (dcsBin.Equals("00000000"))
                {
                    dcsCompressed = string.Empty;
                    dcsAlphabet = SMSOption.DEF;
                    dcsMessageClass = SMSOption.Class0;
                }
                string str10 = dcsCompressed;
                if (str10 == null || !(str10 == ""))
                {
                    XmlElement gdcElement = (XmlElement)xmlDoc.SelectSingleNode("descendant::GDC");
                    gdcElement.SetAttribute("Alphabet", dcsAlphabet);
                    gdcElement.SetAttribute("Compressed", dcsCompressed);
                    gdcElement.SetAttribute("MessageClass", dcsMessageClass);
                    gdcElement.InnerText = "null";
                    XmlAttribute[] gdcAttributes = new XmlAttribute[gdcElement.Attributes.Count];
                    gdcElement.Attributes.CopyTo(gdcAttributes, 0);
                    XmlAttribute[] xmlAttributeArray = gdcAttributes;
                    for (int i = 0; i < (int)xmlAttributeArray.Length; i++)
                    {
                        XmlAttribute attr = xmlAttributeArray[i];
                        if (attr.Value.Equals(string.Empty))
                        {
                            gdcElement.RemoveAttribute(attr.Name);
                        }
                    }
                }
                else
                {
                    XmlElement dataCodingElement = xmlDoc.CreateElement("DataCoding");
                    dataCodingElement.SetAttribute("Alphabet", dcsAlphabet);
                    dataCodingElement.SetAttribute("MessageClass", dcsMessageClass);
                    int num = Convert.ToInt32(dcsCodingGroup, 2);
                    dataCodingElement.SetAttribute("CodingGroup", num.ToString());
                    dataCodingElement.InnerText = "null";
                    XmlAttribute[] dataCodingAttributes = new XmlAttribute[dataCodingElement.Attributes.Count];
                    dataCodingElement.Attributes.CopyTo(dataCodingAttributes, 0);
                    XmlAttribute[] xmlAttributeArray1 = dataCodingAttributes;
                    for (int j = 0; j < (int)xmlAttributeArray1.Length; j++)
                    {
                        XmlAttribute attr = xmlAttributeArray1[j];
                        if (attr.Value.Equals(string.Empty))
                        {
                            dataCodingElement.RemoveAttribute(attr.Name);
                        }
                    }
                    xmlDoc.SelectSingleNode("descendant::DCS").ReplaceChild(dataCodingElement, xmlDoc.SelectSingleNode("descendant::GDC"));
                }
                xmlDoc.SelectSingleNode("descendant::Delivery_Report").InnerText = this.DeliveryReport.ToString().Substring(0, 1);
                //string xmlResult = Utility.XmlDocumentToString(xmlDoc).Replace("null", "").Replace("<?xml version=\"1.0\"?>", (new StringBuilder()).AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").Append("<!DOCTYPE APITK_Request SYSTEM \"http://www.t-mobile.co.uk/dtd/tpg_request_v1.0.dtd\">").ToString()).Trim();
                string xmlResult = xmlDoc.InnerXml.Replace("null", "").Replace("<?xml version=\"1.0\"?>", (new StringBuilder()).AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").Append("<!DOCTYPE APITK_Request SYSTEM \"http://www.t-mobile.co.uk/dtd/tpg_request_v1.0.dtd\">").ToString()).Trim();
                //string contentBody = string.Format(simpleTemplate, HttpUtility.UrlEncode(xmlResult)).Trim();
                string contentBody = string.Format(simpleTemplate, Uri.EscapeUriString(xmlResult)).Trim();
                //string contentBody = string.Format(simpleTemplate, Uri.EscapeDataString(xmlResult)).Trim();

                log = string.Concat(log, string.Format(logTemplate, xmlResult).Trim());
                return Encoding.UTF8.GetBytes(contentBody);
            }
            throw new Exception("Coding group not supported!");
        }

        public APITKRequest Copy()
        {
            APITKRequest result = new APITKRequest()
            {
                ExtReference = this.ExtReference,
                ClientIp = this.ClientIp,
                MessageText = this.MessageText,
                MsisdnList = new List<string>()
            };
            foreach (string msisdn in this.MsisdnList)
            {
                result.MsisdnList.Add(msisdn);
            }
            result.ValidityPeriod = this.ValidityPeriod;
            result.DeliveryReport = this.DeliveryReport;
            result.ReplyNumber = this.ReplyNumber;
            result.DCS = this.DCS;
            result.Sent = this.Sent;
            return result;
        }

        public static string GenerateRef(string key)
        {
            DateTime now = DateTime.Now;
            TimeSpan dateTime = now - new DateTime(2000, 1, 1);
            int days = (int)dateTime.TotalDays;
            int secs = (int)(now - DateTime.Today).TotalSeconds;
            int num = APITKRequest._intCounter;
            APITKRequest._intCounter = num + 1;
            int counter = num % 1000;
            return string.Concat(key, days.ToString("0000"), secs.ToString("00000"), counter.ToString("000"));
        }

        public List<byte[]> TextData(string keyReference, ref string log)
        {
            int maxLength = 153;
            List<byte[]> result = new List<byte[]>();
            string simpleTemplate = (new StringBuilder()).AppendLine("SMS-Request={0}").ToString();
            string bulkTemplate = (new StringBuilder()).AppendLine("--F117A").AppendLine("Content-Disposition: form-data; name=\"SMS-Request\"").AppendLine().AppendLine("{0}").AppendLine().AppendLine().AppendLine("--F117A").AppendLine("Content-Disposition: form-data; name=\"MSISDN_File\"; filename=\"multipartctns.txt\"").AppendLine("Content-Type: text/plain").AppendLine().AppendLine("{1}").AppendLine().AppendLine().AppendLine("--F117A--").ToString();
            string logSimpleTemplate = (new StringBuilder()).AppendLine("Part {0} of {1}").AppendLine("=============").AppendLine().AppendLine("SMS-Request=").AppendLine("{2}").AppendLine().ToString();
            string logBulkTemplate = (new StringBuilder()).AppendLine("Part {0} of {1}").AppendLine("=============").AppendLine().AppendLine("{2}").AppendLine().ToString();
            FileStream tpgRequest = new FileStream(ConfigurationSettings.AppSettings["APITKRequest_Text_xml"].ToString(), FileMode.Open);
            StreamReader tpgRequestReader = new StreamReader(tpgRequest);
            string tpgRequestString = tpgRequestReader.ReadToEnd().Trim();
            tpgRequestReader.Close();
            int pos = 0;
            while (pos < this.MessageText.Length)
            {
                string msgText = this.MessageText.Substring(pos, (this.MessageText.Length - pos > maxLength ? maxLength : this.MessageText.Length - pos));
                pos += maxLength;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(tpgRequestString);
                xmlDoc.SelectSingleNode("descendant::Ext_Reference").InnerText = APITKRequest.GenerateRef(keyReference);
                //msgText = System.Web.HttpUtility.UrlEncode(msgText).Replace("+", "%2B").Replace("&", "%26").Replace("€", "%E2%82%AC").Replace("£", "%C2%A3").Replace("¥", "%C2%A5");
                xmlDoc.SelectSingleNode("descendant::Message_Text").InnerText = msgText;
                
                Encoding.UTF8.GetBytes(this.MessageText);
                byte[] udhValue = new byte[5];
                int totSms = (int)Math.Ceiling((double)this.MessageText.Length / (double)maxLength);
                int numSms = (int)Math.Ceiling((double)pos / (double)maxLength);
                if (this.MessageText.Length <= maxLength)
                {
                    xmlDoc.SelectSingleNode("descendant::SMS_Message").RemoveChild(xmlDoc.SelectSingleNode("descendant::UDH"));
                }
                else
                {
                    udhValue[0] = Convert.ToByte("00", 16);
                    udhValue[1] = Convert.ToByte("03", 16);
                    udhValue[2] = Convert.ToByte("27", 16);
                    udhValue[3] = Convert.ToByte(totSms);
                    udhValue[4] = Convert.ToByte(numSms);
                    xmlDoc.SelectSingleNode("descendant::UDH").InnerText = Convert.ToBase64String(udhValue);
                }
                string multipartctns = string.Empty;
                if (this.MsisdnList.Count <= 1)
                {
                    xmlDoc.SelectSingleNode("descendant::MSISDN").InnerText = this.MsisdnList[0];
                }
                else
                {
                    StringBuilder msisdnList = new StringBuilder();
                    foreach (string msisdn in this.MsisdnList)
                    {
                        msisdnList.AppendLine(msisdn);
                    }
                    multipartctns = msisdnList.ToString().Trim();
                    XmlElement msisdnFileElement = xmlDoc.CreateElement("MSISDN_File");
                    XmlElement fileNameElement = xmlDoc.CreateElement("File_Name");
                    fileNameElement.InnerText = "multipartctns.txt";
                    msisdnFileElement.AppendChild(fileNameElement);
                    xmlDoc.SelectSingleNode("descendant::Recipient").ReplaceChild(msisdnFileElement, xmlDoc.SelectSingleNode("descendant::MSISDN_List"));
                }
                xmlDoc.SelectSingleNode("descendant::Validity_Period").InnerText = this.ValidityPeriod.ToString();
                xmlDoc.SelectSingleNode("descendant::Reply_Number").InnerText = this.ReplyNumber;
                xmlDoc.SelectSingleNode("descendant::Delivery_Report").InnerText = this.DeliveryReport.ToString().Substring(0, 1);
                //string xmlResult = Utility.XmlDocumentToString(xmlDoc).Replace("null", "").Replace("<?xml version=\"1.0\"?>", (new StringBuilder()).AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").Append("<!DOCTYPE APITK_Request SYSTEM \"http://www.t-mobile.co.uk/dtd/tpg_request_v1.0.dtd\">").ToString()).Trim();
                string xmlResult = xmlDoc.InnerXml.Replace("null", "").Replace("<?xml version=\"1.0\"?>", (new StringBuilder()).AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").Append("<!DOCTYPE APITK_Request SYSTEM \"http://www.t-mobile.co.uk/dtd/tpg_request_v1.0.dtd\">").ToString()).Trim();
                string contentBody = string.Empty;
                if (this.MsisdnList.Count != 1)
                {
                    contentBody = string.Format(bulkTemplate, xmlResult, multipartctns).Trim();
                    log = string.Concat(log, string.Format(logBulkTemplate, (int)Math.Ceiling((double)pos / (double)maxLength), (int)Math.Ceiling((double)this.MessageText.Length / (double)maxLength), contentBody));
                }
                else
                {
                    //contentBody = string.Format(simpleTemplate, HttpUtility.UrlEncode(xmlResult)).Trim();
                    contentBody = string.Format(simpleTemplate, Uri.EscapeUriString(xmlResult)).Trim();
                    //contentBody = string.Format(simpleTemplate, Uri.EscapeDataString(xmlResult)).Trim();

                    log = string.Concat(log, string.Format(logSimpleTemplate, (int)Math.Ceiling((double)pos / (double)maxLength), (int)Math.Ceiling((double)this.MessageText.Length / (double)maxLength), xmlResult));
                }
                result.Add(Encoding.UTF8.GetBytes(contentBody));
            }
            log = log.Trim();
            return result;
        }
    }
}
