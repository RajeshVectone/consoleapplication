﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace TPGServiceConsole
{
    public class ATKResponse
    {
        public string ExtReference
        {
            get;
            set;
        }

        public APITKRequest Request
        {
            get;
            set;
        }

        public string ResultCode
        {
            get;
            set;
        }

        public string ResultMessage
        {
            get;
            set;
        }

        public string SmsResponse
        {
            get;
            set;
        }

        public string TransactionId
        {
            get;
            set;
        }

        public ATKResponse()
        {
        }

        public ATKResponse(HttpWebResponse tpgWebResponse, ref string log)
        {
            string innerText;
            string str;
            string innerText1;
            StreamReader tpgResponseReader = new StreamReader(tpgWebResponse.GetResponseStream());
            string tpgResponseString = tpgResponseReader.ReadToEnd().Trim();
            tpgResponseReader.Close();
            Regex evalXml = new Regex("(<\\!DOCTYPE[^<]+?>)|(<\\?xml[^<]+?>)");
            if (evalXml.IsMatch(tpgResponseString))
            {
                tpgResponseString = evalXml.Replace(tpgResponseString, "").TrimStart(new char[0]);
            }
            XmlDocument tpgResponsexml = new XmlDocument();
            tpgResponsexml.LoadXml(tpgResponseString);
            string deepMessage = string.Empty;
            for (XmlNode node = tpgResponsexml.FirstChild; node != null; node = node.FirstChild)
            {
                deepMessage = node.InnerText;
            }
            this.ResultCode = (tpgResponsexml.DocumentElement["Result_Code"] == null ? "-1" : tpgResponsexml.DocumentElement["Result_Code"].InnerText);
            this.ResultMessage = (tpgResponsexml.DocumentElement["Result_Message"] == null ? deepMessage : tpgResponsexml.DocumentElement["Result_Message"].InnerText);
            if (tpgResponsexml.DocumentElement["Ext_Reference"] == null)
            {
                innerText = null;
            }
            else
            {
                innerText = tpgResponsexml.DocumentElement["Ext_Reference"].InnerText;
            }
            this.ExtReference = innerText;
            if (tpgResponsexml.DocumentElement["Transaction_ID"] == null)
            {
                str = null;
            }
            else
            {
                str = tpgResponsexml.DocumentElement["Transaction_ID"].InnerText;
            }
            this.TransactionId = str;
            if (tpgResponsexml.DocumentElement["SMS_Response"] == null)
            {
                innerText1 = null;
            }
            else
            {
                innerText1 = tpgResponsexml.DocumentElement["SMS_Response"].InnerText;
            }
            this.SmsResponse = innerText1;
        }
    }
}
