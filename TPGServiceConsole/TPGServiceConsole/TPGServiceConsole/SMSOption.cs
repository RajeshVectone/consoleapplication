﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPGServiceConsole
{
    public class SMSOption
    {
        public static string bit
        {
            get
            {
                return "8bit";
            }
        }

        public static string Class0
        {
            get
            {
                return "Class0";
            }
        }

        public static string DEF
        {
            get
            {
                return "DEF";
            }
        }

        public static string MEDataDownload
        {
            get
            {
                return "MEDataDownload";
            }
        }

        public static string MEDSME
        {
            get
            {
                return "MEDSME";
            }
        }

        public static string MeSpecific
        {
            get
            {
                return "ME_Specific";
            }
        }

        public static string ReturnCall
        {
            get
            {
                return "ReturnCall";
            }
        }

        public static string SIMDataDownload
        {
            get
            {
                return "SIMDataDownload";
            }
        }

        public static string SIMSpecific
        {
            get
            {
                return "SIM_Specific";
            }
        }

        public static string TESpecific
        {
            get
            {
                return "TE_Specific";
            }
        }

        public static string UCS2
        {
            get
            {
                return "UCS2";
            }
        }

        public SMSOption()
        {
        }
    }
}
