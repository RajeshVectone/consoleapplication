﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BulkSMSConsole;
using System.Text.RegularExpressions;
using System.Threading;

namespace BulkSMSConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Declaration
            string SourcePath = string.Empty;
            string LogPath = string.Empty;
            string[] batFiles;
            string MapClient = string.Empty;
            string[] MapClientArr;
            string reqresult = string.Empty;
            string Source = string.Empty;
            DateTime currenttime = DateTime.Now;
            DateTime Scheduldetime = DateTime.Now;
            //string CampaignId = string.Empty;
            #endregion

            try
            {
                //Console.WriteLine("Start");
                SourcePath = ConfigurationSettings.AppSettings["SourcePath"].ToString();
                LogPath = ConfigurationSettings.AppSettings["LogPath"].ToString();
                batFiles = GetFileNames(SourcePath, "*.txt");

                if (batFiles.Count() > 0)
                {
                    //Console.WriteLine("batFiles strt");
                    for (int iCount = 0; iCount < batFiles.Count(); iCount++)
                    {
                        Source = SourcePath + batFiles[iCount];

                        var webClient = new WebClient();
                        MapClient = webClient.DownloadString(Source);
                        byte[] bytes = Encoding.GetEncoding(1252).GetBytes(MapClient);
                        MapClient = Encoding.UTF8.GetString(bytes);

                        MapClientArr = Regex.Split(MapClient, "\r\n");
                        //CampaignId = batFiles[0].Substring(0, batFiles[0].IndexOf("bat"));
                        //string[] MapClientArr = MapClient.Split(new[] { "http" }, StringSplitOptions.None);

                        if (MapClientArr != null && MapClientArr[0].ToString().StartsWith("Timing"))
                        {
                            Scheduldetime = Convert.ToDateTime(MapClientArr[0].Replace("Timing : ", ""));
                            if (currenttime >= Scheduldetime)
                            {
                                for (int MCCount = 1; MCCount < MapClientArr.Count(); MCCount++)
                                {
                                    if (MapClientArr[MCCount] != null)
                                    {
                                        //Console.WriteLine(MapClientArr[MCCount]);

                                        if (!string.IsNullOrEmpty(MapClientArr[MCCount].ToString().Trim()) && (MapClientArr[MCCount].ToString().Trim() != ""))
                                        {
                                            try
                                            {
                                                reqresult = Mapclient.callMapclient(MapClientArr[MCCount].ToString().Trim());

                                                int index = reqresult.IndexOf(':');
                                                if (index > 0)
                                                    reqresult = reqresult.Substring(0, index);

                                                if (reqresult != "0")
                                                {
                                                    if (!System.IO.Directory.Exists(LogPath + DateTime.Now.ToString("yyyyMMdd")))
                                                    {
                                                        System.IO.Directory.CreateDirectory(LogPath + DateTime.Now.ToString("yyyyMMdd"));
                                                    }
                                                    StreamWriter sw = new StreamWriter(string.Concat(LogPath + DateTime.Now.ToString("yyyyMMdd"), "\\SMS_Failure_Status.txt"), true);
                                                    sw.WriteLine("=================================START==========================================");
                                                    sw.WriteLine(MapClientArr[MCCount].ToString().Trim() + "\r\n");
                                                    sw.WriteLine(reqresult);
                                                    sw.WriteLine("=================================END============================================\r\n");
                                                    sw.Close();
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                if (!System.IO.Directory.Exists(LogPath + DateTime.Now.ToString("yyyyMMdd")))
                                                {
                                                    System.IO.Directory.CreateDirectory(LogPath + DateTime.Now.ToString("yyyyMMdd"));
                                                }
                                                StreamWriter sw = new StreamWriter(string.Concat(LogPath + DateTime.Now.ToString("yyyyMMdd"), "\\SMS_Failure_Status.txt"), true);
                                                sw.WriteLine("=================================START==========================================");
                                                sw.WriteLine(currenttime + ex.Message + "\r\n" + MapClientArr[MCCount].ToString().Trim());
                                                sw.WriteLine("=================================END============================================\r\n");
                                                sw.Close();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    //"Sleep for 2 seconds"
                                    Thread.Sleep(2000);
                                }
                                //Console.WriteLine("MoveFile strt");
                                //To Move finished file
                                FileManage.MoveFile(batFiles[iCount], Source);
                                //Console.WriteLine("MoveFile stop");
                            }



                        }
                    }

                    //if (currenttime > Scheduldetime && CampaignId != string.Empty || CampaignId != "")
                    if (currenttime > Scheduldetime)
                    {
                        //exec SP
                        //batFiles = GetFileNames(SourcePath, CampaignId + "batSP.bat");

                        batFiles = GetFileNames(SourcePath, "*batSP.bat");

                        if (batFiles != null)
                        {
                            for (int batCount = 0; batCount < batFiles.Count(); batCount++)
                            {
                                Source = SourcePath + batFiles[batCount];
                                System.Diagnostics.Process.Start(Source);
                                FileManage.MoveFile(batFiles[batCount], Source);
                            }
                        }
                    }

                }
                //Console.WriteLine("File is null");
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                if (!System.IO.Directory.Exists(LogPath + DateTime.Now.ToString("yyyyMMdd")))
                {
                    System.IO.Directory.CreateDirectory(LogPath + DateTime.Now.ToString("yyyyMMdd"));
                }
                StreamWriter sw = new StreamWriter(string.Concat(LogPath + DateTime.Now.ToString("yyyyMMdd"), "\\SMS_Failure_Status.txt"), true);
                sw.WriteLine("=================================START==========================================");
                sw.WriteLine(currenttime + ex.Message);
                sw.WriteLine("=================================END============================================\r\n");
                sw.Close();
            }
        }

        private static string[] GetFileNames(string path, string filter)
        {
            string[] files = Directory.GetFiles(path, filter);
            for (int i = 0; i < files.Length; i++)
                files[i] = Path.GetFileName(files[i]);
            return files;
        }
    }
}
