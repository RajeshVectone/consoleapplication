﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSMSConsole
{
    class FileManage
    {
        public static string MoveFile(string filename, string source)
        {
            string Dest = ConfigurationSettings.AppSettings["DestinationPath"].ToString() + DateTime.Now.ToString("yyyyMMdd") + "\\";
            bool exists = System.IO.Directory.Exists(Dest);
            if (!exists)
                System.IO.Directory.CreateDirectory(Dest);
            string newfilename = filename + ".txt";
            File.Move(source, Path.Combine(Dest, newfilename));
            return "Success";
        }
    }
}
