﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace DelightCallingTopUp
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region DoPaymonthlySMSSummaryReport
        public static List<DoPaymonthlySMSSummaryReport> DoPaymonthlySMSSummaryReport()
        {
            List<DoPaymonthlySMSSummaryReport> OutputList = new List<DoPaymonthlySMSSummaryReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "tariff_automation_paym_sms_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new DoPaymonthlySMSSummaryReport()
                        {
                            date = r.date,
                            DATE_FORMAT = r.DATE_FORMAT,
                            SITECODE = r.SITECODE,
                            NATIONAL_NO_SMS = r.NATIONAL_NO_SMS,
                            NATIONAL_SMS_CLI = r.NATIONAL_SMS_CLI,
                            INTERNATIONAL_SMS = r.INTERNATIONAL_SMS,
                            INTERNATIONAL_SMS_CLI = r.INTERNATIONAL_SMS_CLI,
                            ONNET_NO_SMS = r.ONNET_NO_SMS,
                            ONNET_SMS_CLI = r.ONNET_SMS_CLI,
                            UNIQUE_CLI = r.UNIQUE_CLI,
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoPaymonthlySMSSummaryReport() : " + ex.Message);
                log.Error("DoPaymonthlySMSSummaryReport() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region DoPaymonthlyMinuteSummaryReport
        public static List<DoPaymonthlyMinuteSummaryReport> DoPaymonthlyMinuteSummaryReport()
        {
            List<DoPaymonthlyMinuteSummaryReport> OutputList = new List<DoPaymonthlyMinuteSummaryReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "tariff_automation_paym_call_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new DoPaymonthlyMinuteSummaryReport()
                        {
                            date = r.date,
                            DATE_FORMAT = r.DATE_FORMAT,
                            SITECODE = r.SITECODE,
                            NATIONAL_MO = r.NATIONAL_MO,
                            NATIONAL_MO_CLI = r.NATIONAL_MO_CLI,
                            INTER_NATIONAL_MO = r.INTER_NATIONAL_MO,
                            INTER_NATIONAL_MO_CLI = r.INTER_NATIONAL_MO_CLI,
                            ONNET_MO = r.ONNET_MO,
                            ONNET_CLI = r.ONNET_CLI,
                            MT_MIN = r.MT_MIN,
                            MT_CLI = r.MT_CLI,
                            UNIQUE_CLI = r.UNIQUE_CLI
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoPaymonthlyMinuteSummaryReport() : " + ex.Message);
                log.Error("DoPaymonthlyMinuteSummaryReport() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region TariffAutomationSitecodeWiseMOMinSummary
        public static List<TariffAutomationSitecodeWiseMOMinSummary> TariffAutomationSitecodeWiseMOMinSummary()
        {
            List<TariffAutomationSitecodeWiseMOMinSummary> OutputList = new List<TariffAutomationSitecodeWiseMOMinSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_MO"].ConnectionString))
                {
                    conn.Open();
                    var sp = "tariff_automation_sitecode_wise_mo_min_summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationSitecodeWiseMOMinSummary()
                        {
                            calldate = r.calldate,
                            sitecode = r.sitecode,
                            Noofcals = r.Noofcals,
                            Total_min = r.Total_min
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationSitecodeWiseMOMinSummary() : " + ex.Message);
                log.Error("TariffAutomationSitecodeWiseMOMinSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion


        #region TariffAutomationSitecodeWiseMTMinSummary
        public static List<TariffAutomationSitecodeWiseMTMinSummary> TariffAutomationSitecodeWiseMTMinSummary()
        {
            List<TariffAutomationSitecodeWiseMTMinSummary> OutputList = new List<TariffAutomationSitecodeWiseMTMinSummary>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection_MT"].ConnectionString))
                {
                    conn.Open();
                    var sp = "tariff_automation_sitecode_wise_mT_min_summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationSitecodeWiseMTMinSummary()
                        {
                            calldate = r.calldate,
                            sitecode = r.sitecode,
                            noofcalls = r.noofcalls,
                            incomming_min = r.incomming_min
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationSitecodeWiseMTMinSummary() : " + ex.Message);
                log.Error("TariffAutomationSitecodeWiseMTMinSummary() : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}
