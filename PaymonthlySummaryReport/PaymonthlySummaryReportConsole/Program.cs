﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace DelightCallingTopUp
{
    #region DoPaymonthlySMSSummaryReport
    public class DoPaymonthlySMSSummaryReport
    {
        public string date { get; set; }
        public string DATE_FORMAT { get; set; }
        public string SITECODE { get; set; }
        public decimal? NATIONAL_NO_SMS { get; set; }
        public decimal? NATIONAL_SMS_CLI { get; set; }
        public decimal? INTERNATIONAL_SMS { get; set; }
        public decimal? INTERNATIONAL_SMS_CLI { get; set; }
        public decimal? ONNET_NO_SMS { get; set; }
        public decimal? ONNET_SMS_CLI { get; set; }

        public int? UNIQUE_CLI { get; set; }
    }
    #endregion


    #region DoPaymonthlyMinuteSummaryReport
    public class DoPaymonthlyMinuteSummaryReport
    {
        public string date { get; set; }
        public string DATE_FORMAT { get; set; }
        public string SITECODE { get; set; }
        public decimal? NATIONAL_MO { get; set; }
        public decimal? NATIONAL_MO_CLI { get; set; }
        public decimal? INTER_NATIONAL_MO { get; set; }
        public decimal? INTER_NATIONAL_MO_CLI { get; set; }
        public decimal? ONNET_MO { get; set; }
        public decimal? ONNET_CLI { get; set; }
        public decimal? MT_MIN { get; set; }
        public decimal? MT_CLI { get; set; }

        public int? UNIQUE_CLI { get; set; }
    }
    #endregion

    public class TariffAutomationSitecodeWiseMOMinSummary
    {
        public string calldate { get; set; }
        public string sitecode { get; set; }
        public int? Noofcals { get; set; }
        public decimal? Total_min { get; set; }
    }


    public class TariffAutomationSitecodeWiseMTMinSummary
    {
        public string calldate { get; set; }
        public string sitecode { get; set; }
        public int? noofcalls { get; set; }
        public int? incomming_min { get; set; }
    }


    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                DoPaymonthlySMSSummaryReport();
                DoPaymonthlyMinuteSummaryReport();
                TariffAutomationSitecodeWiseMOMinSummary();
                TariffAutomationSitecodeWiseMTMinSummary();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region DoPaymonthlySMSSummaryReport
        static void DoPaymonthlySMSSummaryReport()
        {
            Console.WriteLine("DoPaymonthlySMSSummaryReport()");
            log.Info("DoPaymonthlySMSSummaryReport()");
            try
            {
                int iRowCount = 0;

                while (iRowCount == 0)
                {
                    var records = DataAccess.DoPaymonthlySMSSummaryReport();
                    if (records != null && records.Count > 0)
                    {
                        iRowCount = records.Count;

                        Console.WriteLine("No of records : " + iRowCount);
                        log.Info("No of records : " + iRowCount);

                        string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile1"]);
                        string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName1"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                        File.Copy(templateName, docName);

                        Console.WriteLine("docName : " + docName);
                        log.Info("docName : " + docName);

                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "SMS Summary").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            var distinctDates = (from ld in records select new { id = ld.DATE_FORMAT }).ToList().Distinct(); //.OrderBy(x => x.id);

                            int index2 = 0;
                            foreach (var itemDates in distinctDates)
                            {
                                if (index2 == 0)
                                {
                                    for (uint i = 1; i <= 41;)
                                    {
                                        InsertValue("C", i, itemDates.id, CellValues.String, wsPart);
                                        i += 10;
                                    }
                                }
                                else if (index2 == 1)
                                {
                                    for (uint i = 1; i <= 41;)
                                    {
                                        InsertValue("G", i, itemDates.id, CellValues.String, wsPart);
                                        i += 10;
                                    }
                                }
                                else if (index2 == 2)
                                {
                                    for (uint i = 1; i <= 41;)
                                    {
                                        InsertValue("K", i, itemDates.id, CellValues.String, wsPart);
                                        i += 10;
                                    }
                                }
                                else if (index2 == 3)
                                {
                                    for (uint i = 1; i <= 41;)
                                    {
                                        InsertValue("O", i, itemDates.id, CellValues.String, wsPart);
                                        i += 10;
                                    }
                                }
                                index2++;
                            }
                            var distinctCurr = (from ld in records select new { id = ld.SITECODE }).ToList().Distinct();

                            foreach (var itemCurr in distinctCurr)
                            {
                                uint iRowIndex = 0;
                                string currency = itemCurr.id.ToUpper();
                                if (currency == "MCM")
                                    iRowIndex = 3;
                                else if (currency == "BAU")
                                    iRowIndex = 13;
                                else if (currency == "MBE")
                                    iRowIndex = 23;
                                else if (currency == "MFR")
                                    iRowIndex = 33;

                                int index3 = 0;
                                var item2 = records.Where(r => r.SITECODE.ToUpper().Trim() == currency).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    foreach (var item in item2)
                                    {
                                        if (index3 == 0)
                                        {
                                            InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTERNATIONAL_SMS)), CellValues.Number, wsPart);
                                            InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTERNATIONAL_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("C", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_NO_SMS)), CellValues.Number, wsPart);
                                            InsertValue("D", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("C", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_NO_SMS)), CellValues.Number, wsPart);
                                            InsertValue("D", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("D", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.UNIQUE_CLI)), CellValues.Number, wsPart);
                                        }
                                        else if (index3 == 1)
                                        {
                                            InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTERNATIONAL_SMS)), CellValues.Number, wsPart);
                                            InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTERNATIONAL_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("G", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_NO_SMS)), CellValues.Number, wsPart);
                                            InsertValue("H", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("G", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_NO_SMS)), CellValues.Number, wsPart);
                                            InsertValue("H", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("H", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.UNIQUE_CLI)), CellValues.Number, wsPart);
                                        }
                                        else if (index3 == 2)
                                        {
                                            InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTERNATIONAL_SMS)), CellValues.Number, wsPart);
                                            InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTERNATIONAL_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("K", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_NO_SMS)), CellValues.Number, wsPart);
                                            InsertValue("L", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("K", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_NO_SMS)), CellValues.Number, wsPart);
                                            InsertValue("L", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("L", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.UNIQUE_CLI)), CellValues.Number, wsPart);
                                        }
                                        else if (index3 == 3)
                                        {
                                            InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTERNATIONAL_SMS)), CellValues.Number, wsPart);
                                            InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTERNATIONAL_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("O", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_NO_SMS)), CellValues.Number, wsPart);
                                            InsertValue("P", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("O", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_NO_SMS)), CellValues.Number, wsPart);
                                            InsertValue("P", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_SMS_CLI)), CellValues.Number, wsPart);

                                            InsertValue("P", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.UNIQUE_CLI)), CellValues.Number, wsPart);
                                        }
                                        index3++;
                                    }
                                }
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            wsPart.Worksheet.Save();
                        }
                        try
                        {
                            //Mail Sending 
                            string mailContent = string.Empty;
                            string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject1"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                            MailAddressCollection mailTo = new MailAddressCollection();
                            var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                            foreach (var item in MailTo)
                            {
                                mailTo.Add(new MailAddress(item));
                            }
                            MailAddressCollection mailCC = new MailAddressCollection();
                            var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                            foreach (var item in MailCc)
                            {
                                mailCC.Add(new MailAddress(item));
                            }
                            log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Mail Sending Error : " + ex.Message);
                            log.Debug("Mail Sending Error : " + ex.Message);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No records found!");
                        log.Debug("No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoPaymonthlySMSSummaryReport : " + ex.Message);
                log.Error("DoPaymonthlySMSSummaryReport : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationSitecodeWiseMOMinSummary
        static void TariffAutomationSitecodeWiseMOMinSummary()
        {
            Console.WriteLine("TariffAutomationSitecodeWiseMOMinSummary()");
            log.Info("TariffAutomationSitecodeWiseMOMinSummary()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationSitecodeWiseMOMinSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile3"]);
                    string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName3"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                    File.Copy(templateName, docName);

                    Console.WriteLine("docName : " + docName);
                    log.Info("docName : " + docName);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "MOMinutesSitecodeSummary").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct(); //.OrderBy(x => x.id);

                        uint rowIndex = 3;
                        uint iStartIndex = 3;

                        Console.WriteLine("Date Count : " + distinctDates.Count());
                        log.Info("Date Count : " + distinctDates.Count());

                        foreach (var itemDates in distinctDates)
                        {
                            //Insert Rows
                            //InsertRow(rowIndex, wsPart);

                            InsertValue("A", rowIndex, itemDates.id, CellValues.String, wsPart);

                            var item2 = records.Where(r => r.sitecode.ToUpper().Trim() == "MCM" && r.calldate == itemDates.id).Select(r => r).ToList();
                            if (item2 != null && item2.Count() > 0)
                            {
                                foreach (var item in item2)
                                {
                                    InsertValue("B", rowIndex, item2.ElementAt(0).Noofcals, CellValues.Number, wsPart);
                                    InsertValue("C", rowIndex, item2.ElementAt(0).Total_min, CellValues.Number, wsPart);
                                }
                                Cell cellFormula = GetCell("B", 35, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(B{0}:B{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("C", 35, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(C{0}:C{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;
                            }
                            item2 = records.Where(r => r.sitecode.ToUpper().Trim() == "BAU" && r.calldate == itemDates.id).Select(r => r).ToList();
                            if (item2 != null && item2.Count() > 0)
                            {
                                foreach (var item in item2)
                                {
                                    InsertValue("D", rowIndex, item2.ElementAt(0).Noofcals, CellValues.Number, wsPart);
                                    InsertValue("E", rowIndex, item2.ElementAt(0).Total_min, CellValues.Number, wsPart);
                                }
                                Cell cellFormula = GetCell("D", 35, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(D{0}:D{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("E", 35, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(E{0}:E{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;
                            }
                            item2 = records.Where(r => r.sitecode.ToUpper().Trim() == "MFR" && r.calldate == itemDates.id).Select(r => r).ToList();
                            if (item2 != null && item2.Count() > 0)
                            {
                                foreach (var item in item2)
                                {
                                    InsertValue("F", rowIndex, item2.ElementAt(0).Noofcals, CellValues.Number, wsPart);
                                    InsertValue("G", rowIndex, item2.ElementAt(0).Total_min, CellValues.Number, wsPart);
                                }
                                Cell cellFormula = GetCell("F", 35, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(F{0}:F{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("G", 35, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(G{0}:G{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;
                            }
                            item2 = records.Where(r => r.sitecode.ToUpper().Trim() == "MBE" && r.calldate == itemDates.id).Select(r => r).ToList();
                            if (item2 != null && item2.Count() > 0)
                            {
                                foreach (var item in item2)
                                {
                                    InsertValue("H", rowIndex, item2.ElementAt(0).Noofcals, CellValues.Number, wsPart);
                                    InsertValue("I", rowIndex, item2.ElementAt(0).Total_min, CellValues.Number, wsPart);
                                }
                                Cell cellFormula = GetCell("H", 35, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(H{0}:H{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("I", 35, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(I{0}:I{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;
                            }
                            rowIndex++;
                        }
                        ////Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        for (int i = 34; i >= rowIndex; i--)
                        {
                            Worksheet worksheet = wsPart.Worksheet;
                            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                            if (sheetData.Elements<Row>().Where(r => r.RowIndex == i).Count() != 0)
                            {
                                Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == i).First();
                                if (row != null)
                                {
                                    row.Hidden = true;
                                }
                            }
                        }

                        wsPart.Worksheet.Save();
                    }

                    try
                    {
                        //Mail Sending 
                        string mailContent = string.Empty;
                        string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject3"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                        MailAddressCollection mailTo = new MailAddressCollection();
                        var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                        foreach (var item in MailTo)
                        {
                            mailTo.Add(new MailAddress(item));
                        }
                        MailAddressCollection mailCC = new MailAddressCollection();
                        var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        foreach (var item in MailCc)
                        {
                            mailCC.Add(new MailAddress(item));
                        }
                        log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Mail Sending Error : " + ex.Message);
                        log.Debug("Mail Sending Error : " + ex.Message);
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationSitecodeWiseMOMinSummary : " + ex.Message);
                log.Error("TariffAutomationSitecodeWiseMOMinSummary : " + ex.Message);
            }
        }
        #endregion

        #region TariffAutomationSitecodeWiseMTMinSummary
        static void TariffAutomationSitecodeWiseMTMinSummary()
        {
            Console.WriteLine("TariffAutomationSitecodeWiseMTMinSummary()");
            log.Info("TariffAutomationSitecodeWiseMTMinSummary()");
            try
            {
                int iRowCount = 0;
                //while (iRowCount == 0)
                //{
                var records = DataAccess.TariffAutomationSitecodeWiseMTMinSummary();
                if (records != null && records.Count > 0)
                {
                    iRowCount = records.Count;
                    Console.WriteLine("No of records : " + iRowCount);
                    log.Info("No of records : " + iRowCount);

                    string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile4"]);
                    string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName4"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                    File.Copy(templateName, docName);

                    Console.WriteLine("docName : " + docName);
                    log.Info("docName : " + docName);

                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                    {
                        WorkbookPart bkPart = spreadSheet.WorkbookPart;
                        Workbook workbook = bkPart.Workbook;
                        Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "MTMinutesSitecodeSummary").FirstOrDefault();
                        WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                        var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct(); //.OrderBy(x => x.id);

                        uint rowIndex = 3;
                        uint iStartIndex = 3;

                        int currMonth = DateTime.Now.Month;

                        Console.WriteLine("Date Count : " + distinctDates.Count());
                        log.Info("Date Count : " + distinctDates.Count());

                        for (int index = 0; index < distinctDates.Count(); index++)
                        {
                            if (Convert.ToInt32(distinctDates.ElementAt(index).id.Substring(5, 2)) != currMonth)
                            {
                                iStartIndex += (uint)index + 1;
                                break;
                            }
                        }

                        foreach (var itemDates in distinctDates)
                        {
                            //Insert Rows
                            //InsertRow(rowIndex, wsPart);

                            InsertValue("A", rowIndex, itemDates.id, CellValues.String, wsPart);

                            var item2 = records.Where(r => r.sitecode.ToUpper().Trim() == "MCM" && r.calldate == itemDates.id).Select(r => r).ToList();
                            if (item2 != null && item2.Count() > 0)
                            {
                                foreach (var item in item2)
                                {
                                    InsertValue("B", rowIndex, item2.ElementAt(0).noofcalls, CellValues.Number, wsPart);
                                    InsertValue("C", rowIndex, item2.ElementAt(0).incomming_min, CellValues.Number, wsPart);
                                }
                                Cell cellFormula = GetCell("B", 35, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(B{0}:B{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("C", 35, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(C{0}:C{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;


                            }
                            item2 = records.Where(r => r.sitecode.ToUpper().Trim() == "BAU" && r.calldate == itemDates.id).Select(r => r).ToList();
                            if (item2 != null && item2.Count() > 0)
                            {
                                foreach (var item in item2)
                                {
                                    InsertValue("D", rowIndex, item2.ElementAt(0).noofcalls, CellValues.Number, wsPart);
                                    InsertValue("E", rowIndex, item2.ElementAt(0).incomming_min, CellValues.Number, wsPart);
                                }
                                Cell cellFormula = GetCell("D", 35, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(D{0}:D{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("E", 35, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(E{0}:E{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                            }
                            item2 = records.Where(r => r.sitecode.ToUpper().Trim() == "MFR" && r.calldate == itemDates.id).Select(r => r).ToList();
                            if (item2 != null && item2.Count() > 0)
                            {
                                foreach (var item in item2)
                                {
                                    InsertValue("F", rowIndex, item2.ElementAt(0).noofcalls, CellValues.Number, wsPart);
                                    InsertValue("G", rowIndex, item2.ElementAt(0).incomming_min, CellValues.Number, wsPart);
                                }
                                Cell cellFormula = GetCell("F", 35, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(F{0}:F{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("G", 35, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(G{0}:G{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                            }
                            item2 = records.Where(r => r.sitecode.ToUpper().Trim() == "MBE" && r.calldate == itemDates.id).Select(r => r).ToList();
                            if (item2 != null && item2.Count() > 0)
                            {
                                foreach (var item in item2)
                                {
                                    InsertValue("H", rowIndex, item2.ElementAt(0).noofcalls, CellValues.Number, wsPart);
                                    InsertValue("I", rowIndex, item2.ElementAt(0).incomming_min, CellValues.Number, wsPart);
                                }
                                Cell cellFormula = GetCell("H", 35, wsPart);
                                CellFormula cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(H{0}:H{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                                cellFormula = GetCell("I", 35, wsPart);
                                cellformula = new CellFormula();
                                cellformula.Text = string.Format("SUM(I{0}:I{1})", iStartIndex, rowIndex);
                                cellFormula.CellFormula = cellformula;

                            }
                            rowIndex++;
                        }


                        Cell cellFormula2 = GetCell("B", 38, wsPart);
                        CellFormula cellformula2 = new CellFormula();
                        cellformula2.Text = string.Format("=B{0}-B{1}", rowIndex - 1, rowIndex - 8);
                        //cellformula.Text = string.Format("=B{0}-B{1}", rowIndex, iStartIndex + 1);
                        cellFormula2.CellFormula = cellformula2;

                        cellFormula2 = GetCell("C", 38, wsPart);
                        cellformula2 = new CellFormula();
                        cellformula2.Text = string.Format("=C{0}-C{1}", rowIndex - 1, rowIndex - 8);
                        //cellformula.Text = string.Format("=C{0}-C{1}", rowIndex, iStartIndex + 1);
                        cellFormula2.CellFormula = cellformula2;

                        cellFormula2 = GetCell("D", 38, wsPart);
                        cellformula2 = new CellFormula();
                        cellformula2.Text = string.Format("=D{0}-D{1}", rowIndex - 1, rowIndex - 8);
                        //cellformula.Text = string.Format("=D{0}-D{1}", rowIndex, iStartIndex + 1);
                        cellFormula2.CellFormula = cellformula2;

                        cellFormula2 = GetCell("E", 38, wsPart);
                        cellformula2 = new CellFormula();
                        cellformula2.Text = string.Format("=E{0}-E{1}", rowIndex - 1, rowIndex - 8);
                        //cellformula.Text = string.Format("=E{0}-E{1}", rowIndex, iStartIndex + 1);
                        cellFormula2.CellFormula = cellformula2;

                        cellFormula2 = GetCell("F", 38, wsPart);
                        cellformula2 = new CellFormula();
                        cellformula2.Text = string.Format("=F{0}-F{1}", rowIndex - 1, rowIndex - 8);
                        //cellformula.Text = string.Format("=F{0}-F{1}", rowIndex, iStartIndex + 1);
                        cellFormula2.CellFormula = cellformula2;

                        cellFormula2 = GetCell("G", 38, wsPart);
                        cellformula2 = new CellFormula();
                        cellformula2.Text = string.Format("=G{0}-G{1}", rowIndex - 1, rowIndex - 8);
                        //cellformula.Text = string.Format("=G{0}-G{1}", rowIndex, iStartIndex + 1);
                        cellFormula2.CellFormula = cellformula2;

                        cellFormula2 = GetCell("H", 38, wsPart);
                        cellformula2 = new CellFormula();
                        cellformula2.Text = string.Format("=H{0}-H{1}", rowIndex - 1, rowIndex - 8);
                        //cellformula.Text = string.Format("=H{0}-H{1}", rowIndex, iStartIndex + 1);
                        cellFormula2.CellFormula = cellformula2;

                        cellFormula2 = GetCell("I", 38, wsPart);
                        cellformula2 = new CellFormula();
                        cellformula2.Text = string.Format("=I{0}-I{1}", rowIndex - 1, rowIndex - 8);
                        //cellformula.Text = string.Format("=I{0}-I{1}", rowIndex, iStartIndex + 1);
                        cellFormula2.CellFormula = cellformula2;

                        Cell cellFormula1 = GetCell("A", rowIndex - 1, wsPart);
                        Cell cellFormula3 = GetCell("A", rowIndex - 8, wsPart);
                        //Cell cellFormula2 = GetCell("A", iStartIndex + 1, wsPart);
                        string strComparison = cellFormula1.InnerText.Substring(8) + " & " + cellFormula3.InnerText.Substring(8);
                        InsertValue("A", 38, strComparison, CellValues.String, wsPart);
                        ////Used to execute the formula in all the cells
                        foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                        {
                            cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                        }

                        for (int i = 34; i >= rowIndex; i--)
                        {
                            Worksheet worksheet = wsPart.Worksheet;
                            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                            if (sheetData.Elements<Row>().Where(r => r.RowIndex == i).Count() != 0)
                            {
                                Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == i).First();
                                if (row != null)
                                {
                                    row.Hidden = true;
                                }
                            }
                        }

                        wsPart.Worksheet.Save();
                    }

                    try
                    {
                        //Mail Sending 
                        string mailContent = string.Empty;
                        string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject4"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                        MailAddressCollection mailTo = new MailAddressCollection();
                        var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                        foreach (var item in MailTo)
                        {
                            mailTo.Add(new MailAddress(item));
                        }
                        MailAddressCollection mailCC = new MailAddressCollection();
                        var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        foreach (var item in MailCc)
                        {
                            mailCC.Add(new MailAddress(item));
                        }
                        log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Mail Sending Error : " + ex.Message);
                        log.Debug("Mail Sending Error : " + ex.Message);
                    }
                }
                else
                {
                    Console.WriteLine("No records found!");
                    log.Debug("No records found!");
                }
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("TariffAutomationSitecodeWiseMTMinSummary : " + ex.Message);
                log.Error("TariffAutomationSitecodeWiseMTMinSummary : " + ex.Message);
            }
        }
        #endregion

        #region GetCell
        private static Cell GetCell(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
            return null;
        }
        #endregion

        #region InsertRow
        //InsertRow(iRowIndex, wsPart);
        static void InsertRow(uint rowIndex, WorksheetPart wrksheetPart)
        {
            Worksheet worksheet = wrksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row = null;
            if (sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).Count() != 0)
            {
                Row refRow = sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).First();
                //Copy row from refRow and insert it
                row = CopyToLine(refRow, rowIndex, sheetData);
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }
        }

        //Copy an existing row and insert it
        //We don't need to copy styles of a refRow because a CloneNode() or Clone() methods do it for us
        static Row CopyToLine(Row refRow, uint rowIndex, SheetData sheetData)
        {
            uint newRowIndex;
            var newRow = (Row)refRow.CloneNode(true);
            // Loop through all the rows in the worksheet with higher row 
            // index values than the one you just added. For each one,
            // increment the existing row index.
            IEnumerable<Row> rows = sheetData.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);
            foreach (Row row in rows)
            {
                newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value + 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }
                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }

            sheetData.InsertBefore(newRow, refRow);
            return newRow;
        }
        #endregion

        #region DoPaymonthlyMinuteSummaryReport
        static void DoPaymonthlyMinuteSummaryReport()
        {
            Console.WriteLine("DoPaymonthlyMinuteSummaryReport()");
            log.Info("DoPaymonthlyMinuteSummaryReport()");
            try
            {
                int iRowCount = 0;

                while (iRowCount == 0)
                {
                    var records = DataAccess.DoPaymonthlyMinuteSummaryReport();
                    if (records != null && records.Count > 0)
                    {
                        iRowCount = records.Count;

                        Console.WriteLine("No of records : " + iRowCount);
                        log.Info("No of records : " + iRowCount);

                        string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile2"]);
                        string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName2"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";
                        File.Copy(templateName, docName);

                        Console.WriteLine("docName : " + docName);
                        log.Info("docName : " + docName);

                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Minute Summary").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            var distinctDates = (from ld in records select new { id = ld.DATE_FORMAT }).ToList().Distinct(); //.OrderBy(x => x.id);

                            int index2 = 0;
                            foreach (var itemDates in distinctDates)
                            {
                                if (index2 == 0)
                                {
                                    for (uint i = 1; i <= 43;)
                                    {
                                        InsertValue("C", i, itemDates.id, CellValues.String, wsPart);
                                        i += 12;
                                    }
                                }
                                else if (index2 == 1)
                                {
                                    for (uint i = 1; i <= 43;)
                                    {
                                        InsertValue("G", i, itemDates.id, CellValues.String, wsPart);
                                        i += 12;
                                    }
                                }
                                else if (index2 == 2)
                                {
                                    for (uint i = 1; i <= 43;)
                                    {
                                        InsertValue("K", i, itemDates.id, CellValues.String, wsPart);
                                        i += 12;
                                    }
                                }
                                else if (index2 == 3)
                                {
                                    for (uint i = 1; i <= 43;)
                                    {
                                        InsertValue("O", i, itemDates.id, CellValues.String, wsPart);
                                        i += 12;
                                    }
                                }
                                index2++;
                            }
                            var distinctCurr = (from ld in records select new { id = ld.SITECODE }).ToList().Distinct();

                            foreach (var itemCurr in distinctCurr)
                            {
                                uint iRowIndex = 0;
                                string currency = itemCurr.id.ToUpper();
                                if (currency == "MCM")
                                    iRowIndex = 3;
                                else if (currency == "BAU")
                                    iRowIndex = 15;
                                else if (currency == "MBE")
                                    iRowIndex = 27;
                                else if (currency == "MFR")
                                    iRowIndex = 39;

                                int index3 = 0;
                                var item2 = records.Where(r => r.SITECODE.ToUpper().Trim() == currency).Select(r => r).ToList();
                                if (item2 != null && item2.Count() > 0)
                                {
                                    foreach (var item in item2)
                                    {
                                        if (index3 == 0)
                                        {
                                            InsertValue("C", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTER_NATIONAL_MO)), CellValues.Number, wsPart);
                                            InsertValue("D", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTER_NATIONAL_MO_CLI)), CellValues.Number, wsPart);

                                            InsertValue("C", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_MO)), CellValues.Number, wsPart);
                                            InsertValue("D", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_MO_CLI)), CellValues.Number, wsPart);

                                            InsertValue("C", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_MO)), CellValues.Number, wsPart);
                                            InsertValue("D", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_CLI)), CellValues.Number, wsPart);

                                            InsertValue("C", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.MT_MIN)), CellValues.Number, wsPart);
                                            InsertValue("D", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.MT_CLI)), CellValues.Number, wsPart);

                                            InsertValue("D", iRowIndex + 8, Convert.ToString(Convert.ToInt32(item.UNIQUE_CLI)), CellValues.Number, wsPart);
                                        }
                                        else if (index3 == 1)
                                        {
                                            InsertValue("G", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTER_NATIONAL_MO)), CellValues.Number, wsPart);
                                            InsertValue("H", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTER_NATIONAL_MO_CLI)), CellValues.Number, wsPart);

                                            InsertValue("G", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_MO)), CellValues.Number, wsPart);
                                            InsertValue("H", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_MO_CLI)), CellValues.Number, wsPart);

                                            InsertValue("G", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_MO)), CellValues.Number, wsPart);
                                            InsertValue("H", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_CLI)), CellValues.Number, wsPart);

                                            InsertValue("G", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.MT_MIN)), CellValues.Number, wsPart);
                                            InsertValue("H", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.MT_CLI)), CellValues.Number, wsPart);

                                            InsertValue("H", iRowIndex + 8, Convert.ToString(Convert.ToInt32(item.UNIQUE_CLI)), CellValues.Number, wsPart);
                                        }
                                        else if (index3 == 2)
                                        {
                                            InsertValue("K", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTER_NATIONAL_MO)), CellValues.Number, wsPart);
                                            InsertValue("L", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTER_NATIONAL_MO_CLI)), CellValues.Number, wsPart);

                                            InsertValue("K", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_MO)), CellValues.Number, wsPart);
                                            InsertValue("L", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_MO_CLI)), CellValues.Number, wsPart);

                                            InsertValue("K", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_MO)), CellValues.Number, wsPart);
                                            InsertValue("L", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_CLI)), CellValues.Number, wsPart);

                                            InsertValue("K", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.MT_MIN)), CellValues.Number, wsPart);
                                            InsertValue("L", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.MT_CLI)), CellValues.Number, wsPart);

                                            InsertValue("L", iRowIndex + 8, Convert.ToString(Convert.ToInt32(item.UNIQUE_CLI)), CellValues.Number, wsPart);
                                        }
                                        else if (index3 == 3)
                                        {
                                            InsertValue("O", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTER_NATIONAL_MO)), CellValues.Number, wsPart);
                                            InsertValue("P", iRowIndex, Convert.ToString(Convert.ToInt32(item.INTER_NATIONAL_MO_CLI)), CellValues.Number, wsPart);

                                            InsertValue("O", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_MO)), CellValues.Number, wsPart);
                                            InsertValue("P", iRowIndex + 2, Convert.ToString(Convert.ToInt32(item.NATIONAL_MO_CLI)), CellValues.Number, wsPart);

                                            InsertValue("O", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_MO)), CellValues.Number, wsPart);
                                            InsertValue("P", iRowIndex + 4, Convert.ToString(Convert.ToInt32(item.ONNET_CLI)), CellValues.Number, wsPart);

                                            InsertValue("O", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.MT_MIN)), CellValues.Number, wsPart);
                                            InsertValue("P", iRowIndex + 6, Convert.ToString(Convert.ToInt32(item.MT_CLI)), CellValues.Number, wsPart);

                                            InsertValue("P", iRowIndex + 8, Convert.ToString(Convert.ToInt32(item.UNIQUE_CLI)), CellValues.Number, wsPart);
                                        }
                                        index3++;
                                    }
                                }
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            wsPart.Worksheet.Save();
                        }
                        try
                        {
                            //Mail Sending 
                            string mailContent = string.Empty;
                            string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject2"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                            MailAddressCollection mailTo = new MailAddressCollection();
                            var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                            foreach (var item in MailTo)
                            {
                                mailTo.Add(new MailAddress(item));
                            }
                            MailAddressCollection mailCC = new MailAddressCollection();
                            var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                            foreach (var item in MailCc)
                            {
                                mailCC.Add(new MailAddress(item));
                            }
                            log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Mail Sending Error : " + ex.Message);
                            log.Debug("Mail Sending Error : " + ex.Message);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No records found!");
                        log.Debug("No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DoPaymonthlyMinuteSummaryReport : " + ex.Message);
                log.Error("DoPaymonthlyMinuteSummaryReport : " + ex.Message);
            }
        }
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, object value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(Convert.ToString(value));
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
