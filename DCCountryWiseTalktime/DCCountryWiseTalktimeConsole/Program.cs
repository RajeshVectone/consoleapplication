﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Configuration;
using System.IO;

namespace DCCountryWiseTalktime
{
    #region TariffAutomationDCCountryWiseTotalReport
    public class TariffAutomationDCCountryWiseTotalReport
    {
        public string calldate { get; set; }
        public string Country { get; set; }
        public double? Talktime { get; set; }
        public double? LCRCost { get; set; }
    }
    #endregion

    class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        #region Main
        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");
            log.Info("Process Started");
            try
            {
                DoProcess();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Process Completed");
            log.Info("Process Completed");
            //Console.ReadLine();
        }
        #endregion

        #region DoProcess
        static void DoProcess()
        {
            try
            {
                string templateName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["TemplateFile"]);
                string docName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XLSX", ConfigurationManager.AppSettings["FileName"] + DateTime.Now.AddDays(-1).ToString("ddMMMyyyy_HHmm")) + ".xlsx";

                Console.WriteLine("docName : " + docName);
                log.Info("docName : " + docName);

                File.Copy(templateName, docName);

                //Total
                FillTotalRecord(docName);

                //Bundle
                FillBundleRecord(docName);

                //Free
                FillFreeRecord(docName);

                //Standard
                FillStandardRecord(docName);

                SendEmail(docName);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }
        #endregion

        #region FillTotalRecord
        static void FillTotalRecord(string docName)
        {
            Console.WriteLine("FillTotalRecord()");
            log.Info("FillTotalRecord()");
            try
            {
                int iRowCount = 0;
                while (iRowCount == 0)
                {
                    var records = DataAccess.GetTotalRecord();
                    if (records != null && records.Count > 0)
                    {
                        iRowCount = records.Count;
                        Console.WriteLine("No of records : " + iRowCount);
                        log.Info("No of records : " + iRowCount);

                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Total").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct().OrderBy(x => x.id);

                            var distinctCountry = (from ld in records select new { id = ld.Country }).ToList().Distinct().OrderBy(x => x.id);

                            uint colIndex = 1;
                            foreach (var itemDates in distinctDates)
                            {
                                if (colIndex == 1)
                                    InsertValue("B", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 2)
                                    InsertValue("D", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 3)
                                    InsertValue("F", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 4)
                                    InsertValue("H", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 5)
                                    InsertValue("J", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 6)
                                    InsertValue("L", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 7)
                                    InsertValue("N", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 8)
                                    InsertValue("P", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 9)
                                    InsertValue("R", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 10)
                                    InsertValue("T", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 11)
                                    InsertValue("V", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 12)
                                    InsertValue("X", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 13)
                                    InsertValue("Z", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 14)
                                    InsertValue("AB", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 15)
                                    InsertValue("AD", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 16)
                                    InsertValue("AF", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 17)
                                    InsertValue("AH", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 18)
                                    InsertValue("AJ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 19)
                                    InsertValue("AL", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 20)
                                    InsertValue("AN", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 21)
                                    InsertValue("AP", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 22)
                                    InsertValue("AR", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 23)
                                    InsertValue("AT", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 24)
                                    InsertValue("AV", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 25)
                                    InsertValue("AX", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 26)
                                    InsertValue("AZ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 27)
                                    InsertValue("BB", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 28)
                                    InsertValue("BD", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 29)
                                    InsertValue("BF", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 30)
                                    InsertValue("BH", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 31)
                                    InsertValue("BJ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                colIndex++;
                            }

                            
                            colIndex = 3;
                            //Insert Rows
                            uint startIndex = colIndex;
                            foreach (var itemCountry in distinctCountry)
                            {
                                //Insert Rows
                                InsertRow(colIndex, wsPart);
                                InsertValue("A", colIndex, Convert.ToString(itemCountry.id), CellValues.String, wsPart);
                                colIndex++;
                            }
                            //Insert Rows
                            uint endIndex = colIndex;

                            //Appy Formula
                            colIndex = 1;
                            foreach (var itemDates in distinctDates)
                            {
                                if (colIndex == 1)
                                {
                                    Cell cellFormula = GetCell("B", endIndex + 1 , wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(B{0}:B{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("C", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(C{0}:C{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 2)
                                {
                                    Cell cellFormula = GetCell("D", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(D{0}:D{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("E", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(E{0}:E{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 3)
                                {
                                    Cell cellFormula = GetCell("F", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(F{0}:F{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("G", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(G{0}:G{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 4)
                                {
                                    Cell cellFormula = GetCell("H", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(H{0}:H{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("I", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(I{0}:I{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 5)
                                {
                                    Cell cellFormula = GetCell("J", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(J{0}:J{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("K", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(K{0}:K{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 6)
                                {
                                    Cell cellFormula = GetCell("L", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(L{0}:L{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("M", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(M{0}:M{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 7)
                                {
                                    Cell cellFormula = GetCell("N", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(N{0}:N{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("O", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(O{0}:O{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 8)
                                {
                                    Cell cellFormula = GetCell("P", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(P{0}:P{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("Q", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Q{0}:Q{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 9)
                                {
                                    Cell cellFormula = GetCell("R", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(R{0}:R{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("S", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(S{0}:S{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 10)
                                {
                                    Cell cellFormula = GetCell("T", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(T{0}:T{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("U", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(U{0}:U{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 11)
                                {
                                    Cell cellFormula = GetCell("V", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(V{0}:V{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("W", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(W{0}:W{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 12)
                                {
                                    Cell cellFormula = GetCell("X", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(X{0}:X{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("Y", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Y{0}:Y{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 13)
                                {
                                    Cell cellFormula = GetCell("Z", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Z{0}:Z{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AA", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AA{0}:AA{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 14)
                                {
                                    Cell cellFormula = GetCell("AB", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AB{0}:AB{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AC", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AC{0}:AC{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 15)
                                {
                                    Cell cellFormula = GetCell("AD", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AD{0}:AD{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AE", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AE{0}:AE{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 16)
                                {
                                    Cell cellFormula = GetCell("AF", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AF{0}:AF{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AG", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AG{0}:AG{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 17)
                                {
                                    Cell cellFormula = GetCell("AH", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AH{0}:AH{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AI", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AI{0}:AI{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 18)
                                {
                                    Cell cellFormula = GetCell("AJ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AJ{0}:AJ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AK", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AK{0}:AK{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 19)
                                {
                                    Cell cellFormula = GetCell("AL", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AL{0}:AL{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AM", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AM{0}:AM{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 20)
                                {
                                    Cell cellFormula = GetCell("AN", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AN{0}:AN{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AO", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AO{0}:AO{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 21)
                                {
                                    Cell cellFormula = GetCell("AP", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AP{0}:AP{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AQ", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AQ{0}:AQ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 22)
                                {
                                    Cell cellFormula = GetCell("AR", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AR{0}:AR{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AS", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AS{0}:AS{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 23)
                                {
                                    Cell cellFormula = GetCell("AT", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AT{0}:AT{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AU", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AU{0}:AU{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 24)
                                {
                                    Cell cellFormula = GetCell("AV", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AV{0}:AV{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AW", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AW{0}:AW{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 25)
                                {
                                    Cell cellFormula = GetCell("AX", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AX{0}:AX{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AY", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AY{0}:AY{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 26)
                                {
                                    Cell cellFormula = GetCell("AZ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AZ{0}:AZ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BA", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BA{0}:BA{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 27)
                                {
                                    Cell cellFormula = GetCell("BB", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BB{0}:BB{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BC", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BC{0}:BC{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 28)
                                {
                                    Cell cellFormula = GetCell("BD", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BD{0}:BD{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BE", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BE{0}:BE{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 29)
                                {
                                    Cell cellFormula = GetCell("BF", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BF{0}:BF{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BG", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BG{0}:BG{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 30)
                                {
                                    Cell cellFormula = GetCell("BH", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BH{0}:BH{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BI", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BI{0}:BI{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 31)
                                {
                                    Cell cellFormula = GetCell("BJ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BJ{0}:BJ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BK", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BK{0}:BK{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                colIndex++;
                            }

                            uint rowIndex = 3;
                            foreach (var itemCountry in distinctCountry)
                            {
                                colIndex = 1;
                                foreach (var itemDates in distinctDates)
                                {
                                    string Talktime = "0";
                                    string LCRCost = "0";
                                    var item2 = records.Where(r => r.calldate == itemDates.id && r.Country == itemCountry.id).Select(r => r).FirstOrDefault();
                                    if (item2 != null)
                                    {
                                        Talktime = Convert.ToString(Convert.ToDecimal(item2.Talktime));
                                        LCRCost = Convert.ToString(Convert.ToDecimal(item2.LCRCost));
                                    }
                                    if (colIndex == 1)
                                    {
                                        InsertValue("B", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("C", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 2)
                                    {
                                        InsertValue("D", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("E", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 3)
                                    {
                                        InsertValue("F", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("G", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 4)
                                    {
                                        InsertValue("H", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("I", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 5)
                                    {
                                        InsertValue("J", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("K", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 6)
                                    {
                                        InsertValue("L", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("M", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 7)
                                    {
                                        InsertValue("N", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("O", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 8)
                                    {
                                        InsertValue("P", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("Q", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 9)
                                    {
                                        InsertValue("R", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("S", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 10)
                                    {
                                        InsertValue("T", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("U", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 11)
                                    {
                                        InsertValue("V", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("W", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 12)
                                    {
                                        InsertValue("X", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("Y", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 13)
                                    {
                                        InsertValue("Z", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AA", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 14)
                                    {
                                        InsertValue("AB", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AC", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 15)
                                    {
                                        InsertValue("AD", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AE", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 16)
                                    {
                                        InsertValue("AF", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AG", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 17)
                                    {
                                        InsertValue("AH", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AI", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 18)
                                    {
                                        InsertValue("AJ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AK", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 19)
                                    {
                                        InsertValue("AL", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AM", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 20)
                                    {
                                        InsertValue("AN", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AO", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 21)
                                    {
                                        InsertValue("AP", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AQ", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 22)
                                    {
                                        InsertValue("AR", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AS", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 23)
                                    {
                                        InsertValue("AT", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AU", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 24)
                                    {
                                        InsertValue("AV", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AW", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 25)
                                    {
                                        InsertValue("AX", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AY", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 26)
                                    {
                                        InsertValue("AZ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BA", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 27)
                                    {
                                        InsertValue("BB", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BC", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 28)
                                    {
                                        InsertValue("BD", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BE", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 29)
                                    {
                                        InsertValue("BF", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BG", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 30)
                                    {
                                        InsertValue("BH", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BI", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 31)
                                    {
                                        InsertValue("BJ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BK", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    colIndex++;
                                }
                                rowIndex++;
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No records found!");
                        log.Debug("No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillTotalRecord : " + ex.Message);
                log.Error("FillTotalRecord : " + ex.Message);
            }
        }
        #endregion

        #region FillBundleRecord
        static void FillBundleRecord(string docName)
        {
            Console.WriteLine("FillBundleRecord()");
            log.Info("FillBundleRecord()");
            try
            {
                int iRowCount = 0;
                while (iRowCount == 0)
                {
                    var records = DataAccess.GetBundleRecord();
                    if (records != null && records.Count > 0)
                    {
                        iRowCount = records.Count;
                        Console.WriteLine("No of records : " + iRowCount);
                        log.Info("No of records : " + iRowCount);
                        
                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Bundles").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct().OrderBy(x => x.id);

                            var distinctCountry = (from ld in records select new { id = ld.Country }).ToList().Distinct().OrderBy(x => x.id);

                            uint colIndex = 1;
                            foreach (var itemDates in distinctDates)
                            {
                                if (colIndex == 1)
                                    InsertValue("B", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 2)
                                    InsertValue("D", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 3)
                                    InsertValue("F", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 4)
                                    InsertValue("H", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 5)
                                    InsertValue("J", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 6)
                                    InsertValue("L", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 7)
                                    InsertValue("N", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 8)
                                    InsertValue("P", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 9)
                                    InsertValue("R", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 10)
                                    InsertValue("T", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 11)
                                    InsertValue("V", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 12)
                                    InsertValue("X", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 13)
                                    InsertValue("Z", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 14)
                                    InsertValue("AB", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 15)
                                    InsertValue("AD", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 16)
                                    InsertValue("AF", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 17)
                                    InsertValue("AH", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 18)
                                    InsertValue("AJ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 19)
                                    InsertValue("AL", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 20)
                                    InsertValue("AN", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 21)
                                    InsertValue("AP", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 22)
                                    InsertValue("AR", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 23)
                                    InsertValue("AT", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 24)
                                    InsertValue("AV", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 25)
                                    InsertValue("AX", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 26)
                                    InsertValue("AZ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 27)
                                    InsertValue("BB", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 28)
                                    InsertValue("BD", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 29)
                                    InsertValue("BF", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 30)
                                    InsertValue("BH", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 31)
                                    InsertValue("BJ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                colIndex++;
                            }

                            colIndex = 3;
                            //Insert Rows
                            uint startIndex = colIndex;
                            foreach (var itemCountry in distinctCountry)
                            {
                                //Insert Rows
                                InsertRow(colIndex, wsPart);
                                InsertValue("A", colIndex, Convert.ToString(itemCountry.id), CellValues.String, wsPart);
                                colIndex++;
                            }
                            //Insert Rows
                            uint endIndex = colIndex;

                            //Appy Formula
                            colIndex = 1;
                            foreach (var itemDates in distinctDates)
                            {
                                if (colIndex == 1)
                                {
                                    Cell cellFormula = GetCell("B", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(B{0}:B{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("C", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(C{0}:C{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 2)
                                {
                                    Cell cellFormula = GetCell("D", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(D{0}:D{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("E", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(E{0}:E{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 3)
                                {
                                    Cell cellFormula = GetCell("F", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(F{0}:F{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("G", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(G{0}:G{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 4)
                                {
                                    Cell cellFormula = GetCell("H", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(H{0}:H{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("I", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(I{0}:I{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 5)
                                {
                                    Cell cellFormula = GetCell("J", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(J{0}:J{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("K", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(K{0}:K{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 6)
                                {
                                    Cell cellFormula = GetCell("L", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(L{0}:L{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("M", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(M{0}:M{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 7)
                                {
                                    Cell cellFormula = GetCell("N", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(N{0}:N{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("O", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(O{0}:O{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 8)
                                {
                                    Cell cellFormula = GetCell("P", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(P{0}:P{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("Q", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Q{0}:Q{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 9)
                                {
                                    Cell cellFormula = GetCell("R", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(R{0}:R{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("S", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(S{0}:S{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 10)
                                {
                                    Cell cellFormula = GetCell("T", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(T{0}:T{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("U", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(U{0}:U{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 11)
                                {
                                    Cell cellFormula = GetCell("V", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(V{0}:V{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("W", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(W{0}:W{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 12)
                                {
                                    Cell cellFormula = GetCell("X", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(X{0}:X{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("Y", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Y{0}:Y{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 13)
                                {
                                    Cell cellFormula = GetCell("Z", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Z{0}:Z{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AA", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AA{0}:AA{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 14)
                                {
                                    Cell cellFormula = GetCell("AB", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AB{0}:AB{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AC", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AC{0}:AC{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 15)
                                {
                                    Cell cellFormula = GetCell("AD", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AD{0}:AD{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AE", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AE{0}:AE{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 16)
                                {
                                    Cell cellFormula = GetCell("AF", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AF{0}:AF{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AG", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AG{0}:AG{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 17)
                                {
                                    Cell cellFormula = GetCell("AH", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AH{0}:AH{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AI", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AI{0}:AI{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 18)
                                {
                                    Cell cellFormula = GetCell("AJ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AJ{0}:AJ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AK", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AK{0}:AK{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 19)
                                {
                                    Cell cellFormula = GetCell("AL", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AL{0}:AL{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AM", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AM{0}:AM{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 20)
                                {
                                    Cell cellFormula = GetCell("AN", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AN{0}:AN{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AO", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AO{0}:AO{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 21)
                                {
                                    Cell cellFormula = GetCell("AP", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AP{0}:AP{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AQ", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AQ{0}:AQ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 22)
                                {
                                    Cell cellFormula = GetCell("AR", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AR{0}:AR{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AS", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AS{0}:AS{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 23)
                                {
                                    Cell cellFormula = GetCell("AT", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AT{0}:AT{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AU", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AU{0}:AU{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 24)
                                {
                                    Cell cellFormula = GetCell("AV", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AV{0}:AV{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AW", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AW{0}:AW{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 25)
                                {
                                    Cell cellFormula = GetCell("AX", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AX{0}:AX{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AY", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AY{0}:AY{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 26)
                                {
                                    Cell cellFormula = GetCell("AZ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AZ{0}:AZ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BA", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BA{0}:BA{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 27)
                                {
                                    Cell cellFormula = GetCell("BB", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BB{0}:BB{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BC", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BC{0}:BC{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 28)
                                {
                                    Cell cellFormula = GetCell("BD", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BD{0}:BD{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BE", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BE{0}:BE{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 29)
                                {
                                    Cell cellFormula = GetCell("BF", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BF{0}:BF{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BG", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BG{0}:BG{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 30)
                                {
                                    Cell cellFormula = GetCell("BH", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BH{0}:BH{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BI", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BI{0}:BI{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 31)
                                {
                                    Cell cellFormula = GetCell("BJ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BJ{0}:BJ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BK", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BK{0}:BK{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                colIndex++;
                            }

                            uint rowIndex = 3;
                            foreach (var itemCountry in distinctCountry)
                            {
                                colIndex = 1;
                                foreach (var itemDates in distinctDates)
                                {
                                    string Talktime = "0";
                                    string LCRCost = "0";
                                    var item2 = records.Where(r => r.calldate == itemDates.id && r.Country == itemCountry.id).Select(r => r).FirstOrDefault();
                                    if (item2 != null)
                                    {
                                        Talktime = Convert.ToString(Convert.ToDecimal(item2.Talktime));
                                        LCRCost = Convert.ToString(Convert.ToDecimal(item2.LCRCost));
                                    }
                                    if (colIndex == 1)
                                    {
                                        InsertValue("B", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("C", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 2)
                                    {
                                        InsertValue("D", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("E", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 3)
                                    {
                                        InsertValue("F", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("G", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 4)
                                    {
                                        InsertValue("H", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("I", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 5)
                                    {
                                        InsertValue("J", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("K", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 6)
                                    {
                                        InsertValue("L", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("M", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 7)
                                    {
                                        InsertValue("N", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("O", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 8)
                                    {
                                        InsertValue("P", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("Q", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 9)
                                    {
                                        InsertValue("R", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("S", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 10)
                                    {
                                        InsertValue("T", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("U", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 11)
                                    {
                                        InsertValue("V", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("W", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 12)
                                    {
                                        InsertValue("X", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("Y", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 13)
                                    {
                                        InsertValue("Z", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AA", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 14)
                                    {
                                        InsertValue("AB", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AC", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 15)
                                    {
                                        InsertValue("AD", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AE", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 16)
                                    {
                                        InsertValue("AF", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AG", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 17)
                                    {
                                        InsertValue("AH", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AI", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 18)
                                    {
                                        InsertValue("AJ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AK", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 19)
                                    {
                                        InsertValue("AL", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AM", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 20)
                                    {
                                        InsertValue("AN", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AO", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 21)
                                    {
                                        InsertValue("AP", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AQ", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 22)
                                    {
                                        InsertValue("AR", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AS", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 23)
                                    {
                                        InsertValue("AT", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AU", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 24)
                                    {
                                        InsertValue("AV", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AW", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 25)
                                    {
                                        InsertValue("AX", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AY", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 26)
                                    {
                                        InsertValue("AZ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BA", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 27)
                                    {
                                        InsertValue("BB", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BC", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 28)
                                    {
                                        InsertValue("BD", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BE", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 29)
                                    {
                                        InsertValue("BF", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BG", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 30)
                                    {
                                        InsertValue("BH", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BI", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 31)
                                    {
                                        InsertValue("BJ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BK", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    colIndex++;
                                }
                                rowIndex++;
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No records found!");
                        log.Debug("No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillBundleRecord : " + ex.Message);
                log.Error("FillBundleRecord : " + ex.Message);
            }
        }
        #endregion

        #region FillFreeRecord
        static void FillFreeRecord(string docName)
        {
            Console.WriteLine("FillFreeRecord()");
            log.Info("FillFreeRecord()");
            try
            {
                int iRowCount = 0;
                while (iRowCount == 0)
                {
                    var records = DataAccess.GetFreeRecord();
                    if (records != null && records.Count > 0)
                    {
                        iRowCount = records.Count;
                        Console.WriteLine("No of records : " + iRowCount);
                        log.Info("No of records : " + iRowCount);

                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Free").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct().OrderBy(x => x.id);

                            var distinctCountry = (from ld in records select new { id = ld.Country }).ToList().Distinct().OrderBy(x => x.id);

                            uint colIndex = 1;
                            foreach (var itemDates in distinctDates)
                            {
                                if (colIndex == 1)
                                    InsertValue("B", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 2)
                                    InsertValue("D", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 3)
                                    InsertValue("F", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 4)
                                    InsertValue("H", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 5)
                                    InsertValue("J", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 6)
                                    InsertValue("L", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 7)
                                    InsertValue("N", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 8)
                                    InsertValue("P", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 9)
                                    InsertValue("R", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 10)
                                    InsertValue("T", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 11)
                                    InsertValue("V", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 12)
                                    InsertValue("X", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 13)
                                    InsertValue("Z", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 14)
                                    InsertValue("AB", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 15)
                                    InsertValue("AD", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 16)
                                    InsertValue("AF", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 17)
                                    InsertValue("AH", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 18)
                                    InsertValue("AJ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 19)
                                    InsertValue("AL", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 20)
                                    InsertValue("AN", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 21)
                                    InsertValue("AP", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 22)
                                    InsertValue("AR", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 23)
                                    InsertValue("AT", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 24)
                                    InsertValue("AV", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 25)
                                    InsertValue("AX", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 26)
                                    InsertValue("AZ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 27)
                                    InsertValue("BB", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 28)
                                    InsertValue("BD", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 29)
                                    InsertValue("BF", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 30)
                                    InsertValue("BH", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 31)
                                    InsertValue("BJ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                colIndex++;
                            }

                            colIndex = 3;
                            //Insert Rows
                            uint startIndex = colIndex;
                            foreach (var itemCountry in distinctCountry)
                            {
                                //Insert Rows
                                InsertRow(colIndex, wsPart);
                                InsertValue("A", colIndex, Convert.ToString(itemCountry.id), CellValues.String, wsPart);
                                colIndex++;
                            }
                            //Insert Rows
                            uint endIndex = colIndex;

                            //Appy Formula
                            colIndex = 1;
                            foreach (var itemDates in distinctDates)
                            {
                                if (colIndex == 1)
                                {
                                    Cell cellFormula = GetCell("B", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(B{0}:B{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("C", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(C{0}:C{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 2)
                                {
                                    Cell cellFormula = GetCell("D", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(D{0}:D{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("E", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(E{0}:E{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 3)
                                {
                                    Cell cellFormula = GetCell("F", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(F{0}:F{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("G", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(G{0}:G{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 4)
                                {
                                    Cell cellFormula = GetCell("H", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(H{0}:H{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("I", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(I{0}:I{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 5)
                                {
                                    Cell cellFormula = GetCell("J", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(J{0}:J{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("K", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(K{0}:K{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 6)
                                {
                                    Cell cellFormula = GetCell("L", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(L{0}:L{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("M", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(M{0}:M{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 7)
                                {
                                    Cell cellFormula = GetCell("N", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(N{0}:N{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("O", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(O{0}:O{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 8)
                                {
                                    Cell cellFormula = GetCell("P", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(P{0}:P{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("Q", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Q{0}:Q{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 9)
                                {
                                    Cell cellFormula = GetCell("R", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(R{0}:R{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("S", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(S{0}:S{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 10)
                                {
                                    Cell cellFormula = GetCell("T", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(T{0}:T{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("U", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(U{0}:U{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 11)
                                {
                                    Cell cellFormula = GetCell("V", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(V{0}:V{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("W", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(W{0}:W{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 12)
                                {
                                    Cell cellFormula = GetCell("X", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(X{0}:X{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("Y", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Y{0}:Y{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 13)
                                {
                                    Cell cellFormula = GetCell("Z", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Z{0}:Z{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AA", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AA{0}:AA{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 14)
                                {
                                    Cell cellFormula = GetCell("AB", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AB{0}:AB{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AC", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AC{0}:AC{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 15)
                                {
                                    Cell cellFormula = GetCell("AD", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AD{0}:AD{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AE", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AE{0}:AE{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 16)
                                {
                                    Cell cellFormula = GetCell("AF", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AF{0}:AF{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AG", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AG{0}:AG{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 17)
                                {
                                    Cell cellFormula = GetCell("AH", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AH{0}:AH{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AI", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AI{0}:AI{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 18)
                                {
                                    Cell cellFormula = GetCell("AJ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AJ{0}:AJ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AK", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AK{0}:AK{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 19)
                                {
                                    Cell cellFormula = GetCell("AL", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AL{0}:AL{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AM", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AM{0}:AM{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 20)
                                {
                                    Cell cellFormula = GetCell("AN", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AN{0}:AN{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AO", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AO{0}:AO{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 21)
                                {
                                    Cell cellFormula = GetCell("AP", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AP{0}:AP{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AQ", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AQ{0}:AQ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 22)
                                {
                                    Cell cellFormula = GetCell("AR", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AR{0}:AR{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AS", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AS{0}:AS{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 23)
                                {
                                    Cell cellFormula = GetCell("AT", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AT{0}:AT{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AU", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AU{0}:AU{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 24)
                                {
                                    Cell cellFormula = GetCell("AV", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AV{0}:AV{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AW", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AW{0}:AW{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 25)
                                {
                                    Cell cellFormula = GetCell("AX", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AX{0}:AX{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AY", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AY{0}:AY{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 26)
                                {
                                    Cell cellFormula = GetCell("AZ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AZ{0}:AZ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BA", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BA{0}:BA{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 27)
                                {
                                    Cell cellFormula = GetCell("BB", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BB{0}:BB{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BC", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BC{0}:BC{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 28)
                                {
                                    Cell cellFormula = GetCell("BD", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BD{0}:BD{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BE", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BE{0}:BE{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 29)
                                {
                                    Cell cellFormula = GetCell("BF", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BF{0}:BF{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BG", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BG{0}:BG{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 30)
                                {
                                    Cell cellFormula = GetCell("BH", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BH{0}:BH{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BI", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BI{0}:BI{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 31)
                                {
                                    Cell cellFormula = GetCell("BJ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BJ{0}:BJ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BK", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BK{0}:BK{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                colIndex++;
                            }

                            uint rowIndex = 3;
                            foreach (var itemCountry in distinctCountry)
                            {
                                colIndex = 1;
                                foreach (var itemDates in distinctDates)
                                {
                                    string Talktime = "0";
                                    string LCRCost = "0";
                                    var item2 = records.Where(r => r.calldate == itemDates.id && r.Country == itemCountry.id).Select(r => r).FirstOrDefault();
                                    if (item2 != null)
                                    {
                                        Talktime = Convert.ToString(Convert.ToDecimal(item2.Talktime));
                                        LCRCost = Convert.ToString(Convert.ToDecimal(item2.LCRCost));
                                    }
                                    if (colIndex == 1)
                                    {
                                        InsertValue("B", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("C", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 2)
                                    {
                                        InsertValue("D", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("E", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 3)
                                    {
                                        InsertValue("F", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("G", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 4)
                                    {
                                        InsertValue("H", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("I", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 5)
                                    {
                                        InsertValue("J", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("K", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 6)
                                    {
                                        InsertValue("L", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("M", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 7)
                                    {
                                        InsertValue("N", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("O", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 8)
                                    {
                                        InsertValue("P", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("Q", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 9)
                                    {
                                        InsertValue("R", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("S", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 10)
                                    {
                                        InsertValue("T", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("U", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 11)
                                    {
                                        InsertValue("V", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("W", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 12)
                                    {
                                        InsertValue("X", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("Y", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 13)
                                    {
                                        InsertValue("Z", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AA", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 14)
                                    {
                                        InsertValue("AB", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AC", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 15)
                                    {
                                        InsertValue("AD", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AE", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 16)
                                    {
                                        InsertValue("AF", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AG", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 17)
                                    {
                                        InsertValue("AH", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AI", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 18)
                                    {
                                        InsertValue("AJ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AK", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 19)
                                    {
                                        InsertValue("AL", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AM", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 20)
                                    {
                                        InsertValue("AN", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AO", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 21)
                                    {
                                        InsertValue("AP", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AQ", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 22)
                                    {
                                        InsertValue("AR", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AS", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 23)
                                    {
                                        InsertValue("AT", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AU", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 24)
                                    {
                                        InsertValue("AV", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AW", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 25)
                                    {
                                        InsertValue("AX", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AY", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 26)
                                    {
                                        InsertValue("AZ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BA", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 27)
                                    {
                                        InsertValue("BB", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BC", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 28)
                                    {
                                        InsertValue("BD", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BE", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 29)
                                    {
                                        InsertValue("BF", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BG", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 30)
                                    {
                                        InsertValue("BH", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BI", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 31)
                                    {
                                        InsertValue("BJ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BK", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    colIndex++;
                                }
                                rowIndex++;
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No records found!");
                        log.Debug("No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillFreeRecord : " + ex.Message);
                log.Error("FillFreeRecord : " + ex.Message);
            }
        }
        #endregion

        #region FillStandardRecord
        static void FillStandardRecord(string docName)
        {
            Console.WriteLine("FillStandardRecord()");
            log.Info("FillStandardRecord()");
            try
            {
                int iRowCount = 0;
                while (iRowCount == 0)
                {
                    var records = DataAccess.GetStandardRecord();
                    if (records != null && records.Count > 0)
                    {
                        iRowCount = records.Count;
                        Console.WriteLine("No of records : " + iRowCount);
                        log.Info("No of records : " + iRowCount);

                        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                        {
                            WorkbookPart bkPart = spreadSheet.WorkbookPart;
                            Workbook workbook = bkPart.Workbook;
                            Sheet s = workbook.Descendants<Sheet>().Where(sht => sht.Name == "Standard").FirstOrDefault();
                            WorksheetPart wsPart = (WorksheetPart)bkPart.GetPartById(s.Id);

                            var distinctDates = (from ld in records select new { id = ld.calldate }).ToList().Distinct().OrderBy(x => x.id);

                            var distinctCountry = (from ld in records select new { id = ld.Country }).ToList().Distinct().OrderBy(x => x.id);

                            uint colIndex = 1;
                            foreach (var itemDates in distinctDates)
                            {
                                if (colIndex == 1)
                                    InsertValue("B", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 2)
                                    InsertValue("D", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 3)
                                    InsertValue("F", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 4)
                                    InsertValue("H", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 5)
                                    InsertValue("J", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 6)
                                    InsertValue("L", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 7)
                                    InsertValue("N", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 8)
                                    InsertValue("P", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 9)
                                    InsertValue("R", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 10)
                                    InsertValue("T", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 11)
                                    InsertValue("V", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 12)
                                    InsertValue("X", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 13)
                                    InsertValue("Z", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 14)
                                    InsertValue("AB", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 15)
                                    InsertValue("AD", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 16)
                                    InsertValue("AF", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 17)
                                    InsertValue("AH", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 18)
                                    InsertValue("AJ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 19)
                                    InsertValue("AL", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 20)
                                    InsertValue("AN", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 21)
                                    InsertValue("AP", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 22)
                                    InsertValue("AR", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 23)
                                    InsertValue("AT", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 24)
                                    InsertValue("AV", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 25)
                                    InsertValue("AX", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 26)
                                    InsertValue("AZ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 27)
                                    InsertValue("BB", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 28)
                                    InsertValue("BD", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 29)
                                    InsertValue("BF", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 30)
                                    InsertValue("BH", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                else if (colIndex == 31)
                                    InsertValue("BJ", 1, Convert.ToDateTime(itemDates.id).ToString("dd/MM/yyyy"), CellValues.String, wsPart);
                                colIndex++;
                            }

                            colIndex = 3;
                            //Insert Rows
                            uint startIndex = colIndex;
                            foreach (var itemCountry in distinctCountry)
                            {
                                //Insert Rows
                                InsertRow(colIndex, wsPart);
                                InsertValue("A", colIndex, Convert.ToString(itemCountry.id), CellValues.String, wsPart);
                                colIndex++;
                            }
                            //Insert Rows
                            uint endIndex = colIndex;

                            //Appy Formula
                            colIndex = 1;
                            foreach (var itemDates in distinctDates)
                            {
                                if (colIndex == 1)
                                {
                                    Cell cellFormula = GetCell("B", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(B{0}:B{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("C", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(C{0}:C{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 2)
                                {
                                    Cell cellFormula = GetCell("D", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(D{0}:D{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("E", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(E{0}:E{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 3)
                                {
                                    Cell cellFormula = GetCell("F", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(F{0}:F{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("G", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(G{0}:G{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 4)
                                {
                                    Cell cellFormula = GetCell("H", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(H{0}:H{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("I", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(I{0}:I{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 5)
                                {
                                    Cell cellFormula = GetCell("J", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(J{0}:J{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("K", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(K{0}:K{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 6)
                                {
                                    Cell cellFormula = GetCell("L", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(L{0}:L{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("M", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(M{0}:M{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 7)
                                {
                                    Cell cellFormula = GetCell("N", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(N{0}:N{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("O", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(O{0}:O{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 8)
                                {
                                    Cell cellFormula = GetCell("P", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(P{0}:P{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("Q", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Q{0}:Q{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 9)
                                {
                                    Cell cellFormula = GetCell("R", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(R{0}:R{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("S", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(S{0}:S{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 10)
                                {
                                    Cell cellFormula = GetCell("T", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(T{0}:T{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("U", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(U{0}:U{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 11)
                                {
                                    Cell cellFormula = GetCell("V", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(V{0}:V{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("W", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(W{0}:W{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 12)
                                {
                                    Cell cellFormula = GetCell("X", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(X{0}:X{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("Y", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Y{0}:Y{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 13)
                                {
                                    Cell cellFormula = GetCell("Z", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(Z{0}:Z{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AA", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AA{0}:AA{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 14)
                                {
                                    Cell cellFormula = GetCell("AB", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AB{0}:AB{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AC", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AC{0}:AC{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 15)
                                {
                                    Cell cellFormula = GetCell("AD", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AD{0}:AD{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AE", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AE{0}:AE{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 16)
                                {
                                    Cell cellFormula = GetCell("AF", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AF{0}:AF{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AG", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AG{0}:AG{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 17)
                                {
                                    Cell cellFormula = GetCell("AH", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AH{0}:AH{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AI", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AI{0}:AI{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 18)
                                {
                                    Cell cellFormula = GetCell("AJ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AJ{0}:AJ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AK", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AK{0}:AK{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 19)
                                {
                                    Cell cellFormula = GetCell("AL", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AL{0}:AL{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AM", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AM{0}:AM{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 20)
                                {
                                    Cell cellFormula = GetCell("AN", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AN{0}:AN{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AO", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AO{0}:AO{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 21)
                                {
                                    Cell cellFormula = GetCell("AP", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AP{0}:AP{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AQ", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AQ{0}:AQ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 22)
                                {
                                    Cell cellFormula = GetCell("AR", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AR{0}:AR{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AS", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AS{0}:AS{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 23)
                                {
                                    Cell cellFormula = GetCell("AT", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AT{0}:AT{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AU", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AU{0}:AU{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 24)
                                {
                                    Cell cellFormula = GetCell("AV", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AV{0}:AV{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AW", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AW{0}:AW{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 25)
                                {
                                    Cell cellFormula = GetCell("AX", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AX{0}:AX{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("AY", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AY{0}:AY{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 26)
                                {
                                    Cell cellFormula = GetCell("AZ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(AZ{0}:AZ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BA", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BA{0}:BA{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 27)
                                {
                                    Cell cellFormula = GetCell("BB", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BB{0}:BB{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BC", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BC{0}:BC{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 28)
                                {
                                    Cell cellFormula = GetCell("BD", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BD{0}:BD{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BE", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BE{0}:BE{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 29)
                                {
                                    Cell cellFormula = GetCell("BF", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BF{0}:BF{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BG", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BG{0}:BG{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 30)
                                {
                                    Cell cellFormula = GetCell("BH", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BH{0}:BH{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BI", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BI{0}:BI{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                else if (colIndex == 31)
                                {
                                    Cell cellFormula = GetCell("BJ", endIndex + 1, wsPart);
                                    CellFormula cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BJ{0}:BJ{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;

                                    cellFormula = GetCell("BK", endIndex + 1, wsPart);
                                    cellformula = new CellFormula();
                                    cellformula.Text = string.Format("SUM(BK{0}:BK{1})", startIndex, endIndex - 1);
                                    cellFormula.CellFormula = cellformula;
                                }
                                colIndex++;
                            }

                            uint rowIndex = 3;
                            foreach (var itemCountry in distinctCountry)
                            {
                                colIndex = 1;
                                foreach (var itemDates in distinctDates)
                                {
                                    string Talktime = "0";
                                    string LCRCost = "0";
                                    var item2 = records.Where(r => r.calldate == itemDates.id && r.Country == itemCountry.id).Select(r => r).FirstOrDefault();
                                    if (item2 != null)
                                    {
                                        Talktime = Convert.ToString(Convert.ToDecimal(item2.Talktime));
                                        LCRCost = Convert.ToString(Convert.ToDecimal(item2.LCRCost));
                                    }
                                    if (colIndex == 1)
                                    {
                                        InsertValue("B", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("C", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 2)
                                    {
                                        InsertValue("D", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("E", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 3)
                                    {
                                        InsertValue("F", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("G", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 4)
                                    {
                                        InsertValue("H", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("I", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 5)
                                    {
                                        InsertValue("J", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("K", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 6)
                                    {
                                        InsertValue("L", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("M", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 7)
                                    {
                                        InsertValue("N", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("O", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 8)
                                    {
                                        InsertValue("P", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("Q", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 9)
                                    {
                                        InsertValue("R", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("S", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 10)
                                    {
                                        InsertValue("T", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("U", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 11)
                                    {
                                        InsertValue("V", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("W", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 12)
                                    {
                                        InsertValue("X", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("Y", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 13)
                                    {
                                        InsertValue("Z", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AA", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 14)
                                    {
                                        InsertValue("AB", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AC", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 15)
                                    {
                                        InsertValue("AD", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AE", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 16)
                                    {
                                        InsertValue("AF", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AG", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 17)
                                    {
                                        InsertValue("AH", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AI", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 18)
                                    {
                                        InsertValue("AJ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AK", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 19)
                                    {
                                        InsertValue("AL", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AM", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 20)
                                    {
                                        InsertValue("AN", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AO", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 21)
                                    {
                                        InsertValue("AP", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AQ", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 22)
                                    {
                                        InsertValue("AR", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AS", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 23)
                                    {
                                        InsertValue("AT", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AU", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 24)
                                    {
                                        InsertValue("AV", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AW", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 25)
                                    {
                                        InsertValue("AX", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("AY", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 26)
                                    {
                                        InsertValue("AZ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BA", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 27)
                                    {
                                        InsertValue("BB", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BC", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 28)
                                    {
                                        InsertValue("BD", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BE", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 29)
                                    {
                                        InsertValue("BF", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BG", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 30)
                                    {
                                        InsertValue("BH", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BI", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    else if (colIndex == 31)
                                    {
                                        InsertValue("BJ", rowIndex, Talktime, CellValues.Number, wsPart);
                                        InsertValue("BK", rowIndex, LCRCost, CellValues.Number, wsPart);
                                    }
                                    colIndex++;
                                }
                                rowIndex++;
                            }

                            //Used to execute the formula in all the cells
                            foreach (Cell cell in wsPart.Worksheet.Descendants<Cell>().Where(x => x.CellFormula != null))
                            {
                                cell.CellFormula.CalculateCell = BooleanValue.FromBoolean(true);
                            }

                            wsPart.Worksheet.Save();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No records found!");
                        log.Debug("No records found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FillStandardRecord : " + ex.Message);
                log.Error("FillStandardRecord : " + ex.Message);
            }
        }
        #endregion

        #region InsertRow
        //InsertRow(iRowIndex, wsPart);
        static void InsertRow(uint rowIndex, WorksheetPart wrksheetPart)
        {
            Worksheet worksheet = wrksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row = null;
            if (sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).Count() != 0)
            {
                Row refRow = sheetData.Elements<Row>().Where(r => rowIndex == r.RowIndex).First();
                //Copy row from refRow and insert it
                row = CopyToLine(refRow, rowIndex, sheetData);
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }
        }

        //Copy an existing row and insert it
        //We don't need to copy styles of a refRow because a CloneNode() or Clone() methods do it for us
        static Row CopyToLine(Row refRow, uint rowIndex, SheetData sheetData)
        {
            uint newRowIndex;
            var newRow = (Row)refRow.CloneNode(true);
            // Loop through all the rows in the worksheet with higher row 
            // index values than the one you just added. For each one,
            // increment the existing row index.
            IEnumerable<Row> rows = sheetData.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);
            foreach (Row row in rows)
            {
                newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value + 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }
                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }

            sheetData.InsertBefore(newRow, refRow);
            return newRow;
        }
        #endregion

        #region SendEmail
        static void SendEmail(string docName)
        {
            try
            {
                //Mail Sending 
                string mailContent = string.Empty;
                string mailSubject = string.Format(ConfigurationManager.AppSettings["MailSubject"], DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy HH:mm:ss"));
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = ConfigurationManager.AppSettings["MailTo"].Split(';').ToList();
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = ConfigurationManager.AppSettings["MailCc"].Split(';').ToList();
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item));
                }
                log.Debug("Email Send : {0}", Send(true, new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Vectone Mobile"), mailTo, mailCC, null, mailSubject, mailContent, docName) ? "Success" : "Failure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Mail Sending Error : " + ex.Message);
                log.Debug("Mail Sending Error : " + ex.Message);
            }
        }
        #endregion

        #region InsertValue
        private static void InsertValue(string columnName, uint rowIndex, object value, CellValues cellValues, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            Cell cell = row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                            if (cell != null)
                            {
                                cell.CellValue = new CellValue(Convert.ToString(value));
                                cell.DataType = new EnumValue<CellValues>(cellValues);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
        }
        #endregion

        #region GetCell
        private static Cell GetCell(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            try
            {
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
                {
                    Row row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
                    if (row != null)
                    {
                        string cellReference = columnName + rowIndex;
                        if (row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).Count() > 0)
                        {
                            return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
            }
            return null;
        }
        #endregion

        #region Send
        private static bool Send(bool isHtml, MailAddress mailFrom, MailAddressCollection mailTo, MailAddressCollection mailCC, MailAddressCollection mailBCC, string subject, string content, string attachmentFile)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.From = mailFrom;
                foreach (MailAddress addr in mailTo) email.To.Add(addr);
                if (mailCC != null)
                    foreach (MailAddress addr in mailCC) email.CC.Add(addr);
                if (mailBCC != null)
                    foreach (MailAddress addr in mailBCC) email.Bcc.Add(addr);
                email.Subject = subject;
                email.IsBodyHtml = isHtml;
                email.Body = content;
                email.Attachments.Add(new Attachment(attachmentFile));
                email.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                SmtpClient smtp = new SmtpClient();
                smtp.Send(email);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.Error(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
