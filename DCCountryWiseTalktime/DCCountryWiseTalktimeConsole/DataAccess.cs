﻿#region Assemblies
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using NLog;
#endregion

namespace DCCountryWiseTalktime
{
    public static class DataAccess
    {
        #region Declarations
        static Logger log = LogManager.GetLogger("Utility");
        #endregion

        #region GetTotalRecord
        public static List<TariffAutomationDCCountryWiseTotalReport> GetTotalRecord()
        {
            List<TariffAutomationDCCountryWiseTotalReport> OutputList = new List<TariffAutomationDCCountryWiseTotalReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_DC_countrywise_total_report";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                               
                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationDCCountryWiseTotalReport()
                        {
                            calldate = r.calldate,
                            Country = r.Country,
                            Talktime = r.Talktime,
                            LCRCost = r.LCRCost
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTotalRecord() : " + ex.Message);
                log.Error("GetTotalRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetBundleRecord
        public static List<TariffAutomationDCCountryWiseTotalReport> GetBundleRecord()
        {
            List<TariffAutomationDCCountryWiseTotalReport> OutputList = new List<TariffAutomationDCCountryWiseTotalReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_DC_countrywise_Bundle_usage";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationDCCountryWiseTotalReport()
                        {
                            calldate = r.calldate,
                            Country = r.Country,
                            Talktime = r.Talktime,
                            LCRCost = r.LCRCost
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetBundleRecord() : " + ex.Message);
                log.Error("GetBundleRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetFreeRecord
        public static List<TariffAutomationDCCountryWiseTotalReport> GetFreeRecord()
        {
            List<TariffAutomationDCCountryWiseTotalReport> OutputList = new List<TariffAutomationDCCountryWiseTotalReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_DC_countrywise_Freecredit_usage";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationDCCountryWiseTotalReport()
                        {
                            calldate = r.calldate,
                            Country = r.Country,
                            Talktime = r.Talktime,
                            LCRCost = r.LCRCost
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetFreeRecord() : " + ex.Message);
                log.Error("GetFreeRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion

        #region GetStandardRecord
        public static List<TariffAutomationDCCountryWiseTotalReport> GetStandardRecord()
        {
            List<TariffAutomationDCCountryWiseTotalReport> OutputList = new List<TariffAutomationDCCountryWiseTotalReport>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Tariff_Automation_DC_countrywise_Standard_usage";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure, commandTimeout: 0);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new TariffAutomationDCCountryWiseTotalReport()
                        {
                            calldate = r.calldate,
                            Country = r.Country,
                            Talktime = r.Talktime,
                            LCRCost = r.LCRCost
                        }));
                    }
                    return OutputList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetStandardRecord() : " + ex.Message);
                log.Error("GetStandardRecord() : " + ex.Message);
            }
            return null;
        }
        #endregion
    }
}
